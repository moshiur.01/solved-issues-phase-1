﻿using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.Report
{
    partial class frmUserIdWiseAccApprovedInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.lblSubAgent = new System.Windows.Forms.Label();
            this.lblAgent = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnShowReport = new System.Windows.Forms.Button();
            this.cmbBranchName = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbUserType = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbUserStatus = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpFromdate = new MISL.Ababil.Agent.UI.forms.CustomControls.CustomDateTimePicker();
            this.dtpToDate = new MISL.Ababil.Agent.UI.forms.CustomControls.CustomDateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbUserTp = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.SuspendLayout();
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.customTitlebar1.Location = new System.Drawing.Point(-1, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(712, 26);
            this.customTitlebar1.TabIndex = 9;
            // 
            // lblSubAgent
            // 
            this.lblSubAgent.AutoSize = true;
            this.lblSubAgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubAgent.Location = new System.Drawing.Point(12, 34);
            this.lblSubAgent.Name = "lblSubAgent";
            this.lblSubAgent.Size = new System.Drawing.Size(101, 16);
            this.lblSubAgent.TabIndex = 2;
            this.lblSubAgent.Text = "User Category :";
            this.lblSubAgent.Visible = false;
            // 
            // lblAgent
            // 
            this.lblAgent.AutoSize = true;
            this.lblAgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAgent.Location = new System.Drawing.Point(205, 130);
            this.lblAgent.Name = "lblAgent";
            this.lblAgent.Size = new System.Drawing.Size(77, 16);
            this.lblAgent.TabIndex = 0;
            this.lblAgent.Text = "From Date :";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.Location = new System.Drawing.Point(133, 112);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(0, 13);
            this.lblUserName.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(151, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(410, 25);
            this.label4.TabIndex = 6;
            this.label4.Text = "User Wise Account Approve Summary";
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(443, 207);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 31);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnShowReport
            // 
            this.btnShowReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnShowReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShowReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShowReport.ForeColor = System.Drawing.Color.White;
            this.btnShowReport.Location = new System.Drawing.Point(308, 207);
            this.btnShowReport.Name = "btnShowReport";
            this.btnShowReport.Size = new System.Drawing.Size(127, 31);
            this.btnShowReport.TabIndex = 4;
            this.btnShowReport.Text = "Show Report";
            this.btnShowReport.UseVisualStyleBackColor = false;
            this.btnShowReport.Click += new System.EventHandler(this.btnShowReport_Click);
            // 
            // cmbBranchName
            // 
            this.cmbBranchName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBranchName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbBranchName.FormattingEnabled = true;
            this.cmbBranchName.InputScopeAllowEmpty = false;
            this.cmbBranchName.IsValid = null;
            this.cmbBranchName.Location = new System.Drawing.Point(286, 98);
            this.cmbBranchName.Name = "cmbBranchName";
            this.cmbBranchName.PromptText = "(Select)";
            this.cmbBranchName.ReadOnly = false;
            this.cmbBranchName.ShowMandatoryMark = false;
            this.cmbBranchName.Size = new System.Drawing.Size(212, 21);
            this.cmbBranchName.TabIndex = 11;
            this.cmbBranchName.ValidationErrorMessage = "Validation Error!";
            // 
            // cmbUserType
            // 
            this.cmbUserType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUserType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbUserType.FormattingEnabled = true;
            this.cmbUserType.InputScopeAllowEmpty = false;
            this.cmbUserType.IsValid = null;
            this.cmbUserType.Location = new System.Drawing.Point(51, 33);
            this.cmbUserType.Name = "cmbUserType";
            this.cmbUserType.PromptText = "(Select)";
            this.cmbUserType.ReadOnly = false;
            this.cmbUserType.ShowMandatoryMark = false;
            this.cmbUserType.Size = new System.Drawing.Size(212, 21);
            this.cmbUserType.TabIndex = 16;
            this.cmbUserType.ValidationErrorMessage = "Validation Error!";
            this.cmbUserType.Visible = false;
            // 
            // cmbUserStatus
            // 
            this.cmbUserStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUserStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbUserStatus.FormattingEnabled = true;
            this.cmbUserStatus.InputScopeAllowEmpty = false;
            this.cmbUserStatus.IsValid = null;
            this.cmbUserStatus.Location = new System.Drawing.Point(454, -32);
            this.cmbUserStatus.Name = "cmbUserStatus";
            this.cmbUserStatus.PromptText = "(Select)";
            this.cmbUserStatus.ReadOnly = false;
            this.cmbUserStatus.ShowMandatoryMark = false;
            this.cmbUserStatus.Size = new System.Drawing.Size(230, 21);
            this.cmbUserStatus.TabIndex = 19;
            this.cmbUserStatus.ValidationErrorMessage = "Validation Error!";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(305, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 16);
            this.label2.TabIndex = 20;
            this.label2.Text = "User Type :";
            this.label2.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(65, -136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "User Status  :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(186, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "Branch Name :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(219, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "To Date :";
            // 
            // dtpFromdate
            // 
            this.dtpFromdate.Date = "25/01/2017";
            this.dtpFromdate.Location = new System.Drawing.Point(286, 127);
            this.dtpFromdate.MaximumSize = new System.Drawing.Size(400, 25);
            this.dtpFromdate.MinimumSize = new System.Drawing.Size(60, 25);
            this.dtpFromdate.Name = "dtpFromdate";
            this.dtpFromdate.PresetServerDate = true;
            this.dtpFromdate.Size = new System.Drawing.Size(212, 25);
            this.dtpFromdate.TabIndex = 28;
            this.dtpFromdate.Value = new System.DateTime(2017, 1, 25, 13, 7, 38, 436);
            // 
            // dtpToDate
            // 
            this.dtpToDate.Date = "25/01/2017";
            this.dtpToDate.Location = new System.Drawing.Point(286, 159);
            this.dtpToDate.MaximumSize = new System.Drawing.Size(400, 25);
            this.dtpToDate.MinimumSize = new System.Drawing.Size(60, 25);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.PresetServerDate = true;
            this.dtpToDate.Size = new System.Drawing.Size(212, 25);
            this.dtpToDate.TabIndex = 28;
            this.dtpToDate.Value = new System.DateTime(2017, 1, 25, 13, 7, 38, 436);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(412, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "User Status  :";
            this.label1.Visible = false;
            // 
            // cmbUserTp
            // 
            this.cmbUserTp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUserTp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbUserTp.FormattingEnabled = true;
            this.cmbUserTp.InputScopeAllowEmpty = false;
            this.cmbUserTp.IsValid = null;
            this.cmbUserTp.Location = new System.Drawing.Point(479, 24);
            this.cmbUserTp.Name = "cmbUserTp";
            this.cmbUserTp.PromptText = "(Select)";
            this.cmbUserTp.ReadOnly = false;
            this.cmbUserTp.ShowMandatoryMark = false;
            this.cmbUserTp.Size = new System.Drawing.Size(230, 21);
            this.cmbUserTp.TabIndex = 23;
            this.cmbUserTp.ValidationErrorMessage = "Validation Error!";
            this.cmbUserTp.Visible = false;
            // 
            // frmUserIdWiseAccApprovedInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(709, 252);
            this.Controls.Add(this.cmbBranchName);
            this.Controls.Add(this.cmbUserType);
            this.Controls.Add(this.cmbUserStatus);
            this.Controls.Add(this.cmbUserTp);
            this.Controls.Add(this.dtpToDate);
            this.Controls.Add(this.dtpFromdate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnShowReport);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblSubAgent);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblAgent);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.customTitlebar1);
            this.DisplayHeader = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmUserIdWiseAccApprovedInfo";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.DropShadow;
            this.ShowInTaskbar = false;
            this.Text = "User Wise Account Approve Summary";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmOutletUserInfoReport_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomTitlebar customTitlebar1;
        private System.Windows.Forms.Label lblSubAgent;
        private System.Windows.Forms.Label lblAgent;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnShowReport;
        private CustomComboBoxDropDownList cmbBranchName;
        private CustomComboBoxDropDownList cmbUserType;
        private CustomComboBoxDropDownList cmbUserStatus;
        private System.Windows.Forms.Label label2;
        private UI.forms.CustomControls.CustomDateTimePicker dtpToDate;
        private UI.forms.CustomControls.CustomDateTimePicker dtpFromdate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private CustomComboBoxDropDownList cmbUserTp;
    }
}