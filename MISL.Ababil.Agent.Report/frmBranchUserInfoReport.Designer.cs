﻿using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.Report
{
    partial class frmBranchUserInfoReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSubAgent = new System.Windows.Forms.Label();
            this.lblAgent = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnShowReport = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbBranchName = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbUserType = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbUserStatus = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbUserTp = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.SuspendLayout();
            // 
            // lblSubAgent
            // 
            this.lblSubAgent.AutoSize = true;
            this.lblSubAgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubAgent.Location = new System.Drawing.Point(25, 140);
            this.lblSubAgent.Name = "lblSubAgent";
            this.lblSubAgent.Size = new System.Drawing.Size(101, 16);
            this.lblSubAgent.TabIndex = 2;
            this.lblSubAgent.Text = "User Category :";
            // 
            // lblAgent
            // 
            this.lblAgent.AutoSize = true;
            this.lblAgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAgent.Location = new System.Drawing.Point(30, 106);
            this.lblAgent.Name = "lblAgent";
            this.lblAgent.Size = new System.Drawing.Size(96, 16);
            this.lblAgent.TabIndex = 0;
            this.lblAgent.Text = "Branch Name :";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.Location = new System.Drawing.Point(133, 112);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(0, 13);
            this.lblUserName.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(362, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "User Status  :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(205, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(343, 25);
            this.label4.TabIndex = 6;
            this.label4.Text = "Branch User Information Report";
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(443, 207);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 31);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnShowReport
            // 
            this.btnShowReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnShowReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShowReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShowReport.ForeColor = System.Drawing.Color.White;
            this.btnShowReport.Location = new System.Drawing.Point(308, 207);
            this.btnShowReport.Name = "btnShowReport";
            this.btnShowReport.Size = new System.Drawing.Size(127, 31);
            this.btnShowReport.TabIndex = 4;
            this.btnShowReport.Text = "Show Report";
            this.btnShowReport.UseVisualStyleBackColor = false;
            this.btnShowReport.Click += new System.EventHandler(this.btnShowReport_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(370, 140);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 16);
            this.label2.TabIndex = 20;
            this.label2.Text = "User Type :";
            // 
            // cmbBranchName
            // 
            this.cmbBranchName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBranchName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbBranchName.FormattingEnabled = true;
            this.cmbBranchName.InputScopeAllowEmpty = false;
            this.cmbBranchName.IsValid = null;
            this.cmbBranchName.Location = new System.Drawing.Point(129, 104);
            this.cmbBranchName.Name = "cmbBranchName";
            this.cmbBranchName.PromptText = "(Select)";
            this.cmbBranchName.ReadOnly = false;
            this.cmbBranchName.ShowMandatoryMark = false;
            this.cmbBranchName.Size = new System.Drawing.Size(212, 21);
            this.cmbBranchName.TabIndex = 11;
            this.cmbBranchName.ValidationErrorMessage = "Validation Error!";
            // 
            // cmbUserType
            // 
            this.cmbUserType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUserType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbUserType.FormattingEnabled = true;
            this.cmbUserType.InputScopeAllowEmpty = false;
            this.cmbUserType.IsValid = null;
            this.cmbUserType.Location = new System.Drawing.Point(129, 141);
            this.cmbUserType.Name = "cmbUserType";
            this.cmbUserType.PromptText = "(Select)";
            this.cmbUserType.ReadOnly = false;
            this.cmbUserType.ShowMandatoryMark = false;
            this.cmbUserType.Size = new System.Drawing.Size(212, 21);
            this.cmbUserType.TabIndex = 16;
            this.cmbUserType.ValidationErrorMessage = "Validation Error!";
            // 
            // cmbUserStatus
            // 
            this.cmbUserStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUserStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbUserStatus.FormattingEnabled = true;
            this.cmbUserStatus.InputScopeAllowEmpty = false;
            this.cmbUserStatus.IsValid = null;
            this.cmbUserStatus.Location = new System.Drawing.Point(454, 104);
            this.cmbUserStatus.Name = "cmbUserStatus";
            this.cmbUserStatus.PromptText = "(Select)";
            this.cmbUserStatus.ReadOnly = false;
            this.cmbUserStatus.ShowMandatoryMark = false;
            this.cmbUserStatus.Size = new System.Drawing.Size(230, 21);
            this.cmbUserStatus.TabIndex = 19;
            this.cmbUserStatus.ValidationErrorMessage = "Validation Error!";
            // 
            // cmbUserTp
            // 
            this.cmbUserTp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUserTp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbUserTp.FormattingEnabled = true;
            this.cmbUserTp.InputScopeAllowEmpty = false;
            this.cmbUserTp.IsValid = null;
            this.cmbUserTp.Location = new System.Drawing.Point(454, 141);
            this.cmbUserTp.Name = "cmbUserTp";
            this.cmbUserTp.PromptText = "(Select)";
            this.cmbUserTp.ReadOnly = false;
            this.cmbUserTp.ShowMandatoryMark = false;
            this.cmbUserTp.Size = new System.Drawing.Size(230, 21);
            this.cmbUserTp.TabIndex = 23;
            this.cmbUserTp.ValidationErrorMessage = "Validation Error!";
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.customTitlebar1.Location = new System.Drawing.Point(-1, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(723, 26);
            this.customTitlebar1.TabIndex = 9;
            // 
            // frmBranchUserInfoReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(720, 273);
            this.Controls.Add(this.cmbBranchName);
            this.Controls.Add(this.cmbUserType);
            this.Controls.Add(this.cmbUserStatus);
            this.Controls.Add(this.cmbUserTp);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnShowReport);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblSubAgent);
            this.Controls.Add(this.lblAgent);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.customTitlebar1);
            this.DisplayHeader = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmBranchUserInfoReport";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.DropShadow;
            this.ShowInTaskbar = false;
            this.Text = "Branch User Information Report";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmOutletUserInfoReport_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomTitlebar customTitlebar1;
        private System.Windows.Forms.Label lblSubAgent;
        private System.Windows.Forms.Label lblAgent;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnShowReport;
        private CustomComboBoxDropDownList cmbBranchName;
        private CustomComboBoxDropDownList cmbUserType;
        private CustomComboBoxDropDownList cmbUserStatus;
        private CustomComboBoxDropDownList cmbUserTp;
        private System.Windows.Forms.Label label2;
    }
}