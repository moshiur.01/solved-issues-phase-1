﻿using MISL.Ababil.Agent.CustomControls;
namespace MISL.Ababil.Agent.Report
{
    partial class frmOutletInfoReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbUpazilla = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbOutletStatus = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbAgentName = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.lblSubAgent = new System.Windows.Forms.Label();
            this.lblAgent = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblUserName = new System.Windows.Forms.Label();
            this.btnShowReport = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbDivision = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbDistrict = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.label2 = new System.Windows.Forms.Label();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbSubAgnetName = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.SuspendLayout();
            // 
            // cmbUpazilla
            // 
            this.cmbUpazilla.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUpazilla.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbUpazilla.FormattingEnabled = true;
            this.cmbUpazilla.InputScopeAllowEmpty = false;
            this.cmbUpazilla.IsValid = null;
            this.cmbUpazilla.ItemHeight = 13;
            this.cmbUpazilla.Location = new System.Drawing.Point(455, 68);
            this.cmbUpazilla.Name = "cmbUpazilla";
            this.cmbUpazilla.PromptText = "(Select)";
            this.cmbUpazilla.ReadOnly = false;
            this.cmbUpazilla.ShowMandatoryMark = false;
            this.cmbUpazilla.Size = new System.Drawing.Size(230, 21);
            this.cmbUpazilla.TabIndex = 9;
            this.cmbUpazilla.ValidationErrorMessage = "Validation Error!";
            this.cmbUpazilla.Visible = false;
            // 
            // cmbOutletStatus
            // 
            this.cmbOutletStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOutletStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbOutletStatus.FormattingEnabled = true;
            this.cmbOutletStatus.InputScopeAllowEmpty = false;
            this.cmbOutletStatus.IsValid = null;
            this.cmbOutletStatus.ItemHeight = 13;
            this.cmbOutletStatus.Location = new System.Drawing.Point(151, 33);
            this.cmbOutletStatus.Name = "cmbOutletStatus";
            this.cmbOutletStatus.PromptText = "(Select)";
            this.cmbOutletStatus.ReadOnly = false;
            this.cmbOutletStatus.ShowMandatoryMark = false;
            this.cmbOutletStatus.Size = new System.Drawing.Size(230, 21);
            this.cmbOutletStatus.TabIndex = 3;
            this.cmbOutletStatus.ValidationErrorMessage = "Validation Error!";
            this.cmbOutletStatus.Visible = false;
            // 
            // cmbAgentName
            // 
            this.cmbAgentName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAgentName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbAgentName.FormattingEnabled = true;
            this.cmbAgentName.InputScopeAllowEmpty = false;
            this.cmbAgentName.IsValid = null;
            this.cmbAgentName.ItemHeight = 13;
            this.cmbAgentName.Location = new System.Drawing.Point(235, 111);
            this.cmbAgentName.Name = "cmbAgentName";
            this.cmbAgentName.PromptText = "(Select)";
            this.cmbAgentName.ReadOnly = false;
            this.cmbAgentName.ShowMandatoryMark = false;
            this.cmbAgentName.Size = new System.Drawing.Size(230, 21);
            this.cmbAgentName.TabIndex = 1;
            this.cmbAgentName.ValidationErrorMessage = "Validation Error!";
            this.cmbAgentName.SelectedIndexChanged += new System.EventHandler(this.cmbAgentName_SelectedIndexChanged);
            // 
            // lblSubAgent
            // 
            this.lblSubAgent.AutoSize = true;
            this.lblSubAgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubAgent.Location = new System.Drawing.Point(60, 38);
            this.lblSubAgent.Name = "lblSubAgent";
            this.lblSubAgent.Size = new System.Drawing.Size(88, 16);
            this.lblSubAgent.TabIndex = 2;
            this.lblSubAgent.Text = "Outlet Status :";
            this.lblSubAgent.Visible = false;
            // 
            // lblAgent
            // 
            this.lblAgent.AutoSize = true;
            this.lblAgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAgent.Location = new System.Drawing.Point(182, 115);
            this.lblAgent.Name = "lblAgent";
            this.lblAgent.Size = new System.Drawing.Size(49, 16);
            this.lblAgent.TabIndex = 0;
            this.lblAgent.Text = "Agent :";
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(514, 207);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 31);
            this.btnClose.TabIndex = 11;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.Location = new System.Drawing.Point(239, 106);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(0, 13);
            this.lblUserName.TabIndex = 12;
            // 
            // btnShowReport
            // 
            this.btnShowReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnShowReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShowReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShowReport.ForeColor = System.Drawing.Color.White;
            this.btnShowReport.Location = new System.Drawing.Point(379, 207);
            this.btnShowReport.Name = "btnShowReport";
            this.btnShowReport.Size = new System.Drawing.Size(127, 31);
            this.btnShowReport.TabIndex = 10;
            this.btnShowReport.Text = "Show Report";
            this.btnShowReport.UseVisualStyleBackColor = false;
            this.btnShowReport.Click += new System.EventHandler(this.btnShowReport_Click_1);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(388, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 16);
            this.label3.TabIndex = 8;
            this.label3.Text = "Upazilla :";
            this.label3.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(386, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Division  :";
            this.label1.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(219, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(276, 25);
            this.label4.TabIndex = 13;
            this.label4.Text = "Outlet Information Report";
            // 
            // cmbDivision
            // 
            this.cmbDivision.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDivision.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbDivision.FormattingEnabled = true;
            this.cmbDivision.InputScopeAllowEmpty = false;
            this.cmbDivision.IsValid = null;
            this.cmbDivision.ItemHeight = 13;
            this.cmbDivision.Location = new System.Drawing.Point(455, 32);
            this.cmbDivision.Name = "cmbDivision";
            this.cmbDivision.PromptText = "(Select)";
            this.cmbDivision.ReadOnly = false;
            this.cmbDivision.ShowMandatoryMark = false;
            this.cmbDivision.Size = new System.Drawing.Size(230, 21);
            this.cmbDivision.TabIndex = 5;
            this.cmbDivision.ValidationErrorMessage = "Validation Error!";
            this.cmbDivision.Visible = false;
            this.cmbDivision.SelectedIndexChanged += new System.EventHandler(this.cmbDivision_SelectedIndexChanged);
            // 
            // cmbDistrict
            // 
            this.cmbDistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDistrict.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbDistrict.FormattingEnabled = true;
            this.cmbDistrict.InputScopeAllowEmpty = false;
            this.cmbDistrict.IsValid = null;
            this.cmbDistrict.ItemHeight = 13;
            this.cmbDistrict.Location = new System.Drawing.Point(151, 72);
            this.cmbDistrict.Name = "cmbDistrict";
            this.cmbDistrict.PromptText = "(Select)";
            this.cmbDistrict.ReadOnly = false;
            this.cmbDistrict.ShowMandatoryMark = false;
            this.cmbDistrict.Size = new System.Drawing.Size(230, 21);
            this.cmbDistrict.TabIndex = 7;
            this.cmbDistrict.ValidationErrorMessage = "Validation Error!";
            this.cmbDistrict.Visible = false;
            this.cmbDistrict.SelectedIndexChanged += new System.EventHandler(this.cmbDistrict_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(94, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "District :";
            this.label2.Visible = false;
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(646, 26);
            this.customTitlebar1.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(155, 149);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "Sub Agent :";
            // 
            // cmbSubAgnetName
            // 
            this.cmbSubAgnetName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSubAgnetName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbSubAgnetName.FormattingEnabled = true;
            this.cmbSubAgnetName.InputScopeAllowEmpty = false;
            this.cmbSubAgnetName.IsValid = null;
            this.cmbSubAgnetName.ItemHeight = 13;
            this.cmbSubAgnetName.Location = new System.Drawing.Point(235, 146);
            this.cmbSubAgnetName.Name = "cmbSubAgnetName";
            this.cmbSubAgnetName.PromptText = "(Select)";
            this.cmbSubAgnetName.ReadOnly = false;
            this.cmbSubAgnetName.ShowMandatoryMark = false;
            this.cmbSubAgnetName.Size = new System.Drawing.Size(230, 21);
            this.cmbSubAgnetName.TabIndex = 1;
            this.cmbSubAgnetName.ValidationErrorMessage = "Validation Error!";
            // 
            // frmOutletInfoReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(646, 274);
            this.Controls.Add(this.cmbUpazilla);
            this.Controls.Add(this.cmbOutletStatus);
            this.Controls.Add(this.cmbAgentName);
            this.Controls.Add(this.cmbDivision);
            this.Controls.Add(this.cmbDistrict);
            this.Controls.Add(this.cmbSubAgnetName);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblSubAgent);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblAgent);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.btnShowReport);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmOutletInfoReport";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Outlet Info Report";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmOutletInfoReport_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomComboBoxDropDownList cmbUpazilla;
        private CustomComboBoxDropDownList cmbOutletStatus;
        private CustomComboBoxDropDownList cmbAgentName;
        private System.Windows.Forms.Label lblSubAgent;
        private System.Windows.Forms.Label lblAgent;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Button btnShowReport;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private CustomComboBoxDropDownList cmbDivision;
        private CustomComboBoxDropDownList cmbDistrict;
        private System.Windows.Forms.Label label2;
        private CustomTitlebar customTitlebar1;
        private CustomComboBoxDropDownList cmbSubAgnetName;
        private System.Windows.Forms.Label label5;
    }
}