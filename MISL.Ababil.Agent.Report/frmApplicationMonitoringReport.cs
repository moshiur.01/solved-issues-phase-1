﻿using MetroFramework.Forms;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Report.Properties;
using MISL.Ababil.Agent.Services;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Report.Reports;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using CrystalDecisions.CrystalReports.Engine;
using MISL.Ababil.Agent.Module.Common.UI.MessageUI;

namespace MISL.Ababil.Agent.Report
{
    public partial class frmApplicationMonitoringReport : Form
    {
        private List<AgentInformation> objAgentInfoList = new List<AgentInformation>();
        List<ApplicationMonitoringDto> applicationMonitoringDtos = new List<ApplicationMonitoringDto>();
        private AgentServices agentServices = new AgentServices();
        private AgentInformation agentInformation = new AgentInformation();
        public frmApplicationMonitoringReport()
        {
            InitializeComponent();
            LoadSetupData();
        }

        private void btnShowReport_Click(object sender, EventArgs e)
        {
            crApplicationMonitoringReport report = new crApplicationMonitoringReport();
            frmReportViewer frm = new frmReportViewer();
            ReportHeaders rptHeaders = new ReportHeaders();
            rptHeaders = UtilityServices.getReportHeaders("Outlet User Information Report");

            TextObject txtBankName = report.ReportDefinition.ReportObjects["txtBankName"] as TextObject;
            TextObject txtBranchName = report.ReportDefinition.ReportObjects["txtBranchName"] as TextObject;
            TextObject txtBranchAddress = report.ReportDefinition.ReportObjects["txtBranchAddress"] as TextObject;
            TextObject txtReportHeading = report.ReportDefinition.ReportObjects["txtReportHeading"] as TextObject;
            //TextObject txtPrintDate = report.ReportDefinition.ReportObjects["txtPrintDate"] as TextObject;


            TextObject txtAgentName = report.ReportDefinition.ReportObjects["txtAgentName"] as TextObject;
            TextObject txtOutletName = report.ReportDefinition.ReportObjects["txtOutletName"] as TextObject;

            if (rptHeaders != null)
            {
                if (rptHeaders.branchDto != null)
                {
                    txtBankName.Text = rptHeaders.branchDto.bankName;
                    txtBranchName.Text = rptHeaders.branchDto.branchName;
                    txtBranchAddress.Text = rptHeaders.branchDto.branchAddress;
                }
                txtReportHeading.Text = rptHeaders.reportHeading;
                //txtPrintUser.Text = rptHeaders.printUser;
                //txtPrintDate.Text = rptHeaders.printDate.ToString("dd-MM-yyyy").Replace("-", "/");
                //txtAgentName.Text =((AgentInformation)cmbAgentName.SelectedItem).businessName;
                string txt = ((AgentInformation)cmbAgentName.SelectedItem).businessName;
                txtAgentName.Text = DoInitCap.ConvertTo_ProperCase(txt);
                txtOutletName.Text = ((SubAgentInformation)cmbOutletName.SelectedItem).name;

            }


            ApplicationMonitoringDto applicationMonitoringDto = new ApplicationMonitoringDto();
            applicationMonitoringDto.accountName = "ajfkds";
            applicationMonitoringDto.accountNo = "0123456789123";
            applicationMonitoringDto.applyDate = "02/05/16";
            applicationMonitoringDto.applyUser = "rajib";
            applicationMonitoringDto.approveDate = "03/05/16";
            applicationMonitoringDto.approveOfficer = "rakib";
            applicationMonitoringDto.submitDate = "02/05/16";
            applicationMonitoringDto.submitUser = "rafi";
            applicationMonitoringDto.userStatus = "active";
            applicationMonitoringDto.verifyDate = "02/05/16";
            applicationMonitoringDto.verifyOfficer = "jarif";
            applicationMonitoringDto.refNo = "05";
            applicationMonitoringDto.mobileNo = "0152121212";
            applicationMonitoringDtos.Add(applicationMonitoringDto);
            report.SetDataSource(applicationMonitoringDtos);


            frm.crvReportViewer.ReportSource = report;
            frm.Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void LoadSetupData()
        {
            objAgentInfoList = agentServices.getAgentInfoBranchWise();
            BindingSource bsAgent = new BindingSource();
            bsAgent.DataSource = objAgentInfoList;

            AgentInformation agSelect = new AgentInformation();
            agSelect.businessName = Resources.SetupData__Select_;
            objAgentInfoList.Insert(0, agSelect);
            //AgentInformation agAll = new AgentInformation();
            //agAll.businessName = Resources.SetupData__All_;
            //objAgentInfoList.Insert(1, agAll);

            UtilityServices.fillComboBox(cmbAgentName, bsAgent, "businessName", "id");
            cmbAgentName.Text = "Select";
            cmbAgentName.SelectedIndex = 0;


        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        private void setSubagent()
        {
            if (agentInformation != null)
            {
                BindingSource bs = new BindingSource();
                bs.DataSource = agentInformation.subAgents;

                try
                {
                    SubAgentInformation saiSelect = new SubAgentInformation();
                    saiSelect.name = "(Select)";

                    //SubAgentInformation saiAll = new SubAgentInformation();
                    //saiAll.name = "(All)";

                    agentInformation.subAgents.Insert(0, saiSelect);
                    // agentInformation.subAgents.Insert(1, saiAll);
                }
                catch (Exception exp)
                {
                }

                UtilityServices.fillComboBox(cmbOutletName, bs, "name", "id");
                if (cmbOutletName.Items.Count > 0)
                {
                    cmbOutletName.SelectedIndex = 0;
                }
            }
        }
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmApplicationMonitoringReport_Load(object sender, EventArgs e)
        {

        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbAgentName_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!cb.Focused)
            {
                return;
            }

            try
            {
                if (cmbAgentName.SelectedIndex > 0)
                {
                    agentInformation = agentServices.getAgentInfoById(cmbAgentName.SelectedValue.ToString());
                }
                if (cmbAgentName.SelectedIndex < 1)
                {
                    cmbOutletName.DataSource = null;
                    cmbOutletName.Items.Clear();
                    return;
                }
            }
            catch (Exception ex)
            {
                MsgBox.showError(ex.Message);
            }
            setSubagent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            crApplicationMonitoringReport report = new crApplicationMonitoringReport();
            frmReportViewer frm = new frmReportViewer();
            ReportHeaders rptHeaders = new ReportHeaders();
            rptHeaders = UtilityServices.getReportHeaders("Outlet User Information Report");

            TextObject txtBankName = report.ReportDefinition.ReportObjects["txtBankName"] as TextObject;
            TextObject txtBranchName = report.ReportDefinition.ReportObjects["txtBranchName"] as TextObject;
            TextObject txtBranchAddress = report.ReportDefinition.ReportObjects["txtBranchAddress"] as TextObject;
            TextObject txtReportHeading = report.ReportDefinition.ReportObjects["txtReportHeading"] as TextObject;
            //TextObject txtPrintDate = report.ReportDefinition.ReportObjects["txtPrintDate"] as TextObject;


            TextObject txtAgentName = report.ReportDefinition.ReportObjects["txtAgentName"] as TextObject;
            TextObject txtOutletName = report.ReportDefinition.ReportObjects["txtOutletName"] as TextObject;

            if (rptHeaders != null)
            {
                if (rptHeaders.branchDto != null)
                {
                    txtBankName.Text = rptHeaders.branchDto.bankName;
                    txtBranchName.Text = rptHeaders.branchDto.branchName;
                    txtBranchAddress.Text = rptHeaders.branchDto.branchAddress;
                }
                txtReportHeading.Text = rptHeaders.reportHeading;
                //txtPrintUser.Text = rptHeaders.printUser;
                //txtPrintDate.Text = rptHeaders.printDate.ToString("dd-MM-yyyy").Replace("-", "/");
                //txtAgentName.Text =((AgentInformation)cmbAgentName.SelectedItem).businessName;
                string txt = ((AgentInformation)cmbAgentName.SelectedItem).businessName;
                txtAgentName.Text = DoInitCap.ConvertTo_ProperCase(txt);
                txtOutletName.Text = ((SubAgentInformation)cmbOutletName.SelectedItem).name;

            }


            ApplicationMonitoringDto applicationMonitoringDto = new ApplicationMonitoringDto();
            applicationMonitoringDto.accountName = "ajfkds";
            applicationMonitoringDto.accountNo = "0123456789123";
            applicationMonitoringDto.applyDate = "02/05/16";
            applicationMonitoringDto.applyUser = "rajib";
            applicationMonitoringDto.approveDate = "03/05/16";
            applicationMonitoringDto.approveOfficer = "rakib";
            applicationMonitoringDto.submitDate = "02/05/16";
            applicationMonitoringDto.submitUser = "rafi";
            applicationMonitoringDto.userStatus = "active";
            applicationMonitoringDto.verifyDate = "02/05/16";
            applicationMonitoringDto.verifyOfficer = "jarif";
            applicationMonitoringDto.refNo = "05";
            applicationMonitoringDto.mobileNo = "0152121212";
            applicationMonitoringDtos.Add(applicationMonitoringDto);
            report.SetDataSource(applicationMonitoringDtos);


            frm.crvReportViewer.ReportSource = report;
            frm.Show();

        }
    }
}
