﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Infrastructure.Models.models.transaction;
using MISL.Ababil.Agent.Infrastructure.Validation;
using MISL.Ababil.Agent.Report.Reports;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Report.DataSets;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using MetroFramework.Forms;
using System.Globalization;
using MISL.Ababil.Agent.Common.UI;
using MISL.Ababil.Agent.Module.Common.UI.MessageUI;

namespace MISL.Ababil.Agent.Report
{
    public partial class frmOnlineReport : MetroForm
    {
        public GUI gui = new GUI();
        List<AgentInformation> objAgentInfoList = new List<AgentInformation>();
        AgentServices objAgentServices = new AgentServices();
        AgentInformation agentInformation = new AgentInformation();
        AgentServices agentServices = new AgentServices();

        OnlineReportDS agentDS = new OnlineReportDS();

        //public List<AccountMonitoringInfo> _accountMonitoringList = new List<AccountMonitoringInfo>();
        private List<AgentProduct> _agentProducts;

        List<SubAgentInformation> subAgentList = new List<SubAgentInformation>();       // WALI :: 14-Jan-2016

        public frmOnlineReport()
        {
            InitializeComponent();
            GetSetupData();
            try
            {
                //cmbAgentName.SelectedIndex = 0;
                cmbAgentName.SelectedIndex = -1;
            }
            catch (Exception exp)
            { }
            controlActivity();
            ConfigUIEnhancement();
        }

        public void ConfigUIEnhancement()
        {
            gui = new GUI(this);
            gui.Config(ref cmbAgentName, ValidCheck.VALIDATIONTYPES.COMBOBOX_EMPTY, null);
            gui.Config(ref cmbSubAgnetName);
            gui.Config(ref dtpFromDate);
            gui.Config(ref dtpToDate);
        }

        private void controlActivity()
        {
            try
            {
                if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_CENTRALLY.ToString())) //branch user
                {
                    cmbAgentName.Enabled = true;
                    cmbSubAgnetName.Enabled = true;
                }
                else if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_AGENTWISE.ToString())) //agent user
                {
                    cmbAgentName.SelectedValue = UtilityServices.getCurrentAgent().id;
                    cmbAgentName.Enabled = false;
                    cmbSubAgnetName.Enabled = true;
                    //agentInformation = agentServices.getAgentInfoById(UtilityServices.getCurrentAgent().id.ToString());
                    subAgentList = agentServices.GetSubagentsByAgentId(UtilityServices.getCurrentAgent().id);                  // WALI :: 14-Jan-2016
                    setSubagent();
                }
                else
                {
                    SubAgentInformation currentSubagentInfo = UtilityServices.getCurrentSubAgent();
                    cmbAgentName.SelectedValue = currentSubagentInfo.agent.id;
                    //agentInformation = agentServices.getAgentInfoById(currentSubagentInfo.agent.id.ToString());
                    subAgentList = agentServices.GetSubagentsByAgentId(UtilityServices.getCurrentAgent().id);                  // WALI :: 14-Jan-2016
                    setSubagent();
                    cmbAgentName.Enabled = false;
                    cmbSubAgnetName.SelectedValue = currentSubagentInfo.id;
                    cmbSubAgnetName.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {

            //chacking valid date
            DateTime fromDate, toDate;
            try
            {
                //fromDate = UtilityServices.ParseDateTime(dtpFromDate.Date);                
                fromDate = DateTime.ParseExact(dtpFromDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
            }
            catch
            {
                CustomMessage.showWarning("Please enter the Date in correct format!!");
                return;
            }
            try
            {
                //toDate = UtilityServices.ParseDateTime(dtpToDate.Date);
                toDate = DateTime.ParseExact(dtpToDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
            }
            catch
            {
                CustomMessage.showWarning("Please enter the Date in correct format!!");
                return;
            }

            if (SessionInfo.currentDate < toDate)
            {
                CustomMessage.showWarning("Future date is not allowed!!");
                return;
            }
            if (fromDate > toDate)
            {
                CustomMessage.showWarning("From date can not be greater than to date!!.");
                return;
            }

            try
            {
                if (cmbAgentName.SelectedIndex > 0)
                {
                    crOnlineReport report = new crOnlineReport();
                    frmReportViewer viewer = new frmReportViewer();

                    ReportHeaders rptHeaders = new ReportHeaders();
                    rptHeaders = UtilityServices.getReportHeaders("Online Report");

                    TextObject txtBankName = report.ReportDefinition.ReportObjects["txtBankName"] as TextObject;
                    TextObject txtBranchName = report.ReportDefinition.ReportObjects["txtBranchName"] as TextObject;
                    TextObject txtBranchAddress = report.ReportDefinition.ReportObjects["txtBranchAddress"] as TextObject;
                    TextObject txtReportHeading = report.ReportDefinition.ReportObjects["txtReportHeading"] as TextObject;
                    TextObject txtPrintUser = report.ReportDefinition.ReportObjects["txtPrintUser"] as TextObject;
                    TextObject txtPrintDate = report.ReportDefinition.ReportObjects["txtPrintDate"] as TextObject;
                    TextObject txtFromDate = report.ReportDefinition.ReportObjects["txtFromDate"] as TextObject;
                    TextObject txtToDate = report.ReportDefinition.ReportObjects["txtToDate"] as TextObject;

                    if (rptHeaders != null)
                    {
                        if (rptHeaders.branchDto != null)
                        {
                            txtBankName.Text = rptHeaders.branchDto.bankName;
                            txtBranchName.Text = rptHeaders.branchDto.branchName;
                            txtBranchAddress.Text = rptHeaders.branchDto.branchAddress;
                        }
                        txtReportHeading.Text = rptHeaders.reportHeading;
                        txtPrintUser.Text = rptHeaders.printUser;
                        txtPrintDate.Text = rptHeaders.printDate.ToString("dd/MM/yyyy").Replace("-", "/");
                    }
                    txtFromDate.Text = dtpFromDate.Date;
                    txtToDate.Text = dtpToDate.Date;

                    LoadAccountMonitoring();
                    report.SetDataSource(agentDS);

                    viewer.crvReportViewer.ReportSource = report;
                    viewer.ShowDialog(this);
                }
                else
                {
                    CustomMessage.showWarning("Please select an option!");
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        private void LoadAccountMonitoring()
        {
            //AccountMonitoringInfo accountMonitoringInfo;
            //_accountMonitoringList.Clear();
            AccountSearchDto accountSearchDto = FillAccountSearchDto();
            ServiceResult result;
            agentDS = new OnlineReportDS();

            try
            {
                result = AgentServices.GetOnlineReport(accountSearchDto);

                if (!result.Success)
                {
                    CustomMessage.showError(result.Message);
                    return;
                }

                List<OnlineReportResultDto> accountMonitoring = result.ReturnedObject as List<OnlineReportResultDto>;

                if (accountMonitoring != null)
                {
                    foreach (OnlineReportResultDto account in accountMonitoring)
                    {
                        OnlineReportDS.OnlineReportDTRow newRow = agentDS.OnlineReportDT.NewOnlineReportDTRow();

                        newRow.agentId = account.agentId.ToString();
                        newRow.agentName = account.agentName;
                        newRow.outletId = account.outletId.ToString();
                        newRow.oetletName = account.outletName;
                        newRow.trDate = account.trDate;

                        newRow.accNo = account.accNo;
                        newRow.accName = account.accName;

                        newRow.branchName = account.branchName;
                        newRow.debit = account.debitAmount;

                        newRow.credit= account.creditAmount;
                        newRow.particulars = account.particulars;

                        //newRow.NoOfMTDAccount = account.noOfMTDAccount ?? 0;
                        //newRow.MTDAccBalance = account.mtdAccBalance ?? 0;

                        //newRow.NoOfInvestmentAccount = account.noOfInvestmentAccount ?? 0;
                        //newRow.InvestedAmount = account.investedAmount ?? 0;

                        agentDS.OnlineReportDT.AddOnlineReportDTRow(newRow);
                    }
                }
                agentDS.AcceptChanges();
            }
            catch (Exception)
            {
                //ignored
            }
        }

        private AccountSearchDto FillAccountSearchDto()
        {
            AccountSearchDto accountSearchDto = new AccountSearchDto();

            if (this.cmbAgentName.SelectedIndex > -1)
            {
                if (cmbAgentName.SelectedIndex != 0 || cmbAgentName.SelectedIndex != 1)
                {
                    accountSearchDto.agent = new AgentInformation { id = (long)cmbAgentName.SelectedValue };
                }
                if (cmbAgentName.SelectedIndex == 1 || cmbAgentName.SelectedIndex == 0)
                {
                    accountSearchDto.agent = null; // new AgentInformation();
                }
                if (cmbAgentName.SelectedIndex == 0)
                {
                }
            }
            if (this.cmbSubAgnetName.SelectedIndex > -1)
            {
                if (cmbSubAgnetName.SelectedIndex != 0 || cmbSubAgnetName.SelectedIndex != 1)
                {
                    accountSearchDto.subAgent = new SubAgentInformation { id = (long)cmbSubAgnetName.SelectedValue };
                }
                if (cmbSubAgnetName.SelectedIndex == 1 || cmbSubAgnetName.SelectedIndex == 0)
                {
                    accountSearchDto.subAgent = null; // new SubAgentInformation();
                }
                if (cmbSubAgnetName.SelectedIndex == 0)
                {
                }
            }

            accountSearchDto.fromDate = UtilityServices.GetLongDate
            (
                DateTime.ParseExact(dtpFromDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture)
            );

            accountSearchDto.toDate = UtilityServices.GetLongDate
            (
                DateTime.ParseExact(dtpToDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture)
            );

            return accountSearchDto;
        }

        private void GetSetupData()
        {

            try
            {
                setAgentList();
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
        }
        private void setAgentList()
        {
            objAgentInfoList = objAgentServices.getAgentInfoBranchWise();
            BindingSource bs = new BindingSource();
            bs.DataSource = objAgentInfoList;

            AgentInformation agSelect = new AgentInformation();
            agSelect.businessName = "(Select)";
            objAgentInfoList.Insert(0, agSelect);
            AgentInformation agAll = new AgentInformation();
            agAll.businessName = "(All)";
            objAgentInfoList.Insert(1, agAll);

            UtilityServices.fillComboBox(cmbAgentName, bs, "businessName", "id");
            //            cmbAgentName.Text = "Select";
            cmbAgentName.SelectedIndex = 0;
        }
        private void cmbAgentName_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!cb.Focused)
            {
                return;
            }

            try
            {
                if (cmbAgentName.SelectedIndex > 1)
                {
                    //agentInformation = agentServices.getAgentInfoById(cmbAgentName.SelectedValue.ToString());
                    subAgentList = agentServices.GetSubagentsByAgentId(long.Parse(cmbAgentName.SelectedValue.ToString()));         // WALI :: 14-Jan-2016
                }
                if (cmbAgentName.SelectedIndex == 1)
                {
                    //agentInformation = agentServices.getAgentInfoById(null);
                }
                if (cmbAgentName.SelectedIndex < 2)
                {
                    cmbSubAgnetName.DataSource = null;
                    cmbSubAgnetName.Items.Clear();
                    return;
                }
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
            setSubagent();
        }
        private void setSubagent()
        {
            #region WALI :: Commented :: 14-Jan-2016
            //if (agentInformation != null)
            //{
            //    BindingSource bs = new BindingSource();
            //    bs.DataSource = agentInformation.subAgents;

            //    try
            //    {
            //        SubAgentInformation saiSelect = new SubAgentInformation();
            //        saiSelect.name = "(Select)";

            //        SubAgentInformation saiAll = new SubAgentInformation();
            //        saiAll.name = "(All)";

            //        agentInformation.subAgents.Insert(0, saiSelect);
            //        agentInformation.subAgents.Insert(1, saiAll);
            //    }
            //    catch (Exception exp)
            //    { }

            //    UtilityServices.fillComboBox(cmbSubAgnetName, bs, "name", "id");
            //    if (cmbSubAgnetName.Items.Count > 0)
            //    {
            //        cmbSubAgnetName.SelectedIndex = 0;
            //    }
            //}
            #endregion

            if (subAgentList != null)
            {
                BindingSource bs = new BindingSource();
                bs.DataSource = subAgentList;

                try
                {
                    SubAgentInformation saiSelect = new SubAgentInformation();
                    saiSelect.name = "(Select)";

                    SubAgentInformation saiAll = new SubAgentInformation();
                    saiAll.name = "(All)";

                    subAgentList.Insert(0, saiSelect);
                    subAgentList.Insert(1, saiAll);
                }
                catch (Exception exp)
                { }

                UtilityServices.fillComboBox(cmbSubAgnetName, bs, "name", "id");
                if (cmbSubAgnetName.Items.Count > 0)
                {
                    cmbSubAgnetName.SelectedIndex = 0;
                }
            }
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {

        }



        private void frmTrMonitoringRport_Load(object sender, EventArgs e)
        {
            dtpFromDate.Value = SessionInfo.currentDate;
            dtpToDate.Value = SessionInfo.currentDate;
        }

        private void frmAccountMonitoringRport_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }
    }
    //public class AccountMonitoringInfo
    //{
    //    public long AgentId = 0;
    //    public string AgentName = "";
    //    public long SubAgentId = 0;
    //    public string SubAgentName = "";
    //    public decimal? AgentAccBalance = 0;

    //    public long NoOfMSDAccount = 0;
    //    public decimal MSDAccBalance = 0;

    //    public long NoOfCDAccount = 0;
    //    public decimal CDAccBalance = 0;

    //    public long NoOfITDAccount = 0;
    //    public decimal ITDAccBalance = 0;

    //    public long NoOfMTDAccount = 0;
    //    public decimal MTDAccBalance = 0;

    //    public long NoOfInvestmentAccount = 0;
    //    public decimal InvestedAmount = 0;
    //}


}
