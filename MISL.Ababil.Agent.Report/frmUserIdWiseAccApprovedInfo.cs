﻿using MetroFramework.Forms;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Report.Properties;
using MISL.Ababil.Agent.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Report.Reports;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using CrystalDecisions.CrystalReports.Engine;
using MISL.Ababil.Agent.Common.UI;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Report.DataSets;
using System.Globalization;
using MISL.Ababil.Agent.Communication;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Module.Common.UI.MessageUI;

namespace MISL.Ababil.Agent.Report
{
    public partial class frmUserIdWiseAccApprovedInfo : MetroForm
    {

        private List<BankUserReportDto> BankUserReportDtos;
        private List<OutletUserInfoReportSearchDto> result;
        UserIdWiseApprovedAccDS userIdWiseApprovedAcc = new UserIdWiseApprovedAccDS();
        // List<OutletUserInfoReportResultDto> _outletUserInfoReportResultDto;

        public frmUserIdWiseAccApprovedInfo()
        {
            InitializeComponent();
            LoadSetupData();


        }

        private UserIdWiseAccApproveSearchDto FillSearchDto()
        {
            UserIdWiseAccApproveSearchDto userIdWiseAccApproveSearchDto = new UserIdWiseAccApproveSearchDto();
            if (cmbBranchName.SelectedIndex == 0)
            {
                MsgBox.showWarning("Please select a branch");
                return null;
            }
            else
            {
                if (cmbBranchName.SelectedIndex == 1)
                {
                    userIdWiseAccApproveSearchDto.branchId = 0;
                }

                else
                {
                    userIdWiseAccApproveSearchDto.branchId = (long)cmbBranchName.SelectedValue;
                }

            }
            if (dtpFromdate.Value > SessionInfo.currentDate || dtpToDate.Value > SessionInfo.currentDate)
            {
                MsgBox.showWarning("Future Date Not Allowed ");
                return null;

            }
            if (dtpFromdate.Value > dtpToDate.Value)
            {
                MsgBox.showWarning("From Date Should Not be Greater Than To Date");
                return null;
            }
            else
            {
                userIdWiseAccApproveSearchDto.fromDate = UtilityServices.GetLongDate(DateTime.ParseExact(dtpFromdate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture));
                userIdWiseAccApproveSearchDto.toDate = UtilityServices.GetLongDate(DateTime.ParseExact(dtpToDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture));

            }


            return userIdWiseAccApproveSearchDto;


        }

        private void btnShowReport_Click(object sender, EventArgs e)
        {

            try
            {
                if (LoadbBranchUserInfoReport() == false)
                {
                    return;
                }

                crUserIdWiseAccApprovedReport report = new crUserIdWiseAccApprovedReport();
                frmReportViewer frm = new frmReportViewer();
                ReportHeaders rptHeaders = new ReportHeaders();
                rptHeaders = UtilityServices.getReportHeaders("User Wise Account Approve Summary");

                TextObject txtBankName = report.ReportDefinition.ReportObjects["txtBankName"] as TextObject;
                TextObject txtBranchName = report.ReportDefinition.ReportObjects["txtBranchName"] as TextObject;
                TextObject txtBranchAddress = report.ReportDefinition.ReportObjects["txtBranchAddress"] as TextObject;
                TextObject txtReportHeading = report.ReportDefinition.ReportObjects["txtReportHeading"] as TextObject;
                TextObject txtPrintUser = report.ReportDefinition.ReportObjects["txtPrintUser"] as TextObject;
                TextObject txtPrintDate = report.ReportDefinition.ReportObjects["txtPrintDate"] as TextObject;
                TextObject txtFromDate = report.ReportDefinition.ReportObjects["txtFromDate"] as TextObject;
                TextObject txtToDate = report.ReportDefinition.ReportObjects["txtToDate"] as TextObject;
                //TextObject txtAgentName = report.ReportDefinition.ReportObjects["txtAgentName"] as TextObject;
                // TextObject txtOutletName = report.ReportDefinition.ReportObjects["txtOutletName"] as TextObject;

                if (rptHeaders != null)
                {
                    if (rptHeaders.branchDto != null)
                    {
                        txtBankName.Text = rptHeaders.branchDto.bankName;
                        txtBranchName.Text = rptHeaders.branchDto.branchName;
                        txtBranchAddress.Text = rptHeaders.branchDto.branchAddress;
                    }
                    txtReportHeading.Text = rptHeaders.reportHeading;
                    txtPrintUser.Text = rptHeaders.printUser;
                    txtPrintDate.Text = rptHeaders.printDate.ToString("dd-MM-yyyy").Replace("-", "/");
                    //txtAgentName.Text =((AgentInformation)cmbAgentName.SelectedItem).businessName;
                    //string txt = ((AgentInformation)cmbAgentName.SelectedItem).businessName;
                    //txtAgentName.Text = DoInitCap.ConvertTo_ProperCase(txt);
                    //txtOutletName.Text = ((SubAgentInformation)cmbUserType.SelectedItem).name;
                    txtFromDate.Text = dtpFromdate.Value.ToString("dd-MM-yyyy").Replace("-", "/");
                    txtToDate.Text = dtpToDate.Value.ToString("dd-MM-yyyy").Replace("-", "/");
                    
                }
                report.SetDataSource(userIdWiseApprovedAcc);


                frm.crvReportViewer.ReportSource = report;
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadSetupData()
        {

            UtilityServices.fillBranchesWithAllandSelect(ref cmbBranchName);
            cmbBranchName.SelectedIndex = 0;


            Array das = Enum.GetValues(typeof(BankUserType));
            List<object> userTypeList = new List<object>();
            userTypeList.Insert(0, "(Select)");
            userTypeList.Insert(1, "(All)");
            for (int i = 0; i < das.Length; i++)
            {
                userTypeList.Add(das.GetValue(i));
            }
            cmbUserType.DataSource = userTypeList;
            cmbUserType.SelectedIndex = 0;


            Array dataSource = Enum.GetValues(typeof(UserStatus));
            List<object> userStatusList = new List<object>();
            userStatusList.Insert(0, "(Select)");
            userStatusList.Insert(1, "(All)");
            for (int i = 0; i < dataSource.Length; i++)
            {
                userStatusList.Add(dataSource.GetValue(i));
            }
            cmbUserStatus.DataSource = userStatusList;
            cmbUserStatus.SelectedIndex = 0;


            Array dts = Enum.GetValues(typeof(UserType));
            List<object> userTpList = new List<object>();
            userTpList.Insert(0, "(Select)");
            userTpList.Insert(1, "(All)");
            for (int i = 0; i < dts.Length; i++)
            {
                userTpList.Add(dts.GetValue(i));
            }
            cmbUserTp.DataSource = userTpList;
            cmbUserTp.SelectedIndex = 0;



        }





        private bool LoadbBranchUserInfoReport()
        {
            UserService userService = new UserService();

            UserIdWiseAccApproveSearchDto userIdWiseAccApproveSearchDto = FillSearchDto();
            if (userIdWiseAccApproveSearchDto == null)
            {
                return false;
            }

            List<UserIdWiseAccApproveResultDto> userIdWiseAccApproveResultDtos = userService.GetUserIdWiseAccApproveInfo(userIdWiseAccApproveSearchDto);


            try
            {
                //result = agentServices.getOutletUserInfoResultList(outletSearchDto);
                if (userIdWiseAccApproveResultDtos != null)
                {
                    userIdWiseApprovedAcc.Clear();
                    if (userIdWiseAccApproveResultDtos != null)
                    {
                        for (int i = 0; i < userIdWiseAccApproveResultDtos.Count; i++)
                        {
                            UserIdWiseAccApproveResultDto bankUser = userIdWiseAccApproveResultDtos[i];
                            UserIdWiseApprovedAccDS.UserIdWiseApprovedAccDTRow newRow =
                                userIdWiseApprovedAcc.UserIdWiseApprovedAccDT.NewUserIdWiseApprovedAccDTRow();

                            newRow.branchName = bankUser.branchName;
                            newRow.userID = bankUser.userId;
                            newRow.noOfMSD = bankUser.noOfMSD.ToString();
                            newRow.noOfCD = bankUser.noOfCD.ToString();
                            newRow.noOfMTDR = bankUser.noOfMTDR.ToString();
                            newRow.noOfITD = bankUser.noOfSSP.ToString();

                            newRow.noOfTotal =
                                (bankUser.noOfMSD + bankUser.noOfCD + bankUser.noOfMTDR + bankUser.noOfSSP).ToString();

                            userIdWiseApprovedAcc.UserIdWiseApprovedAccDT.AddUserIdWiseApprovedAccDTRow(newRow);

                        }
                    }//("N", new CultureInfo("BN-BD")
                    userIdWiseApprovedAcc.AcceptChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                CustomMessage.showError("Report data loading error.\n" + ex.Message);
            }
            return false;
        }





        private void frmOutletUserInfoReport_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }


    }


}
