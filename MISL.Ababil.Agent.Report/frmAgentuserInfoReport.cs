﻿using MetroFramework.Forms;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Report.Properties;
using MISL.Ababil.Agent.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Report.Reports;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using CrystalDecisions.CrystalReports.Engine;
using MISL.Ababil.Agent.Common.UI;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Report.DataSets;
using System.Globalization;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Module.Common.UI.MessageUI;

namespace MISL.Ababil.Agent.Report
{
    public partial class frmAgentuserInfoReport : MetroForm
    {
        private List<AgentInformation> objAgentInfoList = new List<AgentInformation>();
        private AgentServices agentServices = new AgentServices();
        private AgentInformation agentInformation = new AgentInformation();
        private List<OutletUserInfoReportSearchDto> result;
        agentUserInfoDS userInfo = new agentUserInfoDS();
        // List<OutletUserInfoReportResultDto> _outletUserInfoReportResultDto;
        List<SubAgentUserLimitDto> _outletUserInfoReportResultDto;
        private List<AgentUserReportDto> agentUserInfoDtos = new List<AgentUserReportDto>();
        public frmAgentuserInfoReport()
        {
            InitializeComponent();
            LoadSetupData();

        }

        private void btnShowReport_Click(object sender, EventArgs e)
        {
            try
            {
                //LoadOutletReportData();
                if (LoadbBranchUserInfoReport() == false)
                {
                    return;
                }

                crAgentUserInfoReport report = new crAgentUserInfoReport();
                frmReportViewer frm = new frmReportViewer();
                ReportHeaders rptHeaders = new ReportHeaders();
                rptHeaders = UtilityServices.getReportHeaders("Agent User Information Report");

                TextObject txtBankName = report.ReportDefinition.ReportObjects["txtBankName"] as TextObject;
                TextObject txtBranchName = report.ReportDefinition.ReportObjects["txtBranchName"] as TextObject;
                TextObject txtBranchAddress = report.ReportDefinition.ReportObjects["txtBranchAddress"] as TextObject;
                TextObject txtReportHeading = report.ReportDefinition.ReportObjects["txtReportHeading"] as TextObject;
                TextObject txtPrintUser = report.ReportDefinition.ReportObjects["txtPrintUser"] as TextObject;
                TextObject txtPrintDate = report.ReportDefinition.ReportObjects["txtPrintDate"] as TextObject;
                //TextObject txtFromDate = report.ReportDefinition.ReportObjects["txtFromDate"] as TextObject;
                //TextObject txtToDate = report.ReportDefinition.ReportObjects["txtToDate"] as TextObject;
                //TextObject txtAgentName = report.ReportDefinition.ReportObjects["txtAgentName"] as TextObject;
                //TextObject txtOutletName = report.ReportDefinition.ReportObjects["txtOutletName"] as TextObject;

                if (rptHeaders != null)
                {
                    if (rptHeaders.branchDto != null)
                    {
                        txtBankName.Text = rptHeaders.branchDto.bankName;
                        txtBranchName.Text = rptHeaders.branchDto.branchName;
                        txtBranchAddress.Text = rptHeaders.branchDto.branchAddress;
                    }
                    txtReportHeading.Text = rptHeaders.reportHeading;
                    txtPrintUser.Text = rptHeaders.printUser;
                    txtPrintDate.Text = rptHeaders.printDate.ToString("dd-MM-yyyy").Replace("-", "/");
                    //txtAgentName.Text =((AgentInformation)cmbAgentName.SelectedItem).businessName;
                    string txt = ((AgentInformation)cmbAgentName.SelectedItem).businessName;
                    //txtAgentName.Text = DoInitCap.ConvertTo_ProperCase(txt);
                    //txtOutletName.Text = ((SubAgentInformation)cmbOutletName.SelectedItem).name;

                }
                report.SetDataSource(userInfo);


                frm.crvReportViewer.ReportSource = report;
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadSetupData()
        {
            objAgentInfoList = agentServices.getAgentInfoBranchWise();
            AgentInformation agSelect = new AgentInformation();
            agSelect.businessName = Resources.SetupData__Select_;
            objAgentInfoList.Insert(0, agSelect);
            AgentInformation agAll = new AgentInformation();
            agAll.businessName = Resources.SetupData__All_;
            objAgentInfoList.Insert(1, agAll);

            BindingSource bsAgent = new BindingSource();
            bsAgent.DataSource = objAgentInfoList;

            UtilityServices.fillComboBox(cmbAgentName, bsAgent, "businessName", "id");
            cmbAgentName.SelectedIndex = 0;



            Array retval = Enum.GetValues(typeof(UserStatus));
            List<object> userStatusList = new List<object>();
            userStatusList.Insert(0, "(Select)");
            userStatusList.Insert(1, "(All)");
            for (int i = 0; i < retval.Length; i++)
            {
                userStatusList.Add(retval.GetValue(i));
            }            
            cmbUserStatus.DataSource = userStatusList;
            cmbUserStatus.SelectedIndex = 0;



            Array ds = Enum.GetValues(typeof(UserType));
            List<object> userTypeList = new List<object>();
            userTypeList.Insert(0, "(Select)");
            userTypeList.Insert(1, "(All)");
            for (int i = 0; i < ds.Length; i++)
            {
                userTypeList.Add(ds.GetValue(i));
            }
            cmbUserType.DataSource = userTypeList;
            cmbUserType.SelectedIndex = 0;



        }

        



        private bool LoadbBranchUserInfoReport()
        {
            UserService userService = new UserService();

            UserSearchDtoForAgent userSearchDtoForAgent = Search();
            if (userSearchDtoForAgent == null)
            {
                return false;
            }
            agentUserInfoDtos = userService.getAgentUserInfoReport(userSearchDtoForAgent);


            try
            {
                //result = agentServices.getOutletUserInfoResultList(outletSearchDto);
                if (agentUserInfoDtos != null)
                {
                    userInfo.Clear();
                    if (agentUserInfoDtos != null)
                    {
                        for (int i = 0; i < agentUserInfoDtos.Count; i++)
                        {
                            AgentUserReportDto bankUser = agentUserInfoDtos[i];
                            agentUserInfoDS.agentUserInfoReportDtRow newRow =
                                userInfo.agentUserInfoReportDt.NewagentUserInfoReportDtRow();

                            newRow.userName = bankUser.userName;
                            newRow.userId = bankUser.userId.ToString();
                            newRow.contactNo = bankUser.contactNo;
                            newRow.status = bankUser.status.ToString();
                            newRow.userType = bankUser.usertype.ToString();
                            newRow.agentName = bankUser.agentName;

                            userInfo.agentUserInfoReportDt.AddagentUserInfoReportDtRow(newRow);
                        }
                    }//("N", new CultureInfo("BN-BD")
                    userInfo.AcceptChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                CustomMessage.showError("Report data loading error.\n" + ex.Message);
            }
            return false;
        }

        private UserSearchDtoForAgent Search()
        {
            UserSearchDtoForAgent userSearchDtoForAgent = new UserSearchDtoForAgent();
            if (cmbAgentName.SelectedIndex <1)
            {
                MsgBox.showWarning("Please select an agent");
                return null;
            }
            else
            {
                if (cmbAgentName.SelectedIndex == 1)
                {
                    userSearchDtoForAgent.agentId = null;
                }
                else
                {
                    userSearchDtoForAgent.agentId = (long)cmbAgentName.SelectedValue;
                }
            }
            if (cmbUserStatus.SelectedIndex < 1)
            {
                MsgBox.showWarning("Please select user status");
                return null;
            }
            else
            {
                if (cmbUserStatus.SelectedIndex == 1)
                {
                    userSearchDtoForAgent.userStatus = null;
                }
                else
                {
                    userSearchDtoForAgent.userStatus = (UserStatus?)cmbUserStatus.SelectedValue;
                }
            }
            if (cmbUserType.SelectedIndex < 1)
            {
                MsgBox.showWarning("Please select user type");
                return null;
            }
            else
            {
                if (cmbUserType.SelectedIndex == 1)
                {
                    userSearchDtoForAgent.userType = null;
                }
                else
                {
                    userSearchDtoForAgent.userType = (UserType?)cmbUserType.SelectedValue;
                }
            }

            return userSearchDtoForAgent;

        }


        
        private void frmOutletUserInfoReport_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void frmAgentuserInfoReport_Load(object sender, EventArgs e)
        {

        }

        private void lblAgent_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }


}
