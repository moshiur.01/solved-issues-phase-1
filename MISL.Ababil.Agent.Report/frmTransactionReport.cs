﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using MetroFramework.Forms;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Infrastructure.Models.models.transaction;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using MISL.Ababil.Agent.Module.Common.UI.MessageUI;
using MISL.Ababil.Agent.Report.DataSets;
using MISL.Ababil.Agent.Report.Properties;
using MISL.Ababil.Agent.Report.Reports;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;

namespace MISL.Ababil.Agent.Report
{
    public partial class frmTransactionReport : MetroForm
    {

        private GUI gui = new GUI();
        List<AgentInformation> objAgentInfoList = new List<AgentInformation>();
        AgentServices agentServices = new AgentServices();
        AgentInformation agentInformation = new AgentInformation();
        TransactionService transactionService = new TransactionService();
        ServiceResult result;

        public List<TransactionReport> _txnReportList = new List<TransactionReport>();

        public frmTransactionReport()
        {
            InitializeComponent();

            dtpDateFrom.Value = SessionInfo.currentDate;
            dtpDateTo.Value = SessionInfo.currentDate;
            LoadSetupData();
            controlActivity();
            ConfigUIEnhancement();
            cmbTransactionType.SelectedIndex =0 ;
        }
        private void LoadSetupData()
        {
            try
            {
                //List<AgentServicesTypeList> svcTypeList = Enum.GetValues(typeof(AgentServicesType)).Cast<AgentServicesType>()
                //                                                                                .Select(a => new AgentServicesTypeList
                //                                                                                {
                //                                                                                    AgentServiceTypeName = a.ToString(),
                //                                                                                }).ToList();
                ////



                //AgentServicesTypeList[] retVal = svcTypeList.Where(a => 
                //    a.AgentServiceTypeName == AgentServicesType.BalanceEnquiry.ToString()
                //    ||
                //    a.AgentServiceTypeName == AgentServicesType.MiniStatement.ToString()
                //    ).ToArray();

                //for (int i = 0; i < retVal.Length; i++)
                //{
                //    svcTypeList.Remove(retVal[i]);    
                //}
                
                //;
                               
                //

                //AgentServicesTypeList allSvcType = new AgentServicesTypeList();
                //BindingSource bsTxn = new BindingSource();
                //allSvcType.AgentServiceTypeName = "All";
                //svcTypeList.Insert(0, allSvcType);
                //bsTxn.DataSource = svcTypeList;
                //UtilityServices.fillComboBox(cmbTransactionType, bsTxn, "AgentServiceTypeName", "AgentServiceTypeName");

                objAgentInfoList = agentServices.getAgentInfoBranchWise();
                BindingSource bsAgent = new BindingSource();
                bsAgent.DataSource = objAgentInfoList;

                AgentInformation agSelect = new AgentInformation();
                agSelect.businessName = Resources.SetupData__Select_;
                objAgentInfoList.Insert(0, agSelect);
                AgentInformation agAll = new AgentInformation();
                agAll.businessName = Resources.SetupData__All_;
                objAgentInfoList.Insert(1, agAll);

                UtilityServices.fillComboBox(cmbAgentName, bsAgent, "businessName", "id");
                cmbAgentName.Text = "Select";
                cmbAgentName.SelectedIndex = 0;
            }
            catch (Exception ex)
            { MsgBox.ShowError(ex.Message); }
        }
        private void controlActivity()
        {
            try
            {
                //if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_CENTRALLY.ToString())) //branch user
                if (SessionInfo.userBasicInformation.userCategory == UserCategory.BranchUser) //branch user
                {
                    cmbAgentName.Enabled = true;
                    cmbSubAgnetName.Enabled = true;
                }
                //else if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_AGENTWISE.ToString())) //agent user
                else if (SessionInfo.userBasicInformation.userCategory == UserCategory.AgentUser) //agent user
                {
                    cmbAgentName.SelectedValue = UtilityServices.getCurrentAgent().id;
                    cmbAgentName.Enabled = false;
                    cmbSubAgnetName.Enabled = true;
                    agentInformation = agentServices.getAgentInfoById(UtilityServices.getCurrentAgent().id.ToString());
                    setSubagent();
                }
                else
                {
                    SubAgentInformation currentSubagentInfo = UtilityServices.getCurrentSubAgent();
                    cmbAgentName.SelectedValue = currentSubagentInfo.agent.id;
                    agentInformation = agentServices.getAgentInfoById(currentSubagentInfo.agent.id.ToString());
                    setSubagent();
                    cmbAgentName.Enabled = false;
                    cmbSubAgnetName.SelectedValue = currentSubagentInfo.id;
                    cmbSubAgnetName.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }
        public void ConfigUIEnhancement()
        {
            gui = new GUI(this);
            gui.Config(ref cmbAgentName, ValidCheck.VALIDATIONTYPES.COMBOBOX_EMPTY, null);
            gui.Config(ref cmbSubAgnetName);
            gui.Config(ref dtpDateFrom);
            gui.Config(ref dtpDateTo);
        }
        private void setSubagent()
        {
            if (agentInformation != null)
            {
                BindingSource bs = new BindingSource();
                bs.DataSource = agentInformation.subAgents;

                try
                {
                    SubAgentInformation saiSelect = new SubAgentInformation();
                    saiSelect.name = "(Select)";

                    SubAgentInformation saiAll = new SubAgentInformation();
                    saiAll.name = "(All)";

                    agentInformation.subAgents.Insert(0, saiSelect);
                    agentInformation.subAgents.Insert(1, saiAll);
                }
                catch (Exception exp)
                { }
                UtilityServices.fillComboBox(cmbSubAgnetName, bs, "name", "id");
                if (cmbSubAgnetName.Items.Count > 0)
                { cmbSubAgnetName.SelectedIndex = 0; }
            }
        }
        private void cmbAgentName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!cmbAgentName.Focused) return;

            try
            {
                if (cmbAgentName.SelectedIndex > 1)
                { agentInformation = agentServices.getAgentInfoById(cmbAgentName.SelectedValue.ToString()); }

                if (cmbAgentName.SelectedIndex < 2)
                {
                    cmbSubAgnetName.DataSource = null;
                    cmbSubAgnetName.Items.Clear();
                    return;
                }
            }
            catch (Exception ex)
            { MsgBox.ShowError(ex.Message); }
            setSubagent();
        }

        private void LoadTransactionRegisterData()
        {
            TransactionReport txnReportRow;
            _txnReportList = new List<TransactionReport>();

            TransactionReportSearchDto dailyTrnRecordSearchDto = FillTxnReportSearchDto();
            try
            {
                result = transactionService.getTransactionReport(dailyTrnRecordSearchDto);
                if (result.Success)
                {
                    List<TransactionReportResultDto> txnReportResult = result.ReturnedObject as List<TransactionReportResultDto>;
                    if (txnReportResult != null)
                    {
                        foreach (TransactionReportResultDto txnRecord in txnReportResult)
                        {
                            txnReportRow = new TransactionReport();
                            txnReportRow.agentId = txnRecord.agentId;
                            txnReportRow.agentName = txnRecord.agentName;
                            txnReportRow.subAgentId = txnRecord.subAgentId;
                            txnReportRow.subAgentName = txnRecord.subAgentName;
                            txnReportRow.txnId = txnRecord.txnId;
                            //txnReportRow.txnDate = UtilityServices.getDateFromLong(txnRecord.txnDate);
                            txnReportRow.txnDate = txnRecord.txnDate;
                            txnReportRow.voucherNo = txnRecord.voucherNo;
                            txnReportRow.transactionAmount = txnRecord.transactionAmount;
                            txnReportRow.comissionAmount = txnRecord.comissionAmount;
                            txnReportRow.agentService = txnRecord.agentService;
                            txnReportRow.debitAccount = txnRecord.debitAccount;
                            txnReportRow.creditAccount = txnRecord.creditAccount;

                            _txnReportList.Add(txnReportRow);
                        }
                    }
                }
            }
            catch (Exception exp)
            { MsgBox.ShowError("Report data loading error.\n" + exp.Message); }
        }
        private TransactionReportSearchDto FillTxnReportSearchDto()
        {
            TransactionReportSearchDto TxnReportSearchDto = new TransactionReportSearchDto();
            try
            {
                if (this.cmbAgentName.SelectedIndex > -1)
                {
                    if (cmbAgentName.SelectedIndex != 0 && cmbAgentName.SelectedIndex != 1)
                    {
                        TxnReportSearchDto.agentId = (long)cmbAgentName.SelectedValue;
                    }
                    else if (cmbAgentName.SelectedIndex == 1 || cmbAgentName.SelectedIndex == 0)
                    {
                        TxnReportSearchDto.agentId = 0;
                    }
                }
                if (this.cmbSubAgnetName.SelectedIndex > -1)
                {
                    if (cmbSubAgnetName.SelectedIndex != 0 && cmbSubAgnetName.SelectedIndex != 1)
                    {
                        TxnReportSearchDto.subAgentId = (long)cmbSubAgnetName.SelectedValue;
                    }
                    else if (cmbSubAgnetName.SelectedIndex == 1 || cmbSubAgnetName.SelectedIndex == 0)
                    {
                        TxnReportSearchDto.subAgentId = 0;
                    }
                }
                else TxnReportSearchDto.subAgentId = 0;

                if (cmbTransactionType.SelectedValue == "(All)")
                {
                    //TxnReportSearchDto.agentService = null;
                    TxnReportSearchDto.eventId = null;
                }
                else
                {
                    //TxnReportSearchDto.agentService = (AgentServicesType)Enum.Parse(typeof(AgentServicesType), cmbTransactionType.SelectedValue.ToString());
                    switch (cmbTransactionType.SelectedItem.ToString())
                    {
                        case "(All)":
                            TxnReportSearchDto.eventId = null;
                            break;
                        case "Cash Deposit":
                            TxnReportSearchDto.eventId = 7;
                            break;
                        case "Cash Withdraw":
                            TxnReportSearchDto.eventId = 8;
                            break;
                        case "FundTransfer":
                            TxnReportSearchDto.eventId = 9;
                            break;
                        case "Remittance":
                            TxnReportSearchDto.eventId = 15;
                            break;
                        case "Bill Payment":
                            TxnReportSearchDto.eventId = 16;
                            break;
                        case "Itd":
                            TxnReportSearchDto.eventId = 19;
                            break;
                    }
                }
                TxnReportSearchDto.fromDate = DateTime.ParseExact(dtpDateFrom.Date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                TxnReportSearchDto.toDate = DateTime.ParseExact(dtpDateTo.Date, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                return TxnReportSearchDto;
            }
            catch (Exception exp)
            { MsgBox.ShowError("Error filling search object.\n" + exp.Message); return null; }
        }
        private void btnShowReport_Click(object sender, EventArgs e)
        {
            try
            {
                #region Validation
                DateTime fromDate, toDate;
                try
                {                
                    fromDate = DateTime.ParseExact(dtpDateFrom.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                catch
                {
                    MsgBox.showWarning("Please enter the Date in correct format!!");
                    return;
                }
                try
                {
                    toDate = DateTime.ParseExact(dtpDateTo.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                catch
                {
                    MsgBox.showWarning("Please enter the Date in correct format!!");
                    return;
                }

                if (SessionInfo.currentDate < toDate)
                {
                    MsgBox.showWarning("Future date is not allowed!");
                    return;
                }
                if (fromDate > toDate)
                {
                    MsgBox.showWarning("From date can not be greater than to date!!");
                    return;
                }

                if (cmbAgentName.SelectedIndex < 1)
                {
                    MsgBox.showWarning("Please select an agent!!");
                    return;
                }
                #endregion

                crTransactionReport report = new crTransactionReport();
                frmReportViewer viewer = new frmReportViewer();

                ReportHeaders rptHeaders = new ReportHeaders();
                rptHeaders = UtilityServices.getReportHeaders("Transaction Register");

                TextObject txtBankName = report.ReportDefinition.ReportObjects["txtBankName"] as TextObject;
                TextObject txtBranchName = report.ReportDefinition.ReportObjects["txtBranchName"] as TextObject;
                TextObject txtBranchAddress = report.ReportDefinition.ReportObjects["txtBranchAddress"] as TextObject;
                TextObject txtReportHeading = report.ReportDefinition.ReportObjects["txtReportHeading"] as TextObject;
                TextObject txtPrintUser = report.ReportDefinition.ReportObjects["txtPrintUser"] as TextObject;
                TextObject txtPrintDate = report.ReportDefinition.ReportObjects["txtPrintDate"] as TextObject;
                TextObject txtFromDate = report.ReportDefinition.ReportObjects["txtFromDate"] as TextObject;
                TextObject txtToDate = report.ReportDefinition.ReportObjects["txtToDate"] as TextObject;
                if (rptHeaders != null)
                {
                    if (rptHeaders.branchDto != null)
                    {
                        txtBankName.Text = rptHeaders.branchDto.bankName;
                        txtBranchName.Text = rptHeaders.branchDto.branchName;
                        txtBranchAddress.Text = rptHeaders.branchDto.branchAddress;
                    }
                    txtReportHeading.Text = rptHeaders.reportHeading;
                    txtPrintUser.Text = rptHeaders.printUser;
                    txtPrintDate.Text = rptHeaders.printDate.ToString("dd/MM/yyyy").Replace("-", "/");
                }

                txtFromDate.Text = dtpDateFrom.Date;
                txtToDate.Text = dtpDateTo.Date;

                LoadTransactionRegisterData();

                if (!result.Success)
                {
                    MsgBox.ShowError("Report data loading error.\n" + result.Message);
                    return;
                }

                report.SetDataSource(_txnReportList);
                viewer.crvReportViewer.ReportSource = report;
                viewer.Show(this.Parent);
            }
            catch (Exception exp)
            { MsgBox.ShowError(exp.Message); }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmTransactionReport_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

    }
    public class AgentServicesTypeList
    {
        public string AgentServiceTypeName { get; set; }
    }

    public class TransactionReport
    {
        public long agentId = 0;
        public string agentName = "";
        public long subAgentId = 0;
        public string subAgentName = "";
        public string userName = "";

        public long txnId = 0;
        //public DateTime txnDate = new DateTime();
        public string txnDate = "";
        public string voucherNo = "";
        public decimal transactionAmount = 0;
        public decimal comissionAmount = 0;
        public string agentService = "";
        public string debitAccount = "";
        public string debitAccountTitle = "";
        public string creditAccount = "";
        public string creditAccountTitle = "";
        public string amountInWords = "";
    }

}
