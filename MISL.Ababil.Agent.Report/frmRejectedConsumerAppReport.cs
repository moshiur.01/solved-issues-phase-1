﻿using CrystalDecisions.CrystalReports.Engine;
using MetroFramework.Forms;
using MISL.Ababil.Agent.Common.UI;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using MISL.Ababil.Agent.Report.Reports;
using MISL.Ababil.Agent.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.Module.Common.UI.MessageUI;

namespace MISL.Ababil.Agent.Report
{
    public partial class frmRejectedConsumerAppReport : MetroForm
    {
        private GUI gui = new GUI();
        List<AgentInformation> objAgentInfoList = new List<AgentInformation>();
        AgentInformation agentInformation = new AgentInformation();
        AgentServices objAgentServices = new AgentServices();
        List<AgentIncomeStatementDto> _agentIncomeStatList = new List<AgentIncomeStatementDto>();
        private List<RejectedConsumerAppDto> _rejectedConsumerApp = new List<RejectedConsumerAppDto>();

        List<SubAgentInformation> subAgentList = new List<SubAgentInformation>();       // WALI :: 14-Jan-2016

        public frmRejectedConsumerAppReport()
        {
            InitializeComponent();
            controlActivity();
            ConfigUIEnhancement();

            cmbStatus.Enabled = false;
            try
            {
                setAgentList();

            }
            catch (Exception)
            {

                //throw;
            }

        }
        private void btnViewReport_Click(object sender, EventArgs e)
        {
            DateTime fromDate, toDate;
            try
            {
                fromDate = DateTime.ParseExact(dtpFromDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
            }
            catch
            {
                CustomMessage.showWarning("Please enter the Date in correct format!!");
                return;
            }
            try
            {
                toDate = DateTime.ParseExact(dtpToDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
            }
            catch
            {
                CustomMessage.showWarning("Please enter the Date in correct format!!");
                return;
            }

            if (SessionInfo.currentDate < toDate)
            {
                CustomMessage.showWarning("Future date is not allowed!");
                return;
            }
            if (fromDate > toDate)
            {
                CustomMessage.showWarning("From date can not be greater than to date!!");
                return;
            }

            if (cmbAgentName.SelectedIndex < 0)
            {
                CustomMessage.showWarning("Please select an Agent!!");
                return;
            }

            try
            {
                crRejectedConsumerReport report = new crRejectedConsumerReport();
                frmReportViewer viewer = new frmReportViewer();

                ReportHeaders rptHeaders = new ReportHeaders();
                rptHeaders = UtilityServices.getReportHeaders("");

                TextObject txtBankName = report.ReportDefinition.ReportObjects["txtBankName"] as TextObject;
                TextObject txtBranchName = report.ReportDefinition.ReportObjects["txtBranchName"] as TextObject;
                TextObject txtBranchAddress = report.ReportDefinition.ReportObjects["txtBranchAddress"] as TextObject;
                TextObject txtReportHeading = report.ReportDefinition.ReportObjects["txtReportHeading"] as TextObject;
                TextObject txtPrintUser = report.ReportDefinition.ReportObjects["txtPrintUser"] as TextObject;
                TextObject txtPrintDate = report.ReportDefinition.ReportObjects["txtPrintDate"] as TextObject;
                TextObject txtFromDate = report.ReportDefinition.ReportObjects["txtFromDate"] as TextObject;
                TextObject txtToDate = report.ReportDefinition.ReportObjects["txtToDate"] as TextObject;
                if ((ApplicationStatus)cmbStatus.SelectedValue == ApplicationStatus.draft)
                {
                    txtReportHeading.Text = "Draft Application Information";
                }
                if ((ApplicationStatus)cmbStatus.SelectedValue == ApplicationStatus.submitted)
                {
                    txtReportHeading.Text = "Submitted Application Information";
                }
                if ((ApplicationStatus)cmbStatus.SelectedValue == ApplicationStatus.approved)
                {
                    txtReportHeading.Text = "Approve Application Information";
                }

                if ((ApplicationStatus)cmbStatus.SelectedValue == ApplicationStatus.canceled)
                {
                    txtReportHeading.Text = "Rejected Application Information";
                }
                if ((ApplicationStatus)cmbStatus.SelectedValue == ApplicationStatus.correction)
                {
                    txtReportHeading.Text = "Correction Application Information";
                }
                if ((ApplicationStatus)cmbStatus.SelectedValue == ApplicationStatus.verified)
                {
                    txtReportHeading.Text = "Verify Application Information";
                }
                if (rptHeaders != null)
                {
                    if (rptHeaders.branchDto != null)
                    {
                        txtBankName.Text = rptHeaders.branchDto.bankName;
                        txtBranchName.Text = rptHeaders.branchDto.branchName;
                        txtBranchAddress.Text = rptHeaders.branchDto.branchAddress;
                    }
                    //txtReportHeading.Text = rptHeaders.reportHeading;
                    txtPrintUser.Text = rptHeaders.printUser;
                    txtPrintDate.Text = rptHeaders.printDate.ToString("dd-MM-yyyy").Replace("-", "/");
                }

                txtFromDate.Text = dtpFromDate.Date;
                txtToDate.Text = dtpToDate.Date;

                LoadRejectedConsumerAppData();

                report.SetDataSource(_rejectedConsumerApp);

                viewer.crvReportViewer.ReportSource = report;
                viewer.ShowDialog(this.Parent);
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        private BasicSearchDto FillRejectedConsumerSearchDto()
        {
            BasicSearchDto basicAccSearchDto = new BasicSearchDto();

            if (this.cmbAgentName.SelectedIndex > -1)
            {
                if (cmbAgentName.SelectedIndex != 0)
                {
                    basicAccSearchDto.agentId = (long)cmbAgentName.SelectedValue;
                }
                if (cmbAgentName.SelectedIndex == 0)
                {
                    basicAccSearchDto.agentId = 0; // all Agent
                }
            }
            else basicAccSearchDto.agentId = 0; // all Agent

            if (this.cmbSubAgnetName.SelectedIndex > -1)
            {
                if (cmbSubAgnetName.SelectedIndex != 0 && cmbSubAgnetName.SelectedIndex != 1)
                {
                    basicAccSearchDto.outletId = (long)cmbSubAgnetName.SelectedValue;
                }
                if (cmbSubAgnetName.SelectedIndex == 0 || cmbSubAgnetName.SelectedIndex == 1)
                {
                    basicAccSearchDto.outletId = 0; // all SubAgent
                }
            }
            else
            {
                basicAccSearchDto.outletId = 0;
            }
            if (this.cmbStatus.SelectedIndex == 0)
            {
                basicAccSearchDto.status = 0;
            }
            else basicAccSearchDto.status = (ApplicationStatus)cmbStatus.SelectedValue;
            //(ApplicationStatus)Enum.Parse(typeof(ApplicationStatus), cmbStatus.SelectedValue.ToString());

            basicAccSearchDto.fromDate = UtilityServices.GetLongDate
            (
                DateTime.ParseExact(dtpFromDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture)
            );

            basicAccSearchDto.toDate = UtilityServices.GetLongDate
            (
                DateTime.ParseExact(dtpToDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture)
            );


            return basicAccSearchDto;
        }
        private void frmRejectedConsumerAppReport_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbAgentName_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!cb.Focused)
            {
                return;
            }


            try
            {
                if (cmbAgentName.SelectedIndex > -1)
                {
                    if (cmbAgentName.SelectedIndex > 0)
                    {
                        subAgentList = objAgentServices.GetSubagentsByAgentId((long)cmbAgentName.SelectedValue);
                    }
                    else subAgentList = null;
                }
                if (cmbAgentName.SelectedIndex == 0)
                {
                    cmbSubAgnetName.DataSource = null;
                    cmbSubAgnetName.Items.Clear();
                    return;
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
            setSubagent();
        }
        private void setAgentList()
        {
            objAgentInfoList = objAgentServices.getAgentInfoBranchWise();
            BindingSource bs = new BindingSource();
            bs.DataSource = objAgentInfoList;

            AgentInformation agSelect = new AgentInformation();
            agSelect.businessName = "(All)";
            objAgentInfoList.Insert(0, agSelect);
            UtilityServices.fillComboBox(cmbAgentName, bs, "businessName", "id");
            cmbAgentName.SelectedIndex = -1;
            #region
            //List<ConsumerApplicationStatusList> svcTypeList = Enum.GetValues(typeof(ApplicationStatus)).Cast<ApplicationStatus>()
            //                                                                    .Select(a => new ConsumerApplicationStatusList
            //                                                                    {
            //                                                                        ConsumerApplicationStatusName = a.ToString(),
            //                                                                    }).ToList();


            //ConsumerApplicationStatusList allSvcType = new ConsumerApplicationStatusList();
            //BindingSource bsStatus = new BindingSource();
            //allSvcType.ConsumerApplicationStatusName = "All";
            //svcTypeList.Insert(0, allSvcType);
            //bsStatus.DataSource = svcTypeList;
            //UtilityServices.fillComboBox(cmbStatus, bsStatus, "ConsumerApplicationStatusName", "ConsumerApplicationStatusName");
            #endregion
            cmbStatus.DataSource = Enum.GetValues(typeof(ApplicationStatus));
            cmbStatus.Text = ApplicationStatus.canceled.ToString();
        }
        private void setSubagent()
        {
            #region Commented :: WALI :: 14-Jan-2016
            //if (agentInformation != null)
            //{
            //    BindingSource bs = new BindingSource();
            //    bs.DataSource = agentInformation.subAgents;


            //    {
            //        try
            //        {
            //            SubAgentInformation saiSelect = new SubAgentInformation();
            //            saiSelect.name = "(Select)";

            //            SubAgentInformation saiAll = new SubAgentInformation();
            //            saiAll.name = "(All)";

            //            agentInformation.subAgents.Insert(0, saiSelect);
            //            agentInformation.subAgents.Insert(1, saiAll);
            //        }
            //        catch //suppressed
            //        {

            //        }
            //    }
            //    UtilityServices.fillComboBox(cmbSubAgnetName, bs, "name", "id");
            //    if (cmbSubAgnetName.Items.Count > 0)
            //    {
            //        cmbSubAgnetName.SelectedIndex = 0;
            //    }
            //}
            #endregion

            if (subAgentList != null)
            {
                BindingSource bs = new BindingSource();
                bs.DataSource = subAgentList;

                try
                {
                    SubAgentInformation saiSelect = new SubAgentInformation();
                    saiSelect.name = "(Select)";

                    SubAgentInformation saiAll = new SubAgentInformation();
                    saiAll.name = "(All)";

                    subAgentList.Insert(0, saiSelect);
                    subAgentList.Insert(1, saiAll);
                }
                catch //suppressed
                { }

                UtilityServices.fillComboBox(cmbSubAgnetName, bs, "name", "id");
                if (cmbSubAgnetName.Items.Count > 0)
                {
                    cmbSubAgnetName.SelectedIndex = 0;
                }
            }
        }
        public void ConfigUIEnhancement()
        {
            gui = new GUI(this);
            gui.Config(ref cmbAgentName, ValidCheck.VALIDATIONTYPES.COMBOBOX_EMPTY, null);
            gui.Config(ref cmbSubAgnetName);
            gui.Config(ref dtpFromDate);
            gui.Config(ref dtpToDate);
        }
        private void controlActivity()
        {
            try
            {
                if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_CENTRALLY.ToString())) //branch user
                {
                    cmbAgentName.Enabled = true;
                    cmbSubAgnetName.Enabled = true;
                }
                else if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_AGENTWISE.ToString())) //agent user
                {
                    cmbAgentName.SelectedValue = UtilityServices.getCurrentAgent().id;
                    cmbAgentName.Enabled = false;
                    cmbSubAgnetName.Enabled = true;
                    //agentInformation = objAgentServices.getAgentInfoById(UtilityServices.getCurrentAgent().id.ToString());
                    subAgentList = objAgentServices.GetSubagentsByAgentId((long)cmbAgentName.SelectedValue);         // WALI :: 14-Jan-2016
                    setSubagent();
                }
                else
                {
                    SubAgentInformation currentSubagentInfo = UtilityServices.getCurrentSubAgent();
                    cmbAgentName.SelectedValue = currentSubagentInfo.agent.id;
                    //agentInformation = objAgentServices.getAgentInfoById(currentSubagentInfo.agent.id.ToString());
                    subAgentList = objAgentServices.GetSubagentsByAgentId((long)cmbAgentName.SelectedValue);         // WALI :: 14-Jan-2016
                    setSubagent();
                    cmbAgentName.Enabled = false;
                    cmbSubAgnetName.SelectedValue = currentSubagentInfo.id;
                    cmbSubAgnetName.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
        }
        private void LoadRejectedConsumerAppData()
        {
            RejectedConsumerAppDto rejectedAppDto;
            _rejectedConsumerApp.Clear();
            BasicSearchDto rejectedConsumerDto = FillRejectedConsumerSearchDto();
            ServiceResult result1;

            try
            {
                result1 = ReportService.GetRejectedConsumerReport(rejectedConsumerDto);

                if (!result1.Success)
                {
                    MessageBox.Show(result1.Message);
                    return;
                }

                List<CustomerApplicationInfoDto> rejectedConsumerResultDto = result1.ReturnedObject as List<CustomerApplicationInfoDto>;

                _rejectedConsumerApp = new List<RejectedConsumerAppDto>();

                if (rejectedConsumerResultDto != null)
                {
                    for (int i = 0; i < rejectedConsumerResultDto.Count; i++)
                    {
                        CustomerApplicationInfoDto rejectedRow = rejectedConsumerResultDto[i];
                        rejectedAppDto = new RejectedConsumerAppDto();
                        rejectedAppDto.agentId = rejectedRow.agentId;
                        rejectedAppDto.outletId = rejectedRow.outletId;
                        rejectedAppDto.outletCode = rejectedRow.outletCode;
                        rejectedAppDto.agentName = rejectedRow.agentName;
                        rejectedAppDto.outlateName = rejectedRow.outlateName;
                        rejectedAppDto.consumerName = rejectedRow.consumerName;
                        rejectedAppDto.mobileNo = rejectedRow.mobileNo;
                        //rejectedAppDto.nationalId = rejectedRow.nationalId;
                        rejectedAppDto.nationalId = rejectedRow.documentType + " - " + rejectedRow.documentNo;
                        rejectedAppDto.referenceNo = rejectedRow.referenceNo;
                        rejectedAppDto.openingAmount = (rejectedRow.openingAmount != null)
                            ? rejectedRow.openingAmount
                            : 0;
                        rejectedAppDto.applyUser = rejectedRow.applyUser;
                        rejectedAppDto.applyDate = UtilityServices.getBDFormattedDateFromLong(rejectedRow.applyDate);
                        if (rejectedRow.rejectedDate != null)
                        {
                            rejectedAppDto.rejectedDate = UtilityServices.getDateFromLong(rejectedRow.rejectedDate);
                        }
                        // rejectedAppDto.rejectedDate = rejectedRow.rejectedDate ?? new DateTime();

                        rejectedAppDto.rejectedUser = rejectedRow.rejectedUser;
                        rejectedAppDto.comments = rejectedRow.comments;

                        _rejectedConsumerApp.Add(rejectedAppDto);
                    }
                }

            }
            catch (Exception exp)
            { }
        }

    }
    public class ConsumerApplicationStatusList
    {
        public string ConsumerApplicationStatusName { get; set; }
    }
    public class RejectedConsumerAppDto
    {
        public long agentId { get; set; }
        public string agentName { get; set; }
        public long outletId { get; set; }
        public string outlateName { get; set; }
        public string outletCode { get; set; }
        public string consumerName { get; set; }
        public string nationalId { get; set; }
        public string mobileNo { get; set; }
        public string referenceNo { get; set; }
        public decimal openingAmount { get; set; }
        public string applyDate { get; set; }
        public string applyUser { get; set; }
        public string rejectedUser { get; set; }
        public DateTime rejectedDate { get; set; }
        public string comments { get; set; }   
    }
}
