﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MetroFramework.Forms;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using MISL.Ababil.Agent.Report.Properties;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.Report.Reports;
using CrystalDecisions.CrystalReports.Engine;
using MISL.Ababil.Agent.LocalStorageService;
using MISL.Ababil.Agent.Common.UI;
using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.Report
{
    public partial class frmOutletInfoReport : CustomForm
    {
        List<Division> _divisions;
        List<District> _districts;
        List<Thana> _cthanas;
        List<AgentInformation> objAgentInfoList = new List<AgentInformation>();
        AgentServices agentServices = new AgentServices();
        AgentInformation agentInformation = new AgentInformation();
        ServiceResult result;
        List<OutletInfoReport> _outletInfoReportList = new List<OutletInfoReport>();
        List<SubAgentInformation> subAgentList = new List<SubAgentInformation>();
        public frmOutletInfoReport()
        {
            InitializeComponent();

            LoadSetupData();
            controlActivity();
        }
        private void LoadSetupData()
        {
            try
            {




                _divisions = LocalCache.GetDivisions();

                BindingSource bsDivisionContact = new BindingSource();
                bsDivisionContact.DataSource = _divisions;
                _districts = LocalCache.GetDistricts();
                BindingSource bsDistrict = new BindingSource();
                bsDistrict.DataSource = _districts;


                UtilityServices.fillComboBox(cmbDivision, bsDivisionContact, "name", "id");
                cmbDivision.SelectedIndex = -1;





                #region Fill Outlet Status
                List<OutletStatusList> olStatusList = Enum.GetValues(typeof(OutLetStatus)).Cast<OutLetStatus>()
                                                                                                .Select(s => new OutletStatusList
                                                                                                {
                                                                                                    OutletStatus = s.ToString(),
                                                                                                }).ToList();
                OutletStatusList allStatus = new OutletStatusList();
                BindingSource bsStatus = new BindingSource();
                allStatus.OutletStatus = "All";
                olStatusList.Insert(0, allStatus);
                bsStatus.DataSource = olStatusList;
                UtilityServices.fillComboBox(cmbOutletStatus, bsStatus, "OutletStatus", "OutletStatus");
                #endregion
                #region Fill Agent List
                objAgentInfoList = agentServices.getAgentInfoBranchWise();
                BindingSource bsAgent = new BindingSource();
                bsAgent.DataSource = objAgentInfoList;

                AgentInformation agSelect = new AgentInformation();
                agSelect.businessName = Resources.SetupData__Select_;
                objAgentInfoList.Insert(0, agSelect);
                AgentInformation agAll = new AgentInformation();
                agAll.businessName = Resources.SetupData__All_;
                objAgentInfoList.Insert(1, agAll);

                UtilityServices.fillComboBox(cmbAgentName, bsAgent, "businessName", "id");
                cmbAgentName.Text = "Select";
                cmbAgentName.SelectedIndex = 0;
                #endregion
            }
            catch (Exception ex)
            { CustomMessage.showError(ex.Message); }
        }

        private bool LoadOutletReportData()
        {

            _outletInfoReportList = new List<OutletInfoReport>();

            OutletReportSearchDto outletSearchDto = FillOutletReportSearchDto();

            if (outletSearchDto == null) return false;
            try
            {
                result = agentServices.getOutletInformationReportList(outletSearchDto);
                if (result.Success)
                {
                    int i = 0;
                    string tmpagentname ="";
                    string tmpOutletName = "";
                    List<OutletReportDto> outletInfoResult = result.ReturnedObject as List<OutletReportDto>;
                    if (outletInfoResult != null)
                    {
                        foreach (OutletReportDto outlet in outletInfoResult)
                        {
                            OutletInfoReport outletInfoReportRow = new OutletInfoReport();
                            if (tmpagentname == outlet.agentName)
                            {
                                if (tmpOutletName.Trim().ToLower() != outlet.name.Trim().ToLower())
                                {
                                    ++i;
                                    outletInfoReportRow.slno = i.ToString();
                                }
                                else
                                {
                                    outletInfoReportRow.slno = "";
                                }
                            }
                            else
                            {
                                i = 1;
                                outletInfoReportRow.slno = i.ToString();
                            }
                            tmpagentname = outlet.agentName;
                            tmpOutletName = outlet.name;

                            outletInfoReportRow.id = outlet.id;


                            outletInfoReportRow.name = outlet.name;
                            outletInfoReportRow.businessAddress = outlet.businessAddress;
                            outletInfoReportRow.mobleNumber = outlet.mobleNumber;
                            outletInfoReportRow.branchName = outlet.branchName;
                            outletInfoReportRow.fieldOfficerName = outlet.fieldOfficerName;
                            outletInfoReportRow.fieldOfficerMobNo = outlet.fieldOfficerMobNo;
                            outletInfoReportRow.outletZone = outlet.outletZone;
                            if (outlet.creationDate != null)
                            {
                                outletInfoReportRow.creationDate = Convert.ToDateTime(outlet.creationDate).ToString("dd-MM-yyyy");
                            }
                            outletInfoReportRow.status = outlet.status;
                            outletInfoReportRow.agentId = outlet.agentId;
                            outletInfoReportRow.agentCode = outlet.agentCode;
                            outletInfoReportRow.agentName = outlet.agentName;

                            _outletInfoReportList.Add(outletInfoReportRow);
                        }
                    }
                }
            }
            catch (Exception exp)
            { CustomMessage.showError("Report data loading error.\n" + exp.Message); }
            return true;
        }
        private OutletReportSearchDto FillOutletReportSearchDto()
        {
            OutletReportSearchDto outletSearchDto = new OutletReportSearchDto();
            try
            {
                if (this.cmbAgentName.SelectedIndex < 1)
                {
                    MessageBox.Show("Please select an Agent!", "Selection", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return null;
                }

                if (this.cmbAgentName.SelectedIndex > -1)
                {
                    if (cmbAgentName.SelectedIndex != 0 && cmbAgentName.SelectedIndex != 1)
                    {
                        outletSearchDto.agentId = (long)cmbAgentName.SelectedValue;
                    }
                    else if (cmbAgentName.SelectedIndex == 1 || cmbAgentName.SelectedIndex == 0)
                    {
                        outletSearchDto.agentId = 0;
                    }
                }
                if (cmbAgentName.SelectedIndex>1 && this.cmbSubAgnetName.SelectedIndex < 1)
                {
                    MessageBox.Show("Please select an Outlet", "Selection", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return null;
                }

                if (this.cmbSubAgnetName.SelectedIndex > -1)
                {
                    if (cmbSubAgnetName.SelectedIndex != 0 && cmbSubAgnetName.SelectedIndex != 1)
                    {
                        outletSearchDto.outletId = (long)cmbSubAgnetName.SelectedValue;
                    }
                    else if (cmbSubAgnetName.SelectedIndex == 1 || cmbSubAgnetName.SelectedIndex == 0)
                    {
                        outletSearchDto.outletId = 0;
                    }
                }
                if (cmbAgentName.SelectedIndex == 1 && cmbSubAgnetName.SelectedIndex >= -1)
                {
                    outletSearchDto.agentId = 0;
                    outletSearchDto.outletId = 0;
                }
                if (this.cmbDivision.SelectedIndex > -1)
                {
                    if (cmbDivision.SelectedIndex > -1)
                    {
                        outletSearchDto.divisionId = long.Parse(cmbDivision.SelectedValue.ToString());
                    }
                }
                else outletSearchDto.divisionId = 0;

                if (this.cmbDistrict.SelectedIndex > -1)
                {

                    outletSearchDto.districtId = long.Parse(cmbDistrict.SelectedValue.ToString());
                }
                else outletSearchDto.districtId = 0;

                if (this.cmbUpazilla.SelectedIndex > -1)
                {

                    outletSearchDto.thanaId = long.Parse(cmbUpazilla.SelectedValue.ToString());
                }
                else outletSearchDto.thanaId = 0;

                if (cmbOutletStatus.SelectedValue == "All") outletSearchDto.status = null;
                else outletSearchDto.status = (OutLetStatus)Enum.Parse(typeof(OutLetStatus), cmbOutletStatus.SelectedValue.ToString());

                return outletSearchDto;
            }
            catch (Exception exp)
            { CustomMessage.showError("Error filling search object.\n" + exp.Message); return null; }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnShowReport_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (LoadOutletReportData() == false) return;
                crOutletInformationReport report = new crOutletInformationReport();
                frmReportViewer viewer = new frmReportViewer();

                ReportHeaders rptHeaders = new ReportHeaders();
                rptHeaders = UtilityServices.getReportHeaders("Outlet Information Report");

                TextObject txtBankName = report.ReportDefinition.ReportObjects["txtBankName"] as TextObject;
                TextObject txtBranchName = report.ReportDefinition.ReportObjects["txtBranchName"] as TextObject;
                TextObject txtBranchAddress = report.ReportDefinition.ReportObjects["txtBranchAddress"] as TextObject;
                TextObject txtReportHeading = report.ReportDefinition.ReportObjects["txtReportHeading"] as TextObject;
                TextObject txtPrintUser = report.ReportDefinition.ReportObjects["txtPrintUser"] as TextObject;
                TextObject txtPrintDate = report.ReportDefinition.ReportObjects["txtPrintDate"] as TextObject;

                if (rptHeaders != null)
                {
                    if (rptHeaders.branchDto != null)
                    {
                        txtBankName.Text = rptHeaders.branchDto.bankName;
                        txtBranchName.Text = rptHeaders.branchDto.branchName;
                        txtBranchAddress.Text = rptHeaders.branchDto.branchAddress;
                    }
                    txtReportHeading.Text = rptHeaders.reportHeading;
                    txtPrintUser.Text = rptHeaders.printUser;
                    txtPrintDate.Text = rptHeaders.printDate.ToString("dd/MM/yyyy").Replace("-", "/");
                }
                if (!result.Success)
                {
                    CustomMessage.showWarning("Report data loading error.\n" + result.Message);
                    return;
                }

                report.SetDataSource(_outletInfoReportList);
                viewer.crvReportViewer.ReportSource = report;
                viewer.ShowDialog(this.Parent);
            }
            catch (Exception exp)
            { CustomMessage.showError(exp.Message); }
        }

        private void cmbDivision_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!cb.Focused)
            {
                return;
            }

            List<District> DistrictTmpContact = LocalCache.GetDistrictsByDivisionID(((Division)cmbDivision.SelectedItem).id);
            BindingSource bsDistrictContact = new BindingSource();
            bsDistrictContact.DataSource = DistrictTmpContact;
            UtilityServices.fillComboBox(cmbDistrict, bsDistrictContact, "title", "id");
            cmbDistrict.SelectedIndex = -1;
            cmbUpazilla.SelectedIndex = -1;

        }

        private void cmbDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!cb.Focused)
            {
                return;
            }

            long tmpIdContact = ((District)cmbDistrict.SelectedItem).id;
            List<Thana> ThanaTmpContact = LocalCache.GetThanasByDistrictID(tmpIdContact);
            BindingSource bsThanaContact = new BindingSource();
            bsThanaContact.DataSource = ThanaTmpContact;
            UtilityServices.fillComboBox(cmbUpazilla, bsThanaContact, "title", "id");
            cmbUpazilla.SelectedIndex = -1;

        }

        private void cmbAgentName_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!cb.Focused)
            {
                return;
            }

            try
            {
                if (cmbAgentName.SelectedIndex > 1)
                {
                    //agentInformation = agentServices.getAgentInfoById(cmbAgentName.SelectedValue.ToString());
                    subAgentList = agentServices.GetSubagentsByAgentId(long.Parse(cmbAgentName.SelectedValue.ToString()));         // WALI :: 14-Jan-2016
                }
                if (cmbAgentName.SelectedIndex == 1)
                {
                    //agentInformation = agentServices.getAgentInfoById(null);
                }
                if (cmbAgentName.SelectedIndex < 2)
                {
                    cmbSubAgnetName.DataSource = null;
                    cmbSubAgnetName.Items.Clear();
                    return;
                }
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
            setSubagent();
        }
        private void setSubagent()
        {
            #region WALI :: Commented :: 14-Jan-2016
            //if (agentInformation != null)
            //{
            //    BindingSource bs = new BindingSource();
            //    bs.DataSource = agentInformation.subAgents;

            //    try
            //    {
            //        SubAgentInformation saiSelect = new SubAgentInformation();
            //        saiSelect.name = "(Select)";

            //        SubAgentInformation saiAll = new SubAgentInformation();
            //        saiAll.name = "(All)";

            //        agentInformation.subAgents.Insert(0, saiSelect);
            //        agentInformation.subAgents.Insert(1, saiAll);
            //    }
            //    catch (Exception exp)
            //    { }

            //    UtilityServices.fillComboBox(cmbSubAgnetName, bs, "name", "id");
            //    if (cmbSubAgnetName.Items.Count > 0)
            //    {
            //        cmbSubAgnetName.SelectedIndex = 0;
            //    }
            //}
            #endregion

            if (subAgentList != null)
            {
                BindingSource bs = new BindingSource();
                bs.DataSource = subAgentList;

                try
                {
                    SubAgentInformation saiSelect = new SubAgentInformation();
                    saiSelect.name = "(Select)";

                    SubAgentInformation saiAll = new SubAgentInformation();
                    saiAll.name = "(All)";

                    subAgentList.Insert(0, saiSelect);
                    subAgentList.Insert(1, saiAll);
                }
                catch (Exception exp)
                { }

                UtilityServices.fillComboBox(cmbSubAgnetName, bs, "name", "id");
                if (cmbSubAgnetName.Items.Count > 0)
                {
                    cmbSubAgnetName.SelectedIndex = 0;
                }
            }
        }

        private void frmOutletInfoReport_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void controlActivity()
        {
            try
            {
                if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_CENTRALLY.ToString())) //branch user
                {
                    cmbAgentName.Enabled = true;
                }
                else if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_AGENTWISE.ToString())) //agent user
                {
                    cmbAgentName.SelectedValue = UtilityServices.getCurrentAgent().id;
                    cmbAgentName.Enabled = false;
                    ;
                }
                else
                {
                    SubAgentInformation currentSubagentInfo = UtilityServices.getCurrentSubAgent();
                    cmbAgentName.SelectedValue = currentSubagentInfo.agent.id;
                    cmbAgentName.Enabled = false;

                }
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);

            }
        }
    }

    public class OutletStatusList
    {
        public string OutletStatus { get; set; }
    }
    public class OutletInfoReport
    {
        public long id = 0;
        public string subAgentCode = "";
        public string name = "";
        public string businessAddress = "";
        public string mobleNumber = "";
        public string creationDate = "";
        public string status = "";
        public long agentId = 0;
        public string agentCode = "";
        public string agentName = "";
        public string branchName = "";
        public string fieldOfficerName = "";
        public string fieldOfficerMobNo = "";
        public string outletZone = "";
        public string slno = "";

    }
}
