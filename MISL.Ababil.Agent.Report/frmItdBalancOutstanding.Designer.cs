﻿using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.Report
{
    partial class frmItdBalancOutstanding
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClose = new System.Windows.Forms.Button();
            this.btnViewReport = new System.Windows.Forms.Button();
            this.lblProductName = new System.Windows.Forms.Label();
            this.lblSubAgent = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblAgent = new System.Windows.Forms.Label();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.cmbProducts = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbAgentName = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbSubAgnetName = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnClose.Image = global::MISL.Ababil.Agent.Report.Properties.Resources.Delete_16__1_;
            this.btnClose.Location = new System.Drawing.Point(627, 155);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(95, 33);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Close";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnViewReport
            // 
            this.btnViewReport.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.btnViewReport.FlatAppearance.BorderSize = 0;
            this.btnViewReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewReport.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnViewReport.Image = global::MISL.Ababil.Agent.Report.Properties.Resources.repor;
            this.btnViewReport.Location = new System.Drawing.Point(479, 155);
            this.btnViewReport.Name = "btnViewReport";
            this.btnViewReport.Size = new System.Drawing.Size(134, 33);
            this.btnViewReport.TabIndex = 7;
            this.btnViewReport.Text = "View Report";
            this.btnViewReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnViewReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnViewReport.UseVisualStyleBackColor = false;
            this.btnViewReport.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // lblProductName
            // 
            this.lblProductName.AutoSize = true;
            this.lblProductName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lblProductName.Location = new System.Drawing.Point(55, 161);
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.Size = new System.Drawing.Size(69, 17);
            this.lblProductName.TabIndex = 3;
            this.lblProductName.Text = "Product  :";
            // 
            // lblSubAgent
            // 
            this.lblSubAgent.AutoSize = true;
            this.lblSubAgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lblSubAgent.Location = new System.Drawing.Point(420, 113);
            this.lblSubAgent.Name = "lblSubAgent";
            this.lblSubAgent.Size = new System.Drawing.Size(54, 17);
            this.lblSubAgent.TabIndex = 5;
            this.lblSubAgent.Text = "Outlet :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(257, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(211, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "ITD Balance Outstanding";
            // 
            // lblAgent
            // 
            this.lblAgent.AutoSize = true;
            this.lblAgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lblAgent.Location = new System.Drawing.Point(70, 116);
            this.lblAgent.Name = "lblAgent";
            this.lblAgent.Size = new System.Drawing.Size(53, 17);
            this.lblAgent.TabIndex = 1;
            this.lblAgent.Text = "Agent :";
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.customTitlebar1.Location = new System.Drawing.Point(-3, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(767, 26);
            this.customTitlebar1.TabIndex = 33;
            // 
            // cmbProducts
            // 
            this.cmbProducts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProducts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbProducts.FormattingEnabled = true;
            this.cmbProducts.InputScopeAllowEmpty = false;
            this.cmbProducts.IsValid = null;
            this.cmbProducts.Location = new System.Drawing.Point(126, 162);
            this.cmbProducts.Name = "cmbProducts";
            this.cmbProducts.PromptText = "(Select)";
            this.cmbProducts.ReadOnly = false;
            this.cmbProducts.ShowMandatoryMark = false;
            this.cmbProducts.Size = new System.Drawing.Size(238, 21);
            this.cmbProducts.TabIndex = 34;
            this.cmbProducts.ValidationErrorMessage = "Validation Error!";
            // 
            // cmbAgentName
            // 
            this.cmbAgentName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAgentName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbAgentName.FormattingEnabled = true;
            this.cmbAgentName.InputScopeAllowEmpty = false;
            this.cmbAgentName.IsValid = null;
            this.cmbAgentName.Location = new System.Drawing.Point(126, 117);
            this.cmbAgentName.Name = "cmbAgentName";
            this.cmbAgentName.PromptText = "(Select)";
            this.cmbAgentName.ReadOnly = false;
            this.cmbAgentName.ShowMandatoryMark = false;
            this.cmbAgentName.Size = new System.Drawing.Size(238, 21);
            this.cmbAgentName.TabIndex = 37;
            this.cmbAgentName.ValidationErrorMessage = "Validation Error!";
            this.cmbAgentName.SelectedIndexChanged += new System.EventHandler(this.cmbAgentName_SelectedIndexChanged);
            // 
            // cmbSubAgnetName
            // 
            this.cmbSubAgnetName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSubAgnetName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbSubAgnetName.FormattingEnabled = true;
            this.cmbSubAgnetName.InputScopeAllowEmpty = false;
            this.cmbSubAgnetName.IsValid = null;
            this.cmbSubAgnetName.Location = new System.Drawing.Point(479, 113);
            this.cmbSubAgnetName.Name = "cmbSubAgnetName";
            this.cmbSubAgnetName.PromptText = "(Select)";
            this.cmbSubAgnetName.ReadOnly = false;
            this.cmbSubAgnetName.ShowMandatoryMark = false;
            this.cmbSubAgnetName.Size = new System.Drawing.Size(238, 21);
            this.cmbSubAgnetName.TabIndex = 40;
            this.cmbSubAgnetName.ValidationErrorMessage = "Validation Error!";
            // 
            // frmItdBalancOutstanding
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(761, 253);
            this.Controls.Add(this.cmbProducts);
            this.Controls.Add(this.cmbAgentName);
            this.Controls.Add(this.cmbSubAgnetName);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnViewReport);
            this.Controls.Add(this.lblProductName);
            this.Controls.Add(this.lblSubAgent);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblAgent);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "frmItdBalancOutstanding";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ITD Balance Outstanding";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmItdBalancOutstanding_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnViewReport;
        private System.Windows.Forms.Label lblProductName;
        private System.Windows.Forms.Label lblSubAgent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblAgent;
        private CustomTitlebar customTitlebar1;
        private CustomComboBoxDropDownList cmbProducts;
        private CustomComboBoxDropDownList cmbSubAgnetName;
        private CustomComboBoxDropDownList cmbAgentName;
    }
}