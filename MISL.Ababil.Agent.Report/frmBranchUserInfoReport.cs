﻿using MetroFramework.Forms;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Report.Properties;
using MISL.Ababil.Agent.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Report.Reports;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using CrystalDecisions.CrystalReports.Engine;
using MISL.Ababil.Agent.Common.UI;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Report.DataSets;
using System.Globalization;
using MISL.Ababil.Agent.Communication;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Module.Common.UI.MessageUI;

namespace MISL.Ababil.Agent.Report
{
    public partial class frmBranchUserInfoReport : MetroForm
    {

        private List<BankUserReportDto> BankUserReportDtos;
        private List<OutletUserInfoReportSearchDto> result;
        BranchUserInfoReportDS userInfo = new BranchUserInfoReportDS();
        // List<OutletUserInfoReportResultDto> _outletUserInfoReportResultDto;

        public frmBranchUserInfoReport()
        {
            InitializeComponent();
            LoadSetupData();


        }

        private BankUserSearchDto FillSearchDto()
        {
            BankUserSearchDto bankUserSearchDto = new BankUserSearchDto();
            if (cmbBranchName.SelectedIndex == 0)
            {
                MsgBox.showWarning("Please select a branch");
                return null;
            }
            else
            {
                bankUserSearchDto.branchId = (long)cmbBranchName.SelectedValue;
            }
            if (cmbUserStatus.SelectedIndex == -1 || cmbUserStatus.SelectedIndex == 0)
            {
                MsgBox.showWarning("Please select user status");
                return null;
            }
            else
            {
                if (cmbUserStatus.SelectedIndex == 1)
                {
                    bankUserSearchDto.userStatus = null;
                }
                else
                {
                    bankUserSearchDto.userStatus = (UserStatus?)cmbUserStatus.SelectedValue;
                }
            }
            if (cmbUserType.SelectedIndex == -1 || cmbUserType.SelectedIndex == 0)
            {
                MsgBox.showWarning("Please select user category");
                return null;
            }
            else
            {
                if (cmbUserType.SelectedIndex == 1)
                {
                    bankUserSearchDto.bankUserType = null;
                }
                else
                {
                    bankUserSearchDto.bankUserType =  (BankUserType?)cmbUserType.SelectedValue ;
                }
            }

            if (cmbUserTp.SelectedIndex == -1 || cmbUserTp.SelectedIndex == 0)
            {
                MsgBox.showWarning("Please select user type");
                return null;
            }
            else
            {
                if (cmbUserTp.SelectedIndex == 1)
                {
                    bankUserSearchDto.userType = null;
                }
                else
                {
                    bankUserSearchDto.userType = (UserType?)cmbUserTp.SelectedValue;
                }
            }


            return bankUserSearchDto;


        }

        private void btnShowReport_Click(object sender, EventArgs e)
        {

            try
            {
                if (LoadbBranchUserInfoReport() == false)
                {
                    return;
                }

                crBranchUserInfoReport report = new crBranchUserInfoReport();
                frmReportViewer frm = new frmReportViewer();
                ReportHeaders rptHeaders = new ReportHeaders();
                rptHeaders = UtilityServices.getReportHeaders("Branch User Information Report");

                TextObject txtBankName = report.ReportDefinition.ReportObjects["txtBankName"] as TextObject;
                TextObject txtBranchName = report.ReportDefinition.ReportObjects["txtBranchName"] as TextObject;
                TextObject txtBranchAddress = report.ReportDefinition.ReportObjects["txtBranchAddress"] as TextObject;
                TextObject txtReportHeading = report.ReportDefinition.ReportObjects["txtReportHeading"] as TextObject;
                TextObject txtPrintUser = report.ReportDefinition.ReportObjects["txtPrintUser"] as TextObject;
                TextObject txtPrintDate = report.ReportDefinition.ReportObjects["txtPrintDate"] as TextObject;
                //TextObject txtFromDate = report.ReportDefinition.ReportObjects["txtFromDate"] as TextObject;
                //TextObject txtToDate = report.ReportDefinition.ReportObjects["txtToDate"] as TextObject;
                //TextObject txtAgentName = report.ReportDefinition.ReportObjects["txtAgentName"] as TextObject;
                // TextObject txtOutletName = report.ReportDefinition.ReportObjects["txtOutletName"] as TextObject;

                if (rptHeaders != null)
                {
                    if (rptHeaders.branchDto != null)
                    {
                        txtBankName.Text = rptHeaders.branchDto.bankName;
                        txtBranchName.Text = rptHeaders.branchDto.branchName;
                        txtBranchAddress.Text = rptHeaders.branchDto.branchAddress;
                    }
                    txtReportHeading.Text = rptHeaders.reportHeading;
                    txtPrintUser.Text = rptHeaders.printUser;
                    txtPrintDate.Text = rptHeaders.printDate.ToString("dd-MM-yyyy").Replace("-", "/");
                    //txtAgentName.Text =((AgentInformation)cmbAgentName.SelectedItem).businessName;
                    //string txt = ((AgentInformation)cmbAgentName.SelectedItem).businessName;
                    //txtAgentName.Text = DoInitCap.ConvertTo_ProperCase(txt);
                    //txtOutletName.Text = ((SubAgentInformation)cmbUserType.SelectedItem).name;

                }
                report.SetDataSource(userInfo);


                frm.crvReportViewer.ReportSource = report;
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadSetupData()
        {
            
            UtilityServices.fillBranches(ref cmbBranchName);
            cmbBranchName.SelectedIndex = 0;


            Array das = Enum.GetValues(typeof(BankUserType));
            List<object> userTypeList = new List<object>();
            userTypeList.Insert(0, "(Select)");
            userTypeList.Insert(1, "(All)");
            for (int i = 0; i < das.Length; i++)
            {
                userTypeList.Add(das.GetValue(i));
            }
            cmbUserType.DataSource = userTypeList;
            cmbUserType.SelectedIndex = 0;


            Array dataSource = Enum.GetValues(typeof(UserStatus));
            List<object> userStatusList = new List<object>();
            userStatusList.Insert(0, "(Select)");
            userStatusList.Insert(1, "(All)");
            for (int i = 0; i < dataSource.Length; i++)
            {
                userStatusList.Add(dataSource.GetValue(i));
            }
            cmbUserStatus.DataSource = userStatusList;
            cmbUserStatus.SelectedIndex = 0;


            Array dts = Enum.GetValues(typeof(UserType));
            List<object> userTpList = new List<object>();
            userTpList.Insert(0, "(Select)");
            userTpList.Insert(1, "(All)");
            for (int i = 0; i < dts.Length; i++)
            {
                userTpList.Add(dts.GetValue(i));
            }
            cmbUserTp.DataSource = userTpList;
            cmbUserTp.SelectedIndex = 0;


        }





        private bool LoadbBranchUserInfoReport()
        {
            UserService userService = new UserService();

            BankUserSearchDto _bankUserSearchDto = FillSearchDto();
            if (_bankUserSearchDto == null)
            {
                return false;
            }

            List<BankUserReportDto> bankUserReportDtos = userService.GetBankUserInformation(_bankUserSearchDto);


            try
            {
                //result = agentServices.getOutletUserInfoResultList(outletSearchDto);
                if (bankUserReportDtos != null)
                {
                    userInfo.Clear();
                    if (bankUserReportDtos != null)
                    {
                        for (int i = 0; i < bankUserReportDtos.Count; i++)
                        {
                            BankUserReportDto bankUser = bankUserReportDtos[i];
                            BranchUserInfoReportDS.BrancUserInfoReportDTRow newRow =
                                userInfo.BrancUserInfoReportDT.NewBrancUserInfoReportDTRow();

                            newRow.userName = bankUser.userName;
                            newRow.userId = bankUser.userId.ToString();
                            newRow.status = bankUser.status.ToString();
                            newRow.contactNo = bankUser.contactNo;
                            newRow.userCategory = bankUser.bankUserType.ToString();
                            newRow.Branch = bankUser.branchName + "(" + bankUser.branchId + ")";
                            newRow.userType = bankUser.usertype.ToString();
                            userInfo.BrancUserInfoReportDT.AddBrancUserInfoReportDTRow(newRow);

                        }
                    }//("N", new CultureInfo("BN-BD")
                    userInfo.AcceptChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                CustomMessage.showError("Report data loading error.\n" + ex.Message);
            }
            return false;
        }





        private void frmOutletUserInfoReport_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }


    }


}
