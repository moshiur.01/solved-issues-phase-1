﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Infrastructure.Models.models.transaction;
using MISL.Ababil.Agent.Infrastructure.Validation;
using MISL.Ababil.Agent.Report.Reports;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Report.DataSets;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using MetroFramework.Forms;
using System.Globalization;
using MISL.Ababil.Agent.Common.UI;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using MISL.Ababil.Agent.Module.Common.UI.MessageUI;
using MISL.Ababil.Agent.UI.forms.ProgressUI;

namespace MISL.Ababil.Agent.Report
{
    public partial class frmDailyReport : CustomForm
    {
        private GUI gui = new GUI();
        List<AgentInformation> objAgentInfoList = new List<AgentInformation>();
        AgentServices objAgentServices = new AgentServices();
        AgentInformation agentInformation = new AgentInformation();
        AgentServices agentServices = new AgentServices();

        // AgentIncomeStatementDS agentDS = new AgentIncomeStatementDS();

        //public List<OutletWiseAcc> _agentTrMonitorings = new List<OutletWiseAcc>();
        private List<AgentProduct> _agentProducts;

        List<SubAgentInformation> subAgentList = new List<SubAgentInformation>();       // WALI :: 14-Jan-2016

        DailyReportDs _outletWiseAccountInfoReportDs = new DailyReportDs();

        public frmDailyReport()
        {
            InitializeComponent();
            GetSetupData();
            SetStatus();
            try
            {
                cmbAgentName.SelectedIndex = 0;
            }
            catch (Exception exp)
            { }
            controlActivity();
            ConfigUIEnhancement();
        }

        private void SetStatus()
        {
            cmbStatus.Items.Insert(0, "All");
            cmbStatus.Items.Add(ApplicationStatus.draft);
            cmbStatus.Items.Add(ApplicationStatus.correction);
            cmbStatus.SelectedIndex = -1;

        }

        public void ConfigUIEnhancement()
        {
            gui = new GUI(this);
            gui.Config(ref cmbAgentName, ValidCheck.VALIDATIONTYPES.COMBOBOX_EMPTY, null);
            gui.Config(ref cmbSubAgnetName);
            gui.Config(ref dtpFromDate);
            gui.Config(ref dtpToDate);
        }
        private void controlActivity()
        {
            try
            {
                if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_CENTRALLY.ToString())) //branch user
                {
                    cmbAgentName.Enabled = true;
                    cmbSubAgnetName.Enabled = true;
                }
                else if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_AGENTWISE.ToString())) //agent user
                {
                    cmbAgentName.SelectedValue = UtilityServices.getCurrentAgent().id;
                    cmbAgentName.Enabled = false;
                    cmbSubAgnetName.Enabled = true;
                    //agentInformation = agentServices.getAgentInfoById(UtilityServices.getCurrentAgent().id.ToString());
                    subAgentList = agentServices.GetSubagentsByAgentId((long)cmbAgentName.SelectedValue);         // WALI :: 14-Jan-2016
                    setSubagent();
                }
                else
                {
                    SubAgentInformation currentSubagentInfo = UtilityServices.getCurrentSubAgent();
                    cmbAgentName.SelectedValue = currentSubagentInfo.agent.id;
                    //agentInformation = agentServices.getAgentInfoById(currentSubagentInfo.agent.id.ToString());
                    subAgentList = agentServices.GetSubagentsByAgentId((long)cmbAgentName.SelectedValue);         // WALI :: 14-Jan-2016
                    setSubagent();
                    cmbAgentName.Enabled = false;
                    cmbSubAgnetName.SelectedValue = currentSubagentInfo.id;
                    cmbSubAgnetName.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);

            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            if (DateTime.ParseExact(dtpDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture) >
                SessionInfo.currentDate)
            {
                MsgBox.showWarning("future Date Is Not Allowed.....");
                return;

            }//DateTime fromDate, toDate;
            //try
            //{
            //    fromDate = DateTime.ParseExact(dtpFromDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
            //}
            //catch
            //{
            //    CustomMessage.showWarning("Please enter the Date in correct format!!");
            //    return;
            //}
            //try
            //{
            //    toDate = DateTime.ParseExact(dtpToDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
            //}
            //catch
            //{
            //    CustomMessage.showWarning("Please enter the Date in correct format!!");
            //    return;
            //}

            //if (SessionInfo.currentDate < toDate)
            //{
            //    CustomMessage.showWarning("Future date is not allowed!");
            //    return;
            //}
            //if (fromDate > toDate)
            //{
            //    CustomMessage.showWarning("From date can not be greater than to date!!");
            //    return;
            //}
            try
            {
                if (cmbAgentName.SelectedIndex > 0)
                {
                    crDailyReport report = new crDailyReport();
                    frmReportViewer viewer = new frmReportViewer();

                    ReportHeaders rptHeaders = new ReportHeaders();
                    rptHeaders = UtilityServices.getReportHeaders("Daily Report");

                    TextObject txtBankName = report.ReportDefinition.ReportObjects["txtBankName"] as TextObject;
                    TextObject txtBranchName = report.ReportDefinition.ReportObjects["txtBranchName"] as TextObject;
                    TextObject txtBranchAddress = report.ReportDefinition.ReportObjects["txtBranchAddress"] as TextObject;
                    TextObject txtReportHeading = report.ReportDefinition.ReportObjects["txtReportHeading"] as TextObject;
                    TextObject txtPrintUser = report.ReportDefinition.ReportObjects["txtPrintUser"] as TextObject;
                    TextObject txtPrintDate = report.ReportDefinition.ReportObjects["txtPrintDate"] as TextObject;
                    TextObject txtDate1 = report.ReportDefinition.ReportObjects["txtDate1"] as TextObject;
                    TextObject txtDate2 = report.ReportDefinition.ReportObjects["txtDate2"] as TextObject;
                    TextObject txtDate3 = report.ReportDefinition.ReportObjects["txtDate3"] as TextObject;
                    TextObject txtDate4 = report.ReportDefinition.ReportObjects["txtDate4"] as TextObject;
                    TextObject txtDate5 = report.ReportDefinition.ReportObjects["txtDate5"] as TextObject;
                    TextObject txtDate6 = report.ReportDefinition.ReportObjects["txtDate6"] as TextObject;
                    TextObject txtDate7 = report.ReportDefinition.ReportObjects["txtDate7"] as TextObject;
                    //TextObject txtFromDate = report.ReportDefinition.ReportObjects["txtFromDate"] as TextObject;
                    //TextObject txtToDate = report.ReportDefinition.ReportObjects["txtToDate"] as TextObject;
                    if (rptHeaders != null)
                    {
                        if (rptHeaders.branchDto != null)
                        {
                            txtBankName.Text = rptHeaders.branchDto.bankName;
                            txtBranchName.Text = rptHeaders.branchDto.branchName;
                            txtBranchAddress.Text = rptHeaders.branchDto.branchAddress;
                        }
                        txtReportHeading.Text = rptHeaders.reportHeading;
                        txtPrintUser.Text = rptHeaders.printUser;
                        txtPrintDate.Text = rptHeaders.printDate.ToString("dd/MM/yyyy").Replace("-", "/");
                    }
                    #region harun
                    //txtFromDate.Text = Convert.ToDateTime(dtpFromDate.Text).ToString("dd/MM/yyyy");
                    //txtToDate.Text = Convert.ToDateTime(dtpToDate.Text).ToString("dd/MM/yyyy");
                    #endregion

                    txtDate1.Text = dtpDate.Date;
                    txtDate2.Text = dtpDate.Date;
                    txtDate3.Text = dtpDate.Date;
                    txtDate4.Text = dtpDate.Date;
                    txtDate5.Text = dtpDate.Date;
                    txtDate6.Text = dtpDate.Date;
                    txtDate7.Text = dtpDate.Date;
                    //txtFromDate.Text = dtpFromDate.Date;
                    //txtToDate.Text = dtpToDate.Date;

                    LoadAgentTrMonitoring();

                    //report.SetDataSource(_agentTrMonitorings);
                    report.SetDataSource(_outletWiseAccountInfoReportDs);

                    viewer.crvReportViewer.ReportSource = report;
                    viewer.ShowDialog(this.Parent);
                }
                else
                {
                    CustomMessage.showWarning("Please select an option!");
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        private void LoadAgentTrMonitoring()
        {
            //OutletWiseAcc agentTrMonitoringInfo;

            _outletWiseAccountInfoReportDs.Clear();
            DailyReportSearchDto accountSearchDto = new DailyReportSearchDto();
            if (FillAccountSearchDto() != null)
            {
                accountSearchDto = FillAccountSearchDto();
            }
            ServiceResult result;
            _outletWiseAccountInfoReportDs = new DailyReportDs();

            try
            {
                ProgressUIManager.ShowProgress(this);
                result = new AccountOpeningReportService().GetDailyReport(accountSearchDto);
                ProgressUIManager.CloseProgress();

                if (!result.Success)
                {
                    MessageBox.Show(result.Message);
                    return;
                }

                List<DailyReportResultDto> agentTrMonitoring = result.ReturnedObject as List<DailyReportResultDto>;

                if (agentTrMonitoring != null)
                {
                    foreach (DailyReportResultDto agentCommission in agentTrMonitoring)
                    {
                        DailyReportDs.DailyReportDtRow newRow = _outletWiseAccountInfoReportDs.DailyReportDt.NewDailyReportDtRow();


                        newRow.agentName = agentCommission.agentName;

                        newRow.outletName = agentCommission.outletName;
                        newRow.outletId = agentCommission.outletId.ToString();
                        newRow.agentId = agentCommission.agentId.ToString();
                        newRow.accOpenedasOn = agentCommission.accOpenedAsOn;
                        newRow.submitedToBranchAsOn = agentCommission.submittedToBranchAsOn;
                        newRow.approvedOn = agentCommission.ApprovedOn;
                        newRow.correctionFromHeadOn = agentCommission.correctionFromHeadOfficeOn;
                        newRow.correctionFromBranchOn = agentCommission.correctionFromBranchOn;
                        newRow.pendingForApprovalOn = agentCommission.pendingForApprovalOn;
                        newRow.draftAsOn = agentCommission.draftAsOn;




                        _outletWiseAccountInfoReportDs.DailyReportDt.AddDailyReportDtRow(newRow);

                        #region Used the datased instead :: WALI 
                        //agentTrMonitoringInfo = new OutletWiseAcc();
                        //agentTrMonitoringInfo.AgentId = agentCommission.agentId;
                        //agentTrMonitoringInfo.AgentName = agentCommission.agentName;
                        //agentTrMonitoringInfo.SubAgentId = agentCommission.subAgentId;
                        //agentTrMonitoringInfo.SubAgentName = agentCommission.subAgentName;
                        //agentTrMonitoringInfo.NoOfAccount = agentCommission.noOfAccount ?? 0;

                        //agentTrMonitoringInfo.NoOfDeposit = agentCommission.noOfDeposit ?? 0;
                        //agentTrMonitoringInfo.DepositAmount = agentCommission.depositAmount ?? 0;

                        //agentTrMonitoringInfo.NoOfWithdraw = agentCommission.noOfWithdraw ?? 0;
                        //agentTrMonitoringInfo.WithdrawAmount = agentCommission.withdrawAmount ?? 0;

                        //agentTrMonitoringInfo.NoOfFundTransfer = agentCommission.noOfFundTransfer ?? 0;
                        //agentTrMonitoringInfo.TransferAmount = agentCommission.transferAmount ?? 0;

                        //agentTrMonitoringInfo.NoOfRemittance = agentCommission.noOfRemittance ?? 0;
                        //agentTrMonitoringInfo.RemittanceAmount = agentCommission.remittanceAmount ?? 0;

                        //agentTrMonitoringInfo.NoOfUtilityBill = agentCommission.noOfUtilityBill ?? 0;
                        //agentTrMonitoringInfo.BillCollectionAmount = agentCommission.billCollectionAmount ?? 0;

                        //agentTrMonitoringInfo.NoOfLoanDisbursed = agentCommission.noOfLoanDisbursed ?? 0;
                        //agentTrMonitoringInfo.DisbursedAmount = agentCommission.disbursedAmount ?? 0;

                        //agentTrMonitoringInfo.NoOfInstallmentRecovered = agentCommission.noOfInstallmentRecovered ?? 0;
                        //agentTrMonitoringInfo.RecoveredAmount = agentCommission.recoveredAmount ?? 0;

                        //_agentTrMonitorings.Add(agentTrMonitoringInfo);
                        #endregion
                    }
                }
                _outletWiseAccountInfoReportDs.AcceptChanges();
            }
            catch (Exception)
            {
                ProgressUIManager.CloseProgress();
                //ignored
            }
        }

        private DailyReportSearchDto FillAccountSearchDto()
        {
            DailyReportSearchDto outletWiseAccountInfoReportParaDto = new DailyReportSearchDto();

            if (this.cmbAgentName.SelectedIndex > -1)
            {
                if (cmbAgentName.SelectedIndex != 0 || cmbAgentName.SelectedIndex != 1)
                {
                    outletWiseAccountInfoReportParaDto.agentId = (long)cmbAgentName.SelectedValue;
                }
                if (cmbAgentName.SelectedIndex == 1 || cmbAgentName.SelectedIndex == 0)
                {
                    outletWiseAccountInfoReportParaDto.agentId = 0; // new AgentInformation();
                }
                if (cmbAgentName.SelectedIndex == 0)
                {

                }
            }
            if (this.cmbSubAgnetName.SelectedIndex > -1)
            {
                if (cmbSubAgnetName.SelectedIndex != 0 || cmbSubAgnetName.SelectedIndex != 1)
                {
                    outletWiseAccountInfoReportParaDto.outletId = (long)cmbSubAgnetName.SelectedValue;
                }
                if (cmbSubAgnetName.SelectedIndex == 1 || cmbSubAgnetName.SelectedIndex == 0)
                {
                    outletWiseAccountInfoReportParaDto.outletId = 0; // new SubAgentInformation();
                }
                if (cmbSubAgnetName.SelectedIndex == 0)
                {

                }
            }


            outletWiseAccountInfoReportParaDto.date = UtilityServices.GetLongDate
            (
                DateTime.ParseExact(dtpDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture)
            );

            return outletWiseAccountInfoReportParaDto;
        }

        private void GetSetupData()
        {

            try
            {
                setAgentList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void setAgentList()
        {
            objAgentInfoList = objAgentServices.getAgentInfoBranchWise();
            BindingSource bs = new BindingSource();
            bs.DataSource = objAgentInfoList;

            AgentInformation agSelect = new AgentInformation();
            agSelect.businessName = "(Select)";
            objAgentInfoList.Insert(0, agSelect);
            AgentInformation agAll = new AgentInformation();
            agAll.businessName = "(All)";
            objAgentInfoList.Insert(1, agAll);

            UtilityServices.fillComboBox(cmbAgentName, bs, "businessName", "id");
            cmbAgentName.SelectedIndex = 0;
        }
        private void cmbAgentName_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!cb.Focused)
            {
                return;
            }

            try
            {
                if (cmbAgentName.SelectedIndex > 1)
                {
                    subAgentList = agentServices.GetSubagentsByAgentId((long)cmbAgentName.SelectedValue);         // WALI :: 14-Jan-2016
                }
                if (cmbAgentName.SelectedIndex == 1)
                {

                }
                if (cmbAgentName.SelectedIndex < 2)
                {
                    cmbSubAgnetName.DataSource = null;
                    cmbSubAgnetName.Items.Clear();
                    return;
                }
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
            setSubagent();
        }
        private void setSubagent()
        {
            #region Commented :: WALI :: 14-Jan-2016
            //if (agentInformation != null)
            //{
            //    BindingSource bs = new BindingSource();
            //    bs.DataSource = agentInformation.subAgents;

            //    try
            //    {
            //        SubAgentInformation saiSelect = new SubAgentInformation();
            //        saiSelect.name = "(Select)";

            //        SubAgentInformation saiAll = new SubAgentInformation();
            //        saiAll.name = "(All)";

            //        agentInformation.subAgents.Insert(0, saiSelect);
            //        agentInformation.subAgents.Insert(1, saiAll);
            //    }
            //    catch (Exception exp)
            //    { }

            //    UtilityServices.fillComboBox(cmbSubAgnetName, bs, "name", "id");
            //    if (cmbSubAgnetName.Items.Count > 0)
            //    {
            //        cmbSubAgnetName.SelectedIndex = 0;
            //    }
            //}
            #endregion

            if (subAgentList != null)
            {
                BindingSource bs = new BindingSource();
                bs.DataSource = subAgentList;

                try
                {
                    SubAgentInformation saiSelect = new SubAgentInformation();
                    saiSelect.name = "(Select)";

                    SubAgentInformation saiAll = new SubAgentInformation();
                    saiAll.name = "(All)";

                    subAgentList.Insert(0, saiSelect);
                    subAgentList.Insert(1, saiAll);
                }
                catch (Exception exp)
                { }

                UtilityServices.fillComboBox(cmbSubAgnetName, bs, "name", "id");
                if (cmbSubAgnetName.Items.Count > 0)
                {
                    cmbSubAgnetName.SelectedIndex = 0;
                }
            }
        }
        #region========== harun(22-09-015)==========
        //private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        //{
        //    mtbFromDate.Text = dtpFromDate.Value.ToString("dd-MM-yyyy");
        //}

        //private void dtpToDate_ValueChanged(object sender, EventArgs e)
        //{
        //    mtbToDate.Text = dtpToDate.Value.ToString("dd-MM-yyyy");
        //}

        //private void mtbFromDate_KeyUp(object sender, KeyEventArgs e)
        //{
        //    //suppressed to avoid mtb to dtp conversion
        //    try
        //    {
        //        string[] str = mtbFromDate.Text.Split('-');
        //        DateTime d = new DateTime(int.Parse(str[2].Trim()), int.Parse(str[1].Trim()), int.Parse(str[0].Trim()));
        //        dtpFromDate.Value = d;
        //    }
        //    catch (Exception ex) { }
        //}

        //private void mtbToDate_KeyUp(object sender, KeyEventArgs e)
        //{
        //    //suppressed to avoid mtb to dtp conversion
        //    try
        //    {
        //        string[] str = mtbToDate.Text.Split('-');
        //        DateTime d = new DateTime(int.Parse(str[2].Trim()), int.Parse(str[1].Trim()), int.Parse(str[0].Trim()));
        //        dtpToDate.Value = d;
        //    }
        //    catch (Exception ex) { }
        //}      
        #endregion


        private void frmTrMonitoringRport_Load(object sender, EventArgs e)
        {
            dtpFromDate.Value = SessionInfo.currentDate;
            dtpToDate.Value = SessionInfo.currentDate;
        }

        private void frmTrMonitoringRport_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void cmbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbSubAgnetName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
    //public class OutletWiseAcc
    //{
    //    public long AgentId = 0;
    //    public string AgentName = "";
    //    public long SubAgentId = 0;
    //    public string SubAgentName = "";
    //    public long NoOfAccount;

    //    public long NoOfDeposit = 0;
    //    public decimal DepositAmount = 0;

    //    public long NoOfWithdraw = 0;
    //    public decimal WithdrawAmount = 0;

    //    public long NoOfFundTransfer = 0;
    //    public decimal TransferAmount = 0;

    //    public long NoOfRemittance = 0;
    //    public decimal RemittanceAmount = 0;

    //    public long NoOfUtilityBill = 0;
    //    public decimal BillCollectionAmount = 0;

    //    public long NoOfLoanDisbursed = 0;
    //    public decimal DisbursedAmount = 0;

    //    public long NoOfInstallmentRecovered = 0;
    //    public decimal RecoveredAmount = 0;
    //}


}
