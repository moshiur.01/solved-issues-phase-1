﻿using MetroFramework.Forms;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Report.Properties;
using MISL.Ababil.Agent.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Report.Reports;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using CrystalDecisions.CrystalReports.Engine;
using MISL.Ababil.Agent.Common.UI;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Report.DataSets;
using System.Globalization;
using MISL.Ababil.Agent.Module.Common.UI.MessageUI;

namespace MISL.Ababil.Agent.Report
{
    public partial class frmOutletUserInfoReport : MetroForm
    {
        private List<AgentInformation> objAgentInfoList = new List<AgentInformation>();
        private AgentServices agentServices = new AgentServices();
        private AgentInformation agentInformation = new AgentInformation();
        private List<OutletUserInfoReportSearchDto> result;
        OutletUserInfoDS userInfo = new OutletUserInfoDS();
        // List<OutletUserInfoReportResultDto> _outletUserInfoReportResultDto;
        List<SubAgentUserLimitDto> _outletUserInfoReportResultDto;
        public frmOutletUserInfoReport()
        {
            InitializeComponent();
            LoadSetupData();
            controlActivity();
        }
        private void controlActivity()
        {
            try
            {
                if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_CENTRALLY.ToString())) //branch user
                {
                    cmbAgentName.Enabled = true;
                }
                else if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_AGENTWISE.ToString())) //agent user
                {
                    cmbAgentName.SelectedValue = UtilityServices.getCurrentAgent().id;
                    cmbAgentName.Enabled = false;

                }
                else
                {
                    SubAgentInformation currentSubagentInfo = UtilityServices.getCurrentSubAgent();
                    cmbAgentName.SelectedValue = currentSubagentInfo.agent.id;
                    cmbAgentName.Enabled = false;

                }
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);

            }
        }
        private void btnShowReport_Click(object sender, EventArgs e)
        {
            try
            {
                if (CheckForValidation(true))
                {
                    LoadOutletReportData();

                    crOutletUserInfoReport report = new crOutletUserInfoReport();
                    frmReportViewer frm = new frmReportViewer();
                    ReportHeaders rptHeaders = new ReportHeaders();
                    rptHeaders = UtilityServices.getReportHeaders("Outlet User Information Report");

                    TextObject txtBankName = report.ReportDefinition.ReportObjects["txtBankName"] as TextObject;
                    TextObject txtBranchName = report.ReportDefinition.ReportObjects["txtBranchName"] as TextObject;
                    TextObject txtBranchAddress = report.ReportDefinition.ReportObjects["txtBranchAddress"] as TextObject;
                    TextObject txtReportHeading = report.ReportDefinition.ReportObjects["txtReportHeading"] as TextObject;
                    TextObject txtPrintUser = report.ReportDefinition.ReportObjects["txtPrintUser"] as TextObject;
                    TextObject txtPrintDate = report.ReportDefinition.ReportObjects["txtPrintDate"] as TextObject;
                    //TextObject txtFromDate = report.ReportDefinition.ReportObjects["txtFromDate"] as TextObject;
                    //TextObject txtToDate = report.ReportDefinition.ReportObjects["txtToDate"] as TextObject;
                    TextObject txtAgentName = report.ReportDefinition.ReportObjects["txtAgentName"] as TextObject;
                    TextObject txtOutletName = report.ReportDefinition.ReportObjects["txtOutletName"] as TextObject;

                    if (rptHeaders != null)
                    {
                        if (rptHeaders.branchDto != null)
                        {
                            txtBankName.Text = rptHeaders.branchDto.bankName;
                            txtBranchName.Text = rptHeaders.branchDto.branchName;
                            txtBranchAddress.Text = rptHeaders.branchDto.branchAddress;
                        }
                        txtReportHeading.Text = rptHeaders.reportHeading;
                        txtPrintUser.Text = rptHeaders.printUser;
                        txtPrintDate.Text = rptHeaders.printDate.ToString("dd-MM-yyyy").Replace("-", "/");
                        //txtAgentName.Text =((AgentInformation)cmbAgentName.SelectedItem).businessName;
                        string txt = ((AgentInformation)cmbAgentName.SelectedItem).businessName;
                        txtAgentName.Text = DoInitCap.ConvertTo_ProperCase(txt);
                        txtOutletName.Text = ((SubAgentInformation)cmbOutletName.SelectedItem).name;

                    }
                    report.SetDataSource(userInfo);


                    frm.crvReportViewer.ReportSource = report;
                    frm.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadSetupData()
        {
            objAgentInfoList = agentServices.getAgentInfoBranchWise();
            BindingSource bsAgent = new BindingSource();
            bsAgent.DataSource = objAgentInfoList;

            AgentInformation agSelect = new AgentInformation();
            agSelect.businessName = Resources.SetupData__Select_;
            objAgentInfoList.Insert(0, agSelect);
            //AgentInformation agAll = new AgentInformation();
            //agAll.businessName = Resources.SetupData__All_;
            //objAgentInfoList.Insert(1, agAll);

            UtilityServices.fillComboBox(cmbAgentName, bsAgent, "businessName", "id");
            cmbAgentName.Text = "Select";
            cmbAgentName.SelectedIndex = 0;

            cmbUserStatus.DataSource = Enum.GetValues(typeof(UserStatus));
            cmbUserStatus.SelectedIndex = -1;
            // setSubagent();

        }

        private void cmbAgentName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmbAgentName.SelectedIndex > 0)
                {
                    agentInformation = agentServices.getAgentInfoById(cmbAgentName.SelectedValue.ToString());
                }
                if (cmbAgentName.SelectedIndex < 1)
                {
                    cmbOutletName.DataSource = null;
                    cmbOutletName.Items.Clear();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            setSubagent();
        }

        private void setSubagent()
        {
            agentInformation = agentServices.getAgentInfoById(cmbAgentName.SelectedValue.ToString());
            if (agentInformation != null)
            {
                BindingSource bs = new BindingSource();
                bs.DataSource = agentInformation.subAgents;


                try
                {
                    SubAgentInformation saiSelect = new SubAgentInformation();
                    saiSelect.name = "(Select)";

                    //SubAgentInformation saiAll = new SubAgentInformation();
                    //saiAll.name = "(All)";

                    agentInformation.subAgents.Insert(0, saiSelect);
                    //  agentInformation.subAgents.Insert(1, saiAll);
                }
                catch (Exception exp)
                {
                }

                UtilityServices.fillComboBox(cmbOutletName, bs, "name", "id");
                if (cmbOutletName.Items.Count > 0)
                {
                    cmbOutletName.SelectedIndex = 0;
                }
            }
        }

        private void LoadOutletReportData()
        {
            UserService userService = new UserService();

            _outletUserInfoReportResultDto = userService.GetUserBasicInformation(cmbOutletName.SelectedValue.ToString());
            try
            {
                //result = agentServices.getOutletUserInfoResultList(outletSearchDto);
                if (_outletUserInfoReportResultDto != null)
                {
                    userInfo.Clear();
                    if (_outletUserInfoReportResultDto != null)
                    {
                        for (int i = 0; i < _outletUserInfoReportResultDto.Count; i++)
                        {
                            SubAgentUserLimitDto account = _outletUserInfoReportResultDto[i];
                            OutletUserInfoDS.OutletUserInfoDTRow newRow =
                                userInfo.OutletUserInfoDT.NewOutletUserInfoDTRow();

                            newRow.userName = account.userName;
                            newRow.userStatus = account.userStatus.ToString();
                            newRow.fullName = account.fullName;
                            newRow.mobileNumber = account.mobileNumber;
                            if (account.creationDate != null)
                            {
                                newRow.creationDate = UtilityServices.getDateFromLong(account.creationDate);
                            }
                            newRow.cashBalance = account.cashBalance ?? 0;
                            newRow.cashLimit = account.cashLimit ?? 0;
                            userInfo.OutletUserInfoDT.AddOutletUserInfoDTRow(newRow);
                        }
                    }//("N", new CultureInfo("BN-BD")
                    userInfo.AcceptChanges();
                }
            }
            catch (Exception ex)
            {
                CustomMessage.showError("Report data loading error.\n" + ex.Message);
            }
        }



        private bool CheckForValidation(bool showMessages)
        {
            OutletUserInfoReportSearchDto outletUserInfoSearchDto = new OutletUserInfoReportSearchDto();

            if (this.cmbAgentName.SelectedIndex > 0)
            {
                outletUserInfoSearchDto._agent = new AgentInformation { id = (long)cmbAgentName.SelectedValue };
            }
            else
            {
                if (showMessages == true)
                {
                    CustomMessage.showWarning("Please select an Agent!");
                }
                return false;
            }

            if (this.cmbOutletName.SelectedIndex > 0)
            {
                outletUserInfoSearchDto._agent = new AgentInformation { id = (long)cmbAgentName.SelectedValue };
            }
            else
            {
                if (showMessages == true)
                {
                    CustomMessage.showWarning("Please select an Outlet!");
                }
                return false;
            }
            if (this.cmbOutletName.SelectedIndex > 0)
            {
                if (cmbUserStatus.SelectedItem != null)
                {
                    outletUserInfoSearchDto.userStatus = (UserStatus)cmbUserStatus.SelectedItem;
                }
            }
            return true;
        }

        private void frmOutletUserInfoReport_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }


    }


}
