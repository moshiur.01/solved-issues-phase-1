﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Infrastructure.Models.models.transaction;
using MISL.Ababil.Agent.Infrastructure.Validation;
using MISL.Ababil.Agent.Report.Reports;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.ssp;

using MISL.Ababil.Agent.Report.DataSets;
using MetroFramework.Forms;
using System.Threading.Tasks;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.termaccount;
using MISL.Ababil.Agent.LocalStorageService;
using System.Globalization;
using MISL.Ababil.Agent.Common.UI;
using MISL.Ababil.Agent.Module.Common.UI.MessageUI;

namespace MISL.Ababil.Agent.Report
{
    public partial class frmAccountOpeningRport : MetroForm
    {
        private GUI gui = new GUI();
        List<AgentInformation> objAgentInfoList = new List<AgentInformation>();
        AgentServices objAgentServices = new AgentServices();
        AgentInformation agentInformation = new AgentInformation();
        AgentServices agentServices = new AgentServices();

        public List<AccountOpeningInformation> _accountInformations = new List<AccountOpeningInformation>();
        private List<AgentProduct> _agentProducts;
        private List<FinanceProduct> _financeProducts;
        private List<SspProductType> _agentSspProducts;
        AccountOpeningReport accRep = new AccountOpeningReport();

        List<SubAgentInformation> subAgentList = new List<SubAgentInformation>();       // WALI :: 14-Jan-2016

        public frmAccountOpeningRport()
        {
            InitializeComponent();
            GetSetupData();
            try
            {
                cmbAgentName.SelectedIndex = 0;
                cmbProduct.SelectedIndex = 0;
            }
            catch
            {
            }
            controlActivity();
            ConfigUIEnhancement();


        }
        public void ConfigUIEnhancement()
        {
            gui = new GUI(this);
            gui.Config(ref cmbAgentName, ValidCheck.VALIDATIONTYPES.COMBOBOX_EMPTY, null);
            gui.Config(ref cmbSubAgnetName);
            gui.Config(ref cmbProduct, ValidCheck.VALIDATIONTYPES.COMBOBOX_EMPTY, null);
            gui.Config(ref dtpFromDate);
            gui.Config(ref dtpToDate);
        }
        private void controlActivity()
        {
            try
            {
                if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_CENTRALLY.ToString())) //branch user
                {
                    cmbAgentName.Enabled = true;
                    cmbSubAgnetName.Enabled = true;
                }
                else if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_AGENTWISE.ToString())) //agent user
                {
                    cmbAgentName.SelectedValue = UtilityServices.getCurrentAgent().id;
                    cmbAgentName.Enabled = false;
                    cmbSubAgnetName.Enabled = true;
                    //agentInformation = agentServices.getAgentInfoById(UtilityServices.getCurrentAgent().id.ToString());
                    subAgentList = agentServices.GetSubagentsByAgentId((long)cmbAgentName.SelectedValue);         // WALI :: 14-Jan-2016
                    setSubagent();
                }
                else
                {
                    SubAgentInformation currentSubagentInfo = UtilityServices.getCurrentSubAgent();
                    cmbAgentName.SelectedValue = currentSubagentInfo.agent.id;
                    //agentInformation = agentServices.getAgentInfoById(currentSubagentInfo.agent.id.ToString());
                    subAgentList = agentServices.GetSubagentsByAgentId((long)cmbAgentName.SelectedValue);         // WALI :: 14-Jan-2016
                    setSubagent();
                    cmbAgentName.Enabled = false;
                    cmbSubAgnetName.SelectedValue = currentSubagentInfo.id;
                    cmbSubAgnetName.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            accRep.AccountOpenings.Clear();

            DateTime fromDate, toDate;

            try
            {
                fromDate = DateTime.ParseExact(dtpFromDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                toDate = DateTime.ParseExact(dtpToDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                CustomMessage.showWarning("Invalid date format!!");
                return;
            }


            if (SessionInfo.currentDate < toDate)
            {
                CustomMessage.showWarning("Future date is not allowed!");
                return;
            }
            if (fromDate > toDate)
            {
                CustomMessage.showWarning("From date can not be greater than to date!!");
                return;
            }

            try
            {
                if (cmbAgentName.SelectedIndex > 0 && cmbProduct.SelectedIndex > 0) //((ComboboxItem) cmbProduct.SelectedItem).Text.ToLower() != "(all)" && ((ComboboxItem)cmbProduct.SelectedItem).Text.ToLower() != "(select)")
                {

                    crAccountOpeningReportByDateRange_Eng report = new crAccountOpeningReportByDateRange_Eng();
                    frmReportViewer viewer = new frmReportViewer();

                    ReportHeaders rptHeaders = new ReportHeaders();
                    rptHeaders = UtilityServices.getReportHeaders("Account Opening Register");

                    TextObject txtBankName = report.ReportDefinition.ReportObjects["txtBankName"] as TextObject;
                    TextObject txtBranchName = report.ReportDefinition.ReportObjects["txtBranchName"] as TextObject;
                    TextObject txtBranchAddress = report.ReportDefinition.ReportObjects["txtBranchAddress"] as TextObject;
                    TextObject txtReportHeading = report.ReportDefinition.ReportObjects["txtReportHeading"] as TextObject;
                    TextObject txtPrintUser = report.ReportDefinition.ReportObjects["txtPrintUser"] as TextObject;
                    TextObject txtPrintDate = report.ReportDefinition.ReportObjects["txtPrintDate"] as TextObject;
                    TextObject txtFromDate = report.ReportDefinition.ReportObjects["txtFromDate"] as TextObject;
                    TextObject txtToDate = report.ReportDefinition.ReportObjects["txtToDate"] as TextObject;
                    if (rptHeaders != null)
                    {
                        if (rptHeaders.branchDto != null)
                        {
                            txtBankName.Text = rptHeaders.branchDto.bankName;
                            txtBranchName.Text = rptHeaders.branchDto.branchName;
                            txtBranchAddress.Text = rptHeaders.branchDto.branchAddress;
                        }
                        txtReportHeading.Text = rptHeaders.reportHeading;
                        txtPrintUser.Text = rptHeaders.printUser;
                        txtPrintDate.Text = rptHeaders.printDate.ToString("dd-MM-yyyy").Replace("-", "/");
                    }
                    txtFromDate.Text = dtpFromDate.Date;
                    txtToDate.Text = dtpToDate.Date;



                    if (rBtnDeposit.Checked == true)
                    {
                        LoadAccountBalanceInformation(ProductType.Deposit);

                    }
                    if (rBtnITDMTD.Checked == true)
                    {
                        LoadAccountBalanceInformation(ProductType.SSP);
                    }
                    if (rBtnMTD.Checked == true)
                    {
                        LoadAccountBalanceInformation(ProductType.MTDR);
                    }
                    if(rBtnInvestment.Checked == true)
                    {
                        LoadAccountBalanceInformation(ProductType.INVESTMENT);
                    }


                    report.SetDataSource(accRep);

                    viewer.crvReportViewer.ReportSource = report;
                    viewer.ShowDialog();
                }
                else
                {
                    CustomMessage.showWarning("Please select an option!");
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        private void LoadAccountBalanceInformation(ProductType productType)
        {
            try
            {
                AccountOpeningInformation accountOpeningInformation = new AccountOpeningInformation();
                _accountInformations.Clear();
                AccountSearchDto accountSearchDto = new AccountSearchDto(); //FillAccountSearchDto();
                SspAccountSearchDto sspAccountSearchDto = new SspAccountSearchDto(); //FillSspSearchDto(); 
                SspAccountSearchDto mtdAccountSearchDto = new SspAccountSearchDto(); //FillSspSearchDto(); 
                FinanceAccountSearchDto finaneAccountSearchDto = new FinanceAccountSearchDto();  //FillInvestmentSearchDto

                if (productType == ProductType.Deposit)
                {
                    accountSearchDto = FillAccountSearchDto();
                }
                if (productType == ProductType.SSP)
                {
                    sspAccountSearchDto = FillSspSearchDto();
                }
                if (productType == ProductType.MTDR)
                {
                    mtdAccountSearchDto = FillMTDSearchDto();
                }
                if (productType == ProductType.INVESTMENT)
                {
                    finaneAccountSearchDto = FillInvestmentSearchDto();
                }

                ServiceResult result1 = new ServiceResult();

                if (accountSearchDto.product == null)
                {
                    try
                    {
                        if (productType == ProductType.Deposit)
                        {
                            result1 = AccountOpeningReportService.GetAccountOpeningList(accountSearchDto);
                        }
                        if (productType == ProductType.SSP)
                        {
                            result1 = AccountOpeningReportService.GetAccountOpeningList(sspAccountSearchDto, productType);
                        }
                        if (productType == ProductType.MTDR)
                        {
                            result1 = AccountOpeningReportService.GetAccountOpeningList(mtdAccountSearchDto, productType);
                        }
                        if(productType == ProductType.INVESTMENT)
                        {
                            result1 = AccountOpeningReportService.GetAccountOpeningList(finaneAccountSearchDto, productType);
                        }
                        if (!result1.Success)
                        {
                            MessageBox.Show(result1.Message);
                            return;
                        }

                        List<AccountBlanceDto> accountOpenings = null;

                        accountOpenings = result1.ReturnedObject as List<AccountBlanceDto>;



                        if (accountOpenings != null)
                        {
                            foreach (AccountBlanceDto balanceDto in accountOpenings)
                            {
                                AccountOpeningReport.AccountOpeningsRow row = accRep.AccountOpenings.NewAccountOpeningsRow();
                                row.AccountNumber = balanceDto.accountNumber;
                                row.AccountName = balanceDto.accTitle;
                                row.OpeningDate = UtilityServices.getDateFromLong(balanceDto.openDate);
                                row.ProductName = balanceDto.productTitle;

                                row.MobileNumber = balanceDto.mobileNumber;
                                row.AgentName = balanceDto.agentName;
                                row.AgentId = balanceDto.agentId;
                                row.SubAgentName = balanceDto.subAgentName;
                                row.SubAgentId = balanceDto.subAgentId;
                                row.Balance = balanceDto.balance ?? 0;
                                accRep.AccountOpenings.AddAccountOpeningsRow(row);

                            }
                            accRep.AcceptChanges();
                        }

                    }
                    catch (Exception)
                    {
                        //ignored
                    }
                }
                else
                {
                    if (productType == ProductType.Deposit)
                    {
                        result1 = AccountOpeningReportService.GetAccountOpeningList(accountSearchDto);
                    }
                    if (productType == ProductType.SSP)
                    {
                        result1 = AccountOpeningReportService.GetAccountOpeningList(sspAccountSearchDto, productType);
                    }
                    if (productType == ProductType.MTDR)
                    {
                        result1 = AccountOpeningReportService.GetAccountOpeningList(mtdAccountSearchDto, productType);
                    }
                    if(productType == ProductType.INVESTMENT)
                    {
                        result1 = AccountOpeningReportService.GetAccountOpeningList(finaneAccountSearchDto, productType);
                    }
                    if (!result1.Success)
                    {
                        MessageBox.Show(result1.Message);
                        return;
                    }

                    List<AccountBlanceDto> accountOpenings = result1.ReturnedObject as List<AccountBlanceDto>;

                    if (accountOpenings != null)
                    {
                        foreach (AccountBlanceDto balanceDto in accountOpenings)
                        {
                            AccountOpeningReport.AccountOpeningsRow row = accRep.AccountOpenings.NewAccountOpeningsRow();
                            row.AccountNumber = balanceDto.accountNumber;
                            row.AccountName = balanceDto.accTitle;
                            row.OpeningDate = UtilityServices.getDateFromLong(balanceDto.openDate);
                            row.ProductName = balanceDto.productTitle;

                            row.MobileNumber = balanceDto.mobileNumber;
                            row.AgentName = balanceDto.agentName;
                            row.AgentId = balanceDto.agentId;
                            row.SubAgentName = balanceDto.subAgentName;
                            row.SubAgentId = balanceDto.subAgentId;

                            row.Balance = balanceDto.balance ?? 0;
                            accRep.AccountOpenings.AddAccountOpeningsRow(row);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
        }

        private FinanceAccountSearchDto FillInvestmentSearchDto()
        {
            FinanceAccountSearchDto financeaccountSearchDto = new FinanceAccountSearchDto();

            if (this.cmbAgentName.SelectedIndex > -1)
            {
                if (cmbAgentName.SelectedIndex != 0 || cmbAgentName.SelectedIndex != 1)
                {
                    financeaccountSearchDto.agentId = (long)cmbAgentName.SelectedValue;
                }
                if (cmbAgentName.SelectedIndex == 1 || cmbAgentName.SelectedIndex == 0)
                {
                    financeaccountSearchDto.agentId = null; 
                }

            }
            if (this.cmbSubAgnetName.SelectedIndex > -1)
            {
                if (cmbSubAgnetName.SelectedIndex != 0 || cmbSubAgnetName.SelectedIndex != 1)
                {
                    financeaccountSearchDto.outletId = (long)cmbSubAgnetName.SelectedValue;
                }
                if (cmbSubAgnetName.SelectedIndex == 1)
                {
                    financeaccountSearchDto.outletId = null;
                }

            }
            if (this.cmbProduct.SelectedIndex > -1)
            {
                if (cmbProduct.SelectedIndex != 0 && cmbProduct.SelectedIndex != 1)
                {
                    financeaccountSearchDto.financeProduct = new FinanceProduct { cbsId = (long)(cmbProduct.SelectedValue) };
                }
                if (cmbProduct.SelectedIndex == 1)
                {
                    financeaccountSearchDto.financeProduct = null; // new AgentProduct();
                }
                if (cmbProduct.SelectedIndex == 0)
                {
                    //MessageBox.Show("Please select an option!", "Selection", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    //return null;
                }
            }

            financeaccountSearchDto.fromDate = UtilityServices.GetLongDate
            (
                DateTime.ParseExact(dtpFromDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture)
            );

            financeaccountSearchDto.toDate = UtilityServices.GetLongDate
            (
                DateTime.ParseExact(dtpToDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture)
            );

            return financeaccountSearchDto;
        }

        private void LoadAccountOpeningInformation()
        {
            AccountOpeningInformation accountOpeningInformation = new AccountOpeningInformation();
            _accountInformations.Clear();
            AccountSearchDto accountSearchDto = FillAccountSearchDto();
            ServiceResult result1;

            if (accountSearchDto.product == null)
            {
                try
                {
                    #region OLD
                    // accountSearchDto.product = new AgentProduct {id = _agentProducts[2].id};
                    // result1 = AccountOpeningReportService.GetAccountOpeningList(accountSearchDto);
                    // accountSearchDto.product = new AgentProduct { id = _agentProducts[3].id };
                    // ServiceResult result2 = AccountOpeningReportService.GetAccountOpeningList(accountSearchDto);
                    // if (!result1.Success || !result2.Success)
                    // {
                    //     MessageBox.Show(result1.Message + "\n" + result2.Message);
                    //     return;
                    // }

                    // List<AccountBlanceDto> accountOpenings = result1.ReturnedObject as List<AccountBlanceDto>;

                    // if (accountOpenings != null)
                    //     foreach (AccountBlanceDto balanceDto in accountOpenings)
                    //     {
                    //         accountOpeningInformation = new AccountOpeningInformation();
                    //         accountOpeningInformation.AccountNumber = balanceDto.accountNumber;
                    //         accountOpeningInformation.AccountName = balanceDto.accTitle;
                    //         accountOpeningInformation.OpeningDate = UtilityServices.getDateFromLong(balanceDto.openDate);
                    //         //accountOpeningInformation.ProductName = _agentProducts[2].productTitle;   // WALI

                    //         accountOpeningInformation.ProductName = balanceDto.productTitle;
                    //         accountOpeningInformation.MobileNumber = balanceDto.mobileNumber;
                    //         accountOpeningInformation.AgentName = balanceDto.agentName;
                    //         accountOpeningInformation.AgentId = balanceDto.agentId;
                    //         accountOpeningInformation.SubAgentName = balanceDto.subAgentName;
                    //         accountOpeningInformation.SubAgentId = balanceDto.subAgentId;

                    //         _accountInformations.Add(accountOpeningInformation);
                    //     }

                    //accountOpenings = result2.ReturnedObject as List<AccountBlanceDto>;

                    // if (accountOpenings != null)
                    //     foreach (AccountBlanceDto balanceDto in accountOpenings)
                    //     {
                    //         accountOpeningInformation = new AccountOpeningInformation();
                    //         accountOpeningInformation.AccountNumber = balanceDto.accountNumber;
                    //         accountOpeningInformation.AccountName = balanceDto.accTitle;
                    //         accountOpeningInformation.OpeningDate = UtilityServices.getDateFromLong(balanceDto.openDate);
                    //         accountOpeningInformation.ProductName = _agentProducts[3].productTitle;
                    //         _accountInformations.Add(accountOpeningInformation);
                    //     }
                    #endregion

                    //accountSearchDto.product = new AgentProduct { id = _agentProducts[2].id };                    
                    result1 = AccountOpeningReportService.GetAccountOpeningList(accountSearchDto);
                    if (!result1.Success)
                    {
                        MessageBox.Show(result1.Message);
                        return;
                    }

                    List<AccountBlanceDto> accountOpenings = result1.ReturnedObject as List<AccountBlanceDto>;

                    if (accountOpenings != null)
                        foreach (AccountBlanceDto balanceDto in accountOpenings)
                        {
                            accountOpeningInformation = new AccountOpeningInformation();
                            accountOpeningInformation.AccountNumber = balanceDto.accountNumber;
                            accountOpeningInformation.AccountName = balanceDto.accTitle;
                            accountOpeningInformation.OpeningDate = UtilityServices.getDateFromLong(balanceDto.openDate);
                            //accountOpeningInformation.ProductName = _agentProducts[2].productTitle;   // WALI

                            accountOpeningInformation.ProductName = balanceDto.productTitle;
                            accountOpeningInformation.MobileNumber = balanceDto.mobileNumber;
                            accountOpeningInformation.AgentName = balanceDto.agentName;
                            accountOpeningInformation.AgentId = balanceDto.agentId;
                            accountOpeningInformation.SubAgentName = balanceDto.subAgentName;
                            accountOpeningInformation.SubAgentId = balanceDto.subAgentId;
                            accountOpeningInformation.balance = Convert.ToDecimal(balanceDto.balance);
                            _accountInformations.Add(accountOpeningInformation);
                        }
                }
                catch (Exception)
                {
                    //ignored
                }
            }
            else
            {
                result1 = AccountOpeningReportService.GetAccountOpeningList(accountSearchDto);

                if (!result1.Success)
                {
                    MessageBox.Show(result1.Message);
                    return;
                }

                List<AccountBlanceDto> accountOpenings = result1.ReturnedObject as List<AccountBlanceDto>;

                if (accountOpenings != null)
                    foreach (AccountBlanceDto balanceDto in accountOpenings)
                    {
                        accountOpeningInformation = new AccountOpeningInformation();
                        accountOpeningInformation.AccountNumber = balanceDto.accountNumber;
                        accountOpeningInformation.AccountName = balanceDto.accTitle;
                        accountOpeningInformation.OpeningDate = UtilityServices.getDateFromLong(balanceDto.openDate);
                        accountOpeningInformation.ProductName = cmbProduct.Text;
                        _accountInformations.Add(accountOpeningInformation);
                    }
            }
        }

        private AccountSearchDto FillAccountSearchDto()
        {
            AccountSearchDto accountSearchDto = new AccountSearchDto();

            if (this.cmbAgentName.SelectedIndex > -1)
            {
                if (cmbAgentName.SelectedIndex != 0 || cmbAgentName.SelectedIndex != 1)
                {
                    accountSearchDto.agent = new AgentInformation { id = (long)cmbAgentName.SelectedValue };
                }
                if (cmbAgentName.SelectedIndex == 1 || cmbAgentName.SelectedIndex == 0)
                {
                    accountSearchDto.agent = null; // new AgentInformation();
                }

            }
            if (this.cmbSubAgnetName.SelectedIndex > -1)
            {
                if (cmbSubAgnetName.SelectedIndex != 0 || cmbSubAgnetName.SelectedIndex != 1)
                {
                    accountSearchDto.subAgent = new SubAgentInformation { id = (long)cmbSubAgnetName.SelectedValue };
                }
                if (cmbSubAgnetName.SelectedIndex == 1)
                {
                    accountSearchDto.subAgent = null; // new SubAgentInformation();
                }

            }
            if (this.cmbProduct.SelectedIndex > -1)
            {
                if (cmbProduct.SelectedIndex != 0 || cmbProduct.SelectedIndex != 1)
                {
                    accountSearchDto.product = new AgentProduct { id = (long)(cmbProduct.SelectedValue) };
                }
                if (cmbProduct.SelectedIndex == 1)
                {
                    accountSearchDto.product = null; // new AgentProduct();
                }

            }

            accountSearchDto.fromDate = UtilityServices.GetLongDate
            (
                DateTime.ParseExact(dtpFromDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture)
            );

            accountSearchDto.toDate = UtilityServices.GetLongDate
            (
                DateTime.ParseExact(dtpToDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture)
            );


            return accountSearchDto;
        }
        private SspAccountSearchDto FillSspSearchDto()
        {
            SspAccountSearchDto accountSearchDto = new SspAccountSearchDto();

            if (this.cmbAgentName.SelectedIndex > -1)
            {
                if (cmbAgentName.SelectedIndex != 0 || cmbAgentName.SelectedIndex != 1)
                {
                    accountSearchDto.agent = new AgentInformation { id = (long)cmbAgentName.SelectedValue };
                }
                if (cmbAgentName.SelectedIndex == 1 || cmbAgentName.SelectedIndex == 0)
                {
                    accountSearchDto.agent = null; // new AgentInformation();
                }

            }
            if (this.cmbSubAgnetName.SelectedIndex > -1)
            {
                if (cmbSubAgnetName.SelectedIndex != 0 || cmbSubAgnetName.SelectedIndex != 1)
                {
                    accountSearchDto.subAgent = new SubAgentInformation { id = (long)cmbSubAgnetName.SelectedValue };
                }
                if (cmbSubAgnetName.SelectedIndex == 1)
                {
                    accountSearchDto.subAgent = null; // new SubAgentInformation();
                }


            }
            if (this.cmbProduct.SelectedIndex > -1)
            {
                if (cmbProduct.SelectedValue != null && (cmbProduct.SelectedIndex != 0 || cmbProduct.SelectedIndex != 1))
                {
                    //accountSearchDto.product = new AgentProduct { id = (long)(cmbProduct.SelectedValue) };
                    accountSearchDto.product = new SspProductType { id = (long)(cmbProduct.SelectedValue) };
                }
                if (cmbProduct.SelectedIndex == 1)
                {
                    accountSearchDto.product = null; // new AgentProduct();
                }

            }


            accountSearchDto.fromDate = UtilityServices.GetLongDate
            (
                DateTime.ParseExact(dtpFromDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture)
            );

            accountSearchDto.toDate = UtilityServices.GetLongDate
            (
                DateTime.ParseExact(dtpToDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture)
            );

            return accountSearchDto;
        }

        private SspAccountSearchDto FillMTDSearchDto()
        {
            SspAccountSearchDto accountSearchDto = new SspAccountSearchDto();

            if (this.cmbAgentName.SelectedIndex > -1)
            {
                if (cmbAgentName.SelectedIndex != 0 || cmbAgentName.SelectedIndex != 1)
                {
                    accountSearchDto.agent = new AgentInformation { id = (long)cmbAgentName.SelectedValue };
                }
                if (cmbAgentName.SelectedIndex == 1 || cmbAgentName.SelectedIndex == 0)
                {
                    accountSearchDto.agent = null; // new AgentInformation();
                }

            }
            if (this.cmbSubAgnetName.SelectedIndex > -1)
            {
                if (cmbSubAgnetName.SelectedIndex != 0 || cmbSubAgnetName.SelectedIndex != 1)
                {
                    accountSearchDto.subAgent = new SubAgentInformation { id = (long)cmbSubAgnetName.SelectedValue };
                }
                if (cmbSubAgnetName.SelectedIndex == 1)
                {
                    accountSearchDto.subAgent = null; // new SubAgentInformation();
                }

            }
            if (this.cmbProduct.SelectedIndex > -1)
            {
                if (cmbProduct.SelectedIndex != 0 && cmbProduct.SelectedIndex != 1)
                {
                    //accountSearchDto.product = new AgentProduct { id = (long)(cmbProduct.SelectedValue) };
                    accountSearchDto.product = new SspProductType { id = (long)(cmbProduct.SelectedValue) };
                }
                if (cmbProduct.SelectedIndex == 1)
                {
                    accountSearchDto.product = null; // new AgentProduct();
                }
                if (cmbProduct.SelectedIndex == 0)
                {
                    //MessageBox.Show("Please select an option!", "Selection", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    //return null;
                }
            }

            accountSearchDto.fromDate = UtilityServices.GetLongDate
            (
                DateTime.ParseExact(dtpFromDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture)
            );

            accountSearchDto.toDate = UtilityServices.GetLongDate
            (
                DateTime.ParseExact(dtpToDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture)
            );

            return accountSearchDto;
        }



        private void GetSetupData()
        {

            try
            {
                setAgentList();

                setProductList();

            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
        }
        private void setAgentList()
        {
            objAgentInfoList = objAgentServices.getAgentInfoBranchWise();
            BindingSource bs = new BindingSource();
            bs.DataSource = objAgentInfoList;

            AgentInformation agSelect = new AgentInformation();
            agSelect.businessName = "(Select)";
            objAgentInfoList.Insert(0, agSelect);
            AgentInformation agAll = new AgentInformation();
            agAll.businessName = "(All)";
            objAgentInfoList.Insert(1, agAll);


            UtilityServices.fillComboBox(cmbAgentName, bs, "businessName", "id");
            //            cmbAgentName.Text = "Select";
            cmbAgentName.SelectedIndex = 0;
        }
        private void setProductList()
        {
            BindingSource bs1 = new BindingSource();
            ServiceResult serviceResult = AccountOpeningReportService.GetAllAgentProducts();
            if (serviceResult.Success)
            {
                _agentProducts = serviceResult.ReturnedObject as List<AgentProduct>;
                bs1.DataSource = _agentProducts;

                AgentProduct apSelect = new AgentProduct();
                apSelect.productTitle = "(Select)";
                ((List<AgentProduct>)serviceResult.ReturnedObject).Insert(0, apSelect);

                AgentProduct apAll = new AgentProduct();
                apAll.productTitle = "(All)";
                ((List<AgentProduct>)serviceResult.ReturnedObject).Insert(1, apAll);

                UtilityServices.fillComboBox(cmbProduct, bs1, "productTitle", "id");
                //cmbProduct.DataSource = AccountOpeningReportService.GetAllAgentProducts();
                //Enum.GetValues(typeof(ProductType));
                cmbProduct.SelectedIndex = 0;
            }
        }
        private void cmbAgentName_SelectedIndexChanged(object sender, EventArgs e)
        {

            ComboBox cb = (ComboBox)sender;
            if (!cb.Focused)
            {
                return;
            }


            try
            {
                if (cmbAgentName.SelectedIndex > 1)
                {
                    //agentInformation = agentServices.getAgentInfoById(cmbAgentName.SelectedValue.ToString());
                    subAgentList = agentServices.GetSubagentsByAgentId((long)cmbAgentName.SelectedValue);         // WALI :: 14-Jan-2016
                }
                if (cmbAgentName.SelectedIndex == 1)
                {
                    //agentInformation = agentServices.getAgentInfoById(null);
                }
                if (cmbAgentName.SelectedIndex < 2)
                {
                    cmbSubAgnetName.DataSource = null;
                    cmbSubAgnetName.Items.Clear();
                    return;
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
            setSubagent();
        }
        private void setSubagent()
        {
            #region Commented :: WALI :: 14-Jan-2016
            //if (agentInformation != null)
            //{
            //    BindingSource bs = new BindingSource();
            //    bs.DataSource = agentInformation.subAgents;


            //    {
            //        try
            //        {
            //            SubAgentInformation saiSelect = new SubAgentInformation();
            //            saiSelect.name = "(Select)";

            //            SubAgentInformation saiAll = new SubAgentInformation();
            //            saiAll.name = "(All)";

            //            agentInformation.subAgents.Insert(0, saiSelect);
            //            agentInformation.subAgents.Insert(1, saiAll);
            //        }
            //        catch //suppressed
            //        {

            //        }
            //    }
            //    UtilityServices.fillComboBox(cmbSubAgnetName, bs, "name", "id");
            //    if (cmbSubAgnetName.Items.Count > 0)
            //    {
            //        cmbSubAgnetName.SelectedIndex = 0;
            //    }
            //}
            #endregion

            if (subAgentList != null)
            {
                BindingSource bs = new BindingSource();
                bs.DataSource = subAgentList;

                try
                {
                    SubAgentInformation saiSelect = new SubAgentInformation();
                    saiSelect.name = "(Select)";

                    SubAgentInformation saiAll = new SubAgentInformation();
                    saiAll.name = "(All)";

                    subAgentList.Insert(0, saiSelect);
                    subAgentList.Insert(1, saiAll);
                }
                catch (Exception exp) { }

                UtilityServices.fillComboBox(cmbSubAgnetName, bs, "name", "id");
                if (cmbSubAgnetName.Items.Count > 0)
                {
                    cmbSubAgnetName.SelectedIndex = 0;
                }
            }
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            //mtbFromDate.Text = dtpFromDate.Value.ToString("dd-MM-yyyy");
        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            //mtbToDate.Text = dtpToDate.Value.ToString("dd-MM-yyyy");
        }

        private void frmAccountOpeningRport_Load(object sender, EventArgs e)
        {
            dtpFromDate.Value = SessionInfo.currentDate;
            dtpToDate.Value = SessionInfo.currentDate;
        }

        private void mtbFromDate_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void rBtnDeposit_CheckedChanged(object sender, EventArgs e)
        {
            if (rBtnDeposit.Checked == true)
            {
                cmbProduct.Enabled = true;
                BindingSource bs1 = new BindingSource();
                ServiceResult serviceResult = AccountOpeningReportService.GetDepositProducts();
                if (serviceResult.Success)
                {
                    _agentProducts = serviceResult.ReturnedObject as List<AgentProduct>;

                    AgentProduct apSelect = new AgentProduct();
                    apSelect.productTitle = "(Select)";
                    _agentProducts = (List<AgentProduct>)serviceResult.ReturnedObject;
                    (_agentProducts).Insert(0, apSelect);

                    AgentProduct apAll = new AgentProduct();
                    apAll.productTitle = "(All)";
                    (_agentProducts).Insert(1, apAll);

                    bs1.DataSource = _agentProducts;

                    UtilityServices.fillComboBox(cmbProduct, bs1, "productTitle", "id");

                    cmbProduct.SelectedIndex = 0;
                }
            }
        }

        private void rBtnITDMTD_CheckedChanged(object sender, EventArgs e)
        {
            if (rBtnITDMTD.Checked == true)
            {
                cmbProduct.Enabled = true;
                BindingSource bs1 = new BindingSource();
                bs1.DataSource = null;
                //_itdProducts
                List<TermProductType> _itdProducts = _itdProducts = LocalCache.GetITDProducts();
                {
                    if (_itdProducts.Count > 0)
                    {
                        if (_itdProducts[0].productDescription != "(Select)")
                        {
                            TermProductType apSelect = new TermProductType();
                            apSelect.productDescription = "(Select)";
                            (_itdProducts).Insert(0, apSelect);
                        }
                        if (_itdProducts[1].productDescription != "(All)")
                        {
                            TermProductType apAll = new TermProductType();
                            apAll.productDescription = "(All)";
                            (_itdProducts).Insert(1, apAll);
                        }
                    }
                }
                if (
                        _itdProducts[3].productDescription == "(Select)"
                    ||
                        _itdProducts[3].productDescription == "(All)"

                    )
                {
                    _itdProducts.RemoveAt(3);
                }
                if (
                        _itdProducts[2].productDescription == "(Select)"
                    ||
                        _itdProducts[2].productDescription == "(All)"

                    )
                {
                    _itdProducts.RemoveAt(2);
                }
                bs1.DataSource = _itdProducts;

                UtilityServices.fillComboBox(cmbProduct, bs1, "productDescription", "id");
                cmbProduct.SelectedIndex = 0;
            }
            #region---comments------
            //if (rBtnITDMTD.Checked == true)
            //{
            //    cmbProduct.Enabled = true;
            //    BindingSource bs1 = new BindingSource();
            //    ServiceResult serviceResult = AccountOpeningReportService.GetSchemeProducts();
            //    if (serviceResult.Success)
            //    {
            //        _agentSspProducts = serviceResult.ReturnedObject as List<SspProductType>;

            //        SspProductType apSelect = new SspProductType();
            //        apSelect.productDescription = "(Select)";
            //        _agentSspProducts = (List<SspProductType>)serviceResult.ReturnedObject;
            //        (_agentSspProducts).Insert(0, apSelect);

            //        SspProductType apAll = new SspProductType();
            //        apAll.productDescription = "(All)";
            //        (_agentSspProducts).Insert(1, apAll);

            //        bs1.DataSource = _agentSspProducts;

            //        UtilityServices.fillComboBox(cmbProduct, bs1, "productDescription", "id");

            //        cmbProduct.SelectedIndex = 0;
            //    }
            //}
            #endregion
        }

        private void rBtnMTD_CheckedChanged(object sender, EventArgs e)
        {
            if (rBtnMTD.Checked == true)
            {
                cmbProduct.Enabled = true;
                BindingSource bs1 = new BindingSource();
                bs1.DataSource = null;
                //_mtdProducts
                List<TermProductType> _mtdProducts = LocalCache.GetMTDProducts();
                {
                    if (_mtdProducts.Count > 0)
                    {
                        if (_mtdProducts[0].productDescription != "(Select)")
                        {
                            TermProductType apSelect = new TermProductType();
                            apSelect.productDescription = "(Select)";
                            (_mtdProducts).Insert(0, apSelect);
                        }
                        if (_mtdProducts[1].productDescription != "(All)")
                        {
                            TermProductType apAll = new TermProductType();
                            apAll.productDescription = "(All)";
                            (_mtdProducts).Insert(1, apAll);
                        }
                        if (_mtdProducts.Count > 3)
                        {
                            if (
                                  _mtdProducts[3].productDescription == "(Select)"
                              ||
                                  _mtdProducts[3].productDescription == "(All)"

                              )
                            {
                                _mtdProducts.RemoveAt(3);
                            }
                        }
                        if (_mtdProducts.Count > 2)
                        {
                            if (
                                _mtdProducts[2].productDescription == "(Select)"
                            ||
                                _mtdProducts[2].productDescription == "(All)"

                            )
                            {
                                _mtdProducts.RemoveAt(2);
                            }
                        }
                    }
                }
                bs1.DataSource = _mtdProducts;
                if (_mtdProducts.Count > 0)
                {
                    cmbProduct.DataSource = null;
                    UtilityServices.fillComboBox(cmbProduct, bs1, "productDescription", "id");
                    cmbProduct.SelectedIndex = 0;
                }
            }
        }

        private void frmAccountOpeningRport_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void rBtnInvestment_CheckedChanged(object sender, EventArgs e)
        {
            if (rBtnInvestment.Checked == true)
            {
                cmbProduct.Enabled = true;
                BindingSource bs1 = new BindingSource();
                ServiceResult serviceResult = AccountOpeningReportService.GetInvestmentProducts();
                if (serviceResult.Success)
                {
                    _financeProducts = serviceResult.ReturnedObject as List<FinanceProduct>;

                    FinanceProduct fpSelect = new FinanceProduct();
                    fpSelect.productName = "(Select)";
                    _financeProducts = (List<FinanceProduct>)serviceResult.ReturnedObject;
                    (_financeProducts).Insert(0, fpSelect);

                    FinanceProduct fpAll = new FinanceProduct();
                    fpAll.productName = "(All)";
                    (_financeProducts).Insert(1, fpAll);

                    bs1.DataSource = _financeProducts;

                    UtilityServices.fillComboBox(cmbProduct, bs1, "productName", "cbsId");

                    cmbProduct.SelectedIndex = 0;
                }
            }
        }
    }

    public class AccountOpeningInformation
    {
        public string AccountNumber = "";
        public string AccountName = "";
        public DateTime OpeningDate = DateTime.Now;
        public string ProductName = "";
        public string MobileNumber = "";
        public string AgentName = "";
        public string SubAgentName = "";
        public long AgentId = 0;
        public long SubAgentId = 0;
        public decimal balance = 0;
    }
}
