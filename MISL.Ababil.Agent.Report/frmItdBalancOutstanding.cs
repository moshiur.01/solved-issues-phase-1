﻿using CrystalDecisions.CrystalReports.Engine;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.termaccount;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.LocalStorageService;
using MISL.Ababil.Agent.Module.Common.UI.MessageUI;
using MISL.Ababil.Agent.Report.DataSets;
using MISL.Ababil.Agent.Report.Reports;
using MISL.Ababil.Agent.Services;
using System.Collections.Generic;
using System.Windows.Forms;
using System;

namespace MISL.Ababil.Agent.Report
{
    public partial class frmItdBalancOutstanding : CustomForm
    {
        List<AgentInformation> objAgentInfoList = new List<AgentInformation>();
        AgentServices agentServices = new AgentServices();
        AgentInformation agentInformation = new AgentInformation();
        TransactionService transactionService = new TransactionService();
        ServiceResult result;
        List<ItdAccReportResultDto> _itdPResult = new List<ItdAccReportResultDto>();
        ItdInformationDS itdInfoDS = new ItdInformationDS();

        public frmItdBalancOutstanding()
        {
            InitializeComponent();
            LoadSetupData();
            controlActivity();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool checkVelidetion()
        {
            if (cmbAgentName.SelectedIndex <= 0)
            {
                MsgBox.showWarning("Please select an Agent!");
                return false;
            }
            return true;
        }
        private void btnViewReport_Click(object sender, EventArgs e)
        {
            if (checkVelidetion())
            {

                try
                {
                    crITDBalanceOutStanding report = new crITDBalanceOutStanding();
                    frmReportViewer viewer = new frmReportViewer();

                    ReportHeaders rptHeaders = new ReportHeaders();
                    rptHeaders = UtilityServices.getReportHeaders("ITD Outstanding Report");
                    TextObject txtBankName = report.ReportDefinition.ReportObjects["txtBankName"] as TextObject;
                    TextObject txtBranchName = report.ReportDefinition.ReportObjects["txtBranchName"] as TextObject;
                    TextObject txtBranchAddress = report.ReportDefinition.ReportObjects["txtBranchAddress"] as TextObject;
                    TextObject txtReportHeading = report.ReportDefinition.ReportObjects["txtReportHeading"] as TextObject;
                    TextObject txtPrintUser = report.ReportDefinition.ReportObjects["txtPrintUser"] as TextObject;
                    TextObject txtPrintDate = report.ReportDefinition.ReportObjects["txtPrintDate"] as TextObject;
                    // TextObject txtFromDate = report.ReportDefinition.ReportObjects["txtFromDate"] as TextObject;
                    // TextObject txtToDate = report.ReportDefinition.ReportObjects["txtToDate"] as TextObject;

                    if (rptHeaders != null)
                    {
                        if (rptHeaders.branchDto != null)
                        {
                            txtBankName.Text = rptHeaders.branchDto.bankName;
                            txtBranchName.Text = rptHeaders.branchDto.branchName;
                            txtBranchAddress.Text = rptHeaders.branchDto.branchAddress;
                        }

                        txtReportHeading.Text = rptHeaders.reportHeading;
                        txtPrintUser.Text = rptHeaders.printUser;
                        txtPrintDate.Text = rptHeaders.printDate.ToString("dd-MM-yyyy").Replace("-", "/");
                    }

                    LoadItdBalanceOutstandingData();

                    report.SetDataSource(itdInfoDS);

                    viewer.crvReportViewer.ReportSource = report;
                    viewer.Show(this.Parent);
                }
                catch (Exception ex)
                {

                    MsgBox.ShowError("Report loding problem\n" + ex.Message);
                }
            }
        }
        private void LoadSetupData()
        {
            try
            {

                objAgentInfoList = new List<AgentInformation>(agentServices.getAgentInfoBranchWise()); ;
                BindingSource bsAgent = new BindingSource();
                bsAgent.DataSource = objAgentInfoList;

                AgentInformation agSelect = new AgentInformation();
                agSelect.businessName = "(Select)";
                objAgentInfoList.Insert(0, agSelect);
                AgentInformation agAll = new AgentInformation();
                agAll.businessName = "(All)";
                objAgentInfoList.Insert(1, agAll);

                UtilityServices.fillComboBox(cmbAgentName, bsAgent, "businessName", "id");
                //cmbAgentName.Text = "Select";
                cmbAgentName.SelectedIndex = 0;


                List<TermProductType> _itdProducts = new List<TermProductType>(LocalCache.GetITDProducts());
                TermProductType termSelect = new TermProductType();
                termSelect.productDescription = "(All)";
                _itdProducts.Insert(0, termSelect);
                BindingSource bsTermProductTmp = new BindingSource();
                bsTermProductTmp.DataSource = _itdProducts;
                //cmbProducts.DataSource = bsTermProductTmp;
                UtilityServices.fillComboBox(cmbProducts, bsTermProductTmp, "productDescription", "id");
                cmbProducts.SelectedIndex = 0;
            }
            catch (Exception ex)
            { MsgBox.ShowError(ex.Message); }
        }
        private void controlActivity()
        {
            try
            {
                //if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_CENTRALLY.ToString())) //branch user
                if (SessionInfo.userBasicInformation.userCategory == UserCategory.BranchUser) //branch user
                {
                    cmbAgentName.Enabled = true;
                    cmbSubAgnetName.Enabled = true;
                }
                //else if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_AGENTWISE.ToString())) //agent user
                else if (SessionInfo.userBasicInformation.userCategory == UserCategory.AgentUser) //branch user
                {
                    cmbAgentName.SelectedValue = UtilityServices.getCurrentAgent().id;
                    cmbAgentName.Enabled = false;
                    cmbSubAgnetName.Enabled = true;
                    agentInformation = agentServices.getAgentInfoById(UtilityServices.getCurrentAgent().id.ToString());
                    setSubagent();
                }
                else
                {
                    SubAgentInformation currentSubagentInfo = UtilityServices.getCurrentSubAgent();
                    cmbAgentName.SelectedValue = currentSubagentInfo.agent.id;
                    agentInformation = agentServices.getAgentInfoById(currentSubagentInfo.agent.id.ToString());
                    setSubagent();
                    cmbAgentName.Enabled = false;
                    cmbSubAgnetName.SelectedValue = currentSubagentInfo.id;
                    cmbSubAgnetName.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }
        private void setSubagent()
        {
            if (agentInformation != null)
            {
                BindingSource bs = new BindingSource();
                bs.DataSource = agentInformation.subAgents;

                try
                {
                    SubAgentInformation saiSelect = new SubAgentInformation();
                    saiSelect.name = "(Select)";

                    SubAgentInformation saiAll = new SubAgentInformation();
                    saiAll.name = "(All)";

                    agentInformation.subAgents.Insert(0, saiSelect);
                    agentInformation.subAgents.Insert(1, saiAll);
                }
                catch (Exception exp)
                { }
                UtilityServices.fillComboBox(cmbSubAgnetName, bs, "name", "id");
                if (cmbSubAgnetName.Items.Count > 0)
                { cmbSubAgnetName.SelectedIndex = 0; }
            }
        }

        private void frmItdBalancOutstanding_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void cmbAgentName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmbAgentName.SelectedIndex > 1)
                {
                    agentInformation = agentServices.getAgentInfoById(cmbAgentName.SelectedValue.ToString());
                }
                if (cmbAgentName.SelectedIndex < 2)
                {
                    cmbSubAgnetName.DataSource = null;
                    cmbSubAgnetName.Items.Clear();
                    return;
                }
                setSubagent();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }

        }

        private void LoadItdBalanceOutstandingData()
        {
            ItdAccReportSearchDto itdRepSearchDto = FillItdBalanceSearchDto();
            itdInfoDS = new ItdInformationDS();
            try
            {
                ReportService objRep = new ReportService();
                result = objRep.getOutletInformationReportList(itdRepSearchDto);
                if (!result.Success)
                {
                    MsgBox.ShowError(result.Message);
                    return;
                }
                List<ItdAccReportResultDto> itdReportList = result.ReturnedObject as List<ItdAccReportResultDto>;

                if (itdReportList != null)
                {
                    foreach (ItdAccReportResultDto itdRport in itdReportList)
                    {
                        ItdInformationDS.ItdOutStandingDTRow newRow = itdInfoDS.ItdOutStandingDT.NewItdOutStandingDTRow();
                        newRow.agentId = itdRport.agentId;
                        newRow.outletId = itdRport.outletId;
                        newRow.agentName = itdRport.agentName;
                        newRow.outletName = itdRport.outletName;
                        newRow.productId = itdRport.productId;
                        newRow.productName = itdRport.productName;
                        newRow.accountNo = itdRport.accountNo;
                        newRow.accountTitle = itdRport.accountTitle;
                        newRow.dueDate = itdRport.dueDate;
                        newRow.dueInstallmentNo = itdRport.noOfDueInstallment ?? 0;
                        newRow.mobileNo = itdRport.mobileNo;
                        newRow.balance = itdRport.balance;
                        newRow.openingDateValue = itdRport.openDate;
                        newRow.installmentAmountValue = itdRport.installmentAmount.ToString();

                        itdInfoDS.ItdOutStandingDT.AddItdOutStandingDTRow(newRow);
                    }
                }
                itdInfoDS.AcceptChanges();

            }
            catch (Exception exp)
            {
                MsgBox.ShowError("Report data loading error.\n" + exp.Message);

            }
        }
        private ItdAccReportSearchDto FillItdBalanceSearchDto()
        {
            ItdAccReportSearchDto itdAccountSearchDto = new ItdAccReportSearchDto();
            try
            {



                if (cmbAgentName.SelectedIndex != 0 && cmbAgentName.SelectedIndex != 1)
                {
                    itdAccountSearchDto.agentId = (long)cmbAgentName.SelectedValue;
                }
                else if (cmbAgentName.SelectedIndex == 1 || cmbAgentName.SelectedIndex == 0)
                {
                    itdAccountSearchDto.agentId = 0;
                }




                if (cmbSubAgnetName.SelectedIndex > 1)
                {
                    itdAccountSearchDto.outletId = (long)cmbSubAgnetName.SelectedValue;
                }
                else
                {
                    itdAccountSearchDto.outletId = 0;
                }

                if (cmbProducts.SelectedIndex != 0)
                {
                    itdAccountSearchDto.productId = (long)cmbProducts.SelectedValue;
                }
                else itdAccountSearchDto.productId = 0;

                return itdAccountSearchDto;
            }
            catch (Exception exp)
            { MsgBox.ShowError("Error filling search object.\n" + exp.Message); return null; }
        }



    }

}
