﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Infrastructure.Models.models.transaction;
using MISL.Ababil.Agent.Infrastructure.Validation;
using MISL.Ababil.Agent.Report.Reports;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Report.DataSets;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using MetroFramework.Forms;
using System.Globalization;
using MISL.Ababil.Agent.Common.UI;
using MISL.Ababil.Agent.Module.Common.UI.MessageUI;

namespace MISL.Ababil.Agent.Report
{
    public partial class frmDailyPositionReport : MetroForm
    {
        public GUI gui = new GUI();
        List<AgentInformation> objAgentInfoList = new List<AgentInformation>();
        AgentServices objAgentServices = new AgentServices();
        AgentInformation agentInformation = new AgentInformation();
        AgentServices agentServices = new AgentServices();

        DailyPositionReportDs agentDS = new DailyPositionReportDs();

        public List<AccountMonitoringInfo> _accountMonitoringList = new List<AccountMonitoringInfo>();
        private List<AgentProduct> _agentProducts;

        List<SubAgentInformation> subAgentList = new List<SubAgentInformation>();       // WALI :: 14-Jan-2016

        public frmDailyPositionReport()
        {
            InitializeComponent();
            GetSetupData();
            try
            {
                //cmbAgentName.SelectedIndex = 0;
                cmbAgentName.SelectedIndex = -1;
            }
            catch (Exception exp)
            { }
            controlActivity();
            ConfigUIEnhancement();
        }

        public void ConfigUIEnhancement()
        {
            gui = new GUI(this);
            gui.Config(ref cmbAgentName, ValidCheck.VALIDATIONTYPES.COMBOBOX_EMPTY, null);
            gui.Config(ref cmbSubAgnetName);
            gui.Config(ref dtpFromDate);
            gui.Config(ref dtpToDate);
        }

        private void controlActivity()
        {
            try
            {
                if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_CENTRALLY.ToString())) //branch user
                {
                    cmbAgentName.Enabled = true;
                    cmbSubAgnetName.Enabled = true;
                }
                else if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_AGENTWISE.ToString())) //agent user
                {
                    cmbAgentName.SelectedValue = UtilityServices.getCurrentAgent().id;
                    cmbAgentName.Enabled = false;
                    cmbSubAgnetName.Enabled = true;
                    //agentInformation = agentServices.getAgentInfoById(UtilityServices.getCurrentAgent().id.ToString());
                    subAgentList = agentServices.GetSubagentsByAgentId(UtilityServices.getCurrentAgent().id);                  // WALI :: 14-Jan-2016
                    setSubagent();
                }
                else
                {
                    SubAgentInformation currentSubagentInfo = UtilityServices.getCurrentSubAgent();
                    cmbAgentName.SelectedValue = currentSubagentInfo.agent.id;
                    //agentInformation = agentServices.getAgentInfoById(currentSubagentInfo.agent.id.ToString());
                    subAgentList = agentServices.GetSubagentsByAgentId(UtilityServices.getCurrentAgent().id);                  // WALI :: 14-Jan-2016
                    setSubagent();
                    cmbAgentName.Enabled = false;
                    cmbSubAgnetName.SelectedValue = currentSubagentInfo.id;
                    cmbSubAgnetName.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {

            //chacking valid date
            DateTime fromDate, toDate;
            try
            {
                //fromDate = UtilityServices.ParseDateTime(dtpFromDate.Date);                
                fromDate = DateTime.ParseExact(dtpFromDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
            }
            catch
            {
                CustomMessage.showWarning("Please enter the Date in correct format!!");
                return;
            }
            try
            {
                //toDate = UtilityServices.ParseDateTime(dtpToDate.Date);
                toDate = DateTime.ParseExact(dtpToDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
            }
            catch
            {
                CustomMessage.showWarning("Please enter the Date in correct format!!");
                return;
            }

            if (SessionInfo.currentDate < toDate)
            {
                CustomMessage.showWarning("Future date is not allowed!!");
                return;
            }
            if (fromDate > toDate)
            {
                CustomMessage.showWarning("From date can not be greater than to date!!.");
                return;
            }

            try
            {
                if (cmbAgentName.SelectedIndex > 0)
                {
                    crDailyPositionReport report = new crDailyPositionReport();
                    frmReportViewer viewer = new frmReportViewer();

                    ReportHeaders rptHeaders = new ReportHeaders();
                    rptHeaders = UtilityServices.getReportHeaders("Daily Position Report");

                    TextObject txtBankName = report.ReportDefinition.ReportObjects["txtBankName"] as TextObject;
                    TextObject txtBranchName = report.ReportDefinition.ReportObjects["txtBranchName"] as TextObject;
                    TextObject txtBranchAddress = report.ReportDefinition.ReportObjects["txtBranchAddress"] as TextObject;
                    TextObject txtReportHeading = report.ReportDefinition.ReportObjects["txtReportHeading"] as TextObject;
                    TextObject txtPrintUser = report.ReportDefinition.ReportObjects["txtPrintUser"] as TextObject;
                    TextObject txtPrintDate = report.ReportDefinition.ReportObjects["txtPrintDate"] as TextObject;
                    TextObject txtDate = report.ReportDefinition.ReportObjects["txtDate"] as TextObject;
                    //TextObject txtFromDate = report.ReportDefinition.ReportObjects["txtFromDate"] as TextObject;
                    //TextObject txtToDate = report.ReportDefinition.ReportObjects["txtToDate"] as TextObject;

                    if (rptHeaders != null)
                    {
                        if (rptHeaders.branchDto != null)
                        {
                            txtBankName.Text = rptHeaders.branchDto.bankName;
                            txtBranchName.Text = rptHeaders.branchDto.branchName;
                            txtBranchAddress.Text = rptHeaders.branchDto.branchAddress;
                        }
                        txtReportHeading.Text = rptHeaders.reportHeading;
                        txtPrintUser.Text = rptHeaders.printUser;
                        txtPrintDate.Text = rptHeaders.printDate.ToString("dd/MM/yyyy").Replace("-", "/");
                    }
                    //txtFromDate.Text = dtpFromDate.Date;
                    //txtToDate.Text = dtpToDate.Date;
                    txtDate.Text = dtpFromDate.Date;

                    LoadAccountMonitoring();
                    report.SetDataSource(agentDS);

                    viewer.crvReportViewer.ReportSource = report;
                    viewer.ShowDialog(this);
                }
                else
                {
                    CustomMessage.showWarning("Please select an option!");
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        private void LoadAccountMonitoring()
        {
            AccountMonitoringInfo accountMonitoringInfo;
            _accountMonitoringList.Clear();
            DailyPositionReportSearchDto accountSearchDto = FillAccountSearchDto();
            ServiceResult result;
            agentDS = new DailyPositionReportDs();

            try
            {
                result = AgentServices.GetDailyPositionReport(accountSearchDto);

                if (!result.Success)
                {
                    CustomMessage.showError(result.Message);
                    return;
                }

                List<DailyPositionReportResultDto> accountMonitoring = result.ReturnedObject as List<DailyPositionReportResultDto>;


                //accountMonitoring.Add(new DailyPositionReportResultDto
                //{
                //   agentId = 0.ToString(),
                //   agentName = "kaaskakk",
                //   todayDayAmount = "12365478",
                //   previousDayAmount = "85641256",
                //   todayDayAccOpen = "1000",
                //   previousDayAccOpen = "900",
                //   outletName = "kjlgldfk",
                //   openingDate = "01/01/2000",
                //   noOfDraft = "900",
                //   outletid = "009",
                //   agentBalance = "214569856"



                //});
                //accountMonitoring.Add(new DailyPositionReportResultDto
                //{
                //    agentId = 0.ToString(),
                //    agentName = "kaaskakk",
                //    todayDayAmount = "85641256",
                    
                //    previousDayAmount = "12365478",
                //    todayDayAccOpen = "900",
                //    previousDayAccOpen = "1000",
                //    outletName = "kjlgldfk",
                //    openingDate = "01/01/2000",
                //    noOfDraft = "900",
                //    outletid = "009",
                //    agentBalance = "214569856"



                //});

                if (accountMonitoring != null)
                {
                    int previousDayAccOpen = 0;
                    int todayDayAccOpen = 0;
                    Decimal previousDayAmount = 0;
                    Decimal todayDayAmount = 0;
                    foreach (DailyPositionReportResultDto account in accountMonitoring)
                    {
                        DailyPositionReportDs.DailyPositionReportDtRow newRow = agentDS.DailyPositionReportDt.NewDailyPositionReportDtRow();

                        if (account.agentBalance != null)
                        {
                            newRow.agentbalance = Convert.ToDouble((account.agentBalance));
                        }
                        else
                        {
                            newRow.agentbalance = 0;
                        }

                        newRow.agentId = account.agentId;
                        newRow.agentName = account.agentName;
                        newRow.outletId = account.outletid;
                        newRow.outletName = account.outletName;
                        if (account.openingDate != null)
                        {
                            newRow.openingDate = Convert.ToDateTime(account.openingDate).ToString("dd-MM-yyyy");
                        }
                        if (account.previousDayAccOpen != null)
                        {
                            newRow.prevAccOpening =Convert.ToInt64((account.previousDayAccOpen));
                        }
                        else
                        {
                            newRow.prevAccOpening = 0;
                        }
                        if (account.previousDayAmount != null)
                        {
                            newRow.prevAmount = Convert.ToDouble(account.previousDayAmount);
                        }
                        else
                        {
                            newRow.prevAmount = 0;
                        }

                        if (account.todayDayAccOpen != null)
                        {
                            newRow.todayAccopening = Convert.ToInt64(account.todayDayAccOpen);
                        }
                        else
                        {
                            newRow.todayAccopening = 0;

                        }
                        if (account.todayDayAmount != null)
                        {
                            newRow.todayAmount = Convert.ToDouble(account.todayDayAmount);
                        }
                        else
                        {
                            newRow.todayAmount = 0;
                        }

                        if (string.IsNullOrEmpty(account.previousDayAccOpen))
                        {
                            previousDayAccOpen = 0;
                        }
                        if (string.IsNullOrEmpty(account.previousDayAmount))
                        {
                            previousDayAmount = 0;
                        }
                        if (string.IsNullOrEmpty(account.todayDayAccOpen))
                        {
                            todayDayAccOpen = 0;
                        }
                        if (string.IsNullOrEmpty(account.todayDayAmount))
                        {
                            todayDayAmount = 0;
                        }
                        todayDayAccOpen = (int) newRow.todayAccopening;
                        newRow.incDecAcc =
                            (previousDayAccOpen - todayDayAccOpen);
                        newRow.incDecAmount =
                             (double) (previousDayAmount - todayDayAmount);
                        //if (newRow.noOfDraft != null)
                        //{
                            newRow.noOfDraft =account.noOfDraft;
                        //}
                        //else
                        //{
                        //    newRow.noOfDraft = "0";
                        //}
                        previousDayAccOpen = (int) newRow.prevAccOpening;


                        agentDS.DailyPositionReportDt.AddDailyPositionReportDtRow(newRow);
                    }
                }
                agentDS.AcceptChanges();
            }
            catch (Exception ex)
            {
                MsgBox.showWarning(ex.Message);//ignored
            }
        }

        private DailyPositionReportSearchDto FillAccountSearchDto()
        {
            DailyPositionReportSearchDto accountSearchDto = new DailyPositionReportSearchDto();

            if (this.cmbAgentName.SelectedIndex > -1)
            {
                if (cmbAgentName.SelectedIndex != 0 || cmbAgentName.SelectedIndex != 1)
                {
                    accountSearchDto.agentId = (long)cmbAgentName.SelectedValue;
                }
                if (cmbAgentName.SelectedIndex == 1 || cmbAgentName.SelectedIndex == 0)
                {
                    accountSearchDto.agentId = 0; // new AgentInformation();
                }
                if (cmbAgentName.SelectedIndex == 0)
                {
                }
            }
            if (this.cmbSubAgnetName.SelectedIndex > -1)
            {
                if (cmbSubAgnetName.SelectedIndex != 0 || cmbSubAgnetName.SelectedIndex != 1)
                {
                    accountSearchDto.outletId = (long)cmbSubAgnetName.SelectedValue;
                }
                if (cmbSubAgnetName.SelectedIndex == 1 || cmbSubAgnetName.SelectedIndex == 0)
                {
                    accountSearchDto.outletId = 0; // new SubAgentInformation();
                }
                if (cmbSubAgnetName.SelectedIndex == 0)
                {
                }
            }

            accountSearchDto.onDate = UtilityServices.GetLongDate
            (
                DateTime.ParseExact(dtpFromDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture)
            );

            //accountSearchDto.toDate = UtilityServices.GetLongDate
            //(
            //    DateTime.ParseExact(dtpToDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture)
            //);

            return accountSearchDto;
        }

        private void GetSetupData()
        {

            try
            {
                setAgentList();
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
        }
        private void setAgentList()
        {
            objAgentInfoList = objAgentServices.getAgentInfoBranchWise();
            BindingSource bs = new BindingSource();
            bs.DataSource = objAgentInfoList;

            AgentInformation agSelect = new AgentInformation();
            agSelect.businessName = "(Select)";
            objAgentInfoList.Insert(0, agSelect);
            AgentInformation agAll = new AgentInformation();
            agAll.businessName = "(All)";
            objAgentInfoList.Insert(1, agAll);

            UtilityServices.fillComboBox(cmbAgentName, bs, "businessName", "id");
            //            cmbAgentName.Text = "Select";
            cmbAgentName.SelectedIndex = 0;
        }
        private void cmbAgentName_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!cb.Focused)
            {
                return;
            }

            try
            {
                if (cmbAgentName.SelectedIndex > 1)
                {
                    //agentInformation = agentServices.getAgentInfoById(cmbAgentName.SelectedValue.ToString());
                    subAgentList = agentServices.GetSubagentsByAgentId(long.Parse(cmbAgentName.SelectedValue.ToString()));         // WALI :: 14-Jan-2016
                }
                if (cmbAgentName.SelectedIndex == 1)
                {
                    //agentInformation = agentServices.getAgentInfoById(null);
                }
                if (cmbAgentName.SelectedIndex < 2)
                {
                    cmbSubAgnetName.DataSource = null;
                    cmbSubAgnetName.Items.Clear();
                    return;
                }
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
            setSubagent();
        }
        private void setSubagent()
        {
            #region WALI :: Commented :: 14-Jan-2016
            //if (agentInformation != null)
            //{
            //    BindingSource bs = new BindingSource();
            //    bs.DataSource = agentInformation.subAgents;

            //    try
            //    {
            //        SubAgentInformation saiSelect = new SubAgentInformation();
            //        saiSelect.name = "(Select)";

            //        SubAgentInformation saiAll = new SubAgentInformation();
            //        saiAll.name = "(All)";

            //        agentInformation.subAgents.Insert(0, saiSelect);
            //        agentInformation.subAgents.Insert(1, saiAll);
            //    }
            //    catch (Exception exp)
            //    { }

            //    UtilityServices.fillComboBox(cmbSubAgnetName, bs, "name", "id");
            //    if (cmbSubAgnetName.Items.Count > 0)
            //    {
            //        cmbSubAgnetName.SelectedIndex = 0;
            //    }
            //}
            #endregion

            if (subAgentList != null)
            {
                BindingSource bs = new BindingSource();
                bs.DataSource = subAgentList;

                try
                {
                    SubAgentInformation saiSelect = new SubAgentInformation();
                    saiSelect.name = "(Select)";

                    SubAgentInformation saiAll = new SubAgentInformation();
                    saiAll.name = "(All)";

                    subAgentList.Insert(0, saiSelect);
                    subAgentList.Insert(1, saiAll);
                }
                catch (Exception exp)
                { }

                UtilityServices.fillComboBox(cmbSubAgnetName, bs, "name", "id");
                if (cmbSubAgnetName.Items.Count > 0)
                {
                    cmbSubAgnetName.SelectedIndex = 0;
                }
            }
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {

        }



        private void frmTrMonitoringRport_Load(object sender, EventArgs e)
        {
            dtpFromDate.Value = SessionInfo.currentDate;
            dtpToDate.Value = SessionInfo.currentDate;
        }

        private void frmAccountMonitoringRport_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }
    }
    //public class AccountMonitoringInfo
    //{
    //    public long AgentId = 0;
    //    public string AgentName = "";
    //    public long SubAgentId = 0;
    //    public string SubAgentName = "";
    //    public decimal? AgentAccBalance = 0;

    //    public long NoOfMSDAccount = 0;
    //    public decimal MSDAccBalance = 0;

    //    public long NoOfCDAccount = 0;
    //    public decimal CDAccBalance = 0;

    //    public long NoOfITDAccount = 0;
    //    public decimal ITDAccBalance = 0;

    //    public long NoOfMTDAccount = 0;
    //    public decimal MTDAccBalance = 0;

    //    public long NoOfInvestmentAccount = 0;
    //    public decimal InvestedAmount = 0;
    //}


}
