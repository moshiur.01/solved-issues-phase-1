﻿using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.Configuration.UI.forms
{
    partial class frmDepositMapping
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.cmbProductList = new MetroFramework.Controls.MetroComboBox();
            this.txtProductPrefix = new MetroFramework.Controls.MetroTextBox();
            this.txtProductName = new MetroFramework.Controls.MetroTextBox();
            this.txtOpeningDeposit = new MetroFramework.Controls.MetroTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbAccountCategory = new MetroFramework.Controls.MetroComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.btnClose = new System.Windows.Forms.Button();
            this.txtOpeningDepositGL = new MetroFramework.Controls.MetroTextBox();
            this.mandatoryMark1 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark2 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark3 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark4 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark5 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.cmbAccType = new MetroFramework.Controls.MetroComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(35, 62);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(55, 19);
            this.metroLabel1.TabIndex = 101;
            this.metroLabel1.Text = "Product";
            // 
            // cmbProductList
            // 
            this.cmbProductList.FormattingEnabled = true;
            this.cmbProductList.ItemHeight = 23;
            this.cmbProductList.Location = new System.Drawing.Point(180, 53);
            this.cmbProductList.Name = "cmbProductList";
            this.cmbProductList.Size = new System.Drawing.Size(363, 29);
            this.cmbProductList.TabIndex = 1;
            this.cmbProductList.UseSelectable = true;
            this.cmbProductList.SelectedIndexChanged += new System.EventHandler(this.cmbProductList_SelectedIndexChanged);
            // 
            // txtProductPrefix
            // 
            this.txtProductPrefix.Lines = new string[0];
            this.txtProductPrefix.Location = new System.Drawing.Point(180, 90);
            this.txtProductPrefix.MaxLength = 32767;
            this.txtProductPrefix.Name = "txtProductPrefix";
            this.txtProductPrefix.PasswordChar = '\0';
            this.txtProductPrefix.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtProductPrefix.SelectedText = "";
            this.txtProductPrefix.Size = new System.Drawing.Size(133, 23);
            this.txtProductPrefix.TabIndex = 104;
            this.txtProductPrefix.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtProductPrefix.UseSelectable = true;
            // 
            // txtProductName
            // 
            this.txtProductName.Lines = new string[0];
            this.txtProductName.Location = new System.Drawing.Point(-7, 279);
            this.txtProductName.MaxLength = 32767;
            this.txtProductName.Name = "txtProductName";
            this.txtProductName.PasswordChar = '\0';
            this.txtProductName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtProductName.SelectedText = "";
            this.txtProductName.Size = new System.Drawing.Size(294, 23);
            this.txtProductName.TabIndex = 105;
            this.txtProductName.UseSelectable = true;
            this.txtProductName.Visible = false;
            // 
            // txtOpeningDeposit
            // 
            this.txtOpeningDeposit.Lines = new string[0];
            this.txtOpeningDeposit.Location = new System.Drawing.Point(180, 130);
            this.txtOpeningDeposit.MaxLength = 32767;
            this.txtOpeningDeposit.Name = "txtOpeningDeposit";
            this.txtOpeningDeposit.PasswordChar = '\0';
            this.txtOpeningDeposit.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtOpeningDeposit.SelectedText = "";
            this.txtOpeningDeposit.Size = new System.Drawing.Size(133, 23);
            this.txtOpeningDeposit.TabIndex = 2;
            this.txtOpeningDeposit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOpeningDeposit.UseSelectable = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(35, 163);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 16);
            this.label1.TabIndex = 108;
            this.label1.Text = "Opening Deposit GL";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(35, 135);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 16);
            this.label2.TabIndex = 109;
            this.label2.Text = "Opening Deposit";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(35, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 16);
            this.label3.TabIndex = 110;
            this.label3.Text = "Product Prefix";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(35, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 16);
            this.label4.TabIndex = 111;
            this.label4.Text = "Product";
            // 
            // cmbAccountCategory
            // 
            this.cmbAccountCategory.FormattingEnabled = true;
            this.cmbAccountCategory.ItemHeight = 23;
            this.cmbAccountCategory.Items.AddRange(new object[] {
            "Select",
            "MSD",
            "CD"});
            this.cmbAccountCategory.Location = new System.Drawing.Point(180, 186);
            this.cmbAccountCategory.Name = "cmbAccountCategory";
            this.cmbAccountCategory.Size = new System.Drawing.Size(133, 29);
            this.cmbAccountCategory.TabIndex = 4;
            this.cmbAccountCategory.UseSelectable = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(35, 193);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 16);
            this.label5.TabIndex = 113;
            this.label5.Text = "Account Category";
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(387, 268);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 26);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(573, 26);
            this.customTitlebar1.TabIndex = 100;
            this.customTitlebar1.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(468, 268);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 26);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtOpeningDepositGL
            // 
            this.txtOpeningDepositGL.Lines = new string[0];
            this.txtOpeningDepositGL.Location = new System.Drawing.Point(180, 159);
            this.txtOpeningDepositGL.MaxLength = 32767;
            this.txtOpeningDepositGL.Name = "txtOpeningDepositGL";
            this.txtOpeningDepositGL.PasswordChar = '\0';
            this.txtOpeningDepositGL.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtOpeningDepositGL.SelectedText = "";
            this.txtOpeningDepositGL.Size = new System.Drawing.Size(133, 23);
            this.txtOpeningDepositGL.TabIndex = 3;
            this.txtOpeningDepositGL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOpeningDepositGL.UseSelectable = true;
            // 
            // mandatoryMark1
            // 
            this.mandatoryMark1.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark1.Location = new System.Drawing.Point(320, 131);
            this.mandatoryMark1.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.Name = "mandatoryMark1";
            this.mandatoryMark1.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.TabIndex = 114;
            // 
            // mandatoryMark2
            // 
            this.mandatoryMark2.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark2.Location = new System.Drawing.Point(320, 161);
            this.mandatoryMark2.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.Name = "mandatoryMark2";
            this.mandatoryMark2.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.TabIndex = 115;
            // 
            // mandatoryMark3
            // 
            this.mandatoryMark3.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark3.Location = new System.Drawing.Point(549, 56);
            this.mandatoryMark3.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark3.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark3.Name = "mandatoryMark3";
            this.mandatoryMark3.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark3.TabIndex = 116;
            // 
            // mandatoryMark4
            // 
            this.mandatoryMark4.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark4.Location = new System.Drawing.Point(320, 190);
            this.mandatoryMark4.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark4.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark4.Name = "mandatoryMark4";
            this.mandatoryMark4.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark4.TabIndex = 117;
            // 
            // mandatoryMark5
            // 
            this.mandatoryMark5.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark5.Location = new System.Drawing.Point(320, 225);
            this.mandatoryMark5.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark5.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark5.Name = "mandatoryMark5";
            this.mandatoryMark5.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark5.TabIndex = 120;
            // 
            // cmbAccType
            // 
            this.cmbAccType.FormattingEnabled = true;
            this.cmbAccType.ItemHeight = 23;
            this.cmbAccType.Items.AddRange(new object[] {
            "Select",
            "CD",
            "MSD",
            "STDN"});
            this.cmbAccType.Location = new System.Drawing.Point(180, 221);
            this.cmbAccType.Name = "cmbAccType";
            this.cmbAccType.Size = new System.Drawing.Size(133, 29);
            this.cmbAccType.TabIndex = 118;
            this.cmbAccType.UseSelectable = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(35, 228);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 16);
            this.label6.TabIndex = 119;
            this.label6.Text = "Account Type";
            // 
            // frmDepositMapping
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(572, 308);
            this.Controls.Add(this.mandatoryMark5);
            this.Controls.Add(this.cmbAccType);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.mandatoryMark4);
            this.Controls.Add(this.mandatoryMark3);
            this.Controls.Add(this.mandatoryMark2);
            this.Controls.Add(this.mandatoryMark1);
            this.Controls.Add(this.txtOpeningDepositGL);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.cmbAccountCategory);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtOpeningDeposit);
            this.Controls.Add(this.txtProductName);
            this.Controls.Add(this.txtProductPrefix);
            this.Controls.Add(this.cmbProductList);
            this.Controls.Add(this.metroLabel1);
            this.DisplayHeader = false;
            this.Name = "frmDepositMapping";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.DropShadow;
            this.Text = "Deposit Mapping";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroComboBox cmbProductList;
        private MetroFramework.Controls.MetroTextBox txtProductPrefix;
        private MetroFramework.Controls.MetroTextBox txtProductName;
        private MetroFramework.Controls.MetroTextBox txtOpeningDeposit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private MetroFramework.Controls.MetroComboBox cmbAccountCategory;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSave;
        private CustomTitlebar customTitlebar1;
        private System.Windows.Forms.Button btnClose;
        private MetroFramework.Controls.MetroTextBox txtOpeningDepositGL;
        private Agent.UI.forms.CustomControls.MandatoryMark mandatoryMark2;
        private Agent.UI.forms.CustomControls.MandatoryMark mandatoryMark1;
        private Agent.UI.forms.CustomControls.MandatoryMark mandatoryMark4;
        private Agent.UI.forms.CustomControls.MandatoryMark mandatoryMark3;
        private Agent.UI.forms.CustomControls.MandatoryMark mandatoryMark5;
        private MetroFramework.Controls.MetroComboBox cmbAccType;
        private System.Windows.Forms.Label label6;
    }
}