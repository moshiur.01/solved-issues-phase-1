﻿using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.Configuration.UI.forms
{
    partial class frmMTDRMapping
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.customTitlebar1 = new CustomTitlebar();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbProductList = new MetroFramework.Controls.MetroComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAmountMultiple = new MetroFramework.Controls.MetroTextBox();
            this.txtProductName = new MetroFramework.Controls.MetroTextBox();
            this.txtProductPrefix = new MetroFramework.Controls.MetroTextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMinimumAmount = new MetroFramework.Controls.MetroTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtMaximumAmount = new MetroFramework.Controls.MetroTextBox();
            this.mandatoryMark2 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark3 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark4 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark1 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.SuspendLayout();
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(563, 26);
            this.customTitlebar1.TabIndex = 100;
            this.customTitlebar1.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(30, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 16);
            this.label4.TabIndex = 114;
            this.label4.Text = "Product";
            // 
            // cmbProductList
            // 
            this.cmbProductList.FormattingEnabled = true;
            this.cmbProductList.ItemHeight = 23;
            this.cmbProductList.Location = new System.Drawing.Point(187, 50);
            this.cmbProductList.Name = "cmbProductList";
            this.cmbProductList.Size = new System.Drawing.Size(347, 29);
            this.cmbProductList.TabIndex = 1;
            this.cmbProductList.UseSelectable = true;
            this.cmbProductList.SelectedIndexChanged += new System.EventHandler(this.cmbProductList_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(30, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 16);
            this.label3.TabIndex = 119;
            this.label3.Text = "Product Prefix";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(30, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 16);
            this.label2.TabIndex = 118;
            this.label2.Text = "Amount Multiple";
            // 
            // txtAmountMultiple
            // 
            this.txtAmountMultiple.Lines = new string[0];
            this.txtAmountMultiple.Location = new System.Drawing.Point(188, 128);
            this.txtAmountMultiple.MaxLength = 32767;
            this.txtAmountMultiple.Name = "txtAmountMultiple";
            this.txtAmountMultiple.PasswordChar = '\0';
            this.txtAmountMultiple.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAmountMultiple.SelectedText = "";
            this.txtAmountMultiple.Size = new System.Drawing.Size(115, 23);
            this.txtAmountMultiple.TabIndex = 2;
            this.txtAmountMultiple.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAmountMultiple.UseSelectable = true;
            // 
            // txtProductName
            // 
            this.txtProductName.Lines = new string[0];
            this.txtProductName.Location = new System.Drawing.Point(0, 254);
            this.txtProductName.MaxLength = 32767;
            this.txtProductName.Name = "txtProductName";
            this.txtProductName.PasswordChar = '\0';
            this.txtProductName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtProductName.SelectedText = "";
            this.txtProductName.Size = new System.Drawing.Size(294, 23);
            this.txtProductName.TabIndex = 116;
            this.txtProductName.UseSelectable = true;
            this.txtProductName.Visible = false;
            // 
            // txtProductPrefix
            // 
            this.txtProductPrefix.Lines = new string[0];
            this.txtProductPrefix.Location = new System.Drawing.Point(188, 88);
            this.txtProductPrefix.MaxLength = 32767;
            this.txtProductPrefix.Name = "txtProductPrefix";
            this.txtProductPrefix.PasswordChar = '\0';
            this.txtProductPrefix.ReadOnly = true;
            this.txtProductPrefix.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtProductPrefix.SelectedText = "";
            this.txtProductPrefix.Size = new System.Drawing.Size(115, 23);
            this.txtProductPrefix.TabIndex = 115;
            this.txtProductPrefix.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtProductPrefix.UseSelectable = true;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(459, 226);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 26);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(378, 226);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 26);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(30, 163);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 16);
            this.label1.TabIndex = 123;
            this.label1.Text = "Minimum Amount";
            // 
            // txtMinimumAmount
            // 
            this.txtMinimumAmount.Lines = new string[0];
            this.txtMinimumAmount.Location = new System.Drawing.Point(188, 158);
            this.txtMinimumAmount.MaxLength = 32767;
            this.txtMinimumAmount.Name = "txtMinimumAmount";
            this.txtMinimumAmount.PasswordChar = '\0';
            this.txtMinimumAmount.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtMinimumAmount.SelectedText = "";
            this.txtMinimumAmount.Size = new System.Drawing.Size(115, 23);
            this.txtMinimumAmount.TabIndex = 3;
            this.txtMinimumAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMinimumAmount.UseSelectable = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(30, 193);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 16);
            this.label5.TabIndex = 125;
            this.label5.Text = "Maximum Amount";
            // 
            // txtMaximumAmount
            // 
            this.txtMaximumAmount.Lines = new string[0];
            this.txtMaximumAmount.Location = new System.Drawing.Point(188, 188);
            this.txtMaximumAmount.MaxLength = 32767;
            this.txtMaximumAmount.Name = "txtMaximumAmount";
            this.txtMaximumAmount.PasswordChar = '\0';
            this.txtMaximumAmount.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtMaximumAmount.SelectedText = "";
            this.txtMaximumAmount.Size = new System.Drawing.Size(115, 23);
            this.txtMaximumAmount.TabIndex = 4;
            this.txtMaximumAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMaximumAmount.UseSelectable = true;
            // 
            // mandatoryMark2
            // 
            this.mandatoryMark2.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark2.Location = new System.Drawing.Point(309, 128);
            this.mandatoryMark2.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.Name = "mandatoryMark2";
            this.mandatoryMark2.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.TabIndex = 127;
            // 
            // mandatoryMark3
            // 
            this.mandatoryMark3.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark3.Location = new System.Drawing.Point(309, 158);
            this.mandatoryMark3.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark3.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark3.Name = "mandatoryMark3";
            this.mandatoryMark3.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark3.TabIndex = 128;
            // 
            // mandatoryMark4
            // 
            this.mandatoryMark4.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark4.Location = new System.Drawing.Point(309, 189);
            this.mandatoryMark4.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark4.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark4.Name = "mandatoryMark4";
            this.mandatoryMark4.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark4.TabIndex = 129;
            // 
            // mandatoryMark1
            // 
            this.mandatoryMark1.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark1.Location = new System.Drawing.Point(540, 53);
            this.mandatoryMark1.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.Name = "mandatoryMark1";
            this.mandatoryMark1.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.TabIndex = 130;
            // 
            // frmMTDRMapping
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(562, 283);
            this.Controls.Add(this.mandatoryMark1);
            this.Controls.Add(this.mandatoryMark4);
            this.Controls.Add(this.mandatoryMark3);
            this.Controls.Add(this.mandatoryMark2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtMaximumAmount);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtMinimumAmount);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtAmountMultiple);
            this.Controls.Add(this.txtProductName);
            this.Controls.Add(this.txtProductPrefix);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbProductList);
            this.Controls.Add(this.customTitlebar1);
            this.DisplayHeader = false;
            this.Name = "frmMTDRMapping";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.DropShadow;
            this.Text = "MTDR Mapping";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomTitlebar customTitlebar1;
        private System.Windows.Forms.Label label4;
        private MetroFramework.Controls.MetroComboBox cmbProductList;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private MetroFramework.Controls.MetroTextBox txtAmountMultiple;
        private MetroFramework.Controls.MetroTextBox txtProductName;
        private MetroFramework.Controls.MetroTextBox txtProductPrefix;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label5;
        private MetroFramework.Controls.MetroTextBox txtMaximumAmount;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroTextBox txtMinimumAmount;
        private Agent.UI.forms.CustomControls.MandatoryMark mandatoryMark4;
        private Agent.UI.forms.CustomControls.MandatoryMark mandatoryMark3;
        private Agent.UI.forms.CustomControls.MandatoryMark mandatoryMark2;
        private Agent.UI.forms.CustomControls.MandatoryMark mandatoryMark1;
    }
}