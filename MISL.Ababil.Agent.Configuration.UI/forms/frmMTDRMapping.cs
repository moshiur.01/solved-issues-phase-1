﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using MISL.Ababil.Agent.Common.UI;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.configuration;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.termaccount;
using MISL.Ababil.Agent.Infrastructure.Validation;
using MISL.Ababil.Agent.Services;

namespace MISL.Ababil.Agent.Configuration.UI.forms
{
    public partial class frmMTDRMapping : MetroForm
    {
        private List<CbsProduct> _productList_Core;
        List<TermProductType> _productList = new List<TermProductType>();
        ProductConfigDto _mtdrMapConfig = new ProductConfigDto();
        private Packet _receivedPacket;
        private string errorMessage = "";

        public frmMTDRMapping(Packet packet, ProductConfigDto mtdrMap)
        {
            InitializeComponent();
            _receivedPacket = packet;
            _mtdrMapConfig = mtdrMap;

            getSetupData();

            if (_receivedPacket.actionType == FormActionType.Edit)
            {
                FillComponentWithObjectValue();
                cmbProductList.Enabled = false;
            }

            txtProductName.Enabled = false;
            txtProductPrefix.Enabled = false;
        }

        private void getSetupData()
        {
            try
            {
                TermProductType selectProduct = new TermProductType();
                switch (_receivedPacket.actionType)
                {
                    case FormActionType.New:
                        {
                            _productList_Core = new List<CbsProduct>();
                            ServiceResult result = ProductMappingServices.GetProductsList(ProductType.MTDR);
                            _productList_Core = result.ReturnedObject as List<CbsProduct>;
                            _productList = _productList_Core.Select(o => new TermProductType()
                            {
                                id = o.productId,
                                productPrefix = o.productPrefix,
                                productDescription = o.productTitle,

                                maxAmount = 0,
                                minAmount = 0,
                                multipleBy = 0
                            }).ToList();
                            break;
                        }

                    case FormActionType.Edit:
                        _productList = new ProductMappingServices().GetMTDRProducts();
                        break;
                }



                selectProduct.id = 0;
                selectProduct.productPrefix = "";
                selectProduct.productDescription = "Select";
                selectProduct.maxAmount = 0;
                selectProduct.minAmount = 0;
                selectProduct.multipleBy = 0;
                _productList.Add(selectProduct);

                BindingSource bs = new BindingSource();
                bs.DataSource = _productList;
                UtilityServices.fillComboBox(cmbProductList, bs, "productDescription", "productPrefix");
                cmbProductList.SelectedIndex = cmbProductList.Items.Count - 1;

                cmbProductList_IndexChanged();
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
        }
        private bool validationCheck()
        {
            //return ValidationManager.ValidateForm(this);
            bool isValidForm = true;
            bool RangeIsGiven = true;
            decimal minAmount = 0, maxAmount = 0;

            errorMessage = "Following inputs require attention.\n\n";
            if ((txtProductName.Text.Trim() == "") || (txtProductPrefix.Text.Trim() == ""))
            { isValidForm = false; errorMessage += "Select a Product.\n"; }

            if (txtAmountMultiple.Text.Trim() != "")
            {
                try
                { Convert.ToDecimal(txtAmountMultiple.Text.Trim()); }
                catch (Exception exp) { isValidForm = false; errorMessage += "Only numeric input in Multiple Amount.\n"; }
            }
            else
            { isValidForm = false; errorMessage += "Multiple Amount should not be empty.\n"; }

            if (txtMinimumAmount.Text.Trim() != "")
            {
                try
                { minAmount = Convert.ToDecimal(txtMinimumAmount.Text.Trim()); }
                catch (Exception exp)
                { isValidForm = false; errorMessage += "Only numeric input in Minimum Amount.\n"; }
            }
            else
            {
                RangeIsGiven = false;
                isValidForm = false;
                errorMessage += "Minimum Amount should not be empty.\n";
            }

            if (txtMaximumAmount.Text.Trim() != "")
            {
                try
                { maxAmount = Convert.ToDecimal(txtMaximumAmount.Text.Trim()); }
                catch (Exception exp)
                { isValidForm = false; errorMessage += "Only numeric input in Maximum Amount.\n"; }
            }
            else
            {
                RangeIsGiven = false;
                isValidForm = false;
                errorMessage += "Maximum Amount should not be empty.\n";
            }

            if (RangeIsGiven)
            {
                if (maxAmount < minAmount)
                {
                    isValidForm = false;
                    errorMessage += "Minimum amount should be less than maximum amount.";
                }
            }

            return isValidForm;
        }
        public ProductConfigDto newMTDRMapping()
        {
            CbsProduct selectedProduct = _productList_Core.Where(x => x.productPrefix == cmbProductList.SelectedValue.ToString()).SingleOrDefault();

            TermProductType termProductType = new TermProductType();
            termProductType.id = selectedProduct.productId; //0; // selectedProduct.productId
            termProductType.accountType = AccountType.MTDR;
            termProductType.productPrefix = selectedProduct.productPrefix;
            termProductType.productDescription = selectedProduct.productTitle;

            termProductType.maxAmount = Convert.ToDecimal(txtMaximumAmount.Text.Trim());
            termProductType.minAmount = Convert.ToDecimal(txtMinimumAmount.Text.Trim());
            termProductType.multipleBy = Convert.ToDecimal(txtAmountMultiple.Text.Trim());

            _mtdrMapConfig.accountType = AccountType.MTDR;
            _mtdrMapConfig.termProductType = termProductType;
            return _mtdrMapConfig;
        }
        public ProductConfigDto getFilledObject()
        {
            return _mtdrMapConfig;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (validationCheck())
            {
                FillObjectWithComponentValue();

                btnSave.DialogResult = DialogResult.OK;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                btnSave.DialogResult = DialogResult.None;
                this.DialogResult = DialogResult.None;

                CustomMessage.showError(errorMessage);
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void cmbProductList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ComboBox cb = (ComboBox)sender;
            //if (!cb.Focused) return;
            //else
            //{
            //CbsProduct selectedProduct = _productList_Core.Where(x => x.productId == long.Parse(cmbProductList.SelectedValue.ToString())).SingleOrDefault();
            cmbProductList_IndexChanged();
            //}
        }
        private void cmbProductList_IndexChanged()
        {
            txtProductPrefix.Text = txtProductName.Text = "";
            TermProductType selectedProduct = _productList.Where(x => x.productPrefix == cmbProductList.SelectedValue.ToString()).SingleOrDefault();
            if (selectedProduct != null)
            {
                txtProductPrefix.Text = selectedProduct.productPrefix;
                txtProductName.Text = selectedProduct.productDescription;
            }
        }
        private void FillComponentWithObjectValue()
        {
            if (_mtdrMapConfig == null) _mtdrMapConfig = new ProductConfigDto();
            try
            {
                TermProductType selectedProduct = _productList.Where(x => x.productPrefix == _mtdrMapConfig.termProductType.productPrefix).SingleOrDefault();
                cmbProductList.SelectedItem = selectedProduct;
                cmbProductList_IndexChanged();

                txtProductPrefix.Text = _mtdrMapConfig.termProductType.productPrefix;
                txtProductName.Text = _mtdrMapConfig.termProductType.productDescription;
                txtMaximumAmount.Text = _mtdrMapConfig.termProductType.maxAmount.ToString();
                txtMinimumAmount.Text = _mtdrMapConfig.termProductType.minAmount.ToString();
                txtAmountMultiple.Text = _mtdrMapConfig.termProductType.multipleBy.ToString();
            }
            catch (Exception ex)
            {
                //Message.showError(ex.Message);
                CustomMessage.showError(ex.Message);
            }
        }
        private void FillObjectWithComponentValue()
        {
            try
            {
                TermProductType termProductType = new TermProductType();
                TermProductType selectedProduct = _productList.Where(x => x.productPrefix == cmbProductList.SelectedValue.ToString()).SingleOrDefault();

                //if (_receivePacket.actionType != FormActionType.Edit) termProductType.id = 0;
                //else termProductType.id = _mtdrMapConfig.termProductType.id;
                termProductType.id = selectedProduct.id;
                termProductType.accountType = AccountType.MTDR;
                termProductType.productPrefix = selectedProduct.productPrefix;
                termProductType.productDescription = selectedProduct.productDescription;

                termProductType.maxAmount = Convert.ToDecimal(txtMaximumAmount.Text.Trim());
                termProductType.minAmount = Convert.ToDecimal(txtMinimumAmount.Text.Trim());
                termProductType.multipleBy = Convert.ToDecimal(txtAmountMultiple.Text.Trim());

                _mtdrMapConfig.termProductType = termProductType;
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
        }

    }
}
