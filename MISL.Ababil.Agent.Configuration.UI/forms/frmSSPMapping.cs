﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MetroFramework.Forms;
using MISL.Ababil.Agent.Common.UI;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.configuration;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.ssp;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.termaccount;
using MISL.Ababil.Agent.Infrastructure.Validation;
using MISL.Ababil.Agent.Services;

namespace MISL.Ababil.Agent.Configuration.UI.forms
{
    public partial class frmSSPMapping : MetroForm
    {
        private List<CbsProduct> _productList_Core;
        List<TermProductType> _productList = new List<TermProductType>();

        private List<SspInstallment> _installmentList_All = new List<SspInstallment>();
        private List<SspInstallment> _installmentList_Remaining = new List<SspInstallment>();
        private List<SspInstallment> _installmentList_Selected = new List<SspInstallment>();

        ProductConfigDto _sspMap = new ProductConfigDto();
        private Packet _receivedPacket;
        private string errorMessage = "";

        public frmSSPMapping(Packet packet, ProductConfigDto sspMap)
        {
            InitializeComponent();
            _receivedPacket = packet;
            _sspMap = sspMap;

            getSetupData();

            if (_receivedPacket.actionType == FormActionType.Edit)
            {
                FillComponentWithObjectValue();
                cmbProductList.Enabled = false;
            }

            txtProductName.Enabled = false;
            txtProductPrefix.Enabled = false;
        }
        private void getSetupData()
        {
            try
            {
                TermProductType selectProduct = new TermProductType();
                _productList_Core = new List<CbsProduct>();

                switch (_receivedPacket.actionType)
                {
                    case FormActionType.New:
                        {
                            ServiceResult result = ProductMappingServices.GetProductsList(ProductType.SSP);
                            _productList_Core = result.ReturnedObject as List<CbsProduct>;
                            _productList = _productList_Core.Select(o => new TermProductType()
                            {
                                id = o.productId,
                                productPrefix = o.productPrefix,
                                productDescription = o.productTitle
                            }).ToList();
                            break;
                        }

                    case FormActionType.Edit:
                        _productList = new ProductMappingServices().GetSSPProducts();
                        break;
                }

                selectProduct.id = 0;
                selectProduct.productPrefix = "";
                selectProduct.productDescription = "Select";
                selectProduct.maxAmount = 0;
                selectProduct.minAmount = 0;
                selectProduct.multipleBy = 0;
                _productList.Add(selectProduct);

                UtilityServices.fillComboBox(cmbProductList, new BindingSource
                {
                    DataSource = _productList
                }, "productDescription", "productPrefix");
                cmbProductList.SelectedIndex = cmbProductList.Items.Count - 1;

                //cmbProductList_IndexChanged();
            }
            catch (Exception ex)
            { CustomMessage.showError(ex.Message); }
        }
        private void cmbProductList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ComboBox cb = (ComboBox)sender;
            //if (!cb.Focused) return;
            //else
            //{
            cmbProductList_IndexChanged();
            //}
        }
        private void cmbProductList_IndexChanged()
        {
            if (_receivedPacket != null && _receivedPacket.actionType == FormActionType.New)
            {
                lbAllInstallment.DataSource = lbSelectedInstallment.DataSource = null;
                lbAllInstallment.Refresh(); lbSelectedInstallment.Refresh();
                txtProductPrefix.Text = txtProductName.Text = "";

                //CbsProduct selectedProduct = _productList_Core.Where(x => x.productId == long.Parse(cmbProductList.SelectedValue.ToString())).SingleOrDefault();
                CbsProduct selectedProduct = _productList_Core.Where(x => x.productPrefix == cmbProductList.SelectedValue.ToString()).SingleOrDefault();
                if (selectedProduct != null)
                {
                    txtProductPrefix.Text = selectedProduct.productPrefix;
                    txtProductName.Text = selectedProduct.productTitle;

                    #region Load All Installments
                    ServiceResult result = ProductMappingServices.GetSSPInstallmentListByProduct(selectedProduct);

                    _installmentList_All = result.ReturnedObject as List<SspInstallment>;
                    _installmentList_Remaining = result.ReturnedObject as List<SspInstallment>;

                    if (_installmentList_Remaining != null) LoadInstallmentList(lbAllInstallment, _installmentList_Remaining);
                    //else { CustomMessage.showWarning("Installment size not found."); }
                    #endregion
                }
            }
        }

        private bool validationCheck()
        {
            //return ValidationManager.ValidateForm(this);
            bool isValidForm = true;
            errorMessage = "Following inputs require attention.\n\n";
            if ((txtProductName.Text.Trim() == "") || (txtProductPrefix.Text.Trim() == ""))
            { isValidForm = false; errorMessage += "Select a product.\n"; }

            if (_installmentList_Selected.Count == 0)
            { isValidForm = false; errorMessage += "Installments should not be empty."; }

            return isValidForm;
        }
        public ProductConfigDto newSSPConfig()
        {
            CbsProduct selectedProduct = _productList_Core.Where(x => x.productPrefix == cmbProductList.SelectedValue.ToString()).SingleOrDefault();

            _sspMap.accountType = AccountType.SSP;
            _sspMap.termProductType.id = selectedProduct.productId;//0; // selectedProduct.productId;
            _sspMap.termProductType.productPrefix = selectedProduct.productPrefix;
            _sspMap.termProductType.productDescription = selectedProduct.productTitle;
            _sspMap.installments = _installmentList_Selected;
            return _sspMap;
        }

        private void FillComponentWithObjectValue()
        {
            if (_sspMap == null) _sspMap = new ProductConfigDto();

            try
            {
                _installmentList_Selected = _sspMap.installments;
                _installmentList_All = new ProductMappingServices()
                    .GetAllInstallmentByProductID(_sspMap.termProductType.id)
                    .Except<SspInstallment>(_installmentList_Selected)
                    .ToList<SspInstallment>();

                if (_receivedPacket != null && _receivedPacket.actionType == FormActionType.New)
                {
                    cmbProductList.SelectedValue = _productList_Core.Where(o => o.productPrefix == _sspMap.termProductType.productPrefix).First();
                }
                else
                {
                    cmbProductList.SelectedValue = _productList.Where(o => o.productPrefix == _sspMap.termProductType.productPrefix).First().productPrefix;
                }
                cmbProductList_IndexChanged();


                // for (int i = 0; i < _installmentList_Remaining.Count; i++)
                // {
                //     if (_installmentList_Selected.Contains(_installmentList_Remaining[i]))
                //     {
                //         _installmentList_Remaining.Remove(_installmentList_Remaining[i]);
                //         i--;
                //     }
                //
                // }
                //_installmentList_Remaining.RemoveAll(x => _installmentList_Selected.Whe);

                //foreach (SspInstallment installmentSelected in _installmentList_Selected.Reverse<SspInstallment>())
                //{
                //    foreach (SspInstallment installmentAll in _installmentList_Remaining.Reverse<SspInstallment>())
                //    {
                //        if (installmentSelected.installAmount == installmentAll.installAmount) _installmentList_Remaining.Remove(installmentAll);
                //    }
                //}

                //LoadInstallmentList(lbAllInstallment, _installmentList_Remaining);
                LoadInstallmentList(lbAllInstallment, _installmentList_All);

                txtProductPrefix.Text = _sspMap.termProductType.productPrefix;
                txtProductName.Text = _sspMap.termProductType.productDescription;
                _installmentList_Selected = _sspMap.installments;

                LoadInstallmentList(lbSelectedInstallment, _sspMap.installments);
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
        }

        private void FillObjectWithComponentValue()
        {
            try
            {
                if (_receivedPacket.actionType != FormActionType.Edit)
                {
                    _sspMap = new ProductConfigDto();

                    //CbsProduct selectedProduct = _productList_Core.Where(x => x.productPrefix == cmbProductList.SelectedValue.ToString()).SingleOrDefault();
                    TermProductType selectedProduct = cmbProductList.Items.Cast<TermProductType>().Where(x => x.productPrefix == cmbProductList.SelectedValue.ToString()).SingleOrDefault();

                    selectedProduct.accountType = AccountType.SSP;
                    //TermProductType termProductType = new TermProductType();
                    //termProductType.id = selectedProduct.id; //0; // selectedProduct.productId
                    //termProductType.accountType = AccountType.SSP;
                    //termProductType.productPrefix = selectedProduct.productPrefix;
                    //termProductType.productDescription = selectedProduct.productDescription;
                    _sspMap.termProductType = selectedProduct; //termProductType;
                }
                _sspMap.accountType = AccountType.SSP;
                _sspMap.installments = _installmentList_Selected;
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
        }

        public ProductConfigDto getFilledObject()
        {
            return _sspMap;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            if (validationCheck())
            {
                try
                {
                    FillObjectWithComponentValue();

                    //new ProductMappingServices().SaveSSPMapping(_sspMap);
                    //CustomMessage.showInformation("Saved Successfully!");
                }
                catch (Exception ex)
                {
                    CustomMessage.showError(ex.Message);
                }

                btnSave.DialogResult = DialogResult.OK;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                btnSave.DialogResult = DialogResult.None;
                this.DialogResult = DialogResult.None;

                CustomMessage.showError(errorMessage);
            }
        }

        private void btnMoveRightToLeft_Click(object sender, EventArgs e)
        {
            try
            {
                if (_receivedPacket != null && _receivedPacket.actionType == FormActionType.New)
                {

                    SspInstallment selectedInstallment = _installmentList_Remaining.Where(x => x.id == long.Parse(lbAllInstallment.SelectedValue.ToString())).SingleOrDefault();
                    _installmentList_Selected.Add(selectedInstallment);
                    LoadInstallmentList(lbSelectedInstallment, _installmentList_Selected);

                    _installmentList_Remaining.Remove(selectedInstallment);
                    LoadInstallmentList(lbAllInstallment, _installmentList_Remaining);
                }
                else
                {
                    SspInstallment selectedInstallment = _installmentList_All.Where(x => x.id == long.Parse(lbAllInstallment.SelectedValue.ToString())).SingleOrDefault();
                    _installmentList_Selected.Add(selectedInstallment);
                    LoadInstallmentList(lbSelectedInstallment, _installmentList_Selected);

                    _installmentList_All.Remove(selectedInstallment);
                    LoadInstallmentList(lbAllInstallment, _installmentList_All);
                }
            }
            catch (Exception ex)
            {
                //CustomMessage.showError(ex.Message);
            }
        }
        private void btnMoveAllRightToLeft_Click(object sender, EventArgs e)
        {
            try
            {
                if (_receivedPacket != null && _receivedPacket.actionType == FormActionType.New)
                {

                    _installmentList_Selected = new List<SspInstallment>();
                    _installmentList_Selected = _installmentList_Remaining;
                    _installmentList_Remaining = new List<SspInstallment>();

                    LoadInstallmentList(lbSelectedInstallment, _installmentList_Selected);
                    LoadInstallmentList(lbAllInstallment, _installmentList_Remaining);
                }
                else
                {
                    _installmentList_Selected.AddRange(_installmentList_All.Except(_installmentList_Selected).ToList());
                    LoadInstallmentList(lbSelectedInstallment, _installmentList_Selected);
                    _installmentList_All.Clear();
                    LoadInstallmentList(lbAllInstallment, _installmentList_All);
                }
            }
            catch (Exception ex)
            {
                //CustomMessage.showError(ex.Message);
            }
        }
        private void btnMoveLeftToRight_Click(object sender, EventArgs e)
        {
            try
            {
                if (_receivedPacket != null && _receivedPacket.actionType == FormActionType.New)
                {
                    SspInstallment selectedInstallment = _installmentList_Selected.Where(x => x.id == long.Parse(lbSelectedInstallment.SelectedValue.ToString())).SingleOrDefault();
                    _installmentList_Remaining.Add(selectedInstallment);
                    LoadInstallmentList(lbAllInstallment, _installmentList_Remaining);

                    _installmentList_Selected.Remove(selectedInstallment);
                    LoadInstallmentList(lbSelectedInstallment, _installmentList_Selected);
                }
                else
                {
                    SspInstallment selectedInstallment = _installmentList_Selected.Where(x => x.id == long.Parse(lbSelectedInstallment.SelectedValue.ToString())).SingleOrDefault();
                    _installmentList_All.Add(selectedInstallment);
                    LoadInstallmentList(lbAllInstallment, _installmentList_All);

                    _installmentList_Selected.Remove(selectedInstallment);
                    LoadInstallmentList(lbSelectedInstallment, _installmentList_Selected);
                }
            }
            catch (Exception ex)
            {
                //CustomMessage.showError(ex.Message);
            }
        }

        private void LoadInstallmentList(ListBox lb, List<SspInstallment> _installmentList)
        {
            try
            {
                // try
                //{
                //    if (_installmentList.Count > 1)
                //    {
                //        _installmentList.Sort((x, y) =>
                //        {
                //            if (x == null)
                //            {
                //                //throw new ArgumentNullException(nameof(x));
                //                return -1;
                //            }

                //            return x.installAmount.CompareTo(y?.installAmount);
                //        });
                //    }
                //}
                //catch { }
                //lb.Sorted = true;
                lb.DataSource = _installmentList.Select(item => new { item.installAmount, item.id }).ToList();
                lb.DisplayMember = "installAmount";
                lb.ValueMember = "id";
                lb.Refresh();


            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
        }

    }
}
