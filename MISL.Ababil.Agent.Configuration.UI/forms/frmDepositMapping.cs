﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using MISL.Ababil.Agent.Common.UI;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.configuration;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.termaccount;
using MISL.Ababil.Agent.Infrastructure.Validation;
using MISL.Ababil.Agent.Services;

namespace MISL.Ababil.Agent.Configuration.UI.forms
{
    public partial class frmDepositMapping : MetroForm
    {
        private List<CbsProduct> _productList_Core;
        List<AgentProduct> _productList = new List<AgentProduct>();

        ProductConfigDto _depositConfig = new ProductConfigDto();
        private Packet _receivedPacket;
        private string errorMessage = "";

        public frmDepositMapping(Packet packet, ProductConfigDto depositConfig)
        {
            InitializeComponent();
            getSetupData();

            _receivedPacket = packet;
            _depositConfig = depositConfig;
            if (_receivedPacket.actionType == FormActionType.Edit)
            {
                FillComponentWithObjectValue();
                cmbProductList.Enabled = false;
            }

            txtProductName.Enabled = false;
            txtProductPrefix.Enabled = false;
        }
        private void getSetupData()
        {
            try
            {
                //cmbAccountCategory.DataSource = Enum.GetValues(typeof(DepositAccountCategory));  // Combo is HARD CODED
                cmbAccountCategory.SelectedIndex = 0;
                cmbAccType.SelectedIndex = 0;
                _productList_Core = new List<CbsProduct>();
                ServiceResult result = ProductMappingServices.GetProductsList(ProductType.Deposit);
                _productList_Core = result.ReturnedObject as List<CbsProduct>;
                _productList = _productList_Core.Select(o => new AgentProduct()
                                                            {
                                                                id = 0,
                                                                productPrefix = o.productPrefix,
                                                                productTitle = o.productTitle,

                                                                openingDeposit = 0,
                                                                openingDepositGl = 0
                                                            }
                                                        ).ToList();

                AgentProduct selectProduct = new AgentProduct();
                selectProduct.id = 0;
                selectProduct.productPrefix = "";
                selectProduct.productTitle = "Select";
                selectProduct.openingDeposit = 0;
                selectProduct.openingDepositGl = 0;
                _productList.Add(selectProduct);

                BindingSource bs = new BindingSource();
                bs.DataSource = _productList; //_productList_Core;
                UtilityServices.fillComboBox(cmbProductList, bs, "productTitle", "productPrefix");
                cmbProductList.SelectedIndex = cmbProductList.Items.Count - 1;

                cmbProductList_IndexChanged();
            }
            catch (Exception ex)
            { CustomMessage.showError(ex.Message); }
        }
        private bool validationCheck()
        {
            //return ValidationManager.ValidateForm(this);
            bool isValidForm = true;
            errorMessage = "Following inputs require attention.\n\n";
            if ((txtProductName.Text.Trim() == "") || (txtProductPrefix.Text.Trim() == ""))
            { isValidForm = false; errorMessage += "Select a product.\n"; }

            if (txtOpeningDeposit.Text.Trim() != "")
            {
                try
                { Convert.ToDecimal(txtOpeningDeposit.Text.Trim()); }
                catch (Exception exp) { isValidForm = false; errorMessage += "Only numeric input in Opening Deposit.\n"; }
            }
            else
            { isValidForm = false; errorMessage += "Opening Deposit should not be empty.\n"; }
            
            if (txtOpeningDepositGL.Text.Trim() != "")
            {
                try
                { long.Parse(txtOpeningDepositGL.Text.Trim()); }
                catch (Exception exp) { isValidForm = false; errorMessage += "Only integer input in Opening DepositGL.\n"; }
            }
            else
            { isValidForm = false; errorMessage += "Opening DepositGL should not be empty.\n"; }

            if(cmbAccountCategory.SelectedItem.ToString() == "Select")
            { isValidForm = false; errorMessage += "Select an account category."; }

            if (cmbAccType.SelectedItem.ToString() == "Select")
            { isValidForm = false; errorMessage += "Select an acc type."; }

            return isValidForm;
        }

        public ProductConfigDto newDepositMapping()
        {
            _depositConfig.accountType = AccountType.Deposit;

            AgentProduct newAgentProduct = new AgentProduct();
            newAgentProduct.id = 0;
            newAgentProduct.productType = ProductType.Deposit;
            newAgentProduct.productPrefix = txtProductPrefix.Text.Trim();
            newAgentProduct.productTitle = txtProductName.Text.Trim();
            newAgentProduct.openingDeposit = Convert.ToDecimal(txtOpeningDeposit.Text.Trim());
            newAgentProduct.openingDepositGl = long.Parse(txtOpeningDepositGL.Text.Trim());
            //newAgentProduct.accountCategory = (DepositAccountCategory)Enum.Parse(typeof(DepositAccountCategory), cmbAccountCategory.SelectedValue.ToString());
            newAgentProduct.accountCategory = (DepositAccountCategory)Enum.Parse(typeof(DepositAccountCategory), cmbAccountCategory.SelectedItem.ToString());
            newAgentProduct.accType = (AccType)Enum.Parse(typeof(AccType), cmbAccType.SelectedItem.ToString());
            _depositConfig.agentProduct = newAgentProduct;
            return _depositConfig;
        }
        public ProductConfigDto getFilledObject()
        {
            return _depositConfig;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (validationCheck())
            {
                FillObjectWithComponentValue();

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                btnSave.DialogResult = DialogResult.None;
                this.DialogResult = DialogResult.None;

                CustomMessage.showError(errorMessage);
            }
        }

        private void cmbProductList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ComboBox cb = (ComboBox)sender;
            //if (!cb.Focused) return;
            //else
            //{
            cmbProductList_IndexChanged();
            //}
        }
        private void cmbProductList_IndexChanged()
        {
            txtProductPrefix.Text = txtProductName.Text = "";
            //CbsProduct selectedProduct = _productList_Core.Where(x => x.productPrefix == cmbProductList.SelectedValue.ToString()).SingleOrDefault();
            //if (selectedProduct != null)
            //{
            //    txtProductPrefix.Text = selectedProduct.productPrefix;
            //    txtProductName.Text = selectedProduct.productTitle;
            //}

            AgentProduct selectedProduct = _productList.Where(x => x.productPrefix == cmbProductList.SelectedValue.ToString()).SingleOrDefault();
            if (selectedProduct != null)
            {
                txtProductPrefix.Text = selectedProduct.productPrefix;
                txtProductName.Text = selectedProduct.productTitle;
            }
        }

        private void FillComponentWithObjectValue()
        {
            if (_depositConfig == null) _depositConfig = new ProductConfigDto();

            try
            {
                if (_receivedPacket.actionType == FormActionType.Edit)
                {
                    AgentProduct newAgentProduct = new AgentProduct();
                    newAgentProduct.productPrefix = _depositConfig.agentProduct.productPrefix;
                    newAgentProduct.productTitle= _depositConfig.agentProduct.productTitle;
                    _productList = new List<AgentProduct>();
                    _productList.Add(newAgentProduct);

                    BindingSource bs = new BindingSource();
                    bs.DataSource = _productList;
                    UtilityServices.fillComboBox(cmbProductList, bs, "productTitle", "productPrefix");
                    cmbProductList.SelectedItem = newAgentProduct;
                }
               
                cmbProductList_IndexChanged();

                //cmbAccountCategory.SelectedItem = _depositConfig.agentProduct.accountCategory;
                cmbAccountCategory.SelectedItem = _depositConfig.agentProduct.accountCategory.ToString();
                cmbAccType.SelectedItem = _depositConfig.agentProduct.accType.ToString();
                txtProductPrefix.Text = _depositConfig.agentProduct.productPrefix;
                txtProductName.Text = _depositConfig.agentProduct.productTitle;
                txtOpeningDeposit.Text = _depositConfig.agentProduct.openingDeposit.ToString("0.00");
                txtOpeningDepositGL.Text = _depositConfig.agentProduct.openingDepositGl.ToString();
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
        }
        private void FillObjectWithComponentValue()
        {
            try
            {
                AgentProduct newAgentProduct = new AgentProduct();

                if (_receivedPacket.actionType != FormActionType.Edit) newAgentProduct.id = 0;
                else newAgentProduct.id = _depositConfig.agentProduct.id;

                newAgentProduct.productType = ProductType.Deposit;
                newAgentProduct.productPrefix = txtProductPrefix.Text.Trim();
                newAgentProduct.productTitle = txtProductName.Text.Trim();
                newAgentProduct.openingDeposit = Convert.ToDecimal(txtOpeningDeposit.Text.Trim());
                newAgentProduct.openingDepositGl = long.Parse(txtOpeningDepositGL.Text.Trim());
                //newAgentProduct.accountCategory = (DepositAccountCategory)Enum.Parse(typeof(DepositAccountCategory), cmbAccountCategory.SelectedValue.ToString());
                newAgentProduct.accountCategory = (DepositAccountCategory)Enum.Parse(typeof(DepositAccountCategory), cmbAccountCategory.SelectedItem.ToString());
                newAgentProduct.accType = (AccType)Enum.Parse(typeof(AccType), cmbAccType.SelectedItem.ToString());
                _depositConfig.accountType = AccountType.Deposit;
                _depositConfig.agentProduct = newAgentProduct;
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
        }

    }
}
