﻿using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.Configuration.UI.forms
{
    partial class frmSSPMapping
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbProductList = new MetroFramework.Controls.MetroComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtProductName = new MetroFramework.Controls.MetroTextBox();
            this.txtProductPrefix = new MetroFramework.Controls.MetroTextBox();
            this.lbAllInstallment = new System.Windows.Forms.ListBox();
            this.lbSelectedInstallment = new System.Windows.Forms.ListBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnMoveAllRightToLeft = new System.Windows.Forms.Button();
            this.btnMoveRightToLeft = new System.Windows.Forms.Button();
            this.btnMoveLeftToRight = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.mandatoryMark1 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark2 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.SuspendLayout();
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(559, 26);
            this.customTitlebar1.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(25, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 16);
            this.label4.TabIndex = 117;
            this.label4.Text = "Product";
            // 
            // cmbProductList
            // 
            this.cmbProductList.FormattingEnabled = true;
            this.cmbProductList.ItemHeight = 23;
            this.cmbProductList.Location = new System.Drawing.Point(126, 43);
            this.cmbProductList.Name = "cmbProductList";
            this.cmbProductList.Size = new System.Drawing.Size(400, 29);
            this.cmbProductList.TabIndex = 1;
            this.cmbProductList.UseSelectable = true;
            this.cmbProductList.SelectedIndexChanged += new System.EventHandler(this.cmbProductList_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(25, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 16);
            this.label3.TabIndex = 122;
            this.label3.Text = "Product Prefix";
            // 
            // txtProductName
            // 
            this.txtProductName.Lines = new string[0];
            this.txtProductName.Location = new System.Drawing.Point(10, 501);
            this.txtProductName.MaxLength = 32767;
            this.txtProductName.Name = "txtProductName";
            this.txtProductName.PasswordChar = '\0';
            this.txtProductName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtProductName.SelectedText = "";
            this.txtProductName.Size = new System.Drawing.Size(294, 23);
            this.txtProductName.TabIndex = 121;
            this.txtProductName.UseSelectable = true;
            this.txtProductName.Visible = false;
            // 
            // txtProductPrefix
            // 
            this.txtProductPrefix.Lines = new string[0];
            this.txtProductPrefix.Location = new System.Drawing.Point(126, 78);
            this.txtProductPrefix.MaxLength = 32767;
            this.txtProductPrefix.Name = "txtProductPrefix";
            this.txtProductPrefix.PasswordChar = '\0';
            this.txtProductPrefix.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtProductPrefix.SelectedText = "";
            this.txtProductPrefix.Size = new System.Drawing.Size(100, 23);
            this.txtProductPrefix.TabIndex = 120;
            this.txtProductPrefix.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtProductPrefix.UseSelectable = true;
            // 
            // lbAllInstallment
            // 
            this.lbAllInstallment.FormattingEnabled = true;
            this.lbAllInstallment.Location = new System.Drawing.Point(28, 139);
            this.lbAllInstallment.Name = "lbAllInstallment";
            this.lbAllInstallment.Size = new System.Drawing.Size(198, 329);
            this.lbAllInstallment.TabIndex = 2;
            // 
            // lbSelectedInstallment
            // 
            this.lbSelectedInstallment.FormattingEnabled = true;
            this.lbSelectedInstallment.Location = new System.Drawing.Point(328, 139);
            this.lbSelectedInstallment.Name = "lbSelectedInstallment";
            this.lbSelectedInstallment.Size = new System.Drawing.Size(198, 329);
            this.lbSelectedInstallment.TabIndex = 5;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(451, 487);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 26);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(370, 487);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 26);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnMoveAllRightToLeft
            // 
            this.btnMoveAllRightToLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnMoveAllRightToLeft.FlatAppearance.BorderSize = 0;
            this.btnMoveAllRightToLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMoveAllRightToLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveAllRightToLeft.ForeColor = System.Drawing.Color.White;
            this.btnMoveAllRightToLeft.Location = new System.Drawing.Point(254, 292);
            this.btnMoveAllRightToLeft.Name = "btnMoveAllRightToLeft";
            this.btnMoveAllRightToLeft.Size = new System.Drawing.Size(50, 25);
            this.btnMoveAllRightToLeft.TabIndex = 4;
            this.btnMoveAllRightToLeft.Text = ">>";
            this.btnMoveAllRightToLeft.UseVisualStyleBackColor = false;
            this.btnMoveAllRightToLeft.Click += new System.EventHandler(this.btnMoveAllRightToLeft_Click);
            // 
            // btnMoveRightToLeft
            // 
            this.btnMoveRightToLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnMoveRightToLeft.FlatAppearance.BorderSize = 0;
            this.btnMoveRightToLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMoveRightToLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveRightToLeft.ForeColor = System.Drawing.Color.White;
            this.btnMoveRightToLeft.Location = new System.Drawing.Point(254, 259);
            this.btnMoveRightToLeft.Name = "btnMoveRightToLeft";
            this.btnMoveRightToLeft.Size = new System.Drawing.Size(50, 25);
            this.btnMoveRightToLeft.TabIndex = 3;
            this.btnMoveRightToLeft.Text = ">";
            this.btnMoveRightToLeft.UseVisualStyleBackColor = false;
            this.btnMoveRightToLeft.Click += new System.EventHandler(this.btnMoveRightToLeft_Click);
            // 
            // btnMoveLeftToRight
            // 
            this.btnMoveLeftToRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnMoveLeftToRight.FlatAppearance.BorderSize = 0;
            this.btnMoveLeftToRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMoveLeftToRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveLeftToRight.ForeColor = System.Drawing.Color.White;
            this.btnMoveLeftToRight.Location = new System.Drawing.Point(254, 326);
            this.btnMoveLeftToRight.Name = "btnMoveLeftToRight";
            this.btnMoveLeftToRight.Size = new System.Drawing.Size(50, 25);
            this.btnMoveLeftToRight.TabIndex = 6;
            this.btnMoveLeftToRight.Text = "<";
            this.btnMoveLeftToRight.UseVisualStyleBackColor = false;
            this.btnMoveLeftToRight.Click += new System.EventHandler(this.btnMoveLeftToRight_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(25, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 16);
            this.label1.TabIndex = 122;
            this.label1.Text = "Installment Size";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(325, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 16);
            this.label2.TabIndex = 122;
            this.label2.Text = "Installment Size";
            // 
            // mandatoryMark1
            // 
            this.mandatoryMark1.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark1.Location = new System.Drawing.Point(533, 47);
            this.mandatoryMark1.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.Name = "mandatoryMark1";
            this.mandatoryMark1.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.TabIndex = 123;
            // 
            // mandatoryMark2
            // 
            this.mandatoryMark2.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark2.Location = new System.Drawing.Point(533, 136);
            this.mandatoryMark2.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.Name = "mandatoryMark2";
            this.mandatoryMark2.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.TabIndex = 124;
            // 
            // frmSSPMapping
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 536);
            this.Controls.Add(this.mandatoryMark2);
            this.Controls.Add(this.mandatoryMark1);
            this.Controls.Add(this.btnMoveLeftToRight);
            this.Controls.Add(this.btnMoveRightToLeft);
            this.Controls.Add(this.btnMoveAllRightToLeft);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lbSelectedInstallment);
            this.Controls.Add(this.lbAllInstallment);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtProductName);
            this.Controls.Add(this.txtProductPrefix);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbProductList);
            this.Controls.Add(this.customTitlebar1);
            this.DisplayHeader = false;
            this.Name = "frmSSPMapping";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.DropShadow;
            this.Text = "SSP Mapping";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomTitlebar customTitlebar1;
        private System.Windows.Forms.Label label4;
        private MetroFramework.Controls.MetroComboBox cmbProductList;
        private System.Windows.Forms.Label label3;
        private MetroFramework.Controls.MetroTextBox txtProductName;
        private MetroFramework.Controls.MetroTextBox txtProductPrefix;
        private System.Windows.Forms.ListBox lbSelectedInstallment;
        private System.Windows.Forms.ListBox lbAllInstallment;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnMoveAllRightToLeft;
        private System.Windows.Forms.Button btnMoveRightToLeft;
        private System.Windows.Forms.Button btnMoveLeftToRight;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Agent.UI.forms.CustomControls.MandatoryMark mandatoryMark1;
        private Agent.UI.forms.CustomControls.MandatoryMark mandatoryMark2;
    }
}