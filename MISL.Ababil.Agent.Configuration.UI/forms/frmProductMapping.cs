﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using MISL.Ababil.Agent.Common.UI;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.configuration;
using MISL.Ababil.Agent.Services;

namespace MISL.Ababil.Agent.Configuration.UI.forms
{
    public partial class frmProductMapping : MetroForm
    {
        ProductMappingServices objProductMappingServices = new ProductMappingServices();
        int columnLoaded = 0;

        List<ProductConfigDto> depositMappingList = new List<ProductConfigDto>();
        ProductConfigDto _depositMap = new ProductConfigDto();

        List<ProductConfigDto> SSPMappingList = new List<ProductConfigDto>();
        ProductConfigDto _sspMap = new ProductConfigDto();

        List<ProductConfigDto> MTDRMappingList = new List<ProductConfigDto>();
        ProductConfigDto _mtdrMap = new ProductConfigDto();

        public frmProductMapping()
        {
            InitializeComponent();
            getSetupData();
        }

        private void getSetupData()
        {
            try
            {
                List<ProductType> productTypes = Enum.GetValues(typeof(ProductType)).Cast<ProductType>().ToList<ProductType>();
                productTypes.Remove(productTypes.Where(o => o == ProductType.INVESTMENT).First());
                cmbProductType.DataSource = productTypes;
                cmbProductType.Focus();

                if (cmbProductType.SelectedValue.ToString() == "Deposit") loadDepositMapping();
                else if (cmbProductType.SelectedValue.ToString() == "SSP") loadSSPMapping();
                else if (cmbProductType.SelectedValue.ToString() == "MTDR") loadMTDRMapping();
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
        }
        private void btnAddMapping_Click(object sender, EventArgs e)
        {
            try
            {
                _depositMap = new ProductConfigDto();
                _mtdrMap = new ProductConfigDto();



                switch (cmbProductType.SelectedValue.ToString())
                {
                    case "Deposit":
                        {
                            frmDepositMapping frm = new frmDepositMapping(new Packet
                            {
                                actionType = FormActionType.New
                            }, _depositMap);

                            if (frm.ShowDialog() == DialogResult.OK)
                            {
                                #region Load Deposit Map
                                if (depositMappingList == null) { depositMappingList = new List<ProductConfigDto>(); }

                                _depositMap = frm.newDepositMapping();
                                depositMappingList.Add(_depositMap);

                                dgvProductMapping.DataSource = null;
                                dgvProductMapping.DataSource = depositMappingList.Select(o => new AgentProductGrid(o)
                                {
                                    //Id = o.agentProduct.id,
                                    ProductPrefix = o.agentProduct.productPrefix ?? "",
                                    ProductTitle = o.agentProduct.productTitle ?? ""
                                }).ToList();
                                #endregion
                            }
                            break;
                        }

                    case "SSP":
                        {
                            ProductConfigDto sspMap = new ProductConfigDto();
                            frmSSPMapping frm = new frmSSPMapping(new Packet
                            {
                                actionType = FormActionType.New
                            }, sspMap);

                            if (frm.ShowDialog() == DialogResult.OK)
                            {
                                #region Load SSP Map
                                if (SSPMappingList == null) { SSPMappingList = new List<ProductConfigDto>(); }

                                sspMap = frm.newSSPConfig();
                                sspMap.accountType = AccountType.SSP;
                                //sspMap.termProductType.id = 0;
                                SSPMappingList.Add(sspMap);

                                //if (depositMap != null)
                                //{
                                //    if (depositMap.username == "") { Message.showError("User name is required."); return; }
                                //    if (objUserInformation.password == "") { Message.showError("User password is required."); return; }
                                //    if (objUserInformation.fingerDatas == null) { Message.showError("User finger print  is required."); return; }
                                //}

                                ////gvUserInfo.AutoGenerateColumns = false;
                                dgvProductMapping.DataSource = null;
                                dgvProductMapping.DataSource = SSPMappingList.Select(o => new SSPMappingGrid(o)
                                {
                                    //Id = (long) o.termProductType.id,
                                    ProductPrefix = o.termProductType.productPrefix ?? "",
                                    ProductTitle = o.termProductType.productDescription ?? ""
                                }).ToList();
                                #endregion
                            }

                            break;
                        }

                    case "MTDR":
                        {
                            ProductConfigDto mtdrMap = new ProductConfigDto();
                            frmMTDRMapping frm = new frmMTDRMapping(new Packet
                            {
                                actionType = FormActionType.New
                            }, mtdrMap);

                            if (frm.ShowDialog() == DialogResult.OK)
                            {
                                #region Load MTDR Map
                                if (MTDRMappingList == null) { MTDRMappingList = new List<ProductConfigDto>(); }

                                mtdrMap = frm.newMTDRMapping();
                                mtdrMap.accountType = AccountType.MTDR;
                                MTDRMappingList.Add(mtdrMap);

                                //if (depositMap != null)
                                //{
                                //    if (depositMap.username == "") { Message.showError("User name is required."); return; }
                                //    if (objUserInformation.password == "") { Message.showError("User password is required."); return; }
                                //    if (objUserInformation.fingerDatas == null) { Message.showError("User finger print  is required."); return; }
                                //}

                                ////dgvProductMapping.AutoGenerateColumns = false;
                                dgvProductMapping.DataSource = null;
                                dgvProductMapping.DataSource = MTDRMappingList.Select(o => new MTDRMappingGrid(o)
                                {
                                    //Id = (long) o.termProductType.id,
                                    ProductPrefix = o.termProductType.productPrefix ?? "",
                                    ProductTitle = o.termProductType.productDescription ?? ""
                                }).ToList();
                                #endregion
                            }

                            break;
                        }
                }

                loadEditDeleteButtons();
                dgvProductMapping.Columns[dgvProductMapping.Columns.Count - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dgvProductMapping.Refresh();
            }
            catch (Exception exp)
            { CustomMessage.showError(exp.Message); }
        }
        private void loadEditDeleteButtons()
        {
            try
            {
                if (columnLoaded == 0)
                {
                    DataGridViewDisableButtonColumn buttonColumn = new DataGridViewDisableButtonColumn();
                    buttonColumn.Text = "Delete";
                    buttonColumn.Name = "Delete";
                    buttonColumn.UseColumnTextForButtonValue = true;
                    dgvProductMapping.Columns.Insert(0, buttonColumn);
                    columnLoaded = 1;
                    buttonColumn.Visible = false;

                    DataGridViewDisableButtonColumn buttonEditColumn = new DataGridViewDisableButtonColumn();
                    buttonEditColumn.Text = "Edit";
                    buttonEditColumn.Name = "Edit";
                    buttonEditColumn.UseColumnTextForButtonValue = true;
                    dgvProductMapping.Columns.Insert(1, buttonEditColumn);
                    columnLoaded = 1;
                }
            }
            catch (Exception exp)
            {
                CustomMessage.showError(exp.Message);
            }
        }

        private void _loadDetailsGrid(object sender, EventArgs e)
        {
            if (!cmbProductType.Focused) return;

            if (cmbProductType.SelectedValue.ToString() == "Deposit") loadDepositMapping();
            else if (cmbProductType.SelectedValue.ToString() == "SSP") loadSSPMapping();
            else if (cmbProductType.SelectedValue.ToString() == "MTDR") loadMTDRMapping();
        }
        private void loadDepositMapping()
        {
            try
            {
                ProductMappingServices pruductMapService = new ProductMappingServices();

                depositMappingList = pruductMapService.getDepositMappingInfo();
                dgvProductMapping.DataSource = depositMappingList.Select(o => new AgentProductGrid(o)
                {
                    //Id = o.agentProduct.id,
                    ProductPrefix = o.agentProduct.productPrefix ?? "",
                    ProductTitle = o.agentProduct.productTitle ?? ""
                }).ToList();
                loadEditDeleteButtons();
                dgvProductMapping.Columns[dgvProductMapping.Columns.Count - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dgvProductMapping.Refresh();
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
        }
        private void loadSSPMapping()
        {
            try
            {
                ProductMappingServices pruductMapService = new ProductMappingServices();
                SSPMappingList = new List<ProductConfigDto>();

                SSPMappingList = pruductMapService.getSSPMappingInfo();
                dgvProductMapping.DataSource = SSPMappingList.Select(o => new SSPMappingGrid(o)
                {
                    //Id = (long)o.termProductType.id,
                    ProductPrefix = o.termProductType.productPrefix ?? "",
                    ProductTitle = o.termProductType.productDescription ?? ""
                }).ToList();
                loadEditDeleteButtons();
                dgvProductMapping.Columns[dgvProductMapping.Columns.Count - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dgvProductMapping.Refresh();
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
        }
        private void loadMTDRMapping()
        {
            try
            {
                ProductMappingServices pruductMapService = new ProductMappingServices();

                MTDRMappingList = pruductMapService.getMTDRMappingInfo();
                dgvProductMapping.DataSource = MTDRMappingList.Select(o => new MTDRMappingGrid(o)
                {
                    //Id = (long)o.termProductType.id,
                    ProductPrefix = o.termProductType.productPrefix ?? "",
                    ProductTitle = o.termProductType.productDescription ?? ""
                }).ToList();
                loadEditDeleteButtons();
                dgvProductMapping.Columns[dgvProductMapping.Columns.Count - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dgvProductMapping.Refresh();
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
        }

        private void dgvProductMapping_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int rowIndex = e.RowIndex;
                string deleteResult = "";

                if (dgvProductMapping.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "Delete")
                {
                    #region Delete Single Map
                    switch (cmbProductType.SelectedValue.ToString())
                    {
                        case "Deposit":
                            {
                                #region Delete Deposit
                                string result = CustomMessage.showConfirmation("Are you sure to delete ?");
                                if (result == "yes")
                                {
                                    if (depositMappingList[e.RowIndex].agentProduct.id == 0)
                                    {
                                        depositMappingList.RemoveAt(e.RowIndex);
                                    }
                                    else
                                    {
                                        try
                                        {
                                            ProductMappingServices pruductMapService = new ProductMappingServices();
                                            _depositMap = depositMappingList[e.RowIndex];

                                            //ProgressUIManager.ShowProgress(this);
                                            deleteResult = pruductMapService.deleteDepositMap(_depositMap);
                                            //ProgressUIManager.CloseProgress();

                                            if (deleteResult == "Product deleted successfully")
                                            {
                                                depositMappingList.RemoveAt(e.RowIndex);
                                                CustomMessage.showInformation("Product Configuration deleted successfully");
                                            }
                                            else
                                            { CustomMessage.showWarning("Product Configuration is not deleted successfully"); }
                                        }
                                        catch (Exception ex)
                                        {
                                            CustomMessage.showError(ex.Message);
                                        }
                                    }
                                }

                                dgvProductMapping.DataSource = depositMappingList.Select(o => new AgentProductGrid(o)
                                {
                                    //Id = o.agentProduct.id,
                                    ProductPrefix = o.agentProduct.productPrefix,
                                    ProductTitle = o.agentProduct.productTitle
                                }).ToList();
                                break;
                                #endregion
                            }

                        case "SSP":
                            {
                                #region Delete SSP
                                string result = CustomMessage.showConfirmation("Are you sure to delete ?");
                                if (result == "yes")
                                {
                                    if (SSPMappingList[e.RowIndex].termProductType.id == 0)
                                    {
                                        SSPMappingList.RemoveAt(e.RowIndex);
                                    }
                                    else
                                    {
                                        try
                                        {
                                            ProductMappingServices pruductMapService = new ProductMappingServices();
                                            _sspMap = SSPMappingList[e.RowIndex];

                                            //ProgressUIManager.ShowProgress(this);
                                            deleteResult = pruductMapService.deleteSSPMap(_sspMap);
                                            //ProgressUIManager.CloseProgress();

                                            if (deleteResult == "Product deleted successfully")
                                            {
                                                SSPMappingList.RemoveAt(e.RowIndex);
                                                //Message.showInformation("Agent created successfully");
                                                CustomMessage.showInformation("Product Configuration deleted successfully");
                                            }
                                            else
                                            { CustomMessage.showWarning("Product Configuration is not deleted successfully"); }
                                        }
                                        catch (Exception ex)
                                        {
                                            //ProgressUIManager.CloseProgress();
                                            //Message.showError(ex.Message);
                                            CustomMessage.showError(ex.Message);
                                        }
                                    }
                                }
                                dgvProductMapping.DataSource = SSPMappingList.Select(o => new SSPMappingGrid(o)
                                {
                                    //Id = o.agentProduct.id,
                                    ProductPrefix = o.termProductType.productPrefix,
                                    ProductTitle = o.termProductType.productDescription
                                }).ToList();
                                break;
                                #endregion
                            }

                        case "MTDR":
                            {
                                #region Delete MTDR
                                string result = CustomMessage.showConfirmation("Are you sure to delete ?");
                                if (result == "yes")
                                {
                                    if (MTDRMappingList[e.RowIndex].termProductType.id == 0)
                                    {
                                        MTDRMappingList.RemoveAt(e.RowIndex);
                                    }
                                    else
                                    {
                                        try
                                        {
                                            ProductMappingServices pruductMapService = new ProductMappingServices();
                                            _mtdrMap = MTDRMappingList[e.RowIndex];

                                            //ProgressUIManager.ShowProgress(this);
                                            deleteResult = pruductMapService.deleteMTDRMap(_mtdrMap);
                                            //ProgressUIManager.CloseProgress();

                                            if (deleteResult == "Product deleted successfully")
                                            {
                                                MTDRMappingList.RemoveAt(e.RowIndex);
                                                CustomMessage.showInformation("Product deleted successfully");
                                            }
                                            else
                                            { CustomMessage.showWarning("Product is not deleted successfully"); }
                                        }
                                        catch (Exception ex)
                                        {
                                            CustomMessage.showError(ex.Message);
                                        }
                                    }
                                }
                                dgvProductMapping.DataSource = MTDRMappingList.Select(o => new MTDRMappingGrid(o)
                                {
                                    //Id = (long)o.termProductType.id,
                                    ProductPrefix = o.termProductType.productPrefix ?? "",
                                    ProductTitle = o.termProductType.productDescription ?? ""
                                }).ToList();
                                break;
                                #endregion
                            }
                            #endregion
                    }

                }
                else if (dgvProductMapping.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "Edit")
                {
                    #region Edit Single Map

                    switch (cmbProductType.SelectedValue.ToString())
                    {
                        case "Deposit":
                            {
                                #region Edit Deposit
                                _depositMap = depositMappingList[e.RowIndex];
                                frmDepositMapping frm = null;

                                frm = new frmDepositMapping(new Packet
                                {
                                    actionType = FormActionType.Edit,
                                }, _depositMap);

                                if (frm.ShowDialog() == DialogResult.OK)
                                {
                                    _depositMap = frm.getFilledObject();
                                    depositMappingList[e.RowIndex] = _depositMap;

                                    dgvProductMapping.DataSource = depositMappingList.Select(o => new AgentProductGrid(o)
                                    {
                                        //Id = o.agentProduct.id,
                                        ProductPrefix = o.agentProduct.productPrefix,
                                        ProductTitle = o.agentProduct.productTitle
                                    }).ToList();
                                }

                                break;
                                #endregion
                            }

                        case "SSP":
                            {
                                #region Edit SSP
                                ProductConfigDto sspMap = SSPMappingList[e.RowIndex];
                                frmSSPMapping frm = new frmSSPMapping(new Packet
                                {
                                    actionType = FormActionType.Edit,
                                }, sspMap);
                                if (frm.ShowDialog() == DialogResult.OK)
                                {
                                    sspMap = frm.getFilledObject();
                                    SSPMappingList[e.RowIndex] = sspMap;
                                }
                                dgvProductMapping.DataSource = SSPMappingList.Select(o => new SSPMappingGrid(o)
                                {
                                    //Id = o.agentProduct.id,
                                    ProductPrefix = o.termProductType.productPrefix,
                                    ProductTitle = o.termProductType.productDescription
                                }).ToList();
                                break;
                                #endregion
                            }

                        case "MTDR":
                            {
                                #region Edit MTDR
                                ProductConfigDto mtdrMap = MTDRMappingList[e.RowIndex];
                                frmMTDRMapping frm = null;

                                frm = new frmMTDRMapping(new Packet
                                {
                                    actionType = FormActionType.Edit,
                                }, mtdrMap);
                                frm.ShowDialog();
                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    mtdrMap = frm.getFilledObject();
                                    MTDRMappingList[e.RowIndex] = mtdrMap;
                                }
                                dgvProductMapping.DataSource = MTDRMappingList.Select(o => new MTDRMappingGrid(o)
                                {
                                    //Id = (long)o.termProductType.id,
                                    ProductPrefix = o.termProductType.productPrefix ?? "",
                                    ProductTitle = o.termProductType.productDescription ?? ""
                                }).ToList();
                                break;
                                #endregion
                            }
                            #endregion
                    }
                }

                loadEditDeleteButtons();
                dgvProductMapping.Refresh();
            }
            catch (Exception ex)
            {
                CustomMessage.showError(ex.Message);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string result = CustomMessage.showConfirmation("Are you sure to save ?");
                if (result == "yes")
                {
                    ProductMappingServices pruductMapService = new ProductMappingServices();
                    string saveResult = "";

                    //ProgressUIManager.ShowProgress(this);
                    if (cmbProductType.SelectedItem.ToString() == "Deposit") saveResult = pruductMapService.saveDepositMap(depositMappingList);
                    if (cmbProductType.SelectedItem.ToString() == "SSP") saveResult = pruductMapService.saveSSPMapList(SSPMappingList);
                    if (cmbProductType.SelectedItem.ToString() == "MTDR") saveResult = pruductMapService.saveMTDRMap(MTDRMappingList);
                    //ProgressUIManager.CloseProgress();

                    if (saveResult == "Product saved successfully")
                    { CustomMessage.showInformation("Product configuration saved successfully"); }
                    else
                    { CustomMessage.showInformation("Product configuration is not saved successfully"); }

                    _loadDetailsGrid(null, null);
                }
            }
            catch (Exception exp)
            {
                CustomMessage.showError(exp.Message);
            }
        }

    }


    public class AgentProductGrid
    {
        //public long Id { get; set; }
        public string ProductPrefix { get; set; }
        public string ProductTitle { get; set; }

        private ProductConfigDto _obj;
        public AgentProductGrid(ProductConfigDto obj)
        {
            _obj = obj;
        }
        public ProductConfigDto GetModel()
        {
            return _obj;
        }
    }

    public class SSPMappingGrid
    {
        //public long Id { get; set; }
        public string ProductPrefix { get; set; }
        public string ProductTitle { get; set; }


        private ProductConfigDto _obj;
        public SSPMappingGrid(ProductConfigDto obj)
        {
            _obj = obj;
        }
        public ProductConfigDto GetModel()
        {
            return _obj;
        }
    }

    public class MTDRMappingGrid
    {
        //public long Id { get; set; }
        public string ProductPrefix { get; set; }
        public string ProductTitle { get; set; }


        private ProductConfigDto _obj;
        public MTDRMappingGrid(ProductConfigDto obj)
        {
            _obj = obj;
        }
        public ProductConfigDto GetModel()
        {
            return _obj;
        }
    }

}
