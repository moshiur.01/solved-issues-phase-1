﻿using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.Configuration.UI.forms
{
    partial class frmProductMapping
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbProductType = new MetroFramework.Controls.MetroComboBox();
            this.dgvProductMapping = new System.Windows.Forms.DataGridView();
            this.btnAddMapping = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.customTitlebar1 = new CustomTitlebar();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductMapping)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbProductType
            // 
            this.cmbProductType.FormattingEnabled = true;
            this.cmbProductType.ItemHeight = 23;
            this.cmbProductType.Location = new System.Drawing.Point(155, 36);
            this.cmbProductType.Name = "cmbProductType";
            this.cmbProductType.Size = new System.Drawing.Size(200, 29);
            this.cmbProductType.TabIndex = 102;
            this.cmbProductType.UseSelectable = true;
            this.cmbProductType.SelectedIndexChanged += new System.EventHandler(this._loadDetailsGrid);
            // 
            // dgvProductMapping
            // 
            this.dgvProductMapping.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProductMapping.Location = new System.Drawing.Point(23, 105);
            this.dgvProductMapping.Name = "dgvProductMapping";
            this.dgvProductMapping.Size = new System.Drawing.Size(690, 254);
            this.dgvProductMapping.TabIndex = 104;
            this.dgvProductMapping.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProductMapping_CellContentClick);
            // 
            // btnAddMapping
            // 
            this.btnAddMapping.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnAddMapping.FlatAppearance.BorderSize = 0;
            this.btnAddMapping.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddMapping.ForeColor = System.Drawing.Color.White;
            this.btnAddMapping.Location = new System.Drawing.Point(633, 66);
            this.btnAddMapping.Name = "btnAddMapping";
            this.btnAddMapping.Size = new System.Drawing.Size(80, 29);
            this.btnAddMapping.TabIndex = 105;
            this.btnAddMapping.Text = "Add";
            this.btnAddMapping.UseVisualStyleBackColor = false;
            this.btnAddMapping.Click += new System.EventHandler(this.btnAddMapping_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(544, 369);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(80, 29);
            this.btnSave.TabIndex = 106;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(633, 369);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 29);
            this.btnClose.TabIndex = 107;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(23, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 16);
            this.label3.TabIndex = 111;
            this.label3.Text = "Product Type";
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(739, 26);
            this.customTitlebar1.TabIndex = 100;
            this.customTitlebar1.TabStop = false;
            // 
            // frmProductMapping
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 417);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnAddMapping);
            this.Controls.Add(this.dgvProductMapping);
            this.Controls.Add(this.cmbProductType);
            this.Controls.Add(this.customTitlebar1);
            this.DisplayHeader = false;
            this.Name = "frmProductMapping";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.DropShadow;
            this.Text = "Product Mapping";
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductMapping)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomTitlebar customTitlebar1;
        private MetroFramework.Controls.MetroComboBox cmbProductType;
        private System.Windows.Forms.DataGridView dgvProductMapping;
        private System.Windows.Forms.Button btnAddMapping;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label3;
    }
}