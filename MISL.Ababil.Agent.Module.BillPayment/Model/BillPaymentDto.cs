﻿using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.dto;

namespace MISL.Ababil.Agent.Module.BillPayment.Model
{
    public class BillPaymentDto
    {
        public long billCompanyId { get; set; }
        public BillCollectInformation billCollectInformation { get; set; }
        public string txnUserFingerData { get; set; }
        public AccountOperatorDto accountOperatorDto { get; set; }
        public string terminal { get; set; }
        public string userNarration { get; set; }
    }
}