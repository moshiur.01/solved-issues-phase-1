﻿using MISL.Ababil.Agent.Infrastructure.Models.domain.models;

namespace MISL.Ababil.Agent.Module.BillPayment.Model
{
    public class BillSubZoneInfo
    {
        public long? id { get; set; }
        public long? billZoneInfoId { get; set; }
        //public BillZoneInfo billZoneInfo { get; set; }
        public string billSubZoneName { get; set; }
        public string subZoneShortName { get; set; }
        public string contactNo { get; set; }

        public Status? status { get; set; }



    }
}