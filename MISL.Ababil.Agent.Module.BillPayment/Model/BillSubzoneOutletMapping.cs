﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Module.BillPayment.Model
{
    public class BillSubzoneOutletMapping
    {
        public long id { get; set; }
        public long outletId { get; set; }
        public long? billSubZoneId { get; set; }
    }
}