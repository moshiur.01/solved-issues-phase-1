﻿using System.Collections.Generic;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;

namespace MISL.Ababil.Agent.Module.BillPayment.Model
{
    public class BillCompanyInfo
    {
        public long? id { get; set; }
        public string companyName { get; set; }
        public string shortName { get; set; }
        public string contactNo { get; set; }
        public Status? status { get; set; }
        public List<BillZoneInfo> billZoneInfos { get; set; }
    }
}