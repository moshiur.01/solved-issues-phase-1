﻿using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using MISL.Ababil.Agent.Infrastructure.Models.models.transaction;

namespace MISL.Ababil.Agent.Module.BillPayment.Model
{
    public class BillCollectInformation
    {
        public long? id { get; set; }
        public long? transactionDate { get; set; }
        public long? transactionTime { get; set; }
        public decimal billAmount { get; set; }
        public decimal vatAmount { get; set; }
        public decimal serviceChargeAmount { get; set; }
        public decimal stampChargeAmount { get; set; }
        public string outletUser { get; set; }
        public long? outletId { get; set; }
        public string customerName { get; set; }
        public string customerMobileNo { get; set; }
        public string customerBankAccountNo { get; set; }
        public string customerBillAccNo { get; set; }
        public string billNo { get; set; }
        public string voucherNumber { get; set; }
        public string referenceNumber { get; set; }
        public long? subZoneId { get; set; }
        public long? zoneId { get; set; }
        public PaymentType paymentType { get; set; }
        public long collectionYear { get; set; }
        public Month collectionMonth { get; set; }
        public decimal lateFee { get; set; }
    }
}