﻿namespace MISL.Ababil.Agent.Module.BillPayment.Model
{
    public class BillCustomerInfo
    {
        public long id { get; set; }
        public long billSubZoneId { get; set; }
        public string accountNo { get; set; }
        public string customerName { get; set; }
        public string mobileNo { get; set; }
    }
}