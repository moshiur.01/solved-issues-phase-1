﻿using System.Collections.Generic;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.dto;

namespace MISL.Ababil.Agent.Module.BillPayment.Model
{
    public class BillSubzoneOutletMappingDto
    {
        public long? subZoneId { get; set; }
        public List<SubAgentInfoDto> assigned { get; set; }
        public List<SubAgentInfoDto> unAssigned { get; set; }
    }
}