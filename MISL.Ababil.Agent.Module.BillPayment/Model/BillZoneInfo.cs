﻿using System.Collections.Generic;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.Remittance;

namespace MISL.Ababil.Agent.Module.BillPayment.Model
{
    public class BillZoneInfo
    {
        public long? id { get; set; }
        public long? billCompanyId { get; set; }//
        public BillCompanyInfo billCompanyInfo { get; set; }
        public string zoneName { get; set; }//
        public string zoneShortName { get; set; }//
        public string accountNo { get; set; }//
        public List<BillSubZoneInfo> billSubZoneInfos { get; set; }
        public AccountNature? accountNature { get; set; }//
        public AmountType? chargeType { get; set; }//
        public decimal? chargeAmount { get; set; }
        public decimal? minChargeAmount { get; set; }
        public decimal? maxChargeAmount { get; set; }
        public string vatAccountNo { get; set; }//
        public AccountNature? vatAccountNature { get; set; }//
        public string mobileNo { get; set; }//


    }
}