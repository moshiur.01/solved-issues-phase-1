﻿namespace MISL.Ababil.Agent.Module.BillPayment
{
    partial class frmBillPayment2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.customComboBoxDropDownList2 = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.customComboBoxDropDownList1 = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.lblInWordString = new System.Windows.Forms.Label();
            this.lblInWords = new System.Windows.Forms.Label();
            this.lblBillNo = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblCharge = new System.Windows.Forms.Label();
            this.lblAmount = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.customTextBox8 = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.customTextBox7 = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.customTextBox6 = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.customTextBox5 = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.customTextBox4 = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.customTextBox3 = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.customTextBox2 = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.customTextBox1 = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gbxPayment = new System.Windows.Forms.GroupBox();
            this.mandatoryMark6 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.gbxAccount = new System.Windows.Forms.GroupBox();
            this.txtConsumerAccountA = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgvOperators = new System.Windows.Forms.DataGridView();
            this.ownerIdCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.photoColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.nameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mobileNumberColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.captureColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.lblRequiredFingerPrint = new System.Windows.Forms.Label();
            this.lblBalanceV = new System.Windows.Forms.Label();
            this.lblConsumerTitle = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblBalance = new System.Windows.Forms.Label();
            this.rBtnAccount = new System.Windows.Forms.RadioButton();
            this.rBtnCash = new System.Windows.Forms.RadioButton();
            this.lblPaymentType = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.gbxPayment.SuspendLayout();
            this.gbxAccount.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOperators)).BeginInit();
            this.SuspendLayout();
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = null;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(919, 26);
            this.customTitlebar1.TabIndex = 0;
            this.customTitlebar1.TabStop = false;
            // 
            // customComboBoxDropDownList2
            // 
            this.customComboBoxDropDownList2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.customComboBoxDropDownList2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.customComboBoxDropDownList2.FormattingEnabled = true;
            this.customComboBoxDropDownList2.InputScopeAllowEmpty = false;
            this.customComboBoxDropDownList2.IsValid = null;
            this.customComboBoxDropDownList2.Location = new System.Drawing.Point(90, 23);
            this.customComboBoxDropDownList2.Name = "customComboBoxDropDownList2";
            this.customComboBoxDropDownList2.PromptText = "(Select)";
            this.customComboBoxDropDownList2.ReadOnly = false;
            this.customComboBoxDropDownList2.ShowMandatoryMark = false;
            this.customComboBoxDropDownList2.Size = new System.Drawing.Size(288, 21);
            this.customComboBoxDropDownList2.TabIndex = 55;
            this.customComboBoxDropDownList2.ValidationErrorMessage = "Validation Error!";
            // 
            // customComboBoxDropDownList1
            // 
            this.customComboBoxDropDownList1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.customComboBoxDropDownList1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.customComboBoxDropDownList1.FormattingEnabled = true;
            this.customComboBoxDropDownList1.InputScopeAllowEmpty = false;
            this.customComboBoxDropDownList1.IsValid = null;
            this.customComboBoxDropDownList1.Location = new System.Drawing.Point(90, 55);
            this.customComboBoxDropDownList1.Name = "customComboBoxDropDownList1";
            this.customComboBoxDropDownList1.PromptText = "(Select)";
            this.customComboBoxDropDownList1.ReadOnly = false;
            this.customComboBoxDropDownList1.ShowMandatoryMark = false;
            this.customComboBoxDropDownList1.Size = new System.Drawing.Size(288, 21);
            this.customComboBoxDropDownList1.TabIndex = 54;
            this.customComboBoxDropDownList1.ValidationErrorMessage = "Validation Error!";
            // 
            // lblInWordString
            // 
            this.lblInWordString.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInWordString.Location = new System.Drawing.Point(87, 344);
            this.lblInWordString.Name = "lblInWordString";
            this.lblInWordString.Size = new System.Drawing.Size(221, 52);
            this.lblInWordString.TabIndex = 75;
            // 
            // lblInWords
            // 
            this.lblInWords.AutoSize = true;
            this.lblInWords.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInWords.Location = new System.Drawing.Point(23, 344);
            this.lblInWords.Name = "lblInWords";
            this.lblInWords.Size = new System.Drawing.Size(60, 13);
            this.lblInWords.TabIndex = 74;
            this.lblInWords.Text = "In Words :";
            // 
            // lblBillNo
            // 
            this.lblBillNo.AutoSize = true;
            this.lblBillNo.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBillNo.Location = new System.Drawing.Point(33, 186);
            this.lblBillNo.Name = "lblBillNo";
            this.lblBillNo.Size = new System.Drawing.Size(50, 13);
            this.lblBillNo.TabIndex = 69;
            this.lblBillNo.Text = "Bill No. :";
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(33, 312);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(50, 19);
            this.lblTotal.TabIndex = 73;
            this.lblTotal.Text = "Total :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(50, 250);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 15);
            this.label6.TabIndex = 72;
            this.label6.Text = "VAT :";
            // 
            // lblCharge
            // 
            this.lblCharge.AutoSize = true;
            this.lblCharge.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCharge.Location = new System.Drawing.Point(32, 281);
            this.lblCharge.Name = "lblCharge";
            this.lblCharge.Size = new System.Drawing.Size(51, 15);
            this.lblCharge.TabIndex = 71;
            this.lblCharge.Text = "Charge :";
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmount.Location = new System.Drawing.Point(8, 215);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(75, 20);
            this.lblAmount.TabIndex = 70;
            this.lblAmount.Text = "Amount :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 68;
            this.label3.Text = "Account No. :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 154);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 66;
            this.label5.Text = "Mobile No. :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 67;
            this.label2.Text = "Zone :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(42, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 64;
            this.label4.Text = "Name :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 65;
            this.label1.Text = "Shomity :";
            // 
            // customTextBox8
            // 
            this.customTextBox8.BackColor = System.Drawing.Color.White;
            this.customTextBox8.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.customTextBox8.InputScopeAllowEmpty = false;
            this.customTextBox8.InputScopeCustomString = null;
            this.customTextBox8.IsValid = null;
            this.customTextBox8.Location = new System.Drawing.Point(90, 87);
            this.customTextBox8.MaxLength = 32767;
            this.customTextBox8.MinimumSize = new System.Drawing.Size(0, 21);
            this.customTextBox8.Name = "customTextBox8";
            this.customTextBox8.PromptText = "(Type here...)";
            this.customTextBox8.ReadOnly = false;
            this.customTextBox8.ShowMandatoryMark = false;
            this.customTextBox8.Size = new System.Drawing.Size(288, 21);
            this.customTextBox8.TabIndex = 63;
            this.customTextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.customTextBox8.ValidationErrorMessage = "Validation Error!";
            // 
            // customTextBox7
            // 
            this.customTextBox7.BackColor = System.Drawing.Color.White;
            this.customTextBox7.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.customTextBox7.InputScopeAllowEmpty = false;
            this.customTextBox7.InputScopeCustomString = null;
            this.customTextBox7.IsValid = null;
            this.customTextBox7.Location = new System.Drawing.Point(90, 119);
            this.customTextBox7.MaxLength = 32767;
            this.customTextBox7.MinimumSize = new System.Drawing.Size(0, 21);
            this.customTextBox7.Name = "customTextBox7";
            this.customTextBox7.PromptText = "(Type here...)";
            this.customTextBox7.ReadOnly = false;
            this.customTextBox7.ShowMandatoryMark = false;
            this.customTextBox7.Size = new System.Drawing.Size(288, 21);
            this.customTextBox7.TabIndex = 62;
            this.customTextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.customTextBox7.ValidationErrorMessage = "Validation Error!";
            // 
            // customTextBox6
            // 
            this.customTextBox6.BackColor = System.Drawing.Color.White;
            this.customTextBox6.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.customTextBox6.InputScopeAllowEmpty = false;
            this.customTextBox6.InputScopeCustomString = null;
            this.customTextBox6.IsValid = null;
            this.customTextBox6.Location = new System.Drawing.Point(90, 151);
            this.customTextBox6.MaxLength = 32767;
            this.customTextBox6.MinimumSize = new System.Drawing.Size(0, 21);
            this.customTextBox6.Name = "customTextBox6";
            this.customTextBox6.PromptText = "(Type here...)";
            this.customTextBox6.ReadOnly = false;
            this.customTextBox6.ShowMandatoryMark = false;
            this.customTextBox6.Size = new System.Drawing.Size(165, 21);
            this.customTextBox6.TabIndex = 61;
            this.customTextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.customTextBox6.ValidationErrorMessage = "Validation Error!";
            // 
            // customTextBox5
            // 
            this.customTextBox5.BackColor = System.Drawing.Color.White;
            this.customTextBox5.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.customTextBox5.InputScopeAllowEmpty = false;
            this.customTextBox5.InputScopeCustomString = null;
            this.customTextBox5.IsValid = null;
            this.customTextBox5.Location = new System.Drawing.Point(90, 183);
            this.customTextBox5.MaxLength = 32767;
            this.customTextBox5.MinimumSize = new System.Drawing.Size(0, 21);
            this.customTextBox5.Name = "customTextBox5";
            this.customTextBox5.PromptText = "(Type here...)";
            this.customTextBox5.ReadOnly = false;
            this.customTextBox5.ShowMandatoryMark = false;
            this.customTextBox5.Size = new System.Drawing.Size(165, 21);
            this.customTextBox5.TabIndex = 60;
            this.customTextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.customTextBox5.ValidationErrorMessage = "Validation Error!";
            // 
            // customTextBox4
            // 
            this.customTextBox4.BackColor = System.Drawing.Color.White;
            this.customTextBox4.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.customTextBox4.InputScopeAllowEmpty = false;
            this.customTextBox4.InputScopeCustomString = null;
            this.customTextBox4.IsValid = null;
            this.customTextBox4.Location = new System.Drawing.Point(90, 215);
            this.customTextBox4.MaxLength = 32767;
            this.customTextBox4.MinimumSize = new System.Drawing.Size(0, 21);
            this.customTextBox4.Name = "customTextBox4";
            this.customTextBox4.PromptText = "(Type here...)";
            this.customTextBox4.ReadOnly = false;
            this.customTextBox4.ShowMandatoryMark = false;
            this.customTextBox4.Size = new System.Drawing.Size(165, 21);
            this.customTextBox4.TabIndex = 59;
            this.customTextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.customTextBox4.ValidationErrorMessage = "Validation Error!";
            // 
            // customTextBox3
            // 
            this.customTextBox3.BackColor = System.Drawing.Color.White;
            this.customTextBox3.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.customTextBox3.InputScopeAllowEmpty = false;
            this.customTextBox3.InputScopeCustomString = null;
            this.customTextBox3.IsValid = null;
            this.customTextBox3.Location = new System.Drawing.Point(90, 247);
            this.customTextBox3.MaxLength = 32767;
            this.customTextBox3.MinimumSize = new System.Drawing.Size(0, 21);
            this.customTextBox3.Name = "customTextBox3";
            this.customTextBox3.PromptText = "(Type here...)";
            this.customTextBox3.ReadOnly = false;
            this.customTextBox3.ShowMandatoryMark = false;
            this.customTextBox3.Size = new System.Drawing.Size(165, 21);
            this.customTextBox3.TabIndex = 58;
            this.customTextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.customTextBox3.ValidationErrorMessage = "Validation Error!";
            // 
            // customTextBox2
            // 
            this.customTextBox2.BackColor = System.Drawing.Color.White;
            this.customTextBox2.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.customTextBox2.InputScopeAllowEmpty = false;
            this.customTextBox2.InputScopeCustomString = null;
            this.customTextBox2.IsValid = null;
            this.customTextBox2.Location = new System.Drawing.Point(90, 279);
            this.customTextBox2.MaxLength = 32767;
            this.customTextBox2.MinimumSize = new System.Drawing.Size(0, 21);
            this.customTextBox2.Name = "customTextBox2";
            this.customTextBox2.PromptText = "(Type here...)";
            this.customTextBox2.ReadOnly = false;
            this.customTextBox2.ShowMandatoryMark = false;
            this.customTextBox2.Size = new System.Drawing.Size(165, 21);
            this.customTextBox2.TabIndex = 57;
            this.customTextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.customTextBox2.ValidationErrorMessage = "Validation Error!";
            // 
            // customTextBox1
            // 
            this.customTextBox1.BackColor = System.Drawing.Color.White;
            this.customTextBox1.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.customTextBox1.InputScopeAllowEmpty = false;
            this.customTextBox1.InputScopeCustomString = null;
            this.customTextBox1.IsValid = null;
            this.customTextBox1.Location = new System.Drawing.Point(90, 311);
            this.customTextBox1.MaxLength = 32767;
            this.customTextBox1.MinimumSize = new System.Drawing.Size(0, 21);
            this.customTextBox1.Name = "customTextBox1";
            this.customTextBox1.PromptText = "(Type here...)";
            this.customTextBox1.ReadOnly = false;
            this.customTextBox1.ShowMandatoryMark = false;
            this.customTextBox1.Size = new System.Drawing.Size(165, 21);
            this.customTextBox1.TabIndex = 56;
            this.customTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.customTextBox1.ValidationErrorMessage = "Validation Error!";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.customComboBoxDropDownList2);
            this.groupBox1.Controls.Add(this.customComboBoxDropDownList1);
            this.groupBox1.Controls.Add(this.customTextBox4);
            this.groupBox1.Controls.Add(this.customTextBox2);
            this.groupBox1.Controls.Add(this.customTextBox1);
            this.groupBox1.Controls.Add(this.customTextBox3);
            this.groupBox1.Controls.Add(this.customTextBox8);
            this.groupBox1.Controls.Add(this.customTextBox7);
            this.groupBox1.Controls.Add(this.customTextBox6);
            this.groupBox1.Controls.Add(this.customTextBox5);
            this.groupBox1.Controls.Add(this.lblInWordString);
            this.groupBox1.Controls.Add(this.lblInWords);
            this.groupBox1.Controls.Add(this.lblBillNo);
            this.groupBox1.Controls.Add(this.lblTotal);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.lblCharge);
            this.groupBox1.Controls.Add(this.lblAmount);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 32);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(398, 416);
            this.groupBox1.TabIndex = 92;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bill Information";
            // 
            // gbxPayment
            // 
            this.gbxPayment.Controls.Add(this.mandatoryMark6);
            this.gbxPayment.Controls.Add(this.gbxAccount);
            this.gbxPayment.Controls.Add(this.rBtnAccount);
            this.gbxPayment.Controls.Add(this.rBtnCash);
            this.gbxPayment.Controls.Add(this.lblPaymentType);
            this.gbxPayment.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxPayment.Location = new System.Drawing.Point(416, 32);
            this.gbxPayment.Name = "gbxPayment";
            this.gbxPayment.Size = new System.Drawing.Size(590, 475);
            this.gbxPayment.TabIndex = 93;
            this.gbxPayment.TabStop = false;
            this.gbxPayment.Text = "Payment";
            // 
            // mandatoryMark6
            // 
            this.mandatoryMark6.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark6.Location = new System.Drawing.Point(290, 27);
            this.mandatoryMark6.MaximumSize = new System.Drawing.Size(44, 22);
            this.mandatoryMark6.MinimumSize = new System.Drawing.Size(44, 22);
            this.mandatoryMark6.Name = "mandatoryMark6";
            this.mandatoryMark6.Size = new System.Drawing.Size(44, 22);
            this.mandatoryMark6.TabIndex = 3;
            this.mandatoryMark6.TabStop = false;
            // 
            // gbxAccount
            // 
            this.gbxAccount.Controls.Add(this.txtConsumerAccountA);
            this.gbxAccount.Controls.Add(this.groupBox2);
            this.gbxAccount.Controls.Add(this.lblBalanceV);
            this.gbxAccount.Controls.Add(this.lblConsumerTitle);
            this.gbxAccount.Controls.Add(this.label7);
            this.gbxAccount.Controls.Add(this.label8);
            this.gbxAccount.Controls.Add(this.lblBalance);
            this.gbxAccount.Enabled = false;
            this.gbxAccount.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxAccount.Location = new System.Drawing.Point(8, 67);
            this.gbxAccount.Name = "gbxAccount";
            this.gbxAccount.Size = new System.Drawing.Size(574, 404);
            this.gbxAccount.TabIndex = 4;
            this.gbxAccount.TabStop = false;
            this.gbxAccount.Text = "Account Info";
            // 
            // txtConsumerAccountA
            // 
            this.txtConsumerAccountA.BackColor = System.Drawing.Color.White;
            this.txtConsumerAccountA.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtConsumerAccountA.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtConsumerAccountA.InputScopeAllowEmpty = false;
            this.txtConsumerAccountA.InputScopeCustomString = null;
            this.txtConsumerAccountA.IsValid = null;
            this.txtConsumerAccountA.Location = new System.Drawing.Point(130, 40);
            this.txtConsumerAccountA.MaxLength = 13;
            this.txtConsumerAccountA.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtConsumerAccountA.Name = "txtConsumerAccountA";
            this.txtConsumerAccountA.PromptText = "(Type here...)";
            this.txtConsumerAccountA.ReadOnly = false;
            this.txtConsumerAccountA.ShowMandatoryMark = false;
            this.txtConsumerAccountA.Size = new System.Drawing.Size(134, 30);
            this.txtConsumerAccountA.TabIndex = 19;
            this.txtConsumerAccountA.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtConsumerAccountA.ValidationErrorMessage = "Validation Error!";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgvOperators);
            this.groupBox2.Controls.Add(this.lblRequiredFingerPrint);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox2.Location = new System.Drawing.Point(8, 123);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(559, 274);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Finger Print Information";
            // 
            // dgvOperators
            // 
            this.dgvOperators.AllowUserToAddRows = false;
            this.dgvOperators.AllowUserToDeleteRows = false;
            this.dgvOperators.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvOperators.BackgroundColor = System.Drawing.Color.White;
            this.dgvOperators.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvOperators.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgvOperators.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Turquoise;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvOperators.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvOperators.ColumnHeadersHeight = 28;
            this.dgvOperators.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvOperators.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ownerIdCol,
            this.photoColumn,
            this.nameColumn,
            this.mobileNumberColumn,
            this.captureColumn});
            this.dgvOperators.EnableHeadersVisualStyles = false;
            this.dgvOperators.GridColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgvOperators.Location = new System.Drawing.Point(6, 21);
            this.dgvOperators.Name = "dgvOperators";
            this.dgvOperators.ReadOnly = true;
            this.dgvOperators.RowHeadersVisible = false;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.dgvOperators.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvOperators.RowTemplate.Height = 66;
            this.dgvOperators.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvOperators.Size = new System.Drawing.Size(547, 245);
            this.dgvOperators.TabIndex = 9;
            // 
            // ownerIdCol
            // 
            this.ownerIdCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ownerIdCol.FillWeight = 90F;
            this.ownerIdCol.HeaderText = "ID";
            this.ownerIdCol.Name = "ownerIdCol";
            this.ownerIdCol.ReadOnly = true;
            this.ownerIdCol.Visible = false;
            this.ownerIdCol.Width = 90;
            // 
            // photoColumn
            // 
            this.photoColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.photoColumn.FillWeight = 90F;
            this.photoColumn.HeaderText = "Photo";
            this.photoColumn.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.photoColumn.Name = "photoColumn";
            this.photoColumn.ReadOnly = true;
            this.photoColumn.Width = 90;
            // 
            // nameColumn
            // 
            this.nameColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nameColumn.HeaderText = "Name";
            this.nameColumn.Name = "nameColumn";
            this.nameColumn.ReadOnly = true;
            this.nameColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.nameColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // mobileNumberColumn
            // 
            this.mobileNumberColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.mobileNumberColumn.FillWeight = 120F;
            this.mobileNumberColumn.HeaderText = "Mobile Number";
            this.mobileNumberColumn.Name = "mobileNumberColumn";
            this.mobileNumberColumn.ReadOnly = true;
            this.mobileNumberColumn.Width = 120;
            // 
            // captureColumn
            // 
            this.captureColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.captureColumn.FillWeight = 70F;
            this.captureColumn.HeaderText = "Capture";
            this.captureColumn.Name = "captureColumn";
            this.captureColumn.ReadOnly = true;
            this.captureColumn.Text = "Capture";
            this.captureColumn.Width = 70;
            // 
            // lblRequiredFingerPrint
            // 
            this.lblRequiredFingerPrint.AutoSize = true;
            this.lblRequiredFingerPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequiredFingerPrint.Location = new System.Drawing.Point(255, -2);
            this.lblRequiredFingerPrint.Name = "lblRequiredFingerPrint";
            this.lblRequiredFingerPrint.Size = new System.Drawing.Size(55, 15);
            this.lblRequiredFingerPrint.TabIndex = 1;
            this.lblRequiredFingerPrint.Text = "------------";
            // 
            // lblBalanceV
            // 
            this.lblBalanceV.AutoSize = true;
            this.lblBalanceV.Location = new System.Drawing.Point(358, 49);
            this.lblBalanceV.Name = "lblBalanceV";
            this.lblBalanceV.Size = new System.Drawing.Size(38, 13);
            this.lblBalanceV.TabIndex = 22;
            this.lblBalanceV.Text = "label2";
            // 
            // lblConsumerTitle
            // 
            this.lblConsumerTitle.AutoSize = true;
            this.lblConsumerTitle.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConsumerTitle.Location = new System.Drawing.Point(128, 88);
            this.lblConsumerTitle.Name = "lblConsumerTitle";
            this.lblConsumerTitle.Size = new System.Drawing.Size(106, 17);
            this.lblConsumerTitle.TabIndex = 15;
            this.lblConsumerTitle.Text = "<accountName>";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(15, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 21);
            this.label7.TabIndex = 13;
            this.label7.Text = "Account No :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(22, 89);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 17);
            this.label8.TabIndex = 14;
            this.label8.Text = "Account Name :";
            // 
            // lblBalance
            // 
            this.lblBalance.AutoSize = true;
            this.lblBalance.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBalance.Location = new System.Drawing.Point(278, 43);
            this.lblBalance.Name = "lblBalance";
            this.lblBalance.Size = new System.Drawing.Size(78, 21);
            this.lblBalance.TabIndex = 18;
            this.lblBalance.Text = "Balance :";
            // 
            // rBtnAccount
            // 
            this.rBtnAccount.AutoSize = true;
            this.rBtnAccount.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rBtnAccount.Location = new System.Drawing.Point(204, 26);
            this.rBtnAccount.Name = "rBtnAccount";
            this.rBtnAccount.Size = new System.Drawing.Size(81, 24);
            this.rBtnAccount.TabIndex = 2;
            this.rBtnAccount.Text = "Account";
            this.rBtnAccount.UseVisualStyleBackColor = true;
            // 
            // rBtnCash
            // 
            this.rBtnCash.AutoSize = true;
            this.rBtnCash.Checked = true;
            this.rBtnCash.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rBtnCash.Location = new System.Drawing.Point(136, 26);
            this.rBtnCash.Name = "rBtnCash";
            this.rBtnCash.Size = new System.Drawing.Size(58, 24);
            this.rBtnCash.TabIndex = 1;
            this.rBtnCash.TabStop = true;
            this.rBtnCash.Text = "Cash";
            this.rBtnCash.UseVisualStyleBackColor = true;
            // 
            // lblPaymentType
            // 
            this.lblPaymentType.AutoSize = true;
            this.lblPaymentType.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold);
            this.lblPaymentType.Location = new System.Drawing.Point(10, 28);
            this.lblPaymentType.Name = "lblPaymentType";
            this.lblPaymentType.Size = new System.Drawing.Size(116, 20);
            this.lblPaymentType.TabIndex = 0;
            this.lblPaymentType.Text = "Payment Type :";
            // 
            // frmBillPayment2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(919, 497);
            this.Controls.Add(this.gbxPayment);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.customTitlebar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmBillPayment2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bill Payment";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbxPayment.ResumeLayout(false);
            this.gbxPayment.PerformLayout();
            this.gbxAccount.ResumeLayout(false);
            this.gbxAccount.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOperators)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private CustomControls.CustomTitlebar customTitlebar1;
        private CustomControls.CustomTextBox customTextBox1;
        private CustomControls.CustomTextBox customTextBox2;
        private CustomControls.CustomTextBox customTextBox3;
        private CustomControls.CustomTextBox customTextBox4;
        private CustomControls.CustomTextBox customTextBox5;
        private CustomControls.CustomTextBox customTextBox6;
        private CustomControls.CustomTextBox customTextBox7;
        private CustomControls.CustomTextBox customTextBox8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.Label lblCharge;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblBillNo;
        private System.Windows.Forms.Label lblInWords;
        private System.Windows.Forms.Label lblInWordString;
        private CustomControls.CustomComboBoxDropDownList customComboBoxDropDownList1;
        private CustomControls.CustomComboBoxDropDownList customComboBoxDropDownList2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gbxPayment;
        private UI.forms.CustomControls.MandatoryMark mandatoryMark6;
        private System.Windows.Forms.GroupBox gbxAccount;
        private CustomControls.CustomTextBox txtConsumerAccountA;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgvOperators;
        private System.Windows.Forms.DataGridViewTextBoxColumn ownerIdCol;
        private System.Windows.Forms.DataGridViewImageColumn photoColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mobileNumberColumn;
        private System.Windows.Forms.DataGridViewButtonColumn captureColumn;
        private System.Windows.Forms.Label lblRequiredFingerPrint;
        private System.Windows.Forms.Label lblBalanceV;
        private System.Windows.Forms.Label lblConsumerTitle;
        private System.Windows.Forms.Label lblBalance;
        private System.Windows.Forms.RadioButton rBtnAccount;
        private System.Windows.Forms.RadioButton rBtnCash;
        private System.Windows.Forms.Label lblPaymentType;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
    }
}