﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using MISL.Ababil.Agent.Module.BillPayment.Service;
using MISL.Ababil.Agent.Module.Common.UI.MessageUI;
using MISL.Ababil.Agent.Report;
using MISL.Ababil.Agent.Report.DataSets;
using MISL.Ababil.Agent.Report.Reports;
using MISL.Ababil.Agent.Services;

namespace MISL.Ababil.Agent.Module.BillPayment.UI
{
    public partial class frmDepositSlip : CustomForm
    {
        public GUI gui = new GUI();
        List<AgentInformation> objAgentInfoList = new List<AgentInformation>();
        AgentServices objAgentServices = new AgentServices();
        AgentInformation agentInformation = new AgentInformation();
        AgentServices agentServices = new AgentServices();
        //DailyElecticityBillCollectionDS agentDS = new DailyElecticityBillCollectionDS();
        BillCompanyDepositSlip _billCompanyDepositSlip=new BillCompanyDepositSlip();
        private AmountInWords amountInWords = new AmountInWords();


        //public List<AccountMonitoringInfo> _accountMonitoringList = new List<AccountMonitoringInfo>();
        public frmDepositSlip()
        {
            InitializeComponent();
            GetSetupData();
            try
            {
                // cmbAgentName.SelectedIndex = -1;
            }
            catch (Exception exp)
            { }
            controlActivity();
            ConfigUIEnhancement();
        }

        public void ConfigUIEnhancement()
        {
            gui = new GUI(this);
            //gui.Config(ref cmbAgentName, ValidCheck.VALIDATIONTYPES.COMBOBOX_EMPTY, null);
            //gui.Config(ref cmbSubAgnetName);
            gui.Config(ref dtpFromDate);
           // gui.Config(ref dtpToDate);
        }

        private void controlActivity()
        {
            try
            {
                if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_CENTRALLY.ToString())) //branch user
                {
                    cmbAgentName.Enabled = true;
                    cmbSubAgnetName.Enabled = true;
                }
                else if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_AGENTWISE.ToString())) //agent user
                {
                    cmbAgentName.SelectedValue = UtilityServices.getCurrentAgent().id;
                    cmbAgentName.Enabled = false;
                    cmbSubAgnetName.Enabled = true;
                    agentInformation = agentServices.getAgentInfoById(UtilityServices.getCurrentAgent().id.ToString());
                    setSubagent();
                }
                else
                {
                    SubAgentInformation currentSubagentInfo = UtilityServices.getCurrentSubAgent();
                    cmbAgentName.SelectedValue = currentSubagentInfo.agent.id;
                    agentInformation = agentServices.getAgentInfoById(currentSubagentInfo.agent.id.ToString());
                    setSubagent();
                    cmbAgentName.Enabled = false;
                    cmbSubAgnetName.SelectedValue = currentSubagentInfo.id;
                    cmbSubAgnetName.Enabled = false;
                    setZoneList();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //cmbAgentName.Enabled = true;
                //cmbSubAgnetName.Enabled = true;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            try
            {
                if (CheckInput())
                {
                    Cursor.Current = Cursors.WaitCursor;
                    //chacking valid date
                    DateTime fromDate, toDate;
                    try
                    {
                        //fromDate = UtilityServices.ParseDateTime(dtpFromDate.Date);                
                        fromDate = DateTime.ParseExact(dtpFromDate.Date.Replace("/", "-"), "dd-MM-yyyy",
                            CultureInfo.InvariantCulture);
                    }
                    catch (Exception ex)
                    {
                        Cursor.Current = Cursors.Default;
                        MsgBox.ShowError(ex.Message);
                        MessageBox.Show("Please enter the Date in correct format.", this.Text, MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);
                        return;
                    }
                    try
                    {
                        //toDate = UtilityServices.ParseDateTime(dtpToDate.Date);
                        //toDate = DateTime.ParseExact(dtpToDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    catch (Exception ex)
                    {
                        Cursor.Current = Cursors.Default;
                        MsgBox.ShowError(ex.Message);
                        MessageBox.Show("Please enter the Date in correct format.", this.Text, MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);
                        return;
                    }

                    //if (SessionInfo.currentDate < toDate)
                    //{
                    //    Cursor.Current = Cursors.Default;
                    //    MessageBox.Show("Future date is not allowed!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    //    return;
                    //}
                    //if (fromDate > toDate)
                    //{
                    //    Cursor.Current = Cursors.Default;
                    //    MessageBox.Show("From date can not be greater than to date.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    //    return;
                    //}

                    //if (cmbAgentName.SelectedIndex > 0)
                    //{
                    LoadAccountMonitoring();
                    crBillDepositSlip report = new crBillDepositSlip();
                    frmReportViewer viewer = new frmReportViewer();

                    ReportHeaders rptHeaders = new ReportHeaders();
                    rptHeaders = UtilityServices.getReportHeaders("Daily Electricity Bill Collection");

                    TextObject txtBankName = report.ReportDefinition.ReportObjects["txtBankName"] as TextObject;
                    TextObject txtDate = report.ReportDefinition.ReportObjects["txtDate"] as TextObject;
                    TextObject txtMobileNo = report.ReportDefinition.ReportObjects["txtMobileNo"] as TextObject;
                    TextObject txtOutletName = report.ReportDefinition.ReportObjects["txtOutletName"] as TextObject;
                    TextObject txtAccName = report.ReportDefinition.ReportObjects["txtAccName"] as TextObject;
                    TextObject txtDepAccNo = report.ReportDefinition.ReportObjects["txtDepAccNo"] as TextObject;
                    TextObject txtTotalClient = report.ReportDefinition.ReportObjects["txtTotalClient"] as TextObject;
                    TextObject txtTotalTaka = report.ReportDefinition.ReportObjects["txtTotalTaka"] as TextObject;
                    TextObject txtInWord = report.ReportDefinition.ReportObjects["txtInWord"] as TextObject;




                    TextObject txtBankNameV = report.ReportDefinition.ReportObjects["txtBankNameV"] as TextObject;
                    TextObject txtdateV = report.ReportDefinition.ReportObjects["txtdateV"] as TextObject;
                    TextObject txtMobileNoV = report.ReportDefinition.ReportObjects["txtMobileNoV"] as TextObject;
                    TextObject txtOutletNameV = report.ReportDefinition.ReportObjects["txtOutletNameV"] as TextObject;
                    TextObject txtVatAccName = report.ReportDefinition.ReportObjects["txtVatAccName"] as TextObject;
                    TextObject txtVatAccNo = report.ReportDefinition.ReportObjects["txtVatAccNo"] as TextObject;
                    TextObject txtTotalclientV = report.ReportDefinition.ReportObjects["txtTotalclientV"] as TextObject;
                    TextObject txtTotalTkVat = report.ReportDefinition.ReportObjects["txtTotalTkVat"] as TextObject;
                    TextObject txtInWordvat = report.ReportDefinition.ReportObjects["txtInWordvat"] as TextObject;





                    //TextObject txtFromDate = report.ReportDefinition.ReportObjects["txtFromDate"] as TextObject;
                    //TextObject txtToDate = report.ReportDefinition.ReportObjects["txtToDate"] as TextObject;

                    if (rptHeaders != null)
                    {
                        if (rptHeaders.branchDto != null)
                        {
                            txtBankName.Text = rptHeaders.branchDto.bankName;
                            txtBankNameV.Text = rptHeaders.branchDto.bankName;
                            //txtBranchName.Text = rptHeaders.branchDto.branchName;
                            //txtBranchAddress.Text = rptHeaders.branchDto.branchAddress;
                        }
                        //txtReportHeading.Text = rptHeaders.reportHeading;
                        //txtPrintUser.Text = rptHeaders.printUser;
                        //txtPrintDate.Text = rptHeaders.printDate.ToString("dd-MM-yyyy").Replace("-", "/");
                    }
                    //txtFromDate.Text = Convert.ToDateTime(dtpFromDate.Text).ToString("dd/MM/yyyy");
                    //txtToDate.Text = Convert.ToDateTime(dtpToDate.Text).ToString("dd/MM/yyyy");

                    //txtFromDate.Text = dtpFromDate.Date;
                    //txtToDate.Text = dtpToDate.Date;


                    // report.SetDataSource(agentDS);


                    txtDate.Text = dtpFromDate.Value.ToString("dd-MM-yyyy");
                    txtMobileNo.Text = String.IsNullOrEmpty(_billCompanyDepositSlip.zoneMobileNo)
                        ? ""
                        : _billCompanyDepositSlip.zoneMobileNo;
                    txtOutletName.Text = ((SubAgentInformation) cmbSubAgnetName.SelectedItem).name;
                    txtAccName.Text = String.IsNullOrEmpty(_billCompanyDepositSlip.zoneAccountName)
                        ? ""
                        : _billCompanyDepositSlip.zoneAccountName;
                    txtDepAccNo.Text = String.IsNullOrEmpty(_billCompanyDepositSlip.zoneAccountNo)
                        ? ""
                        : _billCompanyDepositSlip.zoneAccountNo;
                    txtTotalClient.Text = _billCompanyDepositSlip.noOfBillCollect.ToString();
                    txtTotalTaka.Text = _billCompanyDepositSlip.totalBillAmount.ToString();
                    txtInWord.Text = amountInWords.ToWords(_billCompanyDepositSlip.totalBillAmount.ToString());



                    txtdateV.Text = dtpFromDate.Value.ToString("dd-MM-yyyy");
                    txtMobileNoV.Text = String.IsNullOrEmpty(_billCompanyDepositSlip.zoneMobileNo)
                        ? ""
                        : _billCompanyDepositSlip.zoneMobileNo;
                    txtOutletNameV.Text = ((SubAgentInformation) cmbSubAgnetName.SelectedItem).name;
                    txtVatAccName.Text = String.IsNullOrEmpty(_billCompanyDepositSlip.zoneVatAccountName)
                        ? ""
                        : _billCompanyDepositSlip.zoneVatAccountName;
                    txtVatAccNo.Text = String.IsNullOrEmpty(_billCompanyDepositSlip.zoneVatAccountNo)
                        ? ""
                        : _billCompanyDepositSlip.zoneVatAccountNo;
                    txtTotalclientV.Text = _billCompanyDepositSlip.noOfBillCollect.ToString();
                    txtTotalTkVat.Text = _billCompanyDepositSlip.totalVatAmount.ToString();
                    txtInWordvat.Text = amountInWords.ToWords(_billCompanyDepositSlip.totalVatAmount.ToString());


                    viewer.crvReportViewer.ReportSource = report;

                    Cursor.Current = Cursors.Default;
                    viewer.Show(this.Parent);
                    //}
                    //else
                    //{
                    //    Cursor.Current = Cursors.Default;
                    //    MsgBox.showWarning("Please select an option!");
                    //}
                    Cursor.Current = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Default;
                MsgBox.ShowError(ex.Message);
            }
        
        }

        private bool CheckInput()
        {
            if (cmbAgentName.SelectedIndex < 1)
            {
                MsgBox.ShowError("Please select an agent.");
                return false;
            }
            if (cbxZone.SelectedIndex < 0)
            {
                MsgBox.ShowError("Please select a somity.");
                return false;
            }
            if (cmbAgentName.SelectedIndex < 1)
            {
                MsgBox.ShowError("Please select an agent.");
                return false;
            }
            if (cmbSubAgnetName.SelectedIndex < 1)
            {
                MsgBox.ShowError("Please select an outlet.");
                return false;
            }
            return true;
        }

        private void LoadAccountMonitoring()
        {
            //AccountMonitoringInfo accountMonitoringInfo;
            //_accountMonitoringList.Clear();
            BillCompanyDepositSlip accountSearchDto = FillAccountSearchDto();
            ServiceResult result;
           // agentDS = new DailyElecticityBillCollectionDS();

            try
            {
                result = AgentServices.GetDailyDepositSlip(accountSearchDto);

                if (!result.Success)
                {
                    MessageBox.Show(result.Message);
                    return;
                }

                _billCompanyDepositSlip = result.ReturnedObject as BillCompanyDepositSlip;

                //if (accountMonitoring != null)
                //{
                //    foreach (DailyBillCollectionReportResultDto account in accountMonitoring)
                //    {
                //        DailyElecticityBillCollectionDS.DailyElecticityBillCollectionDSRow newRow = agentDS._DailyElecticityBillCollectionDS.NewDailyElecticityBillCollectionDSRow();

                //        newRow.agentId = account.agentId.ToString();
                //        newRow.agentName = account.agentName;

                //        newRow.outletId = account.outletId.ToString();
                //        newRow.outletName = account.outletName;

                //        newRow.zoneId = account.zoneId.ToString();
                //        newRow.zoneName = account.zoneName;

                //        newRow.subZoneId = account.subZoneId.ToString();
                //        newRow.subZoneName = account.subZoneName;


                //        newRow.trDateTime = account.transactionDateTime;

                //        newRow.customerBillAccNo = account.customerBillAccNo ;
                //        newRow.customerName = account.customerName ;
                //        newRow.customerMobileNo = account.customerMobileNo;

                //        newRow.collectionMonthYear = account.collectionYearMonth;

                //        newRow.serviceChargeAmount = account.serviceChargeAmount ?? 0;
                //        newRow.stampChargeAmount = account.stampChargeAmount ?? 0;
                //        newRow.vatAmount = account.vatAmount ?? 0;
                //        newRow.lateFee = account.lateFee ?? 0;
                //        newRow.billAmount = account.billAmount ?? 0;
                       

                //        agentDS._DailyElecticityBillCollectionDS.AddDailyElecticityBillCollectionDSRow(newRow);
                //    }
                //}
                //agentDS.AcceptChanges();
            }
            catch (Exception)
            {
                //ignored
            }
        }

        private BillCompanyDepositSlip FillAccountSearchDto()
        {
            BillCompanyDepositSlip billCompanyDepositSlip=new BillCompanyDepositSlip();
            //DailyBillCollectionReportSearchDto accountSearchDto = new DailyBillCollectionReportSearchDto();

            //if (this.cmbAgentName.SelectedIndex > -1)
            //{
            //    if (cmbAgentName.SelectedIndex != 0 || cmbAgentName.SelectedIndex != 1)
            //    {
            //        accountSearchDto.agentId = (long)cmbAgentName.SelectedValue ;
            //    }
            //    if (cmbAgentName.SelectedIndex == 1 || cmbAgentName.SelectedIndex == 0)
            //    {
            //        accountSearchDto.agentId = null; // new AgentInformation();
            //    }

            //}
            if (this.cmbSubAgnetName.SelectedIndex > -1)
            {
                if (cmbSubAgnetName.SelectedIndex != 0 || cmbSubAgnetName.SelectedIndex != 1)
                {
                    billCompanyDepositSlip.outletId = (long)cmbSubAgnetName.SelectedValue;
                }
                if (cmbSubAgnetName.SelectedIndex == 1 || cmbSubAgnetName.SelectedIndex == 0)
                {
                    billCompanyDepositSlip.outletId = null; // new SubAgentInformation();
                }

            }
            if (this.cbxZone.SelectedIndex > -1)
            {
               
                 
                    billCompanyDepositSlip.billZoneId = (long)cbxZone.SelectedValue;
                

            }
            //if (this.cbxSubZone.SelectedIndex > -1)
            //{
            //    if (cbxSubZone.SelectedIndex != 0 || cbxSubZone.SelectedIndex != 1)
            //    {
            //        billCompanyDepositSlip.subZoneId = (long)cmbSubAgnetName.SelectedValue;
            //    }
            //    if (cbxSubZone.SelectedIndex == 1 || cbxSubZone.SelectedIndex == 0)
            //    {
            //        billCompanyDepositSlip.subZoneId = null; // new SubAgentInformation();
            //    }

            //}

            billCompanyDepositSlip.reportdate = UtilityServices.GetLongDate
            (
                DateTime.ParseExact(dtpFromDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture)
            );

            

            return billCompanyDepositSlip;
        }

        private void GetSetupData()
        {
            //string configvalue1 = ConfigurationManager.AppSettings["countryId"];
            try
            {
                setAgentList();
                setZoneList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void setZoneList()
        {
            if (cmbSubAgnetName.SelectedValue == null)
            {
                return;
            }
            long r = ((SubAgentInformation)cmbSubAgnetName.SelectedItem).id;
            UtilityServices.fillComboBox(cbxZone, new BindingSource()
            {
                DataSource = new BillPaymentService().GetBillZoneInfosByCompanyId(r, 1)
            }, "zoneName", "id");
            cbxZone.SelectedIndex = -1;
        }

        private void setAgentList()
        {
            objAgentInfoList = objAgentServices.getAgentInfoBranchWise();
            BindingSource bs = new BindingSource();
            bs.DataSource = objAgentInfoList;

            AgentInformation agSelect = new AgentInformation();
            agSelect.businessName = "(Select)";
            objAgentInfoList.Insert(0, agSelect);
            AgentInformation agAll = new AgentInformation();
            agAll.businessName = "(All)";
            objAgentInfoList.Insert(1, agAll);

            UtilityServices.fillComboBox(cmbAgentName, bs, "businessName", "id");
            //            cmbAgentName.Text = "Select";
            cmbAgentName.SelectedIndex = 0;
        }
        private void cmbAgentName_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!cb.Focused)
            {
                return;
            }

            try
            {
                if (cmbAgentName.SelectedIndex > 1)
                {
                    agentInformation = agentServices.getAgentInfoById(cmbAgentName.SelectedValue.ToString());
                }
                if (cmbAgentName.SelectedIndex == 1)
                {
                    //agentInformation = agentServices.getAgentInfoById(null);
                }
                if (cmbAgentName.SelectedIndex < 2)
                {
                    cmbSubAgnetName.DataSource = null;
                    cmbSubAgnetName.Items.Clear();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            setSubagent();
        }
        private void setSubagent()
        {
            if (agentInformation != null)
            {
                BindingSource bs = new BindingSource();
                bs.DataSource = agentInformation.subAgents;

                try
                {
                    SubAgentInformation saiSelect = new SubAgentInformation();
                    saiSelect.name = "(Select)";

                    SubAgentInformation saiAll = new SubAgentInformation();
                    saiAll.name = "(All)";

                    agentInformation.subAgents.Insert(0, saiSelect);
                    agentInformation.subAgents.Insert(1, saiAll);
                }
                catch (Exception exp)
                { }

                UtilityServices.fillComboBox(cmbSubAgnetName, bs, "name", "id");
                if (cmbSubAgnetName.Items.Count > 0)
                {
                    cmbSubAgnetName.SelectedIndex = 0;
                }
            }
        }

        private void frmTrMonitoringRport_Load(object sender, EventArgs e)
        {
            dtpFromDate.Value = SessionInfo.currentDate;
            //dtpToDate.Value = SessionInfo.currentDate;
        }

       

        private void frmAccountMonitoringRport_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

      

        

        

        

        

      
     

        private void cbxZone_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            cbxSubZone.SelectedIndex = -1;
        }

        private void cbxZone_SelectedValueChanged_1(object sender, EventArgs e)
        {
            if (!cbxZone.Focused)
            {
                return;
            }

            UtilityServices.fillComboBox(cbxSubZone, new BindingSource()
            {
                DataSource = new BillPaymentService().GetBillZoneInfoWithSubZoneByZoneId((long)cbxZone.SelectedValue).billSubZoneInfos
            }, "billSubZoneName", "id");

        }

        private void cmbAgentName_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!cb.Focused)
            {
                return;
            }

            try
            {
                if (cmbAgentName.SelectedIndex > 1)
                {
                    agentInformation = agentServices.getAgentInfoById(cmbAgentName.SelectedValue.ToString());
                }
                if (cmbAgentName.SelectedIndex == 1)
                {
                    //agentInformation = agentServices.getAgentInfoById(null);
                }
                if (cmbAgentName.SelectedIndex < 2)
                {
                    cmbSubAgnetName.DataSource = null;
                    cmbSubAgnetName.Items.Clear();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            setSubagent();
        }

        private void cmbSubAgnetName_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!cb.Focused)
            {
                return;
            }
            setZoneList();
        }
    }
    //public class AccountMonitoringInfo
    //{
    //    public long AgentId = 0;
    //    public string AgentName = "";
    //    public long SubAgentId = 0;
    //    public string SubAgentName = "";
    //    public decimal? AgentAccBalance = 0;

    //    public long NoOfMSDAccount = 0;
    //    public decimal MSDAccBalance = 0;

    //    public long NoOfCDAccount = 0;
    //    public decimal CDAccBalance = 0;

    //    public long NoOfITDAccount = 0;
    //    public decimal ITDAccBalance = 0;

    //    public long NoOfMTDAccount = 0;
    //    public decimal MTDAccBalance = 0;

    //    public long NoOfInvestmentAccount = 0;
    //    public decimal InvestedAmount = 0;
    //}


}
