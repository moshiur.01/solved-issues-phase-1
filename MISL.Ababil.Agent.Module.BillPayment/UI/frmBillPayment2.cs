﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using MISL.Ababil.Agent.GenericFingerprintServices;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.consumer;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Infrastructure.Models.models.transaction;
using MISL.Ababil.Agent.Module.BillPayment.Model;
using MISL.Ababil.Agent.Module.BillPayment.Service;
using MISL.Ababil.Agent.Module.Common.UI.MessageUI;
using MISL.Ababil.Agent.Module.Common.UI.ProgressUI;
using MISL.Ababil.Agent.Report;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.UI.forms.ProgressUI;

namespace MISL.Ababil.Agent.Module.BillPayment.UI
{
    public partial class frmBillPayment2 : Form, FingerprintEventObserver
    {
        private Packet _packet;
        private AccountOperatorDto _accountOperatorDto;
        private ConsumerInformationDto _consumerInformationDto;
        private string _captureFor = "";
        private int _captureIndexNo = -1;
        private byte[] _subagentFingerData;
        private int _noOfCapturefinger;

        private long _outletId = -1;


        public frmBillPayment2(Packet packet)
        {
            _packet = packet;
            InitializeComponent();

            SetupDataLoad();
        }

        private void SetupDataLoad()
        {
            //--Zone--
            UtilityServices.fillComboBox(cbxZone, new BindingSource()
            {
                DataSource = new BillPaymentService().GetBillZoneInfosByCompanyId(SessionInfo.userBasicInformation.outlet.id)
            }, "zoneName", "id");
            cbxZone.SelectedIndex = -1;
            //--------

            cbxYear.Items.Add(DateTime.Now.Year);
            cbxYear.Items.Add(DateTime.Now.Year - 1);
            cbxYear.Items.Add(DateTime.Now.Year - 2);
            cbxYear.SelectedIndex = 0;

            List<Month> monthList = Enum.GetValues(typeof(Month))
                            .Cast<Month>()
                            .ToList();
            cbxMonth.DataSource = monthList;
            cbxMonth.SelectedIndex = -1;
        }

        private void cbxZone_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            cbxSubZone.SelectedIndex = -1;
        }

        private void btnPay_Click(object sender, System.EventArgs e)
        {
            if (MsgBox.showConfirmation("Are you sure to pay?") == "yes")
            {
                _captureFor = "subagent";
                FingerprintServiceFactory.FingerprintServiceFactory factory = new FingerprintServiceFactory.FingerprintServiceFactory();
                FingerprintDevice device = factory.getFingerprintDevice();
                device.registerEventObserver(this);
                device.capture();
            }
        }

        private void cbxZone_SelectedValueChanged(object sender, EventArgs e)
        {
            if (!cbxZone.Focused)
            {
                return;
            }

            UtilityServices.fillComboBox(cbxSubZone, new BindingSource()
            {
                DataSource = new BillPaymentService().GetBillZoneInfoWithSubZoneByZoneId((long)cbxZone.SelectedValue).billSubZoneInfos
            }, "billSubZoneName", "id");
        }

        private void txtAmount_Leave(object sender, EventArgs e)
        {
            Sum();
        }

        private void Sum()
        {
            try
            {
                //txtCharge.Text = new BillPaymentService().GetBillCharge(1, decimal.Parse(txtAmount.Text)).ToString("##.##");
                txtStampCharge.Text = new BillPaymentService().GetBillStampCharge(1, decimal.Parse(txtAmount.Text) + decimal.Parse(txtLateFee.Text) + decimal.Parse(txtVAT.Text)).ToString("##.##");
                if (string.IsNullOrEmpty(txtStampCharge.Text))
                {
                    txtStampCharge.Text = "0";
                }

                if (string.IsNullOrEmpty(txtLateFee.Text))
                {
                    txtLateFee.Text = "0";
                }
                if (string.IsNullOrEmpty(txtVAT.Text))
                {
                    txtVAT.Text = "0";
                }


                txtCharge.Text = new BillPaymentService().GetBillCharge(1,
                        decimal.Parse(txtAmount.Text)
                        + decimal.Parse(txtLateFee.Text)
                        //+ decimal.Parse(txtVAT.Text)
                    ).ToString("##.##");

                txtTotal.Text =
                   (decimal.Parse(txtAmount.Text)
                    + decimal.Parse(txtLateFee.Text)
                    + decimal.Parse(txtCharge.Text)
                    //+ decimal.Parse(txtVAT.Text)
                    //+ decimal.Parse(txtStampCharge.Text)//
                    ).ToString();
            }
            catch (Exception ex)
            {
                txtCharge.Text = "";
                txtTotal.Text = "";
            }
        }

        public void FingerPrintEventOccured(FingerprintEvents eventSpec, object eventData)
        {
            if (eventData == null)
            {
                MsgBox.showWarning("Fingerprint data not found!");
                return;
            }

            if (_captureFor == "consumer")
            {

                _accountOperatorDto.operators[dgvOperators.SelectedRows[0].Index].fingerData
                    = (string)eventData;
                return;
            }

            if (_captureFor == "subagent")
            {
                ProgressUIManager.ShowProgress(this);
                try
                {
                    string retVal = new BillPaymentService().DoBillPayment(new BillPaymentDto()
                    {
                        billCollectInformation = new BillCollectInformation()
                        {
                            zoneId = (long)cbxZone.SelectedValue,
                            subZoneId = (long)cbxSubZone.SelectedValue,
                            customerName = txtName.Text,
                            customerBillAccNo = txtAccountNo.Text,
                            customerMobileNo = txtMobileNo.Text,
                            billNo = txtBillNo.Text,
                            billAmount = decimal.Parse(txtAmount.Text),
                            vatAmount = decimal.Parse(txtVAT.Text),
                            serviceChargeAmount = decimal.Parse(txtCharge.Text),
                            //stampChargeAmount = decimal.Parse(txtStampCharge.Text),//
                            paymentType = rBtnCash.Checked ? PaymentType.Cash : PaymentType.Account,
                            customerBankAccountNo = !rBtnCash.Checked ? txtAccountNumber.Text : "",
                            lateFee = decimal.Parse(txtLateFee.Text),
                            collectionYear = long.Parse(cbxYear.SelectedItem.ToString()),
                            collectionMonth = (Month)cbxMonth.SelectedItem
                        },
                        billCompanyId = 1,
                        txnUserFingerData = (string)eventData,
                        accountOperatorDto = _accountOperatorDto,
                    });
                    ProgressUIManager.CloseProgress();
                    MsgBox.showInformation("Bill successfully received with voucher number: " + retVal);
                    try
                    {
                        frmShowReport objfrmShowReport = new frmShowReport();
                        objfrmShowReport.BillapymentSlip(retVal);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError("Report generate problem: " + ex.Message);
                    }

                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                catch (Exception ex)
                {
                    ProgressUIManager.CloseProgress();
                    MsgBox.ShowError(ex.Message);
                }
                ProgressUIManager.CloseProgress();
            }
        }

        private void rBtnAccount_CheckedChanged(object sender, EventArgs e)
        {
            gbxAccount.Enabled = rBtnAccount.Checked;
        }

        private void txtAccountNumber_Leave(object sender, EventArgs e)
        {
            LoadSourceConsumerInformation();
        }

        private void LoadSourceConsumerInformation()
        {
            if (txtAccountNumber.Text != "")
            {
                try
                {
                    _consumerInformationDto =
                        new ConsumerServices().getConsumerInformationDtoByAcc(txtAccountNumber.Text.Trim());
                    lblAccountTitle.Text = _consumerInformationDto.consumerTitle;
                    lblBalanceV.Text = (_consumerInformationDto.balance ?? 0).ToString("N", new CultureInfo("BN-BD"));

                    //_accountOperatorDto =
                    //    new DepositAccountServices().GetAccountOperatorsByAccountNo(txtAccountNumber.Text.Trim());
                    _accountOperatorDto = new AccountInformationService().GetAccountOperatorDtoByAccountNumber(txtAccountNumber.Text.Trim());

                    dgvOperators.DataSource = null;
                    dgvOperators.Rows.Clear();

                    for (int i = 0; i < _accountOperatorDto.operators.Count; i++)
                    {
                        dgvOperators.Rows.Add
                            (
                                null,
                               // _accountOperatorDto.operators[i].photo?.Length == 0 ? null : _accountOperatorDto.operators[i].photo,
                                _accountOperatorDto.operators[i].individualName,
                                _accountOperatorDto.operators[i].mobileNo,
                                "Capture"
                            );
                    }
                }
                catch (Exception ex)
                {
                    //ClearUI();
                    MsgBox.showWarning(ex.Message);
                }
            }
        }

        private void dgvOperators_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 3 && _accountOperatorDto.operators.Count > 0)
            {
                try
                {
                    if (txtAccountNumber.Text != "" && txtAmount.Text != "")
                    {
                        _captureIndexNo = e.RowIndex;
                        _captureFor = "consumer";
                        FingerprintServiceFactory.FingerprintServiceFactory factory = new FingerprintServiceFactory.FingerprintServiceFactory();
                        FingerprintDevice device = factory.getFingerprintDevice();
                        device.registerEventObserver(this);
                        device.capture();
                    }
                    else
                    {
                        MsgBox.showInformation("Account number or amount may be blank.");
                    }
                }
                catch (Exception ex)
                {
                    MsgBox.showWarning("No operator found for capture.");
                }
            }
        }

        private void txtVAT_Leave(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtLateFee.Text))
            {
                txtLateFee.Text = "0";
            }
            Sum();
        }

        private void txtTotal_TextChangedExt(object sender, EventArgs e)
        {
            try
            {
                lblInWordString.Text = new AmountInWords().ToWords(txtTotal.Text) + " Only.";
            }
            catch (Exception ex)
            {
                lblInWordString.Text = "ERROR";
            }
        }

        private void txtAccountNo_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtAccountNo.Text.Trim()))
            {
                BillCustomerInfo billCustomerInfo = new BillPaymentService().GetBillCustomerInfoByAccNoAndSubzoneId(txtAccountNo.Text.Trim(), (long)cbxSubZone.SelectedValue);
                txtName.Text = billCustomerInfo?.customerName;
                txtMobileNo.Text = billCustomerInfo?.mobileNo;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}