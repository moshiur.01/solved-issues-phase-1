﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Module.BillPayment.Model;
using MISL.Ababil.Agent.Module.BillPayment.Service;
using MISL.Ababil.Agent.Module.Common.UI;
using MISL.Ababil.Agent.Module.Common.UI.MessageUI;
using MISL.Ababil.Agent.Module.Common.UI.ProgressUI;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.Services.Communication;
using MISL.Ababil.Agent.UI;
using MISL.Ababil.Agent.UI.forms.ProgressUI;

namespace MISL.Ababil.Agent.Module.BillPayment.UI.ConfigUI
{
    public partial class frmZoneSetup : CustomForm
    {
        private GUI _gui;
        private object _companyid = 1;
        private List<BillSubZoneInfo> _billSubZoneInfos;
        private const string ButtonHeaderTextDelete = "Delete";

        public frmZoneSetup()
        {
            InitializeComponent();
            _gui = new GUI(this);
            _gui.Config(ref txtSubZoneName, GUIValidation.VALIDATIONTYPES.TEXTBOX_EMPTY, null);
            _gui.Config(ref txtSubZoneShortName, GUIValidation.VALIDATIONTYPES.TEXTBOX_EMPTY, null);
            _gui.Config(ref txtMobileNumber, GUIValidation.VALIDATIONTYPES.TEXTBOX_NUMBER_MOBILE, null);
            ConfigUI();
            cbxCompanyId.SelectedIndex = 0;
        }

        private void ConfigUI()
        {
            UtilityServices.fillComboBox(cbxShomity, new BindingSource()
            {
                DataSource = new WebClientCommunicator<object, List<BillZoneInfo>>().GetResult($"resources/bill/allzones/companyid/{_companyid}")
            }, "zoneName", "id");

            try
            {
                customDataGridViewHeader.ExportReportSubtitleThree = "";
                DataGridHeaderUtility.ReportHederDataExportToPdf(customDataGridViewHeader);
            }
            catch
            {
                //suppressed
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (_gui.IsAllControlValidatedEx())
            {
                try
                {
                    ProgressUIManager.ShowProgress(this);
                    new BillPaymentService().AddNewSubZone(new BillSubZoneInfo()
                    {
                        billZoneInfoId = ((BillZoneInfo)cbxShomity.SelectedItem).id,
                        billSubZoneName = txtSubZoneName.Text,
                        subZoneShortName = txtSubZoneShortName.Text,
                        contactNo = txtMobileNumber.Text, //?? throw new NullReferenceException()
                    });
                    ProgressUIManager.CloseProgress();
                    MsgBox.showInformation("New sub-zone is successfully saved.");
                    Search();
                }
                catch (Exception exception)
                {
                    ProgressUIManager.CloseProgress();
                    MsgBox.ShowError(exception.Message);
                }
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            txtSubZoneName.Text = string.Empty;
            txtSubZoneShortName.Text = string.Empty;
            txtMobileNumber.Text = string.Empty;
        }

        private void cbxShomity_SelectedIndexChanged(object sender, EventArgs e)
        {
            Search();
        }

        private void Search()
        {
            try
            {
                dgv.DataSource = null;
                dgv.Rows.Clear();
                dgv.Columns.Clear();

                //_billSubZoneInfos = new WebClientCommunicator<object, List<BillSubZoneInfo>>().GetResult(
                //    $"resources/bill/zone/id/{((BillZoneInfo)cbxShomity.SelectedItem).id}");

                _billSubZoneInfos = new WebClientCommunicator<object, List<BillSubZoneInfo>>().GetResult(
                    $"resources/bill/subzones/{((BillZoneInfo)cbxShomity.SelectedItem).id}");

                dgv.DataSource = null;
                dgv.DataSource = _billSubZoneInfos.Select(o => new BillSubZoneInfoGrid(o)
                {
                    ID = o.id,
                    SubZoneName = o.billSubZoneName,
                    SubZoneShortName = o.subZoneShortName,
                    ContactNo = o.contactNo,
                    filter =
                    (
                        o.id.ToString() +
                        o.billSubZoneName ?? "".ToString() +
                        o.subZoneShortName ?? "".ToString() +
                        o.contactNo.ToString()
                    ).ToLower()
                }).ToList();

                dgv.Columns.Insert(dgv.Columns.Count - 1
                    , new DataGridViewButtonColumn()
                    {
                        Name = ButtonHeaderTextDelete,
                        Text = ButtonHeaderTextDelete,
                        UseColumnTextForButtonValue = true,
                    });
                dgv.Columns[dgv.Columns.Count - 1].Visible = false;
            }
            catch (Exception exception)
            {

            }

        }

        private class BillSubZoneInfoGrid
        {
            public long? ID { get; set; }

            public string SubZoneName { get; set; }
            public string SubZoneShortName { get; set; }

            public string ContactNo { get; set; }

            public string filter { get; set; }

            private BillSubZoneInfo _billSubZoneInfo;

            public BillSubZoneInfoGrid(BillSubZoneInfo billSubZoneInfo)
            {
                _billSubZoneInfo = billSubZoneInfo;
            }

            public BillSubZoneInfo GetModel()
            {
                return _billSubZoneInfo;
            }
        }

        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                switch (dgv.Columns[e.ColumnIndex].Name)
                {
                    case ButtonHeaderTextDelete:
                        {
                            if (MsgBox.showConfirmation("Are you sure delete?") == "yes")
                            {
                                new WebClientCommunicator<BillSubZoneInfo, object>().GetPostResult
                                (
                                    new BillSubZoneInfo()
                                    {
                                        id = long.Parse(dgv.Rows[e.RowIndex].Cells[0].Value.ToString())
                                    },
                                    $"resources/bill/subzoneinfo/delete"
                                );
                                MsgBox.showInformation("Deleted Successfully.");
                                Search();
                            }
                        }
                        break;
                }
            }
            catch (Exception exception)
            {
                MsgBox.ShowError(exception.Message);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}