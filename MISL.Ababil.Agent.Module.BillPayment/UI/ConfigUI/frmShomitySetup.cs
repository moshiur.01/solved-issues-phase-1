﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.Remittance;
using MISL.Ababil.Agent.Module.BillPayment.Model;
using MISL.Ababil.Agent.Module.BillPayment.Service;

using MISL.Ababil.Agent.Module.Common.UI.MessageUI;
using MISL.Ababil.Agent.Module.Common.UI.ProgressUI;
using MISL.Ababil.Agent.Services.Communication;
using MISL.Ababil.Agent.UI;
using MISL.Ababil.Agent.UI.forms.ProgressUI;
using MISL.Ababil.Agent.Module.Common.UI;

namespace MISL.Ababil.Agent.Module.BillPayment.UI.ConfigUI
{
    public partial class frmShomitySetup : CustomForm
    {
        private int _companyid = 1;
        private List<BillZoneInfo> _billZoneInfos;
        private GUI _gui;
        private const string ButtonHeaderTextDelete = "Delete";

        public frmShomitySetup()
        {
            InitializeComponent();
            _gui = new GUI(this);
            _gui.Config(ref txtShomityName, GUIValidation.VALIDATIONTYPES.TEXTBOX_EMPTY, null);
            _gui.Config(ref txtShomityShortName, GUIValidation.VALIDATIONTYPES.TEXTBOX_EMPTY, null);
            _gui.Config(ref txtMobileNumber, GUIValidation.VALIDATIONTYPES.TEXTBOX_NUMBER_MOBILE, null);
            _gui.Config(ref txtAccountNumber, GUIValidation.VALIDATIONTYPES.TEXTBOX_EMPTY, null);
            _gui.Config(ref txtVATAccountNumber, GUIValidation.VALIDATIONTYPES.TEXTBOX_EMPTY, null);
            cbxCompanyId.SelectedIndex = 0;

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (_gui.IsAllControlValidatedEx())
            {
                try
                {
                    ProgressUIManager.ShowProgress(this);
                    new BillPaymentService().AddNewZone(new BillZoneInfo()
                    {
                        billCompanyId = 1,
                        zoneName = txtShomityName.Text,
                        zoneShortName = txtShomityShortName.Text,
                        mobileNo = txtMobileNumber.Text,
                        accountNo = txtAccountNumber.Text,
                        vatAccountNo = txtVATAccountNumber.Text,
                    });
                    ProgressUIManager.CloseProgress();
                    MsgBox.showInformation("New zone is successfully saved.");
                    Search();
                }
                catch (Exception exception)
                {
                    ProgressUIManager.CloseProgress();
                    MsgBox.ShowError(exception.Message);
                }
            }
        }

        private void loadZones_Click(object sender, EventArgs e)
        {
            Search();
            try
            {
                customDataGridViewHeader.ExportReportSubtitleThree = "";
                DataGridHeaderUtility.ReportHederDataExportToPdf(customDataGridViewHeader);
            }
            catch
            {
                //suppressed
            }
        }

        private void Search()
        {
            dgv.DataSource = null;
            dgv.Rows.Clear();
            dgv.Columns.Clear();

            _billZoneInfos = new WebClientCommunicator<object, List<BillZoneInfo>>().GetResult(
                $"resources/bill/allzones/companyid/{_companyid}");

            dgv.DataSource = null;
            dgv.DataSource = _billZoneInfos.Select(o => new BillZoneInfoGrid(o)
            {
                ID = o.id,
                Zone = o.zoneName,
                ZoneShortName = o.zoneShortName,
                SubZoneCount = o.billSubZoneInfos?.Count,
                Account = o.accountNo,
                mobileNo = o.mobileNo ?? "",
                filter =
                (
                    o.id.ToString() +
                    o.zoneName ?? "".ToString() +
                    o.zoneShortName ?? "".ToString() +
                    o.billSubZoneInfos?.Count.ToString() +
                    o.accountNo?.ToString() +
                    o.mobileNo ?? "".ToString()
                ).ToLower()
            }).ToList();

            dgv.Columns.Insert(dgv.Columns.Count - 1
                , new DataGridViewButtonColumn()
                {
                    Name = ButtonHeaderTextDelete,
                    Text = ButtonHeaderTextDelete,
                    UseColumnTextForButtonValue = true,
                });
            dgv.Columns[4].Visible = false;
            dgv.Columns[7].Visible = false;

        }

        public class BillZoneInfoGrid
        {
            public long? ID { get; set; }
            //public long? billCompanyId { get; set; }//
            //public BillCompanyInfo billCompanyInfo { get; set; }
            //public string Company { get; set; }
            public string Zone { get; set; }//
            public string ZoneShortName { get; set; }//
            public string Account { get; set; }//
            //public List<BillSubZoneInfo> billSubZoneInfos { get; set; }
            public int? SubZoneCount { get; set; }
            //public AccountNature? accountNature { get; set; }//
            //public AmountType? chargeType { get; set; }//
            //public decimal? chargeAmount { get; set; }
            //public decimal? minChargeAmount { get; set; }
            //public decimal? maxChargeAmount { get; set; }
            //public string vatAccountNo { get; set; }//
            //public AccountNature? vatAccountNature { get; set; }//
            public string mobileNo { get; set; }//

            public string filter { get; set; }

            private BillZoneInfo _billZoneInfo;

            public BillZoneInfoGrid(BillZoneInfo billZoneInfo)
            {
                _billZoneInfo = billZoneInfo;
            }

            public BillZoneInfo GetModel()
            {
                return _billZoneInfo;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            txtShomityName.Text = string.Empty;
            txtShomityShortName.Text = string.Empty;
            txtMobileNumber.Text = string.Empty;
            txtAccountNumber.Text = string.Empty;
            txtVATAccountNumber.Text = string.Empty;
        }

        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ProgressUIManager.ShowProgress(this);
                switch (dgv.Columns[e.ColumnIndex].Name)
                {
                    case ButtonHeaderTextDelete:
                        {
                            if (MsgBox.showConfirmation("Are you sure delete?") == "yes")
                            {
                                new WebClientCommunicator<BillZoneInfo, object>().GetPostResult
                                (
                                    new BillZoneInfo()
                                    {
                                        id = long.Parse(dgv.Rows[e.RowIndex].Cells[0].Value.ToString())
                                    },
                                    $"resources/bill/zoneinfo/delete"
                                );
                                MsgBox.showInformation("The zone is successfully deleted.");
                            }
                        }
                        break;
                }
                ProgressUIManager.CloseProgress();
                Search();
            }
            catch (Exception exception)
            {
                ProgressUIManager.CloseProgress();
                MsgBox.ShowError(exception.Message);
            }
        }

        private void txtShomityName_Load(object sender, EventArgs e)
        {

        }
    }
}