﻿namespace MISL.Ababil.Agent.Module.BillPayment.UI.ConfigUI
{
    partial class frmZoneSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblCompanyId = new System.Windows.Forms.Label();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.btnClose = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lblShomity = new System.Windows.Forms.Label();
            this.cbxCompanyId = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cbxShomity = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.txtMobileNumber = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtSubZoneShortName = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtSubZoneName = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.lblMobileNumber = new System.Windows.Forms.Label();
            this.lblSubZoneShortName = new System.Windows.Forms.Label();
            this.lblSubZoneName = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.customDataGridViewHeader = new MISL.Ababil.Agent.Module.Common.Exporter.CustomDataGridViewHeader();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCompanyId
            // 
            this.lblCompanyId.AutoSize = true;
            this.lblCompanyId.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblCompanyId.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompanyId.ForeColor = System.Drawing.Color.White;
            this.lblCompanyId.Location = new System.Drawing.Point(222, 36);
            this.lblCompanyId.Name = "lblCompanyId";
            this.lblCompanyId.Size = new System.Drawing.Size(79, 15);
            this.lblCompanyId.TabIndex = 1;
            this.lblCompanyId.Text = "Company ID :";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(113)))), ((int)(((byte)(198)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.ColumnHeadersHeight = 28;
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.GridColor = System.Drawing.Color.LightGray;
            this.dgv.Location = new System.Drawing.Point(12, 246);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(867, 216);
            this.dgv.TabIndex = 170;
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(776, 475);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(103, 30);
            this.btnClose.TabIndex = 173;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.label2.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(5, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Zone Setup";
            // 
            // lblShomity
            // 
            this.lblShomity.AutoSize = true;
            this.lblShomity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblShomity.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShomity.ForeColor = System.Drawing.Color.White;
            this.lblShomity.Location = new System.Drawing.Point(244, 65);
            this.lblShomity.Name = "lblShomity";
            this.lblShomity.Size = new System.Drawing.Size(57, 15);
            this.lblShomity.TabIndex = 1;
            this.lblShomity.Text = "Shomity :";
            // 
            // cbxCompanyId
            // 
            this.cbxCompanyId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxCompanyId.Enabled = false;
            this.cbxCompanyId.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxCompanyId.FormattingEnabled = true;
            this.cbxCompanyId.InputScopeAllowEmpty = false;
            this.cbxCompanyId.IsValid = null;
            this.cbxCompanyId.Items.AddRange(new object[] {
            "Pally Biddut Samity"});
            this.cbxCompanyId.Location = new System.Drawing.Point(307, 34);
            this.cbxCompanyId.Name = "cbxCompanyId";
            this.cbxCompanyId.PromptText = "(Select)";
            this.cbxCompanyId.ReadOnly = false;
            this.cbxCompanyId.ShowMandatoryMark = false;
            this.cbxCompanyId.Size = new System.Drawing.Size(336, 21);
            this.cbxCompanyId.TabIndex = 2;
            this.cbxCompanyId.ValidationErrorMessage = "Validation Error!";
            // 
            // cbxShomity
            // 
            this.cbxShomity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxShomity.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxShomity.FormattingEnabled = true;
            this.cbxShomity.InputScopeAllowEmpty = false;
            this.cbxShomity.IsValid = null;
            this.cbxShomity.Location = new System.Drawing.Point(307, 63);
            this.cbxShomity.Name = "cbxShomity";
            this.cbxShomity.PromptText = "(Select)";
            this.cbxShomity.ReadOnly = false;
            this.cbxShomity.ShowMandatoryMark = false;
            this.cbxShomity.Size = new System.Drawing.Size(336, 21);
            this.cbxShomity.TabIndex = 2;
            this.cbxShomity.ValidationErrorMessage = "Validation Error!";
            this.cbxShomity.SelectedIndexChanged += new System.EventHandler(this.cbxShomity_SelectedIndexChanged);
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(891, 103);
            this.customTitlebar1.TabIndex = 0;
            this.customTitlebar1.TabStop = false;
            // 
            // txtMobileNumber
            // 
            this.txtMobileNumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtMobileNumber.BackColor = System.Drawing.Color.White;
            this.txtMobileNumber.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtMobileNumber.InputScopeAllowEmpty = false;
            this.txtMobileNumber.InputScopeCustomString = null;
            this.txtMobileNumber.IsValid = null;
            this.txtMobileNumber.Location = new System.Drawing.Point(243, 178);
            this.txtMobileNumber.MaxLength = 32767;
            this.txtMobileNumber.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtMobileNumber.Name = "txtMobileNumber";
            this.txtMobileNumber.PromptText = "(Type here...)";
            this.txtMobileNumber.ReadOnly = false;
            this.txtMobileNumber.ShowMandatoryMark = true;
            this.txtMobileNumber.Size = new System.Drawing.Size(231, 21);
            this.txtMobileNumber.TabIndex = 189;
            this.txtMobileNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtMobileNumber.UpperCaseOnly = false;
            this.txtMobileNumber.ValidationErrorMessage = "Input required !";
            // 
            // txtSubZoneShortName
            // 
            this.txtSubZoneShortName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtSubZoneShortName.BackColor = System.Drawing.Color.White;
            this.txtSubZoneShortName.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtSubZoneShortName.InputScopeAllowEmpty = false;
            this.txtSubZoneShortName.InputScopeCustomString = null;
            this.txtSubZoneShortName.IsValid = null;
            this.txtSubZoneShortName.Location = new System.Drawing.Point(243, 149);
            this.txtSubZoneShortName.MaxLength = 32767;
            this.txtSubZoneShortName.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtSubZoneShortName.Name = "txtSubZoneShortName";
            this.txtSubZoneShortName.PromptText = "(Type here...)";
            this.txtSubZoneShortName.ReadOnly = false;
            this.txtSubZoneShortName.ShowMandatoryMark = true;
            this.txtSubZoneShortName.Size = new System.Drawing.Size(231, 21);
            this.txtSubZoneShortName.TabIndex = 190;
            this.txtSubZoneShortName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtSubZoneShortName.UpperCaseOnly = false;
            this.txtSubZoneShortName.ValidationErrorMessage = "Input required !";
            // 
            // txtSubZoneName
            // 
            this.txtSubZoneName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtSubZoneName.BackColor = System.Drawing.Color.White;
            this.txtSubZoneName.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtSubZoneName.InputScopeAllowEmpty = false;
            this.txtSubZoneName.InputScopeCustomString = null;
            this.txtSubZoneName.IsValid = null;
            this.txtSubZoneName.Location = new System.Drawing.Point(243, 120);
            this.txtSubZoneName.MaxLength = 32767;
            this.txtSubZoneName.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtSubZoneName.Name = "txtSubZoneName";
            this.txtSubZoneName.PromptText = "(Type here...)";
            this.txtSubZoneName.ReadOnly = false;
            this.txtSubZoneName.ShowMandatoryMark = true;
            this.txtSubZoneName.Size = new System.Drawing.Size(474, 21);
            this.txtSubZoneName.TabIndex = 191;
            this.txtSubZoneName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtSubZoneName.UpperCaseOnly = false;
            this.txtSubZoneName.ValidationErrorMessage = "Input required !";
            // 
            // lblMobileNumber
            // 
            this.lblMobileNumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblMobileNumber.AutoSize = true;
            this.lblMobileNumber.Location = new System.Drawing.Point(153, 181);
            this.lblMobileNumber.Name = "lblMobileNumber";
            this.lblMobileNumber.Size = new System.Drawing.Size(84, 13);
            this.lblMobileNumber.TabIndex = 186;
            this.lblMobileNumber.Text = "Mobile Number :";
            // 
            // lblSubZoneShortName
            // 
            this.lblSubZoneShortName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblSubZoneShortName.AutoSize = true;
            this.lblSubZoneShortName.Location = new System.Drawing.Point(140, 152);
            this.lblSubZoneShortName.Name = "lblSubZoneShortName";
            this.lblSubZoneShortName.Size = new System.Drawing.Size(97, 13);
            this.lblSubZoneShortName.TabIndex = 187;
            this.lblSubZoneShortName.Text = "Zone Short Name :";
            // 
            // lblSubZoneName
            // 
            this.lblSubZoneName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblSubZoneName.AutoSize = true;
            this.lblSubZoneName.Location = new System.Drawing.Point(168, 123);
            this.lblSubZoneName.Name = "lblSubZoneName";
            this.lblSubZoneName.Size = new System.Drawing.Size(69, 13);
            this.lblSubZoneName.TabIndex = 188;
            this.lblSubZoneName.Text = "Zone Name :";
            // 
            // btnClear
            // 
            this.btnClear.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(614, 175);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(103, 28);
            this.btnClear.TabIndex = 184;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Location = new System.Drawing.Point(505, 175);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(103, 28);
            this.btnAdd.TabIndex = 185;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // customDataGridViewHeader
            // 
            this.customDataGridViewHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customDataGridViewHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(123)))), ((int)(((byte)(208)))));
            this.customDataGridViewHeader.ExportReportSubtitleOne = "AIBL";
            this.customDataGridViewHeader.ExportReportSubtitleThree = "Subtitle Three";
            this.customDataGridViewHeader.ExportReportSubtitleTwo = "Report";
            this.customDataGridViewHeader.ExportReportTitle = "Remittance";
            this.customDataGridViewHeader.Filter = this.dgv;
            this.customDataGridViewHeader.HeaderText = "Zone";
            this.customDataGridViewHeader.Location = new System.Drawing.Point(12, 218);
            this.customDataGridViewHeader.Name = "customDataGridViewHeader";
            this.customDataGridViewHeader.Size = new System.Drawing.Size(867, 28);
            this.customDataGridViewHeader.TabIndex = 171;
            // 
            // frmZoneSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(891, 517);
            this.Controls.Add(this.cbxShomity);
            this.Controls.Add(this.cbxCompanyId);
            this.Controls.Add(this.txtMobileNumber);
            this.Controls.Add(this.txtSubZoneShortName);
            this.Controls.Add(this.txtSubZoneName);
            this.Controls.Add(this.lblMobileNumber);
            this.Controls.Add(this.lblSubZoneShortName);
            this.Controls.Add(this.lblSubZoneName);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.customDataGridViewHeader);
            this.Controls.Add(this.lblShomity);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblCompanyId);
            this.Controls.Add(this.customTitlebar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmZoneSetup";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Shomity Setup";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomControls.CustomTitlebar customTitlebar1;
        private System.Windows.Forms.Label lblCompanyId;
        private CustomControls.CustomComboBoxDropDownList cbxCompanyId;
        private Common.Exporter.CustomDataGridViewHeader customDataGridViewHeader;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label2;
        private CustomControls.CustomComboBoxDropDownList cbxShomity;
        private System.Windows.Forms.Label lblShomity;
        private CustomControls.CustomTextBox txtMobileNumber;
        private CustomControls.CustomTextBox txtSubZoneShortName;
        private CustomControls.CustomTextBox txtSubZoneName;
        private System.Windows.Forms.Label lblMobileNumber;
        private System.Windows.Forms.Label lblSubZoneShortName;
        private System.Windows.Forms.Label lblSubZoneName;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnAdd;
    }
}