﻿namespace MISL.Ababil.Agent.Module.BillPayment.UI.ConfigUI
{
    partial class frmShomitySetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.lblCompanyId = new System.Windows.Forms.Label();
            this.cbxCompanyId = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.customDataGridViewHeader = new MISL.Ababil.Agent.Module.Common.Exporter.CustomDataGridViewHeader();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.btnClose = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAccountNumber = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.lblAccountNumber = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.lblShomityName = new System.Windows.Forms.Label();
            this.txtShomityName = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.lblShomityShortName = new System.Windows.Forms.Label();
            this.txtShomityShortName = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.lblVATAccountNumber = new System.Windows.Forms.Label();
            this.txtVATAccountNumber = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMobileNumber = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.loadZones = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(967, 68);
            this.customTitlebar1.TabIndex = 0;
            this.customTitlebar1.TabStop = false;
            // 
            // lblCompanyId
            // 
            this.lblCompanyId.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblCompanyId.AutoSize = true;
            this.lblCompanyId.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblCompanyId.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompanyId.ForeColor = System.Drawing.Color.White;
            this.lblCompanyId.Location = new System.Drawing.Point(213, 36);
            this.lblCompanyId.Name = "lblCompanyId";
            this.lblCompanyId.Size = new System.Drawing.Size(65, 15);
            this.lblCompanyId.TabIndex = 1;
            this.lblCompanyId.Text = "Company :";
            // 
            // cbxCompanyId
            // 
            this.cbxCompanyId.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbxCompanyId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxCompanyId.Enabled = false;
            this.cbxCompanyId.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxCompanyId.FormattingEnabled = true;
            this.cbxCompanyId.InputScopeAllowEmpty = false;
            this.cbxCompanyId.IsValid = null;
            this.cbxCompanyId.Items.AddRange(new object[] {
            "Pally Biddut Samity"});
            this.cbxCompanyId.Location = new System.Drawing.Point(282, 34);
            this.cbxCompanyId.Name = "cbxCompanyId";
            this.cbxCompanyId.PromptText = "(Select)";
            this.cbxCompanyId.ReadOnly = false;
            this.cbxCompanyId.ShowMandatoryMark = false;
            this.cbxCompanyId.Size = new System.Drawing.Size(352, 21);
            this.cbxCompanyId.TabIndex = 2;
            this.cbxCompanyId.ValidationErrorMessage = "Validation Error!";
            // 
            // customDataGridViewHeader
            // 
            this.customDataGridViewHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customDataGridViewHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(123)))), ((int)(((byte)(208)))));
            this.customDataGridViewHeader.ExportReportSubtitleOne = "AIBL";
            this.customDataGridViewHeader.ExportReportSubtitleThree = "Subtitle Three";
            this.customDataGridViewHeader.ExportReportSubtitleTwo = "Report";
            this.customDataGridViewHeader.ExportReportTitle = "Remittance";
            this.customDataGridViewHeader.Filter = this.dgv;
            this.customDataGridViewHeader.HeaderText = "Zone";
            this.customDataGridViewHeader.Location = new System.Drawing.Point(12, 178);
            this.customDataGridViewHeader.Name = "customDataGridViewHeader";
            this.customDataGridViewHeader.Size = new System.Drawing.Size(943, 28);
            this.customDataGridViewHeader.TabIndex = 171;
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(113)))), ((int)(((byte)(198)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.ColumnHeadersHeight = 28;
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.GridColor = System.Drawing.Color.LightGray;
            this.dgv.Location = new System.Drawing.Point(12, 206);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(943, 313);
            this.dgv.TabIndex = 170;
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(852, 530);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(103, 28);
            this.btnClose.TabIndex = 173;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.label2.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(5, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Shomity Setup";
            // 
            // txtAccountNumber
            // 
            this.txtAccountNumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtAccountNumber.BackColor = System.Drawing.Color.White;
            this.txtAccountNumber.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtAccountNumber.InputScopeAllowEmpty = false;
            this.txtAccountNumber.InputScopeCustomString = null;
            this.txtAccountNumber.IsValid = null;
            this.txtAccountNumber.Location = new System.Drawing.Point(511, 114);
            this.txtAccountNumber.MaxLength = 32767;
            this.txtAccountNumber.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtAccountNumber.Name = "txtAccountNumber";
            this.txtAccountNumber.PromptText = "(Type here...)";
            this.txtAccountNumber.ReadOnly = false;
            this.txtAccountNumber.ShowMandatoryMark = true;
            this.txtAccountNumber.Size = new System.Drawing.Size(180, 21);
            this.txtAccountNumber.TabIndex = 177;
            this.txtAccountNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtAccountNumber.UpperCaseOnly = false;
            this.txtAccountNumber.ValidationErrorMessage = "Input required !";
            // 
            // lblAccountNumber
            // 
            this.lblAccountNumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblAccountNumber.AutoSize = true;
            this.lblAccountNumber.Location = new System.Drawing.Point(412, 117);
            this.lblAccountNumber.Name = "lblAccountNumber";
            this.lblAccountNumber.Size = new System.Drawing.Size(93, 13);
            this.lblAccountNumber.TabIndex = 176;
            this.lblAccountNumber.Text = "Account Number :";
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Location = new System.Drawing.Point(725, 136);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(103, 28);
            this.btnAdd.TabIndex = 173;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnClear
            // 
            this.btnClear.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(834, 136);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(103, 28);
            this.btnClear.TabIndex = 173;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // lblShomityName
            // 
            this.lblShomityName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblShomityName.AutoSize = true;
            this.lblShomityName.Location = new System.Drawing.Point(56, 88);
            this.lblShomityName.Name = "lblShomityName";
            this.lblShomityName.Size = new System.Drawing.Size(81, 13);
            this.lblShomityName.TabIndex = 176;
            this.lblShomityName.Text = "Shomity Name :";
            // 
            // txtShomityName
            // 
            this.txtShomityName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtShomityName.BackColor = System.Drawing.Color.White;
            this.txtShomityName.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtShomityName.InputScopeAllowEmpty = false;
            this.txtShomityName.InputScopeCustomString = null;
            this.txtShomityName.IsValid = null;
            this.txtShomityName.Location = new System.Drawing.Point(143, 85);
            this.txtShomityName.MaxLength = 32767;
            this.txtShomityName.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtShomityName.Name = "txtShomityName";
            this.txtShomityName.PromptText = "(Type here...)";
            this.txtShomityName.ReadOnly = false;
            this.txtShomityName.ShowMandatoryMark = true;
            this.txtShomityName.Size = new System.Drawing.Size(548, 21);
            this.txtShomityName.TabIndex = 177;
            this.txtShomityName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtShomityName.UpperCaseOnly = false;
            this.txtShomityName.ValidationErrorMessage = "Input required !";
            this.txtShomityName.Load += new System.EventHandler(this.txtShomityName_Load);
            // 
            // lblShomityShortName
            // 
            this.lblShomityShortName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblShomityShortName.AutoSize = true;
            this.lblShomityShortName.Location = new System.Drawing.Point(28, 117);
            this.lblShomityShortName.Name = "lblShomityShortName";
            this.lblShomityShortName.Size = new System.Drawing.Size(109, 13);
            this.lblShomityShortName.TabIndex = 176;
            this.lblShomityShortName.Text = "Shomity Short Name :";
            // 
            // txtShomityShortName
            // 
            this.txtShomityShortName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtShomityShortName.BackColor = System.Drawing.Color.White;
            this.txtShomityShortName.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtShomityShortName.InputScopeAllowEmpty = false;
            this.txtShomityShortName.InputScopeCustomString = null;
            this.txtShomityShortName.IsValid = null;
            this.txtShomityShortName.Location = new System.Drawing.Point(143, 114);
            this.txtShomityShortName.MaxLength = 32767;
            this.txtShomityShortName.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtShomityShortName.Name = "txtShomityShortName";
            this.txtShomityShortName.PromptText = "(Type here...)";
            this.txtShomityShortName.ReadOnly = false;
            this.txtShomityShortName.ShowMandatoryMark = true;
            this.txtShomityShortName.Size = new System.Drawing.Size(217, 21);
            this.txtShomityShortName.TabIndex = 177;
            this.txtShomityShortName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtShomityShortName.UpperCaseOnly = false;
            this.txtShomityShortName.ValidationErrorMessage = "Input required !";
            // 
            // lblVATAccountNumber
            // 
            this.lblVATAccountNumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblVATAccountNumber.AutoSize = true;
            this.lblVATAccountNumber.Location = new System.Drawing.Point(388, 146);
            this.lblVATAccountNumber.Name = "lblVATAccountNumber";
            this.lblVATAccountNumber.Size = new System.Drawing.Size(117, 13);
            this.lblVATAccountNumber.TabIndex = 176;
            this.lblVATAccountNumber.Text = "VAT Account Number :";
            // 
            // txtVATAccountNumber
            // 
            this.txtVATAccountNumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtVATAccountNumber.BackColor = System.Drawing.Color.White;
            this.txtVATAccountNumber.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtVATAccountNumber.InputScopeAllowEmpty = false;
            this.txtVATAccountNumber.InputScopeCustomString = null;
            this.txtVATAccountNumber.IsValid = null;
            this.txtVATAccountNumber.Location = new System.Drawing.Point(511, 143);
            this.txtVATAccountNumber.MaxLength = 32767;
            this.txtVATAccountNumber.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtVATAccountNumber.Name = "txtVATAccountNumber";
            this.txtVATAccountNumber.PromptText = "(Type here...)";
            this.txtVATAccountNumber.ReadOnly = false;
            this.txtVATAccountNumber.ShowMandatoryMark = true;
            this.txtVATAccountNumber.Size = new System.Drawing.Size(180, 21);
            this.txtVATAccountNumber.TabIndex = 177;
            this.txtVATAccountNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtVATAccountNumber.UpperCaseOnly = false;
            this.txtVATAccountNumber.ValidationErrorMessage = "Input required !";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(53, 146);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 176;
            this.label6.Text = "Mobile Number :";
            // 
            // txtMobileNumber
            // 
            this.txtMobileNumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtMobileNumber.BackColor = System.Drawing.Color.White;
            this.txtMobileNumber.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.MobileNumber;
            this.txtMobileNumber.InputScopeAllowEmpty = false;
            this.txtMobileNumber.InputScopeCustomString = null;
            this.txtMobileNumber.IsValid = null;
            this.txtMobileNumber.Location = new System.Drawing.Point(143, 143);
            this.txtMobileNumber.MaxLength = 11;
            this.txtMobileNumber.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtMobileNumber.Name = "txtMobileNumber";
            this.txtMobileNumber.PromptText = "(Type here...)";
            this.txtMobileNumber.ReadOnly = false;
            this.txtMobileNumber.ShowMandatoryMark = true;
            this.txtMobileNumber.Size = new System.Drawing.Size(217, 21);
            this.txtMobileNumber.TabIndex = 177;
            this.txtMobileNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtMobileNumber.UpperCaseOnly = false;
            this.txtMobileNumber.ValidationErrorMessage = "Input required !";
            // 
            // loadZones
            // 
            this.loadZones.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.loadZones.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.loadZones.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro;
            this.loadZones.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.loadZones.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadZones.ForeColor = System.Drawing.Color.White;
            this.loadZones.Location = new System.Drawing.Point(640, 31);
            this.loadZones.Name = "loadZones";
            this.loadZones.Size = new System.Drawing.Size(103, 28);
            this.loadZones.TabIndex = 173;
            this.loadZones.Text = "Load Zones";
            this.loadZones.UseVisualStyleBackColor = false;
            this.loadZones.Click += new System.EventHandler(this.loadZones_Click);
            // 
            // frmShomitySetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(967, 570);
            this.Controls.Add(this.cbxCompanyId);
            this.Controls.Add(this.txtShomityName);
            this.Controls.Add(this.txtShomityShortName);
            this.Controls.Add(this.txtMobileNumber);
            this.Controls.Add(this.txtAccountNumber);
            this.Controls.Add(this.txtVATAccountNumber);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblShomityShortName);
            this.Controls.Add(this.lblVATAccountNumber);
            this.Controls.Add(this.lblShomityName);
            this.Controls.Add(this.lblAccountNumber);
            this.Controls.Add(this.loadZones);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.customDataGridViewHeader);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblCompanyId);
            this.Controls.Add(this.customTitlebar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmShomitySetup";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Shomity Setup";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomControls.CustomTitlebar customTitlebar1;
        private System.Windows.Forms.Label lblCompanyId;
        private CustomControls.CustomComboBoxDropDownList cbxCompanyId;
        private Common.Exporter.CustomDataGridViewHeader customDataGridViewHeader;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label2;
        private CustomControls.CustomTextBox txtAccountNumber;
        private System.Windows.Forms.Label lblAccountNumber;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnClear;
        private CustomControls.CustomTextBox txtShomityShortName;
        private System.Windows.Forms.Label lblShomityShortName;
        private CustomControls.CustomTextBox txtShomityName;
        private System.Windows.Forms.Label lblShomityName;
        private CustomControls.CustomTextBox txtVATAccountNumber;
        private System.Windows.Forms.Label lblVATAccountNumber;
        private CustomControls.CustomTextBox txtMobileNumber;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button loadZones;
    }
}