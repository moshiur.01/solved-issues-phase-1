﻿namespace MISL.Ababil.Agent.Module.BillPayment.UI.ConfigUI
{
    partial class frmOutletMapping
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOutletMapping));
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.lblTitle = new System.Windows.Forms.Label();
            this.cbxZone = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.lstOutlets = new System.Windows.Forms.ListBox();
            this.lstSelectedOutlets = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.loadZones = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cbxAgents = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.txtOutlet = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnFindUnselected = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnFindSelected = new System.Windows.Forms.Button();
            this.txtSelectedOutlet = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.cbxShomity = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.SuspendLayout();
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = this.lblTitle;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(853, 132);
            this.customTitlebar1.TabIndex = 0;
            this.customTitlebar1.TabStop = false;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblTitle.Font = new System.Drawing.Font("Segoe UI", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(7, 6);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(266, 25);
            this.lblTitle.TabIndex = 188;
            this.lblTitle.Text = "Outlet Mapping for Bill Payment";
            // 
            // cbxZone
            // 
            this.cbxZone.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbxZone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxZone.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxZone.FormattingEnabled = true;
            this.cbxZone.InputScopeAllowEmpty = false;
            this.cbxZone.IsValid = null;
            this.cbxZone.Location = new System.Drawing.Point(188, 71);
            this.cbxZone.Name = "cbxZone";
            this.cbxZone.PromptText = "(Select)";
            this.cbxZone.ReadOnly = false;
            this.cbxZone.ShowMandatoryMark = false;
            this.cbxZone.Size = new System.Drawing.Size(492, 21);
            this.cbxZone.TabIndex = 1;
            this.cbxZone.ValidationErrorMessage = "Validation Error!";
            this.cbxZone.SelectedIndexChanged += new System.EventHandler(this.cbxZone_SelectedIndexChanged);
            // 
            // lstOutlets
            // 
            this.lstOutlets.FormattingEnabled = true;
            this.lstOutlets.Location = new System.Drawing.Point(12, 193);
            this.lstOutlets.Name = "lstOutlets";
            this.lstOutlets.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lstOutlets.Size = new System.Drawing.Size(377, 316);
            this.lstOutlets.TabIndex = 4;
            // 
            // lstSelectedOutlets
            // 
            this.lstSelectedOutlets.FormattingEnabled = true;
            this.lstSelectedOutlets.Location = new System.Drawing.Point(464, 193);
            this.lstSelectedOutlets.Name = "lstSelectedOutlets";
            this.lstSelectedOutlets.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lstSelectedOutlets.Size = new System.Drawing.Size(377, 316);
            this.lstSelectedOutlets.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(149, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Zone";
            // 
            // loadZones
            // 
            this.loadZones.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.loadZones.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.loadZones.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.loadZones.FlatAppearance.BorderSize = 0;
            this.loadZones.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.loadZones.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadZones.ForeColor = System.Drawing.Color.White;
            this.loadZones.Image = ((System.Drawing.Image)(resources.GetObject("loadZones.Image")));
            this.loadZones.Location = new System.Drawing.Point(285, 5);
            this.loadZones.Name = "loadZones";
            this.loadZones.Size = new System.Drawing.Size(28, 28);
            this.loadZones.TabIndex = 174;
            this.loadZones.UseVisualStyleBackColor = false;
            this.loadZones.Visible = false;
            this.loadZones.Click += new System.EventHandler(this.loadZones_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Location = new System.Drawing.Point(399, 273);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(55, 28);
            this.btnAdd.TabIndex = 175;
            this.btnAdd.Text = ">>";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(399, 307);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(55, 28);
            this.button1.TabIndex = 175;
            this.button1.Text = ">";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(399, 341);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(55, 28);
            this.button2.TabIndex = 175;
            this.button2.Text = "<";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(399, 375);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(55, 28);
            this.button3.TabIndex = 175;
            this.button3.Text = "<<";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(146, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 176;
            this.label2.Text = "Agent";
            // 
            // cbxAgents
            // 
            this.cbxAgents.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbxAgents.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxAgents.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxAgents.FormattingEnabled = true;
            this.cbxAgents.InputScopeAllowEmpty = false;
            this.cbxAgents.IsValid = null;
            this.cbxAgents.Location = new System.Drawing.Point(188, 101);
            this.cbxAgents.Name = "cbxAgents";
            this.cbxAgents.PromptText = "(Select)";
            this.cbxAgents.ReadOnly = false;
            this.cbxAgents.ShowMandatoryMark = false;
            this.cbxAgents.Size = new System.Drawing.Size(492, 21);
            this.cbxAgents.TabIndex = 1;
            this.cbxAgents.ValidationErrorMessage = "Validation Error!";
            this.cbxAgents.SelectedIndexChanged += new System.EventHandler(this.cbxAgents_SelectedIndexChanged);
            // 
            // txtOutlet
            // 
            this.txtOutlet.BackColor = System.Drawing.Color.White;
            this.txtOutlet.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtOutlet.InputScopeAllowEmpty = false;
            this.txtOutlet.InputScopeCustomString = null;
            this.txtOutlet.IsValid = null;
            this.txtOutlet.Location = new System.Drawing.Point(14, 163);
            this.txtOutlet.MaxLength = 32767;
            this.txtOutlet.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtOutlet.Name = "txtOutlet";
            this.txtOutlet.PromptText = "(Type here...)";
            this.txtOutlet.ReadOnly = false;
            this.txtOutlet.ShowMandatoryMark = false;
            this.txtOutlet.Size = new System.Drawing.Size(343, 21);
            this.txtOutlet.TabIndex = 179;
            this.txtOutlet.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtOutlet.UpperCaseOnly = false;
            this.txtOutlet.ValidationErrorMessage = "Validation Error!";
            this.txtOutlet.TextChangedExt += new System.EventHandler(this.txtOutlet_TextChangedExt);
            this.txtOutlet.Load += new System.EventHandler(this.txtOutlet_Load);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(461, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 176;
            this.label3.Text = "Selected Outlet(s)";
            // 
            // btnFindUnselected
            // 
            this.btnFindUnselected.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnFindUnselected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnFindUnselected.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btnFindUnselected.FlatAppearance.BorderSize = 0;
            this.btnFindUnselected.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFindUnselected.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFindUnselected.ForeColor = System.Drawing.Color.White;
            this.btnFindUnselected.Image = ((System.Drawing.Image)(resources.GetObject("btnFindUnselected.Image")));
            this.btnFindUnselected.Location = new System.Drawing.Point(363, 161);
            this.btnFindUnselected.Name = "btnFindUnselected";
            this.btnFindUnselected.Size = new System.Drawing.Size(26, 25);
            this.btnFindUnselected.TabIndex = 174;
            this.btnFindUnselected.UseVisualStyleBackColor = false;
            this.btnFindUnselected.Click += new System.EventHandler(this.FindUnselected_TextChangedExt);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 176;
            this.label4.Text = "Outlet(s)";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(721, 519);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 28);
            this.btnClose.TabIndex = 175;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(593, 519);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(120, 28);
            this.btnSave.TabIndex = 175;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnFindSelected
            // 
            this.btnFindSelected.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnFindSelected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnFindSelected.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btnFindSelected.FlatAppearance.BorderSize = 0;
            this.btnFindSelected.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFindSelected.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFindSelected.ForeColor = System.Drawing.Color.White;
            this.btnFindSelected.Image = ((System.Drawing.Image)(resources.GetObject("btnFindSelected.Image")));
            this.btnFindSelected.Location = new System.Drawing.Point(815, 161);
            this.btnFindSelected.Name = "btnFindSelected";
            this.btnFindSelected.Size = new System.Drawing.Size(26, 25);
            this.btnFindSelected.TabIndex = 174;
            this.btnFindSelected.UseVisualStyleBackColor = false;
            this.btnFindSelected.Click += new System.EventHandler(this.button8_Click);
            // 
            // txtSelectedOutlet
            // 
            this.txtSelectedOutlet.BackColor = System.Drawing.Color.White;
            this.txtSelectedOutlet.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtSelectedOutlet.InputScopeAllowEmpty = false;
            this.txtSelectedOutlet.InputScopeCustomString = null;
            this.txtSelectedOutlet.IsValid = null;
            this.txtSelectedOutlet.Location = new System.Drawing.Point(466, 163);
            this.txtSelectedOutlet.MaxLength = 32767;
            this.txtSelectedOutlet.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtSelectedOutlet.Name = "txtSelectedOutlet";
            this.txtSelectedOutlet.PromptText = "(Type here...)";
            this.txtSelectedOutlet.ReadOnly = false;
            this.txtSelectedOutlet.ShowMandatoryMark = false;
            this.txtSelectedOutlet.Size = new System.Drawing.Size(343, 21);
            this.txtSelectedOutlet.TabIndex = 179;
            this.txtSelectedOutlet.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtSelectedOutlet.UpperCaseOnly = false;
            this.txtSelectedOutlet.ValidationErrorMessage = "Validation Error!";
            this.txtSelectedOutlet.TextChangedExt += new System.EventHandler(this.FindSelected_TextChangedExt);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(277, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1, 30);
            this.panel1.TabIndex = 189;
            this.panel1.Visible = false;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(138, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Shomity";
            // 
            // cbxShomity
            // 
            this.cbxShomity.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbxShomity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxShomity.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxShomity.FormattingEnabled = true;
            this.cbxShomity.InputScopeAllowEmpty = false;
            this.cbxShomity.IsValid = null;
            this.cbxShomity.Location = new System.Drawing.Point(188, 41);
            this.cbxShomity.Name = "cbxShomity";
            this.cbxShomity.PromptText = "(Select)";
            this.cbxShomity.ReadOnly = false;
            this.cbxShomity.ShowMandatoryMark = false;
            this.cbxShomity.Size = new System.Drawing.Size(492, 21);
            this.cbxShomity.TabIndex = 1;
            this.cbxShomity.ValidationErrorMessage = "Validation Error!";
            this.cbxShomity.SelectedIndexChanged += new System.EventHandler(this.cbxShomity_SelectedIndexChanged);
            // 
            // frmOutletMapping
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(853, 559);
            this.Controls.Add(this.cbxShomity);
            this.Controls.Add(this.cbxAgents);
            this.Controls.Add(this.cbxZone);
            this.Controls.Add(this.txtOutlet);
            this.Controls.Add(this.txtSelectedOutlet);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnFindSelected);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnFindUnselected);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.loadZones);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lstSelectedOutlets);
            this.Controls.Add(this.lstOutlets);
            this.Controls.Add(this.customTitlebar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "frmOutletMapping";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Outlet Mapping for Bill Payment";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomControls.CustomTitlebar customTitlebar1;
        private CustomControls.CustomComboBoxDropDownList cbxZone;
        private System.Windows.Forms.ListBox lstOutlets;
        private System.Windows.Forms.ListBox lstSelectedOutlets;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button loadZones;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label2;
        private CustomControls.CustomComboBoxDropDownList cbxAgents;
        private CustomControls.CustomTextBox txtOutlet;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnFindUnselected;
        private CustomControls.CustomTextBox txtSelectedOutlet;
        private System.Windows.Forms.Button btnFindSelected;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Panel panel1;
        private CustomControls.CustomComboBoxDropDownList cbxShomity;
        private System.Windows.Forms.Label label5;
    }
}