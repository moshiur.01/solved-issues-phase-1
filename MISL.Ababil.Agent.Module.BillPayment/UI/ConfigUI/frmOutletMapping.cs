﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Module.BillPayment.Model;
using MISL.Ababil.Agent.Module.Common.UI.MessageUI;
using MISL.Ababil.Agent.Module.Common.UI.ProgressUI;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.Services.Communication;
using MISL.Ababil.Agent.UI.forms.ProgressUI;


namespace MISL.Ababil.Agent.Module.BillPayment.UI.ConfigUI
{
    public partial class frmOutletMapping : CustomForm
    {
        //private AgentInformation _agentInformation;
        private List<SubAgentInfoDto> _subAgentsSelected = new List<SubAgentInfoDto>();
        private List<SubAgentInformation> _subAgents;
        private List<BillSubZoneInfo> _billSubZoneInfos;
        private int _companyid = 1;
        private long? _zoneid = -1;
        private List<SubAgentInfoDto> _subAgentsUnSelected = new List<SubAgentInfoDto>();

        public frmOutletMapping()
        {
            InitializeComponent();
            InitializeData();
        }

        private void InitializeData()
        {
            LoadShomity();
            LoadZone();
            LoadAgents();

            ReloadSelectedList();
            ReloadUnselectedList();
        }

        private void LoadShomity()
        {
            UtilityServices.fillComboBox(cbxShomity, new BindingSource()
            {
                DataSource = new WebClientCommunicator<object, List<BillZoneInfo>>().GetResult($"resources/bill/allzones/companyid/{_companyid}")
            }, "zoneName", "id");
        }

        private void LoadAgents()
        {
            List<AgentInformation> agentInformations = new AgentServices().getAgentInfoBranchWise();
            agentInformations.Insert(0, new AgentInformation { businessName = "(All)" });
            UtilityServices.fillComboBox(cbxAgents, new BindingSource
            {
                DataSource = agentInformations
            }, "businessName", "id");
            cbxAgents.SelectedIndex = 0;
        }

        private void ReloadSelectedList()
        {
            if (_subAgentsSelected == null)
            {
                lstSelectedOutlets.DataSource = null;
            }

            lstSelectedOutlets.DataSource = new BindingSource
            {
                DataSource = _subAgentsSelected
            };
            lstSelectedOutlets.DisplayMember = "name";
            lstSelectedOutlets.ValueMember = "id";
            lstSelectedOutlets.SelectedIndex = -1;
        }

        private void ReloadUnselectedList()
        {
            if (_subAgentsUnSelected == null)
            {
                lstOutlets.DataSource = null;
                return;
            }

            lstOutlets.DataSource = new BindingSource
            {
                DataSource = _subAgentsUnSelected
            };
            lstOutlets.DisplayMember = "name";
            lstOutlets.ValueMember = "id";
            lstOutlets.SelectedIndex = -1;
        }

        private void ClearSelectedList()
        {
            lstSelectedOutlets.DataSource = new BindingSource
            {
                DataSource = _subAgentsSelected
            };
        }

        private void ClearUnselectedList()
        {
            lstOutlets.DataSource = new BindingSource
            {
                DataSource = null
            };
        }

        private void MoveToSelectedItems()
        {
            for (int i = 0; i < lstOutlets.SelectedItems.Count; i++)
            {
                SubAgentInfoDto item = (SubAgentInfoDto)lstOutlets.SelectedItems[i];
                _subAgentsSelected.Add(item);
                _subAgentsUnSelected.Remove(item);
            }
            ReloadSelectedList();
            ReloadUnselectedList();
        }

        private void MoveToUnelectedItems()
        {
            for (int i = 0; i < lstSelectedOutlets.SelectedItems.Count; i++)
            {
                SubAgentInfoDto item = (SubAgentInfoDto)lstSelectedOutlets.SelectedItems[i];
                _subAgentsUnSelected.Add(item);
                _subAgentsSelected.Remove(item);
            }
            ReloadUnselectedList();
            ReloadSelectedList();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MoveToSelectedItems();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MoveToUnelectedItems();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            MoveAllItemsToSelected();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MoveAllItemsToUnselected();
        }

        private void MoveAllItemsToUnselected()
        {
            for (int i = 0; i < lstSelectedOutlets.Items.Count; i++)
            {
                SubAgentInfoDto item = (SubAgentInfoDto)lstSelectedOutlets.Items[i];
                _subAgentsUnSelected.Add(item);
                _subAgentsSelected.Remove(item);
            }
            ReloadSelectedList();
            ReloadUnselectedList();
        }

        private void MoveAllItemsToSelected()
        {
            for (int i = 0; i < lstOutlets.Items.Count; i++)
            {
                SubAgentInfoDto item = (SubAgentInfoDto)lstOutlets.Items[i];
                _subAgentsSelected.Add(item);
                _subAgentsUnSelected.Remove(item);
            }
            ReloadSelectedList();
            ReloadUnselectedList();
        }



        // Enable and disable buttons.
        private void SetButtonsEditable()
        {
            //btnSelect.Enabled = (lstUnselected.SelectedItems.Count > 0);
            //btnSelectAll.Enabled = (lstUnselected.Items.Count > 0);
            //btnDeselect.Enabled = (lstSelected.SelectedItems.Count > 0);
            //btnDeselectAll.Enabled = (lstSelected.Items.Count > 0);
        }

        private void cbxAgents_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (!((ComboBox)sender).Focused)
            //{
            //    return;
            //}

            try
            {
                ClearUnselectedList();
                ClearSelectedList();

                if (cbxAgents.SelectedIndex == 0)
                {
                    //_subAgents = new SubAgentServices().GetAllSubAgents();
                    LoadMappingForAllAgentsOfSubZone();
                }
                else
                {
                    //_agentInformation = new AgentServices().getAgentInfoById(cbxAgents.SelectedValue.ToString());

                    BillSubzoneOutletMappingDto billSubzoneOutletMappingDto
                        = new WebClientCommunicator<object, BillSubzoneOutletMappingDto>()
                            .GetResult($"resources/bill/mappinginfo/{((BillSubZoneInfo)cbxZone.SelectedItem).id}/{((AgentInformation)cbxAgents.SelectedItem).id}");

                    //////////////_agentInformation = new AgentInformation
                    //////////////{
                    //////////////    subAgents = billSubzoneOutletMappingDto.unAssigned
                    //////////////};
                    _subAgentsUnSelected = billSubzoneOutletMappingDto.unAssigned;
                    

                    _subAgentsSelected = billSubzoneOutletMappingDto.assigned;

                    ReloadSelectedList();
                    ReloadUnselectedList();
                }
            }
            catch (Exception ex)
            {

            }
            //SetSubagent();
        }

        private void SetSubagent()
        {
            if (_subAgentsUnSelected != null)
            {
                lstOutlets.DataSource = new BindingSource
                {
                    DataSource = _subAgentsUnSelected

                };
                lstOutlets.DisplayMember = "name";
                lstOutlets.ValueMember = "id";
                lstOutlets.SelectedIndex = -1;
            }
            else
            {
                lstOutlets.DataSource = new BindingSource
                {
                    DataSource = _subAgents

                };
                lstOutlets.DisplayMember = "name";
                lstOutlets.ValueMember = "id";
                lstOutlets.SelectedIndex = -1;
            }
        }

        private void txtOutlet_Load(object sender, EventArgs e)
        {

        }

        private void txtOutlet_TextChangedExt(object sender, EventArgs e)
        {
            FindInUnSelectedList();
        }

        private void FindInUnSelectedList()
        {
            lstOutlets.SelectionMode = SelectionMode.One;
            try
            {
                int index = lstOutlets.FindString(txtOutlet.Text);
                if (0 <= index)
                {
                    lstOutlets.SelectedIndex = index;
                }
                else
                {
                    lstOutlets.SelectedIndex = -1;
                }
            }
            catch (Exception exception)
            {
                lstOutlets.SelectedIndex = -1;
            }
            lstOutlets.SelectionMode = SelectionMode.MultiExtended;
        }

        private void loadZones_Click(object sender, EventArgs e)
        {
            LoadZone();
        }

        private void LoadZone()
        {
            //_billSubZoneInfos = new WebClientCommunicator<object, List<BillZoneInfo>>().GetResult(
            //    $"resources/bill/allzones/companyid/{_companyid}");
            //UtilityServices.fillComboBox(cbxZone, new BindingSource() { DataSource = _billSubZoneInfos }, "zoneName", "id");
            //cbxZone.SelectedIndex = 0;

            _billSubZoneInfos = new WebClientCommunicator<object, List<BillSubZoneInfo>>().GetResult(
                $"resources/bill/subzones/{_zoneid}");
            UtilityServices.fillComboBox(cbxZone, new BindingSource()
            {
                DataSource = _billSubZoneInfos
            }, "billSubZoneName", "id");
            cbxZone.SelectedIndex = -1;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ProgressUIManager.ShowProgress(this);
                List<BillSubzoneOutletMapping> billSubzoneOutletMappings = new List<BillSubzoneOutletMapping>();

                for (int i = 0; i < lstSelectedOutlets.Items.Count; i++)
                {
                    billSubzoneOutletMappings.Add(new BillSubzoneOutletMapping()
                    {
                        outletId = ((SubAgentInfoDto)lstSelectedOutlets.Items[i]).id,
                        billSubZoneId = ((BillSubZoneInfo)cbxZone.SelectedItem).id
                    });
                }

                new WebClientCommunicator<List<BillSubzoneOutletMapping>, object>()
                    .GetPostResult(billSubzoneOutletMappings, $"resources/bill/outletsubzone/mapping/{((BillSubZoneInfo)cbxZone.SelectedItem).id}");
                ProgressUIManager.CloseProgress();
                MsgBox.showInformation("New mapping is successfully saved!");
            }
            catch (Exception exception)
            {
                ProgressUIManager.CloseProgress();
                MsgBox.ShowError(exception.Message);
            }
        }

        private void cbxZone_SelectedIndexChanged(object sender, EventArgs e)
        {
            //LoadMappingForAllAgentsOfSubZone();
            cbxAgents.SelectedIndex = -1;
            if (cbxAgents.Items.Count > 0)
            {
                cbxAgents.SelectedIndex = 0;
            }
        }

        private void LoadMappingForAllAgentsOfSubZone()
        {
            if (cbxZone.SelectedItem == null)
            {
                ClearSelectedList();
                ClearUnselectedList();
                return;
            }

            BillSubzoneOutletMappingDto billSubzoneOutletMappingDto
                            = new WebClientCommunicator<object, BillSubzoneOutletMappingDto>()
                                .GetResult($"resources/bill/mappinginfo/{((BillSubZoneInfo)cbxZone.SelectedItem).id}");

            //////////_agentInformation = new AgentInformation
            //////////{
            //////////    Agents= billSubzoneOutletMappingDto.unAssigned
            _subAgentsUnSelected = billSubzoneOutletMappingDto.unAssigned;
            //////////};
            _subAgentsSelected = billSubzoneOutletMappingDto.assigned;

            ReloadSelectedList();
            ReloadUnselectedList();
        }

        private void FindUnselected_TextChangedExt(object sender, EventArgs e)
        {
            FindInUnSelectedList();
        }

        private void FindSelected_TextChangedExt(object sender, EventArgs e)
        {
            FindInSelectedList();
        }

        private void FindInSelectedList()
        {
            lstSelectedOutlets.SelectionMode = SelectionMode.One;
            try
            {
                int index = lstSelectedOutlets.FindString(txtSelectedOutlet.Text);
                if (0 <= index)
                {
                    lstSelectedOutlets.SelectedIndex = index;
                }
                else
                {
                    lstSelectedOutlets.SelectedIndex = -1;
                }
            }
            catch (Exception exception)
            {
                lstSelectedOutlets.SelectedIndex = -1;
            }
            lstSelectedOutlets.SelectionMode = SelectionMode.MultiExtended;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            FindInSelectedList();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbxShomity_SelectedIndexChanged(object sender, EventArgs e)
        {
            //LoadMappingForAllAgentsOfSubZone();
            //cbxAgents.SelectedIndex = -1;
            if (cbxShomity.Items.Count > 0)
            {
                _zoneid = ((BillZoneInfo)cbxShomity.SelectedItem).id;
            }

            LoadZone();
        }
    }
}