﻿namespace MISL.Ababil.Agent.Module.BillPayment.UI
{
    partial class frmBillPayment2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblInWordString = new System.Windows.Forms.Label();
            this.lblInWords = new System.Windows.Forms.Label();
            this.lblBillNo = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblCharge = new System.Windows.Forms.Label();
            this.lblAmount = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtAmount = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtMobileNo = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtBillNo = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtCharge = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtTotal = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtVAT = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtLateFee = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.txtStampCharge = new System.Windows.Forms.Label();
            this.lblStampCharge = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.gbxPayment = new System.Windows.Forms.GroupBox();
            this.mandatoryMark6 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.gbxAccount = new System.Windows.Forms.GroupBox();
            this.txtAccountNumber = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgvOperators = new System.Windows.Forms.DataGridView();
            this.ownerIdCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mobileNumberColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.captureColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.lblRequiredFingerPrint = new System.Windows.Forms.Label();
            this.lblBalanceV = new System.Windows.Forms.Label();
            this.lblAccountTitle = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblBalance = new System.Windows.Forms.Label();
            this.rBtnAccount = new System.Windows.Forms.RadioButton();
            this.rBtnCash = new System.Windows.Forms.RadioButton();
            this.lblPaymentType = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnPay = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbxZone = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cbxSubZone = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cbxYear = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cbxMonth = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.txtAccountNo = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtName = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.dataGridViewButtonColumn1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.gbxPayment.SuspendLayout();
            this.gbxAccount.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOperators)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblInWordString
            // 
            this.lblInWordString.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblInWordString.Location = new System.Drawing.Point(123, 295);
            this.lblInWordString.Name = "lblInWordString";
            this.lblInWordString.Size = new System.Drawing.Size(299, 47);
            this.lblInWordString.TabIndex = 22;
            // 
            // lblInWords
            // 
            this.lblInWords.AutoSize = true;
            this.lblInWords.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblInWords.Location = new System.Drawing.Point(59, 295);
            this.lblInWords.Name = "lblInWords";
            this.lblInWords.Size = new System.Drawing.Size(60, 15);
            this.lblInWords.TabIndex = 21;
            this.lblInWords.Text = "In Words :";
            // 
            // lblBillNo
            // 
            this.lblBillNo.AutoSize = true;
            this.lblBillNo.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblBillNo.Location = new System.Drawing.Point(69, 64);
            this.lblBillNo.Name = "lblBillNo";
            this.lblBillNo.Size = new System.Drawing.Size(51, 15);
            this.lblBillNo.TabIndex = 3;
            this.lblBillNo.Text = "Bill No. :";
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(70, 252);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(50, 19);
            this.lblTotal.TabIndex = 18;
            this.lblTotal.Text = "Total :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label6.Location = new System.Drawing.Point(40, 142);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 15);
            this.label6.TabIndex = 9;
            this.label6.Text = "VAT Amount :";
            // 
            // lblCharge
            // 
            this.lblCharge.AutoSize = true;
            this.lblCharge.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblCharge.Location = new System.Drawing.Point(32, 219);
            this.lblCharge.Name = "lblCharge";
            this.lblCharge.Size = new System.Drawing.Size(86, 15);
            this.lblCharge.TabIndex = 15;
            this.lblCharge.Text = "Agent Charge :";
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmount.Location = new System.Drawing.Point(46, 93);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(75, 20);
            this.lblAmount.TabIndex = 5;
            this.lblAmount.Text = "Amount :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label3.Location = new System.Drawing.Point(39, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 15);
            this.label3.TabIndex = 1;
            this.label3.Text = "Account No. :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label5.Location = new System.Drawing.Point(47, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 15);
            this.label5.TabIndex = 1;
            this.label5.Text = "Mobile No. :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label2.Location = new System.Drawing.Point(79, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Zone :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label4.Location = new System.Drawing.Point(74, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Name :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label1.Location = new System.Drawing.Point(62, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Shomity :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtAmount);
            this.groupBox1.Controls.Add(this.txtMobileNo);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.txtBillNo);
            this.groupBox1.Controls.Add(this.txtCharge);
            this.groupBox1.Controls.Add(this.txtTotal);
            this.groupBox1.Controls.Add(this.txtVAT);
            this.groupBox1.Controls.Add(this.txtLateFee);
            this.groupBox1.Controls.Add(this.flowLayoutPanel1);
            this.groupBox1.Controls.Add(this.lblBillNo);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.lblInWordString);
            this.groupBox1.Controls.Add(this.lblInWords);
            this.groupBox1.Controls.Add(this.lblAmount);
            this.groupBox1.Controls.Add(this.lblTotal);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.lblCharge);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.groupBox1.Location = new System.Drawing.Point(12, 268);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(430, 351);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // txtAmount
            // 
            this.txtAmount.BackColor = System.Drawing.Color.White;
            this.txtAmount.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtAmount.InputScopeAllowEmpty = false;
            this.txtAmount.InputScopeCustomString = null;
            this.txtAmount.IsValid = null;
            this.txtAmount.Location = new System.Drawing.Point(126, 93);
            this.txtAmount.MaxLength = 32767;
            this.txtAmount.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.PromptText = "(Type here...)";
            this.txtAmount.ReadOnly = false;
            this.txtAmount.ShowMandatoryMark = true;
            this.txtAmount.Size = new System.Drawing.Size(250, 21);
            this.txtAmount.TabIndex = 6;
            this.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtAmount.UpperCaseOnly = false;
            this.txtAmount.ValidationErrorMessage = "Validation Error!";
            this.txtAmount.Leave += new System.EventHandler(this.txtAmount_Leave);
            // 
            // txtMobileNo
            // 
            this.txtMobileNo.BackColor = System.Drawing.Color.White;
            this.txtMobileNo.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.MobileNumber;
            this.txtMobileNo.InputScopeAllowEmpty = false;
            this.txtMobileNo.InputScopeCustomString = null;
            this.txtMobileNo.IsValid = null;
            this.txtMobileNo.Location = new System.Drawing.Point(126, 29);
            this.txtMobileNo.MaxLength = 11;
            this.txtMobileNo.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtMobileNo.Name = "txtMobileNo";
            this.txtMobileNo.PromptText = "(Type here...)";
            this.txtMobileNo.ReadOnly = false;
            this.txtMobileNo.ShowMandatoryMark = true;
            this.txtMobileNo.Size = new System.Drawing.Size(250, 21);
            this.txtMobileNo.TabIndex = 2;
            this.txtMobileNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtMobileNo.UpperCaseOnly = false;
            this.txtMobileNo.ValidationErrorMessage = "Validation Error!";
            // 
            // txtBillNo
            // 
            this.txtBillNo.BackColor = System.Drawing.Color.White;
            this.txtBillNo.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtBillNo.InputScopeAllowEmpty = false;
            this.txtBillNo.InputScopeCustomString = null;
            this.txtBillNo.IsValid = null;
            this.txtBillNo.Location = new System.Drawing.Point(126, 61);
            this.txtBillNo.MaxLength = 32767;
            this.txtBillNo.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtBillNo.Name = "txtBillNo";
            this.txtBillNo.PromptText = "(Type here...)";
            this.txtBillNo.ReadOnly = false;
            this.txtBillNo.ShowMandatoryMark = true;
            this.txtBillNo.Size = new System.Drawing.Size(250, 21);
            this.txtBillNo.TabIndex = 4;
            this.txtBillNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtBillNo.UpperCaseOnly = false;
            this.txtBillNo.ValidationErrorMessage = "Validation Error!";
            // 
            // txtCharge
            // 
            this.txtCharge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtCharge.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtCharge.InputScopeAllowEmpty = false;
            this.txtCharge.InputScopeCustomString = null;
            this.txtCharge.IsValid = null;
            this.txtCharge.Location = new System.Drawing.Point(126, 217);
            this.txtCharge.MaxLength = 32767;
            this.txtCharge.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtCharge.Name = "txtCharge";
            this.txtCharge.PromptText = "(Type here...)";
            this.txtCharge.ReadOnly = true;
            this.txtCharge.ShowMandatoryMark = true;
            this.txtCharge.Size = new System.Drawing.Size(250, 21);
            this.txtCharge.TabIndex = 16;
            this.txtCharge.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtCharge.UpperCaseOnly = false;
            this.txtCharge.ValidationErrorMessage = "Validation Error!";
            // 
            // txtTotal
            // 
            this.txtTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtTotal.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtTotal.InputScopeAllowEmpty = false;
            this.txtTotal.InputScopeCustomString = null;
            this.txtTotal.IsValid = null;
            this.txtTotal.Location = new System.Drawing.Point(126, 251);
            this.txtTotal.MaxLength = 32767;
            this.txtTotal.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.PromptText = "(Type here...)";
            this.txtTotal.ReadOnly = true;
            this.txtTotal.ShowMandatoryMark = true;
            this.txtTotal.Size = new System.Drawing.Size(250, 21);
            this.txtTotal.TabIndex = 19;
            this.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtTotal.UpperCaseOnly = false;
            this.txtTotal.ValidationErrorMessage = "Validation Error!";
            this.txtTotal.TextChangedExt += new System.EventHandler(this.txtTotal_TextChangedExt);
            // 
            // txtVAT
            // 
            this.txtVAT.BackColor = System.Drawing.Color.White;
            this.txtVAT.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtVAT.InputScopeAllowEmpty = false;
            this.txtVAT.InputScopeCustomString = null;
            this.txtVAT.IsValid = null;
            this.txtVAT.Location = new System.Drawing.Point(126, 139);
            this.txtVAT.MaxLength = 32767;
            this.txtVAT.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtVAT.Name = "txtVAT";
            this.txtVAT.PromptText = "(Type here...)";
            this.txtVAT.ReadOnly = false;
            this.txtVAT.ShowMandatoryMark = true;
            this.txtVAT.Size = new System.Drawing.Size(250, 21);
            this.txtVAT.TabIndex = 10;
            this.txtVAT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtVAT.UpperCaseOnly = false;
            this.txtVAT.ValidationErrorMessage = "Validation Error!";
            this.txtVAT.Leave += new System.EventHandler(this.txtVAT_Leave);
            // 
            // txtLateFee
            // 
            this.txtLateFee.BackColor = System.Drawing.Color.White;
            this.txtLateFee.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtLateFee.InputScopeAllowEmpty = false;
            this.txtLateFee.InputScopeCustomString = null;
            this.txtLateFee.IsValid = null;
            this.txtLateFee.Location = new System.Drawing.Point(126, 171);
            this.txtLateFee.MaxLength = 32767;
            this.txtLateFee.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtLateFee.Name = "txtLateFee";
            this.txtLateFee.PromptText = "(Type here...)";
            this.txtLateFee.ReadOnly = false;
            this.txtLateFee.ShowMandatoryMark = true;
            this.txtLateFee.Size = new System.Drawing.Size(250, 21);
            this.txtLateFee.TabIndex = 13;
            this.txtLateFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtLateFee.UpperCaseOnly = false;
            this.txtLateFee.ValidationErrorMessage = "Validation Error!";
            this.txtLateFee.Leave += new System.EventHandler(this.txtVAT_Leave);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.label14);
            this.flowLayoutPanel1.Controls.Add(this.txtStampCharge);
            this.flowLayoutPanel1.Controls.Add(this.lblStampCharge);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(117, 197);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(267, 17);
            this.flowLayoutPanel1.TabIndex = 26;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label14.Location = new System.Drawing.Point(241, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(23, 15);
            this.label14.TabIndex = 0;
            this.label14.Text = "Tk.";
            // 
            // txtStampCharge
            // 
            this.txtStampCharge.AutoSize = true;
            this.txtStampCharge.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.txtStampCharge.Location = new System.Drawing.Point(222, 0);
            this.txtStampCharge.Name = "txtStampCharge";
            this.txtStampCharge.Size = new System.Drawing.Size(13, 15);
            this.txtStampCharge.TabIndex = 2;
            this.txtStampCharge.Text = "0";
            this.txtStampCharge.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblStampCharge
            // 
            this.lblStampCharge.AutoSize = true;
            this.lblStampCharge.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblStampCharge.Location = new System.Drawing.Point(128, 0);
            this.lblStampCharge.Name = "lblStampCharge";
            this.lblStampCharge.Size = new System.Drawing.Size(88, 15);
            this.lblStampCharge.TabIndex = 1;
            this.lblStampCharge.Text = "Stamp Charge :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label19.Location = new System.Drawing.Point(63, 174);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(56, 15);
            this.label19.TabIndex = 12;
            this.label19.Text = "Late Fee :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 15);
            this.label9.TabIndex = 0;
            this.label9.Text = "Bill Information";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label18.Location = new System.Drawing.Point(399, 174);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(23, 15);
            this.label18.TabIndex = 14;
            this.label18.Text = "Tk.";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label12.Location = new System.Drawing.Point(399, 142);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(23, 15);
            this.label12.TabIndex = 11;
            this.label12.Text = "Tk.";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label11.Location = new System.Drawing.Point(399, 97);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(23, 15);
            this.label11.TabIndex = 7;
            this.label11.Text = "Tk.";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(398, 252);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(30, 19);
            this.label15.TabIndex = 20;
            this.label15.Text = "Tk.";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label13.Location = new System.Drawing.Point(398, 219);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(23, 15);
            this.label13.TabIndex = 17;
            this.label13.Text = "Tk.";
            // 
            // gbxPayment
            // 
            this.gbxPayment.Controls.Add(this.mandatoryMark6);
            this.gbxPayment.Controls.Add(this.gbxAccount);
            this.gbxPayment.Controls.Add(this.rBtnAccount);
            this.gbxPayment.Controls.Add(this.rBtnCash);
            this.gbxPayment.Controls.Add(this.lblPaymentType);
            this.gbxPayment.Controls.Add(this.label10);
            this.gbxPayment.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxPayment.Location = new System.Drawing.Point(450, 34);
            this.gbxPayment.Name = "gbxPayment";
            this.gbxPayment.Size = new System.Drawing.Size(590, 551);
            this.gbxPayment.TabIndex = 4;
            this.gbxPayment.TabStop = false;
            // 
            // mandatoryMark6
            // 
            this.mandatoryMark6.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark6.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.mandatoryMark6.Location = new System.Drawing.Point(285, 23);
            this.mandatoryMark6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.mandatoryMark6.MaximumSize = new System.Drawing.Size(44, 22);
            this.mandatoryMark6.MinimumSize = new System.Drawing.Size(44, 22);
            this.mandatoryMark6.Name = "mandatoryMark6";
            this.mandatoryMark6.Size = new System.Drawing.Size(44, 22);
            this.mandatoryMark6.TabIndex = 4;
            this.mandatoryMark6.TabStop = false;
            // 
            // gbxAccount
            // 
            this.gbxAccount.Controls.Add(this.txtAccountNumber);
            this.gbxAccount.Controls.Add(this.groupBox2);
            this.gbxAccount.Controls.Add(this.lblBalanceV);
            this.gbxAccount.Controls.Add(this.lblAccountTitle);
            this.gbxAccount.Controls.Add(this.label7);
            this.gbxAccount.Controls.Add(this.label8);
            this.gbxAccount.Controls.Add(this.lblBalance);
            this.gbxAccount.Enabled = false;
            this.gbxAccount.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.gbxAccount.Location = new System.Drawing.Point(8, 56);
            this.gbxAccount.Name = "gbxAccount";
            this.gbxAccount.Size = new System.Drawing.Size(574, 495);
            this.gbxAccount.TabIndex = 5;
            this.gbxAccount.TabStop = false;
            this.gbxAccount.Text = "Account Info";
            // 
            // txtAccountNumber
            // 
            this.txtAccountNumber.BackColor = System.Drawing.Color.White;
            this.txtAccountNumber.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtAccountNumber.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtAccountNumber.InputScopeAllowEmpty = false;
            this.txtAccountNumber.InputScopeCustomString = null;
            this.txtAccountNumber.IsValid = null;
            this.txtAccountNumber.Location = new System.Drawing.Point(120, 31);
            this.txtAccountNumber.MaxLength = 13;
            this.txtAccountNumber.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtAccountNumber.Name = "txtAccountNumber";
            this.txtAccountNumber.PromptText = "(Type here...)";
            this.txtAccountNumber.ReadOnly = false;
            this.txtAccountNumber.ShowMandatoryMark = false;
            this.txtAccountNumber.Size = new System.Drawing.Size(149, 30);
            this.txtAccountNumber.TabIndex = 1;
            this.txtAccountNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtAccountNumber.UpperCaseOnly = false;
            this.txtAccountNumber.ValidationErrorMessage = "Validation Error!";
            this.txtAccountNumber.Leave += new System.EventHandler(this.txtAccountNumber_Leave);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgvOperators);
            this.groupBox2.Controls.Add(this.lblRequiredFingerPrint);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.groupBox2.Location = new System.Drawing.Point(7, 105);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(560, 390);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Finger Print Information";
            // 
            // dgvOperators
            // 
            this.dgvOperators.AllowUserToAddRows = false;
            this.dgvOperators.AllowUserToDeleteRows = false;
            this.dgvOperators.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvOperators.BackgroundColor = System.Drawing.Color.White;
            this.dgvOperators.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvOperators.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgvOperators.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Segoe UI", 9F);
            dataGridViewCellStyle29.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.Color.Turquoise;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvOperators.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle29;
            this.dgvOperators.ColumnHeadersHeight = 28;
            this.dgvOperators.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvOperators.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ownerIdCol,
            this.nameColumn,
            this.mobileNumberColumn,
            this.captureColumn});
            this.dgvOperators.EnableHeadersVisualStyles = false;
            this.dgvOperators.GridColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgvOperators.Location = new System.Drawing.Point(8, 21);
            this.dgvOperators.Name = "dgvOperators";
            this.dgvOperators.ReadOnly = true;
            this.dgvOperators.RowHeadersVisible = false;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.dgvOperators.RowsDefaultCellStyle = dataGridViewCellStyle30;
            this.dgvOperators.RowTemplate.Height = 66;
            this.dgvOperators.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvOperators.Size = new System.Drawing.Size(545, 360);
            this.dgvOperators.TabIndex = 1;
            this.dgvOperators.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOperators_CellContentClick);
            // 
            // ownerIdCol
            // 
            this.ownerIdCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ownerIdCol.FillWeight = 90F;
            this.ownerIdCol.HeaderText = "ID";
            this.ownerIdCol.Name = "ownerIdCol";
            this.ownerIdCol.ReadOnly = true;
            this.ownerIdCol.Visible = false;
            this.ownerIdCol.Width = 90;
            // 
            // nameColumn
            // 
            this.nameColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nameColumn.HeaderText = "Name";
            this.nameColumn.Name = "nameColumn";
            this.nameColumn.ReadOnly = true;
            this.nameColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.nameColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // mobileNumberColumn
            // 
            this.mobileNumberColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.mobileNumberColumn.FillWeight = 120F;
            this.mobileNumberColumn.HeaderText = "Mobile Number";
            this.mobileNumberColumn.Name = "mobileNumberColumn";
            this.mobileNumberColumn.ReadOnly = true;
            this.mobileNumberColumn.Width = 120;
            // 
            // captureColumn
            // 
            this.captureColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.captureColumn.FillWeight = 70F;
            this.captureColumn.HeaderText = "Capture";
            this.captureColumn.Name = "captureColumn";
            this.captureColumn.ReadOnly = true;
            this.captureColumn.Text = "Capture";
            this.captureColumn.Width = 70;
            // 
            // lblRequiredFingerPrint
            // 
            this.lblRequiredFingerPrint.AutoSize = true;
            this.lblRequiredFingerPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequiredFingerPrint.Location = new System.Drawing.Point(255, -2);
            this.lblRequiredFingerPrint.Name = "lblRequiredFingerPrint";
            this.lblRequiredFingerPrint.Size = new System.Drawing.Size(55, 15);
            this.lblRequiredFingerPrint.TabIndex = 0;
            this.lblRequiredFingerPrint.Text = "------------";
            // 
            // lblBalanceV
            // 
            this.lblBalanceV.AutoSize = true;
            this.lblBalanceV.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblBalanceV.Location = new System.Drawing.Point(334, 37);
            this.lblBalanceV.Name = "lblBalanceV";
            this.lblBalanceV.Size = new System.Drawing.Size(17, 19);
            this.lblBalanceV.TabIndex = 3;
            this.lblBalanceV.Text = "0";
            // 
            // lblAccountTitle
            // 
            this.lblAccountTitle.AutoSize = true;
            this.lblAccountTitle.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblAccountTitle.Location = new System.Drawing.Point(123, 74);
            this.lblAccountTitle.Name = "lblAccountTitle";
            this.lblAccountTitle.Size = new System.Drawing.Size(0, 15);
            this.lblAccountTitle.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(20, 36);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 19);
            this.label7.TabIndex = 0;
            this.label7.Text = "Account No :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label8.Location = new System.Drawing.Point(22, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 15);
            this.label8.TabIndex = 4;
            this.label8.Text = "Account Name :";
            // 
            // lblBalance
            // 
            this.lblBalance.AutoSize = true;
            this.lblBalance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblBalance.Location = new System.Drawing.Point(274, 36);
            this.lblBalance.Name = "lblBalance";
            this.lblBalance.Size = new System.Drawing.Size(62, 19);
            this.lblBalance.TabIndex = 2;
            this.lblBalance.Text = "Balance :";
            // 
            // rBtnAccount
            // 
            this.rBtnAccount.AutoSize = true;
            this.rBtnAccount.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.rBtnAccount.Location = new System.Drawing.Point(199, 22);
            this.rBtnAccount.Name = "rBtnAccount";
            this.rBtnAccount.Size = new System.Drawing.Size(77, 23);
            this.rBtnAccount.TabIndex = 3;
            this.rBtnAccount.Text = "Account";
            this.rBtnAccount.UseVisualStyleBackColor = true;
            this.rBtnAccount.CheckedChanged += new System.EventHandler(this.rBtnAccount_CheckedChanged);
            // 
            // rBtnCash
            // 
            this.rBtnCash.AutoSize = true;
            this.rBtnCash.Checked = true;
            this.rBtnCash.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.rBtnCash.Location = new System.Drawing.Point(131, 22);
            this.rBtnCash.Name = "rBtnCash";
            this.rBtnCash.Size = new System.Drawing.Size(57, 23);
            this.rBtnCash.TabIndex = 2;
            this.rBtnCash.TabStop = true;
            this.rBtnCash.Text = "Cash";
            this.rBtnCash.UseVisualStyleBackColor = true;
            // 
            // lblPaymentType
            // 
            this.lblPaymentType.AutoSize = true;
            this.lblPaymentType.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.lblPaymentType.Location = new System.Drawing.Point(11, 24);
            this.lblPaymentType.Name = "lblPaymentType";
            this.lblPaymentType.Size = new System.Drawing.Size(112, 19);
            this.lblPaymentType.TabIndex = 1;
            this.lblPaymentType.Text = "Payment Type :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 15);
            this.label10.TabIndex = 0;
            this.label10.Text = "Payment";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(942, 593);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(99, 28);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(837, 593);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(99, 28);
            this.btnClear.TabIndex = 6;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            // 
            // btnPay
            // 
            this.btnPay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnPay.FlatAppearance.BorderSize = 0;
            this.btnPay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPay.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPay.ForeColor = System.Drawing.Color.White;
            this.btnPay.Location = new System.Drawing.Point(732, 593);
            this.btnPay.Name = "btnPay";
            this.btnPay.Size = new System.Drawing.Size(99, 28);
            this.btnPay.TabIndex = 5;
            this.btnPay.Text = "Pay";
            this.btnPay.UseVisualStyleBackColor = false;
            this.btnPay.Click += new System.EventHandler(this.btnPay_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbxSubZone);
            this.groupBox3.Controls.Add(this.cbxZone);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Location = new System.Drawing.Point(12, 34);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(430, 94);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            // 
            // cbxZone
            // 
            this.cbxZone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxZone.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxZone.FormattingEnabled = true;
            this.cbxZone.InputScopeAllowEmpty = false;
            this.cbxZone.IsValid = null;
            this.cbxZone.Location = new System.Drawing.Point(126, 27);
            this.cbxZone.Name = "cbxZone";
            this.cbxZone.PromptText = "(Select)";
            this.cbxZone.ReadOnly = false;
            this.cbxZone.ShowMandatoryMark = true;
            this.cbxZone.Size = new System.Drawing.Size(250, 21);
            this.cbxZone.TabIndex = 2;
            this.cbxZone.ValidationErrorMessage = "Validation Error!";
            this.cbxZone.SelectedIndexChanged += new System.EventHandler(this.cbxZone_SelectedIndexChanged);
            this.cbxZone.SelectedValueChanged += new System.EventHandler(this.cbxZone_SelectedValueChanged);
            // 
            // cbxSubZone
            // 
            this.cbxSubZone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxSubZone.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxSubZone.FormattingEnabled = true;
            this.cbxSubZone.InputScopeAllowEmpty = false;
            this.cbxSubZone.IsValid = null;
            this.cbxSubZone.Location = new System.Drawing.Point(126, 59);
            this.cbxSubZone.Name = "cbxSubZone";
            this.cbxSubZone.PromptText = "(Select)";
            this.cbxSubZone.ReadOnly = false;
            this.cbxSubZone.ShowMandatoryMark = true;
            this.cbxSubZone.Size = new System.Drawing.Size(250, 21);
            this.cbxSubZone.TabIndex = 4;
            this.cbxSubZone.ValidationErrorMessage = "Validation Error!";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(144, 15);
            this.label16.TabIndex = 0;
            this.label16.Text = "Bill Company Information";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cbxMonth);
            this.groupBox4.Controls.Add(this.cbxYear);
            this.groupBox4.Controls.Add(this.txtName);
            this.groupBox4.Controls.Add(this.txtAccountNo);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Location = new System.Drawing.Point(12, 134);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(430, 128);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            // 
            // cbxYear
            // 
            this.cbxYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxYear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxYear.FormattingEnabled = true;
            this.cbxYear.InputScopeAllowEmpty = false;
            this.cbxYear.IsValid = null;
            this.cbxYear.Location = new System.Drawing.Point(126, 94);
            this.cbxYear.Name = "cbxYear";
            this.cbxYear.PromptText = "(Select)";
            this.cbxYear.ReadOnly = false;
            this.cbxYear.ShowMandatoryMark = true;
            this.cbxYear.Size = new System.Drawing.Size(110, 21);
            this.cbxYear.TabIndex = 6;
            this.cbxYear.ValidationErrorMessage = "Validation Error!";
            // 
            // cbxMonth
            // 
            this.cbxMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxMonth.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxMonth.FormattingEnabled = true;
            this.cbxMonth.InputScopeAllowEmpty = false;
            this.cbxMonth.IsValid = null;
            this.cbxMonth.Location = new System.Drawing.Point(266, 94);
            this.cbxMonth.Name = "cbxMonth";
            this.cbxMonth.PromptText = "(Select)";
            this.cbxMonth.ReadOnly = false;
            this.cbxMonth.ShowMandatoryMark = true;
            this.cbxMonth.Size = new System.Drawing.Size(110, 21);
            this.cbxMonth.TabIndex = 7;
            this.cbxMonth.ValidationErrorMessage = "Validation Error!";
            // 
            // txtAccountNo
            // 
            this.txtAccountNo.BackColor = System.Drawing.Color.White;
            this.txtAccountNo.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtAccountNo.InputScopeAllowEmpty = false;
            this.txtAccountNo.InputScopeCustomString = null;
            this.txtAccountNo.IsValid = null;
            this.txtAccountNo.Location = new System.Drawing.Point(126, 26);
            this.txtAccountNo.MaxLength = 32767;
            this.txtAccountNo.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtAccountNo.Name = "txtAccountNo";
            this.txtAccountNo.PromptText = "(Type here...)";
            this.txtAccountNo.ReadOnly = false;
            this.txtAccountNo.ShowMandatoryMark = true;
            this.txtAccountNo.Size = new System.Drawing.Size(250, 21);
            this.txtAccountNo.TabIndex = 2;
            this.txtAccountNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtAccountNo.UpperCaseOnly = false;
            this.txtAccountNo.ValidationErrorMessage = "Validation Error!";
            this.txtAccountNo.Leave += new System.EventHandler(this.txtAccountNo_Leave);
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.Color.White;
            this.txtName.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtName.InputScopeAllowEmpty = false;
            this.txtName.InputScopeCustomString = null;
            this.txtName.IsValid = null;
            this.txtName.Location = new System.Drawing.Point(126, 59);
            this.txtName.MaxLength = 32767;
            this.txtName.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtName.Name = "txtName";
            this.txtName.PromptText = "(Type here...)";
            this.txtName.ReadOnly = false;
            this.txtName.ShowMandatoryMark = true;
            this.txtName.Size = new System.Drawing.Size(250, 21);
            this.txtName.TabIndex = 4;
            this.txtName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtName.UpperCaseOnly = false;
            this.txtName.ValidationErrorMessage = "Validation Error!";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label20.Location = new System.Drawing.Point(82, 96);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(37, 15);
            this.label20.TabIndex = 5;
            this.label20.Text = "Date :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(6, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(90, 15);
            this.label17.TabIndex = 0;
            this.label17.Text = "Bill Information";
            // 
            // dataGridViewButtonColumn1
            // 
            this.dataGridViewButtonColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewButtonColumn1.FillWeight = 70F;
            this.dataGridViewButtonColumn1.HeaderText = "Capture";
            this.dataGridViewButtonColumn1.Name = "dataGridViewButtonColumn1";
            this.dataGridViewButtonColumn1.Text = "Capture";
            this.dataGridViewButtonColumn1.Width = 70;
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(1051, 26);
            this.customTitlebar1.TabIndex = 0;
            this.customTitlebar1.TabStop = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label21.Location = new System.Drawing.Point(121, 119);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(260, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "( Amount = PBS Bill Amount + PBS Charge + VAT )";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label22.Location = new System.Drawing.Point(121, 277);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(218, 13);
            this.label22.TabIndex = 1;
            this.label22.Text = "( Total = Amount + Late + Agent Charge )";
            // 
            // frmBillPayment2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1051, 631);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnPay);
            this.Controls.Add(this.gbxPayment);
            this.Controls.Add(this.customTitlebar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmBillPayment2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bill Payment";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.gbxPayment.ResumeLayout(false);
            this.gbxPayment.PerformLayout();
            this.gbxAccount.ResumeLayout(false);
            this.gbxAccount.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOperators)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private CustomControls.CustomTitlebar customTitlebar1;
        private CustomControls.CustomTextBox txtTotal;
        private CustomControls.CustomTextBox txtCharge;
        private CustomControls.CustomTextBox txtVAT;
        private CustomControls.CustomTextBox txtAmount;
        private CustomControls.CustomTextBox txtBillNo;
        private CustomControls.CustomTextBox txtMobileNo;
        private CustomControls.CustomTextBox txtName;
        private CustomControls.CustomTextBox txtAccountNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.Label lblCharge;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblBillNo;
        private System.Windows.Forms.Label lblInWords;
        private System.Windows.Forms.Label lblInWordString;
        private CustomControls.CustomComboBoxDropDownList cbxSubZone;
        private CustomControls.CustomComboBoxDropDownList cbxZone;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gbxPayment;
        private Agent.UI.forms.CustomControls.MandatoryMark mandatoryMark6;
        private System.Windows.Forms.GroupBox gbxAccount;
        private CustomControls.CustomTextBox txtAccountNumber;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgvOperators;
        private System.Windows.Forms.Label lblRequiredFingerPrint;
        private System.Windows.Forms.Label lblBalanceV;
        private System.Windows.Forms.Label lblAccountTitle;
        private System.Windows.Forms.Label lblBalance;
        private System.Windows.Forms.RadioButton rBtnAccount;
        private System.Windows.Forms.RadioButton rBtnCash;
        private System.Windows.Forms.Label lblPaymentType;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnPay;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label17;
        private CustomControls.CustomTextBox txtLateFee;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private CustomControls.CustomComboBoxDropDownList cbxYear;
        private CustomControls.CustomComboBoxDropDownList cbxMonth;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn1;
        private System.Windows.Forms.Label lblStampCharge;
        private System.Windows.Forms.Label txtStampCharge;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ownerIdCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mobileNumberColumn;
        private System.Windows.Forms.DataGridViewButtonColumn captureColumn;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
    }
}