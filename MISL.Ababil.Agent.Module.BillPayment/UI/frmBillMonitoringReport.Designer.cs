﻿using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.UI.forms.CustomControls;

namespace MISL.Ababil.Agent.Module.BillPayment.UI
{
    partial class frmBillMonitoringReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBillMonitoringReport));
            this.lblAgent = new System.Windows.Forms.Label();
            this.lblSubAgent = new System.Windows.Forms.Label();
            this.lblFromDate = new System.Windows.Forms.Label();
            this.lblTodate = new System.Windows.Forms.Label();
            this.btnViewReport = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbAgentName = new MetroFramework.Controls.MetroComboBox();
            this.cmbSubAgnetName = new MetroFramework.Controls.MetroComboBox();
            this.dtpToDate = new MISL.Ababil.Agent.UI.forms.CustomControls.CustomDateTimePicker();
            this.dtpFromDate = new MISL.Ababil.Agent.UI.forms.CustomControls.CustomDateTimePicker();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbxZone = new MetroFramework.Controls.MetroComboBox();
            this.cbxSubZone = new MetroFramework.Controls.MetroComboBox();
            this.SuspendLayout();
            // 
            // lblAgent
            // 
            this.lblAgent.AutoSize = true;
            this.lblAgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lblAgent.Location = new System.Drawing.Point(26, 114);
            this.lblAgent.Name = "lblAgent";
            this.lblAgent.Size = new System.Drawing.Size(53, 17);
            this.lblAgent.TabIndex = 0;
            this.lblAgent.Text = "Agent :";
            // 
            // lblSubAgent
            // 
            this.lblSubAgent.AutoSize = true;
            this.lblSubAgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lblSubAgent.Location = new System.Drawing.Point(355, 114);
            this.lblSubAgent.Name = "lblSubAgent";
            this.lblSubAgent.Size = new System.Drawing.Size(54, 17);
            this.lblSubAgent.TabIndex = 2;
            this.lblSubAgent.Text = "Outlet :";
            this.lblSubAgent.Click += new System.EventHandler(this.lblSubAgent_Click);
            // 
            // lblFromDate
            // 
            this.lblFromDate.AutoSize = true;
            this.lblFromDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lblFromDate.Location = new System.Drawing.Point(31, 184);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(48, 17);
            this.lblFromDate.TabIndex = 4;
            this.lblFromDate.Text = "From :";
            // 
            // lblTodate
            // 
            this.lblTodate.AutoSize = true;
            this.lblTodate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lblTodate.Location = new System.Drawing.Point(377, 184);
            this.lblTodate.Name = "lblTodate";
            this.lblTodate.Size = new System.Drawing.Size(33, 17);
            this.lblTodate.TabIndex = 6;
            this.lblTodate.Text = "To :";
            // 
            // btnViewReport
            // 
            this.btnViewReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnViewReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewReport.ForeColor = System.Drawing.Color.White;
            this.btnViewReport.Location = new System.Drawing.Point(412, 221);
            this.btnViewReport.Name = "btnViewReport";
            this.btnViewReport.Size = new System.Drawing.Size(127, 30);
            this.btnViewReport.TabIndex = 6;
            this.btnViewReport.Text = "View Report";
            this.btnViewReport.UseVisualStyleBackColor = false;
            this.btnViewReport.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(545, 221);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(105, 30);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(266, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(182, 20);
            this.label1.TabIndex = 11;
            this.label1.Text = "Bill Monitoring Report";
            // 
            // cmbAgentName
            // 
            this.cmbAgentName.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cmbAgentName.FormattingEnabled = true;
            this.cmbAgentName.ItemHeight = 19;
            this.cmbAgentName.Location = new System.Drawing.Point(83, 111);
            this.cmbAgentName.Name = "cmbAgentName";
            this.cmbAgentName.Size = new System.Drawing.Size(238, 25);
            this.cmbAgentName.TabIndex = 0;
            this.cmbAgentName.UseSelectable = true;
            this.cmbAgentName.SelectedIndexChanged += new System.EventHandler(this.cmbAgentName_SelectedIndexChanged);
            // 
            // cmbSubAgnetName
            // 
            this.cmbSubAgnetName.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cmbSubAgnetName.FormattingEnabled = true;
            this.cmbSubAgnetName.ItemHeight = 19;
            this.cmbSubAgnetName.Location = new System.Drawing.Point(412, 111);
            this.cmbSubAgnetName.Name = "cmbSubAgnetName";
            this.cmbSubAgnetName.Size = new System.Drawing.Size(238, 25);
            this.cmbSubAgnetName.TabIndex = 1;
            this.cmbSubAgnetName.UseSelectable = true;
            this.cmbSubAgnetName.SelectedIndexChanged += new System.EventHandler(this.cmbSubAgnetName_SelectedIndexChanged);
            // 
            // dtpToDate
            // 
            this.dtpToDate.Date = "17/09/2015";
            this.dtpToDate.Location = new System.Drawing.Point(412, 181);
            this.dtpToDate.MaximumSize = new System.Drawing.Size(400, 25);
            this.dtpToDate.MinimumSize = new System.Drawing.Size(60, 25);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.PresetServerDate = true;
            this.dtpToDate.Size = new System.Drawing.Size(238, 25);
            this.dtpToDate.TabIndex = 5;
            this.dtpToDate.Value = new System.DateTime(2015, 9, 17, 11, 1, 42, 511);
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.Date = "17/09/2015";
            this.dtpFromDate.Location = new System.Drawing.Point(83, 181);
            this.dtpFromDate.MaximumSize = new System.Drawing.Size(400, 25);
            this.dtpFromDate.MinimumSize = new System.Drawing.Size(60, 25);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.PresetServerDate = true;
            this.dtpFromDate.Size = new System.Drawing.Size(238, 25);
            this.dtpFromDate.TabIndex = 4;
            this.dtpFromDate.Value = new System.DateTime(2015, 9, 17, 10, 30, 33, 651);
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.customTitlebar1.Location = new System.Drawing.Point(-1, -1);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(698, 26);
            this.customTitlebar1.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label2.Location = new System.Drawing.Point(22, 149);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 15);
            this.label2.TabIndex = 16;
            this.label2.Text = "Shomity :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label3.Location = new System.Drawing.Point(369, 149);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 15);
            this.label3.TabIndex = 18;
            this.label3.Text = "Zone :";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // cbxZone
            // 
            this.cbxZone.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbxZone.FormattingEnabled = true;
            this.cbxZone.ItemHeight = 19;
            this.cbxZone.Location = new System.Drawing.Point(83, 146);
            this.cbxZone.Name = "cbxZone";
            this.cbxZone.Size = new System.Drawing.Size(238, 25);
            this.cbxZone.TabIndex = 2;
            this.cbxZone.UseSelectable = true;
            this.cbxZone.SelectedIndexChanged += new System.EventHandler(this.cbxZone_SelectedIndexChanged);
            // 
            // cbxSubZone
            // 
            this.cbxSubZone.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cbxSubZone.FormattingEnabled = true;
            this.cbxSubZone.ItemHeight = 19;
            this.cbxSubZone.Location = new System.Drawing.Point(412, 146);
            this.cbxSubZone.Name = "cbxSubZone";
            this.cbxSubZone.Size = new System.Drawing.Size(238, 25);
            this.cbxSubZone.TabIndex = 3;
            this.cbxSubZone.UseSelectable = true;
            // 
            // frmBillMonitoringReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(697, 272);
            this.Controls.Add(this.cbxSubZone);
            this.Controls.Add(this.cbxZone);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtpToDate);
            this.Controls.Add(this.dtpFromDate);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.cmbSubAgnetName);
            this.Controls.Add(this.cmbAgentName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnViewReport);
            this.Controls.Add(this.lblTodate);
            this.Controls.Add(this.lblFromDate);
            this.Controls.Add(this.lblSubAgent);
            this.Controls.Add(this.lblAgent);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmBillMonitoringReport";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bill Monitoring Report";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmAccountMonitoringRport_FormClosing);
            this.Load += new System.EventHandler(this.frmTrMonitoringRport_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAgent;
        private System.Windows.Forms.Label lblSubAgent;
        private System.Windows.Forms.Label lblFromDate;
        private System.Windows.Forms.Label lblTodate;
        private System.Windows.Forms.Button btnViewReport;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroComboBox cmbAgentName;
        private MetroFramework.Controls.MetroComboBox cmbSubAgnetName;
        private CustomTitlebar customTitlebar1;
        private CustomDateTimePicker dtpFromDate;
        private CustomDateTimePicker dtpToDate;
        private MetroFramework.Controls.MetroComboBox cbxSubZone;
        private MetroFramework.Controls.MetroComboBox cbxZone;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}