﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.Remittance;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using MISL.Ababil.Agent.Module.BillPayment.Model;
using MISL.Ababil.Agent.Module.BillPayment.Service;
using MISL.Ababil.Agent.Module.Common.Exporter;
using MISL.Ababil.Agent.Module.Common.UI.MessageUI;
using MISL.Ababil.Agent.Module.Common.UI.ProgressUI;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.Services.Communication;
using MISL.Ababil.Agent.UI;
using MISL.Ababil.Agent.UI.forms.ProgressUI;

namespace MISL.Ababil.Agent.Module.BillPayment.UI.AdminUI
{
    public partial class frmBillPaymentAdmin : CustomForm
    {
        int columnLoaded = 0;
        private List<DailyBillCollectionReportResultDto> _dailyBillCollectionReportResultDtos;
        private int _companyid = 1;
        private List<BillZoneInfo> _billZoneInfos;
        private DateTime _fromDate;

        public frmBillPaymentAdmin()
        {
            InitializeComponent();

            LoadAgents();
            LoadSubAgents();
        }

        private void LoadAgents()
        {
            try
            {
                List<AgentInformation> agentInformations = new AgentServices().getAgentInfoBranchWise();
                agentInformations.Insert(0, new AgentInformation { businessName = "(All)" });
                UtilityServices.fillComboBox(cbxAgents, new BindingSource
                {
                    DataSource = agentInformations
                }, "businessName", "id");
                cbxAgents.SelectedIndex = 0;
            }
            catch (Exception e)
            {
                // ignored
            }
        }

        private void LoadSubAgents()
        {
            try
            {
                List<SubAgentInformation> agentInformations
                    = new AgentServices()
                          .GetSubagentsByAgentId(((AgentInformation)cbxAgents.SelectedItem ?? new AgentInformation()).id)
                      ?? new List<SubAgentInformation>();
                agentInformations.Insert(0, new SubAgentInformation { name = "(All)" });
                UtilityServices.fillComboBox(cbxSubAgents, new BindingSource
                {
                    DataSource = agentInformations
                }, "name", "id");
                cbxSubAgents.SelectedIndex = 0;
            }
            catch (Exception e)
            {
                // ignored
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Search();
            try
            {
                customDataGridViewHeader.ExportReportSubtitleThree = "Bill Payment";
                DataGridHeaderUtility.ReportHederDataExportToPdf(customDataGridViewHeader);
            }
            catch
            {
                //suppressed
            }
        }

        //private string getOutletName(string voucherNumber)
        //{
        //    ConsumerServices consumerService = new ConsumerServices();
        //    RemittanceReportDto remittanceReportDto = consumerService.GetRemittanceReportDto(voucherNumber);
        //    if (remittanceReportDto.outletName != "")
        //    {
        //        return remittanceReportDto.outletName;
        //    }
        //    return "";
        //    //return "_";
        //}

        private void Search()
        {
            dgv.DataSource = null;
            dgv.Rows.Clear();
            dgv.Columns.Clear();
            DailyBillCollectionReportSearchDto dailyBillCollectionReportSearchDto = FillSearch();

            if (dailyBillCollectionReportSearchDto == null)
            {
                return;
            }

            try
            {
                ProgressUIManager.ShowProgress(this);
                _dailyBillCollectionReportResultDtos =
                    new WebClientCommunicator<DailyBillCollectionReportSearchDto, List<DailyBillCollectionReportResultDto>>().GetPostResult(dailyBillCollectionReportSearchDto,
                        "resources/report/bill/dailycollection");
                ProgressUIManager.CloseProgress();

                if (_dailyBillCollectionReportResultDtos != null)
                {

                    DataGridViewButtonColumn buttonColumn = new DataGridViewButtonColumn
                    {
                        Text = "Correct",
                        Width = 60,
                        UseColumnTextForButtonValue = true
                    };
                    dgv.Columns.Add(buttonColumn);

                    buttonColumn.Visible = dtpFromDate.Value == SessionInfo.currentDate && dtpToDate.Value == SessionInfo.currentDate ? true : false;

                    //DataGridViewButtonColumn printButtonColumn = new DataGridViewButtonColumn
                    //{
                    //    Text = "Print",
                    //    Width = 60,
                    //    UseColumnTextForButtonValue = true
                    //};
                    //dgv.Columns.Add(printButtonColumn);
                    ////columnLoaded = 1;

                    //if (SessionInfo.rights.Contains("REMITANCE_FIRST_APPROVE"))
                    //{
                    //    DataGridViewButtonColumn buttonColumn2 = new DataGridViewButtonColumn
                    //    {
                    //        Text = "Process",
                    //        UseColumnTextForButtonValue = true
                    //    };
                    //    dgv.Columns.Add(buttonColumn2);
                    //    //columnLoaded = 2;
                    //}
                    //else if (SessionInfo.rights.Contains("REMITANCE_SECOND_APPROVE"))
                    //{
                    //    DataGridViewButtonColumn buttonColumn2 = new DataGridViewButtonColumn
                    //    {
                    //        Text = "Process",
                    //        UseColumnTextForButtonValue = true
                    //    };
                    //    dgv.Columns.Add(buttonColumn2);
                    //    //columnLoaded = 2;
                    //}


                    dgv.DataSource = null;
                    dgv.DataSource = _dailyBillCollectionReportResultDtos.Select(o => new DailyBillCollectionReportResultDtoGrid(o)
                    {
                        voucherNumber = o.voucherNumber,
                        zoneName = o.zoneName,
                        subZoneName = o.subZoneName,
                        outletName = o.outletName,
                        outletUser = o.outletUser,
                        transactionDateTime = o.transactionDateTime,
                        collectionYearMonth = o.collectionYearMonth,
                        customerBillAccNo = o.customerBillAccNo,
                        customerName = o.customerName,
                        serviceChargeAmount = o.serviceChargeAmount,
                        billAmount = o.billAmount,
                        customerMobileNo = o.customerMobileNo,
                        vatAmount = o.vatAmount,
                        stampChargeAmount = o.stampChargeAmount,
                        lateFee = o.lateFee,
                        filter =
                        (
                            o.voucherNumber.ToString()
                        ).ToLower()
                    }).ToList();


                    try
                    {

                        dgv.Columns[dgv.Columns.Count - 1].Visible = false;
                    }
                    catch
                    {
                        //suppressed
                    }
                }
                else
                {
                    ProgressUIManager.CloseProgress();
                    btnSearch.Enabled = true;
                    MsgBox.showInformation("No applications available");
                }
            }
            catch (Exception ex)
            {
                ProgressUIManager.CloseProgress();
                btnSearch.Enabled = true;
                MsgBox.ShowError(ex.Message);
            }

            lblItemsFound.Text = "   Item(s) Found: " + dgv.Rows.Count.ToString();
            this.Enabled = true;
            this.UseWaitCursor = false;
        }

        private class DailyBillCollectionReportResultDtoGrid
        {
            public long? voucherNumber { get; set; }

            public string zoneName { get; set; }
            public string subZoneName { get; set; }
            public string outletName { get; set; }
            public string outletUser { get; set; }
            public string transactionDateTime { get; set; }
            public string collectionYearMonth { get; set; }
            public string customerBillAccNo { get; set; }
            public string customerName { get; set; }
            public decimal? serviceChargeAmount { get; set; }
            public decimal? billAmount { get; set; }
            public string customerMobileNo { get; set; }
            public decimal? vatAmount { get; set; }
            public decimal? stampChargeAmount { get; set; }
            public decimal? lateFee { get; set; }
            public string filter { get; set; }

            private DailyBillCollectionReportResultDto _dailyBillCollectionReportResultDto;

            public DailyBillCollectionReportResultDtoGrid(DailyBillCollectionReportResultDto dailyBillCollectionReportResultDto)
            {
                _dailyBillCollectionReportResultDto = dailyBillCollectionReportResultDto;
            }

            public DailyBillCollectionReportResultDto GetModel()
            {
                return _dailyBillCollectionReportResultDto;
            }
        }

        private DailyBillCollectionReportSearchDto FillSearch()
        {
            DateTime fromDate, toDate;

            try
            {
                //fromDate = UtilityServices.ParseDateTime(dtpFromDate.Date);                
                fromDate = DateTime.ParseExact(dtpFromDate.Date.Replace("/", "-"), "dd-MM-yyyy",
                    CultureInfo.InvariantCulture);
            }
            catch
            {
                MessageBox.Show("Please enter the Date in correct format.", this.Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return null;
            }

            try
            {
                //toDate = UtilityServices.ParseDateTime(dtpToDate.Date);
                toDate = DateTime.ParseExact(dtpToDate.Date.Replace("/", "-"), "dd-MM-yyyy",
                    CultureInfo.InvariantCulture);
            }
            catch
            {
                MessageBox.Show("Please enter the Date in correct format.", this.Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return null;
            }

            if (SessionInfo.currentDate < toDate)
            {
                MessageBox.Show("Future date is not allowed!", this.Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return null;
            }

            if (fromDate > toDate)
            {
                MessageBox.Show("From date can not be greater than to date.", this.Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return null;
            }

            return new DailyBillCollectionReportSearchDto
            {
                zoneId = ((BillZoneInfo)cbxZone.SelectedItem ?? new BillZoneInfo() { id = null }).id,
                subZoneId = ((BillSubZoneInfo)cbxSubZone.SelectedItem ?? new BillSubZoneInfo() { id = null }).id,
                voucherNumber = string.IsNullOrEmpty(txtVoucherNumber.Text) == false
                        ? ((long?)long.Parse(txtVoucherNumber.Text))
                        : null,
                agentId = ((AgentInformation)cbxAgents.SelectedItem ?? new AgentInformation()).id,
                subagentId = ((SubAgentInformation)cbxSubAgents.SelectedItem ?? new SubAgentInformation()).id,
                fromDate = UtilityServices.GetLongDate(fromDate),
                toDate = UtilityServices.GetLongDate(toDate)
            };
        }

        public static void FillComboBox(ComboBox cmb, BindingSource bs, string displayMember, string valueMember)
        {
            cmb.DataSource = bs;
            cmb.DisplayMember = displayMember;
            cmb.ValueMember = valueMember;
        }

        public static void FillDataGridView(DataGridView dgv, BindingSource bs, string dataMember)
        {
            dgv.DataSource = bs;
            dgv.DataMember = dataMember;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ResetSearch();
        }

        private void ResetSearch()
        {
            //txtRecipientName.Text = null;
            //txtRecipientNationalID.Text = null;
            txtSenderName.Text = null;
            //cmbNameofExchangeHouse.SelectedIndex = 0;
            //cmbNameofExchangeHouse.Focus();
        }

        private void frmRemittanceAdmin_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            UtilityServices.fillComboBox(cbxZone, new BindingSource()
            {
                DataSource = new WebClientCommunicator<object, List<BillZoneInfo>>().GetResult(
                    $"resources/bill/allzones/companyid/{_companyid}")
            }, "zoneName", "id");
            cbxZone.SelectedIndex = -1;
        }

        private void dgvRemittance_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView senderGrid = (DataGridView)sender;

            if (dgv.Rows.Count <= 0) return;
            if (e.RowIndex < 0 || !(senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn)) return;

            try
            {
                switch (dgv.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString())
                {
                    case "Correct":
                        {
                            if (MsgBox.showConfirmation("Are you sure to reverse this bill for currection?") == "yes")
                            {
                                new WebClientCommunicator<long?, object>().GetPutResult(_dailyBillCollectionReportResultDtos[e.RowIndex].voucherNumber, "resources/bill/correction");
                                MsgBox.showInformation("Bill Payment Transaction is reversed successfully.");
                                Search();
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        private void frmRemittanceAdmin_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void autoRefreshTimer_Tick(object sender, EventArgs e)
        {
            //Search();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                new ExportUtility().GeneratePDFFromDataGridView(
                        dgv,
                        "AIBL",
                        "AIBL Address",
                        "Outlet Name",
                        ""
                    );
            }
            catch (IndexOutOfRangeException)
            {
                MsgBox.showWarning("Data not available.");
            }
            catch (Exception ex)
            {
                MsgBox.showWarning(ex.Message);
            }
        }

        private void cbxZone_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!((CustomComboBoxDropDownList)sender).Focused)
            {
                return;
            }

            UtilityServices.fillComboBox(cbxSubZone, new BindingSource()
            {
                DataSource = ((BillZoneInfo)cbxZone.SelectedItem).billSubZoneInfos
            }, "billSubZoneName", "id");
            cbxSubZone.SelectedIndex = -1;
        }

        private void cbxZone_SelectionChangeCommitted(object sender, EventArgs e)
        {

        }

        private void cbxZone_SelectedValueChanged(object sender, EventArgs e)
        {

        }

        private void cbxZone_ValueMemberChanged(object sender, EventArgs e)
        {

        }

        private void cbxAgents_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSubAgents();
        }
    }
}