﻿using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Module.Common.Exporter;

namespace MISL.Ababil.Agent.Module.BillPayment.UI.AdminUI
{
    partial class frmBillPaymentAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBillPaymentAdmin));
            this.dgv = new System.Windows.Forms.DataGridView();
            this.lblSenderName = new System.Windows.Forms.Label();
            this.lblRecipientNationalID = new System.Windows.Forms.Label();
            this.lblRecipientName = new System.Windows.Forms.Label();
            this.lblNameofExchangeHouse = new System.Windows.Forms.Label();
            this.txtSenderName = new System.Windows.Forms.TextBox();
            this.lblItemsFound = new System.Windows.Forms.Label();
            this.autoRefreshTimer = new System.Windows.Forms.Timer(this.components);
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.customDataGridViewHeader = new MISL.Ababil.Agent.Module.Common.Exporter.CustomDataGridViewHeader();
            this.cbxZone = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cbxSubZone = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.txtVoucherNumber = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dtpFromDate = new MISL.Ababil.Agent.UI.forms.CustomControls.CustomDateTimePicker();
            this.dtpToDate = new MISL.Ababil.Agent.UI.forms.CustomControls.CustomDateTimePicker();
            this.cbxSubAgents = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cbxAgents = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.btnClsoe = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToResizeRows = false;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(113)))), ((int)(((byte)(198)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.ColumnHeadersHeight = 28;
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.GridColor = System.Drawing.Color.LightGray;
            this.dgv.Location = new System.Drawing.Point(1, 155);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(1165, 442);
            this.dgv.TabIndex = 16;
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRemittance_CellContentClick);
            // 
            // lblSenderName
            // 
            this.lblSenderName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblSenderName.AutoSize = true;
            this.lblSenderName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblSenderName.ForeColor = System.Drawing.Color.White;
            this.lblSenderName.Location = new System.Drawing.Point(521, 488);
            this.lblSenderName.Name = "lblSenderName";
            this.lblSenderName.Size = new System.Drawing.Size(80, 17);
            this.lblSenderName.TabIndex = 6;
            this.lblSenderName.Text = "Sender Name :";
            this.lblSenderName.UseCompatibleTextRendering = true;
            this.lblSenderName.Visible = false;
            // 
            // lblRecipientNationalID
            // 
            this.lblRecipientNationalID.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblRecipientNationalID.AutoSize = true;
            this.lblRecipientNationalID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblRecipientNationalID.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecipientNationalID.ForeColor = System.Drawing.Color.White;
            this.lblRecipientNationalID.Location = new System.Drawing.Point(73, 69);
            this.lblRecipientNationalID.Name = "lblRecipientNationalID";
            this.lblRecipientNationalID.Size = new System.Drawing.Size(96, 20);
            this.lblRecipientNationalID.TabIndex = 4;
            this.lblRecipientNationalID.Text = "Voucher Number :";
            this.lblRecipientNationalID.UseCompatibleTextRendering = true;
            // 
            // lblRecipientName
            // 
            this.lblRecipientName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblRecipientName.AutoSize = true;
            this.lblRecipientName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblRecipientName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecipientName.ForeColor = System.Drawing.Color.White;
            this.lblRecipientName.Location = new System.Drawing.Point(133, 39);
            this.lblRecipientName.Name = "lblRecipientName";
            this.lblRecipientName.Size = new System.Drawing.Size(35, 20);
            this.lblRecipientName.TabIndex = 2;
            this.lblRecipientName.Text = "Zone :";
            this.lblRecipientName.UseCompatibleTextRendering = true;
            // 
            // lblNameofExchangeHouse
            // 
            this.lblNameofExchangeHouse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblNameofExchangeHouse.AutoSize = true;
            this.lblNameofExchangeHouse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblNameofExchangeHouse.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameofExchangeHouse.ForeColor = System.Drawing.Color.White;
            this.lblNameofExchangeHouse.Location = new System.Drawing.Point(118, 8);
            this.lblNameofExchangeHouse.Name = "lblNameofExchangeHouse";
            this.lblNameofExchangeHouse.Size = new System.Drawing.Size(51, 20);
            this.lblNameofExchangeHouse.TabIndex = 0;
            this.lblNameofExchangeHouse.Text = "Shomity :";
            this.lblNameofExchangeHouse.UseCompatibleTextRendering = true;
            // 
            // txtSenderName
            // 
            this.txtSenderName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtSenderName.Location = new System.Drawing.Point(618, 488);
            this.txtSenderName.Name = "txtSenderName";
            this.txtSenderName.Size = new System.Drawing.Size(301, 20);
            this.txtSenderName.TabIndex = 7;
            this.txtSenderName.Visible = false;
            // 
            // lblItemsFound
            // 
            this.lblItemsFound.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblItemsFound.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblItemsFound.ForeColor = System.Drawing.Color.White;
            this.lblItemsFound.Location = new System.Drawing.Point(0, 598);
            this.lblItemsFound.Name = "lblItemsFound";
            this.lblItemsFound.Size = new System.Drawing.Size(1167, 24);
            this.lblItemsFound.TabIndex = 19;
            this.lblItemsFound.Text = "   Item(s) Found: 0";
            this.lblItemsFound.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // autoRefreshTimer
            // 
            this.autoRefreshTimer.Interval = 120000;
            this.autoRefreshTimer.Tick += new System.EventHandler(this.autoRefreshTimer_Tick);
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(1167, 126);
            this.customTitlebar1.TabIndex = 20;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 622);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1167, 1);
            this.panel1.TabIndex = 165;
            // 
            // customDataGridViewHeader
            // 
            this.customDataGridViewHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customDataGridViewHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(123)))), ((int)(((byte)(208)))));
            this.customDataGridViewHeader.ExportReportSubtitleOne = "AIBL";
            this.customDataGridViewHeader.ExportReportSubtitleThree = "Subtitle Three";
            this.customDataGridViewHeader.ExportReportSubtitleTwo = "Report";
            this.customDataGridViewHeader.ExportReportTitle = "Remittance";
            this.customDataGridViewHeader.Filter = this.dgv;
            this.customDataGridViewHeader.HeaderText = "Bill Payments";
            this.customDataGridViewHeader.Location = new System.Drawing.Point(1, 127);
            this.customDataGridViewHeader.Name = "customDataGridViewHeader";
            this.customDataGridViewHeader.Size = new System.Drawing.Size(1165, 28);
            this.customDataGridViewHeader.TabIndex = 169;
            // 
            // cbxZone
            // 
            this.cbxZone.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbxZone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxZone.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxZone.FormattingEnabled = true;
            this.cbxZone.InputScopeAllowEmpty = false;
            this.cbxZone.IsValid = null;
            this.cbxZone.Location = new System.Drawing.Point(175, 4);
            this.cbxZone.Name = "cbxZone";
            this.cbxZone.PromptText = "(Select)";
            this.cbxZone.ReadOnly = false;
            this.cbxZone.ShowMandatoryMark = false;
            this.cbxZone.Size = new System.Drawing.Size(330, 21);
            this.cbxZone.TabIndex = 170;
            this.cbxZone.ValidationErrorMessage = "Validation Error!";
            this.cbxZone.SelectedIndexChanged += new System.EventHandler(this.cbxZone_SelectedIndexChanged);
            this.cbxZone.SelectionChangeCommitted += new System.EventHandler(this.cbxZone_SelectionChangeCommitted);
            this.cbxZone.ValueMemberChanged += new System.EventHandler(this.cbxZone_ValueMemberChanged);
            this.cbxZone.SelectedValueChanged += new System.EventHandler(this.cbxZone_SelectedValueChanged);
            // 
            // cbxSubZone
            // 
            this.cbxSubZone.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbxSubZone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxSubZone.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxSubZone.FormattingEnabled = true;
            this.cbxSubZone.InputScopeAllowEmpty = false;
            this.cbxSubZone.IsValid = null;
            this.cbxSubZone.Location = new System.Drawing.Point(175, 35);
            this.cbxSubZone.Name = "cbxSubZone";
            this.cbxSubZone.PromptText = "(Select)";
            this.cbxSubZone.ReadOnly = false;
            this.cbxSubZone.ShowMandatoryMark = false;
            this.cbxSubZone.Size = new System.Drawing.Size(330, 21);
            this.cbxSubZone.TabIndex = 173;
            this.cbxSubZone.ValidationErrorMessage = "Validation Error!";
            // 
            // txtVoucherNumber
            // 
            this.txtVoucherNumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtVoucherNumber.BackColor = System.Drawing.Color.White;
            this.txtVoucherNumber.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Numeric;
            this.txtVoucherNumber.InputScopeAllowEmpty = false;
            this.txtVoucherNumber.InputScopeCustomString = null;
            this.txtVoucherNumber.IsValid = null;
            this.txtVoucherNumber.Location = new System.Drawing.Point(175, 66);
            this.txtVoucherNumber.MaxLength = 32767;
            this.txtVoucherNumber.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtVoucherNumber.Name = "txtVoucherNumber";
            this.txtVoucherNumber.PromptText = "(Type here...)";
            this.txtVoucherNumber.ReadOnly = false;
            this.txtVoucherNumber.ShowMandatoryMark = false;
            this.txtVoucherNumber.Size = new System.Drawing.Size(330, 21);
            this.txtVoucherNumber.TabIndex = 176;
            this.txtVoucherNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtVoucherNumber.ValidationErrorMessage = "Validation Error!";
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.panel3.Controls.Add(this.cbxAgents);
            this.panel3.Controls.Add(this.cbxSubAgents);
            this.panel3.Controls.Add(this.cbxSubZone);
            this.panel3.Controls.Add(this.txtVoucherNumber);
            this.panel3.Controls.Add(this.cbxZone);
            this.panel3.Controls.Add(this.dtpFromDate);
            this.panel3.Controls.Add(this.dtpToDate);
            this.panel3.Controls.Add(this.btnClsoe);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.btnReset);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.btnSearch);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.lblRecipientNationalID);
            this.panel3.Controls.Add(this.lblRecipientName);
            this.panel3.Controls.Add(this.lblNameofExchangeHouse);
            this.panel3.Location = new System.Drawing.Point(0, 30);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1167, 96);
            this.panel3.TabIndex = 180;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtpFromDate.Date = "25/07/2018";
            this.dtpFromDate.Location = new System.Drawing.Point(644, 64);
            this.dtpFromDate.MaximumSize = new System.Drawing.Size(400, 25);
            this.dtpFromDate.MinimumSize = new System.Drawing.Size(60, 25);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.PresetServerDate = true;
            this.dtpFromDate.Size = new System.Drawing.Size(150, 25);
            this.dtpFromDate.TabIndex = 187;
            this.dtpFromDate.Value = new System.DateTime(2018, 7, 25, 11, 36, 2, 316);
            // 
            // dtpToDate
            // 
            this.dtpToDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtpToDate.Date = "25/07/2018";
            this.dtpToDate.Location = new System.Drawing.Point(828, 64);
            this.dtpToDate.MaximumSize = new System.Drawing.Size(400, 25);
            this.dtpToDate.MinimumSize = new System.Drawing.Size(60, 25);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.PresetServerDate = true;
            this.dtpToDate.Size = new System.Drawing.Size(150, 25);
            this.dtpToDate.TabIndex = 187;
            this.dtpToDate.Value = new System.DateTime(2018, 7, 25, 11, 36, 2, 316);
            // 
            // cbxSubAgents
            // 
            this.cbxSubAgents.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbxSubAgents.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxSubAgents.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxSubAgents.FormattingEnabled = true;
            this.cbxSubAgents.InputScopeAllowEmpty = false;
            this.cbxSubAgents.IsValid = null;
            this.cbxSubAgents.Location = new System.Drawing.Point(646, 35);
            this.cbxSubAgents.Name = "cbxSubAgents";
            this.cbxSubAgents.PromptText = "(Select)";
            this.cbxSubAgents.ReadOnly = false;
            this.cbxSubAgents.ShowMandatoryMark = false;
            this.cbxSubAgents.Size = new System.Drawing.Size(330, 21);
            this.cbxSubAgents.TabIndex = 173;
            this.cbxSubAgents.ValidationErrorMessage = "Validation Error!";
            // 
            // cbxAgents
            // 
            this.cbxAgents.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbxAgents.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxAgents.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxAgents.FormattingEnabled = true;
            this.cbxAgents.InputScopeAllowEmpty = false;
            this.cbxAgents.IsValid = null;
            this.cbxAgents.Location = new System.Drawing.Point(646, 4);
            this.cbxAgents.Name = "cbxAgents";
            this.cbxAgents.PromptText = "(Select)";
            this.cbxAgents.ReadOnly = false;
            this.cbxAgents.ShowMandatoryMark = false;
            this.cbxAgents.Size = new System.Drawing.Size(330, 21);
            this.cbxAgents.TabIndex = 170;
            this.cbxAgents.ValidationErrorMessage = "Validation Error!";
            this.cbxAgents.SelectedIndexChanged += new System.EventHandler(this.cbxAgents_SelectedIndexChanged);
            // 
            // btnClsoe
            // 
            this.btnClsoe.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnClsoe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnClsoe.FlatAppearance.BorderSize = 0;
            this.btnClsoe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClsoe.Image = global::MISL.Ababil.Agent.Module.BillPayment.Properties.Resources.close;
            this.btnClsoe.Location = new System.Drawing.Point(985, 64);
            this.btnClsoe.Name = "btnClsoe";
            this.btnClsoe.Size = new System.Drawing.Size(96, 25);
            this.btnClsoe.TabIndex = 70;
            this.btnClsoe.Text = "Close";
            this.btnClsoe.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClsoe.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClsoe.UseVisualStyleBackColor = false;
            this.btnClsoe.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.label4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(803, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "To";
            this.label4.UseCompatibleTextRendering = true;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.label3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(606, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Date :";
            this.label3.UseCompatibleTextRendering = true;
            // 
            // btnReset
            // 
            this.btnReset.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnReset.FlatAppearance.BorderSize = 0;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.Image = global::MISL.Ababil.Agent.Module.BillPayment.Properties.Resources.Broom_16;
            this.btnReset.Location = new System.Drawing.Point(985, 33);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(96, 25);
            this.btnReset.TabIndex = 69;
            this.btnReset.Text = "Reset";
            this.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(598, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Outlet :";
            this.label2.UseCompatibleTextRendering = true;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Image = global::MISL.Ababil.Agent.Module.BillPayment.Properties.Resources.Search_16__1_;
            this.btnSearch.Location = new System.Drawing.Point(985, 2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(96, 25);
            this.btnSearch.TabIndex = 68;
            this.btnSearch.Text = "Search";
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(599, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Agent :";
            this.label1.UseCompatibleTextRendering = true;
            // 
            // frmBillPaymentAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1167, 623);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.customDataGridViewHeader);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.lblItemsFound);
            this.Controls.Add(this.txtSenderName);
            this.Controls.Add(this.lblSenderName);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmBillPaymentAdmin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bill Payments";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmRemittanceAdmin_FormClosing);
            this.Load += new System.EventHandler(this.frmRemittanceAdmin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Label lblSenderName;
        private System.Windows.Forms.Label lblRecipientNationalID;
        private System.Windows.Forms.Label lblRecipientName;
        private System.Windows.Forms.Label lblNameofExchangeHouse;
        private System.Windows.Forms.TextBox txtSenderName;
        private System.Windows.Forms.Label lblItemsFound;
        private System.Windows.Forms.Timer autoRefreshTimer;
        private CustomTitlebar customTitlebar1;
        private System.Windows.Forms.Button btnClsoe;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Panel panel1;
        private CustomDataGridViewHeader customDataGridViewHeader;
        private CustomComboBoxDropDownList cbxSubZone;
        private CustomComboBoxDropDownList cbxZone;
        private CustomTextBox txtVoucherNumber;
        private System.Windows.Forms.Panel panel3;
        private CustomComboBoxDropDownList cbxSubAgents;
        private CustomComboBoxDropDownList cbxAgents;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Agent.UI.forms.CustomControls.CustomDateTimePicker dtpToDate;
        private System.Windows.Forms.Label label4;
        private Agent.UI.forms.CustomControls.CustomDateTimePicker dtpFromDate;
    }
}