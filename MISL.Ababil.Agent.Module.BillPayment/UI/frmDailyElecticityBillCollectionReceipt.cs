﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using MISL.Ababil.Agent.Module.BillPayment.Model;
using MISL.Ababil.Agent.Module.BillPayment.Report;
using MISL.Ababil.Agent.Module.BillPayment.Service;
using MISL.Ababil.Agent.Module.Common.UI;
using MISL.Ababil.Agent.Module.Common.UI.MessageUI;
using MISL.Ababil.Agent.Report;
using MISL.Ababil.Agent.Report.DataSets;
using MISL.Ababil.Agent.Report.Reports;
using MISL.Ababil.Agent.Services;
using GUI = MISL.Ababil.Agent.Report.GUI;
using MISL.Ababil.Agent.Module.BillPayment.DataSets;

namespace MISL.Ababil.Agent.Module.BillPayment.UI
{
    public partial class frmDailyElecticityBillCollectionReceipt : CustomForm
    {
        public GUI gui = new GUI();
        List<AgentInformation> objAgentInfoList = new List<AgentInformation>();
        AgentServices objAgentServices = new AgentServices();
        AgentInformation agentInformation = new AgentInformation();
        AgentServices agentServices = new AgentServices();
        DailyElecticityBillCollectionDS agentDS = new DailyElecticityBillCollectionDS();
        List<BillSubZoneInfo> objBillSubZoneInfoList = new List<BillSubZoneInfo>();

        public frmDailyElecticityBillCollectionReceipt()
        {
            InitializeComponent();
            GetSetupData();
            controlActivity();
            ConfigUIEnhancement();
        }

        public void ConfigUIEnhancement()
        {
            gui = new GUI(this);
            gui.Config(ref cmbAgentName, ValidCheck.VALIDATIONTYPES.COMBOBOX_EMPTY, null);
            gui.Config(ref cmbSubAgnetName);
            gui.Config(ref dtpFromDate);
            gui.Config(ref dtpToDate);
        }

        private void controlActivity()
        {
            //try
            //{
            if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_CENTRALLY.ToString())) //branch user
            {
                cmbAgentName.Enabled = true;
                cmbSubAgnetName.Enabled = true;
            }
            else if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_AGENTWISE.ToString())) //agent user
            {
                cmbAgentName.SelectedValue = UtilityServices.getCurrentAgent().id;
                cmbAgentName.Enabled = false;
                cmbSubAgnetName.Enabled = true;
                agentInformation = agentServices.getAgentInfoById(UtilityServices.getCurrentAgent().id.ToString());
                setSubagent();
            }
            else
            {
                SubAgentInformation currentSubagentInfo = UtilityServices.getCurrentSubAgent();
                cmbAgentName.SelectedValue = currentSubagentInfo.agent.id;
                string r = currentSubagentInfo.agent.id.ToString();
                agentInformation = agentServices.getAgentInfoById(r);
                setSubagent();
                cmbAgentName.Enabled = false;
                cmbSubAgnetName.SelectedValue = currentSubagentInfo.id;
                cmbSubAgnetName.Enabled = false;
                SetZoneList();
            }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //    //cmbAgentName.Enabled = true;
            //    //cmbSubAgnetName.Enabled = true;
            //}
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            try
            {
                if (CheckInput())
                {
                    Cursor.Current = Cursors.WaitCursor;
                    //chacking valid date
                    DateTime fromDate, toDate;
                    try
                    {
                        //fromDate = UtilityServices.ParseDateTime(dtpFromDate.Date);                
                        fromDate = DateTime.ParseExact(dtpFromDate.Date.Replace("/", "-"), "dd-MM-yyyy",
                            CultureInfo.InvariantCulture);
                    }
                    catch
                    {
                        Cursor.Current = Cursors.Default;
                        MessageBox.Show("Please enter the Date in correct format.", this.Text, MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);
                        return;
                    }
                    try
                    {
                        //toDate = UtilityServices.ParseDateTime(dtpToDate.Date);
                        toDate = DateTime.ParseExact(dtpToDate.Date.Replace("/", "-"), "dd-MM-yyyy",
                            CultureInfo.InvariantCulture);
                    }
                    catch
                    {
                        Cursor.Current = Cursors.Default;
                        MessageBox.Show("Please enter the Date in correct format.", this.Text, MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);
                        return;
                    }

                    if (SessionInfo.currentDate < toDate)
                    {
                        Cursor.Current = Cursors.Default;
                        MessageBox.Show("Future date is not allowed!", this.Text, MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);
                        return;
                    }
                    if (fromDate > toDate)
                    {
                        Cursor.Current = Cursors.Default;
                        MessageBox.Show("From date can not be greater than to date.", this.Text, MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);
                        return;
                    }

                    if (cmbAgentName.SelectedIndex > 0)
                    {
                        crDailyElecticityBillCollectionReceipt report = new crDailyElecticityBillCollectionReceipt();
                        frmReportViewer viewer = new frmReportViewer();

                        ReportHeaders rptHeaders = new ReportHeaders();
                        rptHeaders = UtilityServices.getReportHeaders("Daily Electricity Bill Collection");

                        TextObject txtBankName = report.ReportDefinition.ReportObjects["txtBankName"] as TextObject;
                        TextObject txtBranchName = report.ReportDefinition.ReportObjects["txtBranchName"] as TextObject;
                        //TextObject txtBranchAddress = report.ReportDefinition.ReportObjects["txtBranchAddress"] as TextObject;
                        TextObject txtReportHeading =
                            report.ReportDefinition.ReportObjects["txtReportHeading"] as TextObject;
                        TextObject txtPrintUser = report.ReportDefinition.ReportObjects["txtPrintUser"] as TextObject;
                        TextObject txtPrintDate = report.ReportDefinition.ReportObjects["txtPrintDate"] as TextObject;
                        TextObject txtFromDate = report.ReportDefinition.ReportObjects["txtFromDate"] as TextObject;
                        TextObject txtToDate = report.ReportDefinition.ReportObjects["txtToDate"] as TextObject;

                        if (rptHeaders != null)
                        {
                            if (rptHeaders.branchDto != null)
                            {
                                txtBankName.Text = rptHeaders.branchDto.bankName;
                                txtBranchName.Text = rptHeaders.branchDto.branchName;
                                // txtBranchAddress.Text = rptHeaders.branchDto.branchAddress;
                            }
                            txtReportHeading.Text = rptHeaders.reportHeading;
                            txtPrintUser.Text = rptHeaders.printUser;
                            txtPrintDate.Text = rptHeaders.printDate.ToString("dd-MM-yyyy").Replace("-", "/");
                        }
                        //txtFromDate.Text = Convert.ToDateTime(dtpFromDate.Text).ToString("dd/MM/yyyy");
                        //txtToDate.Text = Convert.ToDateTime(dtpToDate.Text).ToString("dd/MM/yyyy");

                        txtFromDate.Text = dtpFromDate.Date;
                        txtToDate.Text = dtpToDate.Date;

                        LoadAccountMonitoring();
                        report.SetDataSource(agentDS);
                        viewer.crvReportViewer.ReportSource = report;

                        Cursor.Current = Cursors.Default;
                        viewer.Show(this.Parent);
                    }
                    else
                    {
                        Cursor.Current = Cursors.Default;
                        MsgBox.showWarning("Please select an option!");
                    }
                    Cursor.Current = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Default;
                MsgBox.ShowError(ex.Message);
            }
        }

        private bool CheckInput()
        {
            if (cmbAgentName.SelectedIndex == 0)
            {
                MsgBox.ShowError("Please select an agent.");
            }
            if (cmbSubAgnetName.SelectedIndex == 0)
            {
                MsgBox.ShowError("Please select an Outlet.");
            }
            if (cmbAgentName.SelectedIndex > 1 || cmbSubAgnetName.SelectedIndex > 1)
            {
                if (cmbSubAgnetName.SelectedIndex > 1)
                {
                    if (cbxZone.SelectedIndex < 0)
                    {
                        MsgBox.ShowError("Please select a somity");
                        return false;
                    }

                    //if (cmbAgentName.SelectedIndex < 1)
                    //{
                    //    MsgBox.ShowError("Please select an agent");
                    //    return false;
                    //}

                    //if (cmbSubAgnetName.SelectedIndex < 1)
                    //{
                    //    MsgBox.ShowError("Please select an outlet");
                    //    return false;
                    //}
                    if (cbxSubZone.SelectedIndex < 1)
                    {
                        MsgBox.ShowError("Plese select a zone");
                        return false;
                    }
                }
            }
            return true;
        }

        private void LoadAccountMonitoring()
        {
            //AccountMonitoringInfo accountMonitoringInfo;
            //_accountMonitoringList.Clear();
            DailyBillCollectionReportSearchDto accountSearchDto = FillAccountSearchDto();
            ServiceResult result;
            agentDS = new DailyElecticityBillCollectionDS();

            try
            {
                result = AgentServices.GetDailyBillCollection(accountSearchDto);

                if (!result.Success)
                {
                    MessageBox.Show(result.Message);
                    return;
                }

                List<DailyBillCollectionReportResultDto> accountMonitoring = result.ReturnedObject as List<DailyBillCollectionReportResultDto>;

                if (accountMonitoring != null)
                {
                    foreach (DailyBillCollectionReportResultDto account in accountMonitoring)
                    {
                        DailyElecticityBillCollectionDS.DailyElecticityBillCollectionDSRow newRow = agentDS._DailyElecticityBillCollectionDS.NewDailyElecticityBillCollectionDSRow();

                        newRow.agentId = account.agentId.ToString();
                        newRow.agentName = account.agentName;

                        newRow.outletId = account.outletId.ToString();
                        newRow.outletName = account.outletName;

                        newRow.zoneId = account.zoneId.ToString();
                        newRow.zoneName = account.zoneName;

                        newRow.subZoneId = account.subZoneId.ToString();
                        newRow.subZoneName = account.subZoneName;


                        newRow.trDateTime = account.transactionDateTime;

                        newRow.customerBillAccNo = account.customerBillAccNo;
                        newRow.customerName = account.customerName;
                        newRow.customerMobileNo = account.customerMobileNo;

                        newRow.collectionMonthYear = account.collectionYearMonth;

                        newRow.serviceChargeAmount = account.serviceChargeAmount ?? 0;
                        newRow.stampChargeAmount = account.stampChargeAmount ?? 0;
                        newRow.vatAmount = account.vatAmount ?? 0;
                        newRow.lateFee = account.lateFee ?? 0;
                        newRow.billAmount = account.billAmount ?? 0;
                        newRow.billNo = account.billNo;

                        agentDS._DailyElecticityBillCollectionDS.AddDailyElecticityBillCollectionDSRow(newRow);
                    }
                }
                agentDS.AcceptChanges();
            }
            catch (Exception)
            {
                //ignored
            }
        }

        private DailyBillCollectionReportSearchDto FillAccountSearchDto()
        {
            DailyBillCollectionReportSearchDto accountSearchDto = new DailyBillCollectionReportSearchDto();

            if (this.cmbAgentName.SelectedIndex > -1)
            {
                if (cmbAgentName.SelectedIndex != 0 || cmbAgentName.SelectedIndex != 1)
                {
                    accountSearchDto.agentId = (long)cmbAgentName.SelectedValue;
                }
                if (cmbAgentName.SelectedIndex == 1 || cmbAgentName.SelectedIndex == 0)
                {
                    accountSearchDto.agentId = null; // new AgentInformation();
                }

            }
            if (this.cmbSubAgnetName.SelectedIndex > -1)
            {
                if (cmbSubAgnetName.SelectedIndex != 0 || cmbSubAgnetName.SelectedIndex != 1)
                {
                    accountSearchDto.subagentId = (long)cmbSubAgnetName.SelectedValue;
                }
                if (cmbSubAgnetName.SelectedIndex == 1 || cmbSubAgnetName.SelectedIndex == 0)
                {
                    accountSearchDto.subagentId = null; // new SubAgentInformation();
                }

            }
            if (this.cbxZone.SelectedIndex > -1)
            {
                if (cbxZone.SelectedIndex >= 0)
                {
                    accountSearchDto.zoneId = (long)cbxZone.SelectedValue;
                }
               // if (cbxZone.SelectedIndex == 1 )
                //{
                  //  accountSearchDto.zoneId = null; // new SubAgentInformation();
                //}

            }
            if (this.cbxSubZone.SelectedIndex > -1)
            {
                if (cbxSubZone.SelectedIndex > 1)
                {
                    accountSearchDto.subZoneId = (long)cbxSubZone.SelectedValue;
                }
                if (cbxSubZone.SelectedIndex == 1 )
                {
                    accountSearchDto.subZoneId = null; // new SubAgentInformation();
                }

            }

            accountSearchDto.fromDate = UtilityServices.GetLongDate
            (
                DateTime.ParseExact(dtpFromDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture)
            );

            accountSearchDto.toDate = UtilityServices.GetLongDate
            (
                DateTime.ParseExact(dtpToDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture)
            );

            return accountSearchDto;
        }

        private void GetSetupData()
        {
            //string configvalue1 = ConfigurationManager.AppSettings["countryId"];
            try
            {
                SetAgentList();
                //SetZoneList();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SetZoneList()
        {
            if (cmbSubAgnetName.SelectedValue == null)
            {
                return;
            }
            long r = ((SubAgentInformation)cmbSubAgnetName.SelectedItem).id;
            UtilityServices.fillComboBox(cbxZone, new BindingSource()
            {
                DataSource = new BillPaymentService().GetBillZoneInfosByCompanyId(r, 1)
            }, "zoneName", "id");
            cbxZone.SelectedIndex = -1;
        }

        private void SetAgentList()
        {
            //objAgentInfoList = objAgentServices.getAgentInfoBranchWise();
            objAgentInfoList = objAgentServices.GetAgentsForBill();
            BindingSource bs = new BindingSource();
            bs.DataSource = objAgentInfoList;

            AgentInformation agSelect = new AgentInformation();
            agSelect.businessName = "(Select)";
            objAgentInfoList.Insert(0, agSelect);
            AgentInformation agAll = new AgentInformation();
            agAll.businessName = "(All)";
            objAgentInfoList.Insert(1, agAll);

            UtilityServices.fillComboBox(cmbAgentName, bs, "businessName", "id");
            //            cmbAgentName.Text = "Select";
            cmbAgentName.SelectedIndex = 0;
        }
        private void cmbAgentName_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!cb.Focused)
            {
                return;
            }

            try
            {
                if (cmbAgentName.SelectedIndex > 1)
                {
                    agentInformation = agentServices.getAgentInfoById(cmbAgentName.SelectedValue.ToString());
                }
                if (cmbAgentName.SelectedIndex == 1)
                {
                    //agentInformation = agentServices.getAgentInfoById(null);
                }
                if (cmbAgentName.SelectedIndex < 2)
                {
                    cmbSubAgnetName.DataSource = null;
                    cmbSubAgnetName.Items.Clear();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            setSubagent();
        }

        private void setSubagent()
        {
            if (agentInformation != null)
            {
                BindingSource bs = new BindingSource();
                bs.DataSource = agentInformation.subAgents;


                SubAgentInformation saiSelect = new SubAgentInformation();
                saiSelect.name = "(Select)";

                SubAgentInformation saiAll = new SubAgentInformation();
                saiAll.name = "(All)";

                agentInformation.subAgents.Insert(0, saiSelect);
                agentInformation.subAgents.Insert(1, saiAll);


                UtilityServices.fillComboBox(cmbSubAgnetName, bs, "name", "id");
                if (cmbSubAgnetName.Items.Count > 0)
                {
                    cmbSubAgnetName.SelectedIndex = 0;
                }
            }
        }

        private void frmTrMonitoringRport_Load(object sender, EventArgs e)
        {
            dtpFromDate.Value = SessionInfo.currentDate;
            dtpToDate.Value = SessionInfo.currentDate;
        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {

        }

        private void frmAccountMonitoringRport_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void lblSubAgent_Click(object sender, EventArgs e)
        {

        }

       

        private void cbxZone_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!cbxZone.Focused)
                {
                    return;
                }


                objBillSubZoneInfoList =
                    new BillPaymentService().GetBillZoneInfoWithSubZoneByZoneId((long)cbxZone.SelectedValue)
                        .billSubZoneInfos;


                BindingSource bs = new BindingSource();
                bs.DataSource = objBillSubZoneInfoList;

                BillSubZoneInfo agSelect = new BillSubZoneInfo();
                agSelect.billSubZoneName = "(Select)";
                objBillSubZoneInfoList.Insert(0, agSelect);
                BillSubZoneInfo agAll = new BillSubZoneInfo();
                agAll.billSubZoneName = "(All)";
                objBillSubZoneInfoList.Insert(1, agAll);

                UtilityServices.fillComboBox(cbxSubZone, bs, "billSubZoneName", "id");
                //            cmbAgentName.Text = "Select";
                cbxSubZone.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
            //cbxSubZone.SelectedIndex = 0;
        }

        private void cmbSubAgnetName_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!cb.Focused)
            {
                return;
            }
            SetZoneList();
        }
    }
    //public class AccountMonitoringInfo
    //{
    //    public long AgentId = 0;
    //    public string AgentName = "";
    //    public long SubAgentId = 0;
    //    public string SubAgentName = "";
    //    public decimal? AgentAccBalance = 0;

    //    public long NoOfMSDAccount = 0;
    //    public decimal MSDAccBalance = 0;

    //    public long NoOfCDAccount = 0;
    //    public decimal CDAccBalance = 0;

    //    public long NoOfITDAccount = 0;
    //    public decimal ITDAccBalance = 0;

    //    public long NoOfMTDAccount = 0;
    //    public decimal MTDAccBalance = 0;

    //    public long NoOfInvestmentAccount = 0;
    //    public decimal InvestedAmount = 0;
    //}


}
