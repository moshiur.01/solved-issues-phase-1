﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Module.BillPayment.Model;

using MISL.Ababil.Agent.Services.Communication;

namespace MISL.Ababil.Agent.Module.BillPayment.Service
{
    public class BillPaymentService
    {
        public List<BillZoneInfo> GetBillZoneInfosByCompanyId(long outletId, long companyid = 1)
        {
            return new WebClientCommunicator<object, List<BillZoneInfo>>().GetResult("resources/bill/zones/companyid/" + companyid.ToString() + "/" + outletId.ToString());
        }

        public BillZoneInfo GetBillZoneInfoWithSubZoneByZoneId(long zoneid)
        {
            return new WebClientCommunicator<object, BillZoneInfo>().GetResult("resources/bill/zone/id/" + zoneid.ToString());
        }

        public BillCustomerInfo GetBillCustomerInfoByAccNoAndSubzoneId(string billaccount, long subzoneid)
        {
            return new WebClientCommunicator<object, BillCustomerInfo>().GetResult("resources/bill/customer/accountandsubzone/" + billaccount + "/" + subzoneid);
        }

        public decimal GetBillCharge(long companyid, decimal billamount)
        {
            return new WebClientCommunicator<object, decimal>().GetResult("resources/bill/charge/companyandamount/" + companyid + "/" + billamount);
        }

        public decimal GetBillStampCharge(long companyid, decimal billamount)
        {
            return new WebClientCommunicator<object, decimal>().GetResult("resources/bill/stampcharge/companyandamount/" + companyid + "/" + billamount);
        }

        public string DoBillPayment(BillPaymentDto billPaymentDto)
        {
            return new WebClientCommunicator<BillPaymentDto, string>().GetPostResult(billPaymentDto, "resources/bill/pollibidyut/payment");
        }

        public void AddNewZone(BillZoneInfo billZoneInfo)
        {
            new WebClientCommunicator<BillZoneInfo, object>().GetPostResult(billZoneInfo, "resources/bill/zoneinfo/save");
        }

        public void AddNewSubZone(BillSubZoneInfo billSubZoneInfo)
        {
            new WebClientCommunicator<BillSubZoneInfo, object>().GetPostResult(billSubZoneInfo, "resources/bill/subzoneinfo/save");
        }
    }
}