﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.consumer
{
    public class ConsumerInformationDto
    {

        private static long serialVersionUID = 1L;
        public long id { get; set; }

        public String consumerTitle { get; set; }

        public String photo { get; set; }

        public String mobileNumber { get; set; }

        public int numberOfOperator { get; set; }

        public List<AccountOperator> accountOperators { get; set; }




    }
}
