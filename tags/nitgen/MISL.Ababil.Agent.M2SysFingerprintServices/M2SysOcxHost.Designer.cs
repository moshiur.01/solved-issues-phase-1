﻿namespace MISL.Ababil.Agent.M2SysFingerprintServices
{
    partial class M2SysOcxHost
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(M2SysOcxHost));
            this.axBioPlugInActX1 = new AxBIOPLUGINACTXLib.AxBioPlugInActX();
            ((System.ComponentModel.ISupportInitialize)(this.axBioPlugInActX1)).BeginInit();
            this.SuspendLayout();
            // 
            // axBioPlugInActX1
            // 
            this.axBioPlugInActX1.Enabled = true;
            this.axBioPlugInActX1.Location = new System.Drawing.Point(334, 155);
            this.axBioPlugInActX1.Name = "axBioPlugInActX1";
            this.axBioPlugInActX1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axBioPlugInActX1.OcxState")));
            this.axBioPlugInActX1.Size = new System.Drawing.Size(100, 50);
            this.axBioPlugInActX1.TabIndex = 0;
            this.axBioPlugInActX1.OnCapture += new System.EventHandler(this.axBioPlugInActX1_OnCapture);
            // 
            // M2SysOcxHost
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(760, 371);
            this.Controls.Add(this.axBioPlugInActX1);
            this.Name = "M2SysOcxHost";
            this.Text = "Fingerprint Manager";
            this.Load += new System.EventHandler(this.M2SysOcxHost_Load);
            ((System.ComponentModel.ISupportInitialize)(this.axBioPlugInActX1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private AxBIOPLUGINACTXLib.AxBioPlugInActX axBioPlugInActX1;
    }
}