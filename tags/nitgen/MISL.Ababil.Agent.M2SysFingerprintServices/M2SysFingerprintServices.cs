﻿using System;
using MISL.Ababil.Agent.GenericFingerprintServices;

namespace MISL.Ababil.Agent.M2SysFingerprintServices
{
    public class M2SysFingerprintServices : AbstractFingerprintDevice
    {
        public override void capture()
        {
            M2SysOcxHost ocxHost = new M2SysOcxHost();
            ocxHost.ShowDialog();
            while (ocxHost.stillCapturing)
            {
                System.Windows.Forms.Application.DoEvents();
            }

            notifyObservers(FingerprintEvents.Captured, ocxHost.data);
        }

    }
}
