﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MISL.Ababil.Agent.M2SysFingerprintServices
{
    public partial class M2SysOcxHost : Form
    {
        public Object data;
        public bool stillCapturing;

        public M2SysOcxHost()
        {
            InitializeComponent();
        }

        private void M2SysOcxHost_Load(object sender, EventArgs e)
        {
            stillCapturing = true;
            axBioPlugInActX1.CaptureFingerData();
        }

        private void axBioPlugInActX1_OnCapture(object sender, EventArgs e)
        {
            AxBIOPLUGINACTXLib.AxBioPlugInActX x = (AxBIOPLUGINACTXLib.AxBioPlugInActX)sender;

            if (x.result == "0")
            {
                string safeFingerData = axBioPlugInActX1.GetSafeLeftFingerData();
                if (!string.IsNullOrEmpty(safeFingerData))
                {
                    data = safeFingerData;
                }
                else
                {
                    data = "";
                    MessageBox.Show("Subagent finger print needed.");
                }
                this.Close();
                stillCapturing = false;
            }
        }
    }
}
