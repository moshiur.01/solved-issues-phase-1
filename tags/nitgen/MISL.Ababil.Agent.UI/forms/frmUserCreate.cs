﻿using MISL.Ababil.Agent.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.fingerprint;
using MISL.Ababil.Agent.Infrastructure.Validation;

namespace MISL.Ababil.Agent.UI.forms
{
    public partial class frmUserCreate : Form
    {
        frmSubAgent frmsubagent;
        private List<BiometricTemplate> fingerList;

        public frmUserCreate()
        {
            InitializeComponent();
            frmsubagent = new frmSubAgent();

            ConfigureValidation();
        }

        private void ConfigureValidation()
        {
            ValidationManager.ConfigureValidation(this, txtUserName, "User Name", (long)ValidationType.UserName, true);
            ValidationManager.ConfigureValidation(this, txtPassword, "Password", (long)ValidationType.StrongPassword, true);
        }

        private bool validationCheck()
        {
            return ValidationManager.ValidateForm(this);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmFingerprintCapture objFrmFinger = new frmFingerprintCapture(txtUserName.Text.Trim());



            DialogResult dr = objFrmFinger.ShowDialog();
            if (dr == DialogResult.OK)
            {

                fingerList = objFrmFinger.bioMetricTemplates;

            }
            if (fingerList != null)
                lblFingerprintResult.Text = "Fingerprint captured successfully";
            else
                lblFingerprintResult.Text = "Fingerprint capture failed";
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (validationCheck())
            {
                btnAdd.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
                //put "ADD" code here.
            }
        }

        private void frmUserCreate_Load(object sender, EventArgs e)
        {

        }

        public SubAgentUser newUsercredentials()
        {


            SubAgentUser usrInfo = new SubAgentUser();

            usrInfo.username = txtUserName.Text;
            usrInfo.password = txtPassword.Text;
            usrInfo.fingerDatas = fingerList;
            return usrInfo;

        }

        private void frmUserCreate_FormClosing(object sender, FormClosingEventArgs e)
        {
            ValidationManager.ReleaseValidationData(this);
        }

    }
}
