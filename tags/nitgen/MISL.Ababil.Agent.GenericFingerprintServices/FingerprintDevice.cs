﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MISL.Ababil.Agent.GenericFingerprintServices
{
    public interface FingerprintDevice
    {

        void capture();

        void registerEventObserver(FingerprintEventObserver observer);

        void notifyObservers(FingerprintEvents eventSpec, object eventData);

    }
}
