﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.GenericFingerprintServices
{
    abstract public class AbstractFingerprintDevice : FingerprintDevice
    {
        ICollection<FingerprintEventObserver> _observers = new List<FingerprintEventObserver>();

        abstract public void capture();

        public void notifyObservers(FingerprintEvents eventSpec, object eventData)
        {
            foreach (FingerprintEventObserver observer in _observers)
            {
                if (observer!=null)
                {
                    observer.FingerPrintEventOccured(FingerprintEvents.Captured, eventData);
                }
            }
        }

        public void registerEventObserver(FingerprintEventObserver observer)
        {
            if (observer!=null)
            {
                _observers.Add (observer);
            }
        }

        ~AbstractFingerprintDevice() 
        {
            _observers.Clear();
        }
    }
}
