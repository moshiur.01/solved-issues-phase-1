﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MISL.Ababil.Agent.GenericFingerprintServices
{
    public interface FingerprintEventObserver
    {
        void FingerPrintEventOccured( FingerprintEvents eventSpec, object EventData);
    }
}
