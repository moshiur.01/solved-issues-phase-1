﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NITGEN.SDK.NBioBSP;

namespace MISL.Ababil.Agent.NitgenFingerprintServices
{
    public partial class NitgenOcxHost : Form
    {
        public string Data;
        public bool StillCapturing = true;

        NBioAPI m_NBioAPI;
        NBioAPI.Export m_Export;

        private bool m_bBspInit;

        private void BSP_Init()
        {

            m_bBspInit = false;

            m_NBioAPI = new NBioAPI();
            m_Export = new NBioAPI.Export(m_NBioAPI);

            if (m_NBioAPI.OpenDevice(NBioAPI.Type.DEVICE_ID.AUTO) == NBioAPI.Error.NONE)
            {
                m_bBspInit = true;
            }

        }

        private void BSP_Dispose()
        {
            m_NBioAPI.CloseDevice(NBioAPI.Type.DEVICE_ID.AUTO);
        }


        public NitgenOcxHost()
        {
            InitializeComponent();
            m_NBioAPI = new NBioAPI();

        }

        private void NitgenOcxHost_Load(object sender, EventArgs e)
        {
            //CaptureFingerprint();
            Register();
        }

        private void CaptureFingerprint()
        {
            uint ret = NBioAPI.Error.NONE;
            NBioAPI.Type.HFIR CaptureFIR;

            StillCapturing = true;

            BSP_Init();
            if (!m_bBspInit)
                return;

            ret = m_NBioAPI.Capture(out CaptureFIR, NBioAPI.Type.TIMEOUT.DEFAULT, null);
            if (ret == NBioAPI.Error.NONE)
            {
                NBioAPI.Export.EXPORT_DATA exportdata;

                //exportdata.FingerData[0].Template[0].Data

                ret = m_Export.NBioBSPToFDx(CaptureFIR, out exportdata, NBioAPI.Type.MINCONV_DATA_TYPE.MINCONV_TYPE_FDU);

                //NBioAPI.Type.MINCONV_DATA_TYPE.

                if (ret != NBioAPI.Error.NONE)
                {
                    MessageBox.Show("Export Error!!!");
                    Data = "";
                    return;
                }
                Data = Convert.ToBase64String(exportdata.FingerData[0].Template[0].Data);

            }
            Close();
            StillCapturing = false;
        }

        private void Register()
        {
            uint ret = NBioAPI.Error.NONE;
            NBioAPI.Type.HFIR NewFIR;

            StillCapturing = true;
            NBioAPI.Type.HFIR haudit_fir = new NBioAPI.Type.HFIR();

            BSP_Init();
            if (!m_bBspInit)
                return;

            ret = m_NBioAPI.Enroll(null, out NewFIR, null, NBioAPI.Type.TIMEOUT.DEFAULT, haudit_fir, null);
            if (ret == NBioAPI.Error.NONE)
            {
                NBioAPI.Export.EXPORT_DATA exportdata;
                NBioAPI.Export.EXPORT_AUDIT_DATA auditdata;
                ret = m_Export.NBioBSPToFDx(NewFIR, out exportdata, NBioAPI.Type.MINCONV_DATA_TYPE.MINCONV_TYPE_FDU);
                ret |= m_Export.NBioBSPToImage(haudit_fir, out auditdata);

                Data = "";
                if (ret != NBioAPI.Error.NONE)
                {
                    MessageBox.Show("Export Error!!!");
                    return;
                }

                if (auditdata.AuditData != null)
                {
                    Data = System.Text.Encoding.Default.GetString(auditdata.AuditData[0].Image[0].Data);
                }

            }
            Close();
            StillCapturing = false;
        }

        private void NitgenOcxHost_FormClosed(object sender, FormClosedEventArgs e)
        {
            BSP_Dispose();
        }
    }
}
