﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.GenericFingerprintServices;

namespace MISL.Ababil.Agent.NitgenFingerprintServices
{
    public class NitgenFingerprintServices : AbstractFingerprintDevice
    {
        public void register()
        {
            NitgenOcxHost ocxHost = new NitgenOcxHost();
            ocxHost.ShowDialog();
            while (ocxHost.StillCapturing)
            {
                System.Windows.Forms.Application.DoEvents();
            }

            notifyObservers(FingerprintEvents.Captured, ocxHost.Data);
        }
        public override void capture()
        {
            NitgenOcxHost ocxHost = new NitgenOcxHost();
            ocxHost.ShowDialog();
            while (ocxHost.StillCapturing)
            {
                System.Windows.Forms.Application.DoEvents();
            }

            notifyObservers(FingerprintEvents.Captured, ocxHost.Data);
        }
    }
}
