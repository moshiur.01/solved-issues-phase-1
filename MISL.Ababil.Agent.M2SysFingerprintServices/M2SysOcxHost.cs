﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MISL.Ababil.Agent.M2SysFingerprintServices
{
    public partial class M2SysOcxHost : Form
    {
        public enum OperationType
        {
            Capture,
            Register
        }

        public string CaptureData;
        public List<string> RegisterData;
        public bool stillCapturing;
        private OperationType _Type;

        public M2SysOcxHost(OperationType type)
        {
            InitializeComponent();
            this._Type = type;
        }

        private void M2SysOcxHost_Load(object sender, EventArgs e)
        {
            stillCapturing = true;
            if (_Type == OperationType.Capture)
            {
                axBioPlugInActX1.CaptureFingerData();
            }
            else
            {
                axBioPlugInActX1.CaptureRegisterSingleFingerData();
            }
        }

        private void axBioPlugInActX1_OnCapture(object sender, EventArgs e)
        {
            AxBIOPLUGINACTXLib.AxBioPlugInActX x = (AxBIOPLUGINACTXLib.AxBioPlugInActX)sender;

            if (x.result == "0")
            {
                string safeFingerData = axBioPlugInActX1.GetSafeLeftFingerData();
                axBioPlugInActX1.GetLeftEnrollData();
                if (!string.IsNullOrEmpty(safeFingerData))
                {
                    if(_Type == OperationType.Capture)
                    {
                        CaptureData = safeFingerData;
                    }
                    else
                    {
                        RegisterData = new List<string>();
                        RegisterData.Add(safeFingerData);
                    }
                }
                else
                {
                    if (_Type == OperationType.Capture)
                    {
                        CaptureData = "";
                    }
                    else
                    {
                        RegisterData = new List<string>();
                    }
                    MessageBox.Show("Subagent finger print needed.");
                }
                this.Close();
                stillCapturing = false;
            }
        }
    }
}
