﻿using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.UI.forms
{
    partial class frmConsumerApplication
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConsumerApplication));
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnPicUpload = new System.Windows.Forms.Button();
            this.btnWebcam = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label4 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.gvIndividualsList = new System.Windows.Forms.DataGridView();
            this.label9 = new System.Windows.Forms.Label();
            this.btnAddIndividual = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblCustomerId = new System.Windows.Forms.Label();
            this.btnSearchCustomer = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.tabControl1 = new MISL.Ababil.Agent.CustomControls.CustomTabControl();
            this.metroTabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtConsumeName = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtMobile = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtDocID = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.cmbDocType = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDown();
            this.chkSendToBranch = new MetroFramework.Controls.MetroCheckBox();
            this.cmbProduct = new MetroFramework.Controls.MetroComboBox();
            this.cmbAccType = new MetroFramework.Controls.MetroComboBox();
            this.txtOpeningDeposit = new MetroFramework.Controls.MetroTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.pbConsumerPic = new System.Windows.Forms.PictureBox();
            this.metroTabPage2 = new System.Windows.Forms.TabPage();
            this.txtAccOperated = new MetroFramework.Controls.MetroTextBox();
            this.cmbOwnerType = new MetroFramework.Controls.MetroComboBox();
            this.cmbCustomerType = new MetroFramework.Controls.MetroComboBox();
            this.tabCtrlCustomer = new MISL.Ababil.Agent.CustomControls.CustomTabControl();
            this.tabPageNewCustomer = new System.Windows.Forms.TabPage();
            this.txtIndiFirstName = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtIndiLastName = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.cbxUseAsCustomerName = new System.Windows.Forms.CheckBox();
            this.tabPageExistingCustomer = new System.Windows.Forms.TabPage();
            this.lblCustomerName = new System.Windows.Forms.Label();
            this.txtCustomerId = new MetroFramework.Controls.MetroTextBox();
            this.txtCustomerTitle = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.titleLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.gvIndividualsList)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.metroTabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbConsumerPic)).BeginInit();
            this.metroTabPage2.SuspendLayout();
            this.tabCtrlCustomer.SuspendLayout();
            this.tabPageNewCustomer.SuspendLayout();
            this.tabPageExistingCustomer.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(25, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Customer Name :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(55, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Mobile No :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(538, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Image";
            // 
            // btnPicUpload
            // 
            this.btnPicUpload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnPicUpload.FlatAppearance.BorderSize = 0;
            this.btnPicUpload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPicUpload.ForeColor = System.Drawing.Color.White;
            this.btnPicUpload.Location = new System.Drawing.Point(541, 249);
            this.btnPicUpload.Name = "btnPicUpload";
            this.btnPicUpload.Size = new System.Drawing.Size(92, 27);
            this.btnPicUpload.TabIndex = 2;
            this.btnPicUpload.Text = "Browse";
            this.btnPicUpload.UseVisualStyleBackColor = false;
            this.btnPicUpload.Click += new System.EventHandler(this.btnPicUpload_Click);
            // 
            // btnWebcam
            // 
            this.btnWebcam.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnWebcam.FlatAppearance.BorderSize = 0;
            this.btnWebcam.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnWebcam.ForeColor = System.Drawing.Color.White;
            this.btnWebcam.Location = new System.Drawing.Point(645, 249);
            this.btnWebcam.Name = "btnWebcam";
            this.btnWebcam.Size = new System.Drawing.Size(92, 27);
            this.btnWebcam.TabIndex = 3;
            this.btnWebcam.Text = "WebCam";
            this.btnWebcam.UseVisualStyleBackColor = false;
            this.btnWebcam.Click += new System.EventHandler(this.btnWebcam_Click);
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnApply.FlatAppearance.BorderSize = 0;
            this.btnApply.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApply.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApply.ForeColor = System.Drawing.Color.White;
            this.btnApply.Location = new System.Drawing.Point(513, 491);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(95, 27);
            this.btnApply.TabIndex = 2;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = false;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(618, 491);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(95, 27);
            this.btnClear.TabIndex = 3;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(30, 296);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Send to branch :";
            this.label4.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(24, 252);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(92, 13);
            this.label15.TabIndex = 14;
            this.label15.Text = "Opening Deposit :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(66, 209);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(50, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Product :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(36, 173);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Account Type :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(11, 352);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 13);
            this.label14.TabIndex = 8;
            this.label14.Text = "No of Operators :";
            // 
            // gvIndividualsList
            // 
            this.gvIndividualsList.AllowUserToAddRows = false;
            this.gvIndividualsList.BackgroundColor = System.Drawing.Color.LightGray;
            this.gvIndividualsList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gvIndividualsList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvIndividualsList.Location = new System.Drawing.Point(13, 191);
            this.gvIndividualsList.Name = "gvIndividualsList";
            this.gvIndividualsList.Size = new System.Drawing.Size(771, 152);
            this.gvIndividualsList.TabIndex = 7;
            this.gvIndividualsList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvIndividualsList_CellContentClick);
            this.gvIndividualsList.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvIndividualsList_CellValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(119, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "First Name";
            // 
            // btnAddIndividual
            // 
            this.btnAddIndividual.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnAddIndividual.FlatAppearance.BorderSize = 0;
            this.btnAddIndividual.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddIndividual.ForeColor = System.Drawing.Color.White;
            this.btnAddIndividual.Location = new System.Drawing.Point(616, 36);
            this.btnAddIndividual.Name = "btnAddIndividual";
            this.btnAddIndividual.Size = new System.Drawing.Size(125, 26);
            this.btnAddIndividual.TabIndex = 5;
            this.btnAddIndividual.Text = "Add Individual";
            this.btnAddIndividual.UseVisualStyleBackColor = false;
            this.btnAddIndividual.Click += new System.EventHandler(this.btnAddIndividual_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(27, 41);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Individual Name :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(365, 19);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Last Name";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(14, 46);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "Customer Name :";
            // 
            // lblCustomerId
            // 
            this.lblCustomerId.AutoSize = true;
            this.lblCustomerId.BackColor = System.Drawing.Color.White;
            this.lblCustomerId.Location = new System.Drawing.Point(31, 18);
            this.lblCustomerId.Name = "lblCustomerId";
            this.lblCustomerId.Size = new System.Drawing.Size(71, 13);
            this.lblCustomerId.TabIndex = 0;
            this.lblCustomerId.Text = "Customer ID :";
            // 
            // btnSearchCustomer
            // 
            this.btnSearchCustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnSearchCustomer.FlatAppearance.BorderSize = 0;
            this.btnSearchCustomer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearchCustomer.ForeColor = System.Drawing.Color.White;
            this.btnSearchCustomer.Location = new System.Drawing.Point(188, 13);
            this.btnSearchCustomer.Name = "btnSearchCustomer";
            this.btnSearchCustomer.Size = new System.Drawing.Size(60, 24);
            this.btnSearchCustomer.TabIndex = 2;
            this.btnSearchCustomer.Text = "Search";
            this.btnSearchCustomer.UseVisualStyleBackColor = false;
            this.btnSearchCustomer.Click += new System.EventHandler(this.btnSearchCustomer_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(408, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Owner Type :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(25, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Customer Type :";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(723, 491);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(95, 27);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl1.Controls.Add(this.metroTabPage1);
            this.tabControl1.Controls.Add(this.metroTabPage2);
            this.tabControl1.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabControl1.Location = new System.Drawing.Point(17, 50);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(795, 434);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 0;
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.BackColor = System.Drawing.Color.White;
            this.metroTabPage1.Controls.Add(this.panel1);
            this.metroTabPage1.Controls.Add(this.label6);
            this.metroTabPage1.Controls.Add(this.btnWebcam);
            this.metroTabPage1.Controls.Add(this.pbConsumerPic);
            this.metroTabPage1.Controls.Add(this.btnPicUpload);
            this.metroTabPage1.Location = new System.Drawing.Point(4, 25);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(787, 405);
            this.metroTabPage1.TabIndex = 0;
            this.metroTabPage1.Text = "General";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.cmbDocType);
            this.panel1.Controls.Add(this.txtDocID);
            this.panel1.Controls.Add(this.txtMobile);
            this.panel1.Controls.Add(this.txtConsumeName);
            this.panel1.Controls.Add(this.chkSendToBranch);
            this.panel1.Controls.Add(this.cmbProduct);
            this.panel1.Controls.Add(this.cmbAccType);
            this.panel1.Controls.Add(this.txtOpeningDeposit);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(14, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(514, 385);
            this.panel1.TabIndex = 1;
            // 
            // txtConsumeName
            // 
            this.txtConsumeName.BackColor = System.Drawing.Color.White;
            this.txtConsumeName.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtConsumeName.InputScopeAllowEmpty = false;
            this.txtConsumeName.InputScopeCustomString = null;
            this.txtConsumeName.IsValid = null;
            this.txtConsumeName.Location = new System.Drawing.Point(128, 23);
            this.txtConsumeName.MaxLength = 150;
            this.txtConsumeName.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtConsumeName.Name = "txtConsumeName";
            this.txtConsumeName.PromptText = "";
            this.txtConsumeName.ReadOnly = false;
            this.txtConsumeName.ShowMandatoryMark = false;
            this.txtConsumeName.Size = new System.Drawing.Size(294, 21);
            this.txtConsumeName.TabIndex = 3;
            this.txtConsumeName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtConsumeName.UpperCaseOnly = true;
            this.txtConsumeName.ValidationErrorMessage = "Input required !";
            // 
            // txtMobile
            // 
            this.txtMobile.BackColor = System.Drawing.Color.White;
            this.txtMobile.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.MobileNumber;
            this.txtMobile.InputScopeAllowEmpty = false;
            this.txtMobile.InputScopeCustomString = null;
            this.txtMobile.IsValid = null;
            this.txtMobile.Location = new System.Drawing.Point(128, 133);
            this.txtMobile.MaxLength = 11;
            this.txtMobile.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtMobile.Name = "txtMobile";
            this.txtMobile.PromptText = "";
            this.txtMobile.ReadOnly = false;
            this.txtMobile.ShowMandatoryMark = false;
            this.txtMobile.Size = new System.Drawing.Size(294, 21);
            this.txtMobile.TabIndex = 9;
            this.txtMobile.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtMobile.UpperCaseOnly = false;
            this.txtMobile.ValidationErrorMessage = "Invalid mobile no. !";
            // 
            // txtDocID
            // 
            this.txtDocID.BackColor = System.Drawing.Color.White;
            this.txtDocID.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtDocID.InputScopeAllowEmpty = false;
            this.txtDocID.InputScopeCustomString = null;
            this.txtDocID.IsValid = null;
            this.txtDocID.Location = new System.Drawing.Point(128, 96);
            this.txtDocID.MaxLength = 32;
            this.txtDocID.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtDocID.Name = "txtDocID";
            this.txtDocID.PromptText = "";
            this.txtDocID.ReadOnly = false;
            this.txtDocID.ShowMandatoryMark = false;
            this.txtDocID.Size = new System.Drawing.Size(294, 21);
            this.txtDocID.TabIndex = 7;
            this.txtDocID.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtDocID.UpperCaseOnly = false;
            this.txtDocID.ValidationErrorMessage = "Invalid national ID !";
            // 
            // cmbDocType
            // 
            this.cmbDocType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbDocType.FormattingEnabled = true;
            this.cmbDocType.InputScopeAllowEmpty = false;
            this.cmbDocType.IsValid = null;
            this.cmbDocType.Items.AddRange(new object[] {
            "National ID",
            "Passport",
            "Birth Certificate",
            "Driving License",
            "TIN"});
            this.cmbDocType.Location = new System.Drawing.Point(128, 59);
            this.cmbDocType.Name = "cmbDocType";
            this.cmbDocType.PromptText = "(Select)";
            this.cmbDocType.ReadOnly = false;
            this.cmbDocType.ShowMandatoryMark = false;
            this.cmbDocType.Size = new System.Drawing.Size(294, 21);
            this.cmbDocType.TabIndex = 5;
            this.cmbDocType.ValidationErrorMessage = "Validation Error!";
            // 
            // chkSendToBranch
            // 
            this.chkSendToBranch.AutoSize = true;
            this.chkSendToBranch.Location = new System.Drawing.Point(128, 295);
            this.chkSendToBranch.Name = "chkSendToBranch";
            this.chkSendToBranch.Size = new System.Drawing.Size(26, 15);
            this.chkSendToBranch.TabIndex = 17;
            this.chkSendToBranch.Text = " ";
            this.chkSendToBranch.UseSelectable = true;
            this.chkSendToBranch.Visible = false;
            // 
            // cmbProduct
            // 
            this.cmbProduct.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cmbProduct.FormattingEnabled = true;
            this.cmbProduct.ItemHeight = 19;
            this.cmbProduct.Location = new System.Drawing.Point(126, 204);
            this.cmbProduct.Name = "cmbProduct";
            this.cmbProduct.Size = new System.Drawing.Size(298, 25);
            this.cmbProduct.TabIndex = 13;
            this.cmbProduct.UseSelectable = true;
            this.cmbProduct.SelectedIndexChanged += new System.EventHandler(this.cmbProduct_SelectedIndexChanged);
            // 
            // cmbAccType
            // 
            this.cmbAccType.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cmbAccType.FormattingEnabled = true;
            this.cmbAccType.ItemHeight = 19;
            this.cmbAccType.Location = new System.Drawing.Point(126, 168);
            this.cmbAccType.Name = "cmbAccType";
            this.cmbAccType.Size = new System.Drawing.Size(298, 25);
            this.cmbAccType.TabIndex = 11;
            this.cmbAccType.UseSelectable = true;
            // 
            // txtOpeningDeposit
            // 
            this.txtOpeningDeposit.Lines = new string[] {
        "10.00"};
            this.txtOpeningDeposit.Location = new System.Drawing.Point(128, 249);
            this.txtOpeningDeposit.MaxLength = 6;
            this.txtOpeningDeposit.Name = "txtOpeningDeposit";
            this.txtOpeningDeposit.PasswordChar = '\0';
            this.txtOpeningDeposit.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtOpeningDeposit.SelectedText = "";
            this.txtOpeningDeposit.Size = new System.Drawing.Size(122, 22);
            this.txtOpeningDeposit.TabIndex = 15;
            this.txtOpeningDeposit.Text = "10.00";
            this.txtOpeningDeposit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtOpeningDeposit.UseSelectable = true;
            this.txtOpeningDeposit.Enter += new System.EventHandler(this.txtOpeningDeposit_Enter);
            this.txtOpeningDeposit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNationalId_KeyPress);
            this.txtOpeningDeposit.Leave += new System.EventHandler(this.txtOpeningDeposit_Leave);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(40, 99);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(76, 13);
            this.label17.TabIndex = 6;
            this.label17.Text = "Document ID :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(27, 62);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(89, 13);
            this.label18.TabIndex = 4;
            this.label18.Text = "Document Type :";
            // 
            // pbConsumerPic
            // 
            this.pbConsumerPic.BackColor = System.Drawing.Color.White;
            this.pbConsumerPic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbConsumerPic.Location = new System.Drawing.Point(541, 30);
            this.pbConsumerPic.Name = "pbConsumerPic";
            this.pbConsumerPic.Size = new System.Drawing.Size(196, 213);
            this.pbConsumerPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbConsumerPic.TabIndex = 5;
            this.pbConsumerPic.TabStop = false;
            // 
            // metroTabPage2
            // 
            this.metroTabPage2.BackColor = System.Drawing.Color.White;
            this.metroTabPage2.Controls.Add(this.txtAccOperated);
            this.metroTabPage2.Controls.Add(this.cmbOwnerType);
            this.metroTabPage2.Controls.Add(this.cmbCustomerType);
            this.metroTabPage2.Controls.Add(this.tabCtrlCustomer);
            this.metroTabPage2.Controls.Add(this.label14);
            this.metroTabPage2.Controls.Add(this.gvIndividualsList);
            this.metroTabPage2.Controls.Add(this.label7);
            this.metroTabPage2.Controls.Add(this.label8);
            this.metroTabPage2.Location = new System.Drawing.Point(4, 25);
            this.metroTabPage2.Name = "metroTabPage2";
            this.metroTabPage2.Size = new System.Drawing.Size(787, 405);
            this.metroTabPage2.TabIndex = 1;
            this.metroTabPage2.Text = "Customer";
            // 
            // txtAccOperated
            // 
            this.txtAccOperated.Lines = new string[] {
        "0"};
            this.txtAccOperated.Location = new System.Drawing.Point(105, 349);
            this.txtAccOperated.MaxLength = 2;
            this.txtAccOperated.Name = "txtAccOperated";
            this.txtAccOperated.PasswordChar = '\0';
            this.txtAccOperated.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAccOperated.SelectedText = "";
            this.txtAccOperated.Size = new System.Drawing.Size(55, 22);
            this.txtAccOperated.TabIndex = 9;
            this.txtAccOperated.Text = "0";
            this.txtAccOperated.UseSelectable = true;
            this.txtAccOperated.Leave += new System.EventHandler(this.txtAccOperated_Leave);
            // 
            // cmbOwnerType
            // 
            this.cmbOwnerType.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cmbOwnerType.FormattingEnabled = true;
            this.cmbOwnerType.ItemHeight = 19;
            this.cmbOwnerType.Location = new System.Drawing.Point(485, 15);
            this.cmbOwnerType.Name = "cmbOwnerType";
            this.cmbOwnerType.Size = new System.Drawing.Size(261, 25);
            this.cmbOwnerType.TabIndex = 4;
            this.cmbOwnerType.UseSelectable = true;
            // 
            // cmbCustomerType
            // 
            this.cmbCustomerType.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cmbCustomerType.FormattingEnabled = true;
            this.cmbCustomerType.ItemHeight = 19;
            this.cmbCustomerType.Location = new System.Drawing.Point(114, 15);
            this.cmbCustomerType.Name = "cmbCustomerType";
            this.cmbCustomerType.Size = new System.Drawing.Size(261, 25);
            this.cmbCustomerType.TabIndex = 1;
            this.cmbCustomerType.UseSelectable = true;
            this.cmbCustomerType.SelectedIndexChanged += new System.EventHandler(this.cmbCustomerType_SelectedIndexChanged);
            // 
            // tabCtrlCustomer
            // 
            this.tabCtrlCustomer.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabCtrlCustomer.Controls.Add(this.tabPageNewCustomer);
            this.tabCtrlCustomer.Controls.Add(this.tabPageExistingCustomer);
            this.tabCtrlCustomer.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabCtrlCustomer.Location = new System.Drawing.Point(10, 54);
            this.tabCtrlCustomer.Name = "tabCtrlCustomer";
            this.tabCtrlCustomer.SelectedIndex = 0;
            this.tabCtrlCustomer.Size = new System.Drawing.Size(781, 127);
            this.tabCtrlCustomer.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabCtrlCustomer.TabIndex = 6;
            this.tabCtrlCustomer.TabStop = false;
            // 
            // tabPageNewCustomer
            // 
            this.tabPageNewCustomer.BackColor = System.Drawing.Color.White;
            this.tabPageNewCustomer.Controls.Add(this.txtIndiFirstName);
            this.tabPageNewCustomer.Controls.Add(this.txtIndiLastName);
            this.tabPageNewCustomer.Controls.Add(this.cbxUseAsCustomerName);
            this.tabPageNewCustomer.Controls.Add(this.label9);
            this.tabPageNewCustomer.Controls.Add(this.btnAddIndividual);
            this.tabPageNewCustomer.Controls.Add(this.label11);
            this.tabPageNewCustomer.Controls.Add(this.label10);
            this.tabPageNewCustomer.Location = new System.Drawing.Point(4, 28);
            this.tabPageNewCustomer.Name = "tabPageNewCustomer";
            this.tabPageNewCustomer.Size = new System.Drawing.Size(773, 95);
            this.tabPageNewCustomer.TabIndex = 0;
            this.tabPageNewCustomer.Text = "New Customer";
            // 
            // txtIndiFirstName
            // 
            this.txtIndiFirstName.BackColor = System.Drawing.Color.White;
            this.txtIndiFirstName.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtIndiFirstName.InputScopeAllowEmpty = false;
            this.txtIndiFirstName.InputScopeCustomString = null;
            this.txtIndiFirstName.IsValid = null;
            this.txtIndiFirstName.Location = new System.Drawing.Point(120, 38);
            this.txtIndiFirstName.MaxLength = 15;
            this.txtIndiFirstName.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtIndiFirstName.Name = "txtIndiFirstName";
            this.txtIndiFirstName.PromptText = "(Type here...)";
            this.txtIndiFirstName.ReadOnly = false;
            this.txtIndiFirstName.ShowMandatoryMark = false;
            this.txtIndiFirstName.Size = new System.Drawing.Size(241, 21);
            this.txtIndiFirstName.TabIndex = 2;
            this.txtIndiFirstName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtIndiFirstName.UpperCaseOnly = true;
            this.txtIndiFirstName.ValidationErrorMessage = "Validation Error!";
            this.txtIndiFirstName.Load += new System.EventHandler(this.txtIndiFirstName_Load);
            this.txtIndiFirstName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIndiFirstName_KeyPress);
            // 
            // txtIndiLastName
            // 
            this.txtIndiLastName.BackColor = System.Drawing.Color.White;
            this.txtIndiLastName.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtIndiLastName.InputScopeAllowEmpty = false;
            this.txtIndiLastName.InputScopeCustomString = null;
            this.txtIndiLastName.IsValid = null;
            this.txtIndiLastName.Location = new System.Drawing.Point(368, 38);
            this.txtIndiLastName.MaxLength = 100;
            this.txtIndiLastName.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtIndiLastName.Name = "txtIndiLastName";
            this.txtIndiLastName.PromptText = "(Type here...)";
            this.txtIndiLastName.ReadOnly = false;
            this.txtIndiLastName.ShowMandatoryMark = false;
            this.txtIndiLastName.Size = new System.Drawing.Size(241, 21);
            this.txtIndiLastName.TabIndex = 4;
            this.txtIndiLastName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtIndiLastName.UpperCaseOnly = true;
            this.txtIndiLastName.ValidationErrorMessage = "Validation Error!";
            // 
            // cbxUseAsCustomerName
            // 
            this.cbxUseAsCustomerName.AutoSize = true;
            this.cbxUseAsCustomerName.BackColor = System.Drawing.Color.White;
            this.cbxUseAsCustomerName.Location = new System.Drawing.Point(119, 69);
            this.cbxUseAsCustomerName.Name = "cbxUseAsCustomerName";
            this.cbxUseAsCustomerName.Size = new System.Drawing.Size(138, 17);
            this.cbxUseAsCustomerName.TabIndex = 101;
            this.cbxUseAsCustomerName.Text = "Use As Customer Name";
            this.cbxUseAsCustomerName.UseVisualStyleBackColor = false;
            // 
            // tabPageExistingCustomer
            // 
            this.tabPageExistingCustomer.BackColor = System.Drawing.Color.White;
            this.tabPageExistingCustomer.Controls.Add(this.lblCustomerName);
            this.tabPageExistingCustomer.Controls.Add(this.txtCustomerId);
            this.tabPageExistingCustomer.Controls.Add(this.label12);
            this.tabPageExistingCustomer.Controls.Add(this.lblCustomerId);
            this.tabPageExistingCustomer.Controls.Add(this.btnSearchCustomer);
            this.tabPageExistingCustomer.Location = new System.Drawing.Point(4, 28);
            this.tabPageExistingCustomer.Name = "tabPageExistingCustomer";
            this.tabPageExistingCustomer.Size = new System.Drawing.Size(773, 95);
            this.tabPageExistingCustomer.TabIndex = 1;
            this.tabPageExistingCustomer.Text = "Existing Customer";
            // 
            // lblCustomerName
            // 
            this.lblCustomerName.AutoSize = true;
            this.lblCustomerName.BackColor = System.Drawing.Color.White;
            this.lblCustomerName.Location = new System.Drawing.Point(105, 46);
            this.lblCustomerName.Name = "lblCustomerName";
            this.lblCustomerName.Size = new System.Drawing.Size(139, 13);
            this.lblCustomerName.TabIndex = 4;
            this.lblCustomerName.Text = "                                            ";
            // 
            // txtCustomerId
            // 
            this.txtCustomerId.Lines = new string[0];
            this.txtCustomerId.Location = new System.Drawing.Point(108, 14);
            this.txtCustomerId.MaxLength = 32767;
            this.txtCustomerId.Name = "txtCustomerId";
            this.txtCustomerId.PasswordChar = '\0';
            this.txtCustomerId.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCustomerId.SelectedText = "";
            this.txtCustomerId.Size = new System.Drawing.Size(74, 22);
            this.txtCustomerId.TabIndex = 1;
            this.txtCustomerId.UseSelectable = true;
            // 
            // txtCustomerTitle
            // 
            this.txtCustomerTitle.BackColor = System.Drawing.Color.White;
            this.txtCustomerTitle.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtCustomerTitle.InputScopeAllowEmpty = false;
            this.txtCustomerTitle.InputScopeCustomString = null;
            this.txtCustomerTitle.IsValid = null;
            this.txtCustomerTitle.Location = new System.Drawing.Point(104, 437);
            this.txtCustomerTitle.MaxLength = 32767;
            this.txtCustomerTitle.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtCustomerTitle.Name = "txtCustomerTitle";
            this.txtCustomerTitle.PromptText = "";
            this.txtCustomerTitle.ReadOnly = false;
            this.txtCustomerTitle.ShowMandatoryMark = false;
            this.txtCustomerTitle.Size = new System.Drawing.Size(294, 21);
            this.txtCustomerTitle.TabIndex = 1;
            this.txtCustomerTitle.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtCustomerTitle.UpperCaseOnly = false;
            this.txtCustomerTitle.ValidationErrorMessage = "Input required !";
            this.txtCustomerTitle.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(-11, 410);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Consumer Title :";
            this.label16.Visible = false;
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = this.titleLabel;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(830, 39);
            this.customTitlebar1.TabIndex = 100;
            this.customTitlebar1.TabStop = false;
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.titleLabel.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.ForeColor = System.Drawing.Color.White;
            this.titleLabel.Location = new System.Drawing.Point(6, 6);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(194, 25);
            this.titleLabel.TabIndex = 101;
            this.titleLabel.Text = "Customer Application";
            // 
            // frmConsumerApplication
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(830, 530);
            this.ControlBox = false;
            this.Controls.Add(this.txtCustomerTitle);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.label16);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmConsumerApplication";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Customer Application";
            this.Deactivate += new System.EventHandler(this.frmConsumerApplication_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmConsumerApplication_FormClosing);
            this.Load += new System.EventHandler(this.frmConsumerApplication_Load);
            this.Shown += new System.EventHandler(this.frmConsumerApplication_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.gvIndividualsList)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.metroTabPage1.ResumeLayout(false);
            this.metroTabPage1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbConsumerPic)).EndInit();
            this.metroTabPage2.ResumeLayout(false);
            this.metroTabPage2.PerformLayout();
            this.tabCtrlCustomer.ResumeLayout(false);
            this.tabPageNewCustomer.ResumeLayout(false);
            this.tabPageNewCustomer.PerformLayout();
            this.tabPageExistingCustomer.ResumeLayout(false);
            this.tabPageExistingCustomer.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pbConsumerPic;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnPicUpload;
        private System.Windows.Forms.Button btnWebcam;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblCustomerId;
        private System.Windows.Forms.Button btnSearchCustomer;
        private System.Windows.Forms.DataGridView gvIndividualsList;
        private System.Windows.Forms.Button btnAddIndividual;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label4;
        private MISL.Ababil.Agent.CustomControls.CustomTabControl tabControl1;
        private System.Windows.Forms.TabPage metroTabPage1;
        private System.Windows.Forms.TabPage metroTabPage2;
        private MISL.Ababil.Agent.CustomControls.CustomTabControl tabCtrlCustomer;
        private System.Windows.Forms.TabPage tabPageNewCustomer;
        private System.Windows.Forms.TabPage tabPageExistingCustomer;
        private MetroFramework.Controls.MetroComboBox cmbAccType;
        private MetroFramework.Controls.MetroComboBox cmbProduct;
        private MetroFramework.Controls.MetroTextBox txtOpeningDeposit;
        private MetroFramework.Controls.MetroCheckBox chkSendToBranch;
        private MetroFramework.Controls.MetroComboBox cmbCustomerType;
        private MetroFramework.Controls.MetroComboBox cmbOwnerType;
        private CustomTextBox txtIndiLastName;
        private CustomTextBox txtIndiFirstName;
        private MetroFramework.Controls.MetroTextBox txtAccOperated;
        private MetroFramework.Controls.MetroTextBox txtCustomerId;
        private System.Windows.Forms.Panel panel1;
        private CustomTitlebar customTitlebar1;
        private CustomTextBox txtMobile;
        private CustomTextBox txtConsumeName;
        private System.Windows.Forms.Label lblCustomerName;
        private CustomTextBox txtCustomerTitle;
        private System.Windows.Forms.Label label16;
        private CustomTextBox txtDocID;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox cbxUseAsCustomerName;
        private CustomComboBoxDropDown cmbDocType;
        private System.Windows.Forms.Label titleLabel;
    }
}