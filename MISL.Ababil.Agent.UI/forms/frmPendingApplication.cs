﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.consumer;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Validation;
using MISL.Ababil.Agent.UI.forms.ProgressUI;
using MetroFramework.Forms;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using MISL.Ababil.Agent.Report;

namespace MISL.Ababil.Agent.UI.forms
{
    public partial class frmPendingApplication : CustomForm
    {
        ConsumerServices _objConsumerServices = new ConsumerServices();
        List<ConsumerAppResultDto> _consumerAppResultDto = new List<ConsumerAppResultDto>();
        int _columnLoaded = 0;

        public frmPendingApplication()
        {
            InitializeComponent();
            //ConfigureValidation();
            setSubagent();
            fillSetupData();
            this.MinimizeBox = false;
        }

        private void fillSetupData()
        {
            List<ApplicationStatus> ds = Enum.GetValues(typeof(ApplicationStatus)).Cast<ApplicationStatus>().ToList();
            ds.RemoveAt(ds.Count - 1);
            cmbApplicationStatus.DataSource = ds;

            //if
            //    (
            //        SessionInfo.userBasicInformation.userType == AgentUserType.Branch
            //        ||
            //        SessionInfo.userBasicInformation.userType == AgentUserType.Admin
            //    )
            if
                (
                    SessionInfo.userBasicInformation.userCategory == UserCategory.BranchUser
                    &&
                    SessionInfo.userBasicInformation.bankUserType == BankUserType.BranchUser
                )
            {
                cmbApplicationStatus.SelectedItem = ApplicationStatus.verified;
            }

            //if
            //    (
            //        SessionInfo.userBasicInformation.userType == AgentUserType.FieldOfficer
            //    )
            else if
                (
                    SessionInfo.userBasicInformation.userCategory == UserCategory.BranchUser
                    &&
                    SessionInfo.userBasicInformation.bankUserType == BankUserType.FieldUser
                )
            {
                cmbApplicationStatus.SelectedItem = ApplicationStatus.submitted;
                cmbApplicationStatus.Enabled = false;
            }

            //if
            //    (
            //        SessionInfo.userBasicInformation.userType == AgentUserType.Outlet
            //    )
            else if
                (
                    SessionInfo.userBasicInformation.userCategory == UserCategory.SubAgentUser
                 )
            {
                cmbApplicationStatus.SelectedItem = ApplicationStatus.draft;
            }
        }

        private void setSubagent()
        {
            try
            {
                AgentServices _objAgentServices = new AgentServices();
                AgentInformation agentInformation = new AgentInformation();
                //if (SessionInfo.userBasicInformation.userType == AgentUserType.Outlet)
                if (SessionInfo.userBasicInformation.userCategory == UserCategory.SubAgentUser)
                {
                    agentInformation = _objAgentServices.getAgentInfoById(SessionInfo.userBasicInformation.agent.id.ToString());
                    if (agentInformation != null)
                    {
                        BindingSource bs = new BindingSource();
                        bs.DataSource = agentInformation.subAgents;
                        {
                            try
                            {
                                SubAgentInformation saiSelect = new SubAgentInformation();
                                saiSelect.name = "(Select)";
                                agentInformation.subAgents.Insert(0, saiSelect);
                            }
                            catch //suppressed
                            {

                            }
                        }
                        UtilityServices.fillComboBox(cmbOutlet, bs, "name", "id");
                        if (cmbOutlet.Items.Count > 0)
                        {
                            cmbOutlet.SelectedValue = SessionInfo.userBasicInformation.outlet.id;
                            cmbOutlet.Enabled = false;
                            return;
                        }
                    }
                }
               
                else
                {
                    SubAgentServices subAgentServices = new SubAgentServices();
                    BindingSource bs = new BindingSource();
                    bs.DataSource = subAgentServices.GetAllSubAgents();
                    {
                        try
                        {
                            SubAgentInformation saiSelect = new SubAgentInformation();
                            saiSelect.name = "(Select)";
                            agentInformation.subAgents.Insert(0, saiSelect);
                        }
                        catch //suppressed
                        {

                        }
                    }
                    UtilityServices.fillComboBox(cmbOutlet, bs, "name", "id");
                    if (cmbOutlet.Items.Count > 0)
                    {
                        cmbOutlet.SelectedIndex = -1;
                    }
                }
            }
            catch { }
        }

        //private void ConfigureValidation()
        //{
        //    ValidationManager.ConfigureValidation(this, texReferenceNo, "Reference No", (long)ValidationType.Numeric, false);
        //    ValidationManager.ConfigureValidation(this, txtMobileNo, "Mobile No", (long)ValidationType.BangladeshiCellphoneNumber, false);
        //}

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void frmPendingApplication_Load(object sender, EventArgs e)
        {
            txtMobileNo.MaxLength = CommonRules.mobileNoLength;
        }

        class ConsumerApplicationComparer : IComparer<ConsumerApplication>
        {
            private readonly string _memberName = string.Empty; // the member name to be sorted
            private readonly SortOrder _sortOrder = SortOrder.None;

            public ConsumerApplicationComparer(string memberName, SortOrder sortingOrder)
            {
                _memberName = memberName;
                _sortOrder = sortingOrder;
            }

            public int Compare(ConsumerApplication consumerApplication1, ConsumerApplication consumerApplication2)
            {
                if (_sortOrder != SortOrder.Ascending)
                {
                    var tmp = consumerApplication1;
                    consumerApplication1 = consumerApplication2;
                    consumerApplication2 = tmp;
                }

                switch (_memberName)
                {
                    case "creationDate":
                        return consumerApplication1.creationDate.Value.CompareTo(consumerApplication2.creationDate.Value);
                    default:
                        return consumerApplication1.creationDate.Value.CompareTo(consumerApplication2.creationDate.Value);
                }
            }
        }

        private void loadAllApplications(ConsumerApplicationDto consumerApplicationDto)
        {
            try
            {
                _consumerAppResultDto = ConsumerServices.getConsumerApplications(consumerApplicationDto);
                if (_consumerAppResultDto != null)
                {
                    dataGridView1.DataSource = null;
                    dataGridView1.Columns.Clear();
                    dataGridView1.DataSource = _consumerAppResultDto.Select(o => new ConsumerAppResultGrid(o)
                    {
                        Outlet = o.outlet,
                        User = o.submitUser,
                        CreationDate = (UtilityServices.getDateFromLong(o.appDate)).ToString("dd-MM-yyyy"),
                        ConsumerName = o.consumerName,
                        //~//NationalId = o.nationalId, 
                        MobileNumber = o.mobileNo,
                        ReferenceNumber = o.refNo,
                        ApplicationStatus = o.appStatus,
                        filter =
                            (
                                o.outlet +
                                o.submitUser +
                                UtilityServices.getDateFromLong(o.appDate).ToString("dd-MM-yyyy") +
                                o.submitUser +
                                o.consumerName +
                                o.nationalId +
                                o.mobileNo +
                                o.refNo +
                                o.appStatus.ToString()
                            ).ToLower()

                    }).ToList();



                    //DataGridViewButtonColumn buttonColumn = new DataGridViewButtonColumn();
                    //if (SessionInfo.userBasicInformation.userType == AgentUserType.Outlet)
                    //{
                    //    buttonColumn.Text = "Edit";
                    //}
                    //else
                    //{
                    //    buttonColumn.Text = "View";
                    //}
                    //buttonColumn.UseColumnTextForButtonValue = true;
                    ////buttonColumn.DefaultCellStyle.
                    //dataGridView1.Columns.Add(buttonColumn);

                    DataGridViewImageColumn buttonColumn = new DataGridViewImageColumn();
                    //if (SessionInfo.userBasicInformation.userType == AgentUserType.Outlet)
                    //{
                    //    //buttonColumn.Text = "Edit";
                    //}
                    //else
                    //{
                    //    buttonColumn.Text = "View";
                    //}
                    //buttonColumn.UseColumnTextForButtonValue = true;

                    dataGridView1.Columns["filter"].Visible = false;

                    buttonColumn.Image = imageList1.Images[0];
                    buttonColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                    buttonColumn.Width = 48;
                    dataGridView1.Columns.Insert(dataGridView1.Columns.Count - 1, buttonColumn);

                    DataGridViewLinkColumn linkColumn = new DataGridViewLinkColumn();
                    linkColumn.Text = "Open Slip";
                    linkColumn.UseColumnTextForLinkValue = true;
                    linkColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    linkColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                    linkColumn.Width = 90;
                    dataGridView1.Columns.Insert(dataGridView1.Columns.Count - 1, linkColumn);


                    _columnLoaded = 1;
                    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                    {
                        ConsumerAppResultDto consumerApplicationToEdit = new ConsumerAppResultDto();
                        consumerApplicationToEdit = _consumerAppResultDto[i];
                        if (consumerApplicationToEdit.appStatus == ApplicationStatus.draft_at_branch)
                            dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.OrangeRed;
                    }


                }
                else
                    MsgBox.showConfirmation("No applications available");
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        //private void loadAllApplications(ConsumerApplicationDto consumerApplicationDto)
        //{
        //    try
        //    {
        //        consumerApplications = ConsumerServices.getConsumerApplications(consumerApplicationDto);

        //        //{
        //        //    DataGridViewColumn column = dataGridView1.Columns[1];

        //        //    string columnName = column.Name;

        //        //    SortOrder sortOrder = column.HeaderCell.SortGlyphDirection == SortOrder.Ascending
        //        //                              ? SortOrder.Descending
        //        //                              : SortOrder.Ascending;

        //        //    consumerApplications.Sort(new ConsumerApplicationComparer(columnName, sortOrder));

        //        //    dataGridView1.Refresh();

        //        //    column.HeaderCell.SortGlyphDirection = sortOrder;

        //        //}
        //        if (consumerApplications != null)
        //        {
        //            dataGridView1.DataSource = null;
        //            dataGridView1.Columns.Clear();
        //            //consumerApplications.Sort();
        //            dataGridView1.DataSource = consumerApplications.Select(o => new ConsumerApplicationGrid(o) { CreationDate = (UtilityServices.getDateFromLong(o.creationDate)).ToString("dd-MM-yyyy"), ConsumerName = o.consumerName, NationalId = o.nationalId, MobileNumber = o.mobileNo, ReferenceNumber = o.referenceNumber, ApplicationStatus = o.applicationStatus }).ToList();
        //            //if (columnLoaded == 0)
        //            //{
        //            DataGridViewButtonColumn buttonColumn = new DataGridViewButtonColumn();
        //            buttonColumn.Text = "Edit";
        //            buttonColumn.UseColumnTextForButtonValue = true;
        //            dataGridView1.Columns.Add(buttonColumn);
        //            columnLoaded = 1;
        //            //}
        //            //else
        //            //{
        //            //    dataGridView1.Columns[0].DisplayIndex = 6;
        //            //}
        //            for (int i = 0; i < dataGridView1.Rows.Count; i++)
        //            {
        //                ConsumerApplication consumerApplicationToEdit = new ConsumerApplication();
        //                consumerApplicationToEdit = consumerApplications[i];
        //                //----if (consumerApplicationToEdit.applicationStatus == ApplicationStatus.draft_at_branch && consumerApplicationToEdit.remarks != null)
        //                if (consumerApplicationToEdit.applicationStatus == ApplicationStatus.draft_at_branch)
        //                    dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.OrangeRed;
        //            }
        //        }
        //        else
        //            MessageBox.Show("No applications available");
        //    }
        //    catch (Exception ex)
        //    {
        //        Message.ShowError(ex.Message);
        //    }
        //}

        public class ConsumerAppResultGrid
        {
            public string Outlet { get; set; }
            public string User { get; set; }
            public string CreationDate { get; set; }

            public string ConsumerName { get; set; }
            //~//public string NationalId { get; set; }
            public string MobileNumber { get; set; }
            public string ReferenceNumber { get; set; }
            public ApplicationStatus ApplicationStatus { get; set; }

            private ConsumerAppResultDto _obj;
            public string filter { get; set; }
            public ConsumerAppResultGrid(ConsumerAppResultDto obj)
            {
                _obj = obj;
            }

            public ConsumerAppResultDto GetModel()
            {
                return _obj;
            }
        }
        private bool validationCheck()
        {
            return ValidationManager.ValidateForm(this);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            btnSearch.Enabled = false;
            search();
            btnSearch.Enabled = true;
            customDataGridViewHeader.ExportReportSubtitleThree = "Consumer Application List";
            DataGridHeaderUtility.ReportHederDataExportToPdf(customDataGridViewHeader);
        }

        private void search()
        {
            //if (validationCheck())
            //{
            ConsumerApplicationDto consumerApplicationDto = new ConsumerApplicationDto(); //SessionInfo.rights.Any(p => p == Rights.CREATE_BANK_USER.ToString()

            //consumerApplicationDto.agentUserType = SessionInfo.userBasicInformation.userType;

            if (cmbOutlet.SelectedValue != null)
            {
                consumerApplicationDto.outletId = (long)cmbOutlet.SelectedValue;
            }
            else
            {
                consumerApplicationDto.outletId = null;
            }
            consumerApplicationDto.consumerName = consumerNameTxt.Text;
            consumerApplicationDto.mobileNo = txtMobileNo.Text;
            consumerApplicationDto.nationalId = txtNationalId.Text;
            consumerApplicationDto.referenceNumber = texReferenceNo.Text;
            consumerApplicationDto.fromDate = UtilityServices.GetLongDate(dtpFromDate.Value);
            consumerApplicationDto.toDate = UtilityServices.GetLongDate(dtpToDate.Value);
            consumerApplicationDto.applicationStatus = (ApplicationStatus?)cmbApplicationStatus.SelectedItem;
            consumerApplicationDto.branchId = SessionInfo.userBasicInformation.userBranch;


            //////////if (UserRights.rights.All(s => s.Contains(Rights.DRAFT_CONSUMER_APPLICATION.ToString())) || UserRights.rights.All(s => s.Contains(Rights.SUBMIT_CONSUMER.ToString())))
            //////////if (UserWiseRights.subAgentRights.All(s => SessionInfo.rights.Contains(s)))
            //////////    consumerApplicationDto.applicationStatus = ApplicationStatus.draft;
            ////////////if (UserRights.rights.All(s => s.Contains(Rights.VERIFY_CONSUMER.ToString())))
            //////////if (UserWiseRights.fieldOfficerRights.All(s => SessionInfo.rights.Contains(s)))
            //////////    consumerApplicationDto.applicationStatus = ApplicationStatus.submitted;
            ////////////if (UserRights.rights.All(s => s.Contains(Rights.APPROVE_CONSUMER.ToString())))
            //////////if (UserWiseRights.BranchRights.All(s => SessionInfo.rights.Contains(s)))
            //////////    consumerApplicationDto.applicationStatus = ApplicationStatus.verified;

            //if (UtilityServices.isRightExist(Rights.DRAFT_CONSUMER_APPLICATION))
            //    consumerApplicationDto.applicationStatus = ApplicationStatus.draft;
            //if (UtilityServices.isRightExist(Rights.VERIFY_CONSUMER))
            //    consumerApplicationDto.applicationStatus = ApplicationStatus.submitted;
            //if (UtilityServices.isRightExist(Rights.APPROVE_CONSUMER))
            //    consumerApplicationDto.applicationStatus = ApplicationStatus.verified;

            ProgressUIManager.ShowProgress(this);
            loadAllApplications(consumerApplicationDto);
            ProgressUIManager.CloseProgress();

            lblItemsFound.Text = "   Item(s) Found: " + dataGridView1.Rows.Count.ToString();
            //}                                   
        }


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int selRow = e.RowIndex;
                var senderGrid = (DataGridView)sender;
                if (senderGrid.Rows.Count > 0)
                {
                    senderGrid.Rows[e.RowIndex].Selected = true;
                }

                if (senderGrid.Columns[e.ColumnIndex] is DataGridViewLinkColumn &&
                    e.RowIndex >= 0)
                {

                    ConsumerApplicationDto consumerApplicationDto = new ConsumerApplicationDto();
                    consumerApplicationDto.referenceNumber = _consumerAppResultDto[e.RowIndex].refNo;
                    List<ConsumerApplication> consumerApplicationList = _objConsumerServices.getListOfConsumerApplicationsA(consumerApplicationDto);

                    ConsumerApplication consumerApplicationToView = new ConsumerApplication();
                    consumerApplicationToView = consumerApplicationList[0];
                    frmShowReport objFrmReport = new frmShowReport();

                    if (consumerApplicationToView.applicationStatus == ApplicationStatus.approved)
                    {
                        objFrmReport.PostAccountReport(consumerApplicationToView.referenceNumber);
                    }
                    else if (consumerApplicationToView.applicationStatus == ApplicationStatus.draft)
                    {
                        objFrmReport.PreAccountReport(consumerApplicationToView.referenceNumber);
                    }
                    else
                    {
                        Message.showInformation("Report available only for draft and approved applications.");
                    }

                }

                if (senderGrid.Columns[e.ColumnIndex] is DataGridViewImageColumn &&
                    e.RowIndex >= 0)
                {
                    ConsumerAppResultDto consumerAppResultDtoEdit = new ConsumerAppResultDto();
                    consumerAppResultDtoEdit = _consumerAppResultDto[e.RowIndex];

                    if (consumerAppResultDtoEdit.appStatus == ApplicationStatus.draft_at_branch)
                    {
                        frmBrRemarks objFrmBrRemarks = new frmBrRemarks(consumerAppResultDtoEdit);
                        objFrmBrRemarks.Show();

                    }
                    else
                    {
                        //ConsumerApplication consumerApplicationTmp = _objConsumerServices.getConsumerApplicationById(consumerAppResultDtoEdit.refNo);
                        ConsumerApplicationDto consumerApplicationDto = new ConsumerApplicationDto();
                        consumerApplicationDto.referenceNumber = consumerAppResultDtoEdit.refNo;
                        List<ConsumerApplication> consumerApplicationList = _objConsumerServices.getListOfConsumerApplicationsA(consumerApplicationDto);
                        try
                        {
                            byte[] consumerPhoto = _objConsumerServices.getConumerPhotoByAppId(consumerAppResultDtoEdit.appId);
                            consumerApplicationList[0].photo = consumerPhoto;
                        }
                        catch (Exception ex)
                        {
                            MsgBox.ShowError(ex.Message);
                        }

                        Packet packet = new Packet();
                        packet.DeveloperMode = false;

                        if (SessionInfo.roles.Contains("Sub Agent"))
                        {
                            if (
                                consumerApplicationList[0].applicationStatus != ApplicationStatus.approved
                                &&
                                consumerApplicationList[0].applicationStatus != ApplicationStatus.canceled
                                )
                            {
                                packet.actionType = FormActionType.Edit;
                            }
                            else
                            {
                                packet.actionType = FormActionType.View;
                            }
                        }
                        else
                        {
                            packet.actionType = FormActionType.View;
                        }

                        packet.intentType = IntentType.Request;


                        frmConsumerCreation objFrmConsumerCreation = new frmConsumerCreation(packet, consumerApplicationList[0]);
                        if (objFrmConsumerCreation.ShowDialog() != DialogResult.No)
                        {
                            btnSearch.Enabled = false;
                            btnSearch.Enabled = true;
                        }
                        search();
                        try
                        {
                            dataGridView1.Rows[selRow].Selected = true;
                        }
                        catch { }
                    }
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        private void txtNationalId_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!char.IsNumber(e.KeyChar) && (Keys)e.KeyChar != Keys.Back && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            base.OnKeyPress(e);
        }

        private void txtMobileNo_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!char.IsNumber(e.KeyChar) && (Keys)e.KeyChar != Keys.Back && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            base.OnKeyPress(e);
        }

        private void frmPendingApplication_FormClosing(object sender, FormClosingEventArgs e)
        {
            //ValidationManager.ReleaseValidationData(this);
            this.Owner = null;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            search();
        }

        private void dtpDOB_ValueChanged(object sender, EventArgs e)
        {
            //mtbDOB.Text = dtpDOB.Value.ToString("dd-MM-yyyy");
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            //mtbFromDate.Text = dtpFromDate.Value.ToString("dd-MM-yyyy");
        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            //mtbToDate.Text = dtpToDate.Value.ToString("dd-MM-yyyy");
        }

        private void mtbDOB_KeyUp(object sender, KeyEventArgs e)
        {
            //suppressed to avoid mtb to dtp conversion
            //try
            //{
            //    string[] str = mtbDOB.Text.Split('-');
            //    DateTime d = new DateTime(int.Parse(str[2].Trim()), int.Parse(str[1].Trim()), int.Parse(str[0].Trim()));
            //    dtpDOB.Value = d;
            //}
            //catch (Exception ex) { }
        }

        //private void mtbFromDate_KeyUp(object sender, KeyEventArgs e)
        //{
        //    //suppressed to avoid mtb to dtp conversion
        //    try
        //    {
        //        string[] str = mtbFromDate.Text.Split('-');
        //        DateTime d = new DateTime(int.Parse(str[2].Trim()), int.Parse(str[1].Trim()), int.Parse(str[0].Trim()));
        //        dtpFromDate.Value = d;
        //    }
        //    catch (Exception ex) { }
        //}

        //private void mtbToDate_KeyUp(object sender, KeyEventArgs e)
        //{
        //    //suppressed to avoid mtb to dtp conversion
        //    try
        //    {
        //        string[] str = mtbToDate.Text.Split('-');
        //        DateTime d = new DateTime(int.Parse(str[2].Trim()), int.Parse(str[1].Trim()), int.Parse(str[0].Trim()));
        //        dtpToDate.Value = d;
        //    }
        //    catch (Exception ex) { }
        //}

        private void btnToDateClear_Click(object sender, EventArgs e)
        {
            ///mtbToDate.Clear();
        }

        private void btnFromDateClear_Click(object sender, EventArgs e)
        {
            //mtbFromDate.Clear();
        }

        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void ClearAll()
        {
            //consumerApplications = new List<ConsumerApplication>();
            _consumerAppResultDto = new List<ConsumerAppResultDto>();
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
            //mtbFromDate.Clear();
            //mtbToDate.Clear();

            texReferenceNo.Clear();
            txtNationalId.Clear();
            consumerNameTxt.Clear();
            txtMobileNo.Clear();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ClearAll();
        }

        private void txtNationalId_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            //if (dataGridView1.Rows.Count <= 20)
            //{
            //    if (dataGridView1.Columns[e.ColumnIndex] is DataGridViewImageColumn &&
            //        e.RowIndex >= 0)
            //    {
            //        (dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex]).Value = imageList1.Images[1];

            //        //try
            //        //{
            //        //    for (int i = 0; i < dataGridView1.Rows.Count; i++)
            //        //    {
            //        //        (dataGridView1.Rows[e.RowIndex].Cells[i]).Style.BackColor = Color.RoyalBlue;
            //        //    }
            //        //}
            //        //catch (Exception)
            //        //{


            //        //}
            //    }
            //}
        }

        private void dataGridView1_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            //if (dataGridView1.Rows.Count <= 20)
            //{
            //    if (dataGridView1.Columns[e.ColumnIndex] is DataGridViewImageColumn &&
            //        e.RowIndex >= 0)
            //    {
            //        (dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex]).Value = imageList1.Images[0];

            //        //try
            //        //{
            //        //    for (int i = 0; i < dataGridView1.Rows.Count; i++)
            //        //    {
            //        //        (dataGridView1.Rows[e.RowIndex].Cells[i]).Style.BackColor = Color.White;
            //        //    }

            //        //}
            //        //catch (Exception)
            //        //{


            //        //}
            //    }
            //}
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Focused && e.RowIndex > -1)
            {
                (dataGridView1.Rows[e.RowIndex].Cells[dataGridView1.ColumnCount - 1]).Value = imageList1.Images[1];
            }
        }

        private void dataGridView1_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Focused)
            {
                (dataGridView1.Rows[e.RowIndex].Cells[dataGridView1.ColumnCount - 1]).Value = imageList1.Images[0];
            }
        }

        private void dataGridView1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1_CellClick(sender, e);
        }
    }
}