﻿using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.UI.forms
{
    partial class frmUserCreationWithPersonalInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnClose = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pnlUserInfo = new System.Windows.Forms.Panel();
            this.cmbUserBranch = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.lblUserBranchHeader = new System.Windows.Forms.Label();
            this.cmbUserType = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbUserStatus = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.pnlChangeCredential = new System.Windows.Forms.Panel();
            this.chkResetFingerPrint = new MetroFramework.Controls.MetroCheckBox();
            this.chkResetPassword = new MetroFramework.Controls.MetroCheckBox();
            this.btnResetCredential = new System.Windows.Forms.Button();
            this.lblNarration = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.lblUserType = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnFingerPrint = new System.Windows.Forms.Button();
            this.txtNarration = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtUserName = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pnlAddress = new System.Windows.Forms.Panel();
            this.cmbContactCountryCode = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbContactPostCode = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbContactThana = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbContactDistrict = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbContactDivision = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbPermanentCountryCode = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbPermanentPostalCode = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbPermanentThana = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbPermanentDistrict = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbPermanentDivision = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.label11 = new System.Windows.Forms.Label();
            this.txtPermanentAddress2 = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtPermanentAddress1 = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.mandatoryMark21 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark20 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark19 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.label20 = new System.Windows.Forms.Label();
            this.mandatoryMark18 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark17 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.txtContactAddress2 = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.mandatoryMark16 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark15 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark14 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark13 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.label22 = new System.Windows.Forms.Label();
            this.mandatoryMark12 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark11 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark10 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.txtContactAddress1 = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.chkUseAsPermanent = new MetroFramework.Controls.MetroCheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.dtpBirthDate = new MISL.Ababil.Agent.UI.forms.CustomControls.CustomDateTimePicker();
            this.mandatoryMark9 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark8 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark7 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark6 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.gbUserImage = new System.Windows.Forms.GroupBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.pbImageUser = new System.Windows.Forms.PictureBox();
            this.btnCapture = new System.Windows.Forms.Button();
            this.txtMothersLastName = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.mandatoryMark3 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark2 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.label9 = new System.Windows.Forms.Label();
            this.txtFirstName = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMothersFirstName = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtLastName = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtFathersFirstName = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtFathersLastName = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtMobileNumber = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lblImgExtension = new System.Windows.Forms.Label();
            this.errProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.groupBox1.SuspendLayout();
            this.pnlUserInfo.SuspendLayout();
            this.pnlChangeCredential.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.pnlAddress.SuspendLayout();
            this.gbUserImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImageUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(764, 675);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(79, 25);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.pnlUserInfo);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(23, 40);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(820, 176);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "User Informaiton";
            // 
            // pnlUserInfo
            // 
            this.pnlUserInfo.Controls.Add(this.cmbUserStatus);
            this.pnlUserInfo.Controls.Add(this.cmbUserType);
            this.pnlUserInfo.Controls.Add(this.cmbUserBranch);
            this.pnlUserInfo.Controls.Add(this.lblUserBranchHeader);
            this.pnlUserInfo.Controls.Add(this.pnlChangeCredential);
            this.pnlUserInfo.Controls.Add(this.lblNarration);
            this.pnlUserInfo.Controls.Add(this.label23);
            this.pnlUserInfo.Controls.Add(this.lblUserType);
            this.pnlUserInfo.Controls.Add(this.label4);
            this.pnlUserInfo.Controls.Add(this.btnFingerPrint);
            this.pnlUserInfo.Controls.Add(this.txtNarration);
            this.pnlUserInfo.Controls.Add(this.txtUserName);
            this.pnlUserInfo.Location = new System.Drawing.Point(35, 16);
            this.pnlUserInfo.Name = "pnlUserInfo";
            this.pnlUserInfo.Size = new System.Drawing.Size(758, 153);
            this.pnlUserInfo.TabIndex = 0;
            // 
            // cmbUserBranch
            // 
            this.cmbUserBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUserBranch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbUserBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.cmbUserBranch.FormattingEnabled = true;
            this.cmbUserBranch.InputScopeAllowEmpty = false;
            this.cmbUserBranch.IsValid = null;
            this.cmbUserBranch.Location = new System.Drawing.Point(101, 67);
            this.cmbUserBranch.Name = "cmbUserBranch";
            this.cmbUserBranch.PromptText = "(Select)";
            this.cmbUserBranch.ReadOnly = false;
            this.cmbUserBranch.ShowMandatoryMark = true;
            this.cmbUserBranch.Size = new System.Drawing.Size(238, 21);
            this.cmbUserBranch.TabIndex = 49;
            this.cmbUserBranch.ValidationErrorMessage = "Input required !";
            // 
            // lblUserBranchHeader
            // 
            this.lblUserBranchHeader.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblUserBranchHeader.AutoSize = true;
            this.lblUserBranchHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserBranchHeader.Location = new System.Drawing.Point(13, 69);
            this.lblUserBranchHeader.Name = "lblUserBranchHeader";
            this.lblUserBranchHeader.Size = new System.Drawing.Size(81, 15);
            this.lblUserBranchHeader.TabIndex = 48;
            this.lblUserBranchHeader.Text = "User Branch :";
            // 
            // cmbUserType
            // 
            this.cmbUserType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbUserType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUserType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbUserType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUserType.FormattingEnabled = true;
            this.cmbUserType.InputScopeAllowEmpty = false;
            this.cmbUserType.IntegralHeight = false;
            this.cmbUserType.IsValid = null;
            this.cmbUserType.ItemHeight = 13;
            this.cmbUserType.Location = new System.Drawing.Point(101, 38);
            this.cmbUserType.Name = "cmbUserType";
            this.cmbUserType.PromptText = "(Select)";
            this.cmbUserType.ReadOnly = false;
            this.cmbUserType.ShowMandatoryMark = true;
            this.cmbUserType.Size = new System.Drawing.Size(238, 21);
            this.cmbUserType.TabIndex = 4;
            this.cmbUserType.ValidationErrorMessage = "Validation Error!";
            // 
            // cmbUserStatus
            // 
            this.cmbUserStatus.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbUserStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUserStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbUserStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUserStatus.FormattingEnabled = true;
            this.cmbUserStatus.InputScopeAllowEmpty = false;
            this.cmbUserStatus.IntegralHeight = false;
            this.cmbUserStatus.IsValid = null;
            this.cmbUserStatus.ItemHeight = 13;
            this.cmbUserStatus.Location = new System.Drawing.Point(101, 96);
            this.cmbUserStatus.Name = "cmbUserStatus";
            this.cmbUserStatus.PromptText = "(Select)";
            this.cmbUserStatus.ReadOnly = false;
            this.cmbUserStatus.ShowMandatoryMark = true;
            this.cmbUserStatus.Size = new System.Drawing.Size(238, 21);
            this.cmbUserStatus.TabIndex = 7;
            this.cmbUserStatus.ValidationErrorMessage = "Validation Error!";
            this.cmbUserStatus.SelectedIndexChanged += new System.EventHandler(this.cmbUserStatus_SelectedIndexChanged);
            // 
            // pnlChangeCredential
            // 
            this.pnlChangeCredential.Controls.Add(this.chkResetFingerPrint);
            this.pnlChangeCredential.Controls.Add(this.chkResetPassword);
            this.pnlChangeCredential.Controls.Add(this.btnResetCredential);
            this.pnlChangeCredential.Location = new System.Drawing.Point(466, 48);
            this.pnlChangeCredential.Name = "pnlChangeCredential";
            this.pnlChangeCredential.Size = new System.Drawing.Size(272, 31);
            this.pnlChangeCredential.TabIndex = 12;
            // 
            // chkResetFingerPrint
            // 
            this.chkResetFingerPrint.AutoSize = true;
            this.chkResetFingerPrint.Location = new System.Drawing.Point(17, 8);
            this.chkResetFingerPrint.Name = "chkResetFingerPrint";
            this.chkResetFingerPrint.Size = new System.Drawing.Size(84, 15);
            this.chkResetFingerPrint.TabIndex = 0;
            this.chkResetFingerPrint.Text = "Finger print";
            this.chkResetFingerPrint.UseSelectable = true;
            this.chkResetFingerPrint.CheckedChanged += new System.EventHandler(this.chkResetCredential_CheckedChanged);
            // 
            // chkResetPassword
            // 
            this.chkResetPassword.AutoSize = true;
            this.chkResetPassword.Location = new System.Drawing.Point(111, 8);
            this.chkResetPassword.Name = "chkResetPassword";
            this.chkResetPassword.Size = new System.Drawing.Size(73, 15);
            this.chkResetPassword.TabIndex = 1;
            this.chkResetPassword.Text = "Password";
            this.chkResetPassword.UseSelectable = true;
            this.chkResetPassword.CheckedChanged += new System.EventHandler(this.chkResetCredential_CheckedChanged);
            // 
            // btnResetCredential
            // 
            this.btnResetCredential.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnResetCredential.FlatAppearance.BorderSize = 0;
            this.btnResetCredential.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnResetCredential.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResetCredential.ForeColor = System.Drawing.Color.White;
            this.btnResetCredential.Location = new System.Drawing.Point(199, 4);
            this.btnResetCredential.Name = "btnResetCredential";
            this.btnResetCredential.Size = new System.Drawing.Size(68, 23);
            this.btnResetCredential.TabIndex = 2;
            this.btnResetCredential.Text = "Reset";
            this.btnResetCredential.UseVisualStyleBackColor = false;
            this.btnResetCredential.Click += new System.EventHandler(this.btnResetCredential_Click);
            // 
            // lblNarration
            // 
            this.lblNarration.AutoSize = true;
            this.lblNarration.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNarration.Location = new System.Drawing.Point(30, 128);
            this.lblNarration.Name = "lblNarration";
            this.lblNarration.Size = new System.Drawing.Size(64, 15);
            this.lblNarration.TabIndex = 9;
            this.lblNarration.Text = "Narration :";
            this.lblNarration.Visible = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(19, 12);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(76, 15);
            this.label23.TabIndex = 0;
            this.label23.Text = "User Name :";
            // 
            // lblUserType
            // 
            this.lblUserType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblUserType.AutoSize = true;
            this.lblUserType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserType.Location = new System.Drawing.Point(27, 42);
            this.lblUserType.Name = "lblUserType";
            this.lblUserType.Size = new System.Drawing.Size(68, 15);
            this.lblUserType.TabIndex = 3;
            this.lblUserType.Text = "User Type :";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(18, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "User Status :";
            // 
            // btnFingerPrint
            // 
            this.btnFingerPrint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnFingerPrint.FlatAppearance.BorderSize = 0;
            this.btnFingerPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFingerPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFingerPrint.ForeColor = System.Drawing.Color.White;
            this.btnFingerPrint.Location = new System.Drawing.Point(580, 16);
            this.btnFingerPrint.Name = "btnFingerPrint";
            this.btnFingerPrint.Size = new System.Drawing.Size(153, 23);
            this.btnFingerPrint.TabIndex = 11;
            this.btnFingerPrint.Text = "Capture Fingerprint";
            this.btnFingerPrint.UseVisualStyleBackColor = false;
            this.btnFingerPrint.Click += new System.EventHandler(this.btnFingerPrint_Click);
            // 
            // txtNarration
            // 
            this.txtNarration.BackColor = System.Drawing.SystemColors.Window;
            this.txtNarration.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNarration.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtNarration.InputScopeAllowEmpty = false;
            this.txtNarration.InputScopeCustomString = null;
            this.txtNarration.IsValid = null;
            this.txtNarration.Location = new System.Drawing.Point(101, 125);
            this.txtNarration.MaxLength = 32767;
            this.txtNarration.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtNarration.Name = "txtNarration";
            this.txtNarration.PromptText = "(Type here...)";
            this.txtNarration.ReadOnly = false;
            this.txtNarration.ShowMandatoryMark = false;
            this.txtNarration.Size = new System.Drawing.Size(238, 21);
            this.txtNarration.TabIndex = 10;
            this.txtNarration.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtNarration.UpperCaseOnly = false;
            this.txtNarration.ValidationErrorMessage = "Input required !";
            this.txtNarration.Visible = false;
            // 
            // txtUserName
            // 
            this.txtUserName.BackColor = System.Drawing.SystemColors.Window;
            this.txtUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserName.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtUserName.InputScopeAllowEmpty = false;
            this.txtUserName.InputScopeCustomString = null;
            this.txtUserName.IsValid = null;
            this.txtUserName.Location = new System.Drawing.Point(101, 9);
            this.txtUserName.MaxLength = 32767;
            this.txtUserName.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.PromptText = "(Type here...)";
            this.txtUserName.ReadOnly = false;
            this.txtUserName.ShowMandatoryMark = true;
            this.txtUserName.Size = new System.Drawing.Size(238, 21);
            this.txtUserName.TabIndex = 1;
            this.txtUserName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtUserName.UpperCaseOnly = false;
            this.txtUserName.ValidationErrorMessage = "Input required !";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.pnlAddress);
            this.groupBox2.Controls.Add(this.dtpBirthDate);
            this.groupBox2.Controls.Add(this.mandatoryMark9);
            this.groupBox2.Controls.Add(this.mandatoryMark8);
            this.groupBox2.Controls.Add(this.mandatoryMark7);
            this.groupBox2.Controls.Add(this.mandatoryMark6);
            this.groupBox2.Controls.Add(this.gbUserImage);
            this.groupBox2.Controls.Add(this.txtMothersLastName);
            this.groupBox2.Controls.Add(this.mandatoryMark3);
            this.groupBox2.Controls.Add(this.mandatoryMark2);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtFirstName);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtMothersFirstName);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtLastName);
            this.groupBox2.Controls.Add(this.txtFathersFirstName);
            this.groupBox2.Controls.Add(this.txtFathersLastName);
            this.groupBox2.Controls.Add(this.txtMobileNumber);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(23, 222);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(820, 439);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Personal Information";
            // 
            // pnlAddress
            // 
            this.pnlAddress.Controls.Add(this.cmbPermanentDivision);
            this.pnlAddress.Controls.Add(this.cmbPermanentDistrict);
            this.pnlAddress.Controls.Add(this.cmbPermanentThana);
            this.pnlAddress.Controls.Add(this.cmbPermanentPostalCode);
            this.pnlAddress.Controls.Add(this.cmbPermanentCountryCode);
            this.pnlAddress.Controls.Add(this.cmbContactDivision);
            this.pnlAddress.Controls.Add(this.cmbContactDistrict);
            this.pnlAddress.Controls.Add(this.cmbContactThana);
            this.pnlAddress.Controls.Add(this.cmbContactPostCode);
            this.pnlAddress.Controls.Add(this.cmbContactCountryCode);
            this.pnlAddress.Controls.Add(this.label11);
            this.pnlAddress.Controls.Add(this.txtPermanentAddress2);
            this.pnlAddress.Controls.Add(this.label17);
            this.pnlAddress.Controls.Add(this.txtPermanentAddress1);
            this.pnlAddress.Controls.Add(this.label18);
            this.pnlAddress.Controls.Add(this.label19);
            this.pnlAddress.Controls.Add(this.mandatoryMark21);
            this.pnlAddress.Controls.Add(this.mandatoryMark20);
            this.pnlAddress.Controls.Add(this.mandatoryMark19);
            this.pnlAddress.Controls.Add(this.label20);
            this.pnlAddress.Controls.Add(this.mandatoryMark18);
            this.pnlAddress.Controls.Add(this.mandatoryMark17);
            this.pnlAddress.Controls.Add(this.txtContactAddress2);
            this.pnlAddress.Controls.Add(this.label21);
            this.pnlAddress.Controls.Add(this.mandatoryMark16);
            this.pnlAddress.Controls.Add(this.mandatoryMark15);
            this.pnlAddress.Controls.Add(this.mandatoryMark14);
            this.pnlAddress.Controls.Add(this.mandatoryMark13);
            this.pnlAddress.Controls.Add(this.label22);
            this.pnlAddress.Controls.Add(this.mandatoryMark12);
            this.pnlAddress.Controls.Add(this.mandatoryMark11);
            this.pnlAddress.Controls.Add(this.mandatoryMark10);
            this.pnlAddress.Controls.Add(this.txtContactAddress1);
            this.pnlAddress.Controls.Add(this.chkUseAsPermanent);
            this.pnlAddress.Controls.Add(this.label16);
            this.pnlAddress.Controls.Add(this.label15);
            this.pnlAddress.Controls.Add(this.label14);
            this.pnlAddress.Controls.Add(this.label13);
            this.pnlAddress.Controls.Add(this.label12);
            this.pnlAddress.Location = new System.Drawing.Point(24, 207);
            this.pnlAddress.Name = "pnlAddress";
            this.pnlAddress.Size = new System.Drawing.Size(789, 231);
            this.pnlAddress.TabIndex = 22;
            // 
            // cmbContactCountryCode
            // 
            this.cmbContactCountryCode.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbContactCountryCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbContactCountryCode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbContactCountryCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbContactCountryCode.FormattingEnabled = true;
            this.cmbContactCountryCode.InputScopeAllowEmpty = false;
            this.cmbContactCountryCode.IntegralHeight = false;
            this.cmbContactCountryCode.IsValid = null;
            this.cmbContactCountryCode.ItemHeight = 13;
            this.cmbContactCountryCode.Location = new System.Drawing.Point(112, 200);
            this.cmbContactCountryCode.Name = "cmbContactCountryCode";
            this.cmbContactCountryCode.PromptText = "(Select)";
            this.cmbContactCountryCode.ReadOnly = false;
            this.cmbContactCountryCode.ShowMandatoryMark = false;
            this.cmbContactCountryCode.Size = new System.Drawing.Size(237, 21);
            this.cmbContactCountryCode.TabIndex = 16;
            this.cmbContactCountryCode.ValidationErrorMessage = "Input required !";
            // 
            // cmbContactPostCode
            // 
            this.cmbContactPostCode.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbContactPostCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbContactPostCode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbContactPostCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbContactPostCode.FormattingEnabled = true;
            this.cmbContactPostCode.InputScopeAllowEmpty = false;
            this.cmbContactPostCode.IntegralHeight = false;
            this.cmbContactPostCode.IsValid = null;
            this.cmbContactPostCode.ItemHeight = 13;
            this.cmbContactPostCode.Location = new System.Drawing.Point(112, 171);
            this.cmbContactPostCode.Name = "cmbContactPostCode";
            this.cmbContactPostCode.PromptText = "(Select)";
            this.cmbContactPostCode.ReadOnly = false;
            this.cmbContactPostCode.ShowMandatoryMark = false;
            this.cmbContactPostCode.Size = new System.Drawing.Size(237, 21);
            this.cmbContactPostCode.TabIndex = 13;
            this.cmbContactPostCode.ValidationErrorMessage = "Input required !";
            // 
            // cmbContactThana
            // 
            this.cmbContactThana.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbContactThana.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbContactThana.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbContactThana.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbContactThana.FormattingEnabled = true;
            this.cmbContactThana.InputScopeAllowEmpty = false;
            this.cmbContactThana.IntegralHeight = false;
            this.cmbContactThana.IsValid = null;
            this.cmbContactThana.ItemHeight = 13;
            this.cmbContactThana.Location = new System.Drawing.Point(112, 142);
            this.cmbContactThana.Name = "cmbContactThana";
            this.cmbContactThana.PromptText = "(Select)";
            this.cmbContactThana.ReadOnly = false;
            this.cmbContactThana.ShowMandatoryMark = false;
            this.cmbContactThana.Size = new System.Drawing.Size(237, 21);
            this.cmbContactThana.TabIndex = 10;
            this.cmbContactThana.ValidationErrorMessage = "Input required !";
            // 
            // cmbContactDistrict
            // 
            this.cmbContactDistrict.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbContactDistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbContactDistrict.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbContactDistrict.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbContactDistrict.FormattingEnabled = true;
            this.cmbContactDistrict.InputScopeAllowEmpty = false;
            this.cmbContactDistrict.IntegralHeight = false;
            this.cmbContactDistrict.IsValid = null;
            this.cmbContactDistrict.ItemHeight = 13;
            this.cmbContactDistrict.Location = new System.Drawing.Point(112, 113);
            this.cmbContactDistrict.Name = "cmbContactDistrict";
            this.cmbContactDistrict.PromptText = "(Select)";
            this.cmbContactDistrict.ReadOnly = false;
            this.cmbContactDistrict.ShowMandatoryMark = false;
            this.cmbContactDistrict.Size = new System.Drawing.Size(237, 21);
            this.cmbContactDistrict.TabIndex = 7;
            this.cmbContactDistrict.ValidationErrorMessage = "Input required !";
            this.cmbContactDistrict.SelectedIndexChanged += new System.EventHandler(this.cmbContactDistrict_SelectedIndexChanged);
            // 
            // cmbContactDivision
            // 
            this.cmbContactDivision.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbContactDivision.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbContactDivision.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbContactDivision.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbContactDivision.FormattingEnabled = true;
            this.cmbContactDivision.InputScopeAllowEmpty = false;
            this.cmbContactDivision.IntegralHeight = false;
            this.cmbContactDivision.IsValid = null;
            this.cmbContactDivision.ItemHeight = 13;
            this.cmbContactDivision.Location = new System.Drawing.Point(112, 84);
            this.cmbContactDivision.Name = "cmbContactDivision";
            this.cmbContactDivision.PromptText = "(Select)";
            this.cmbContactDivision.ReadOnly = false;
            this.cmbContactDivision.ShowMandatoryMark = false;
            this.cmbContactDivision.Size = new System.Drawing.Size(237, 21);
            this.cmbContactDivision.TabIndex = 4;
            this.cmbContactDivision.ValidationErrorMessage = "Input required !";
            this.cmbContactDivision.SelectedIndexChanged += new System.EventHandler(this.cmbContactDivision_SelectedIndexChanged);
            // 
            // cmbPermanentCountryCode
            // 
            this.cmbPermanentCountryCode.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbPermanentCountryCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPermanentCountryCode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbPermanentCountryCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPermanentCountryCode.FormattingEnabled = true;
            this.cmbPermanentCountryCode.InputScopeAllowEmpty = false;
            this.cmbPermanentCountryCode.IntegralHeight = false;
            this.cmbPermanentCountryCode.IsValid = null;
            this.cmbPermanentCountryCode.ItemHeight = 13;
            this.cmbPermanentCountryCode.Location = new System.Drawing.Point(515, 200);
            this.cmbPermanentCountryCode.Name = "cmbPermanentCountryCode";
            this.cmbPermanentCountryCode.PromptText = "(Select)";
            this.cmbPermanentCountryCode.ReadOnly = false;
            this.cmbPermanentCountryCode.ShowMandatoryMark = false;
            this.cmbPermanentCountryCode.Size = new System.Drawing.Size(237, 21);
            this.cmbPermanentCountryCode.TabIndex = 37;
            this.cmbPermanentCountryCode.ValidationErrorMessage = "Input required !";
            // 
            // cmbPermanentPostalCode
            // 
            this.cmbPermanentPostalCode.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbPermanentPostalCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPermanentPostalCode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbPermanentPostalCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPermanentPostalCode.FormattingEnabled = true;
            this.cmbPermanentPostalCode.InputScopeAllowEmpty = false;
            this.cmbPermanentPostalCode.IntegralHeight = false;
            this.cmbPermanentPostalCode.IsValid = null;
            this.cmbPermanentPostalCode.ItemHeight = 13;
            this.cmbPermanentPostalCode.Location = new System.Drawing.Point(515, 171);
            this.cmbPermanentPostalCode.Name = "cmbPermanentPostalCode";
            this.cmbPermanentPostalCode.PromptText = "(Select)";
            this.cmbPermanentPostalCode.ReadOnly = false;
            this.cmbPermanentPostalCode.ShowMandatoryMark = false;
            this.cmbPermanentPostalCode.Size = new System.Drawing.Size(237, 21);
            this.cmbPermanentPostalCode.TabIndex = 34;
            this.cmbPermanentPostalCode.ValidationErrorMessage = "Input required !";
            // 
            // cmbPermanentThana
            // 
            this.cmbPermanentThana.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbPermanentThana.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPermanentThana.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbPermanentThana.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPermanentThana.FormattingEnabled = true;
            this.cmbPermanentThana.InputScopeAllowEmpty = false;
            this.cmbPermanentThana.IntegralHeight = false;
            this.cmbPermanentThana.IsValid = null;
            this.cmbPermanentThana.ItemHeight = 13;
            this.cmbPermanentThana.Location = new System.Drawing.Point(515, 142);
            this.cmbPermanentThana.Name = "cmbPermanentThana";
            this.cmbPermanentThana.PromptText = "(Select)";
            this.cmbPermanentThana.ReadOnly = false;
            this.cmbPermanentThana.ShowMandatoryMark = false;
            this.cmbPermanentThana.Size = new System.Drawing.Size(237, 21);
            this.cmbPermanentThana.TabIndex = 31;
            this.cmbPermanentThana.ValidationErrorMessage = "Input required !";
            // 
            // cmbPermanentDistrict
            // 
            this.cmbPermanentDistrict.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbPermanentDistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPermanentDistrict.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbPermanentDistrict.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPermanentDistrict.FormattingEnabled = true;
            this.cmbPermanentDistrict.InputScopeAllowEmpty = false;
            this.cmbPermanentDistrict.IntegralHeight = false;
            this.cmbPermanentDistrict.IsValid = null;
            this.cmbPermanentDistrict.ItemHeight = 13;
            this.cmbPermanentDistrict.Location = new System.Drawing.Point(515, 113);
            this.cmbPermanentDistrict.Name = "cmbPermanentDistrict";
            this.cmbPermanentDistrict.PromptText = "(Select)";
            this.cmbPermanentDistrict.ReadOnly = false;
            this.cmbPermanentDistrict.ShowMandatoryMark = false;
            this.cmbPermanentDistrict.Size = new System.Drawing.Size(237, 21);
            this.cmbPermanentDistrict.TabIndex = 28;
            this.cmbPermanentDistrict.ValidationErrorMessage = "Input required !";
            this.cmbPermanentDistrict.SelectedIndexChanged += new System.EventHandler(this.cmbPermanentDistrict_SelectedIndexChanged);
            // 
            // cmbPermanentDivision
            // 
            this.cmbPermanentDivision.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbPermanentDivision.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPermanentDivision.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbPermanentDivision.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPermanentDivision.FormattingEnabled = true;
            this.cmbPermanentDivision.InputScopeAllowEmpty = false;
            this.cmbPermanentDivision.IntegralHeight = false;
            this.cmbPermanentDivision.IsValid = null;
            this.cmbPermanentDivision.ItemHeight = 13;
            this.cmbPermanentDivision.Location = new System.Drawing.Point(515, 84);
            this.cmbPermanentDivision.Name = "cmbPermanentDivision";
            this.cmbPermanentDivision.PromptText = "(Select)";
            this.cmbPermanentDivision.ReadOnly = false;
            this.cmbPermanentDivision.ShowMandatoryMark = false;
            this.cmbPermanentDivision.Size = new System.Drawing.Size(237, 21);
            this.cmbPermanentDivision.TabIndex = 25;
            this.cmbPermanentDivision.ValidationErrorMessage = "Input required !";
            this.cmbPermanentDivision.SelectedIndexChanged += new System.EventHandler(this.cmbPermanentDivision_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(4, 32);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(101, 15);
            this.label11.TabIndex = 0;
            this.label11.Text = "Contact Address :";
            // 
            // txtPermanentAddress2
            // 
            this.txtPermanentAddress2.BackColor = System.Drawing.SystemColors.Window;
            this.txtPermanentAddress2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPermanentAddress2.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtPermanentAddress2.InputScopeAllowEmpty = true;
            this.txtPermanentAddress2.InputScopeCustomString = null;
            this.txtPermanentAddress2.IsValid = null;
            this.txtPermanentAddress2.Location = new System.Drawing.Point(515, 56);
            this.txtPermanentAddress2.MaxLength = 32767;
            this.txtPermanentAddress2.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtPermanentAddress2.Name = "txtPermanentAddress2";
            this.txtPermanentAddress2.PromptText = "(Type here...)";
            this.txtPermanentAddress2.ReadOnly = false;
            this.txtPermanentAddress2.ShowMandatoryMark = false;
            this.txtPermanentAddress2.Size = new System.Drawing.Size(237, 21);
            this.txtPermanentAddress2.TabIndex = 23;
            this.txtPermanentAddress2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtPermanentAddress2.UpperCaseOnly = false;
            this.txtPermanentAddress2.ValidationErrorMessage = "Validation Error!";
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(425, 205);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(86, 15);
            this.label17.TabIndex = 36;
            this.label17.Text = "Country Code :";
            // 
            // txtPermanentAddress1
            // 
            this.txtPermanentAddress1.BackColor = System.Drawing.Color.White;
            this.txtPermanentAddress1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPermanentAddress1.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtPermanentAddress1.InputScopeAllowEmpty = false;
            this.txtPermanentAddress1.InputScopeCustomString = null;
            this.txtPermanentAddress1.IsValid = null;
            this.txtPermanentAddress1.Location = new System.Drawing.Point(515, 28);
            this.txtPermanentAddress1.MaxLength = 32767;
            this.txtPermanentAddress1.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtPermanentAddress1.Name = "txtPermanentAddress1";
            this.txtPermanentAddress1.PromptText = "(Type here...)";
            this.txtPermanentAddress1.ReadOnly = false;
            this.txtPermanentAddress1.ShowMandatoryMark = false;
            this.txtPermanentAddress1.Size = new System.Drawing.Size(237, 21);
            this.txtPermanentAddress1.TabIndex = 21;
            this.txtPermanentAddress1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtPermanentAddress1.UpperCaseOnly = false;
            this.txtPermanentAddress1.ValidationErrorMessage = "Input required !";
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(432, 176);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(79, 15);
            this.label18.TabIndex = 33;
            this.label18.Text = "Postal Code :";
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(463, 147);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(48, 15);
            this.label19.TabIndex = 30;
            this.label19.Text = "Thana :";
            // 
            // mandatoryMark21
            // 
            this.mandatoryMark21.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark21.Location = new System.Drawing.Point(755, 34);
            this.mandatoryMark21.MaximumSize = new System.Drawing.Size(51, 22);
            this.mandatoryMark21.MinimumSize = new System.Drawing.Size(12, 5);
            this.mandatoryMark21.Name = "mandatoryMark21";
            this.mandatoryMark21.Size = new System.Drawing.Size(15, 13);
            this.mandatoryMark21.TabIndex = 22;
            this.mandatoryMark21.TabStop = false;
            // 
            // mandatoryMark20
            // 
            this.mandatoryMark20.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark20.Location = new System.Drawing.Point(755, 87);
            this.mandatoryMark20.MaximumSize = new System.Drawing.Size(51, 22);
            this.mandatoryMark20.MinimumSize = new System.Drawing.Size(12, 5);
            this.mandatoryMark20.Name = "mandatoryMark20";
            this.mandatoryMark20.Size = new System.Drawing.Size(15, 13);
            this.mandatoryMark20.TabIndex = 26;
            this.mandatoryMark20.TabStop = false;
            // 
            // mandatoryMark19
            // 
            this.mandatoryMark19.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark19.Location = new System.Drawing.Point(755, 116);
            this.mandatoryMark19.MaximumSize = new System.Drawing.Size(51, 22);
            this.mandatoryMark19.MinimumSize = new System.Drawing.Size(12, 5);
            this.mandatoryMark19.Name = "mandatoryMark19";
            this.mandatoryMark19.Size = new System.Drawing.Size(15, 13);
            this.mandatoryMark19.TabIndex = 29;
            this.mandatoryMark19.TabStop = false;
            // 
            // label20
            // 
            this.label20.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(461, 118);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(50, 15);
            this.label20.TabIndex = 27;
            this.label20.Text = "District :";
            // 
            // mandatoryMark18
            // 
            this.mandatoryMark18.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark18.Location = new System.Drawing.Point(755, 146);
            this.mandatoryMark18.MaximumSize = new System.Drawing.Size(51, 22);
            this.mandatoryMark18.MinimumSize = new System.Drawing.Size(12, 5);
            this.mandatoryMark18.Name = "mandatoryMark18";
            this.mandatoryMark18.Size = new System.Drawing.Size(15, 13);
            this.mandatoryMark18.TabIndex = 32;
            this.mandatoryMark18.TabStop = false;
            // 
            // mandatoryMark17
            // 
            this.mandatoryMark17.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark17.Location = new System.Drawing.Point(755, 173);
            this.mandatoryMark17.MaximumSize = new System.Drawing.Size(51, 22);
            this.mandatoryMark17.MinimumSize = new System.Drawing.Size(12, 5);
            this.mandatoryMark17.Name = "mandatoryMark17";
            this.mandatoryMark17.Size = new System.Drawing.Size(15, 13);
            this.mandatoryMark17.TabIndex = 35;
            this.mandatoryMark17.TabStop = false;
            // 
            // txtContactAddress2
            // 
            this.txtContactAddress2.BackColor = System.Drawing.SystemColors.Window;
            this.txtContactAddress2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContactAddress2.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtContactAddress2.InputScopeAllowEmpty = true;
            this.txtContactAddress2.InputScopeCustomString = null;
            this.txtContactAddress2.IsValid = null;
            this.txtContactAddress2.Location = new System.Drawing.Point(112, 56);
            this.txtContactAddress2.MaxLength = 32767;
            this.txtContactAddress2.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtContactAddress2.Name = "txtContactAddress2";
            this.txtContactAddress2.PromptText = "(Type here...)";
            this.txtContactAddress2.ReadOnly = false;
            this.txtContactAddress2.ShowMandatoryMark = false;
            this.txtContactAddress2.Size = new System.Drawing.Size(237, 21);
            this.txtContactAddress2.TabIndex = 2;
            this.txtContactAddress2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtContactAddress2.UpperCaseOnly = false;
            this.txtContactAddress2.ValidationErrorMessage = "Validation Error!";
            // 
            // label21
            // 
            this.label21.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(455, 89);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(56, 15);
            this.label21.TabIndex = 24;
            this.label21.Text = "Division :";
            // 
            // mandatoryMark16
            // 
            this.mandatoryMark16.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark16.Location = new System.Drawing.Point(755, 202);
            this.mandatoryMark16.MaximumSize = new System.Drawing.Size(44, 22);
            this.mandatoryMark16.MinimumSize = new System.Drawing.Size(10, 5);
            this.mandatoryMark16.Name = "mandatoryMark16";
            this.mandatoryMark16.Size = new System.Drawing.Size(15, 13);
            this.mandatoryMark16.TabIndex = 0;
            this.mandatoryMark16.TabStop = false;
            // 
            // mandatoryMark15
            // 
            this.mandatoryMark15.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark15.Location = new System.Drawing.Point(352, 204);
            this.mandatoryMark15.MaximumSize = new System.Drawing.Size(38, 22);
            this.mandatoryMark15.MinimumSize = new System.Drawing.Size(9, 5);
            this.mandatoryMark15.Name = "mandatoryMark15";
            this.mandatoryMark15.Size = new System.Drawing.Size(13, 13);
            this.mandatoryMark15.TabIndex = 17;
            this.mandatoryMark15.TabStop = false;
            // 
            // mandatoryMark14
            // 
            this.mandatoryMark14.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark14.Location = new System.Drawing.Point(352, 174);
            this.mandatoryMark14.MaximumSize = new System.Drawing.Size(33, 22);
            this.mandatoryMark14.MinimumSize = new System.Drawing.Size(8, 5);
            this.mandatoryMark14.Name = "mandatoryMark14";
            this.mandatoryMark14.Size = new System.Drawing.Size(15, 13);
            this.mandatoryMark14.TabIndex = 14;
            this.mandatoryMark14.TabStop = false;
            // 
            // mandatoryMark13
            // 
            this.mandatoryMark13.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark13.Location = new System.Drawing.Point(352, 146);
            this.mandatoryMark13.MaximumSize = new System.Drawing.Size(33, 22);
            this.mandatoryMark13.MinimumSize = new System.Drawing.Size(8, 5);
            this.mandatoryMark13.Name = "mandatoryMark13";
            this.mandatoryMark13.Size = new System.Drawing.Size(15, 13);
            this.mandatoryMark13.TabIndex = 11;
            this.mandatoryMark13.TabStop = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(390, 32);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(121, 15);
            this.label22.TabIndex = 20;
            this.label22.Text = "Permanent Address :";
            // 
            // mandatoryMark12
            // 
            this.mandatoryMark12.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark12.Location = new System.Drawing.Point(352, 118);
            this.mandatoryMark12.MaximumSize = new System.Drawing.Size(33, 22);
            this.mandatoryMark12.MinimumSize = new System.Drawing.Size(8, 5);
            this.mandatoryMark12.Name = "mandatoryMark12";
            this.mandatoryMark12.Size = new System.Drawing.Size(15, 13);
            this.mandatoryMark12.TabIndex = 8;
            this.mandatoryMark12.TabStop = false;
            // 
            // mandatoryMark11
            // 
            this.mandatoryMark11.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark11.Location = new System.Drawing.Point(352, 88);
            this.mandatoryMark11.MaximumSize = new System.Drawing.Size(33, 22);
            this.mandatoryMark11.MinimumSize = new System.Drawing.Size(8, 5);
            this.mandatoryMark11.Name = "mandatoryMark11";
            this.mandatoryMark11.Size = new System.Drawing.Size(15, 13);
            this.mandatoryMark11.TabIndex = 5;
            this.mandatoryMark11.TabStop = false;
            // 
            // mandatoryMark10
            // 
            this.mandatoryMark10.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark10.Location = new System.Drawing.Point(352, 32);
            this.mandatoryMark10.MaximumSize = new System.Drawing.Size(33, 22);
            this.mandatoryMark10.MinimumSize = new System.Drawing.Size(8, 5);
            this.mandatoryMark10.Name = "mandatoryMark10";
            this.mandatoryMark10.Size = new System.Drawing.Size(15, 13);
            this.mandatoryMark10.TabIndex = 1;
            this.mandatoryMark10.TabStop = false;
            // 
            // txtContactAddress1
            // 
            this.txtContactAddress1.BackColor = System.Drawing.SystemColors.Window;
            this.txtContactAddress1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContactAddress1.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtContactAddress1.InputScopeAllowEmpty = false;
            this.txtContactAddress1.InputScopeCustomString = null;
            this.txtContactAddress1.IsValid = null;
            this.txtContactAddress1.Location = new System.Drawing.Point(112, 28);
            this.txtContactAddress1.MaxLength = 32767;
            this.txtContactAddress1.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtContactAddress1.Name = "txtContactAddress1";
            this.txtContactAddress1.PromptText = "(Type here...)";
            this.txtContactAddress1.ReadOnly = false;
            this.txtContactAddress1.ShowMandatoryMark = false;
            this.txtContactAddress1.Size = new System.Drawing.Size(237, 21);
            this.txtContactAddress1.TabIndex = 0;
            this.txtContactAddress1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtContactAddress1.UpperCaseOnly = false;
            this.txtContactAddress1.ValidationErrorMessage = "Input required !";
            // 
            // chkUseAsPermanent
            // 
            this.chkUseAsPermanent.AutoSize = true;
            this.chkUseAsPermanent.Location = new System.Drawing.Point(515, 5);
            this.chkUseAsPermanent.Name = "chkUseAsPermanent";
            this.chkUseAsPermanent.Size = new System.Drawing.Size(137, 15);
            this.chkUseAsPermanent.TabIndex = 19;
            this.chkUseAsPermanent.Text = "Copy contact address";
            this.chkUseAsPermanent.UseSelectable = true;
            this.chkUseAsPermanent.CheckedChanged += new System.EventHandler(this.chkUseAsPermanent_CheckedChanged);
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(19, 205);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(86, 15);
            this.label16.TabIndex = 15;
            this.label16.Text = "Country Code :";
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(26, 176);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 15);
            this.label15.TabIndex = 12;
            this.label15.Text = "Postal Code :";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(57, 147);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(48, 15);
            this.label14.TabIndex = 9;
            this.label14.Text = "Thana :";
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(55, 118);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(50, 15);
            this.label13.TabIndex = 6;
            this.label13.Text = "District :";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(49, 89);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 15);
            this.label12.TabIndex = 3;
            this.label12.Text = "Division :";
            // 
            // dtpBirthDate
            // 
            this.dtpBirthDate.Date = "13/01/2016";
            this.dtpBirthDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpBirthDate.Location = new System.Drawing.Point(135, 168);
            this.dtpBirthDate.MaximumSize = new System.Drawing.Size(467, 25);
            this.dtpBirthDate.MinimumSize = new System.Drawing.Size(70, 25);
            this.dtpBirthDate.Name = "dtpBirthDate";
            this.dtpBirthDate.PresetServerDate = true;
            this.dtpBirthDate.Size = new System.Drawing.Size(240, 25);
            this.dtpBirthDate.TabIndex = 18;
            this.dtpBirthDate.Value = new System.DateTime(2016, 1, 13, 11, 30, 18, 835);
            // 
            // mandatoryMark9
            // 
            this.mandatoryMark9.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark9.Location = new System.Drawing.Point(376, 143);
            this.mandatoryMark9.MaximumSize = new System.Drawing.Size(33, 22);
            this.mandatoryMark9.MinimumSize = new System.Drawing.Size(8, 5);
            this.mandatoryMark9.Name = "mandatoryMark9";
            this.mandatoryMark9.Size = new System.Drawing.Size(15, 13);
            this.mandatoryMark9.TabIndex = 16;
            this.mandatoryMark9.TabStop = false;
            // 
            // mandatoryMark8
            // 
            this.mandatoryMark8.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark8.Location = new System.Drawing.Point(376, 173);
            this.mandatoryMark8.MaximumSize = new System.Drawing.Size(28, 22);
            this.mandatoryMark8.MinimumSize = new System.Drawing.Size(7, 5);
            this.mandatoryMark8.Name = "mandatoryMark8";
            this.mandatoryMark8.Size = new System.Drawing.Size(15, 13);
            this.mandatoryMark8.TabIndex = 19;
            this.mandatoryMark8.TabStop = false;
            // 
            // mandatoryMark7
            // 
            this.mandatoryMark7.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark7.Location = new System.Drawing.Point(376, 115);
            this.mandatoryMark7.MaximumSize = new System.Drawing.Size(38, 22);
            this.mandatoryMark7.MinimumSize = new System.Drawing.Size(9, 5);
            this.mandatoryMark7.Name = "mandatoryMark7";
            this.mandatoryMark7.Size = new System.Drawing.Size(15, 13);
            this.mandatoryMark7.TabIndex = 13;
            this.mandatoryMark7.TabStop = false;
            // 
            // mandatoryMark6
            // 
            this.mandatoryMark6.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark6.Location = new System.Drawing.Point(376, 86);
            this.mandatoryMark6.MaximumSize = new System.Drawing.Size(33, 22);
            this.mandatoryMark6.MinimumSize = new System.Drawing.Size(8, 5);
            this.mandatoryMark6.Name = "mandatoryMark6";
            this.mandatoryMark6.Size = new System.Drawing.Size(15, 13);
            this.mandatoryMark6.TabIndex = 9;
            this.mandatoryMark6.TabStop = false;
            // 
            // gbUserImage
            // 
            this.gbUserImage.Controls.Add(this.btnBrowse);
            this.gbUserImage.Controls.Add(this.pbImageUser);
            this.gbUserImage.Controls.Add(this.btnCapture);
            this.gbUserImage.Location = new System.Drawing.Point(611, 9);
            this.gbUserImage.Name = "gbUserImage";
            this.gbUserImage.Size = new System.Drawing.Size(200, 193);
            this.gbUserImage.TabIndex = 20;
            this.gbUserImage.TabStop = false;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnBrowse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnBrowse.FlatAppearance.BorderSize = 0;
            this.btnBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBrowse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.btnBrowse.ForeColor = System.Drawing.Color.White;
            this.btnBrowse.Location = new System.Drawing.Point(22, 159);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(74, 23);
            this.btnBrowse.TabIndex = 0;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = false;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // pbImageUser
            // 
            this.pbImageUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbImageUser.Image = global::MISL.Ababil.Agent.UI.Properties.Resources.user_account___Copy;
            this.pbImageUser.Location = new System.Drawing.Point(37, 17);
            this.pbImageUser.Name = "pbImageUser";
            this.pbImageUser.Size = new System.Drawing.Size(126, 136);
            this.pbImageUser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbImageUser.TabIndex = 23;
            this.pbImageUser.TabStop = false;
            // 
            // btnCapture
            // 
            this.btnCapture.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnCapture.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnCapture.FlatAppearance.BorderSize = 0;
            this.btnCapture.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCapture.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.btnCapture.ForeColor = System.Drawing.Color.White;
            this.btnCapture.Location = new System.Drawing.Point(102, 159);
            this.btnCapture.Name = "btnCapture";
            this.btnCapture.Size = new System.Drawing.Size(74, 23);
            this.btnCapture.TabIndex = 1;
            this.btnCapture.Text = "Capture";
            this.btnCapture.UseVisualStyleBackColor = false;
            this.btnCapture.Click += new System.EventHandler(this.btnCapture_Click);
            // 
            // txtMothersLastName
            // 
            this.txtMothersLastName.BackColor = System.Drawing.SystemColors.Window;
            this.txtMothersLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMothersLastName.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtMothersLastName.InputScopeAllowEmpty = false;
            this.txtMothersLastName.InputScopeCustomString = null;
            this.txtMothersLastName.IsValid = null;
            this.txtMothersLastName.Location = new System.Drawing.Point(244, 113);
            this.txtMothersLastName.MaxLength = 32767;
            this.txtMothersLastName.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtMothersLastName.Name = "txtMothersLastName";
            this.txtMothersLastName.PromptText = "(Type here...)";
            this.txtMothersLastName.ReadOnly = false;
            this.txtMothersLastName.ShowMandatoryMark = false;
            this.txtMothersLastName.Size = new System.Drawing.Size(130, 21);
            this.txtMothersLastName.TabIndex = 12;
            this.txtMothersLastName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtMothersLastName.UpperCaseOnly = false;
            this.txtMothersLastName.ValidationErrorMessage = "Input required !";
            // 
            // mandatoryMark3
            // 
            this.mandatoryMark3.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark3.Location = new System.Drawing.Point(376, 57);
            this.mandatoryMark3.MaximumSize = new System.Drawing.Size(28, 22);
            this.mandatoryMark3.MinimumSize = new System.Drawing.Size(7, 5);
            this.mandatoryMark3.Name = "mandatoryMark3";
            this.mandatoryMark3.Size = new System.Drawing.Size(15, 13);
            this.mandatoryMark3.TabIndex = 5;
            this.mandatoryMark3.TabStop = false;
            // 
            // mandatoryMark2
            // 
            this.mandatoryMark2.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark2.Location = new System.Drawing.Point(376, 28);
            this.mandatoryMark2.MaximumSize = new System.Drawing.Size(24, 22);
            this.mandatoryMark2.MinimumSize = new System.Drawing.Size(6, 5);
            this.mandatoryMark2.Name = "mandatoryMark2";
            this.mandatoryMark2.Size = new System.Drawing.Size(15, 13);
            this.mandatoryMark2.TabIndex = 2;
            this.mandatoryMark2.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(64, 173);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 15);
            this.label9.TabIndex = 17;
            this.label9.Text = "Birth Date :";
            // 
            // txtFirstName
            // 
            this.txtFirstName.BackColor = System.Drawing.SystemColors.Window;
            this.txtFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtFirstName.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtFirstName.InputScopeAllowEmpty = false;
            this.txtFirstName.InputScopeCustomString = null;
            this.txtFirstName.IsValid = null;
            this.txtFirstName.Location = new System.Drawing.Point(137, 27);
            this.txtFirstName.MaxLength = 32767;
            this.txtFirstName.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.PromptText = "(Type here...)";
            this.txtFirstName.ReadOnly = false;
            this.txtFirstName.ShowMandatoryMark = false;
            this.txtFirstName.Size = new System.Drawing.Size(237, 21);
            this.txtFirstName.TabIndex = 1;
            this.txtFirstName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtFirstName.UpperCaseOnly = false;
            this.txtFirstName.ValidationErrorMessage = "Input required !";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(33, 116);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 15);
            this.label10.TabIndex = 10;
            this.label10.Text = "Mother\'s Name :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(57, 144);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 15);
            this.label8.TabIndex = 14;
            this.label8.Text = "Contact No. :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(58, 58);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 15);
            this.label7.TabIndex = 3;
            this.label7.Text = "Last Name :";
            // 
            // txtMothersFirstName
            // 
            this.txtMothersFirstName.BackColor = System.Drawing.SystemColors.Window;
            this.txtMothersFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMothersFirstName.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtMothersFirstName.InputScopeAllowEmpty = false;
            this.txtMothersFirstName.InputScopeCustomString = null;
            this.txtMothersFirstName.IsValid = null;
            this.txtMothersFirstName.Location = new System.Drawing.Point(137, 114);
            this.txtMothersFirstName.MaxLength = 32767;
            this.txtMothersFirstName.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtMothersFirstName.Name = "txtMothersFirstName";
            this.txtMothersFirstName.PromptText = "(Type here...)";
            this.txtMothersFirstName.ReadOnly = false;
            this.txtMothersFirstName.ShowMandatoryMark = false;
            this.txtMothersFirstName.Size = new System.Drawing.Size(100, 21);
            this.txtMothersFirstName.TabIndex = 11;
            this.txtMothersFirstName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtMothersFirstName.UpperCaseOnly = false;
            this.txtMothersFirstName.ValidationErrorMessage = "Input required !";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(37, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 15);
            this.label5.TabIndex = 6;
            this.label5.Text = "Father\'s Name :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(58, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 15);
            this.label6.TabIndex = 0;
            this.label6.Text = "First Name :";
            // 
            // txtLastName
            // 
            this.txtLastName.BackColor = System.Drawing.SystemColors.Window;
            this.txtLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtLastName.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtLastName.InputScopeAllowEmpty = false;
            this.txtLastName.InputScopeCustomString = null;
            this.txtLastName.IsValid = null;
            this.txtLastName.Location = new System.Drawing.Point(137, 55);
            this.txtLastName.MaxLength = 32767;
            this.txtLastName.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.PromptText = "(Type here...)";
            this.txtLastName.ReadOnly = false;
            this.txtLastName.ShowMandatoryMark = false;
            this.txtLastName.Size = new System.Drawing.Size(237, 21);
            this.txtLastName.TabIndex = 4;
            this.txtLastName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtLastName.UpperCaseOnly = false;
            this.txtLastName.ValidationErrorMessage = "Input required !";
            // 
            // txtFathersFirstName
            // 
            this.txtFathersFirstName.BackColor = System.Drawing.SystemColors.Window;
            this.txtFathersFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFathersFirstName.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtFathersFirstName.InputScopeAllowEmpty = false;
            this.txtFathersFirstName.InputScopeCustomString = null;
            this.txtFathersFirstName.IsValid = null;
            this.txtFathersFirstName.Location = new System.Drawing.Point(137, 85);
            this.txtFathersFirstName.MaxLength = 32767;
            this.txtFathersFirstName.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtFathersFirstName.Name = "txtFathersFirstName";
            this.txtFathersFirstName.PromptText = "(Type here...)";
            this.txtFathersFirstName.ReadOnly = false;
            this.txtFathersFirstName.ShowMandatoryMark = false;
            this.txtFathersFirstName.Size = new System.Drawing.Size(100, 21);
            this.txtFathersFirstName.TabIndex = 7;
            this.txtFathersFirstName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtFathersFirstName.UpperCaseOnly = false;
            this.txtFathersFirstName.ValidationErrorMessage = "Input required !";
            // 
            // txtFathersLastName
            // 
            this.txtFathersLastName.BackColor = System.Drawing.SystemColors.Window;
            this.txtFathersLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFathersLastName.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtFathersLastName.InputScopeAllowEmpty = false;
            this.txtFathersLastName.InputScopeCustomString = null;
            this.txtFathersLastName.IsValid = null;
            this.txtFathersLastName.Location = new System.Drawing.Point(244, 84);
            this.txtFathersLastName.MaxLength = 32767;
            this.txtFathersLastName.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtFathersLastName.Name = "txtFathersLastName";
            this.txtFathersLastName.PromptText = "(Type here...)";
            this.txtFathersLastName.ReadOnly = false;
            this.txtFathersLastName.ShowMandatoryMark = false;
            this.txtFathersLastName.Size = new System.Drawing.Size(130, 21);
            this.txtFathersLastName.TabIndex = 8;
            this.txtFathersLastName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtFathersLastName.UpperCaseOnly = false;
            this.txtFathersLastName.ValidationErrorMessage = "Input required !";
            // 
            // txtMobileNumber
            // 
            this.txtMobileNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtMobileNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMobileNumber.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.MobileNumber;
            this.txtMobileNumber.InputScopeAllowEmpty = false;
            this.txtMobileNumber.InputScopeCustomString = null;
            this.txtMobileNumber.IsValid = null;
            this.txtMobileNumber.Location = new System.Drawing.Point(137, 141);
            this.txtMobileNumber.MaxLength = 11;
            this.txtMobileNumber.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtMobileNumber.Name = "txtMobileNumber";
            this.txtMobileNumber.PromptText = "(Type here...)";
            this.txtMobileNumber.ReadOnly = false;
            this.txtMobileNumber.ShowMandatoryMark = false;
            this.txtMobileNumber.Size = new System.Drawing.Size(237, 21);
            this.txtMobileNumber.TabIndex = 15;
            this.txtMobileNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtMobileNumber.UpperCaseOnly = false;
            this.txtMobileNumber.ValidationErrorMessage = "Invalid mobile no. !";
            this.txtMobileNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMobileNumber_KeyPress);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(675, 675);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(79, 25);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // lblImgExtension
            // 
            this.lblImgExtension.AutoSize = true;
            this.lblImgExtension.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImgExtension.Location = new System.Drawing.Point(84, 661);
            this.lblImgExtension.Name = "lblImgExtension";
            this.lblImgExtension.Size = new System.Drawing.Size(140, 15);
            this.lblImgExtension.TabIndex = 2;
            this.lblImgExtension.Text = "Img Extension (invisible)";
            this.lblImgExtension.Visible = false;
            // 
            // errProvider
            // 
            this.errProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errProvider.ContainerControl = this;
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(866, 26);
            this.customTitlebar1.TabIndex = 5;
            // 
            // frmUserCreationWithPersonalInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(866, 714);
            this.Controls.Add(this.lblImgExtension);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmUserCreationWithPersonalInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "User Information";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmUserCreationWithPersonalInfo_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.pnlUserInfo.ResumeLayout(false);
            this.pnlUserInfo.PerformLayout();
            this.pnlChangeCredential.ResumeLayout(false);
            this.pnlChangeCredential.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.pnlAddress.ResumeLayout(false);
            this.pnlAddress.PerformLayout();
            this.gbUserImage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbImageUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomTitlebar customTitlebar1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnFingerPrint;
        private System.Windows.Forms.GroupBox groupBox2;
        private CustomComboBoxDropDownList cmbUserType;
        private System.Windows.Forms.Label lblUserType;
        private CustomComboBoxDropDownList cmbUserStatus;
        private System.Windows.Forms.Label label4;
        private CustomTextBox txtFathersFirstName;
        private CustomTextBox txtLastName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private CustomTextBox txtFirstName;
        private System.Windows.Forms.Label label6;
        private CustomTextBox txtMobileNumber;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox gbUserImage;
        private System.Windows.Forms.PictureBox pbImageUser;
        private MetroFramework.Controls.MetroCheckBox chkUseAsPermanent;
        private System.Windows.Forms.Label label17;
        private CustomComboBoxDropDownList cmbPermanentCountryCode;
        private System.Windows.Forms.Label label18;
        private CustomComboBoxDropDownList cmbPermanentPostalCode;
        private System.Windows.Forms.Label label19;
        private CustomComboBoxDropDownList cmbPermanentThana;
        private System.Windows.Forms.Label label20;
        private CustomComboBoxDropDownList cmbPermanentDistrict;
        private System.Windows.Forms.Label label21;
        private CustomComboBoxDropDownList cmbPermanentDivision;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label16;
        private CustomComboBoxDropDownList cmbContactCountryCode;
        private System.Windows.Forms.Label label15;
        private CustomComboBoxDropDownList cmbContactPostCode;
        private System.Windows.Forms.Label label14;
        private CustomComboBoxDropDownList cmbContactThana;
        private System.Windows.Forms.Label label13;
        private CustomComboBoxDropDownList cmbContactDistrict;
        private System.Windows.Forms.Label label12;
        private CustomComboBoxDropDownList cmbContactDivision;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Button btnCapture;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private CustomTextBox txtContactAddress1;
        private CustomTextBox txtContactAddress2;
        private CustomTextBox txtPermanentAddress2;
        private CustomTextBox txtPermanentAddress1;
        private CustomTextBox txtFathersLastName;
        private CustomTextBox txtMothersLastName;
        private CustomTextBox txtMothersFirstName;
        private CustomTextBox txtUserName;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label lblImgExtension;
        private CustomControls.CustomDateTimePicker dtpBirthDate;
        private System.Windows.Forms.Button btnResetCredential;
        private System.Windows.Forms.ErrorProvider errProvider;
        private CustomControls.MandatoryMark mandatoryMark2;
        private CustomControls.MandatoryMark mandatoryMark3;
        private CustomControls.MandatoryMark mandatoryMark6;
        private CustomControls.MandatoryMark mandatoryMark7;
        private CustomControls.MandatoryMark mandatoryMark8;
        private CustomControls.MandatoryMark mandatoryMark9;
        private CustomControls.MandatoryMark mandatoryMark10;
        private CustomControls.MandatoryMark mandatoryMark11;
        private CustomControls.MandatoryMark mandatoryMark12;
        private CustomControls.MandatoryMark mandatoryMark13;
        private CustomControls.MandatoryMark mandatoryMark14;
        private CustomControls.MandatoryMark mandatoryMark15;
        private CustomControls.MandatoryMark mandatoryMark16;
        private CustomControls.MandatoryMark mandatoryMark17;
        private CustomControls.MandatoryMark mandatoryMark18;
        private CustomControls.MandatoryMark mandatoryMark19;
        private CustomControls.MandatoryMark mandatoryMark20;
        private CustomControls.MandatoryMark mandatoryMark21;
        private MetroFramework.Controls.MetroCheckBox chkResetPassword;
        private MetroFramework.Controls.MetroCheckBox chkResetFingerPrint;
        private System.Windows.Forms.Panel pnlChangeCredential;
        private System.Windows.Forms.Panel pnlUserInfo;
        private System.Windows.Forms.Panel pnlAddress;
        private System.Windows.Forms.Label lblNarration;
        private CustomTextBox txtNarration;
        private CustomComboBoxDropDownList cmbUserBranch;
        private System.Windows.Forms.Label lblUserBranchHeader;
    }
}