﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Services;

namespace MISL.Ababil.Agent.UI.forms
{
    public partial class frmTermAccountInquery : CustomForm
    {
        private bool _isModifier = false;

        public frmTermAccountInquery()
        {
            InitializeComponent();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Reset(false);
            if (txtAccountNumber.Text.Trim() != "")
            {
                Search();
            }
        }

        private void FillComponents(TermAccInforamationDto termAccInforamationDto)
        {
            if (termAccInforamationDto.accountType == AccountType.SSP)      // Both SSP & MTDR search is OK, but this restriction was implemented to punnish them.
            {
                txtCustomerId.Text = termAccInforamationDto.custId.ToString();
                txtAccountName.Text = termAccInforamationDto.accountTitle;
                txtAccountStatus.Text = termAccInforamationDto.accountstatus;
                txtProduct.Text = termAccInforamationDto.productTitle;
                //txtCurrentBalance.Text = termAccInforamationDto.currentbalance.ToString(); 
                txtCurrentBalance.Text = string.Format(new CultureInfo("BN-BD"), "{0:N}", termAccInforamationDto.currentbalance);

                //termAccInforamationDto.currentbalance.ToString("N", new CultureInfo("BN-BD"));

                txtOpeningDate.Text = termAccInforamationDto.openingdate;
                txtExpiryDate.Text = termAccInforamationDto.expirydate;
                txtLastTxnDate.Text = termAccInforamationDto.lastOperationdate;
                txtClosingDate.Text = termAccInforamationDto.closingdate;

                txtInstallmentAmount.Text = string.Format(new CultureInfo("BN-BD"), "{0:N}", termAccInforamationDto.installmentAmount);
                txtTotalNoofInstallment.Text = termAccInforamationDto.totalNoOfInstallment.ToString();
                txtNoofGivenInstallment.Text = termAccInforamationDto.noOfGivenInstallment.ToString();
                txtNoDueInstallment.Text = termAccInforamationDto.noOfDueInstallment.ToString();

                if (termAccInforamationDto.accountType == AccountType.SSP)
                {
                    txtNoofAdvanceInstallment.Text = (termAccInforamationDto.totalAdvInstallment / termAccInforamationDto.installmentAmount).ToString();
                }

                txtQuardAmount.Text = string.Format(new CultureInfo("BN-BD"), "{0:N}", termAccInforamationDto.quardAmount);
                txtLienAmount.Text = string.Format(new CultureInfo("BN-BD"), "{0:N}", termAccInforamationDto.lienAmount);

                txtRenewDate.Text = termAccInforamationDto.renewDate;
                txtRenewRate.Text = termAccInforamationDto.renewRate.ToString();
                txtQuardAmount2.Text = string.Format(new CultureInfo("BN-BD"), "{0:N}", termAccInforamationDto.quardAmount);
                txtLienAmount2.Text = string.Format(new CultureInfo("BN-BD"), "{0:N}", termAccInforamationDto.lienAmount);

                switch (termAccInforamationDto.accountType)
                {
                    case AccountType.Deposit:
                        break;
                    case AccountType.SSP:
                        gbxMTDR.Visible = false;
                        gbxITD.Visible = true;
                        break;
                    case AccountType.MTDR:
                        gbxMTDR.Visible = true;
                        gbxITD.Visible = false;
                        break;
                    case null:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            else
            { MsgBox.showWarning("Invalid account."); }
        }

        private void frmTermAccountInquery_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void Reset(bool clearAccountNumber = true)
        {
            if (clearAccountNumber)
            {
                txtAccountNumber.Text = "";
            }

            txtCustomerId.Text = "";
            txtAccountName.Text = "";
            txtAccountStatus.Text = "";
            txtProduct.Text = "";
            txtCurrentBalance.Text = "";

            txtOpeningDate.Text = "";
            txtExpiryDate.Text = "";
            txtLastTxnDate.Text = "";
            txtClosingDate.Text = "";

            txtInstallmentAmount.Text = "";
            txtTotalNoofInstallment.Text = "";
            txtNoofGivenInstallment.Text = "";
            txtNoDueInstallment.Text = "";
            txtNoofAdvanceInstallment.Text = "";

            txtQuardAmount.Text = "";
            txtLienAmount.Text = "";
        }

        private void Search()
        {
            try
            {
                TermAccInforamationDto termAccInforamationDto =
                    (new TermAccountInqueryService()).GetTermAccountInformationByAccountNo(txtAccountNumber.Text);
                FillComponents(termAccInforamationDto);
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        private void txtAccountNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                e.Handled = true;
                Search();
                btnSearch.Focus();
            }
            if (e.Modifiers == Keys.Control)
            {
                _isModifier = true;
            }
        }

        private void txtAccountNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (_isModifier)
            {
                try
                {
                    long.Parse(Clipboard.GetText());
                }
                catch
                {
                    e.Handled = true;
                }
                return;
            }
            if ((!char.IsNumber(e.KeyChar)) && ((Keys)e.KeyChar != Keys.Back) && ((Keys)e.KeyChar != Keys.Control))
            {
                e.Handled = true;
            }
        }
        private void txtAccountNumber_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control)
            {
                _isModifier = false;
            }
        }

        private void panel3_Click(object sender, EventArgs e)
        {
            txtAccountNumber.Focus();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}