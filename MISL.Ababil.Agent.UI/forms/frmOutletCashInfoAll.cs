﻿using MetroFramework.Forms;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MISL.Ababil.Agent.UI.forms
{
    public partial class frmOutletCashInfoAll : MetroForm
    {
        private Packet _packet;
        private GUI _gui = new GUI();
        private List<AgentInformation> _objAgentInfoList = new List<AgentInformation>();
        private AgentServices _objAgentServices = new AgentServices();
        private AgentInformation _agentInformation = new AgentInformation();
        private AgentServices _agentServices = new AgentServices();

        List<SubAgentInformation> subAgentList = new List<SubAgentInformation>();       // WALI :: 14-Jan-2016
        private SubAgentServices _outletServices = new SubAgentServices();
        //List<SubAgentUser> subAgentUserList = new List<SubAgentUser>();
        List<SubAgentUserLimitDto> subAgentUserList = new List<SubAgentUserLimitDto>();
        SubAgentInformation _currentSubagentInfo;

        public frmOutletCashInfoAll(Packet packet)
        {
            try
            {
                _packet = packet;
                InitializeComponent();
                GetSetupData();
                //controlActivity();
                ConfigUIEnhancement();
                SetControlAccessibility();
            }
            catch (Exception exp) { Message.showError(exp.Message); }
        }

        private void SetControlAccessibility()
        {
            try
            {
                bool isAdminUser = (SessionInfo.userBasicInformation.userType == UserType.Admin) ? true : false;

                if (SessionInfo.userBasicInformation.userCategory != UserCategory.BranchUser)
                {
                    if (SessionInfo.userBasicInformation.userCategory == UserCategory.AgentUser)
                    {
                        cmbAgentName.SelectedValue = UtilityServices.getCurrentAgent().id;
                        subAgentList = _agentServices.GetSubagentsByAgentId((long)cmbAgentName.SelectedValue);
                        setSubagent();
                        cmbAgentName.Enabled = false;
                    }
                    else if (SessionInfo.userBasicInformation.userCategory == UserCategory.SubAgentUser)
                    {
                        _currentSubagentInfo = UtilityServices.getCurrentSubAgent();
                        cmbAgentName.SelectedValue = _currentSubagentInfo.agent.id;
                        cmbAgentName_SelectedIndexChanged(null, null);
                        cmbAgentName.Enabled = false;

                        subAgentList = _agentServices.GetSubagentsByAgentId((long)cmbAgentName.SelectedValue);
                        setSubagent();
                        cmbSubAgnetName.SelectedValue = _currentSubagentInfo.id;
                        cmbSubAgnetName.Enabled = false;

                        //subAgentUserList = _outletServices.GetSubAgentUserListBySubAgentId(_currentSubagentInfo.id);
                        subAgentUserList = _outletServices.GetSubAgentUserListWithLimitBySubAgentId(_currentSubagentInfo.id);
                        setSubagentUserCombo();
                        cmbUser.SelectedValue = SessionInfo.userBasicInformation.userId;

                        if (!isAdminUser) cmbUser.Enabled = false;
                    }
                }
            }
            catch (Exception exp) { throw new Exception(exp.Message); }
        }

        private void setSubagentUserCombo()
        {
            if (subAgentUserList != null)
            {
                BindingSource bs = new BindingSource();
                bs.DataSource = subAgentUserList;

                try
                {
                    //SubAgentUser allUser = new SubAgentUser();
                    SubAgentUserLimitDto allUser = new SubAgentUserLimitDto();
                    allUser.userName = "Select";
                    subAgentUserList.Insert(0, allUser);

                    UtilityServices.fillComboBox(cmbUser, bs, "username", "id");
                    if (cmbUser.Items.Count > 0) cmbUser.SelectedIndex = 0;
                }
                catch(Exception exp)
                { throw new Exception("Couldn't load user. " + exp.Message); }
            }
        }

        public void ConfigUIEnhancement()
        {
            _gui = new GUI(this);
            _gui.Config(ref cmbAgentName, ValidCheck.VALIDATIONTYPES.COMBOBOX_EMPTY, null);
            _gui.Config(ref cmbSubAgnetName, ValidCheck.VALIDATIONTYPES.COMBOBOX_EMPTY, null);
            _gui.Config(ref dtpFromDate);
        }

        private void controlActivity()
        {
            try
            {
                if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_CENTRALLY.ToString())) //branch user
                {
                    cmbAgentName.Enabled = true;
                    cmbSubAgnetName.Enabled = true;
                }
                else if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_AGENTWISE.ToString())) //agent user
                {
                    cmbAgentName.SelectedValue = UtilityServices.getCurrentAgent().id;
                    cmbAgentName.Enabled = false;
                    cmbSubAgnetName.Enabled = true;
                    //_agentInformation = _agentServices.getAgentInfoById(UtilityServices.getCurrentAgent().id.ToString());
                    subAgentList = _agentServices.GetSubagentsByAgentId((long)cmbAgentName.SelectedValue);         // WALI :: 14-Jan-2016
                    setSubagent();
                }
                else
                {
                    SubAgentInformation currentSubagentInfo = UtilityServices.getCurrentSubAgent();
                    cmbAgentName.SelectedValue = currentSubagentInfo.agent.id;
                    //_agentInformation = _agentServices.getAgentInfoById(currentSubagentInfo.agent.id.ToString());
                    subAgentList = _agentServices.GetSubagentsByAgentId((long)cmbAgentName.SelectedValue);         // WALI :: 14-Jan-2016
                    setSubagent();
                    cmbAgentName.Enabled = false;
                    cmbSubAgnetName.SelectedValue = currentSubagentInfo.id;
                    cmbSubAgnetName.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void setSubagent()
        {
            #region Commented :: WALI :: 14-Jan-2016
            //if (_agentInformation != null)
            //{
            //    BindingSource bs = new BindingSource();
            //    bs.DataSource = _agentInformation.subAgents;

            //    {
            //        try
            //        {
            //            SubAgentInformation saiSelect = new SubAgentInformation();
            //            saiSelect.name = "(Select)";
            //            _agentInformation.subAgents.Insert(0, saiSelect);
            //        }
            //        catch //suppressed
            //        {

            //        }
            //    }
            //    UtilityServices.fillComboBox(cmbSubAgnetName, bs, "name", "id");
            //    if (cmbSubAgnetName.Items.Count > 0)
            //    {
            //        cmbSubAgnetName.SelectedIndex = 0;
            //    }
            //}
            #endregion

            if (subAgentList != null)
            {
                BindingSource bs = new BindingSource();
                bs.DataSource = subAgentList;

                try
                {
                    SubAgentInformation saiSelect = new SubAgentInformation();
                    saiSelect.name = "(Select)";
                    subAgentList.Insert(0, saiSelect);
                }
                catch //suppressed
                { }

                UtilityServices.fillComboBox(cmbSubAgnetName, bs, "name", "id");
                if (cmbSubAgnetName.Items.Count > 0)
                {
                    cmbSubAgnetName.SelectedIndex = 0;
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmOutletCashInfoAll_Load(object sender, EventArgs e)
        {
            dtpFromDate.Value = SessionInfo.currentDate;
        }

        private void GetSetupData()
        {
            try
            {
                setAgentList();
            }
            catch { }
        }

        private void setAgentList()
        {
            _objAgentInfoList = _objAgentServices.getAgentInfoBranchWise();
            BindingSource bs = new BindingSource();
            bs.DataSource = _objAgentInfoList;

            AgentInformation agSelect = new AgentInformation();
            agSelect.businessName = "(Select)";
            _objAgentInfoList.Insert(0, agSelect);

            UtilityServices.fillComboBox(cmbAgentName, bs, "businessName", "id");
            cmbAgentName.SelectedIndex = 0;
        }

        private void cmbAgentName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!cmbAgentName.Focused)
            {
                return;
            }

            try
            {
                if (cmbAgentName.SelectedIndex > 0)
                {
                    //_agentInformation = _agentServices.getAgentInfoById(cmbAgentName.SelectedValue.ToString());
                    subAgentList = _agentServices.GetSubagentsByAgentId((long)cmbAgentName.SelectedValue);         // WALI :: 14-Jan-2016
                }
                if (cmbAgentName.SelectedIndex < 1)
                {
                    cmbSubAgnetName.DataSource = null;
                    cmbSubAgnetName.Items.Clear();
                    return;
                }
            }

            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
            setSubagent();
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            if (cmbAgentName.SelectedIndex < 1)
            {
                Message.showError("Please select an Agent!");
                return;
            }
            if (cmbSubAgnetName.SelectedIndex < 1)
            {
                Message.showError("Please select an Outlet!");
                return;
            }
            if (dtpFromDate.Value > SessionInfo.currentDate)
            {
                Message.showError("Future date not allowed!");
                return;
            }

            Packet packet = new Packet();
            packet.actionType = FormActionType.View;
            packet.intentType = IntentType.Request;

            frmOutletCashInfo frm;
            OutletCashSumReqDto outletCashSumReqDto = new OutletCashSumReqDto();
            if (cmbUser.SelectedIndex == 0)
            {
                outletCashSumReqDto.outletId = (long)cmbSubAgnetName.SelectedValue;
                outletCashSumReqDto.informationDate = dtpFromDate.Value;

                frm = new frmOutletCashInfo(packet, outletCashSumReqDto, "outlet");
            }
            else
            {
                outletCashSumReqDto.outletId = (long)cmbUser.SelectedValue;
                outletCashSumReqDto.informationDate = dtpFromDate.Value;

                frm = new frmOutletCashInfo(packet, outletCashSumReqDto, "outletuser");
            }
            
            frm.ShowDialog();
        }

        private void frmOutletCashInfoAll_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void cmbSubAgnetName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!cmbSubAgnetName.Focused) return;

            try
            {
                if (cmbSubAgnetName.SelectedIndex > 0)
                {
                    //subAgentUserList = _outletServices.GetSubAgentUserListBySubAgentId((long)cmbSubAgnetName.SelectedValue);
                    subAgentUserList = _outletServices.GetSubAgentUserListWithLimitBySubAgentId((long)cmbSubAgnetName.SelectedValue);
                }
                if (cmbSubAgnetName.SelectedIndex < 1)
                {
                    cmbUser.DataSource = null;
                    cmbUser.Items.Clear();
                    return;
                }
            }

            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
            setSubagentUserCombo();
        }
    }
}