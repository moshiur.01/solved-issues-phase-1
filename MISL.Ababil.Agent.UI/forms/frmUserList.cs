﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MetroFramework.Forms;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.UI.forms.ProgressUI;
using MISL.Ababil.Agent.UI.forms.UserManagementUI;

namespace MISL.Ababil.Agent.UI.forms
{
    public partial class frmUserList : CustomForm
    {
        Packet _packet;
        AgentServices _agentServices = new AgentServices();
        SubAgentServices _subAgentServices = new SubAgentServices();
        List<UserTypeCombo> _userTypeComboList = new List<UserTypeCombo>();
        List<AgentUserType> userSubTypeList;

        List<AgentUserSearchResultDto> _searchResultList;
        UserService userService = new UserService();
        AgentUserSearchDto _searchDto = new AgentUserSearchDto();

        public frmUserList(Packet packet)
        {
            _packet = packet;

            InitializeComponent();

            SetupDataLoad();
        }
        public frmUserList()
        {
            _packet = new Packet();

            InitializeComponent();

            SetupDataLoad();
        }
        private void SetupDataLoad()
        {

            //_userTypeComboList.Add(new UserTypeCombo("Bank User"));
            //_userTypeComboList.Add(new UserTypeCombo("Agent User"));
            //_userTypeComboList.Add(new UserTypeCombo("Outlet User"));

            //BindingSource bs = new BindingSource();
            //bs.DataSource = _userTypeComboList;
            //UtilityServices.fillComboBox(cmbUserType, bs, "UserType", "UserType");

            cmbUserType.DataSource = Enum.GetValues(typeof(UserCategory));
            cmbUserType.SelectedIndex = -1;

            pnlUserSubType.Visible = pnlAgent.Visible = pnlSubAgent.Visible = false;
        }
        private void cmbUserType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmbUserType.SelectedItem != null)
                {
                    //if (cmbUserType.SelectedValue.ToString() == "Bank User") SetFormForBankUser();
                    //if (cmbUserType.SelectedValue.ToString() == "Agent User") SetFormForAgentUser();
                    //if (cmbUserType.SelectedValue.ToString() == "Outlet User") SetFormForOutletUser();

                    dgvUserList.Rows.Clear();
                    dgvUserList.Refresh();

                    if (cmbUserType.SelectedItem.ToString() == UserCategory.BranchUser.ToString())
                    {
                        gridColFullName.Width = 250 + columnTransfer.Width;
                        columnTransfer.Visible = false;
                        SetFormForBankUser();
                    }
                    else if (cmbUserType.SelectedItem.ToString() == UserCategory.AgentUser.ToString())
                    {
                        gridColFullName.Width = 250 + columnTransfer.Width;
                        columnTransfer.Visible = false;
                        SetFormForAgentUser();
                    }
                    else if (cmbUserType.SelectedItem.ToString() == UserCategory.SubAgentUser.ToString())
                    {
                        gridColFullName.Width = 250;
                        columnTransfer.Visible = true;
                        SetFormForOutletUser();
                    }

                    #region rejected
                    //switch ((AgentUserType)cmbUserType.SelectedItem)
                    //{
                    //    case AgentUserType.Outlet:
                    //        break;
                    //    case AgentUserType.Agent:
                    //        try
                    //        {
                    //            List<AgentInformation> objAgentInfoList = _agentServices.getAgentInfoBranchWise();
                    //            BindingSource bs = new BindingSource();
                    //            bs.DataSource = objAgentInfoList;
                    //            UtilityServices.fillComboBox(cmbAgent, bs, "businessName", "id");
                    //        }
                    //        catch (Exception ex)
                    //        { }
                    //        break;
                    //    case AgentUserType.Branch:
                    //        break;
                    //    case AgentUserType.Admin:
                    //        break;
                    //    case AgentUserType.Remittance:
                    //        break;
                    //    case AgentUserType.FieldOfficer:
                    //        break;
                    //    default:
                    //        break;
                    //}
                    #endregion
                }
            }
            catch (Exception exp)
            { Message.showError(exp.Message); }
        }
        private void cmbUserSubType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if ((UserCategory)cmbUserType.SelectedItem == UserCategory.BranchUser)   // Gets user list only for branch category.
                {
                    if (cmbUserSubType.Focused)
                    {
                        _searchDto = new AgentUserSearchDto();
                        _searchDto.userCategory = (UserCategory)cmbUserType.SelectedItem;
                        _searchDto.bankUserType = (BankUserType)cmbUserSubType.SelectedItem;

                        SearchUser(_searchDto);
                    }
                }
            }
            catch (Exception exp)
            { Message.showError(exp.Message); }
        }
        private void cmbAgent_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (cmbAgent.SelectedIndex <= 0)
                //{
                //    cmbOutlet.DataSource = null;
                //    return;
                //}
                //else 
                if ((UserCategory)cmbUserType.SelectedItem == UserCategory.SubAgentUser)
                {
                    #region Load SubAgent list
                    if (cmbAgent.Focused)
                    {
                        pnlSubAgent.Visible = true;

                        long agentId = 0;
                        try { agentId = ((AgentInformation)cmbAgent.SelectedValue).id; }
                        catch (Exception exp)
                        { agentId = long.Parse(cmbAgent.SelectedValue.ToString()); }

                        List<SubAgentInformation> subAgentInformationList = _agentServices.GetSubagentsByAgentId(agentId);

                        if (subAgentInformationList != null)
                        {
                            BindingSource bs = new BindingSource();
                            bs.DataSource = subAgentInformationList;
                            UtilityServices.fillComboBox(cmbOutlet, bs, "name", "id");
                        }
                        cmbOutlet.SelectedIndex = -1;
                    }
                    #endregion
                }
                else if ((UserCategory)cmbUserType.SelectedItem == UserCategory.AgentUser)   // Gets user list only for agent category.
                {
                    #region Load Agent Users
                    if (cmbAgent.Focused)
                    {
                        _searchDto = new AgentUserSearchDto();
                        _searchDto.userCategory = (UserCategory)cmbUserType.SelectedItem;
                        _searchDto.agentId = long.Parse(cmbAgent.SelectedValue.ToString());

                        SearchUser(_searchDto);
                    }
                    #endregion
                }
            }
            catch (Exception exp)
            { Message.showError("Couldn't load data.\n" + exp.Message); }
        }
        private void cmbOutlet_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                dgvUserList.Rows.Clear();
                dgvUserList.Refresh();

                if (cmbOutlet.SelectedIndex == -1) return;
                else if (cmbOutlet.Focused)
                {
                    _searchDto = new AgentUserSearchDto();
                    _searchDto.userCategory = (UserCategory)cmbUserType.SelectedItem;
                    _searchDto.subAgentId = long.Parse(cmbOutlet.SelectedValue.ToString());

                    SearchUser(_searchDto);
                }
            }
            catch (Exception exp)
            { Message.showError("Couldn't load data.\n" + exp.Message); }
        }
        private void SearchUser(AgentUserSearchDto searchDto)
        {
            try
            {
                dgvUserList.Rows.Clear();
                dgvUserList.Refresh();

                ServiceResult serviceResult = ServiceResult.CreateServiceResult();
                serviceResult = userService.GetUserList(searchDto);

                if (!serviceResult.Success)
                {
                    MessageBox.Show(serviceResult.Message);
                    return;
                }

                _searchResultList = serviceResult.ReturnedObject as List<AgentUserSearchResultDto>;

                int rowIndex = 0;
                foreach (AgentUserSearchResultDto user in _searchResultList)
                {
                    dgvUserList.Rows.Add();
                    dgvUserList["gridColUserName", rowIndex].Value = user.userName;
                    dgvUserList["gridColFullName", rowIndex].Value = user.fullName;
                    dgvUserList["gridColUserType", rowIndex].Value = user.userType;
                    dgvUserList["gridColMobileNo", rowIndex].Value = user.mobileNumber;
                    dgvUserList["gridColUserStatus", rowIndex].Value = user.userStatus;
                    dgvUserList["gridColUserId", rowIndex].Value = user.userId;
                    dgvUserList["gridColIndividualId", rowIndex].Value = user.individualId;
                    dgvUserList["grdColShowDetails", rowIndex].Value = "View";
                    dgvUserList["gridColFilter", rowIndex].Value = user.userName+ user.fullName+ user.userType + user.mobileNumber+ user.userStatus;
                    rowIndex++;
                }

                dgvUserList.Columns[0].DefaultCellStyle.Alignment = dgvUserList.Columns[1].DefaultCellStyle.Alignment = dgvUserList.Columns[2].DefaultCellStyle.Alignment =
                    dgvUserList.Columns[3].DefaultCellStyle.Alignment = dgvUserList.Columns[4].DefaultCellStyle.Alignment = dgvUserList.Columns[5].DefaultCellStyle.Alignment =
                    dgvUserList.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                dgvUserList.Columns[0].HeaderCell.Style.Alignment = dgvUserList.Columns[1].HeaderCell.Style.Alignment = dgvUserList.Columns[2].HeaderCell.Style.Alignment =
                    dgvUserList.Columns[3].HeaderCell.Style.Alignment = dgvUserList.Columns[4].HeaderCell.Style.Alignment = dgvUserList.Columns[5].HeaderCell.Style.Alignment =
                    dgvUserList.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
            catch (Exception exp)
            { throw new Exception(exp.Message); }
        }

        private void SetFormForBankUser()
        {
            try
            {
                pnlAgent.Visible = pnlSubAgent.Visible = false;
                pnlUserSubType.Visible = true;
                pnlUserSubType.SendToBack();
                //btnAddAgentUser.Visible = btnAddSubAgentUser.Visible = false;
                //btnAddBranchUser.Visible = true;

                cmbUserSubType.DataSource = Enum.GetValues(typeof(BankUserType));
                cmbUserSubType.Focus();
                cmbUserSubType_SelectedIndexChanged(null, null);
            }
            catch (Exception exp)
            { throw new Exception("Couldn't load UserSubType.\n" + exp.Message); }
        }
        private void SetFormForAgentUser()
        {
            try
            {
                pnlSubAgent.Visible = false;
                pnlUserSubType.Visible = false;
                pnlAgent.Visible = true;
                pnlAgent.SendToBack();
                btnAddAgentUser.Visible = true;
                //btnAddSubAgentUser.Visible = btnAddBranchUser.Visible = false;
                //btnAddAgentUser.Visible = true;
                BindingSource bs = new BindingSource();

                #region User SubType
                userSubTypeList = new List<AgentUserType>();
                {
                    userSubTypeList.Add(AgentUserType.Agent);
                }
                cmbUserSubType.DataSource = userSubTypeList;

                #endregion

                #region Agent List                
                List<AgentInformation> objAgentInfoList = _agentServices.getAgentInfoBranchWise();
                bs = new BindingSource();
                bs.DataSource = objAgentInfoList;
                UtilityServices.fillComboBox(cmbAgent, bs, "businessName", "id");
                cmbAgent.SelectedIndex = -1;
                #endregion
            }
            catch (Exception exp)
            { throw new Exception("Couldn't load Agents.\n" + exp.Message); }
        }

        private void SetFormForOutletUser()
        {
            try
            {
                pnlUserSubType.Visible = false;
                btnAddAgentUser.Visible = false;
                pnlAgent.Visible = true;
                pnlAgent.SendToBack();
                pnlSubAgent.Visible = true;
                pnlSubAgent.SendToBack();

                //btnAddBranchUser.Visible = btnAddAgentUser.Visible = false;
                //btnAddSubAgentUser.Visible = true;

                BindingSource bs = new BindingSource();
                #region User SubType
                userSubTypeList = new List<AgentUserType>();
                {
                    userSubTypeList.Add(AgentUserType.Outlet);
                }
                cmbUserSubType.DataSource = userSubTypeList;

                #endregion

                #region Agent List
                List<AgentInformation> objAgentInfoList = _agentServices.getAgentInfoBranchWise();
                bs = new BindingSource();
                bs.DataSource = objAgentInfoList;
                UtilityServices.fillComboBox(cmbAgent, bs, "businessName", "id");
                cmbAgent.SelectedIndex = -1;
                #endregion
            }
            catch (Exception exp)
            { throw new Exception("Couldn't load Agents.\n" + exp.Message); }
        }

        private void btnAddBranchUser_Click(object sender, EventArgs e)
        {
            if (_searchDto == null || cmbUserSubType.SelectedValue == null)
            {
                MsgBox.showWarning("Please select correct information!");
                return;
            }
            _packet.actionType = FormActionType.New;
            BankUser user = new BankUser();
            frmUserCreationWithPersonalInfo frm = new frmUserCreationWithPersonalInfo(_packet, (BankUserType)cmbUserSubType.SelectedValue, UserCategory.BranchUser, user);
            frm.ShowDialog();
            SearchUser(_searchDto);
        }
        private void btnAddAgentUser_Click(object sender, EventArgs e)
        {
            if (cmbAgent.SelectedIndex > -1)
            {
                _packet.actionType = FormActionType.New;
                AgentUser user = new AgentUser();
                //user.agentInformation = (AgentInformation)cmbAgent.SelectedItem;
                user.agentInformation = new AgentInformation();
                user.agentInformation.id = long.Parse(cmbAgent.SelectedValue.ToString());
                frmUserCreationWithPersonalInfo frm = new frmUserCreationWithPersonalInfo(_packet, (BankUserType)cmbUserSubType.SelectedValue, UserCategory.AgentUser, user);
                frm.ShowDialog();
                SearchUser(_searchDto);
            }
            else
            {
                Message.showWarning("Please select an agent !");
                return;
            }
        }
        private void btnAddSubAgentUser_Click(object sender, EventArgs e)
        {
            if (cmbUserSubType.SelectedValue == null)
            {
                MsgBox.showWarning("Please select correct information!");
                return;
            }
            if (cmbAgent.SelectedIndex > -1)
            {
                if (cmbOutlet.SelectedIndex > -1)
                {
                    _packet.actionType = FormActionType.New;
                    SubAgentUser user = new SubAgentUser();
                    user.subAgentInformation = new SubAgentInformation();
                    user.subAgentInformation.id = long.Parse(cmbOutlet.SelectedValue.ToString());

                    frmUserCreationWithPersonalInfo frm = new frmUserCreationWithPersonalInfo(_packet, (BankUserType)cmbUserSubType.SelectedValue, UserCategory.SubAgentUser, user);
                    frm.ShowDialog();
                    SearchUser(_searchDto);
                }
                else
                {
                    Message.showWarning("Please select an outlet !");
                    return;
                }
            }
            else
            {
                Message.showWarning("Please select agent and outlet !");
                return;
            }
        }

        private void dgvUserList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 5) // View Link
                {
                    ShowUserDetails((UserCategory)cmbUserType.SelectedItem, _searchResultList[e.RowIndex].userId,
                        _searchResultList[e.RowIndex].individualId);
                    SearchUser(_searchDto);
                }
            }
            catch (Exception ex)
            {
                ProgressUIManager.CloseProgress();
                Message.showError(ex.Message);
            }

            try
            {
                if (e.ColumnIndex == 8) // Transfer Link
                {
                    ShowUserDetails((UserCategory)cmbUserType.SelectedItem, _searchResultList[e.RowIndex].userId,
                      _searchResultList[e.RowIndex].individualId, true);
                    SearchUser(_searchDto);
                }
            }
            catch (Exception ex)
            {
                ProgressUIManager.CloseProgress();
                Message.showError(ex.Message);
            }
        }

        private void dgvUserList_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ShowUserDetails((UserCategory)cmbUserType.SelectedItem, _searchResultList[e.RowIndex].userId, _searchResultList[e.RowIndex].individualId);
                SearchUser(_searchDto);
            }
            catch (Exception exp)
            { ProgressUIManager.CloseProgress(); Message.showError(exp.Message); }
        }

        private void ShowUserDetails(UserCategory userCategory, long userId, long individualId, bool isTransfer = false)
        {
            AgentUserDetilSearchDto userSearchDto = new AgentUserDetilSearchDto
            {
                userCategory = userCategory,
                userId = userId,
                individualId = individualId
            };

            ServiceResult serviceResult = ServiceResult.CreateServiceResult();
            ProgressUIManager.ShowProgress(this);
            serviceResult = userService.GetUserDetailsByUserId(userSearchDto);
            ProgressUIManager.CloseProgress();

            if (!serviceResult.Success)
            {
                MessageBox.Show(serviceResult.Message);
                return;
            }

            AgentUserDto userDetails = serviceResult.ReturnedObject as AgentUserDto;
            if (userCategory == UserCategory.BranchUser)
            {
                /*userDetails.bankUser = new BankUser(); */
            }
            else if (userCategory == UserCategory.AgentUser)
            {
                userDetails.agentUser.agentInformation = new AgentInformation
                {
                    id = long.Parse(cmbAgent.SelectedValue.ToString())
                };
            }
            else if (userCategory == UserCategory.SubAgentUser)
            {
                userDetails.subAgentUser.subAgentInformation = new SubAgentInformation
                {
                    id = long.Parse(cmbOutlet.SelectedValue.ToString())
                };
            }

            Packet packet = new Packet
            {
                actionType = FormActionType.Edit,
                intentType = IntentType.Request
            };

            if (isTransfer == false)
            {
                new frmUserCreationWithPersonalInfo(packet, userDetails).ShowDialog(this);
            }
            else
            {
                new frmSubagentTransfer(new Packet()
                {
                    actionType = FormActionType.Edit

                }, userDetails, ((SubAgentInformation)cmbOutlet.SelectedItem).name, ((AgentInformation)cmbAgent.SelectedItem).businessName).ShowDialog(this);
            }
        }

        private void frmUserList_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }

    internal class UserTypeCombo
    {
        public string userCategory { get; set; }

        public UserTypeCombo(string category)
        { userCategory = category; }
    }
}
