﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.consumer;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Infrastructure.Validation;
using MISL.Ababil.Agent.Module.Common.UI.ProgressUI;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.UI.forms.ProgressUI;

namespace MISL.Ababil.Agent.UI.forms
{
    public partial class frmCustomerMobileNoUpdate : CustomForm
    {
        private ConsumerServices _consumerService = new ConsumerServices();
        private ConsumerInformationDto _consumerInformationDto = new ConsumerInformationDto();
        private string _captureFor;
        private int _captureIndexNo;
        private int _noOfCapturefinger = 0;
        private byte[] _capturefingerData;
        private byte[] _subagentFingerData;
        private bool? _isManualChargeApplicable = null;
        private decimal _chargeAmount;
        private AccountOperatorDto _accountOperatorDto;

        public frmCustomerMobileNoUpdate()
        {
            InitializeComponent();
            btnMobileNoUpdate.Enabled = false;
            lblConsumerTitle.Text = "";
            lblMobileNo.Text = "";

        }

        //__________________Quick_Fix_________________
        private int c = 0;
        private void txtConsumerAccount_Leave(object sender, EventArgs e)
        {
            if (c++ >= 1)
            {
                c = 0;
                return;
            }
            if (string.IsNullOrEmpty(txtConsumerAccount.Text))
            {
                return;
            }
            btnMobileNoUpdate.Enabled = true;
            lblMobileNo.Focus();
            if (txtConsumerAccount.Text.IndexOf("990") == 0)
            {
                LoadConsumerInformation();
            }
            else
            {
                MsgBox.showWarning("Please enter an agent banking Account No.");
                ClearControls();
                txtConsumerAccount.Focus();
            }


        }

        private void LoadConsumerInformation()
        {
            if (ValidationManager.ValidateNonEmptyTextWithoutSpace(txtConsumerAccount.Text))
            {
                try
                {
                    _consumerInformationDto = _consumerService.getConsumerInformationDtoByAcc(txtConsumerAccount.Text);
                    lblConsumerTitle.Text = _consumerInformationDto.consumerTitle;
                    lblMobileNo.Text = _consumerInformationDto.mobileNumber;
                    //lblBalanceValue.Text = (_consumerInformationDto.balance ?? 0).ToString("N", new CultureInfo("BN-BD"));

                    if (_consumerInformationDto.photo != null)
                    {
                        byte[] bytes = Convert.FromBase64String(_consumerInformationDto.photo);
                        Image image = UtilityServices.byteArrayToImage(bytes);
                        pic_conusmer.Image = image;
                    }

                    //if (_consumerInformationDto.accountOperators != null)
                    //{
                    //    //fingerPrintGrid.DataSource = _consumerInformationDto.accountOperators.Select(o => new operatorfingerPrintGrid(o) { identity = o.identity, identityName = o.identityName }).ToList();
                    //}

                    LoadOperators(txtConsumerAccount.Text);
                }
                catch (Exception ex)
                {
                    MsgBox.ShowError(ex.Message);
                    //lblConsumerTitle.Text = "";
                    //lblMobileNo.Text = "";
                    //pic_conusmer.Image = null;
                    //fingerPrintGrid.DataSource = null;
                    //lblRequiredFingerPrint.Text = null;
                    ClearControls();
                }
            }
        }

        private void LoadOperators(string accountNumber)
        {
            //_accountOperatorDto = new AccountInformationService().GetDepositAccountOperators(accountNumber);
            //fingerPrintGrid.Rows.Clear();
            //for (int i = 0; i < _accountOperatorDto.operators.Count; i++)
            //{
            //    fingerPrintGrid.Rows.Add(_accountOperatorDto.operators[i].individualId, _accountOperatorDto[i].operatorName, _accountOperatorDto[i].mobileNo, "View");
            //}
            ////GetAccountOperatorDtoByAccountNumber
            _accountOperatorDto = new AccountInformationService().GetAccountOperatorDtoByAccountNumber(accountNumber);
            fingerPrintGrid.Rows.Clear();
            for (int i = 0; i < _accountOperatorDto.operators.Count; i++)
            {
                fingerPrintGrid.Rows.Add
                    (
                        _accountOperatorDto.operators[i].individualId,
                        _accountOperatorDto.operators[i].individualName,
                        _accountOperatorDto.operators[i].mobileNo,
                        "View"
                    );
            }
        }

        private void frmDepositAccountOperator_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void fingerPrintGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.ColumnIndex == 3) //View
            {
                new frmIndividualInformation
                (
                    new Packet()
                    {
                        actionType = FormActionType.View
                    }, new IndividualServices().GetIndividualInfo
                        (
                            int.Parse(fingerPrintGrid.Rows[e.RowIndex].Cells[0].Value.ToString())
                        )
                ).ShowDialog(this);
            }
        }

        private void btnMobileNoUpdate_Click(object sender, EventArgs e)
        {
            if (MsgBox.showConfirmation("Are you sure to change Mobile No.?") == "yes")
            {
                if (CustomControlValidation.IsAllValid(true, true, txtConsumerAccount, lblMobileNo) == true)
                {
                    try
                    {

                        if (string.IsNullOrEmpty(txtReamarks.Text))
                        {
                            MsgBox.showWarning("Please insert a Reason,");
                            return;
                        }
                        MobileNoChangeRequestDto mobileNoChangeRequestDto = new MobileNoChangeRequestDto();
                        mobileNoChangeRequestDto.accountNo = txtConsumerAccount.Text;
                        mobileNoChangeRequestDto.customerMobileNo = lblMobileNo.Text;
                        mobileNoChangeRequestDto.remark = txtReamarks.Text;
                        List<IndividualInfoDto> individualInfoDtos = new List<IndividualInfoDto>();
                        for (int i = 0; i < fingerPrintGrid.RowCount; i++)
                        {
                            IndividualInfoDto individualInfoDto = new IndividualInfoDto();
                            individualInfoDto.individualId = (long)fingerPrintGrid.Rows[i].Cells[0].Value;
                            individualInfoDto.name = fingerPrintGrid.Rows[i].Cells[1].Value.ToString();
                            individualInfoDto.mobileNo = fingerPrintGrid.Rows[i].Cells[2].Value.ToString();
                            individualInfoDtos.Add(individualInfoDto);

                        }
                        mobileNoChangeRequestDto.individualInfoDtos = individualInfoDtos;
                        CustomerMobileNoUpdateService mobileNumberUpdate = new CustomerMobileNoUpdateService();
                        ProgressUIManager.ShowProgress(this);
                        mobileNumberUpdate.cusMobNoUpdate(mobileNoChangeRequestDto);
                        ProgressUIManager.CloseProgress();
                        MsgBox.showInformation("Updated successfully.");
                        ClearControls();
                    }
                    catch (Exception ex)
                    {
                        ProgressUIManager.CloseProgress();
                        MsgBox.ShowError(ex.Message);
                    }
                }
            }
        }

        private void btnclear_Click(object sender, EventArgs e)
        {
            ClearControls();
        }

        private void ClearControls()
        {
            txtConsumerAccount.Text = lblConsumerTitle.Text = lblMobileNo.Text = txtReamarks.Text = string.Empty;
            fingerPrintGrid.DataSource = null;
            pic_conusmer.Image = null;
            fingerPrintGrid.Rows.Clear();
        }

        private void fingerPrintGrid_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex != 2) return;
            string formattedValue = e.FormattedValue.ToString();
            if (!ValidationManager.ValidatePositive(formattedValue))
            {
                e.Cancel = true;
                MsgBox.showWarning(StringTable.Numeric_value_is_expected);
            }
        }
    }
}