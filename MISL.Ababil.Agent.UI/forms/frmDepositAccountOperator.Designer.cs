﻿namespace MISL.Ababil.Agent.UI.forms
{
    partial class frmDepositAccountOperator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.txtConsumerAccount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblMobileNo = new System.Windows.Forms.Label();
            this.lblConsumerTitle = new System.Windows.Forms.Label();
            this.photo = new System.Windows.Forms.GroupBox();
            this.pic_conusmer = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblRequiredFingerPrint = new System.Windows.Forms.Label();
            this.fingerPrintGrid = new System.Windows.Forms.DataGridView();
            this.grdColIndividualId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnCol = new System.Windows.Forms.DataGridViewButtonColumn();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.photo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_conusmer)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fingerPrintGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(58, 129);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label4.Size = new System.Drawing.Size(76, 16);
            this.label4.TabIndex = 12;
            this.label4.Text = "Mobile No :";
            // 
            // txtConsumerAccount
            // 
            this.txtConsumerAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConsumerAccount.Location = new System.Drawing.Point(142, 57);
            this.txtConsumerAccount.MaxLength = 13;
            this.txtConsumerAccount.Name = "txtConsumerAccount";
            this.txtConsumerAccount.Size = new System.Drawing.Size(296, 38);
            this.txtConsumerAccount.TabIndex = 9;
            this.txtConsumerAccount.Leave += new System.EventHandler(this.txtConsumerAccount_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(31, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 16);
            this.label3.TabIndex = 10;
            this.label3.Text = "Account Name :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 25);
            this.label1.TabIndex = 8;
            this.label1.Text = "Account :";
            // 
            // lblMobileNo
            // 
            this.lblMobileNo.AutoSize = true;
            this.lblMobileNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMobileNo.Location = new System.Drawing.Point(142, 129);
            this.lblMobileNo.Name = "lblMobileNo";
            this.lblMobileNo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblMobileNo.Size = new System.Drawing.Size(85, 16);
            this.lblMobileNo.TabIndex = 13;
            this.lblMobileNo.Text = "lbvMobileNo";
            // 
            // lblConsumerTitle
            // 
            this.lblConsumerTitle.AutoSize = true;
            this.lblConsumerTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConsumerTitle.Location = new System.Drawing.Point(142, 104);
            this.lblConsumerTitle.Name = "lblConsumerTitle";
            this.lblConsumerTitle.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblConsumerTitle.Size = new System.Drawing.Size(113, 16);
            this.lblConsumerTitle.TabIndex = 11;
            this.lblConsumerTitle.Text = "lbvConsumerTitle";
            this.lblConsumerTitle.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // photo
            // 
            this.photo.Controls.Add(this.pic_conusmer);
            this.photo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.photo.Location = new System.Drawing.Point(453, 34);
            this.photo.Name = "photo";
            this.photo.Size = new System.Drawing.Size(135, 162);
            this.photo.TabIndex = 16;
            this.photo.TabStop = false;
            this.photo.Text = "Photo";
            // 
            // pic_conusmer
            // 
            this.pic_conusmer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_conusmer.Location = new System.Drawing.Point(10, 23);
            this.pic_conusmer.Name = "pic_conusmer";
            this.pic_conusmer.Size = new System.Drawing.Size(116, 130);
            this.pic_conusmer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_conusmer.TabIndex = 5;
            this.pic_conusmer.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblRequiredFingerPrint);
            this.groupBox2.Controls.Add(this.fingerPrintGrid);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 203);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(576, 167);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Finger Print Information";
            // 
            // lblRequiredFingerPrint
            // 
            this.lblRequiredFingerPrint.AutoSize = true;
            this.lblRequiredFingerPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequiredFingerPrint.Location = new System.Drawing.Point(378, 12);
            this.lblRequiredFingerPrint.Name = "lblRequiredFingerPrint";
            this.lblRequiredFingerPrint.Size = new System.Drawing.Size(0, 15);
            this.lblRequiredFingerPrint.TabIndex = 1;
            // 
            // fingerPrintGrid
            // 
            this.fingerPrintGrid.AllowUserToAddRows = false;
            this.fingerPrintGrid.AllowUserToDeleteRows = false;
            this.fingerPrintGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fingerPrintGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.fingerPrintGrid.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.fingerPrintGrid.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.fingerPrintGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.fingerPrintGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.grdColIndividualId,
            this.Column2,
            this.btnCol});
            this.fingerPrintGrid.Location = new System.Drawing.Point(12, 29);
            this.fingerPrintGrid.Name = "fingerPrintGrid";
            this.fingerPrintGrid.ReadOnly = true;
            this.fingerPrintGrid.RowHeadersVisible = false;
            this.fingerPrintGrid.Size = new System.Drawing.Size(553, 125);
            this.fingerPrintGrid.TabIndex = 0;
            this.fingerPrintGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.fingerPrintGrid_CellContentClick);
            // 
            // grdColIndividualId
            // 
            this.grdColIndividualId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.grdColIndividualId.FillWeight = 162.4366F;
            this.grdColIndividualId.HeaderText = "Individual ID";
            this.grdColIndividualId.Name = "grdColIndividualId";
            this.grdColIndividualId.ReadOnly = true;
            this.grdColIndividualId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.grdColIndividualId.Width = 160;
            // 
            // Column2
            // 
            this.Column2.FillWeight = 37.56345F;
            this.Column2.HeaderText = "Operator Name";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // btnCol
            // 
            this.btnCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.btnCol.HeaderText = "";
            this.btnCol.Name = "btnCol";
            this.btnCol.ReadOnly = true;
            this.btnCol.Text = "View";
            this.btnCol.Width = 80;
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(600, 26);
            this.customTitlebar1.TabIndex = 17;
            this.customTitlebar1.TabStop = false;
            // 
            // frmDepositAccountOperator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(600, 382);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.photo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtConsumerAccount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblMobileNo);
            this.Controls.Add(this.lblConsumerTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDepositAccountOperator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Account Operator Information";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDepositAccountOperator_FormClosing);
            this.photo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_conusmer)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fingerPrintGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtConsumerAccount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMobileNo;
        private System.Windows.Forms.Label lblConsumerTitle;
        private System.Windows.Forms.GroupBox photo;
        private System.Windows.Forms.PictureBox pic_conusmer;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblRequiredFingerPrint;
        private System.Windows.Forms.DataGridView fingerPrintGrid;
        private Agent.CustomControls.CustomTitlebar customTitlebar1;
        private System.Windows.Forms.DataGridViewButtonColumn btnCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdColIndividualId;
    }
}