﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using MISL.Ababil.Agent.Report.DataSets;
using MISL.Ababil.Agent.Report.Reports;
using MISL.Ababil.Agent.Services;

namespace MISL.Ababil.Agent.UI.forms
{
    public partial class frmChargeCommission : Form
    {
        List<AgentInformation> objAgentInfoList = new List<AgentInformation>();
        AgentServices agentServices = new AgentServices();
        List<CommissionTypeDto> _commissionTypes = new List<CommissionTypeDto>();

        private List<AgentChargeSearchResultDto> _agentChargeSearchResultDto;
        private List<AgentChargeTotalGrid> _agentIncomeTotal;
        List<AgentIncomeStatementDto> _agentIncomeStatList = new List<AgentIncomeStatementDto>();
        private bool IsSearch = true;
        private AgentChargeDS agentChargeDs = new AgentChargeDS();
        public frmChargeCommission()
        {
            InitializeComponent();
            preparedUI();
        }

        private void preparedUI()
        {
            try
            {
                SetupDataLoad();
                SetupComponents();
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp.Message);
            }
        }

        private void SetupDataLoad()
        {
            try
            {
                dtpChargeDateFrom.Value = new DateTime(SessionInfo.currentDate.Year, SessionInfo.currentDate.Month, 1);
                dtpChargeDateTo.Value = SessionInfo.currentDate;

                #region Load posted status                
                //List<PostedStatusComboExt> statusList = Enum.GetValues(typeof(PostedStatus)).Cast<PostedStatus>()
                //                                                                         .Select(o => new PostedStatusComboExt
                //                                                                         {
                //                                                                             postedStatus = o.ToString(),
                //                                                                         }).ToList();
                #endregion

                #region Load agent list
                objAgentInfoList = agentServices.getAgentInfoBranchWise();
                BindingSource bsAgent = new BindingSource();
                bsAgent.DataSource = objAgentInfoList;

                //AgentInformation agSelect = new AgentInformation();
                //agSelect.businessName = "(Select)";
                //objAgentInfoList.Insert(0, agSelect);
                AgentInformation agAll = new AgentInformation();
                agAll.businessName = "(All)";
                agAll.id = 0;
                objAgentInfoList.Insert(0, agAll);

                UtilityServices.fillComboBox(cmbAgent, bsAgent, "businessName", "id");
                cmbAgent.SelectedIndex = 0;
                #endregion

                #region Load Commission Types
                //_commissionTypes = TransactionService.getCommissionTypes();
                //BindingSource bs = new BindingSource();
                //bs.DataSource = _commissionTypes;
                //UtilityServices.FillComboBox(cmbIncomeFrom, bs, "commissionName", "id");
                #endregion
            }
            catch (Exception exp)
            { throw exp; }
        }

        private void SetupComponents()
        {
            try
            {
                if (SessionInfo.userBasicInformation.userCategory == UserCategory.AgentUser)
                {
                    cmbAgent.SelectedValue = UtilityServices.getCurrentAgent().id; ;
                    cmbAgent.Enabled = false;
                    LoadSubagents();
                }
                else if (SessionInfo.userBasicInformation.userCategory == UserCategory.SubAgentUser)
                {
                    SubAgentInformation currentSubagentInfo = UtilityServices.getCurrentSubAgent();
                    cmbAgent.SelectedValue = currentSubagentInfo.agent.id;
                    cmbAgent.Enabled = false;

                    BindingSource bs = new BindingSource();
                    bs.DataSource = currentSubagentInfo;
                    UtilityServices.fillComboBox(cmbOutlet, bs, "name", "id");
                    cmbOutlet.Enabled = false;

                    //btnSearch_Click(null, null);
                    SearchExt();
                }
            }
            catch (Exception ex)
            { throw; }
        }

        private void frmChargeCommission_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        [DllImport("dwmapi.dll")]
        public static extern int DwmExtendFrameIntoClientArea(IntPtr hWnd, ref Margins pMarInset);

        [DllImport("dwmapi.dll")]
        public static extern int DwmSetWindowAttribute(IntPtr hwnd, int attr, ref int attrValue, int attrSize);

        [DllImport("dwmapi.dll")]
        public static extern int DwmIsCompositionEnabled(ref int pfEnabled);

        private bool _mAeroEnabled;
        private const int CsDropshadow = 0x00020000;
        private const int WmNcpaint = 0x0085;
        //private const int WmActivateapp = 0x001C;

        public struct Margins
        {
            public int LeftWidth;
            public int RightWidth;
            public int TopHeight;
            public int BottomHeight;
        }

        private const int WmNchittest = 0x84;
        private const int Htclient = 0x1;
        private const int Htcaption = 0x2;

        protected override CreateParams CreateParams
        {
            get
            {
                _mAeroEnabled = CheckAeroEnabled();
                CreateParams cp = base.CreateParams;
                if (!_mAeroEnabled)
                {
                    cp.ClassStyle |= CsDropshadow;
                }
                return cp;
            }
        }

        private bool CheckAeroEnabled()
        {
            if (Environment.OSVersion.Version.Major >= 6)
            {
                int enabled = 0;
                DwmIsCompositionEnabled(ref enabled);
                return (enabled == 1) ? true : false;
            }
            return false;
        }

        //protected override void WndProc(ref Message m)
        //{
        //    switch (m.Msg)
        //    {
        //        case WmNcpaint:
        //            if (_mAeroEnabled)
        //            {
        //                var v = 2;
        //                DwmSetWindowAttribute(this.Handle, 2, ref v, 4);
        //                Margins margins = new Margins()
        //                {
        //                    BottomHeight = 1,
        //                    LeftWidth = 0,
        //                    RightWidth = 0,
        //                    TopHeight = 0
        //                };
        //                DwmExtendFrameIntoClientArea(this.Handle, ref margins);
        //            }
        //            break;
        //        default:
        //            break;
        //    }
        //    base.WndProc(ref m);

        //    if (m.Msg == WmNchittest && (int)m.Result == Htclient)
        //    {
        //        m.Result = (IntPtr)Htcaption;
        //    }
        //}

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                IsSearch = true;
                lblFilter.Text = "";
                Search(false);
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp.Message);
            }
        }

        private void SearchExt()
        {
            try
            {
                IsSearch = true;
                lblFilter.Text = "";
                Search(true);
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp.Message);
            }
        }

        private void Search(bool dateTime)
        {
            try
            {
                dgvResult.DataSource = null;
                dgvResult.Refresh();
                dgvResult.Rows.Clear();
                dgvResult.Columns.Clear();
                AgentChargeSearchDto AgentChargeSearchDto = new AgentChargeSearchDto();

                if (cmbAgent.SelectedValue != null)
                { AgentChargeSearchDto.agentId = (long)cmbAgent.SelectedValue; }
                else
                { AgentChargeSearchDto.agentId = 0; }

                if (cmbOutlet.SelectedValue != null)
                { AgentChargeSearchDto.outletId = (long)cmbOutlet.SelectedValue; }
                else
                { AgentChargeSearchDto.outletId = 0; }
                //AgentChargeSearchDto.outletId = (long)(cmbOutlet.SelectedValue ?? 0);

                if (dateTime == false)
                {
                    AgentChargeSearchDto.chargeDateFrom =
                        UtilityServices.GetLongDate(new DateTime(dtpChargeDateFrom.Value.Year,
                            dtpChargeDateFrom.Value.Month,
                            dtpChargeDateFrom.Value.Day));
                }
                else
                {
                    AgentChargeSearchDto.chargeDateFrom =
                        UtilityServices.GetLongDate(new DateTime(SessionInfo.currentDate.Year, SessionInfo.currentDate.Month, SessionInfo.currentDate.Day));
                }

                AgentChargeSearchDto.chargeDateTo = UtilityServices.GetLongDate(new DateTime(dtpChargeDateTo.Value.Year,
                                                                                             dtpChargeDateTo.Value.Month,
                                                                                             dtpChargeDateTo.Value.Day));

                _agentChargeSearchResultDto = (new AgentChargeService()).GetAgentIncomeSearchResultList(AgentChargeSearchDto);
                LoadItems(_agentChargeSearchResultDto);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        private void LoadItems(List<AgentChargeSearchResultDto> agentIncomeSearchResultDto)
        {
            try
            {
                dgvResult.DataSource = agentIncomeSearchResultDto.Select(o => new AgentChargeSearchResultDtoGrid(o)
                {
                    CollectionDate = UtilityServices.getDateFromLong(o.chargeDate ?? 0).ToString("dd-MMM-yyyy"),
                    //IncomeDate = o.chargeDate.ToString("dd-MMM-yyyy"),
                    ChargeFrom = o.chargeFrom.ToString(),
                    //Amount = (o.amount ?? 0),
                    //Amount = string.Format("{0:F2}", (o.amount ?? 0)),
                    Amount = decimal.Parse(string.Format("{0:F2}", (o.amount ?? 0)), NumberStyles.AllowDecimalPoint),
                    Outlet = o.outletName,
                    User = o.userName
                }).ToList();
                lblResultCount.Text = "   Result : " + dgvResult.Rows.Count + " Item(s)";
                ReCalculateGridWidth();
                CalculateTotal(agentIncomeSearchResultDto);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        private void ReCalculateGridWidth()
        {
            dgvResult.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgvResult.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvResult.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvResult.Columns[2].Width = 100;
        }

        private void CalculateTotal(List<AgentChargeSearchResultDto> agentChargeSearchResultDto)
        {
            if (IsSearch == true)
            {
                SortedDictionary<string, decimal> totalList = new SortedDictionary<string, decimal>();

                string incFrom = "";
                for (int i = 0; i < dgvResult.Rows.Count; i++)
                {
                    incFrom = dgvResult.Rows[i].Cells[1].Value.ToString();
                    if (totalList.ContainsKey(incFrom))
                    {
                        totalList[incFrom] = totalList[incFrom] + (decimal)dgvResult.Rows[i].Cells[2].Value;
                    }
                    else
                    {
                        totalList.Add(incFrom, (decimal)dgvResult.Rows[i].Cells[2].Value);
                    }
                }

                _agentIncomeTotal = totalList.Select(o => new AgentChargeTotalGrid(o)
                {
                    ChargeForm = o.Key,
                    Amount = o.Value

                }).ToList();

                //dgvTotal.DataSource = totalList.Select(o => new AgentChargeTotalGrid(o)
                //{
                //    IncomeForm = o.Key,
                //    Amount = o.Value

                //}).ToList();                
                dgvTotal.DataSource = _agentIncomeTotal;
                dgvTotal.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvTotal.Columns[1].Width = 130;

                dgvTotal.DefaultCellStyle.Font = new Font(llblBalance.Font.FontFamily, dgvTotal.DefaultCellStyle.Font.Size, FontStyle.Bold);


                decimal total = 0;
                pieChart.Series[0].Points.Clear();
                pieChart.Legends[0].CustomItems.Clear();
                for (int i = 0; i < totalList.Count; i++)
                {
                    total += totalList.Values.ElementAt(i);
                    pieChart.Series[0].Points.AddXY(i, totalList.Values.ElementAt((i)));
                    pieChart.Series[0].Points[i].LegendText = totalList.Keys.ElementAt((i));

                    //pieChart.Legends[0].CustomItems.RemoveAt(i);
                }
                //for (int i = 0; i < totalList.Count; i++)
                //{
                //    pieChart.Legends[0].CustomItems[i].Cells[0].Text = totalList.Keys.ElementAt((i));
                //    //(pieChart.Series[0].Points[i].Color, totalList.Keys.ElementAt((i)));
                //}

                lblTotalValue.Text = total.ToString("#.##");
            }
        }

        public class AgentChargeTotalGrid
        {
            private KeyValuePair<string, decimal> o;

            public AgentChargeTotalGrid(KeyValuePair<string, decimal> o)
            {
                this.o = o;
            }

            public string ChargeForm { get; set; }
            public decimal Amount { get; set; }
        }

        public class AgentChargeSearchResultDtoGrid
        {
            public string CollectionDate { get; set; }
            public string ChargeFrom { get; set; }
            public decimal Amount { get; set; }
            public string Outlet { get; set; }
            public string User { get; set; }

            private AgentChargeSearchResultDto _obj;

            public AgentChargeSearchResultDtoGrid(AgentChargeSearchResultDto obj)
            {
                _obj = obj;
            }

            public AgentChargeSearchResultDto GetModel()
            {
                return _obj;
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void Reset()
        {
            if (SessionInfo.userBasicInformation.userCategory != UserCategory.SubAgentUser
                &&
                SessionInfo.userBasicInformation.userCategory != UserCategory.AgentUser)
            { cmbAgent.SelectedIndex = -1; }

            if (SessionInfo.userBasicInformation.userCategory != UserCategory.SubAgentUser)
            { cmbOutlet.SelectedIndex = -1; }

            dtpChargeDateFrom.Value = SessionInfo.currentDate;
            dtpChargeDateTo.Value = SessionInfo.currentDate;

            dgvResult.DataSource = null;
            dgvResult.Refresh();
            lblResultCount.Text = "   Result : 0 Item(s)";
            dgvTotal.DataSource = null;
            dgvTotal.Refresh();

            pieChart.Series[0].Points.Clear();
            pieChart.Legends[0].CustomItems.Clear();
            lblTotalValue.Text = "0.00";
            lblFilter.Text = "";
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbAgent_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmbAgent.SelectedIndex > 1)
                {
                    LoadSubagents();
                }
                else
                {
                    cmbOutlet.DataSource = null;
                    cmbOutlet.Items.Clear();
                    return;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        private void LoadSubagents()
        {
            try
            {
                List<SubAgentInformation> subAgentInformationList = agentServices.GetSubagentsByAgentId(long.Parse(cmbAgent.SelectedValue.ToString()));

                //SubAgentInformation saiSelect = new SubAgentInformation();
                //saiSelect.name = "(Select)";
                //subAgentInformationList.Insert(0, saiSelect);
                SubAgentInformation saiAll = new SubAgentInformation();
                saiAll.name = "(All)";
                saiAll.id = 0;
                subAgentInformationList.Insert(0, saiAll);

                BindingSource bs = new BindingSource();
                bs.DataSource = subAgentInformationList;
                UtilityServices.fillComboBox(cmbOutlet, bs, "name", "id");
                cmbOutlet.SelectedIndex = 0;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        private void dgvTotal_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                IsSearch = false;
                List<AgentChargeSearchResultDto> agentIncomeSearchResultDtoList = _agentChargeSearchResultDto.Where(o => o.chargeFrom == dgvTotal.Rows[e.RowIndex].Cells[0].Value.ToString()).ToList();
                dgvResult.DataSource = null;
                LoadItems(agentIncomeSearchResultDtoList);
                lblFilter.Text = "Filtered By: " + dgvTotal.Rows[e.RowIndex].Cells[0].Value.ToString() + "   ";
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp.Message);
            }
        }



        private void btnViewReport_Click(object sender, EventArgs e)
        {
            try
            {
                crAgentChargeCommission report = new crAgentChargeCommission();
                frmReportViewer viewer = new frmReportViewer();

                ReportHeaders rptHeaders = new ReportHeaders();
                rptHeaders = UtilityServices.getReportHeaders("Agent Charge Commission");

                TextObject txtBankName = report.ReportDefinition.ReportObjects["txtBankName"] as TextObject;
                TextObject txtBranchName = report.ReportDefinition.ReportObjects["txtBranchName"] as TextObject;
                TextObject txtBranchAddress = report.ReportDefinition.ReportObjects["txtBranchAddress"] as TextObject;
                TextObject txtReportHeading = report.ReportDefinition.ReportObjects["txtReportHeading"] as TextObject;
                TextObject txtPrintUser = report.ReportDefinition.ReportObjects["txtPrintUser"] as TextObject;
                TextObject txtPrintDate = report.ReportDefinition.ReportObjects["txtPrintDate"] as TextObject;
                if (rptHeaders != null)
                {
                    if (rptHeaders.branchDto != null)
                    {
                        txtBankName.Text = rptHeaders.branchDto.bankName;
                        txtBranchName.Text = rptHeaders.branchDto.branchName;
                        txtBranchAddress.Text = rptHeaders.branchDto.branchAddress;
                    }
                    txtReportHeading.Text = rptHeaders.reportHeading;
                    txtPrintUser.Text = rptHeaders.printUser;
                    txtPrintDate.Text = rptHeaders.printDate.ToString("dd-MM-yyyy").Replace("-", "/");
                }
                loadAgentChargeCommission();

                report.SetDataSource(agentChargeDs);
                viewer.crvReportViewer.ReportSource = report;
                viewer.Show(this.Parent);
            }
            catch (Exception exp)
            { MsgBox.ShowError(exp.Message); }
        }

        private void loadAgentChargeCommission()
        {

            if (_agentChargeSearchResultDto != null)
            {
                agentChargeDs.Clear();
                foreach (AgentChargeSearchResultDto charge in _agentChargeSearchResultDto)
                {
                    AgentChargeDS.AgentChargeComissionDTRow newRow =
                        agentChargeDs.AgentChargeComissionDT.NewAgentChargeComissionDTRow();
                    newRow.amount = charge.amount ?? 0;
                    newRow.chargeDate = UtilityServices.getDateFromLong(charge.chargeDate ?? 0);
                    newRow.outletId = charge.outletId ?? 0;
                    newRow.chargeFrom = charge.chargeFrom;
                    newRow.outletName = charge.outletName;
                    newRow.userName = charge.userName;
                    agentChargeDs.AgentChargeComissionDT.AddAgentChargeComissionDTRow(newRow);
                }
                agentChargeDs.AcceptChanges();
            }
        }

        private void btnDoPdf_Click(object sender, EventArgs e)
        {

        }
    }
}