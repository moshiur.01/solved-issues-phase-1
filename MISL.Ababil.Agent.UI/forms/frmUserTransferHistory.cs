﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.UI.forms
{
    public partial class frmUserTransferHistory : CustomForm
    {
        public frmUserTransferHistory()
        {
            InitializeComponent();
        }

        private void frmUserTransferHistory_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }
    }
}
