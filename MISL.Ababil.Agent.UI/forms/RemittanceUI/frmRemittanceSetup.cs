﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.Remittance;
using MISL.Ababil.Agent.Services;

namespace MISL.Ababil.Agent.UI.forms.RemittanceUI
{
    public partial class frmRemittanceSetup : CustomForm
    {
        private List<ExchangeHouse> _exchangeHouses;
        private int _rowIndex;

        public frmRemittanceSetup()
        {
            InitializeComponent();
        }

        private void frmRemittanceSetup_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            new frmRemittanceAddDlg(new Packet() { actionType = FormActionType.Edit }).ShowDialog();
            LoadExistingExchangeHouses();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            LoadExistingExchangeHouses();
        }

        private void LoadExistingExchangeHouses()
        {
            try
            {
                _exchangeHouses = new RemittanceServices().GetRemittanceExHouse();
                dataGridView1.Rows.Clear();
                for (int i = 0; i < _exchangeHouses.Count; i++)
                {
                    dataGridView1.Rows.Add
                        (
                            UtilityServices.GetResizedImage((Bitmap)UtilityServices.byteArrayToImage(_exchangeHouses[i].logo), 40, 40),
                            //null,
                            _exchangeHouses[i].companyName,
                            _exchangeHouses[i].shortName,
                            "Edit",
                            _exchangeHouses[i].companyName + _exchangeHouses[i].shortName //filter
                        );
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        private void dataGridView1_SizeChanged(object sender, EventArgs e)
        {
            //if (dataGridView1.Columns[2].Width > 200)
            //{
            int retVal = (int)(dataGridView1.Columns[1].Width / 2.1f);
            dataGridView1.Columns[2].Width = retVal;
            //}
            //else
            //{
            //    dataGridView1.Columns[2].Width = 200;
            //}
        }

        private void dataGridView1_Resize(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                _rowIndex = e.RowIndex;
            }
            catch
            {
                // ignored
            }
            if (dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex] is DataGridViewButtonCell)
            {
                new frmRemittanceAddDlg(new Packet()
                {
                    actionType = FormActionType.Edit,
                    otherObj = _exchangeHouses[e.RowIndex]
                }).ShowDialog();
            }
            LoadExistingExchangeHouses();
            try
            {
                dataGridView1.Focus();
                dataGridView1.Rows[_rowIndex].Selected = true;
                dataGridView1.FirstDisplayedScrollingRowIndex = dataGridView1.SelectedRows[0].Index;
            }
            catch
            {
                // ignored
            }
        }
    }
}