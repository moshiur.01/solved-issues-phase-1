﻿using System.Windows.Forms;

namespace MISL.Ababil.Agent.UI.forms.RemittanceUI
{
    partial class frmRemittanceAddDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitle = new System.Windows.Forms.Label();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pbxLogo = new System.Windows.Forms.PictureBox();
            this.txtName = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtShortName = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.cmbStatus = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAccountNumber = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbAccountType = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblTitle.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(5, 5);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(159, 21);
            this.lblTitle.TabIndex = 14;
            this.lblTitle.Text = "New Exchange House";
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = this.lblTitle;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(553, 34);
            this.customTitlebar1.TabIndex = 13;
            this.customTitlebar1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(93, 254);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Logo :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(89, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Name :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(61, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Short Name :";
            // 
            // pbxLogo
            // 
            this.pbxLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbxLogo.Location = new System.Drawing.Point(136, 254);
            this.pbxLogo.Name = "pbxLogo";
            this.pbxLogo.Size = new System.Drawing.Size(100, 100);
            this.pbxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxLogo.TabIndex = 33;
            this.pbxLogo.TabStop = false;
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.Color.White;
            this.txtName.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtName.InputScopeAllowEmpty = false;
            this.txtName.InputScopeCustomString = null;
            this.txtName.IsValid = null;
            this.txtName.Location = new System.Drawing.Point(136, 51);
            this.txtName.MaxLength = 255;
            this.txtName.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtName.Name = "txtName";
            this.txtName.PromptText = "(Type here...)";
            this.txtName.ReadOnly = false;
            this.txtName.ShowMandatoryMark = false;
            this.txtName.Size = new System.Drawing.Size(339, 21);
            this.txtName.TabIndex = 1;
            this.txtName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtName.UpperCaseOnly = false;
            this.txtName.ValidationErrorMessage = "Validation Error!";
            // 
            // txtShortName
            // 
            this.txtShortName.BackColor = System.Drawing.Color.White;
            this.txtShortName.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtShortName.InputScopeAllowEmpty = false;
            this.txtShortName.InputScopeCustomString = null;
            this.txtShortName.IsValid = null;
            this.txtShortName.Location = new System.Drawing.Point(136, 90);
            this.txtShortName.MaxLength = 255;
            this.txtShortName.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtShortName.Name = "txtShortName";
            this.txtShortName.PromptText = "(Type here...)";
            this.txtShortName.ReadOnly = false;
            this.txtShortName.ShowMandatoryMark = false;
            this.txtShortName.Size = new System.Drawing.Size(165, 21);
            this.txtShortName.TabIndex = 3;
            this.txtShortName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtShortName.UpperCaseOnly = false;
            this.txtShortName.ValidationErrorMessage = "Validation Error!";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(328, 375);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(101, 27);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(440, 375);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(101, 27);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(12, 375);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(101, 27);
            this.btnClear.TabIndex = 12;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnBrowse
            // 
            this.btnBrowse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBrowse.ForeColor = System.Drawing.Color.White;
            this.btnBrowse.Location = new System.Drawing.Point(242, 254);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(101, 27);
            this.btnBrowse.TabIndex = 9;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = false;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // cmbStatus
            // 
            this.cmbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.InputScopeAllowEmpty = false;
            this.cmbStatus.IsValid = null;
            this.cmbStatus.Items.AddRange(new object[] {
            "Active",
            "Inactive"});
            this.cmbStatus.Location = new System.Drawing.Point(136, 213);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.PromptText = "(Select)";
            this.cmbStatus.ReadOnly = false;
            this.cmbStatus.ShowMandatoryMark = false;
            this.cmbStatus.Size = new System.Drawing.Size(165, 21);
            this.cmbStatus.TabIndex = 7;
            this.cmbStatus.ValidationErrorMessage = "Validation Error!";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(87, 216);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Status :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(37, 175);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Account Number :";
            // 
            // txtAccountNumber
            // 
            this.txtAccountNumber.BackColor = System.Drawing.Color.White;
            this.txtAccountNumber.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtAccountNumber.InputScopeAllowEmpty = false;
            this.txtAccountNumber.InputScopeCustomString = null;
            this.txtAccountNumber.IsValid = null;
            this.txtAccountNumber.Location = new System.Drawing.Point(136, 171);
            this.txtAccountNumber.MaxLength = 13;
            this.txtAccountNumber.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtAccountNumber.Name = "txtAccountNumber";
            this.txtAccountNumber.PromptText = "(Type here...)";
            this.txtAccountNumber.ReadOnly = false;
            this.txtAccountNumber.ShowMandatoryMark = false;
            this.txtAccountNumber.Size = new System.Drawing.Size(165, 21);
            this.txtAccountNumber.TabIndex = 5;
            this.txtAccountNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtAccountNumber.UpperCaseOnly = false;
            this.txtAccountNumber.ValidationErrorMessage = "Validation Error!";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(50, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Account Type :";
            // 
            // cmbAccountType
            // 
            this.cmbAccountType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAccountType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbAccountType.FormattingEnabled = true;
            this.cmbAccountType.InputScopeAllowEmpty = false;
            this.cmbAccountType.IsValid = null;
            this.cmbAccountType.Items.AddRange(new object[] {
            "GL_Account",
            "Party_Account"});
            this.cmbAccountType.Location = new System.Drawing.Point(136, 131);
            this.cmbAccountType.Name = "cmbAccountType";
            this.cmbAccountType.PromptText = "(Select)";
            this.cmbAccountType.ReadOnly = false;
            this.cmbAccountType.ShowMandatoryMark = false;
            this.cmbAccountType.Size = new System.Drawing.Size(165, 21);
            this.cmbAccountType.TabIndex = 7;
            this.cmbAccountType.ValidationErrorMessage = "Validation Error!";
            // 
            // frmRemittanceAddDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(553, 414);
            this.ControlBox = false;
            this.Controls.Add(this.cmbAccountType);
            this.Controls.Add(this.cmbStatus);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtAccountNumber);
            this.Controls.Add(this.txtShortName);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.pbxLogo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.customTitlebar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRemittanceAddDlg";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmRemittanceAddDlg";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmRemittanceAddDlg_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pbxLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblTitle;
        private Agent.CustomControls.CustomTitlebar customTitlebar1;
        private Agent.CustomControls.CustomTextBox txtShortName;
        private Agent.CustomControls.CustomTextBox txtName;
        private PictureBox pbxLogo;
        private Label label3;
        private Label label2;
        private Label label1;
        private Button btnClear;
        private Button btnCancel;
        private Button btnSave;
        private Button btnBrowse;
        private Agent.CustomControls.CustomComboBoxDropDownList cmbStatus;
        private Label label4;
        private Agent.CustomControls.CustomTextBox txtAccountNumber;
        private Label label5;
        private Agent.CustomControls.CustomComboBoxDropDownList cmbAccountType;
        private Label label6;
    }
}