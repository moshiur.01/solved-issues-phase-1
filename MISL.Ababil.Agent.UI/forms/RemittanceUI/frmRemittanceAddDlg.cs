﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.Remittance;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.UI.forms.ProgressUI;


namespace MISL.Ababil.Agent.UI.forms.RemittanceUI
{
    public partial class frmRemittanceAddDlg : CustomForm
    {
        private Packet _packet;
        private ExchangeHouse _exchangeHouse;

        public frmRemittanceAddDlg(Packet packet)
        {
            //_packet = packet;
            _exchangeHouse = (ExchangeHouse)packet.otherObj;
            InitializeComponent();

            SetupData();

            switch (packet.actionType)
            {
                case FormActionType.New:
                    break;
                case FormActionType.Edit:
                    FillComponentWithObjectValue();
                    break;
                case FormActionType.View:
                    FillComponentWithObjectValue();
                    ApplyReadonlyView();
                    break;
                case FormActionType.Verify:
                    FillComponentWithObjectValue();
                    ApplyReadonlyView();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void ApplyReadonlyView()
        {
            txtName.ReadOnly = true;
            txtShortName.ReadOnly = true;
            cmbStatus.Enabled = false;
            btnBrowse.Enabled = false;
        }

        private void FillComponentWithObjectValue()
        {
            if (_exchangeHouse != null)
            {
                txtName.Text = _exchangeHouse.companyName;
                txtShortName.Text = _exchangeHouse.shortName;
                if (_exchangeHouse.accountNature != null)
                {
                    cmbAccountType.SelectedIndex = (int)_exchangeHouse.accountNature;
                }
                txtAccountNumber.Text = _exchangeHouse.accountNumber;
                if (_exchangeHouse.status != null)
                {
                    cmbStatus.SelectedItem = _exchangeHouse.status;
                }
                else
                {
                    cmbStatus.SelectedIndex = -1;
                }
                if (_exchangeHouse.logo != null && _exchangeHouse.logo.Length > 0)
                {
                    pbxLogo.Image = UtilityServices.byteArrayToImage(_exchangeHouse.logo);
                }
            }
        }

        private void SetupData()
        {
            List<Status> ds = Enum.GetValues(typeof(Status)).Cast<Status>().ToList();
            cmbStatus.DataSource = ds;
            cmbStatus.SelectedIndex = -1;
        }

        private void frmRemittanceAddDlg_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ProgressUIManager.ShowProgress(this);
                byte[] byteImg = UtilityServices.imageToByteArray(pbxLogo.Image);
                new RemittanceServices().SaveRemittanceExHouse(new ExchangeHouse()
                {
                    id = _exchangeHouse != null ? _exchangeHouse.id : 0,
                    companyName = txtName.Text,
                    shortName = txtShortName.Text,
                    status = (Status)cmbStatus.SelectedValue,
                    accountNature = (AccountNature)cmbAccountType.SelectedIndex,
                    accountNumber = txtAccountNumber.Text,
                    logo = byteImg,
                    remitanceAccountSetup= _exchangeHouse != null ? _exchangeHouse.remitanceAccountSetup : 0
                });
                ProgressUIManager.CloseProgress();
                MsgBox.showInformation("Successfully saved!");
                this.Close();
            }
            catch (Exception ex)
            {
                ProgressUIManager.CloseProgress();
                MsgBox.ShowError(ex.Message);
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                pbxLogo.Image = UtilityServices.GetResizedImage((Bitmap)Image.FromFile(openFileDialog.FileName), 196, 196);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearAllControls();
        }

        private void ClearAllControls()
        {
            try
            {
                txtName.Text = string.Empty;
                txtShortName.Text = string.Empty;
                cmbStatus.SelectedIndex = -1;
                pbxLogo.Image = null;
            }
            catch
            {
                //suppressed                
            }
        }
    }
}