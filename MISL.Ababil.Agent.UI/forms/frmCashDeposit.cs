﻿using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.consumer;
using MISL.Ababil.Agent.Infrastructure.Models.models.transaction;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.Report.Reports;
using MISL.Ababil.Agent.Report;
using MISL.Ababil.Agent.UI.forms.ProgressUI;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.GenericFingerprintServices;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;

namespace MISL.Ababil.Agent.UI.forms
{
    public partial class frmCashDeposit : CustomForm, FingerprintEventObserver
    {
        public GUI _gui = new GUI();
        TransactionService _service = new TransactionService();
        ConsumerServices _consumerService = new ConsumerServices();
        ConsumerInformationDto _consumerInformationDto = new ConsumerInformationDto();
        string _subagentFingerPrint;
        string _serviceResponse;
        private AccountOperatorDto _accountOperatorDto;

        public frmCashDeposit()
        {
            InitializeComponent();            
            lblConsumerTitle.Text = "";
            lblMobileNo.Text = "";
            lblBalanceValue.Text = "";

            this.FormBorderStyle = FormBorderStyle.None;
            ConfigUIEnhancement();
        }

        public void ConfigUIEnhancement()
        {
            _gui = new GUI(this);

            _gui.Config(ref txtConsumerAccount, ValidCheck.VALIDATIONTYPES.TEXTBOX_EMPTY, null);
            _gui.Config(ref txtAmount, ValidCheck.VALIDATIONTYPES.TEXTBOX_NUMBER, null);
            _gui.Config(ref txtrCharge);
            _gui.Config(ref txtrTotal);
        }

        private void btnDeposit_Click(object sender, EventArgs e)
        {
            btnDeposit.Enabled = false;

            if (_gui.IsAllControlValidated())
            {
                string result = Message.showConfirmation("Are you sure to deposit?");

                if (result == "yes")
                {
                    if (txtConsumerAccount.Text != "" && txtAmount.Text != "")
                    {
                        //bio.CaptureFingerData();
                        //OnCaptureAlternative();
                        FingerprintServiceFactory.FingerprintServiceFactory factory = new FingerprintServiceFactory.FingerprintServiceFactory();
                        FingerprintDevice device = factory.getFingerprintDevice();
                        device.registerEventObserver(this);
                        device.capture();
                    }
                    else
                    {
                        Message.showWarning("Account no or amount may be blank.");
                        txtConsumerAccount.Focus();
                        _gui.IsAllControlValidated();
                    }
                }
            }
            btnDeposit.Enabled = true;
        }

        private CashInRequest FillCashInRequestData()
        {
            CashInRequest request = new CashInRequest();
            request.customerAccount = txtConsumerAccount.Text;
            request.amount = decimal.Parse(txtAmount.Text, CultureInfo.InvariantCulture);
            request.chargeAmount = decimal.Parse(txtrCharge.Text, CultureInfo.InvariantCulture);
            request.transactionDate = UtilityServices.GetLongDate(SessionInfo.currentDate);
            return request;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtConsumerAccount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) && (Keys)e.KeyChar != Keys.Back && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            base.OnKeyPress(e);
        }

        private void txtConsumerAccount_Leave(object sender, EventArgs e)
        {
            if (txtConsumerAccount.Text != "")
            {
                try
                {
                    _consumerInformationDto = _consumerService.getConsumerInformationDtoByAcc(txtConsumerAccount.Text.Trim());
                    lblConsumerTitle.Text = _consumerInformationDto.consumerTitle;
                    lblMobileNo.Text = _consumerInformationDto.mobileNumber;
                    lblBalanceValue.Text = (_consumerInformationDto.balance ?? 0).ToString("N", new CultureInfo("BN-BD"));

                    //txtrCharge.Text = "";
                    //txtrTotal.Text = "";

                    if (_consumerInformationDto.photo != null)
                    {
                        byte[] bytes = Convert.FromBase64String(_consumerInformationDto.photo);
                        Image image;
                        image = UtilityServices.byteArrayToImage(bytes);
                        pic_conusmer.Image = image;
                    }
                }
                catch (Exception ex)
                {
                    clearUi();
                    Message.showWarning(ex.Message);
                }
            }
        }

        private void clearUi()
        {
            lblConsumerTitle.Text = "";
            lblMobileNo.Text = "";
            pic_conusmer.Image = null;
            txtAmount.Text = "";
            txtConsumerAccount.Text = "";
            txtrCharge.Text = "";
            txtrTotal.Text = "";
            lblInWords.Text = "";
            lblBalanceValue.Text = "";
        }

        private void OnCaptureAlternative()
        {
            _subagentFingerPrint = ""; //data
            if (_subagentFingerPrint != null)
            {
                if (txtConsumerAccount.Text != "" && txtAmount.Text != "")
                {
                    CashInRequest request = FillCashInRequestData();
                    request.agentFingerData = _subagentFingerPrint;
                    try
                    {
                        if (TransactionUIService.isTxnSafe("CashDeposit", request.customerAccount, "", request.amount))
                        {
                            ProgressUIManager.ShowProgress(this);
                            _serviceResponse = _service.CashIn(request);
                            ProgressUIManager.CloseProgress();
                            TransactionUIService.cacheCurrentTxn("CashDeposit", request.customerAccount, "", request.amount);
                            clearUi();
                            Message.showInformation("Cash Deposited successfully.");
                            clearUi();

                            try
                            {
                                frmShowReport objfrmShowReport = new frmShowReport();
                                objfrmShowReport.DepositeReport(_serviceResponse);
                            }
                            catch (Exception ex)
                            {
                                Message.showError("Report generate problem: " + ex.Message);
                            }
                        }
                        else
                        {
                            clearUi();
                        }
                    }
                    catch (Exception ex)
                    {
                        ProgressUIManager.CloseProgress();
                        btnDeposit.Enabled = true;
                        Message.showError(ex.Message);
                    }
                }
                else
                {
                    Message.showWarning("Account no or amount may be blank.");
                }
            }
            else
            {
                Message.showWarning("Subagent finger print needed.");
            }
        }

        //private void bio_OnCapture(object sender, EventArgs e)
        //{
        //    AxBIOPLUGINACTXLib.AxBioPlugInActX x = (AxBIOPLUGINACTXLib.AxBioPlugInActX)sender;

        //    if (x.result == "0")
        //    {
        //        _subagentFingerPrint = bio.GetSafeLeftFingerData();
        //        if (_subagentFingerPrint != null)
        //        {
        //            if (txtConsumerAccount.Text != "" && txtAmount.Text != "")
        //            {
        //                CashInRequest request = FillCashInRequestData();
        //                request.agentFingerData = _subagentFingerPrint;
        //                try
        //                {
        //                    if (TransactionUIService.isTxnSafe("CashDeposit", request.customerAccount, "", request.amount))
        //                    {
        //                        ProgressUIManager.ShowProgress(this);
        //                        _serviceResponse = _service.CashIn(request);
        //                        ProgressUIManager.CloseProgress();
        //                        TransactionUIService.cacheCurrentTxn("CashDeposit", request.customerAccount, "", request.amount);
        //                        clearUi();
        //                        Message.showInformation("Cash Deposited successfully.");
        //                        clearUi();

        //                        try
        //                        {
        //                            frmShowReport objfrmShowReport = new frmShowReport();
        //                            objfrmShowReport.DepositeReport(_serviceResponse);
        //                        }
        //                        catch (Exception ex)
        //                        {
        //                            Message.ShowError("Report generate problem: " + ex.Message);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        clearUi();
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    ProgressUIManager.CloseProgress();
        //                    btnDeposit.Enabled = true;
        //                    Message.ShowError(ex.Message);
        //                }
        //            }
        //            else
        //            {
        //                Message.showWarning("Account no or amount may be blank.");
        //            }
        //        }
        //        else
        //        {
        //            Message.showWarning("Subagent finger print needed.");
        //        }
        //    }
        //}

        private void showCashWithdrowRport()
        {
            crWithdraw objRpt = new crWithdraw();
            frmReportViewer frm = new frmReportViewer();
            frm.crvReportViewer.ReportSource = objRpt;
            frm.ShowDialog();
        }

        private void txtAmount_Leave(object sender, EventArgs e)
        {
            double decValue;
            if (double.TryParse(txtAmount.Text, out decValue))
            {
                txtAmount.Text = decValue.ToString("N", new CultureInfo("BN-BD"));
            }

            if (txtConsumerAccount.Text != "" && txtAmount.Text != "")
            {
                if (decimal.Parse(txtConsumerAccount.Text) > 0)
                {
                    ChargeCalculationRequest chargeRequest = new ChargeCalculationRequest();
                    chargeRequest.srcAccount = txtConsumerAccount.Text;
                    chargeRequest.dstAccount = "";
                    chargeRequest.transactionAmount = decimal.Parse(txtAmount.Text);
                    chargeRequest.agentServices = "CashDeposit";
                    chargeRequest.entryUser = SessionInfo.username;
                    try
                    {
                        decimal chargeAmount = _service.getChargeAmount(chargeRequest);
                        txtrCharge.Text = chargeAmount.ToString("N", new CultureInfo("BN-BD"));//.ToString("#.00");
                        txtrTotal.Text = ((decimal.Parse(txtAmount.Text) + decimal.Parse(txtrCharge.Text))).ToString(
                            "N", new CultureInfo("BN-BD")); //.ToString("#.00");
                        AmountInWords amountInWords = new AmountInWords();
                        lblInWords.Text = amountInWords.ToWords(txtrTotal.Text);

                        //lblInWords.Text = String.Format("{0:##,##,##,###.##}", (decimal.Parse(txtAmount.Text) + decimal.Parse(txtrCharge.Text)));
                        //lblInWords.Text = (decimal.Parse(txtAmount.Text) + decimal.Parse(txtrCharge.Text)).ToString("N", new CultureInfo("BN-BD"));

                    }
                    catch (Exception ex)
                    {
                        Message.showError(ex.Message);
                    }
                }
                else
                {
                    Message.showWarning("Amount must be greater than zero");
                }
            }
            else
            {
                txtrCharge.Text = "";
                txtrTotal.Text = "";
            }
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) && (Keys)e.KeyChar != Keys.Back && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            base.OnKeyPress(e);
        }

        private void btshowAccHolderInfo_Click(object sender, EventArgs e)
        {
            _consumerInformationDto = _consumerService.getConsumerInformationDtoByAcc(txtConsumerAccount.Text);
            lblConsumerTitle.Text = _consumerInformationDto.consumerTitle;
            lblMobileNo.Text = _consumerInformationDto.mobileNumber;

            if (_consumerInformationDto.photo != null)
            {
                byte[] bytes = Convert.FromBase64String(_consumerInformationDto.photo);
                Image image;
                image = UtilityServices.byteArrayToImage(bytes);
                pic_conusmer.Image = image;
            }
        }

        private void btChargeCalculation_Click(object sender, EventArgs e)
        {
            if (txtConsumerAccount.Text != "" && txtAmount.Text != "")
            {
                ChargeCalculationRequest chargeRequest = new ChargeCalculationRequest();
                chargeRequest.srcAccount = txtConsumerAccount.Text;
                chargeRequest.dstAccount = "";
                chargeRequest.transactionAmount = decimal.Parse(txtAmount.Text);
                chargeRequest.agentServices = "CashDeposit";
                chargeRequest.entryUser = SessionInfo.username;
                try
                {
                    decimal chargeAmount = _service.getChargeAmount(chargeRequest);
                    Message.showInformation("Charge Amount : " + chargeAmount);
                }
                catch (Exception ex)
                {
                    Message.showError(ex.Message);
                }
            }
            else
            {
                Message.showWarning("Account number and amount require to calculate charge.");
            }
        }

        private void frmCashDeposit_FormClosing(object sender, FormClosingEventArgs e)
        {
            //ValidationManager.ReleaseValidationData(this);
            this.Owner = null;
        }

        private void btClear_Click(object sender, EventArgs e)
        {
            clearUi();
        }

        public void FingerPrintEventOccured(FingerprintEvents eventSpec, object EventData)
        {
            if (EventData == null)
            {
                return;
            }

            string data = EventData.ToString();
            //MessageBox.Show(data);
            if (!string.IsNullOrEmpty(data))
            {
                _subagentFingerPrint = data;
                if (_subagentFingerPrint != null)
                {
                    if (txtConsumerAccount.Text != "" && txtAmount.Text != "")
                    {
                        CashInRequest request = FillCashInRequestData();
                        request.agentFingerData = _subagentFingerPrint;
                        try
                        {
                            if (TransactionUIService.isTxnSafe("CashDeposit", request.customerAccount, "", request.amount))
                            {
                                ProgressUIManager.ShowProgress(this);
                                _serviceResponse = _service.CashIn(request);
                                ProgressUIManager.CloseProgress();
                                TransactionUIService.cacheCurrentTxn("CashDeposit", request.customerAccount, "", request.amount);

                                decimal finalValue = (_consumerInformationDto.balance ?? 0) + request.amount;
                                Message.showInformation("Cash Deposited successfully.\n\nCurrent Balance is: " + finalValue.ToString("N", new CultureInfo("BN-BD")));

                                clearUi();

                                try
                                {
                                    frmShowReport objfrmShowReport = new frmShowReport();
                                    objfrmShowReport.DepositeReport(_serviceResponse);
                                }
                                catch (Exception ex)
                                {
                                    Message.showError("Report generate problem: " + ex.Message);
                                }
                            }
                            else
                            {
                                clearUi();
                            }
                        }
                        catch (Exception ex)
                        {
                            ProgressUIManager.CloseProgress();
                            btnDeposit.Enabled = true;
                            Message.showError(ex.Message);
                        }
                    }
                    else
                    {
                        Message.showWarning("Account no or amount may be blank.");
                    }
                }
                else
                {
                    Message.showWarning("Subagent finger print needed.");
                }
            }
        }
    }
}