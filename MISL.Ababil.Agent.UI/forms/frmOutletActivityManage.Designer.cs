﻿namespace MISL.Ababil.Agent.UI.forms
{
    partial class frmOutletActivityManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dgvoutletName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvAllClose = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dgvTxnClose = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dgvTFSClose = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(744, 26);
            this.customTitlebar1.TabIndex = 0;
            this.customTitlebar1.TabStop = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvoutletName,
            this.dgvAllClose,
            this.dgvTxnClose,
            this.dgvTFSClose});
            this.dataGridView1.Location = new System.Drawing.Point(0, 96);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(741, 150);
            this.dataGridView1.TabIndex = 1;
            // 
            // dgvoutletName
            // 
            this.dgvoutletName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvoutletName.HeaderText = "Outlet Name";
            this.dgvoutletName.Name = "dgvoutletName";
            this.dgvoutletName.ReadOnly = true;
            // 
            // dgvAllClose
            // 
            this.dgvAllClose.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dgvAllClose.HeaderText = "Close";
            this.dgvAllClose.Name = "dgvAllClose";
            this.dgvAllClose.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAllClose.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dgvAllClose.Width = 80;
            // 
            // dgvTxnClose
            // 
            this.dgvTxnClose.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dgvTxnClose.HeaderText = "Txn Close";
            this.dgvTxnClose.Name = "dgvTxnClose";
            this.dgvTxnClose.Width = 80;
            // 
            // dgvTFSClose
            // 
            this.dgvTFSClose.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dgvTFSClose.HeaderText = "24/7 Close";
            this.dgvTFSClose.Name = "dgvTFSClose";
            this.dgvTFSClose.Width = 80;
            // 
            // frmOutletActivityManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(744, 462);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.customTitlebar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmOutletActivityManage";
            this.Text = "Outlet Activity Management";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Agent.CustomControls.CustomTitlebar customTitlebar1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dgvTFSClose;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dgvTxnClose;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dgvAllClose;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvoutletName;
    }
}