﻿namespace MISL.Ababil.Agent.UI.forms.Dashboard
{
    partial class DashboardDemo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.scoreBoardItem1 = new MISL.Ababil.Agent.UI.forms.Dashboard.DashBoardControls.ScoreBoardItem();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Controls.Add(this.scoreBoardItem1);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1043, 457);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // scoreBoardItem1
            // 
            this.scoreBoardItem1.BackColor = System.Drawing.Color.White;
            this.scoreBoardItem1.Font = new System.Drawing.Font("Segoe UI", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scoreBoardItem1.Icon = MISL.Ababil.Agent.UI.forms.Dashboard.DashBoardControls.ScoreBoardItem.ScoreBoardItemIcon.NoChange;
            this.scoreBoardItem1.Location = new System.Drawing.Point(14, 15);
            this.scoreBoardItem1.Margin = new System.Windows.Forms.Padding(14, 15, 14, 15);
            this.scoreBoardItem1.Name = "scoreBoardItem1";
            this.scoreBoardItem1.Size = new System.Drawing.Size(265, 125);
            this.scoreBoardItem1.TabIndex = 0;
            this.scoreBoardItem1.Title = "ScoreBoardItemTitle";
            this.scoreBoardItem1.Value = "1233";
            this.scoreBoardItem1.ValueDifference = "+0000";
            // 
            // DashboardDemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1043, 457);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "DashboardDemo";
            this.Text = "DashboardDemo";
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DashBoardControls.ScoreBoardItem scoreBoardItem1;
    }
}