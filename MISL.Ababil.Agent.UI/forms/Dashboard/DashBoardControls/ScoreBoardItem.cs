﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MISL.Ababil.Agent.UI.forms.Dashboard.DashBoardControls
{
    public partial class ScoreBoardItem : UserControl
    {
        public ScoreBoardItem()
        {
            InitializeComponent();
            if (_icon != ScoreBoardItemIcon.NoChange)
            {
                pbxUpDown.Image = imageList1.Images[(int)_icon];
                txtDifference.Visible = pbxUpDown.Visible = true;
                panelIconAndDiff.Visible = true;
                if (txtValue.Text.Length <= 3)
                {
                    panelSpace.Visible = true;
                }
                else
                {
                    panelSpace.Visible = false;
                }

            }
            else
            {
                pbxUpDown.Image = null;
                txtDifference.Visible = pbxUpDown.Visible = false;
                panelSpace.Visible = panelIconAndDiff.Visible = false;
            }

        }

        private ScoreBoardItemIcon _icon = ScoreBoardItemIcon.NoChange;

        public string Title
        {
            get
            {
                return txtTitle != null ? txtTitle.Text : "";
            }
            set
            {
                if (txtTitle != null) txtTitle.Text = value;
            }
        }

        public string Value
        {
            get
            {
                return txtValue != null ? txtValue.Text : "";
            }
            set
            {
                if (_icon != ScoreBoardItemIcon.NoChange)
                {
                    pbxUpDown.Image = imageList1.Images[(int)_icon];
                    txtDifference.Visible = pbxUpDown.Visible = true;
                    panelIconAndDiff.Visible = true;
                    if (value.Length <= 3)
                    {
                        panelSpace.Visible = true;
                    }
                    else
                    {
                        panelSpace.Visible = false;
                    }

                }
                else
                {
                    pbxUpDown.Image = null;
                    txtDifference.Visible = pbxUpDown.Visible = false;
                    panelSpace.Visible = panelIconAndDiff.Visible = false;
                }
                if (txtValue != null) txtValue.Text = value;
            }
        }

        public string ValueDifference
        {
            get
            {
                return txtDifference != null ? txtDifference.Text : "";
            }
            set
            {
                if (txtDifference != null) txtDifference.Text = value;
            }
        }

        public ScoreBoardItemIcon Icon
        {
            get
            {
                return _icon;
            }
            set
            {
                _icon = value;
                if (value != ScoreBoardItemIcon.NoChange)
                {
                    pbxUpDown.Image = imageList1.Images[(int)_icon];
                    txtDifference.Visible = pbxUpDown.Visible = true;
                    panelIconAndDiff.Visible = true;
                    if (txtValue.Text.Length <= 3)
                    {
                        panelSpace.Visible = true;
                    }
                    else
                    {
                        panelSpace.Visible = false;
                    }

                }
                else
                {
                    pbxUpDown.Image = null;
                    txtDifference.Visible = pbxUpDown.Visible = false;
                    panelSpace.Visible = panelIconAndDiff.Visible = false;
                }
            }
        }

        public enum ScoreBoardItemIcon
        {
            NoChange,
            Increased,
            Decresded
        }


        private void ScoreBoardItem_Load(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ScoreBoardItem_MouseEnter(object sender, EventArgs e)
        {

        }

        private void ScoreBoardItem_MouseLeave(object sender, EventArgs e)
        {
            //panelIconButton.Visible = false;
        }
    }
}
