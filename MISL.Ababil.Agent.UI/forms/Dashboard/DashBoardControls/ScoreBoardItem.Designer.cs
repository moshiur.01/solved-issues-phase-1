﻿namespace MISL.Ababil.Agent.UI.forms.Dashboard.DashBoardControls
{
    partial class ScoreBoardItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScoreBoardItem));
            this.txtValue = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.txtTitle = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pbxUpDown = new System.Windows.Forms.PictureBox();
            this.txtDifference = new System.Windows.Forms.Label();
            this.panelIconAndDiff = new System.Windows.Forms.Panel();
            this.panelSpace = new System.Windows.Forms.Panel();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxUpDown)).BeginInit();
            this.panelIconAndDiff.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtValue
            // 
            this.txtValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtValue.Font = new System.Drawing.Font("Segoe UI Light", 52F);
            this.txtValue.Location = new System.Drawing.Point(50, 0);
            this.txtValue.Margin = new System.Windows.Forms.Padding(14, 0, 14, 0);
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(155, 80);
            this.txtValue.TabIndex = 0;
            this.txtValue.Text = "0000";
            this.txtValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtValue.MouseEnter += new System.EventHandler(this.ScoreBoardItem_MouseEnter);
            this.txtValue.MouseLeave += new System.EventHandler(this.ScoreBoardItem_MouseLeave);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.White;
            this.imageList1.Images.SetKeyName(0, "dashboard_icon_flat.png");
            this.imageList1.Images.SetKeyName(1, "icon_up_1.png");
            this.imageList1.Images.SetKeyName(2, "icon_down_1.png");
            this.imageList1.Images.SetKeyName(3, "dashboard_icon_up.png");
            this.imageList1.Images.SetKeyName(4, "dashboard_icon_down.png");
            // 
            // txtTitle
            // 
            this.txtTitle.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.txtTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtTitle.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.txtTitle.ForeColor = System.Drawing.Color.White;
            this.txtTitle.Location = new System.Drawing.Point(0, 0);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(265, 26);
            this.txtTitle.TabIndex = 2;
            this.txtTitle.Text = "ScoreBoardItemTitle";
            this.txtTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtTitle.MouseEnter += new System.EventHandler(this.ScoreBoardItem_MouseEnter);
            this.txtTitle.MouseLeave += new System.EventHandler(this.ScoreBoardItem_MouseLeave);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 119);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(265, 6);
            this.panel1.TabIndex = 3;
            this.panel1.MouseEnter += new System.EventHandler(this.ScoreBoardItem_MouseEnter);
            this.panel1.MouseLeave += new System.EventHandler(this.ScoreBoardItem_MouseLeave);
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "icondetail2.png");
            this.imageList2.Images.SetKeyName(1, "iconrefresh2.png");
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ImageIndex = 0;
            this.button1.ImageList = this.imageList2;
            this.button1.Location = new System.Drawing.Point(109, 97);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(19, 19);
            this.button1.TabIndex = 5;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button2.BackColor = System.Drawing.Color.White;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.CheckedBackColor = System.Drawing.Color.White;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ImageIndex = 1;
            this.button2.ImageList = this.imageList2;
            this.button2.Location = new System.Drawing.Point(135, 97);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(19, 19);
            this.button2.TabIndex = 6;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.txtValue);
            this.panel3.Controls.Add(this.panelSpace);
            this.panel3.Controls.Add(this.panelIconAndDiff);
            this.panel3.Location = new System.Drawing.Point(5, 13);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(255, 80);
            this.panel3.TabIndex = 9;
            // 
            // pbxUpDown
            // 
            this.pbxUpDown.Image = global::MISL.Ababil.Agent.UI.Properties.Resources.dashboard_icon_flat;
            this.pbxUpDown.Location = new System.Drawing.Point(0, 24);
            this.pbxUpDown.Name = "pbxUpDown";
            this.pbxUpDown.Size = new System.Drawing.Size(51, 32);
            this.pbxUpDown.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbxUpDown.TabIndex = 1;
            this.pbxUpDown.TabStop = false;
            this.pbxUpDown.MouseEnter += new System.EventHandler(this.ScoreBoardItem_MouseEnter);
            this.pbxUpDown.MouseLeave += new System.EventHandler(this.ScoreBoardItem_MouseLeave);
            // 
            // txtDifference
            // 
            this.txtDifference.Font = new System.Drawing.Font("Segoe UI Semibold", 10F);
            this.txtDifference.ForeColor = System.Drawing.Color.Black;
            this.txtDifference.Location = new System.Drawing.Point(-2, 59);
            this.txtDifference.Name = "txtDifference";
            this.txtDifference.Size = new System.Drawing.Size(51, 19);
            this.txtDifference.TabIndex = 2;
            this.txtDifference.Text = "+0000";
            this.txtDifference.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtDifference.MouseEnter += new System.EventHandler(this.ScoreBoardItem_MouseEnter);
            this.txtDifference.MouseLeave += new System.EventHandler(this.ScoreBoardItem_MouseLeave);
            // 
            // panelIconAndDiff
            // 
            this.panelIconAndDiff.Controls.Add(this.txtDifference);
            this.panelIconAndDiff.Controls.Add(this.pbxUpDown);
            this.panelIconAndDiff.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelIconAndDiff.Location = new System.Drawing.Point(205, 0);
            this.panelIconAndDiff.Name = "panelIconAndDiff";
            this.panelIconAndDiff.Size = new System.Drawing.Size(50, 80);
            this.panelIconAndDiff.TabIndex = 8;
            // 
            // panelSpace
            // 
            this.panelSpace.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSpace.Location = new System.Drawing.Point(0, 0);
            this.panelSpace.Name = "panelSpace";
            this.panelSpace.Size = new System.Drawing.Size(50, 80);
            this.panelSpace.TabIndex = 9;
            // 
            // ScoreBoardItem
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtTitle);
            this.Controls.Add(this.panel3);
            this.Font = new System.Drawing.Font("Segoe UI", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(14, 15, 14, 15);
            this.Name = "ScoreBoardItem";
            this.Size = new System.Drawing.Size(265, 125);
            this.Load += new System.EventHandler(this.ScoreBoardItem_Load);
            this.MouseEnter += new System.EventHandler(this.ScoreBoardItem_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.ScoreBoardItem_MouseLeave);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxUpDown)).EndInit();
            this.panelIconAndDiff.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label txtValue;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label txtTitle;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panelIconAndDiff;
        private System.Windows.Forms.Label txtDifference;
        private System.Windows.Forms.PictureBox pbxUpDown;
        private System.Windows.Forms.Panel panelSpace;
    }
}
