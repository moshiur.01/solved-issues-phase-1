﻿using MetroFramework.Forms;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MISL.Ababil.Agent.UI.forms
{
    public partial class frmSubagentSearch : MetroForm
    {
        List<AgentInformation> objAgentInfoList = new List<AgentInformation>();
        AgentServices objAgentServices = new AgentServices();

        List<SubAgentInformation> _subAgentInformationList;
        SubAgentServices _objSubAgentServices = new SubAgentServices();
        SubAgentSearchDto _searchDto = new SubAgentSearchDto();

        public frmSubagentSearch()
        {
            InitializeComponent();
            InitializeSetupData();
        }
        private void InitializeSetupData()
        {
            txtMobileNo.MaxLength = CommonRules.mobileNoLength;
            try
            {
                #region Load AgentList
                objAgentInfoList = objAgentServices.getAgentInfoBranchWise();
                BindingSource bs = new BindingSource();

                AgentInformation allAgents = new AgentInformation();
                allAgents.id = 0;
                allAgents.businessName = "All";
                objAgentInfoList.Insert(0, allAgents);

                bs.DataSource = objAgentInfoList;
                UtilityServices.fillComboBox(cmbAgentName, bs, "businessName", "id");
                cmbAgentName.Text = "Select";
                #endregion

                #region Load Status
                List<OutletStatusList> statusList = Enum.GetValues(typeof(OutLetStatus)).Cast<OutLetStatus>().Select(o => new OutletStatusList
                                                                                                            {
                                                                                                                OutletStatus = o.ToString(),
                                                                                                            }).ToList();
                OutletStatusList allStatus = new OutletStatusList();
                bs = new BindingSource();
                allStatus.OutletStatus = "All";
                statusList.Insert(0, allStatus);
                bs.DataSource = statusList;
                UtilityServices.fillComboBox(cmbOutletStatus, bs, "OutletStatus", "OutletStatus");
                #endregion
            }
            catch (Exception ex)
            {
                Message.showError(ex.Message);
            }
        }



        private void btnSearch_Click(object sender, EventArgs e)
        {
            showSubagents();
        }
        private void showSubagents()
        {
            #region COmmented
            //AgentInformation agentInfo = null;
            //int agentId = Convert.ToInt32(cmbAgentName.SelectedValue);
            //try
            //{
            //    dgvSubAgent.DataSource = null;
            //    dgvSubAgent.Rows.Clear();
            //    agentInfo = objAgentServices.getAgentInfoById(agentId.ToString());
            //    if (agentInfo.subAgents != null)
            //    {
            //        if (agentInfo.subAgents.Count > 0)
            //        {
            //            if (dgvSubAgent.Columns.Count == 0)
            //            {
            //                DataGridViewButtonColumn buttonColumn = new DataGridViewButtonColumn();
            //                buttonColumn.Text = "View";
            //                buttonColumn.UseColumnTextForButtonValue = true;
            //                dgvSubAgent.Columns.Add(buttonColumn);
            //            }
            //            subAgentInformationList = agentInfo.subAgents;
            //            dgvSubAgent.DataSource = subAgentInformationList.Select(o => new SubAgentInformationGrid(o) { id = o.id, name = o.name, subAgentCode = o.subAgentCode, businessAddress = o.businessAddress, mobleNumber = o.mobleNumber, phoneNumber = o.phoneNumber }).ToList();
            //        }
            //    }
            //    //lblItemCount.Text = agentInfo.subAgents.Count.ToString();
            //}
            //catch (Exception ex)
            //{
            //    Message.ShowError(ex.Message);
            //}
            #endregion

            _subAgentInformationList = new List<SubAgentInformation>();
            FillSearchDto();
            try
            {
                dgvSubAgent.DataSource = null;
                dgvSubAgent.Rows.Clear();
                _subAgentInformationList = _objSubAgentServices.SearchSubAgents(_searchDto);
                if (_subAgentInformationList != null)
                {
                    if (_subAgentInformationList.Count > 0)
                    {
                        dgvSubAgent.Rows.Clear();
                        int rowIndex = 0;
                        foreach (SubAgentInformation outlet in _subAgentInformationList)
                        {
                            dgvSubAgent.Rows.Add();
                            dgvSubAgent["gridColOutletCode", rowIndex].Value = outlet.subAgentCode;
                            dgvSubAgent["gridColOutletName", rowIndex].Value = outlet.name;
                            dgvSubAgent["gridColMobileNo", rowIndex].Value = outlet.mobleNumber;
                            dgvSubAgent["gridColCreationDate", rowIndex].Value = UtilityServices.getDateFromLong(outlet.creationDate ?? 0).ToString("dd/MM/yyyy");
                            dgvSubAgent["gridColOutletStatus", rowIndex].Value = outlet.outletStatus ?? null;
                            dgvSubAgent["gridColSubagentId", rowIndex].Value = outlet.id; 
                             rowIndex++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Message.showError(ex.Message);
            }
        }

        private void FillSearchDto()
        {
            try
            {
                _searchDto = new SubAgentSearchDto();

                //if (cmbAgentName.SelectedIndex == -1) _searchDto.agentId = 0;
                //else _searchDto.agentId = long.Parse(cmbAgentName.SelectedValue.ToString());
                _searchDto.agentId = long.Parse(cmbAgentName.SelectedValue.ToString());

                if (cmbOutletStatus.SelectedValue.ToString() == "All") _searchDto.outLetStatus = null;
                else _searchDto.outLetStatus = (OutLetStatus)Enum.Parse(typeof(OutLetStatus), cmbOutletStatus.SelectedValue.ToString());

                _searchDto.outletCode = txtOutletCode.Text.Trim();
                _searchDto.outletName = txtOutletName.Text.Trim();
                _searchDto.outletMobileNumber = txtMobileNo.Text.Trim();

                //_searchDto.fromDate = UtilityServices.GetLongDate(dtpCreationDateFrom.Value);
                //_searchDto.toDate = UtilityServices.GetLongDate(dtpCreationDateTo.Value);
            }
            catch (Exception exp)
            { throw new Exception(exp.Message); }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            dgvSubAgent.Rows.Clear();
            txtMobileNo.Text = txtOutletCode.Text = txtOutletName.Text = "";
            cmbAgentName.SelectedIndex = cmbOutletStatus.SelectedIndex = 0;

            //dtpCreationDateFrom.Value = dtpCreationDateTo.Value = SessionInfo.currentDate;
        }

        private void frmSubagentSearch_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void dgvSubAgent_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //if (dgvSubAgent.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "View")
            if (e.ColumnIndex == 6)
            {
                try
                {
                    if (dgvSubAgent.RowCount == 0) return;

                    SubAgentInformation subAgentInformation = _subAgentInformationList[e.RowIndex];
                    if (subAgentInformation != null) ShowSubAgent(subAgentInformation.id);
                }
                catch (Exception exp)
                { Message.showError(exp.Message); }
            }
        }

        private void dgvSubAgent_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                SubAgentInformation subAgentInformation = _subAgentInformationList[e.RowIndex];
                if (subAgentInformation != null) ShowSubAgent(subAgentInformation.id);
            }
            catch (Exception exp)
            { Message.showError(exp.Message); }
        }

        private void ShowSubAgent(long outletId)
        {
            SubAgentServices services = new SubAgentServices();
            try
            {
                //subAgentInformation = services.getSubAgentTotalInfoById(subAgentInformation.id);
                ServiceResult result = services.getSubAgentDetailsById(outletId);

                if (result.Success)
                {
                    SubAgentInformation subAgentInformation = result.ReturnedObject as SubAgentInformation;
                    if (subAgentInformation != null)
                    {
                        frmSubAgent objfrmSubAgent = new frmSubAgent(subAgentInformation, subAgentInformation.agent.id,
                            ActionType.update);
                        objfrmSubAgent.ShowDialog();
                        showSubagents();
                    }
                }
                else
                {
                    Message.showError(result.Message);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }

    internal class OutletStatusList
    {
        public string OutletStatus { get; set; }
    }

    internal class SubAgentListGrid
    {
        public long id { get; set; }
        public string subAgentCode { get; set; }
        public string name { get; set; }
        public string businessAddress { get; set; }
        public string mobleNumber { get; set; }
        public DateTime? creationDate { get; set; }
        public OutLetStatus? outLetStatus { get; set; }


        private SubAgentInformation _obj;

        public SubAgentListGrid(SubAgentInformation obj)
        {
            _obj = obj;
        }

        public SubAgentInformation GetModel()
        {
            return _obj;
        }
    }
}
