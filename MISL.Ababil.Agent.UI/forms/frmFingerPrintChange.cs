﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MetroFramework.Forms;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.fingerprint;
using MISL.Ababil.Agent.Common.UI;

namespace MISL.Ababil.Agent.UI.forms
{
    public partial class frmFingerPrintChange : MetroForm
    {
        GUI _gui = new GUI();
        private bool _isForced;             // WALI :: 17-Jan-2016

        PersonFingerInfo _personFingerTemplate = new PersonFingerInfo();
        FingerInfo _finger;
        private List<FingerInfo> _fingerList;
        UserService userService = new UserService();
        
        public frmFingerPrintChange(bool isForced) // WALI :: 17-Jan-2016
        {
            _isForced = isForced;
            InitializeComponent();
            AgentUserSearchResultDto userInfo = userService.GetUserDetailsByUserName(SessionInfo.username);

            if (userInfo != null)
            {
                lblUserNameText.Text = userInfo.userName;
                lblUserFullNameText.Text = userInfo.fullName;
                lblContactNoText.Text = userInfo.mobileNumber;
            }
            else
            {
                Message.showError("Error loading user data !");
                btnFingerPrint.Enabled = btnOK.Enabled = false;
            }

            SetupComponent();
            btnFingerPrint.Focus();
        }

        private void SetupComponent()
        {
            if (_isForced)
            {
                btnCancel.Visible = false;
                btnExit.Visible = true;
                btnExit.Location = btnCancel.Location;
            }
            else
            {
                btnCancel.Visible = true;
                btnExit.Visible = false;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmPasswordChange_Load(object sender, EventArgs e)
        {

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (_personFingerTemplate == null) { Message.showWarning("Fingerprint required !"); return; }
                else if (_personFingerTemplate.fingerInfos == null) { Message.showWarning("Fingerprint required !"); return; }
                else if (_personFingerTemplate.fingerInfos.Count == 0) { Message.showWarning("Fingerprint required !"); return; }

                PasswordChangeService passwordChangeService = new PasswordChangeService();
                ChangeIdentity changeIdentity = new ChangeIdentity();
                FingerChangeInfo fingerChngInfo = new FingerChangeInfo();

                fingerChngInfo.userName = SessionInfo.username;
                fingerChngInfo.personFingerInfo = _personFingerTemplate;
                changeIdentity.fingerChangeInfo = fingerChngInfo;

                string responseString = passwordChangeService.saveChangedFingerPrint(changeIdentity);

                if (responseString == "SUCCESS")
                {
                    Message.showInformation("Fingerprint successfully changed.");
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                {
                    Message.showError(responseString);
                }
            }
            catch (Exception ex)
            {
                Message.showError(ex.Message);
            }
        }

        private void frmPasswordChange_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            g.DrawRectangle(Pens.Black, 0, 0, this.Width - 1, this.Height - 1);
        }

        int tmpX = 0;
        int tmpY = 0;

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left = Cursor.Position.X - tmpX;
                this.Top = Cursor.Position.Y - tmpY;
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            tmpX = e.X;
            tmpY = e.Y;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //if (this.WindowState == FormWindowState.Normal)
            //{
            //    this.WindowState = FormWindowState.Maximized;
            //    button2.Text = "3";
            //}
            //if (this.WindowState == FormWindowState.Maximized)
            //{
            //    this.WindowState = FormWindowState.Normal;
            //    button2.Text = "2";
            //}
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void metroTextBox1_Enter(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            g.DrawRectangle(new Pen(Color.DodgerBlue), ((Control)sender).Left - 1, ((Control)sender).Top - 1, ((Control)sender).Width + 1, ((Control)sender).Height + 1);
            g.Flush();
        }

        private void metroTextBox1_Leave(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            g.Clear(this.BackColor);
            //g.DrawRectangle(Pens.Black, metroTextBox1.Left - 1, metroTextBox1.Top - 1, metroTextBox1.Width + 2, metroTextBox1.Height + 2);
            g.Flush();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmPasswordChange_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }
        
        private void btnExit_Click(object sender, EventArgs e)
        {
            if (Message.showConfirmation("Do you want to exit?") == "yes")
            {
                Application.ExitThread();
                Application.Restart();
            }
        }

        private void btnFingerPrint_Click(object sender, EventArgs e)
        {
            try
            {
                frmFingerprintCapture objFrmFinger = new frmFingerprintCapture(lblUserNameText.Text.Trim());
                Color defaultButtonColor = Color.FromArgb(0, 122, 170);

                DialogResult dr = objFrmFinger.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    _fingerList = objFrmFinger.FingerInfos;

                    if (_fingerList != null)
                    {
                        if (_fingerList.Count > 0)
                        {
                            btnFingerPrint.BackColor = Color.LightGreen;

                            string fingerName;
                            _personFingerTemplate.fingerInfos = new List<FingerInfo>();
                            foreach (FingerInfo finger in _fingerList)
                            {
                                _finger = new FingerInfo();
                                _finger.id = 0;
                                _finger.fingerData = finger.fingerData;

                                //fingerName = finger.hand + finger.finger;
                                //FingerIndex fingIndex = (FingerIndex)Enum.Parse(typeof(FingerIndex), fingerName);
                                _finger.fingerIndex = finger.fingerIndex;

                                _personFingerTemplate.fingerInfos.Add(_finger);
                            }
                        }
                        else btnFingerPrint.BackColor = defaultButtonColor;
                    }
                    else btnFingerPrint.BackColor = defaultButtonColor;
                }
            }
            catch (Exception exp)
            { Message.showError(exp.Message); }
        }
    }
}

