﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using MISL.Ababil.Agent.Report.Reports;
using MISL.Ababil.Agent.Services;
//using MISL.Ababil.Agent.UI.forms.TempUI;

namespace MISL.Ababil.Agent.UI.forms
{
    public partial class frmAgentIncome : Form
    {
        List<AgentInformation> objAgentInfoList = new List<AgentInformation>();
        AgentServices agentServices = new AgentServices();
        List<CommissionTypeDto> _commissionTypes = new List<CommissionTypeDto>();

        private List<AgentIncomeSearchResultDto> _agentIncomeSearchResultDto;
        private List<AgentIncomeTotalGrid> _agentIncomeTotal;
        List<AgentIncomeStatementDto> _agentIncomeStatList = new List<AgentIncomeStatementDto>();
        private bool IsSearch = true;
        private bool isAllSearched = false;

        public frmAgentIncome()
        {
            InitializeComponent();
            preparedUI();
        }

        private void preparedUI()
        {
            try
            {
                SetupDataLoad();
                SetupComponents();
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp.Message);
            }
        }

        private void SetupDataLoad()
        {
            try
            {
                dtpIncomeDateFrom.Value = new DateTime(SessionInfo.currentDate.Year, SessionInfo.currentDate.Month, 1);
                dtpIncomeDateTo.Value = SessionInfo.currentDate;

                #region Load posted status
                //cmbPostingStatus.DataSource = Enum.GetValues(typeof(PostedStatus));
                List<PostedStatusCombo> statusList = Enum.GetValues(typeof(PostedStatus)).Cast<PostedStatus>()
                                                                                         .Select(o => new PostedStatusCombo
                                                                                         {
                                                                                             postedStatus = o.ToString(),
                                                                                         }).ToList();
                PostedStatusCombo allStatus = new PostedStatusCombo();
                BindingSource bs = new BindingSource();
                allStatus.postedStatus = "All";
                statusList.Insert(0, allStatus);
                bs.DataSource = statusList;
                UtilityServices.fillComboBox(cmbPostingStatus, bs, "postedStatus", "postedStatus");
                #endregion

                #region Load agent list
                objAgentInfoList = agentServices.getAgentInfoBranchWise();
                BindingSource bsAgent = new BindingSource();
                bsAgent.DataSource = objAgentInfoList;

                //AgentInformation agSelect = new AgentInformation();
                //agSelect.businessName = "(Select)";
                //objAgentInfoList.Insert(0, agSelect);
                AgentInformation agAll = new AgentInformation();
                agAll.businessName = "(All)";
                agAll.id = 0;
                objAgentInfoList.Insert(0, agAll);

                UtilityServices.fillComboBox(cmbAgent, bsAgent, "businessName", "id");
                cmbAgent.SelectedIndex = 0;
                #endregion

                #region Load Commission Types
                //_commissionTypes = TransactionService.getCommissionTypes();
                //BindingSource bs = new BindingSource();
                //bs.DataSource = _commissionTypes;
                //UtilityServices.FillComboBox(cmbIncomeFrom, bs, "commissionName", "id");
                #endregion
            }
            catch (Exception exp)
            { throw exp; }
        }

        private void SetupComponents()
        {
            try
            {
                if (SessionInfo.userBasicInformation.userCategory == UserCategory.AgentUser)
                {
                    cmbAgent.SelectedValue = UtilityServices.getCurrentAgent().id; ;
                    cmbAgent.Enabled = false;
                    LoadSubagents();
                }
                else if (SessionInfo.userBasicInformation.userCategory == UserCategory.SubAgentUser)
                {
                    SubAgentInformation currentSubagentInfo = UtilityServices.getCurrentSubAgent();
                    cmbAgent.SelectedValue = currentSubagentInfo.agent.id;
                    cmbAgent.Enabled = false;

                    BindingSource bs = new BindingSource();
                    bs.DataSource = currentSubagentInfo;
                    UtilityServices.fillComboBox(cmbOutlet, bs, "name", "id");
                    cmbOutlet.Enabled = false;

                    Application.DoEvents();

                    //btnSearch_Click(null, null);
                    SearchExt();
                }
            }
            catch (Exception)
            { throw; }
        }


        private void frmAgentIncome_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        [DllImport("dwmapi.dll")]
        public static extern int DwmExtendFrameIntoClientArea(IntPtr hWnd, ref Margins pMarInset);

        [DllImport("dwmapi.dll")]
        public static extern int DwmSetWindowAttribute(IntPtr hwnd, int attr, ref int attrValue, int attrSize);

        [DllImport("dwmapi.dll")]
        public static extern int DwmIsCompositionEnabled(ref int pfEnabled);

        private bool _mAeroEnabled;
        private const int CsDropshadow = 0x00020000;
        private const int WmNcpaint = 0x0085;
        //private const int WmActivateapp = 0x001C;

        public struct Margins
        {
            public int LeftWidth;
            public int RightWidth;
            public int TopHeight;
            public int BottomHeight;
        }

        private const int WmNchittest = 0x84;
        private const int Htclient = 0x1;
        private const int Htcaption = 0x2;

        protected override CreateParams CreateParams
        {
            get
            {
                _mAeroEnabled = CheckAeroEnabled();
                CreateParams cp = base.CreateParams;
                if (!_mAeroEnabled)
                {
                    cp.ClassStyle |= CsDropshadow;
                }
                return cp;
            }
        }

        private bool CheckAeroEnabled()
        {
            if (Environment.OSVersion.Version.Major >= 6)
            {
                int enabled = 0;
                DwmIsCompositionEnabled(ref enabled);
                return (enabled == 1) ? true : false;
            }
            return false;
        }

        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            switch (m.Msg)
            {
                case WmNcpaint:
                    if (_mAeroEnabled)
                    {
                        var v = 2;
                        DwmSetWindowAttribute(this.Handle, 2, ref v, 4);
                        Margins margins = new Margins()
                        {
                            BottomHeight = 1,
                            LeftWidth = 0,
                            RightWidth = 0,
                            TopHeight = 0
                        };
                        DwmExtendFrameIntoClientArea(this.Handle, ref margins);
                    }
                    break;
                default:
                    break;
            }
            base.WndProc(ref m);

            if (m.Msg == WmNchittest && (int)m.Result == Htclient)
            {
                m.Result = (IntPtr)Htcaption;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                IsSearch = true;
                lblFilter.Text = "";



                if (
                        (UtilityServices.CheckDateDiff(dtpIncomeDateFrom.Value, dtpIncomeDateTo.Value) ?? true) == false
                    ||
                        (UtilityServices.CheckDateDiff(dtpPostingDateFrom.Value, dtpPostingDateTo.Value) ?? true) == false
                    )
                {
                    MsgBox.showWarning("Date difference cannot be more than " + ConfigurationSettings.AppSettings["dayOfDiff"] + " days!");
                    IsSearch = false;
                    return;
                }
                Search(false);
                customDataGridViewHeader.ExportReportSubtitleThree = "Agent Income Information";
                DataGridHeaderUtility.ReportHederDataExportToPdf(customDataGridViewHeader);
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp.Message);
            }
        }

        private void SearchExt()
        {
            try
            {
                IsSearch = true;
                lblFilter.Text = "";



                if (
                        (UtilityServices.CheckDateDiff(dtpIncomeDateFrom.Value, dtpIncomeDateTo.Value) ?? true) == false
                    ||
                        (UtilityServices.CheckDateDiff(dtpPostingDateFrom.Value, dtpPostingDateTo.Value) ?? true) == false
                    )
                {
                    MsgBox.showWarning("Date difference cannot be more than " + ConfigurationSettings.AppSettings["dayOfDiff"] + " days!");
                    IsSearch = false;
                    return;
                }
                Search(true);
                customDataGridViewHeader.ExportReportSubtitleThree = "Agent Income Information";
                DataGridHeaderUtility.ReportHederDataExportToPdf(customDataGridViewHeader);
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp.Message);
            }
        }

        private void Search(bool dateTime )
        {
            try
            {
                dgvResult.DataSource = null;
                dgvResult.Refresh();
                dgvResult.Rows.Clear();
                dgvResult.Columns.Clear();
                AgentIncomeSearchDto agentIncomeSearchDto = new AgentIncomeSearchDto();

                if (cmbAgent.SelectedValue != null)
                { agentIncomeSearchDto.agentId = (long)cmbAgent.SelectedValue; }
                else
                { agentIncomeSearchDto.agentId = 0; }

                if (cmbOutlet.SelectedValue != null)
                { agentIncomeSearchDto.outletId = (long)cmbOutlet.SelectedValue; }
                else
                { agentIncomeSearchDto.outletId = 0; }
                //agentIncomeSearchDto.outletId = (long)(cmbOutlet.SelectedValue ?? 0);


                if (dateTime == false)
                {
                    agentIncomeSearchDto.incomeDateFrom =
                        UtilityServices.GetLongDate(new DateTime(dtpIncomeDateFrom.Value.Year,
                            dtpIncomeDateFrom.Value.Month,
                            dtpIncomeDateFrom.Value.Day));
                }
                else
                {
                    agentIncomeSearchDto.incomeDateFrom =
                        UtilityServices.GetLongDate(new DateTime(SessionInfo.currentDate.Year, SessionInfo.currentDate.Month, SessionInfo.currentDate.Day));
                }

                agentIncomeSearchDto.incomeDateTo = UtilityServices.GetLongDate(new DateTime(dtpIncomeDateTo.Value.Year,
                                                                                             dtpIncomeDateTo.Value.Month,
                                                                                             dtpIncomeDateTo.Value.Day));
                if (cmbPostingStatus.SelectedIndex > 0)
                //{ agentIncomeSearchDto.postedStatus = (PostedStatus) (cmbPostingStatus.SelectedItem ?? PostedStatus.Posted);}
                {
                    agentIncomeSearchDto.postedStatus = (PostedStatus)Enum.Parse(typeof(PostedStatus), cmbPostingStatus.SelectedValue.ToString());
                    isAllSearched = false;
                }
                else
                {
                    isAllSearched = true;
                }

                _agentIncomeSearchResultDto = (new AgentIncomeService()).GetAgentIncomeSearchResultList(agentIncomeSearchDto);
                LoadItems(_agentIncomeSearchResultDto);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        private void LoadItems(List<AgentIncomeSearchResultDto> agentIncomeSearchResultDto)
        {
            try
            {
                dgvResult.DataSource = agentIncomeSearchResultDto.Select(o => new AgentIncomeSearchResultDtoGrid(o)
                {
                    IncomeDate = UtilityServices.getDateFromLong(o.incomeDate ?? 0).ToString("dd-MMM-yyyy"),
                    //IncomeDate = o.incomeDate.ToString("dd-MMM-yyyy"),
                    IncomeFrom = o.incomeFrom.ToString(),
                    Amount = decimal.Parse(string.Format("{0:F2}", (o.amount ?? 0)), NumberStyles.AllowDecimalPoint),
                    Outlet = o.outletName,
                    User = o.userName,
                    PostedStatus = o.postedStatus.ToString(),
                    PostingDate = ((o.postingDate ?? 0) != 0) ? UtilityServices.getDateFromLong(o.postingDate ?? 0).ToString("dd-MMM-yyyy") : "",

                    filter =
                    (
                        o.incomeDate +
                        o.incomeFrom.ToString() +
                        o.amount +
                        o.outletName +
                        o.userName +
                        o.postedStatus +
                        o.postingDate
                    ).ToLower()
                }).ToList();
                dgvResult.Columns["filter"].Visible = false;
                lblResultCount.Text = "   Result : " + dgvResult.Rows.Count + " Item(s)";
                CalculateTotal(agentIncomeSearchResultDto);
                CalculateDGVColWidth();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        private void CalculateDGVColWidth()
        {
            dgvResult.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgvResult.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgvResult.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgvResult.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;

            dgvResult.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgvResult.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgvResult.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;


        }

        private void CalculateTotal(List<AgentIncomeSearchResultDto> agentIncomeSearchResultDto)
        {
            if (IsSearch == true)
            {
                SortedDictionary<string, decimal> totalList = new SortedDictionary<string, decimal>();

                string incFrom = "";
                for (int i = 0; i < dgvResult.Rows.Count; i++)
                {
                    incFrom = dgvResult.Rows[i].Cells[1].Value.ToString();
                    if (totalList.ContainsKey(incFrom))
                    {
                        totalList[incFrom] = totalList[incFrom] + (decimal)dgvResult.Rows[i].Cells[2].Value;
                    }
                    else
                    {
                        totalList.Add(incFrom, (decimal)dgvResult.Rows[i].Cells[2].Value);
                    }
                }

                _agentIncomeTotal = totalList.Select(o => new AgentIncomeTotalGrid(o)
                {
                    IncomeForm = o.Key,
                    Amount = o.Value

                }).ToList();

                //dgvTotal.DataSource = totalList.Select(o => new AgentIncomeTotalGrid(o)
                //{
                //    IncomeForm = o.Key,
                //    Amount = o.Value

                //}).ToList();                
                dgvTotal.DataSource = _agentIncomeTotal;
                dgvTotal.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvTotal.Columns[1].Width = 130;

                dgvTotal.DefaultCellStyle.Font = new Font(llblBalance.Font.FontFamily, dgvTotal.DefaultCellStyle.Font.Size, FontStyle.Bold);


                decimal total = 0;
                pieChart.Series[0].Points.Clear();
                pieChart.Legends[0].CustomItems.Clear();
                for (int i = 0; i < totalList.Count; i++)
                {
                    total += totalList.Values.ElementAt(i);
                    pieChart.Series[0].Points.AddXY(i, totalList.Values.ElementAt((i)));
                    pieChart.Series[0].Points[i].LegendText = totalList.Keys.ElementAt((i));

                    //pieChart.Legends[0].CustomItems.RemoveAt(i);
                }
                //for (int i = 0; i < totalList.Count; i++)
                //{
                //    pieChart.Legends[0].CustomItems[i].Cells[0].Text = totalList.Keys.ElementAt((i));
                //    //(pieChart.Series[0].Points[i].Color, totalList.Keys.ElementAt((i)));
                //}

                lblTotalValue.Text = total.ToString("#.##");
            }
        }

        public class AgentIncomeTotalGrid
        {
            private KeyValuePair<string, decimal> o;

            public AgentIncomeTotalGrid(KeyValuePair<string, decimal> o)
            {
                this.o = o;
            }

            public string IncomeForm { get; set; }
            public decimal Amount { get; set; }
        }

        public class AgentIncomeSearchResultDtoGrid
        {
            public string IncomeDate { get; set; }
            public string IncomeFrom { get; set; }
            public decimal Amount { get; set; }
            public string Outlet { get; set; }
            public string User { get; set; }
            public string PostedStatus { get; set; }
            public string PostingDate { get; set; }
            public string filter { get; set; }

            private AgentIncomeSearchResultDto _obj;

            public AgentIncomeSearchResultDtoGrid(AgentIncomeSearchResultDto obj)
            {
                _obj = obj;
            }

            public AgentIncomeSearchResultDto GetModel()
            {
                return _obj;
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void Reset()
        {
            if (SessionInfo.userBasicInformation.userCategory != UserCategory.SubAgentUser
                &&
                SessionInfo.userBasicInformation.userCategory != UserCategory.AgentUser)
            { cmbAgent.SelectedIndex = -1; }

            if (SessionInfo.userBasicInformation.userCategory != UserCategory.SubAgentUser)
            { cmbOutlet.SelectedIndex = -1; }

            dtpIncomeDateFrom.Value = SessionInfo.currentDate;
            dtpIncomeDateTo.Value = SessionInfo.currentDate;
            cmbPostingStatus.SelectedIndex = -1;

            dgvResult.DataSource = null;
            dgvResult.Refresh();
            lblResultCount.Text = "   Result : 0 Item(s)";
            dgvTotal.DataSource = null;
            dgvTotal.Refresh();

            pieChart.Series[0].Points.Clear();
            pieChart.Legends[0].CustomItems.Clear();
            lblTotalValue.Text = "0.00";
            lblFilter.Text = "";

            //txtUser.Text = "";
            //cmbIncomeFrom.SelectedIndex = -1;
            //dtpPostingDateFrom.Value = SessionInfo.currentDate;
            //dtpPostingDateFrom.Value = SessionInfo.currentDate;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbAgent_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmbAgent.SelectedIndex > 0)
                {
                    LoadSubagents();
                }
                else
                {
                    cmbOutlet.DataSource = null;
                    cmbOutlet.Items.Clear();
                    return;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        private void LoadSubagents()
        {
            try
            {
                List<SubAgentInformation> subAgentInformationList = agentServices.GetSubagentsByAgentId(long.Parse(cmbAgent.SelectedValue.ToString()));

                //SubAgentInformation saiSelect = new SubAgentInformation();
                //saiSelect.name = "(Select)";
                //subAgentInformationList.Insert(0, saiSelect);
                SubAgentInformation saiAll = new SubAgentInformation();
                saiAll.name = "(All)";
                saiAll.id = 0;
                subAgentInformationList.Insert(0, saiAll);

                BindingSource bs = new BindingSource();
                bs.DataSource = subAgentInformationList;
                UtilityServices.fillComboBox(cmbOutlet, bs, "name", "id");
                cmbOutlet.SelectedIndex = 0;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        private void dgvTotal_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                IsSearch = false;
                List<AgentIncomeSearchResultDto> agentIncomeSearchResultDtoList = _agentIncomeSearchResultDto.Where(o => o.incomeFrom == dgvTotal.Rows[e.RowIndex].Cells[0].Value.ToString()).ToList();
                dgvResult.DataSource = null;
                LoadItems(agentIncomeSearchResultDtoList);
                lblFilter.Text = "Filtered By: " + dgvTotal.Rows[e.RowIndex].Cells[0].Value.ToString() + "   ";

            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp.Message);
            }
        }

        #region Report
        private void btnSummaryReport_Click(object sender, EventArgs e)
        {
            try
            {
                if (!isAllSearched)
                {
                    MsgBox.showWarning("Report is available only for posting status 'All'.");
                    return;
                }
                crAgentIncomeStatement report = new crAgentIncomeStatement();
                frmReportViewer viewer = new frmReportViewer();

                ReportHeaders rptHeaders = new ReportHeaders();
                rptHeaders = UtilityServices.getReportHeaders("Agent Income Statement");

                TextObject txtBankName = report.ReportDefinition.ReportObjects["txtBankName"] as TextObject;
                TextObject txtBranchName = report.ReportDefinition.ReportObjects["txtBranchName"] as TextObject;
                TextObject txtBranchAddress = report.ReportDefinition.ReportObjects["txtBranchAddress"] as TextObject;
                TextObject txtReportHeading = report.ReportDefinition.ReportObjects["txtReportHeading"] as TextObject;
                TextObject txtPrintUser = report.ReportDefinition.ReportObjects["txtPrintUser"] as TextObject;
                TextObject txtPrintDate = report.ReportDefinition.ReportObjects["txtPrintDate"] as TextObject;
                TextObject txtFromDate = report.ReportDefinition.ReportObjects["txtFromDate"] as TextObject;
                TextObject txtToDate = report.ReportDefinition.ReportObjects["txtToDate"] as TextObject;

                if (rptHeaders != null)
                {
                    if (rptHeaders.branchDto != null)
                    {
                        txtBankName.Text = rptHeaders.branchDto.bankName;
                        txtBranchName.Text = rptHeaders.branchDto.branchName;
                        txtBranchAddress.Text = rptHeaders.branchDto.branchAddress;
                    }
                    txtReportHeading.Text = rptHeaders.reportHeading;
                    txtPrintUser.Text = rptHeaders.printUser;
                    txtPrintDate.Text = rptHeaders.printDate.ToString("dd-MM-yyyy").Replace("-", "/");
                }

                txtFromDate.Text = dtpIncomeDateFrom.Date;
                txtToDate.Text = dtpIncomeDateTo.Date;

                loadAgentIncomeStatement();

                report.SetDataSource(_agentIncomeStatList);

                viewer.crvReportViewer.ReportSource = report;
                viewer.Show(this.Parent);
            }
            catch (Exception exp)
            { MsgBox.ShowError(exp.Message); }
        }
        private void loadAgentIncomeStatement()
        {
            AccountSearchDto accountSearchDto = FillAccountSearchDto();
            try
            {
                _agentIncomeStatList = (new AgentServices()).getAgentIncomeStatementDtoList(accountSearchDto);
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }
        private AccountSearchDto FillAccountSearchDto()
        {
            AccountSearchDto accountSearchDto = new AccountSearchDto();

            if (cmbAgent.SelectedValue != null)
            { accountSearchDto.agent = new AgentInformation { id = (long)cmbAgent.SelectedValue }; }
            else
            { accountSearchDto.agent = null; }

            if (cmbOutlet.SelectedValue != null)
            { accountSearchDto.subAgent = new SubAgentInformation { id = (long)cmbOutlet.SelectedValue }; }
            else
            { accountSearchDto.subAgent = null; }

            accountSearchDto.product = null;
            accountSearchDto.fromDate = UtilityServices.GetLongDate(dtpIncomeDateFrom.Value);
            accountSearchDto.toDate = UtilityServices.GetLongDate(dtpIncomeDateTo.Value);

            return accountSearchDto;
        }
        #endregion

        private void btnDataVisualization_Click(object sender, EventArgs e)
        {
            //if (_agentIncomeSearchResultDto == null)
            //{
            //    return;
            //}

            //frmTemp frm = new TempUI.frmTemp();

            //Dictionary<string, decimal> dataToVisualize = new Dictionary<string, decimal>();

            //string tempDate = null;
            //decimal? totalOnDate = 0;
            //for (int i = 0; i < _agentIncomeSearchResultDto.Count; i++)
            //{
            //    if (tempDate != UtilityServices.getBDFormattedDateFromLong(_agentIncomeSearchResultDto[i].incomeDate))
            //    {
            //        string date = UtilityServices.getBDFormattedDateFromLong(_agentIncomeSearchResultDto[i].incomeDate);
            //        if (!dataToVisualize.Keys.Contains(date))
            //        {
            //            dataToVisualize.Add(date, totalOnDate ?? 0);
            //        }
            //        else
            //        {
            //            dataToVisualize[date] += _agentIncomeSearchResultDto[i].amount ?? 0;
            //        }
            //        tempDate = UtilityServices.getBDFormattedDateFromLong(_agentIncomeSearchResultDto[i].incomeDate);
            //        totalOnDate = _agentIncomeSearchResultDto[i].amount;
            //    }
            //    else
            //    {
            //        totalOnDate += _agentIncomeSearchResultDto[i].amount;
            //    }
            //}

            //frm.customVisualizerDataByHour1.BindingDictionaryObject = dataToVisualize;
            //frm.customVisualizerDataByHour1.RenderChart();
            //frm.ShowDialog(this);
        }

        private void dgvTotal_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }

    internal class PostedStatusCombo
    {
        public string postedStatus { get; set; }
    }
}