﻿namespace MISL.Ababil.Agent.UI.forms
{
    partial class frmCustomerMobileNoUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblConsumerTitle = new System.Windows.Forms.Label();
            this.photo = new System.Windows.Forms.GroupBox();
            this.pic_conusmer = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblRequiredFingerPrint = new System.Windows.Forms.Label();
            this.fingerPrintGrid = new System.Windows.Forms.DataGridView();
            this.grdColIndividualId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mobil = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnCol = new System.Windows.Forms.DataGridViewButtonColumn();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.btnMobileNoUpdate = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtReamarks = new System.Windows.Forms.TextBox();
            this.mandatoryMark1 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.btnclear = new System.Windows.Forms.Button();
            this.lblMobileNo = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtConsumerAccount = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.photo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_conusmer)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fingerPrintGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(55, 129);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label4.Size = new System.Drawing.Size(76, 16);
            this.label4.TabIndex = 12;
            this.label4.Text = "Mobile No :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(29, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 16);
            this.label3.TabIndex = 10;
            this.label3.Text = "Account Name :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 25);
            this.label1.TabIndex = 8;
            this.label1.Text = "Account :";
            // 
            // lblConsumerTitle
            // 
            this.lblConsumerTitle.AutoSize = true;
            this.lblConsumerTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConsumerTitle.Location = new System.Drawing.Point(135, 104);
            this.lblConsumerTitle.Name = "lblConsumerTitle";
            this.lblConsumerTitle.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblConsumerTitle.Size = new System.Drawing.Size(113, 16);
            this.lblConsumerTitle.TabIndex = 11;
            this.lblConsumerTitle.Text = "lbvConsumerTitle";
            this.lblConsumerTitle.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // photo
            // 
            this.photo.Controls.Add(this.pic_conusmer);
            this.photo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.photo.Location = new System.Drawing.Point(453, 34);
            this.photo.Name = "photo";
            this.photo.Size = new System.Drawing.Size(135, 162);
            this.photo.TabIndex = 16;
            this.photo.TabStop = false;
            this.photo.Text = "Photo";
            // 
            // pic_conusmer
            // 
            this.pic_conusmer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_conusmer.Location = new System.Drawing.Point(10, 23);
            this.pic_conusmer.Name = "pic_conusmer";
            this.pic_conusmer.Size = new System.Drawing.Size(116, 130);
            this.pic_conusmer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_conusmer.TabIndex = 5;
            this.pic_conusmer.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblRequiredFingerPrint);
            this.groupBox2.Controls.Add(this.fingerPrintGrid);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 203);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(576, 167);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Account Owner Info";
            // 
            // lblRequiredFingerPrint
            // 
            this.lblRequiredFingerPrint.AutoSize = true;
            this.lblRequiredFingerPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequiredFingerPrint.Location = new System.Drawing.Point(378, 12);
            this.lblRequiredFingerPrint.Name = "lblRequiredFingerPrint";
            this.lblRequiredFingerPrint.Size = new System.Drawing.Size(0, 15);
            this.lblRequiredFingerPrint.TabIndex = 1;
            // 
            // fingerPrintGrid
            // 
            this.fingerPrintGrid.AllowUserToAddRows = false;
            this.fingerPrintGrid.AllowUserToDeleteRows = false;
            this.fingerPrintGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fingerPrintGrid.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.fingerPrintGrid.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.fingerPrintGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.fingerPrintGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.grdColIndividualId,
            this.Column2,
            this.Mobil,
            this.btnCol});
            this.fingerPrintGrid.Location = new System.Drawing.Point(12, 29);
            this.fingerPrintGrid.Name = "fingerPrintGrid";
            this.fingerPrintGrid.RowHeadersVisible = false;
            this.fingerPrintGrid.Size = new System.Drawing.Size(553, 125);
            this.fingerPrintGrid.TabIndex = 0;
            this.fingerPrintGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.fingerPrintGrid_CellContentClick);
            this.fingerPrintGrid.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.fingerPrintGrid_CellValidating);
            // 
            // grdColIndividualId
            // 
            this.grdColIndividualId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.grdColIndividualId.FillWeight = 162.4366F;
            this.grdColIndividualId.HeaderText = "Individual ID";
            this.grdColIndividualId.Name = "grdColIndividualId";
            this.grdColIndividualId.ReadOnly = true;
            this.grdColIndividualId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.grdColIndividualId.Width = 120;
            // 
            // Column2
            // 
            this.Column2.FillWeight = 37.56345F;
            this.Column2.HeaderText = "Operator Name";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 220;
            // 
            // Mobil
            // 
            this.Mobil.HeaderText = "Mobile No";
            this.Mobil.MaxInputLength = 11;
            this.Mobil.Name = "Mobil";
            this.Mobil.Width = 110;
            // 
            // btnCol
            // 
            this.btnCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.btnCol.HeaderText = "";
            this.btnCol.Name = "btnCol";
            this.btnCol.Text = "View";
            this.btnCol.Width = 80;
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(600, 26);
            this.customTitlebar1.TabIndex = 17;
            this.customTitlebar1.TabStop = false;
            // 
            // btnMobileNoUpdate
            // 
            this.btnMobileNoUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnMobileNoUpdate.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnMobileNoUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMobileNoUpdate.ForeColor = System.Drawing.Color.White;
            this.btnMobileNoUpdate.Location = new System.Drawing.Point(477, 421);
            this.btnMobileNoUpdate.Name = "btnMobileNoUpdate";
            this.btnMobileNoUpdate.Size = new System.Drawing.Size(111, 28);
            this.btnMobileNoUpdate.TabIndex = 19;
            this.btnMobileNoUpdate.Text = "Update";
            this.btnMobileNoUpdate.UseVisualStyleBackColor = false;
            this.btnMobileNoUpdate.Click += new System.EventHandler(this.btnMobileNoUpdate_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 381);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Reason of change :";
            // 
            // txtReamarks
            // 
            this.txtReamarks.Location = new System.Drawing.Point(135, 378);
            this.txtReamarks.MaxLength = 180;
            this.txtReamarks.Multiline = true;
            this.txtReamarks.Name = "txtReamarks";
            this.txtReamarks.Size = new System.Drawing.Size(420, 34);
            this.txtReamarks.TabIndex = 21;
            // 
            // mandatoryMark1
            // 
            this.mandatoryMark1.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark1.Location = new System.Drawing.Point(561, 378);
            this.mandatoryMark1.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.Name = "mandatoryMark1";
            this.mandatoryMark1.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.TabIndex = 22;
            // 
            // btnclear
            // 
            this.btnclear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnclear.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnclear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnclear.ForeColor = System.Drawing.Color.White;
            this.btnclear.Location = new System.Drawing.Point(12, 421);
            this.btnclear.Name = "btnclear";
            this.btnclear.Size = new System.Drawing.Size(111, 28);
            this.btnclear.TabIndex = 19;
            this.btnclear.Text = "Clear";
            this.btnclear.UseVisualStyleBackColor = false;
            this.btnclear.Click += new System.EventHandler(this.btnclear_Click);
            // 
            // lblMobileNo
            // 
            this.lblMobileNo.BackColor = System.Drawing.Color.White;
            this.lblMobileNo.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.MobileNumber;
            this.lblMobileNo.InputScopeAllowEmpty = false;
            this.lblMobileNo.InputScopeCustomString = null;
            this.lblMobileNo.IsValid = null;
            this.lblMobileNo.Location = new System.Drawing.Point(137, 129);
            this.lblMobileNo.MaxLength = 11;
            this.lblMobileNo.MinimumSize = new System.Drawing.Size(0, 21);
            this.lblMobileNo.Name = "lblMobileNo";
            this.lblMobileNo.PromptText = "(Type here...)";
            this.lblMobileNo.ReadOnly = false;
            this.lblMobileNo.ShowMandatoryMark = false;
            this.lblMobileNo.Size = new System.Drawing.Size(152, 21);
            this.lblMobileNo.TabIndex = 23;
            this.lblMobileNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.lblMobileNo.ValidationErrorMessage = "Validation Error!";
            // 
            // txtConsumerAccount
            // 
            this.txtConsumerAccount.BackColor = System.Drawing.Color.White;
            this.txtConsumerAccount.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Numeric;
            this.txtConsumerAccount.InputScopeAllowEmpty = false;
            this.txtConsumerAccount.InputScopeCustomString = null;
            this.txtConsumerAccount.IsValid = null;
            this.txtConsumerAccount.Location = new System.Drawing.Point(137, 70);
            this.txtConsumerAccount.MaxLength = 13;
            this.txtConsumerAccount.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtConsumerAccount.Name = "txtConsumerAccount";
            this.txtConsumerAccount.PromptText = "(Type here...)";
            this.txtConsumerAccount.ReadOnly = false;
            this.txtConsumerAccount.ShowMandatoryMark = false;
            this.txtConsumerAccount.Size = new System.Drawing.Size(205, 21);
            this.txtConsumerAccount.TabIndex = 26;
            this.txtConsumerAccount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtConsumerAccount.ValidationErrorMessage = "Validation Error!";
            this.txtConsumerAccount.Leave += new System.EventHandler(this.txtConsumerAccount_Leave);
            // 
            // frmCustomerMobileNoUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(600, 461);
            this.Controls.Add(this.lblMobileNo);
            this.Controls.Add(this.txtConsumerAccount);
            this.Controls.Add(this.btnclear);
            this.Controls.Add(this.btnMobileNoUpdate);
            this.Controls.Add(this.mandatoryMark1);
            this.Controls.Add(this.txtReamarks);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.photo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblConsumerTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCustomerMobileNoUpdate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Customer Mobile No. Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDepositAccountOperator_FormClosing);
            this.photo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_conusmer)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fingerPrintGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblConsumerTitle;
        private System.Windows.Forms.GroupBox photo;
        private System.Windows.Forms.PictureBox pic_conusmer;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblRequiredFingerPrint;
        private System.Windows.Forms.DataGridView fingerPrintGrid;
        private Agent.CustomControls.CustomTitlebar customTitlebar1;
        private System.Windows.Forms.Button btnMobileNoUpdate;
        private System.Windows.Forms.Label label2;
        private CustomControls.MandatoryMark mandatoryMark1;
        private System.Windows.Forms.TextBox txtReamarks;
        private System.Windows.Forms.Button btnclear;
        private Agent.CustomControls.CustomTextBox lblMobileNo;
        private Agent.CustomControls.CustomTextBox txtConsumerAccount;
        private System.Windows.Forms.DataGridViewButtonColumn btnCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mobil;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdColIndividualId;
    }
}