﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.consumer;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Infrastructure.Validation;
using MISL.Ababil.Agent.Services;

namespace MISL.Ababil.Agent.UI.forms
{
    public partial class frmDepositAccountOperator : CustomForm
    {
        private ConsumerServices _consumerService = new ConsumerServices();
        private ConsumerInformationDto _consumerInformationDto = new ConsumerInformationDto();
        private string _captureFor;
        private int _captureIndexNo;
        private int _noOfCapturefinger = 0;
        private byte[] _capturefingerData;
        private byte[] _subagentFingerData;
        private bool? _isManualChargeApplicable = null;
        private decimal _chargeAmount;
        private AccountOperatorDto _accountOperatorDto;

        public frmDepositAccountOperator()
        {
            InitializeComponent();
            lblConsumerTitle.Text = "";
            lblMobileNo.Text = "";
        }

        private void txtConsumerAccount_Leave(object sender, EventArgs e)
        {
            
            if (txtConsumerAccount.Text.IndexOf("990") == 0)
            {
                LoadConsumerInformation();
            }
            else
            {
                MsgBox.showWarning("Please enter an agent banking Account No.");
            }
                
            
        }

        private void LoadConsumerInformation()
        {
            if (ValidationManager.ValidateNonEmptyTextWithoutSpace(txtConsumerAccount.Text))
            {
                try
                {
                    _consumerInformationDto = _consumerService.getConsumerInformationDtoByAcc(txtConsumerAccount.Text);
                    lblConsumerTitle.Text = _consumerInformationDto.consumerTitle;
                    lblMobileNo.Text = _consumerInformationDto.mobileNumber;
                    //lblBalanceValue.Text = (_consumerInformationDto.balance ?? 0).ToString("N", new CultureInfo("BN-BD"));

                    if (_consumerInformationDto.photo != null)
                    {
                        byte[] bytes = Convert.FromBase64String(_consumerInformationDto.photo);
                        Image image = UtilityServices.byteArrayToImage(bytes);
                        pic_conusmer.Image = image;
                    }

                    //if (_consumerInformationDto.accountOperators != null)
                    //{
                    //    //fingerPrintGrid.DataSource = _consumerInformationDto.accountOperators.Select(o => new operatorfingerPrintGrid(o) { identity = o.identity, identityName = o.identityName }).ToList();
                    //}

                    LoadOperators(txtConsumerAccount.Text);
                }
                catch (Exception ex)
                {
                    MsgBox.ShowError(ex.Message);

                    lblConsumerTitle.Text = "";
                    lblMobileNo.Text = "";
                    pic_conusmer.Image = null;
                    fingerPrintGrid.DataSource = null;
                    lblRequiredFingerPrint.Text = null;
                }
            }
        }


        private void LoadOperators(string accountNumber)
        {
            //GetAccountOperatorDtoByAccountNumber
            _accountOperatorDto = new AccountInformationService().GetAccountOperatorDtoByAccountNumber(accountNumber);
            fingerPrintGrid.Rows.Clear();
            for (int i = 0; i < _accountOperatorDto.operators.Count; i++)
            {
                fingerPrintGrid.Rows.Add
                    (
                        _accountOperatorDto.operators[i].individualId,
                        _accountOperatorDto.operators[i].individualName, "View"
                    );
            }
        }

        private void frmDepositAccountOperator_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void fingerPrintGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.ColumnIndex == 2) //View
            {
                new frmIndividualInformation
                (
                    new Packet()
                    {
                        actionType = FormActionType.View
                    }, new IndividualServices().GetIndividualInfo
                        (
                            int.Parse(fingerPrintGrid.Rows[e.RowIndex].Cells[0].Value.ToString())
                        )
                ).ShowDialog(this);
            }
        }
    }
}