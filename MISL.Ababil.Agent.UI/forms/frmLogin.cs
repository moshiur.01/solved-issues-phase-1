﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.Infrastructure.Validation;
using MISL.Ababil.Agent.Communication;
using MISL.Ababil.Agent.UI.forms.ProgressUI;
using MISL.Ababil.Agent.LocalStorageService;
using System.Threading.Tasks;
using MISL.Ababil.Agent.Report.Reports;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.GenericFingerprintServices;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.UI.forms.RemittanceUI;

namespace MISL.Ababil.Agent.UI.forms
{

    public partial class frmLogin : Form, FingerprintEventObserver
    {
        private bool fingerPrintApplicable = false;
        LoginService loginService = new LoginService();
        private string bioTemplate = "";
        //ActivityMonitor.ActivityMonitor _am = new ActivityMonitor.ActivityMonitor();
        private bool frmClosing = false;

        public frmLogin()
        {
            this.MaximumSize = new Size(Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height);

            this.TransparencyKey = Color.Empty;
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            InitializeComponent();
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            //bio.OnCapture += new EventHandler(bio_OnCapture);
            //_am.WarningMinutes = 45; //values are changed to seconds
            //_am.MaxMinutesIdle = 300; //values are changed to seconds
            //_am.Idle += new EventHandler(am_Idle);
            //pbAbabilLogo.Image = Image.FromFile(@"Contents\Ababil_Logo.jpg");
            time.BackColor = System.Drawing.Color.Transparent;
            lblDate.BackColor = System.Drawing.Color.Transparent;
            lblAgent.BackColor = System.Drawing.Color.Transparent;
            lblBankName.BackColor = System.Drawing.Color.Transparent;
            time.Text = SessionInfo.currentDate.ToString("h:mm:ss tt");
            lblDate.Text = DateTime.Now.ToLongDateString(); //check
            lbl_version.Text = "Version: " + UpdateCom.CurrentVersion;
            lbl_version.BackColor = System.Drawing.Color.Transparent;
            lbl_version.Left = lblAgent.Left + lblAgent.Width - lbl_version.Width - 3;
            //txtVersion.Text = "Version: " + UpdateCom.CurrentVersion;


            //lblPassword.BackColor = System.Drawing.Color.Transparent;
            timer1.Enabled = true;
            timer1.Interval = 1000;

            ConfigureValidation();

            Task task2 = new Task(new Action(PreLoadOnBackground), TaskCreationOptions.LongRunning);
            task2.Start();

            //show label to indicate TEST/Production
            {
                if (UtilityServices.getConfigData("mode") == "test")
                {
                    panelTest.Visible = true;
                    panelTest.Left = this.Width - panelTest.Width + 4;
                }
                else
                {
                    panelTest.Visible = false;
                }
            }
        }

        private void ConfigureValidation()
        {
            ValidationManager.ConfigureValidation(this, txtUsername, "Username", (long)ValidationType.UserName, true);
            ValidationManager.ConfigureValidation(this, txtPassword, "Password", (long)ValidationType.Password, true);
        }

        private bool validationCheck()
        {
            return ValidationManager.ValidateForm(this);
        }

        void am_Idle(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public void enableTimer()
        {
            timer1.Enabled = true;
            txtUsername.Focus();
        }
        private void btnLogin_Click(object sender, EventArgs e)
        {
            btnLogin.Enabled = false;
            UserLogin();
            btnLogin.Enabled = true;
        }

        private void UserLogin()
        {
            ProgressUIManager.ShowProgress(this);
            try
            {
                if (validationCheck())
                {
                    if (txtUsername.Text == "")
                    {
                        MsgBox.showWarning("User name can not be left blank");
                    }
                    else
                    {
                        if (txtPassword.Text == "")
                        {
                            MsgBox.showWarning("Password can not be left blank");
                        }
                        else
                        {

                            if (UtilityServices.getConfigData("fingerPrintApplicable") == "Y")
                            {
                                fingerPrintApplicable = true;
                            }
                            else
                            {
                                fingerPrintApplicable = false;

                            }

                            AuthConfigDto authType = new LoginService().GetAuthenticationType(txtUsername.Text.Trim(), txtPassword.Text.Trim());

                            fingerPrintApplicable = false;
                            if (authType == null)
                            {
                                ProgressUIManager.CloseProgress();
                                btnLogin.Enabled = true;
                                Message.showError("Cannot determine authentication level.");
                                return;
                            }
                            if (authType.reqPassChange == false && authType.reqFingerChange == false)
                            {
                                if (authType.authType == "BIOMETRIC")
                                {
                                    fingerPrintApplicable = true;
                                    FingerprintServiceFactory.FingerprintServiceFactory factory = new FingerprintServiceFactory.FingerprintServiceFactory();
                                    FingerprintDevice device = factory.getFingerprintDevice();
                                    device.registerEventObserver(this);
                                    device.capture();
                                }
                                else if (authType.authType == "BASIC")
                                {
                                    Login();
                                }
                                else
                                {
                                    ProgressUIManager.CloseProgress();
                                    btnLogin.Enabled = true;
                                    Message.showError("Cannot determine authentication level.");
                                }
                            }
                            else

                            {
                                Login();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ProgressUIManager.CloseProgress();
                btnLogin.Enabled = true;
                MsgBox.ShowError(ex.Message);
            }
            ProgressUIManager.CloseProgress();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            exitConfirmation();
        }

        private bool exitConfirmation()
        {
            if (Message.showConfirmation("Do you want to exit the application?") == "yes")
            {
                frmClosing = true;
                timer1.Enabled = false;
                Application.Exit();
                return true;
            }
            return false;
        }

        private void clearInputControls()
        {
            txtUsername.Text = string.Empty;
            txtPassword.Text = string.Empty;
        }

        private void taskRun()
        {
            LocalStorage localStorage = new LocalStorage();
            localStorage.Sync();
        }

        private void Login()
        {
            string userName = txtUsername.Text.Trim();
            string password = txtPassword.Text.Trim();

            try
            {
                bool retVal = false;
                try
                {
                    retVal = new LoginService().authenticate(userName, password, fingerPrintApplicable, bioTemplate);
                }
                catch (Exception ex)
                {
                    ProgressUIManager.CloseProgress();
                    MsgBox.ShowError(ex.Message);
                    return;
                }

                if (retVal)
                {
                    frmMainApp _mainApp = new frmMainApp(this);
                    SessionUIStorage.MainForm = _mainApp;
                    _mainApp.Show();
                    ChangeUIOnLogin();
                    new Task(TaskRun).Start();

                    if (SessionInfo.isexpired == true && SessionInfo.isfingerexpired == true)
                    {
                        ProgressUIManager.CloseProgress();
                        ChangeUIOnExpire(_mainApp);
                        if (new frmPasswordChange(true) { btnFingerPrint = { Visible = true } }.ShowDialog() == DialogResult.OK)
                        {
                            RestartApplication();
                        }
                    }
                    else if (SessionInfo.isexpired == true && SessionInfo.isfingerexpired == false)
                    {
                        ProgressUIManager.CloseProgress();
                        ChangeUIOnExpire(_mainApp);
                        if (new frmPasswordChange(true) { btnFingerPrint = { Visible = false } }.ShowDialog() == DialogResult.OK)
                        {
                            RestartApplication();
                        }
                    }
                    else if (SessionInfo.isexpired == false && SessionInfo.isfingerexpired == true)
                    {
                        ProgressUIManager.CloseProgress();
                        ChangeUIOnExpire(_mainApp);
                        if (new frmFingerPrintChange(true).ShowDialog() == DialogResult.OK)
                        {
                            RestartApplication();
                        }
                    }
                }
                else
                {
                    MsgBox.ShowError("Invalid credential!");
                }
            }
            catch (Exception ex)
            {
                ProgressUIManager.CloseProgress();
                //if (ex.Message == "Invalid user information")
                //{
                //    MsgBox.ShowError("Invalid credential!");
                //}
                //else
                //{
                MsgBox.ShowError(ex.Message);
                //}
            }
            ProgressUIManager.CloseProgress();
        }

        private void ChangeUIOnLogin()
        {
            ClearInputControls();
            timer1.Enabled = false;
            this.Hide();
            SessionUIStorage.StorgedLoginForm = this;
        }

        private static void ChangeUIOnExpire(frmMainApp mainApp)
        {
            mainApp.Enabled = false;
            mainApp.MainMenuStrip.Enabled = false;
        }

        private static void RestartApplication()
        {
            Application.ExitThread();
            System.Threading.Thread.Sleep(200);
            Application.Restart();
        }

        private void ClearInputControls()
        {
            txtUsername.Text = string.Empty;
            txtPassword.Text = string.Empty;
        }

        private static void TaskRun()
        {
            LocalStorage localStorage = new LocalStorage();
            localStorage.Sync();
        }

        private void PreLoadOnBackground()
        {
            frmReportViewer frm = new frmReportViewer();
            crBlankReport repObj = new crBlankReport();
            frm.crvReportViewer.ReportSource = repObj;
            frm.Width = 0;
            frm.Height = 0;
            frm.ShowInTaskbar = false;
            frm.FormBorderStyle = FormBorderStyle.None;
            frm.WindowState = FormWindowState.Minimized;
            frm.Show();
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //btnLogin.Focus();                
                UserLogin();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            time.Text = DateTime.Now.ToString("h:mm:ss tt");
        }

        private void time_Click(object sender, EventArgs e)
        {

        }

        private void txtUsername_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtPassword.Focus();
            }
        }

        private void frmLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (frmClosing == false)
            {
                frmClosing = true;
                if (exitConfirmation() == false)
                {
                    e.Cancel = true;
                    frmClosing = false;
                }
                else
                {
                    frmClosing = true;
                    ValidationManager.ReleaseValidationData(this);
                }
            }
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            //ConfigureValidation();
            txtUsername.Focus();
        }

        private void frmLogin_Paint(object sender, PaintEventArgs e)
        {
            ////this.BackImage = this.BackgroundImage;
            //Graphics g = this.CreateGraphics();
            ////g.DrawImage(this.BackgroundImage, 0, 0, this.Width, this.Height);
            //g.DrawRectangle(new Pen(Color.FromArgb(0, 68, 68, 68)), 0, 0, this.Width, this.Height);
        }

        private void frmLogin_Activated(object sender, EventArgs e)
        {
            txtUsername.Focus();
        }

        public void FingerPrintEventOccured(FingerprintEvents eventSpec, object EventData)
        {
            if (EventData == null)
            {
                ProgressUIManager.CloseProgress();
                return;
            }
            bioTemplate = EventData.ToString(); //bio.GetSafeLeftFingerData();
            if (bioTemplate != "")
            {
                Login();
            }
            else
            {
                Message.showWarning("Finger print not found");
                ProgressUIManager.CloseProgress();
            }
        }

        private void frmLogin_Shown(object sender, EventArgs e)
        {
            txtUsername.Focus();
        }

        private void frmLogin_VisibleChanged(object sender, EventArgs e)
        {
            timer1.Enabled = this.Visible;
        }

        public void EnableTimer()
        {
            timer1.Enabled = true;
            txtUsername.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {            
            
        }
    }
}