﻿namespace MISL.Ababil.Agent.UI.forms
{
    partial class frmUpdater
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pgbDownloadProgress = new System.Windows.Forms.ProgressBar();
            this.lblCheckForUpdates = new System.Windows.Forms.Label();
            this.lblUpdateStatus = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pgbDownloadProgress
            // 
            this.pgbDownloadProgress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pgbDownloadProgress.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.pgbDownloadProgress.Location = new System.Drawing.Point(12, 115);
            this.pgbDownloadProgress.Name = "pgbDownloadProgress";
            this.pgbDownloadProgress.Size = new System.Drawing.Size(557, 30);
            this.pgbDownloadProgress.TabIndex = 0;
            this.pgbDownloadProgress.Value = 50;
            this.pgbDownloadProgress.Visible = false;
            this.pgbDownloadProgress.Click += new System.EventHandler(this.pgbDownloadProgress_Click);
            // 
            // lblCheckForUpdates
            // 
            this.lblCheckForUpdates.AutoSize = true;
            this.lblCheckForUpdates.BackColor = System.Drawing.Color.Transparent;
            this.lblCheckForUpdates.ForeColor = System.Drawing.Color.Black;
            this.lblCheckForUpdates.Location = new System.Drawing.Point(472, 25);
            this.lblCheckForUpdates.Name = "lblCheckForUpdates";
            this.lblCheckForUpdates.Size = new System.Drawing.Size(119, 13);
            this.lblCheckForUpdates.TabIndex = 1;
            this.lblCheckForUpdates.Text = "Checking for Updates...";
            this.lblCheckForUpdates.Visible = false;
            // 
            // lblUpdateStatus
            // 
            this.lblUpdateStatus.AutoSize = true;
            this.lblUpdateStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblUpdateStatus.ForeColor = System.Drawing.Color.Black;
            this.lblUpdateStatus.Location = new System.Drawing.Point(535, 56);
            this.lblUpdateStatus.Name = "lblUpdateStatus";
            this.lblUpdateStatus.Size = new System.Drawing.Size(112, 13);
            this.lblUpdateStatus.TabIndex = 2;
            this.lblUpdateStatus.Text = "Downloading Updates";
            this.lblUpdateStatus.Visible = false;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(581, 38);
            this.label1.TabIndex = 3;
            this.label1.Text = "Ababil Agent Banking";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblUpdateStatus);
            this.panel1.Controls.Add(this.lblCheckForUpdates);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(581, 58);
            this.panel1.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(557, 23);
            this.label2.TabIndex = 5;
            this.label2.Text = "Updating";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // frmUpdater
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(581, 156);
            this.ControlBox = false;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pgbDownloadProgress);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmUpdater";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agent Banking Updater";
            this.Load += new System.EventHandler(this.frmUpdater_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ProgressBar pgbDownloadProgress;
        private System.Windows.Forms.Label lblCheckForUpdates;
        private System.Windows.Forms.Label lblUpdateStatus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
    }
}