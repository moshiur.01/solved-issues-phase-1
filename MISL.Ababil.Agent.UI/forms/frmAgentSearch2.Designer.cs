﻿using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.UI.forms
{
    partial class frmAgentSearch2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAgentCode = new System.Windows.Forms.Label();
            this.txtAgentCode = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.lblBusinessName = new System.Windows.Forms.Label();
            this.txtBusinessName = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.lblCreationDateFrom = new System.Windows.Forms.Label();
            this.lblCreationDateTo = new System.Windows.Forms.Label();
            this.cmbTransactionStatus = new MetroFramework.Controls.MetroComboBox();
            this.cmbApprovalStatus = new MetroFramework.Controls.MetroComboBox();
            this.lblTransactionStatus = new System.Windows.Forms.Label();
            this.lblApprovalStatus = new System.Windows.Forms.Label();
            this.dgResults = new System.Windows.Forms.DataGridView();
            this.btnDownload = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnView = new System.Windows.Forms.Button();
            this.lblMobileNumber = new System.Windows.Forms.Label();
            this.txtMobileBumber = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.dtpCreationDateTo = new MISL.Ababil.Agent.UI.forms.CustomControls.CustomDateTimePicker();
            this.dtpCreationDateFrom = new MISL.Ababil.Agent.UI.forms.CustomControls.CustomDateTimePicker();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgResults)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblAgentCode
            // 
            this.lblAgentCode.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblAgentCode.AutoSize = true;
            this.lblAgentCode.Location = new System.Drawing.Point(10, 20);
            this.lblAgentCode.Name = "lblAgentCode";
            this.lblAgentCode.Size = new System.Drawing.Size(69, 13);
            this.lblAgentCode.TabIndex = 0;
            this.lblAgentCode.Text = "Agent Code :";
            // 
            // txtAgentCode
            // 
            this.txtAgentCode.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtAgentCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtAgentCode.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtAgentCode.InputScopeAllowEmpty = false;
            this.txtAgentCode.InputScopeCustomString = null;
            this.txtAgentCode.IsValid = null;
            this.txtAgentCode.Location = new System.Drawing.Point(84, 17);
            this.txtAgentCode.MaxLength = 32767;
            this.txtAgentCode.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtAgentCode.Name = "txtAgentCode";
            this.txtAgentCode.PromptText = "(Type here...)";
            this.txtAgentCode.ReadOnly = false;
            this.txtAgentCode.ShowMandatoryMark = false;
            this.txtAgentCode.Size = new System.Drawing.Size(113, 21);
            this.txtAgentCode.TabIndex = 1;
            this.txtAgentCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtAgentCode.UpperCaseOnly = false;
            this.txtAgentCode.ValidationErrorMessage = "Validation Error!";
            // 
            // lblBusinessName
            // 
            this.lblBusinessName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblBusinessName.AutoSize = true;
            this.lblBusinessName.Location = new System.Drawing.Point(205, 20);
            this.lblBusinessName.Name = "lblBusinessName";
            this.lblBusinessName.Size = new System.Drawing.Size(86, 13);
            this.lblBusinessName.TabIndex = 2;
            this.lblBusinessName.Text = "Business Name :";
            // 
            // txtBusinessName
            // 
            this.txtBusinessName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtBusinessName.BackColor = System.Drawing.SystemColors.Window;
            this.txtBusinessName.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtBusinessName.InputScopeAllowEmpty = false;
            this.txtBusinessName.InputScopeCustomString = null;
            this.txtBusinessName.IsValid = null;
            this.txtBusinessName.Location = new System.Drawing.Point(296, 17);
            this.txtBusinessName.MaxLength = 32767;
            this.txtBusinessName.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtBusinessName.Name = "txtBusinessName";
            this.txtBusinessName.PromptText = "(Type here...)";
            this.txtBusinessName.ReadOnly = false;
            this.txtBusinessName.ShowMandatoryMark = false;
            this.txtBusinessName.Size = new System.Drawing.Size(205, 21);
            this.txtBusinessName.TabIndex = 3;
            this.txtBusinessName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtBusinessName.UpperCaseOnly = false;
            this.txtBusinessName.ValidationErrorMessage = "Validation Error!";
            // 
            // lblCreationDateFrom
            // 
            this.lblCreationDateFrom.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblCreationDateFrom.AutoSize = true;
            this.lblCreationDateFrom.Location = new System.Drawing.Point(450, 286);
            this.lblCreationDateFrom.Name = "lblCreationDateFrom";
            this.lblCreationDateFrom.Size = new System.Drawing.Size(104, 13);
            this.lblCreationDateFrom.TabIndex = 8;
            this.lblCreationDateFrom.Text = "Creation Date From :";
            this.lblCreationDateFrom.Visible = false;
            // 
            // lblCreationDateTo
            // 
            this.lblCreationDateTo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblCreationDateTo.AutoSize = true;
            this.lblCreationDateTo.Location = new System.Drawing.Point(671, 286);
            this.lblCreationDateTo.Name = "lblCreationDateTo";
            this.lblCreationDateTo.Size = new System.Drawing.Size(26, 13);
            this.lblCreationDateTo.TabIndex = 10;
            this.lblCreationDateTo.Text = "To :";
            this.lblCreationDateTo.Visible = false;
            // 
            // cmbTransactionStatus
            // 
            this.cmbTransactionStatus.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbTransactionStatus.Enabled = false;
            this.cmbTransactionStatus.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cmbTransactionStatus.FormattingEnabled = true;
            this.cmbTransactionStatus.ItemHeight = 19;
            this.cmbTransactionStatus.Location = new System.Drawing.Point(497, 485);
            this.cmbTransactionStatus.Name = "cmbTransactionStatus";
            this.cmbTransactionStatus.Size = new System.Drawing.Size(277, 25);
            this.cmbTransactionStatus.TabIndex = 7;
            this.cmbTransactionStatus.UseSelectable = true;
            this.cmbTransactionStatus.Visible = false;
            // 
            // cmbApprovalStatus
            // 
            this.cmbApprovalStatus.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbApprovalStatus.Enabled = false;
            this.cmbApprovalStatus.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cmbApprovalStatus.FormattingEnabled = true;
            this.cmbApprovalStatus.ItemHeight = 19;
            this.cmbApprovalStatus.Location = new System.Drawing.Point(105, 485);
            this.cmbApprovalStatus.Name = "cmbApprovalStatus";
            this.cmbApprovalStatus.Size = new System.Drawing.Size(277, 25);
            this.cmbApprovalStatus.TabIndex = 5;
            this.cmbApprovalStatus.UseSelectable = true;
            this.cmbApprovalStatus.Visible = false;
            // 
            // lblTransactionStatus
            // 
            this.lblTransactionStatus.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblTransactionStatus.AutoSize = true;
            this.lblTransactionStatus.Enabled = false;
            this.lblTransactionStatus.Location = new System.Drawing.Point(389, 491);
            this.lblTransactionStatus.Name = "lblTransactionStatus";
            this.lblTransactionStatus.Size = new System.Drawing.Size(102, 13);
            this.lblTransactionStatus.TabIndex = 6;
            this.lblTransactionStatus.Text = "Transaction Status :";
            this.lblTransactionStatus.Visible = false;
            // 
            // lblApprovalStatus
            // 
            this.lblApprovalStatus.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblApprovalStatus.AutoSize = true;
            this.lblApprovalStatus.Enabled = false;
            this.lblApprovalStatus.Location = new System.Drawing.Point(11, 491);
            this.lblApprovalStatus.Name = "lblApprovalStatus";
            this.lblApprovalStatus.Size = new System.Drawing.Size(88, 13);
            this.lblApprovalStatus.TabIndex = 4;
            this.lblApprovalStatus.Text = "Approval Status :";
            this.lblApprovalStatus.Visible = false;
            // 
            // dgResults
            // 
            this.dgResults.AllowUserToAddRows = false;
            this.dgResults.AllowUserToDeleteRows = false;
            this.dgResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgResults.Location = new System.Drawing.Point(20, 86);
            this.dgResults.MultiSelect = false;
            this.dgResults.Name = "dgResults";
            this.dgResults.ReadOnly = true;
            this.dgResults.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgResults.Size = new System.Drawing.Size(934, 418);
            this.dgResults.TabIndex = 1;
            this.dgResults.SelectionChanged += new System.EventHandler(this.dgResults_SelectionChanged);
            // 
            // btnDownload
            // 
            this.btnDownload.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnDownload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnDownload.FlatAppearance.BorderSize = 0;
            this.btnDownload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDownload.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btnDownload.ForeColor = System.Drawing.Color.White;
            this.btnDownload.Location = new System.Drawing.Point(719, 15);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(100, 24);
            this.btnDownload.TabIndex = 6;
            this.btnDownload.Text = "&Search";
            this.btnDownload.UseVisualStyleBackColor = false;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(825, 15);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 24);
            this.button1.TabIndex = 7;
            this.button1.Text = "&Reset";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(841, 515);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(113, 24);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnView
            // 
            this.btnView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnView.FlatAppearance.BorderSize = 0;
            this.btnView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btnView.ForeColor = System.Drawing.Color.White;
            this.btnView.Location = new System.Drawing.Point(717, 515);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(113, 24);
            this.btnView.TabIndex = 2;
            this.btnView.Text = "&View";
            this.btnView.UseVisualStyleBackColor = false;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // lblMobileNumber
            // 
            this.lblMobileNumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblMobileNumber.AutoSize = true;
            this.lblMobileNumber.Location = new System.Drawing.Point(509, 20);
            this.lblMobileNumber.Name = "lblMobileNumber";
            this.lblMobileNumber.Size = new System.Drawing.Size(84, 13);
            this.lblMobileNumber.TabIndex = 4;
            this.lblMobileNumber.Text = "Mobile Number :";
            // 
            // txtMobileBumber
            // 
            this.txtMobileBumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtMobileBumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtMobileBumber.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtMobileBumber.InputScopeAllowEmpty = false;
            this.txtMobileBumber.InputScopeCustomString = null;
            this.txtMobileBumber.IsValid = null;
            this.txtMobileBumber.Location = new System.Drawing.Point(598, 17);
            this.txtMobileBumber.MaxLength = 32767;
            this.txtMobileBumber.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtMobileBumber.Name = "txtMobileBumber";
            this.txtMobileBumber.PromptText = "(Type here...)";
            this.txtMobileBumber.ReadOnly = false;
            this.txtMobileBumber.ShowMandatoryMark = false;
            this.txtMobileBumber.Size = new System.Drawing.Size(113, 21);
            this.txtMobileBumber.TabIndex = 5;
            this.txtMobileBumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtMobileBumber.UpperCaseOnly = false;
            this.txtMobileBumber.ValidationErrorMessage = "Validation Error!";
            // 
            // dtpCreationDateTo
            // 
            this.dtpCreationDateTo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtpCreationDateTo.Date = "17/12/2015";
            this.dtpCreationDateTo.Location = new System.Drawing.Point(699, 281);
            this.dtpCreationDateTo.MaximumSize = new System.Drawing.Size(400, 25);
            this.dtpCreationDateTo.MinimumSize = new System.Drawing.Size(60, 25);
            this.dtpCreationDateTo.Name = "dtpCreationDateTo";
            this.dtpCreationDateTo.PresetServerDate = true;
            this.dtpCreationDateTo.Size = new System.Drawing.Size(101, 25);
            this.dtpCreationDateTo.TabIndex = 11;
            this.dtpCreationDateTo.Value = new System.DateTime(2015, 12, 17, 15, 57, 35, 470);
            this.dtpCreationDateTo.Visible = false;
            // 
            // dtpCreationDateFrom
            // 
            this.dtpCreationDateFrom.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtpCreationDateFrom.Date = "17/12/2015";
            this.dtpCreationDateFrom.Location = new System.Drawing.Point(560, 281);
            this.dtpCreationDateFrom.MaximumSize = new System.Drawing.Size(400, 25);
            this.dtpCreationDateFrom.MinimumSize = new System.Drawing.Size(60, 25);
            this.dtpCreationDateFrom.Name = "dtpCreationDateFrom";
            this.dtpCreationDateFrom.PresetServerDate = true;
            this.dtpCreationDateFrom.Size = new System.Drawing.Size(101, 25);
            this.dtpCreationDateFrom.TabIndex = 9;
            this.dtpCreationDateFrom.Value = new System.DateTime(2015, 12, 17, 15, 57, 35, 195);
            this.dtpCreationDateFrom.Visible = false;
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(973, 26);
            this.customTitlebar1.TabIndex = 12;
            this.customTitlebar1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.btnDownload);
            this.groupBox1.Controls.Add(this.txtMobileBumber);
            this.groupBox1.Controls.Add(this.txtBusinessName);
            this.groupBox1.Controls.Add(this.lblMobileNumber);
            this.groupBox1.Controls.Add(this.lblBusinessName);
            this.groupBox1.Controls.Add(this.txtAgentCode);
            this.groupBox1.Controls.Add(this.lblAgentCode);
            this.groupBox1.Location = new System.Drawing.Point(20, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(934, 49);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // frmAgentSearch2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(973, 556);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dtpCreationDateTo);
            this.Controls.Add(this.dtpCreationDateFrom);
            this.Controls.Add(this.lblCreationDateTo);
            this.Controls.Add(this.lblCreationDateFrom);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.cmbApprovalStatus);
            this.Controls.Add(this.cmbTransactionStatus);
            this.Controls.Add(this.lblApprovalStatus);
            this.Controls.Add(this.lblTransactionStatus);
            this.Controls.Add(this.dgResults);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmAgentSearch2";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agent Search";
            ((System.ComponentModel.ISupportInitialize)(this.dgResults)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAgentCode;
        private CustomTextBox txtAgentCode;
        private System.Windows.Forms.Label lblBusinessName;
        private CustomTextBox txtBusinessName;
        private System.Windows.Forms.Label lblCreationDateFrom;
        private System.Windows.Forms.Label lblCreationDateTo;
        private CustomControls.CustomDateTimePicker dtpCreationDateFrom;
        private CustomControls.CustomDateTimePicker dtpCreationDateTo;
        private MetroFramework.Controls.MetroComboBox cmbTransactionStatus;
        private MetroFramework.Controls.MetroComboBox cmbApprovalStatus;
        private System.Windows.Forms.Label lblTransactionStatus;
        private System.Windows.Forms.Label lblApprovalStatus;
        private System.Windows.Forms.DataGridView dgResults;
        private CustomTitlebar customTitlebar1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnDownload;
        private System.Windows.Forms.Button btnView;
        private CustomTextBox txtMobileBumber;
        private System.Windows.Forms.Label lblMobileNumber;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}