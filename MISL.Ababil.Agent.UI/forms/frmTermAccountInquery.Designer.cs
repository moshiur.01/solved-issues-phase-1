﻿using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.UI.forms
{
    partial class frmTermAccountInquery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAccountNumber = new System.Windows.Forms.Label();
            this.lblCustomerId = new System.Windows.Forms.Label();
            this.lblAccountStatus = new System.Windows.Forms.Label();
            this.lblOpeningDate = new System.Windows.Forms.Label();
            this.lblExpiryDate = new System.Windows.Forms.Label();
            this.lblLastTxnDate = new System.Windows.Forms.Label();
            this.lblClosingDate = new System.Windows.Forms.Label();
            this.lblAccountName = new System.Windows.Forms.Label();
            this.lblProduct = new System.Windows.Forms.Label();
            this.lblCurrentBalance = new System.Windows.Forms.Label();
            this.gbxITD = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtNoofAdvanceInstallment = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtNoDueInstallment = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtTotalNoofInstallment = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtLienAmount = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtNoofGivenInstallment = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtInstallmentAmount = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtQuardAmount = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.lblNoofAdvanceInstallment = new System.Windows.Forms.Label();
            this.lblNoDueInstallment = new System.Windows.Forms.Label();
            this.lblTotalNoofInstallment = new System.Windows.Forms.Label();
            this.lblNoofGivenInstallment = new System.Windows.Forms.Label();
            this.lblInstallmentAmount = new System.Windows.Forms.Label();
            this.lblLienAmount = new System.Windows.Forms.Label();
            this.lblQuardAmount = new System.Windows.Forms.Label();
            this.gbxMTDR = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtRenewRate = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtLienAmount2 = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtRenewDate = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtQuardAmount2 = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.lblRenewRate = new System.Windows.Forms.Label();
            this.lblRenewDate = new System.Windows.Forms.Label();
            this.lblLienAmount2 = new System.Windows.Forms.Label();
            this.lblQuardAmount2 = new System.Windows.Forms.Label();
            this.txtAccountNumber = new System.Windows.Forms.TextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtCurrentBalance = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtProduct = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtAccountName = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtAccountStatus = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtCustomerId = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.txtOpeningDate = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtLastTxnDate = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtExpiryDate = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtClosingDate = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.gbxITD.SuspendLayout();
            this.panel1.SuspendLayout();
            this.gbxMTDR.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblAccountNumber
            // 
            this.lblAccountNumber.AutoSize = true;
            this.lblAccountNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblAccountNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAccountNumber.ForeColor = System.Drawing.Color.White;
            this.lblAccountNumber.Location = new System.Drawing.Point(28, 32);
            this.lblAccountNumber.Name = "lblAccountNumber";
            this.lblAccountNumber.Size = new System.Drawing.Size(143, 26);
            this.lblAccountNumber.TabIndex = 0;
            this.lblAccountNumber.Text = "Account No. :";
            // 
            // lblCustomerId
            // 
            this.lblCustomerId.AutoSize = true;
            this.lblCustomerId.Location = new System.Drawing.Point(94, 91);
            this.lblCustomerId.Name = "lblCustomerId";
            this.lblCustomerId.Size = new System.Drawing.Size(71, 13);
            this.lblCustomerId.TabIndex = 4;
            this.lblCustomerId.Text = "Customer ID :";
            // 
            // lblAccountStatus
            // 
            this.lblAccountStatus.AutoSize = true;
            this.lblAccountStatus.Location = new System.Drawing.Point(79, 144);
            this.lblAccountStatus.Name = "lblAccountStatus";
            this.lblAccountStatus.Size = new System.Drawing.Size(86, 13);
            this.lblAccountStatus.TabIndex = 8;
            this.lblAccountStatus.Text = "Account Status :";
            // 
            // lblOpeningDate
            // 
            this.lblOpeningDate.AutoSize = true;
            this.lblOpeningDate.Location = new System.Drawing.Point(449, 92);
            this.lblOpeningDate.Name = "lblOpeningDate";
            this.lblOpeningDate.Size = new System.Drawing.Size(79, 13);
            this.lblOpeningDate.TabIndex = 14;
            this.lblOpeningDate.Text = "Opening Date :";
            // 
            // lblExpiryDate
            // 
            this.lblExpiryDate.AutoSize = true;
            this.lblExpiryDate.Location = new System.Drawing.Point(461, 119);
            this.lblExpiryDate.Name = "lblExpiryDate";
            this.lblExpiryDate.Size = new System.Drawing.Size(67, 13);
            this.lblExpiryDate.TabIndex = 16;
            this.lblExpiryDate.Text = "Expiry Date :";
            // 
            // lblLastTxnDate
            // 
            this.lblLastTxnDate.AutoSize = true;
            this.lblLastTxnDate.Location = new System.Drawing.Point(448, 146);
            this.lblLastTxnDate.Name = "lblLastTxnDate";
            this.lblLastTxnDate.Size = new System.Drawing.Size(80, 13);
            this.lblLastTxnDate.TabIndex = 18;
            this.lblLastTxnDate.Text = "Last Txn Date :";
            // 
            // lblClosingDate
            // 
            this.lblClosingDate.AutoSize = true;
            this.lblClosingDate.Location = new System.Drawing.Point(455, 173);
            this.lblClosingDate.Name = "lblClosingDate";
            this.lblClosingDate.Size = new System.Drawing.Size(73, 13);
            this.lblClosingDate.TabIndex = 20;
            this.lblClosingDate.Text = "Closing Date :";
            // 
            // lblAccountName
            // 
            this.lblAccountName.AutoSize = true;
            this.lblAccountName.Location = new System.Drawing.Point(81, 117);
            this.lblAccountName.Name = "lblAccountName";
            this.lblAccountName.Size = new System.Drawing.Size(84, 13);
            this.lblAccountName.TabIndex = 6;
            this.lblAccountName.Text = "Account Name :";
            // 
            // lblProduct
            // 
            this.lblProduct.AutoSize = true;
            this.lblProduct.Location = new System.Drawing.Point(115, 173);
            this.lblProduct.Name = "lblProduct";
            this.lblProduct.Size = new System.Drawing.Size(50, 13);
            this.lblProduct.TabIndex = 10;
            this.lblProduct.Text = "Product :";
            // 
            // lblCurrentBalance
            // 
            this.lblCurrentBalance.AutoSize = true;
            this.lblCurrentBalance.Location = new System.Drawing.Point(76, 199);
            this.lblCurrentBalance.Name = "lblCurrentBalance";
            this.lblCurrentBalance.Size = new System.Drawing.Size(89, 13);
            this.lblCurrentBalance.TabIndex = 12;
            this.lblCurrentBalance.Text = "Current Balance :";
            // 
            // gbxITD
            // 
            this.gbxITD.Controls.Add(this.panel1);
            this.gbxITD.Location = new System.Drawing.Point(13, 235);
            this.gbxITD.Name = "gbxITD";
            this.gbxITD.Size = new System.Drawing.Size(776, 170);
            this.gbxITD.TabIndex = 22;
            this.gbxITD.TabStop = false;
            this.gbxITD.Text = "Installment Information";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtQuardAmount);
            this.panel1.Controls.Add(this.txtInstallmentAmount);
            this.panel1.Controls.Add(this.txtNoofGivenInstallment);
            this.panel1.Controls.Add(this.txtLienAmount);
            this.panel1.Controls.Add(this.txtTotalNoofInstallment);
            this.panel1.Controls.Add(this.txtNoDueInstallment);
            this.panel1.Controls.Add(this.txtNoofAdvanceInstallment);
            this.panel1.Controls.Add(this.lblNoofAdvanceInstallment);
            this.panel1.Controls.Add(this.lblNoDueInstallment);
            this.panel1.Controls.Add(this.lblTotalNoofInstallment);
            this.panel1.Controls.Add(this.lblNoofGivenInstallment);
            this.panel1.Controls.Add(this.lblInstallmentAmount);
            this.panel1.Controls.Add(this.lblLienAmount);
            this.panel1.Controls.Add(this.lblQuardAmount);
            this.panel1.Location = new System.Drawing.Point(3, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(767, 149);
            this.panel1.TabIndex = 0;
            // 
            // txtNoofAdvanceInstallment
            // 
            this.txtNoofAdvanceInstallment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtNoofAdvanceInstallment.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtNoofAdvanceInstallment.InputScopeAllowEmpty = false;
            this.txtNoofAdvanceInstallment.InputScopeCustomString = null;
            this.txtNoofAdvanceInstallment.IsValid = null;
            this.txtNoofAdvanceInstallment.Location = new System.Drawing.Point(154, 120);
            this.txtNoofAdvanceInstallment.MaxLength = 32767;
            this.txtNoofAdvanceInstallment.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtNoofAdvanceInstallment.Name = "txtNoofAdvanceInstallment";
            this.txtNoofAdvanceInstallment.PromptText = "";
            this.txtNoofAdvanceInstallment.ReadOnly = true;
            this.txtNoofAdvanceInstallment.ShowMandatoryMark = false;
            this.txtNoofAdvanceInstallment.Size = new System.Drawing.Size(237, 21);
            this.txtNoofAdvanceInstallment.TabIndex = 9;
            this.txtNoofAdvanceInstallment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNoofAdvanceInstallment.UpperCaseOnly = false;
            this.txtNoofAdvanceInstallment.ValidationErrorMessage = "Validation Error!";
            // 
            // txtNoDueInstallment
            // 
            this.txtNoDueInstallment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtNoDueInstallment.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtNoDueInstallment.InputScopeAllowEmpty = false;
            this.txtNoDueInstallment.InputScopeCustomString = null;
            this.txtNoDueInstallment.IsValid = null;
            this.txtNoDueInstallment.Location = new System.Drawing.Point(154, 93);
            this.txtNoDueInstallment.MaxLength = 32767;
            this.txtNoDueInstallment.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtNoDueInstallment.Name = "txtNoDueInstallment";
            this.txtNoDueInstallment.PromptText = "";
            this.txtNoDueInstallment.ReadOnly = true;
            this.txtNoDueInstallment.ShowMandatoryMark = false;
            this.txtNoDueInstallment.Size = new System.Drawing.Size(237, 21);
            this.txtNoDueInstallment.TabIndex = 7;
            this.txtNoDueInstallment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNoDueInstallment.UpperCaseOnly = false;
            this.txtNoDueInstallment.ValidationErrorMessage = "Validation Error!";
            // 
            // txtTotalNoofInstallment
            // 
            this.txtTotalNoofInstallment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtTotalNoofInstallment.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtTotalNoofInstallment.InputScopeAllowEmpty = false;
            this.txtTotalNoofInstallment.InputScopeCustomString = null;
            this.txtTotalNoofInstallment.IsValid = null;
            this.txtTotalNoofInstallment.Location = new System.Drawing.Point(154, 39);
            this.txtTotalNoofInstallment.MaxLength = 32767;
            this.txtTotalNoofInstallment.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtTotalNoofInstallment.Name = "txtTotalNoofInstallment";
            this.txtTotalNoofInstallment.PromptText = "";
            this.txtTotalNoofInstallment.ReadOnly = true;
            this.txtTotalNoofInstallment.ShowMandatoryMark = false;
            this.txtTotalNoofInstallment.Size = new System.Drawing.Size(237, 21);
            this.txtTotalNoofInstallment.TabIndex = 3;
            this.txtTotalNoofInstallment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTotalNoofInstallment.UpperCaseOnly = false;
            this.txtTotalNoofInstallment.ValidationErrorMessage = "Validation Error!";
            // 
            // txtLienAmount
            // 
            this.txtLienAmount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtLienAmount.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtLienAmount.InputScopeAllowEmpty = false;
            this.txtLienAmount.InputScopeCustomString = null;
            this.txtLienAmount.IsValid = null;
            this.txtLienAmount.Location = new System.Drawing.Point(517, 42);
            this.txtLienAmount.MaxLength = 32767;
            this.txtLienAmount.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtLienAmount.Name = "txtLienAmount";
            this.txtLienAmount.PromptText = "";
            this.txtLienAmount.ReadOnly = true;
            this.txtLienAmount.ShowMandatoryMark = false;
            this.txtLienAmount.Size = new System.Drawing.Size(237, 21);
            this.txtLienAmount.TabIndex = 13;
            this.txtLienAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLienAmount.UpperCaseOnly = false;
            this.txtLienAmount.ValidationErrorMessage = "Validation Error!";
            // 
            // txtNoofGivenInstallment
            // 
            this.txtNoofGivenInstallment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtNoofGivenInstallment.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtNoofGivenInstallment.InputScopeAllowEmpty = false;
            this.txtNoofGivenInstallment.InputScopeCustomString = null;
            this.txtNoofGivenInstallment.IsValid = null;
            this.txtNoofGivenInstallment.Location = new System.Drawing.Point(154, 66);
            this.txtNoofGivenInstallment.MaxLength = 32767;
            this.txtNoofGivenInstallment.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtNoofGivenInstallment.Name = "txtNoofGivenInstallment";
            this.txtNoofGivenInstallment.PromptText = "";
            this.txtNoofGivenInstallment.ReadOnly = true;
            this.txtNoofGivenInstallment.ShowMandatoryMark = false;
            this.txtNoofGivenInstallment.Size = new System.Drawing.Size(237, 21);
            this.txtNoofGivenInstallment.TabIndex = 5;
            this.txtNoofGivenInstallment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNoofGivenInstallment.UpperCaseOnly = false;
            this.txtNoofGivenInstallment.ValidationErrorMessage = "Validation Error!";
            // 
            // txtInstallmentAmount
            // 
            this.txtInstallmentAmount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtInstallmentAmount.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtInstallmentAmount.InputScopeAllowEmpty = false;
            this.txtInstallmentAmount.InputScopeCustomString = null;
            this.txtInstallmentAmount.IsValid = null;
            this.txtInstallmentAmount.Location = new System.Drawing.Point(154, 12);
            this.txtInstallmentAmount.MaxLength = 32767;
            this.txtInstallmentAmount.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtInstallmentAmount.Name = "txtInstallmentAmount";
            this.txtInstallmentAmount.PromptText = "";
            this.txtInstallmentAmount.ReadOnly = true;
            this.txtInstallmentAmount.ShowMandatoryMark = false;
            this.txtInstallmentAmount.Size = new System.Drawing.Size(237, 21);
            this.txtInstallmentAmount.TabIndex = 1;
            this.txtInstallmentAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtInstallmentAmount.UpperCaseOnly = false;
            this.txtInstallmentAmount.ValidationErrorMessage = "Validation Error!";
            // 
            // txtQuardAmount
            // 
            this.txtQuardAmount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtQuardAmount.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtQuardAmount.InputScopeAllowEmpty = false;
            this.txtQuardAmount.InputScopeCustomString = null;
            this.txtQuardAmount.IsValid = null;
            this.txtQuardAmount.Location = new System.Drawing.Point(517, 15);
            this.txtQuardAmount.MaxLength = 32767;
            this.txtQuardAmount.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtQuardAmount.Name = "txtQuardAmount";
            this.txtQuardAmount.PromptText = "";
            this.txtQuardAmount.ReadOnly = true;
            this.txtQuardAmount.ShowMandatoryMark = false;
            this.txtQuardAmount.Size = new System.Drawing.Size(237, 21);
            this.txtQuardAmount.TabIndex = 11;
            this.txtQuardAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtQuardAmount.UpperCaseOnly = false;
            this.txtQuardAmount.ValidationErrorMessage = "Validation Error!";
            // 
            // lblNoofAdvanceInstallment
            // 
            this.lblNoofAdvanceInstallment.AutoSize = true;
            this.lblNoofAdvanceInstallment.Location = new System.Drawing.Point(8, 122);
            this.lblNoofAdvanceInstallment.Name = "lblNoofAdvanceInstallment";
            this.lblNoofAdvanceInstallment.Size = new System.Drawing.Size(141, 13);
            this.lblNoofAdvanceInstallment.TabIndex = 8;
            this.lblNoofAdvanceInstallment.Text = "No. of Advance Installment :";
            // 
            // lblNoDueInstallment
            // 
            this.lblNoDueInstallment.AutoSize = true;
            this.lblNoDueInstallment.Location = new System.Drawing.Point(31, 95);
            this.lblNoDueInstallment.Name = "lblNoDueInstallment";
            this.lblNoDueInstallment.Size = new System.Drawing.Size(118, 13);
            this.lblNoDueInstallment.TabIndex = 6;
            this.lblNoDueInstallment.Text = "No. of Due Installment :";
            // 
            // lblTotalNoofInstallment
            // 
            this.lblTotalNoofInstallment.AutoSize = true;
            this.lblTotalNoofInstallment.Location = new System.Drawing.Point(27, 41);
            this.lblTotalNoofInstallment.Name = "lblTotalNoofInstallment";
            this.lblTotalNoofInstallment.Size = new System.Drawing.Size(122, 13);
            this.lblTotalNoofInstallment.TabIndex = 2;
            this.lblTotalNoofInstallment.Text = "Total No. of Installment :";
            // 
            // lblNoofGivenInstallment
            // 
            this.lblNoofGivenInstallment.AutoSize = true;
            this.lblNoofGivenInstallment.Location = new System.Drawing.Point(23, 69);
            this.lblNoofGivenInstallment.Name = "lblNoofGivenInstallment";
            this.lblNoofGivenInstallment.Size = new System.Drawing.Size(126, 13);
            this.lblNoofGivenInstallment.TabIndex = 4;
            this.lblNoofGivenInstallment.Text = "No. of Given Installment :";
            // 
            // lblInstallmentAmount
            // 
            this.lblInstallmentAmount.AutoSize = true;
            this.lblInstallmentAmount.Location = new System.Drawing.Point(47, 15);
            this.lblInstallmentAmount.Name = "lblInstallmentAmount";
            this.lblInstallmentAmount.Size = new System.Drawing.Size(102, 13);
            this.lblInstallmentAmount.TabIndex = 0;
            this.lblInstallmentAmount.Text = "Installment Amount :";
            // 
            // lblLienAmount
            // 
            this.lblLienAmount.AutoSize = true;
            this.lblLienAmount.Location = new System.Drawing.Point(439, 44);
            this.lblLienAmount.Name = "lblLienAmount";
            this.lblLienAmount.Size = new System.Drawing.Size(72, 13);
            this.lblLienAmount.TabIndex = 12;
            this.lblLienAmount.Text = "Lien Amount :";
            // 
            // lblQuardAmount
            // 
            this.lblQuardAmount.AutoSize = true;
            this.lblQuardAmount.Location = new System.Drawing.Point(430, 18);
            this.lblQuardAmount.Name = "lblQuardAmount";
            this.lblQuardAmount.Size = new System.Drawing.Size(81, 13);
            this.lblQuardAmount.TabIndex = 10;
            this.lblQuardAmount.Text = "Quard Amount :";
            // 
            // gbxMTDR
            // 
            this.gbxMTDR.Controls.Add(this.panel2);
            this.gbxMTDR.Location = new System.Drawing.Point(13, 235);
            this.gbxMTDR.Name = "gbxMTDR";
            this.gbxMTDR.Size = new System.Drawing.Size(776, 170);
            this.gbxMTDR.TabIndex = 23;
            this.gbxMTDR.TabStop = false;
            this.gbxMTDR.Tag = "";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtQuardAmount2);
            this.panel2.Controls.Add(this.txtRenewDate);
            this.panel2.Controls.Add(this.txtLienAmount2);
            this.panel2.Controls.Add(this.txtRenewRate);
            this.panel2.Controls.Add(this.lblRenewRate);
            this.panel2.Controls.Add(this.lblRenewDate);
            this.panel2.Controls.Add(this.lblLienAmount2);
            this.panel2.Controls.Add(this.lblQuardAmount2);
            this.panel2.Location = new System.Drawing.Point(20, 18);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(750, 79);
            this.panel2.TabIndex = 0;
            // 
            // txtRenewRate
            // 
            this.txtRenewRate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtRenewRate.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtRenewRate.InputScopeAllowEmpty = false;
            this.txtRenewRate.InputScopeCustomString = null;
            this.txtRenewRate.IsValid = null;
            this.txtRenewRate.Location = new System.Drawing.Point(137, 37);
            this.txtRenewRate.MaxLength = 32767;
            this.txtRenewRate.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtRenewRate.Name = "txtRenewRate";
            this.txtRenewRate.PromptText = "";
            this.txtRenewRate.ReadOnly = true;
            this.txtRenewRate.ShowMandatoryMark = false;
            this.txtRenewRate.Size = new System.Drawing.Size(237, 21);
            this.txtRenewRate.TabIndex = 3;
            this.txtRenewRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRenewRate.UpperCaseOnly = false;
            this.txtRenewRate.ValidationErrorMessage = "Validation Error!";
            // 
            // txtLienAmount2
            // 
            this.txtLienAmount2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtLienAmount2.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtLienAmount2.InputScopeAllowEmpty = false;
            this.txtLienAmount2.InputScopeCustomString = null;
            this.txtLienAmount2.IsValid = null;
            this.txtLienAmount2.Location = new System.Drawing.Point(500, 40);
            this.txtLienAmount2.MaxLength = 32767;
            this.txtLienAmount2.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtLienAmount2.Name = "txtLienAmount2";
            this.txtLienAmount2.PromptText = "";
            this.txtLienAmount2.ReadOnly = true;
            this.txtLienAmount2.ShowMandatoryMark = false;
            this.txtLienAmount2.Size = new System.Drawing.Size(237, 21);
            this.txtLienAmount2.TabIndex = 7;
            this.txtLienAmount2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLienAmount2.UpperCaseOnly = false;
            this.txtLienAmount2.ValidationErrorMessage = "Validation Error!";
            // 
            // txtRenewDate
            // 
            this.txtRenewDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtRenewDate.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtRenewDate.InputScopeAllowEmpty = false;
            this.txtRenewDate.InputScopeCustomString = null;
            this.txtRenewDate.IsValid = null;
            this.txtRenewDate.Location = new System.Drawing.Point(137, 10);
            this.txtRenewDate.MaxLength = 32767;
            this.txtRenewDate.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtRenewDate.Name = "txtRenewDate";
            this.txtRenewDate.PromptText = "";
            this.txtRenewDate.ReadOnly = true;
            this.txtRenewDate.ShowMandatoryMark = false;
            this.txtRenewDate.Size = new System.Drawing.Size(237, 21);
            this.txtRenewDate.TabIndex = 1;
            this.txtRenewDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtRenewDate.UpperCaseOnly = false;
            this.txtRenewDate.ValidationErrorMessage = "Validation Error!";
            // 
            // txtQuardAmount2
            // 
            this.txtQuardAmount2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtQuardAmount2.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtQuardAmount2.InputScopeAllowEmpty = false;
            this.txtQuardAmount2.InputScopeCustomString = null;
            this.txtQuardAmount2.IsValid = null;
            this.txtQuardAmount2.Location = new System.Drawing.Point(500, 13);
            this.txtQuardAmount2.MaxLength = 32767;
            this.txtQuardAmount2.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtQuardAmount2.Name = "txtQuardAmount2";
            this.txtQuardAmount2.PromptText = "";
            this.txtQuardAmount2.ReadOnly = true;
            this.txtQuardAmount2.ShowMandatoryMark = false;
            this.txtQuardAmount2.Size = new System.Drawing.Size(237, 21);
            this.txtQuardAmount2.TabIndex = 5;
            this.txtQuardAmount2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtQuardAmount2.UpperCaseOnly = false;
            this.txtQuardAmount2.ValidationErrorMessage = "Validation Error!";
            // 
            // lblRenewRate
            // 
            this.lblRenewRate.AutoSize = true;
            this.lblRenewRate.Location = new System.Drawing.Point(59, 39);
            this.lblRenewRate.Name = "lblRenewRate";
            this.lblRenewRate.Size = new System.Drawing.Size(73, 13);
            this.lblRenewRate.TabIndex = 2;
            this.lblRenewRate.Text = "Renew Rate :";
            // 
            // lblRenewDate
            // 
            this.lblRenewDate.AutoSize = true;
            this.lblRenewDate.Location = new System.Drawing.Point(59, 13);
            this.lblRenewDate.Name = "lblRenewDate";
            this.lblRenewDate.Size = new System.Drawing.Size(73, 13);
            this.lblRenewDate.TabIndex = 0;
            this.lblRenewDate.Text = "Renew Date :";
            // 
            // lblLienAmount2
            // 
            this.lblLienAmount2.AutoSize = true;
            this.lblLienAmount2.Location = new System.Drawing.Point(423, 42);
            this.lblLienAmount2.Name = "lblLienAmount2";
            this.lblLienAmount2.Size = new System.Drawing.Size(72, 13);
            this.lblLienAmount2.TabIndex = 6;
            this.lblLienAmount2.Text = "Lien Amount :";
            // 
            // lblQuardAmount2
            // 
            this.lblQuardAmount2.AutoSize = true;
            this.lblQuardAmount2.Location = new System.Drawing.Point(414, 16);
            this.lblQuardAmount2.Name = "lblQuardAmount2";
            this.lblQuardAmount2.Size = new System.Drawing.Size(81, 13);
            this.lblQuardAmount2.TabIndex = 4;
            this.lblQuardAmount2.Text = "Quard Amount :";
            // 
            // txtAccountNumber
            // 
            this.txtAccountNumber.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAccountNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.txtAccountNumber.Location = new System.Drawing.Point(8, 2);
            this.txtAccountNumber.MaxLength = 13;
            this.txtAccountNumber.Name = "txtAccountNumber";
            this.txtAccountNumber.Size = new System.Drawing.Size(224, 25);
            this.txtAccountNumber.TabIndex = 0;
            this.txtAccountNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAccountNumber_KeyDown);
            this.txtAccountNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAccountNumber_KeyPress);
            this.txtAccountNumber.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtAccountNumber_KeyUp);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Image = global::MISL.Ababil.Agent.UI.Properties.Resources.Delete_16__1_;
            this.btnClose.Location = new System.Drawing.Point(669, 417);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 26);
            this.btnClose.TabIndex = 24;
            this.btnClose.Text = "Close";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnReset.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnReset.FlatAppearance.BorderSize = 0;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.Image = global::MISL.Ababil.Agent.UI.Properties.Resources.Broom_24;
            this.btnReset.Location = new System.Drawing.Point(453, 30);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(32, 36);
            this.btnReset.TabIndex = 3;
            this.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnSearch.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Image = global::MISL.Ababil.Agent.UI.Properties.Resources.Search___Filled_24;
            this.btnSearch.Location = new System.Drawing.Point(415, 30);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(32, 36);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtCurrentBalance
            // 
            this.txtCurrentBalance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtCurrentBalance.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtCurrentBalance.InputScopeAllowEmpty = false;
            this.txtCurrentBalance.InputScopeCustomString = null;
            this.txtCurrentBalance.IsValid = null;
            this.txtCurrentBalance.Location = new System.Drawing.Point(170, 195);
            this.txtCurrentBalance.MaxLength = 32767;
            this.txtCurrentBalance.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtCurrentBalance.Name = "txtCurrentBalance";
            this.txtCurrentBalance.PromptText = "(Type here...)";
            this.txtCurrentBalance.ReadOnly = true;
            this.txtCurrentBalance.ShowMandatoryMark = false;
            this.txtCurrentBalance.Size = new System.Drawing.Size(237, 21);
            this.txtCurrentBalance.TabIndex = 13;
            this.txtCurrentBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCurrentBalance.UpperCaseOnly = false;
            this.txtCurrentBalance.ValidationErrorMessage = "Validation Error!";
            // 
            // txtProduct
            // 
            this.txtProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtProduct.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtProduct.InputScopeAllowEmpty = false;
            this.txtProduct.InputScopeCustomString = null;
            this.txtProduct.IsValid = null;
            this.txtProduct.Location = new System.Drawing.Point(170, 168);
            this.txtProduct.MaxLength = 32767;
            this.txtProduct.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtProduct.Name = "txtProduct";
            this.txtProduct.PromptText = "(Type here...)";
            this.txtProduct.ReadOnly = true;
            this.txtProduct.ShowMandatoryMark = false;
            this.txtProduct.Size = new System.Drawing.Size(237, 21);
            this.txtProduct.TabIndex = 11;
            this.txtProduct.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtProduct.UpperCaseOnly = false;
            this.txtProduct.ValidationErrorMessage = "Validation Error!";
            // 
            // txtAccountName
            // 
            this.txtAccountName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtAccountName.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtAccountName.InputScopeAllowEmpty = false;
            this.txtAccountName.InputScopeCustomString = null;
            this.txtAccountName.IsValid = null;
            this.txtAccountName.Location = new System.Drawing.Point(170, 114);
            this.txtAccountName.MaxLength = 32767;
            this.txtAccountName.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtAccountName.Name = "txtAccountName";
            this.txtAccountName.PromptText = "(Type here...)";
            this.txtAccountName.ReadOnly = true;
            this.txtAccountName.ShowMandatoryMark = false;
            this.txtAccountName.Size = new System.Drawing.Size(237, 21);
            this.txtAccountName.TabIndex = 7;
            this.txtAccountName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtAccountName.UpperCaseOnly = false;
            this.txtAccountName.ValidationErrorMessage = "Validation Error!";
            // 
            // txtAccountStatus
            // 
            this.txtAccountStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtAccountStatus.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtAccountStatus.InputScopeAllowEmpty = false;
            this.txtAccountStatus.InputScopeCustomString = null;
            this.txtAccountStatus.IsValid = null;
            this.txtAccountStatus.Location = new System.Drawing.Point(170, 141);
            this.txtAccountStatus.MaxLength = 32767;
            this.txtAccountStatus.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtAccountStatus.Name = "txtAccountStatus";
            this.txtAccountStatus.PromptText = "(Type here...)";
            this.txtAccountStatus.ReadOnly = true;
            this.txtAccountStatus.ShowMandatoryMark = false;
            this.txtAccountStatus.Size = new System.Drawing.Size(237, 21);
            this.txtAccountStatus.TabIndex = 9;
            this.txtAccountStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtAccountStatus.UpperCaseOnly = false;
            this.txtAccountStatus.ValidationErrorMessage = "Validation Error!";
            // 
            // txtCustomerId
            // 
            this.txtCustomerId.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtCustomerId.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtCustomerId.InputScopeAllowEmpty = false;
            this.txtCustomerId.InputScopeCustomString = null;
            this.txtCustomerId.IsValid = null;
            this.txtCustomerId.Location = new System.Drawing.Point(170, 87);
            this.txtCustomerId.MaxLength = 32767;
            this.txtCustomerId.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtCustomerId.Name = "txtCustomerId";
            this.txtCustomerId.PromptText = "(Type here...)";
            this.txtCustomerId.ReadOnly = true;
            this.txtCustomerId.ShowMandatoryMark = false;
            this.txtCustomerId.Size = new System.Drawing.Size(237, 21);
            this.txtCustomerId.TabIndex = 5;
            this.txtCustomerId.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtCustomerId.UpperCaseOnly = false;
            this.txtCustomerId.ValidationErrorMessage = "Validation Error!";
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(802, 76);
            this.customTitlebar1.TabIndex = 25;
            // 
            // txtOpeningDate
            // 
            this.txtOpeningDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtOpeningDate.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtOpeningDate.InputScopeAllowEmpty = false;
            this.txtOpeningDate.InputScopeCustomString = null;
            this.txtOpeningDate.IsValid = null;
            this.txtOpeningDate.Location = new System.Drawing.Point(533, 87);
            this.txtOpeningDate.MaxLength = 32767;
            this.txtOpeningDate.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtOpeningDate.Name = "txtOpeningDate";
            this.txtOpeningDate.PromptText = "(Type here...)";
            this.txtOpeningDate.ReadOnly = true;
            this.txtOpeningDate.ShowMandatoryMark = false;
            this.txtOpeningDate.Size = new System.Drawing.Size(237, 21);
            this.txtOpeningDate.TabIndex = 15;
            this.txtOpeningDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtOpeningDate.UpperCaseOnly = false;
            this.txtOpeningDate.ValidationErrorMessage = "Validation Error!";
            // 
            // txtLastTxnDate
            // 
            this.txtLastTxnDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtLastTxnDate.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtLastTxnDate.InputScopeAllowEmpty = false;
            this.txtLastTxnDate.InputScopeCustomString = null;
            this.txtLastTxnDate.IsValid = null;
            this.txtLastTxnDate.Location = new System.Drawing.Point(533, 141);
            this.txtLastTxnDate.MaxLength = 32767;
            this.txtLastTxnDate.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtLastTxnDate.Name = "txtLastTxnDate";
            this.txtLastTxnDate.PromptText = "(Type here...)";
            this.txtLastTxnDate.ReadOnly = true;
            this.txtLastTxnDate.ShowMandatoryMark = false;
            this.txtLastTxnDate.Size = new System.Drawing.Size(237, 21);
            this.txtLastTxnDate.TabIndex = 19;
            this.txtLastTxnDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtLastTxnDate.UpperCaseOnly = false;
            this.txtLastTxnDate.ValidationErrorMessage = "Validation Error!";
            // 
            // txtExpiryDate
            // 
            this.txtExpiryDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtExpiryDate.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtExpiryDate.InputScopeAllowEmpty = false;
            this.txtExpiryDate.InputScopeCustomString = null;
            this.txtExpiryDate.IsValid = null;
            this.txtExpiryDate.Location = new System.Drawing.Point(533, 114);
            this.txtExpiryDate.MaxLength = 32767;
            this.txtExpiryDate.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtExpiryDate.Name = "txtExpiryDate";
            this.txtExpiryDate.PromptText = "(Type here...)";
            this.txtExpiryDate.ReadOnly = true;
            this.txtExpiryDate.ShowMandatoryMark = false;
            this.txtExpiryDate.Size = new System.Drawing.Size(237, 21);
            this.txtExpiryDate.TabIndex = 17;
            this.txtExpiryDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtExpiryDate.UpperCaseOnly = false;
            this.txtExpiryDate.ValidationErrorMessage = "Validation Error!";
            // 
            // txtClosingDate
            // 
            this.txtClosingDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtClosingDate.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtClosingDate.InputScopeAllowEmpty = false;
            this.txtClosingDate.InputScopeCustomString = null;
            this.txtClosingDate.IsValid = null;
            this.txtClosingDate.Location = new System.Drawing.Point(533, 168);
            this.txtClosingDate.MaxLength = 32767;
            this.txtClosingDate.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtClosingDate.Name = "txtClosingDate";
            this.txtClosingDate.PromptText = "(Type here...)";
            this.txtClosingDate.ReadOnly = true;
            this.txtClosingDate.ShowMandatoryMark = false;
            this.txtClosingDate.Size = new System.Drawing.Size(237, 21);
            this.txtClosingDate.TabIndex = 21;
            this.txtClosingDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtClosingDate.UpperCaseOnly = false;
            this.txtClosingDate.ValidationErrorMessage = "Validation Error!";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtAccountNumber);
            this.panel3.Location = new System.Drawing.Point(168, 30);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(241, 36);
            this.panel3.TabIndex = 1;
            this.panel3.Click += new System.EventHandler(this.panel3_Click);
            // 
            // frmTermAccountInquery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(802, 456);
            this.Controls.Add(this.txtCustomerId);
            this.Controls.Add(this.txtAccountStatus);
            this.Controls.Add(this.txtLastTxnDate);
            this.Controls.Add(this.txtAccountName);
            this.Controls.Add(this.txtProduct);
            this.Controls.Add(this.txtExpiryDate);
            this.Controls.Add(this.txtCurrentBalance);
            this.Controls.Add(this.txtClosingDate);
            this.Controls.Add(this.txtOpeningDate);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.lblClosingDate);
            this.Controls.Add(this.lblLastTxnDate);
            this.Controls.Add(this.lblExpiryDate);
            this.Controls.Add(this.lblOpeningDate);
            this.Controls.Add(this.lblAccountStatus);
            this.Controls.Add(this.lblCustomerId);
            this.Controls.Add(this.lblCurrentBalance);
            this.Controls.Add(this.lblProduct);
            this.Controls.Add(this.lblAccountName);
            this.Controls.Add(this.lblAccountNumber);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.gbxITD);
            this.Controls.Add(this.gbxMTDR);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "frmTermAccountInquery";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Term Account Inquery";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmTermAccountInquery_FormClosing);
            this.gbxITD.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.gbxMTDR.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Agent.CustomControls.CustomTextBox txtCustomerId;
        private Agent.CustomControls.CustomTextBox txtAccountStatus;
        private Agent.CustomControls.CustomTextBox txtAccountName;
        private Agent.CustomControls.CustomTextBox txtProduct;
        private Agent.CustomControls.CustomTextBox txtCurrentBalance;
        private System.Windows.Forms.Label lblAccountNumber;
        private System.Windows.Forms.Label lblCustomerId;
        private System.Windows.Forms.Label lblAccountStatus;
        private System.Windows.Forms.Label lblOpeningDate;
        private System.Windows.Forms.Label lblExpiryDate;
        private System.Windows.Forms.Label lblLastTxnDate;
        private System.Windows.Forms.Label lblClosingDate;
        private System.Windows.Forms.Label lblAccountName;
        private System.Windows.Forms.Label lblProduct;
        private System.Windows.Forms.Label lblCurrentBalance;
        private CustomTitlebar customTitlebar1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtAccountNumber;
        private System.Windows.Forms.GroupBox gbxITD;
        private Agent.CustomControls.CustomTextBox txtQuardAmount;
        private System.Windows.Forms.Label lblLienAmount;
        private System.Windows.Forms.Label lblQuardAmount;
        private Agent.CustomControls.CustomTextBox txtLienAmount;
        private Agent.CustomControls.CustomTextBox txtTotalNoofInstallment;
        private Agent.CustomControls.CustomTextBox txtInstallmentAmount;
        private System.Windows.Forms.Label lblTotalNoofInstallment;
        private System.Windows.Forms.Label lblInstallmentAmount;
        private Agent.CustomControls.CustomTextBox txtNoofAdvanceInstallment;
        private Agent.CustomControls.CustomTextBox txtNoDueInstallment;
        private System.Windows.Forms.Label lblNoofAdvanceInstallment;
        private Agent.CustomControls.CustomTextBox txtNoofGivenInstallment;
        private System.Windows.Forms.Label lblNoDueInstallment;
        private System.Windows.Forms.Label lblNoofGivenInstallment;
        private System.Windows.Forms.GroupBox gbxMTDR;
        private Agent.CustomControls.CustomTextBox txtRenewRate;
        private Agent.CustomControls.CustomTextBox txtLienAmount2;
        private Agent.CustomControls.CustomTextBox txtRenewDate;
        private System.Windows.Forms.Label lblRenewRate;
        private Agent.CustomControls.CustomTextBox txtQuardAmount2;
        private System.Windows.Forms.Label lblRenewDate;
        private System.Windows.Forms.Label lblLienAmount2;
        private System.Windows.Forms.Label lblQuardAmount2;
        private Agent.CustomControls.CustomTextBox txtClosingDate;
        private Agent.CustomControls.CustomTextBox txtExpiryDate;
        private Agent.CustomControls.CustomTextBox txtLastTxnDate;
        private Agent.CustomControls.CustomTextBox txtOpeningDate;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
    }
}