﻿using MISL.Ababil.Agent.Infrastructure.Models.models.transaction;
using MISL.Ababil.Agent.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Windows.Forms;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Report;
using MISL.Ababil.Agent.Infrastructure.Validation;
using MISL.Ababil.Agent.UI.forms.ProgressUI;
using MISL.Ababil.Agent.Infrastructure.Models.common;

namespace MISL.Ababil.Agent.UI.forms
{
    public partial class frmTransactionRecodeSearch : CustomForm
    {
        //private List<TransactionRecord> _transactionRecordSearch = new List<TransactionRecord>();
        List<AgentTransactionRecord> _transactionRecordSearch = new List<AgentTransactionRecord>();
        private int _columnLoaded = 0;
        List<TransactionalEventDto> _transactionalEvents = new List<TransactionalEventDto>();
        private DateTime txnDate;

        public frmTransactionRecodeSearch()
        {
            InitializeComponent();
            fillSetupData();
            mtbTransactionDate.Text = dtpTransactionDate.Value.ToString("dd-MM-yyyy");
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool validationCheck()
        {
            return ValidationManager.ValidateForm(this);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dvAllTransactinRecordSearch.DataSource = null;
                dvAllTransactinRecordSearch.Refresh();
                dvAllTransactinRecordSearch.Rows.Clear();
                dvAllTransactinRecordSearch.Columns.Clear();

                btnSearch.Enabled = false;

                //chacking valid date
                try
                {
                    string[] str = mtbTransactionDate.Text.Split('-');
                    DateTime d = new DateTime(int.Parse(str[2].Trim()), int.Parse(str[1].Trim()), int.Parse(str[0].Trim()));
                    dtpTransactionDate.Value = d;

                    //if ((UtilityServices.CheckDateDiff(dtpTransactionDate.Value, DateTime.Now) ?? true) == false)
                    //{
                    //    MsgBox.showWarning("Date difference cannot be more than " + ConfigurationSettings.AppSettings["dayOfDiff"] + " days!");
                    //    btnSearch.Enabled = true;
                    //    return;
                    //}
                }
                catch
                {
                    MsgBox.ShowError("Please enter the Issue Date in correct format.");
                    btnSearch.Enabled = true;
                    return;
                }

                txnDate = Convert.ToDateTime(dtpTransactionDate.Text.Trim());
                TransactionRecordSerchDto objTransactionRecordSerchDto = new TransactionRecordSerchDto
                {
                    voucherNumber = txtVoucherNumber.Text,
                    transactionDate = UtilityServices.GetLongDate(txnDate),
                    eventId = Convert.ToInt32(cmbAgentServices.SelectedValue)
                };


                //AgentServicesType status = new AgentServicesType();
                //Enum.TryParse<AgentServicesType>(cmbAgentServices.Text, out status);
                //objTransactionRecordSerchDto.agentServicesType = status;

                ProgressUIManager.ShowProgress(this);
                loadAllApplications(objTransactionRecordSerchDto);
                ProgressUIManager.CloseProgress();

                btnSearch.Enabled = true;
            }
            catch (Exception exp)
            {
                ProgressUIManager.CloseProgress();
                btnSearch.Enabled = true;
                MsgBox.ShowError(exp.Message);
            }

        }

        private void loadAllApplications(TransactionRecordSerchDto transactionRecordSerchDto)
        {
            try
            {
                _transactionRecordSearch = TransactionService.getTransactionReocrd(transactionRecordSerchDto);
                if (_transactionRecordSearch != null)
                {
                    dvAllTransactinRecordSearch.DataSource = _transactionRecordSearch.Select(o => new TransactionRecordGrid(o)
                    {
                        VoucherNumber = o.txnRefno,
                        Amount = o.txnAmount ?? 0,
                        TransactionDate = txnDate.ToString("dd-MMM-yyyy"),
                        //TransactionDate = o.txnDate.ToString("dd-MM-yyyy").Replace("-", "/"),
                        //AgentServices = o.agentServices,
                        SubAgentUser = o.txnUser,
                        DebitAccount = o.debitAccount,
                        CreditAccount = o.creditAccount
                    }
                                                                                            ).ToList();
                    //if (_columnLoaded == 0)
                    //{
                    DataGridViewButtonColumn buttonColumn = new DataGridViewButtonColumn();
                    buttonColumn.Text = "View Report";
                    buttonColumn.UseColumnTextForButtonValue = true;
                    dvAllTransactinRecordSearch.Columns.Add(buttonColumn);
                    _columnLoaded = 1;
                    //}
                    //else
                    //{
                    //    dvAllTransactinRecordSearch.Columns[0].DisplayIndex = 6;
                    //}
                }
                else
                {
                    MessageBox.Show("No Transaction record available");
                }

                lblItemsFound.Text = "Item(s) Found: " + dvAllTransactinRecordSearch.Rows.Count.ToString();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        private void fillSetupData()
        {
            //AgentServicesType[] agentServiceTypeArray =  (AgentServicesType[])Enum.GetValues(typeof(AgentServicesType));
            //List<AgentServicesType> agentServiceTypeList = agentServiceTypeArray.ToList();
            //agentServiceTypeList.Remove(AgentServicesType.MiniStatement);
            //agentServiceTypeList.Remove(AgentServicesType.BalanceEnquiry);
            //agentServiceTypeList.Remove(AgentServicesType.ChequeRequisition);
            //agentServiceTypeList.Remove(AgentServicesType.Finance);
            //cmbAgentServices.DataSource = agentServiceTypeList;
            //dtpTransactionDate.Value = SessionInfo.currentDate;

            try
            {
                dtpTransactionDate.Value = SessionInfo.currentDate;

                _transactionalEvents = TransactionService.getTransactionalEvents();
                BindingSource bs = new BindingSource();
                bs.DataSource = _transactionalEvents;
                UtilityServices.fillComboBox(cmbAgentServices, bs, "eventName", "eventId");
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp.Message);
            }
        }

        public class TransactionRecordGrid
        {
            //public AgentServicesType AgentServices { get; set; }
            public string TransactionDate { get; set; }
            public decimal Amount { get; set; }
            public string SubAgentUser { get; set; }
            public string DebitAccount { get; set; }
            public string CreditAccount { get; set; }
            public string VoucherNumber { get; set; }

            public AgentTransactionRecord _obj;
            //public TransactionRecord _obj;

            public TransactionRecordGrid(AgentTransactionRecord obj)
            {
                _obj = obj;
            }
            //public TransactionRecordGrid(TransactionRecord obj)
            //{
            //    _obj = obj;
            //}

            public AgentTransactionRecord GetModel()
            {
                return _obj;
            }
        }

        private void dvAllTransactinRecordSearch_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                var senderGrid = (DataGridView)sender;

                if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
                {
                    //TransactionRecord transactionRecordView = new TransactionRecord();
                    AgentTransactionRecord transactionRecordView = new AgentTransactionRecord();
                    transactionRecordView = _transactionRecordSearch[e.RowIndex];
                    frmShowReport objFrmReport = new frmShowReport();

                    #region OLD...
                    //if (transactionRecordView.agentServices == AgentServicesType.CashWithdraw)
                    //{
                    //    objFrmReport.WithDrawlReport(transactionRecordView.voucherNumber);
                    //}
                    //if (transactionRecordView.agentServices == AgentServicesType.CashDeposit)
                    //{
                    //    objFrmReport.DepositeReport(transactionRecordView.voucherNumber);
                    //}
                    //if (transactionRecordView.agentServices == AgentServicesType.FundTransfer)
                    //{
                    //    objFrmReport.MoneyTransferReport(transactionRecordView.voucherNumber);
                    //}
                    //if (transactionRecordView.agentServices == AgentServicesType.Remittance)
                    //{
                    //    objFrmReport.ShowRemittanceReport(transactionRecordView.voucherNumber);
                    //}
                    //if (transactionRecordView.agentServices == AgentServicesType.BillPayment)
                    //{
                    //    objFrmReport.BillReportDto(transactionRecordView.voucherNumber);
                    //}
                    //if (transactionRecordView.agentServices == AgentServicesType.Itd)
                    //{
                    //    objFrmReport.ItdDirectDepostReport(transactionRecordView.voucherNumber);
                    //}
                    #endregion

                    int eventId = Convert.ToInt32(cmbAgentServices.SelectedValue);

                    if (eventId == 8)
                    {
                        objFrmReport.WithDrawlReport(transactionRecordView.txnRefno);
                    }
                    else if (eventId == 7)
                    {
                        objFrmReport.DepositeReport(transactionRecordView.txnRefno);
                    }
                    else if (eventId == 9)
                    {
                        objFrmReport.MoneyTransferReport(transactionRecordView.txnRefno);
                    }
                    else if (eventId == 16)
                    {
                        objFrmReport.BillReportDto(transactionRecordView.txnRefno);
                    }
                    else if (eventId == 19)
                    {
                        objFrmReport.ItdDirectDepostReport(transactionRecordView.txnRefno);
                    }
                    //else if (eventId == 15)
                    //{
                    //    objFrmReport.ShowRemittanceReport(transactionRecordView.txnRefno);
                    //}
                }
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp.Message);
            }

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            dvAllTransactinRecordSearch.DataSource = null;
            dvAllTransactinRecordSearch.Rows.Clear();
        }

        private void mtbTransactionDate_KeyUp(object sender, KeyEventArgs e)
        {
            //suppressed to avoid mtb to dtp conversion
            try
            {
                string[] str = mtbTransactionDate.Text.Split('-');
                DateTime d = new DateTime(int.Parse(str[2].Trim()), int.Parse(str[1].Trim()), int.Parse(str[0].Trim()));
                dtpTransactionDate.Value = d;
            }
            catch (Exception ex) { }
        }

        private void dtpTransactionDate_ValueChanged(object sender, EventArgs e)
        {
            mtbTransactionDate.Text = dtpTransactionDate.Value.ToString("dd-MM-yyyy");
        }

        private void frmTransactionRecodeSearch_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }
    }
}