﻿namespace MISL.Ababil.Agent.UI.forms
{
    partial class frmPasswordChange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPasswordChange));
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblCurrentpassword = new System.Windows.Forms.Label();
            this.lblNewPassword = new System.Windows.Forms.Label();
            this.lblRetypeNewPassword = new System.Windows.Forms.Label();
            this.lblUserNameText = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtCurrentPassword = new MetroFramework.Controls.MetroTextBox();
            this.txtNewPassword = new MetroFramework.Controls.MetroTextBox();
            this.txtRetypeNewPassword = new MetroFramework.Controls.MetroTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.mandatoryMark1 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark2 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark3 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.btnFingerPrint = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.lblUserName.Location = new System.Drawing.Point(252, 102);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(82, 20);
            this.lblUserName.TabIndex = 0;
            this.lblUserName.Text = "Username :";
            // 
            // lblCurrentpassword
            // 
            this.lblCurrentpassword.AutoSize = true;
            this.lblCurrentpassword.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.lblCurrentpassword.Location = new System.Drawing.Point(205, 133);
            this.lblCurrentpassword.Name = "lblCurrentpassword";
            this.lblCurrentpassword.Size = new System.Drawing.Size(129, 20);
            this.lblCurrentpassword.TabIndex = 2;
            this.lblCurrentpassword.Text = "Current Password :";
            // 
            // lblNewPassword
            // 
            this.lblNewPassword.AutoSize = true;
            this.lblNewPassword.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.lblNewPassword.Location = new System.Drawing.Point(223, 165);
            this.lblNewPassword.Name = "lblNewPassword";
            this.lblNewPassword.Size = new System.Drawing.Size(111, 20);
            this.lblNewPassword.TabIndex = 5;
            this.lblNewPassword.Text = "New Password :";
            // 
            // lblRetypeNewPassword
            // 
            this.lblRetypeNewPassword.AutoSize = true;
            this.lblRetypeNewPassword.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.lblRetypeNewPassword.Location = new System.Drawing.Point(167, 197);
            this.lblRetypeNewPassword.Name = "lblRetypeNewPassword";
            this.lblRetypeNewPassword.Size = new System.Drawing.Size(167, 20);
            this.lblRetypeNewPassword.TabIndex = 8;
            this.lblRetypeNewPassword.Text = "Re-type New Password :";
            // 
            // lblUserNameText
            // 
            this.lblUserNameText.AutoSize = true;
            this.lblUserNameText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserNameText.Location = new System.Drawing.Point(340, 103);
            this.lblUserNameText.Name = "lblUserNameText";
            this.lblUserNameText.Size = new System.Drawing.Size(82, 20);
            this.lblUserNameText.TabIndex = 1;
            this.lblUserNameText.Text = "userName";
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnCancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(49)))), ((int)(((byte)(69)))));
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(514, 292);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 27);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnOK.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(49)))), ((int)(((byte)(69)))));
            this.btnOK.FlatAppearance.BorderSize = 0;
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOK.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.btnOK.ForeColor = System.Drawing.Color.White;
            this.btnOK.Location = new System.Drawing.Point(342, 292);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(80, 27);
            this.btnOK.TabIndex = 12;
            this.btnOK.Text = "Change";
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(-1, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(114, 341);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // txtCurrentPassword
            // 
            this.txtCurrentPassword.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtCurrentPassword.Lines = new string[0];
            this.txtCurrentPassword.Location = new System.Drawing.Point(340, 131);
            this.txtCurrentPassword.MaxLength = 32767;
            this.txtCurrentPassword.Name = "txtCurrentPassword";
            this.txtCurrentPassword.PasswordChar = '•';
            this.txtCurrentPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCurrentPassword.SelectedText = "";
            this.txtCurrentPassword.Size = new System.Drawing.Size(254, 25);
            this.txtCurrentPassword.TabIndex = 3;
            this.txtCurrentPassword.UseSelectable = true;
            this.txtCurrentPassword.Enter += new System.EventHandler(this.metroTextBox1_Enter);
            this.txtCurrentPassword.Leave += new System.EventHandler(this.metroTextBox1_Leave);
            // 
            // txtNewPassword
            // 
            this.txtNewPassword.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtNewPassword.Lines = new string[0];
            this.txtNewPassword.Location = new System.Drawing.Point(340, 163);
            this.txtNewPassword.MaxLength = 32767;
            this.txtNewPassword.Name = "txtNewPassword";
            this.txtNewPassword.PasswordChar = '•';
            this.txtNewPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNewPassword.SelectedText = "";
            this.txtNewPassword.Size = new System.Drawing.Size(254, 25);
            this.txtNewPassword.TabIndex = 6;
            this.txtNewPassword.UseSelectable = true;
            this.txtNewPassword.Enter += new System.EventHandler(this.metroTextBox1_Enter);
            this.txtNewPassword.Leave += new System.EventHandler(this.metroTextBox1_Leave);
            // 
            // txtRetypeNewPassword
            // 
            this.txtRetypeNewPassword.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtRetypeNewPassword.Lines = new string[0];
            this.txtRetypeNewPassword.Location = new System.Drawing.Point(340, 195);
            this.txtRetypeNewPassword.MaxLength = 32767;
            this.txtRetypeNewPassword.Name = "txtRetypeNewPassword";
            this.txtRetypeNewPassword.PasswordChar = '•';
            this.txtRetypeNewPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtRetypeNewPassword.SelectedText = "";
            this.txtRetypeNewPassword.Size = new System.Drawing.Size(254, 25);
            this.txtRetypeNewPassword.TabIndex = 9;
            this.txtRetypeNewPassword.UseSelectable = true;
            this.txtRetypeNewPassword.Enter += new System.EventHandler(this.metroTextBox1_Enter);
            this.txtRetypeNewPassword.Leave += new System.EventHandler(this.metroTextBox1_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(283, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(210, 26);
            this.label1.TabIndex = 16;
            this.label1.Text = "Change Credential";
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnClear.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(49)))), ((int)(((byte)(69)))));
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(428, 292);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(80, 27);
            this.btnClear.TabIndex = 13;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(49)))), ((int)(((byte)(69)))));
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Location = new System.Drawing.Point(514, 259);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(80, 27);
            this.btnExit.TabIndex = 15;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // mandatoryMark1
            // 
            this.mandatoryMark1.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark1.Location = new System.Drawing.Point(599, 134);
            this.mandatoryMark1.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.Name = "mandatoryMark1";
            this.mandatoryMark1.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.TabIndex = 4;
            this.mandatoryMark1.TabStop = false;
            // 
            // mandatoryMark2
            // 
            this.mandatoryMark2.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark2.Location = new System.Drawing.Point(599, 164);
            this.mandatoryMark2.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.Name = "mandatoryMark2";
            this.mandatoryMark2.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.TabIndex = 7;
            this.mandatoryMark2.TabStop = false;
            // 
            // mandatoryMark3
            // 
            this.mandatoryMark3.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark3.Location = new System.Drawing.Point(599, 196);
            this.mandatoryMark3.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark3.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark3.Name = "mandatoryMark3";
            this.mandatoryMark3.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark3.TabIndex = 10;
            this.mandatoryMark3.TabStop = false;
            // 
            // btnFingerPrint
            // 
            this.btnFingerPrint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnFingerPrint.FlatAppearance.BorderSize = 0;
            this.btnFingerPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFingerPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFingerPrint.ForeColor = System.Drawing.Color.White;
            this.btnFingerPrint.Location = new System.Drawing.Point(340, 226);
            this.btnFingerPrint.Name = "btnFingerPrint";
            this.btnFingerPrint.Size = new System.Drawing.Size(153, 23);
            this.btnFingerPrint.TabIndex = 11;
            this.btnFingerPrint.Text = "Capture Fingerprint";
            this.btnFingerPrint.UseVisualStyleBackColor = false;
            this.btnFingerPrint.Click += new System.EventHandler(this.btnFingerPrint_Click);
            // 
            // frmPasswordChange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 346);
            this.Controls.Add(this.btnFingerPrint);
            this.Controls.Add(this.mandatoryMark3);
            this.Controls.Add(this.mandatoryMark2);
            this.Controls.Add(this.mandatoryMark1);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.txtRetypeNewPassword);
            this.Controls.Add(this.txtNewPassword);
            this.Controls.Add(this.txtCurrentPassword);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblUserNameText);
            this.Controls.Add(this.lblRetypeNewPassword);
            this.Controls.Add(this.lblNewPassword);
            this.Controls.Add(this.lblCurrentpassword);
            this.Controls.Add(this.lblUserName);
            this.DisplayHeader = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPasswordChange";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.DropShadow;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Change Password";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPasswordChange_FormClosing);
            this.Load += new System.EventHandler(this.frmPasswordChange_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.frmPasswordChange_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblCurrentpassword;
        private System.Windows.Forms.Label lblNewPassword;
        private System.Windows.Forms.Label lblRetypeNewPassword;
        private System.Windows.Forms.Label lblUserNameText;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroTextBox txtCurrentPassword;
        private MetroFramework.Controls.MetroTextBox txtNewPassword;
        private MetroFramework.Controls.MetroTextBox txtRetypeNewPassword;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnExit;
        private CustomControls.MandatoryMark mandatoryMark1;
        private CustomControls.MandatoryMark mandatoryMark2;
        private CustomControls.MandatoryMark mandatoryMark3;
        public System.Windows.Forms.Button btnFingerPrint;
    }
}