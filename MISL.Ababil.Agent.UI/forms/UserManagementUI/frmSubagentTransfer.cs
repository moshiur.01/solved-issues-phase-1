﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.UI.forms.ProgressUI;

namespace MISL.Ababil.Agent.UI.forms.UserManagementUI
{
    public partial class frmSubagentTransfer : CustomForm
    {
        private AgentUserDto _agentUserDto;
        private string _outletName;
        private string _agentName;

        public frmSubagentTransfer(Packet packet, AgentUserDto agentUserDto, string outletName, string agentName)
        {
            InitializeComponent();
            _agentUserDto = agentUserDto;
            _outletName = outletName;
            _agentName = agentName;

            SetupComponent();
            ClearComponenet();
            FillComponentWithObjectValue();
        }

        private void ClearComponenet()
        {
            lblUsername.Text = string.Empty;
            lblName.Text = string.Empty;
            lblFatherName.Text = string.Empty;
            lblMobileNumber.Text = string.Empty;

            lblPresentAgent.Text = string.Empty;
            lblPresentOutlet.Text = string.Empty;
            lblPresentDivision.Text = string.Empty;
            lblPresentDistrict.Text = string.Empty;
            lblPresentThana.Text = string.Empty;

            lblTransferAgent.Text = string.Empty;
            lblTransferDivision.Text = string.Empty;
            lblTransferDistrict.Text = string.Empty;
            lblTransferThana.Text = string.Empty;
        }

        private void SetupComponent()
        {
            UtilityServices.fillComboBox(cmbTransferOutlet, new BindingSource
            {
                DataSource = new SubAgentServices().GetAllSubAgents()
            }, "name", "id");
            cmbTransferOutlet.SelectedIndex = -1;
        }

        private void FillComponentWithObjectValue()
        {
            //if (_agentUserDto.image != null && _agentUserDto.image.Length > 0)
            //{
            //    pbxUser.Image = UtilityServices.byteArrayToImage(_agentUserDto.image);
            //}
            //else
            //{
            //    pbxUser.Image = null;
            //}

            lblUsername.Text = _agentUserDto.subAgentUser.username;
            lblName.Text = _agentUserDto.individualInformation.firstName + " " + _agentUserDto.individualInformation.lastName;
            lblFatherName.Text = _agentUserDto.individualInformation.fatherFirstName + " " + _agentUserDto.individualInformation.fatherFirstName;
            lblMobileNumber.Text = _agentUserDto.individualInformation.mobileNo;


            lblPresentAgent.Text = _agentName; //_agentUserDto.subAgentUser.subAgentInformation.agent.businessName;
            lblPresentOutlet.Text = _outletName; //_agentUserDto.subAgentUser.subAgentInformation.name;
            lblPresentDivision.Text = _agentUserDto.individualInformation.presentAddress.division.name;
            lblPresentDistrict.Text = _agentUserDto.individualInformation.presentAddress.district.title;
            lblPresentThana.Text = _agentUserDto.individualInformation.presentAddress.thana.title;
        }

        private void btnClsoe_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmSubagentTransfer_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void frmSubagentTransfer_Load(object sender, EventArgs e)
        {

        }

        private void btnTransfer_Click(object sender, EventArgs e)
        {
            try
            {
                ProgressUIManager.ShowProgress(this);
                new SubagentTransferService().TransferSubagentUser(_agentUserDto.subAgentUser.username,
                    cmbTransferOutlet.SelectedValue.ToString());
                ProgressUIManager.CloseProgress();
                MsgBox.showInformation("Outlet user is transfered successfully.");
                this.Close();
            }
            catch (Exception ex)
            {
                ProgressUIManager.CloseProgress();
                MsgBox.ShowError(ex.Message);
            }
        }

        private void cmbTransferOutlet_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!cmbTransferOutlet.Focused)
            {
                return;
            }
            try
            {
                ProgressUIManager.ShowProgress(this);
                SubAgentInformation subAgentInformation = new SubAgentServices().getSubAgentDetailsById(long.Parse(cmbTransferOutlet.SelectedValue.ToString())).ReturnedObject as SubAgentInformation;
                lblTransferAgent.Text = subAgentInformation.agent.businessName;
                lblTransferDivision.Text = subAgentInformation.businessAddress.division.name;
                lblTransferDistrict.Text = subAgentInformation.businessAddress.district.title;
                lblTransferThana.Text = subAgentInformation.businessAddress.thana.title;
                ProgressUIManager.CloseProgress();
            }
            catch (Exception ex)
            {
                ProgressUIManager.CloseProgress();
                MsgBox.ShowError(ex.Message);
            }
        }
    }
}