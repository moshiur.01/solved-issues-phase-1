﻿namespace MISL.Ababil.Agent.UI.forms.UserManagementUI
{
    partial class frmSubagentTransfer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPresentAgent = new System.Windows.Forms.Label();
            this.lblPresentOutlet = new System.Windows.Forms.Label();
            this.lblUsername = new System.Windows.Forms.Label();
            this.lblPresentDivision = new System.Windows.Forms.Label();
            this.lblPresentDistrict = new System.Windows.Forms.Label();
            this.lblPresentThana = new System.Windows.Forms.Label();
            this.btnClsoe = new System.Windows.Forms.Button();
            this.btnTransfer = new System.Windows.Forms.Button();
            this.cmbTransferOutlet = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lblTransferDivision = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.lblTransferDistrict = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.lblTransferThana = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.lblTransferAgent = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.pbxUser = new System.Windows.Forms.PictureBox();
            this.lblName = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.lblFatherName = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.lblMobileNumber = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.customHeaderControl1 = new MISL.Ababil.Agent.CustomControls.CustomHeaderControl();
            this.customHeaderControl2 = new MISL.Ababil.Agent.CustomControls.CustomHeaderControl();
            this.customHeaderControl3 = new MISL.Ababil.Agent.CustomControls.CustomHeaderControl();
            ((System.ComponentModel.ISupportInitialize)(this.pbxUser)).BeginInit();
            this.SuspendLayout();
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(517, 169);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Thana";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(517, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "District";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(517, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Division";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(517, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Outlet";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(517, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Agent";
            // 
            // lblPresentAgent
            // 
            this.lblPresentAgent.AutoSize = true;
            this.lblPresentAgent.BackColor = System.Drawing.Color.White;
            this.lblPresentAgent.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPresentAgent.ForeColor = System.Drawing.Color.Black;
            this.lblPresentAgent.Location = new System.Drawing.Point(627, 66);
            this.lblPresentAgent.Name = "lblPresentAgent";
            this.lblPresentAgent.Size = new System.Drawing.Size(54, 13);
            this.lblPresentAgent.TabIndex = 8;
            this.lblPresentAgent.Text = "<Agent>";
            // 
            // lblPresentOutlet
            // 
            this.lblPresentOutlet.AutoSize = true;
            this.lblPresentOutlet.BackColor = System.Drawing.Color.White;
            this.lblPresentOutlet.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPresentOutlet.ForeColor = System.Drawing.Color.Black;
            this.lblPresentOutlet.Location = new System.Drawing.Point(627, 92);
            this.lblPresentOutlet.Name = "lblPresentOutlet";
            this.lblPresentOutlet.Size = new System.Drawing.Size(56, 13);
            this.lblPresentOutlet.TabIndex = 8;
            this.lblPresentOutlet.Text = "<Outlet>";
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.BackColor = System.Drawing.Color.White;
            this.lblUsername.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsername.ForeColor = System.Drawing.Color.Black;
            this.lblUsername.Location = new System.Drawing.Point(134, 80);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(124, 25);
            this.lblUsername.TabIndex = 8;
            this.lblUsername.Text = "<Username>";
            // 
            // lblPresentDivision
            // 
            this.lblPresentDivision.AutoSize = true;
            this.lblPresentDivision.BackColor = System.Drawing.Color.White;
            this.lblPresentDivision.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPresentDivision.ForeColor = System.Drawing.Color.Black;
            this.lblPresentDivision.Location = new System.Drawing.Point(628, 118);
            this.lblPresentDivision.Name = "lblPresentDivision";
            this.lblPresentDivision.Size = new System.Drawing.Size(64, 13);
            this.lblPresentDivision.TabIndex = 8;
            this.lblPresentDivision.Text = "<Division>";
            // 
            // lblPresentDistrict
            // 
            this.lblPresentDistrict.AutoSize = true;
            this.lblPresentDistrict.BackColor = System.Drawing.Color.White;
            this.lblPresentDistrict.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPresentDistrict.ForeColor = System.Drawing.Color.Black;
            this.lblPresentDistrict.Location = new System.Drawing.Point(628, 144);
            this.lblPresentDistrict.Name = "lblPresentDistrict";
            this.lblPresentDistrict.Size = new System.Drawing.Size(59, 13);
            this.lblPresentDistrict.TabIndex = 8;
            this.lblPresentDistrict.Text = "<District>";
            // 
            // lblPresentThana
            // 
            this.lblPresentThana.AutoSize = true;
            this.lblPresentThana.BackColor = System.Drawing.Color.White;
            this.lblPresentThana.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPresentThana.ForeColor = System.Drawing.Color.Black;
            this.lblPresentThana.Location = new System.Drawing.Point(628, 169);
            this.lblPresentThana.Name = "lblPresentThana";
            this.lblPresentThana.Size = new System.Drawing.Size(54, 13);
            this.lblPresentThana.TabIndex = 8;
            this.lblPresentThana.Text = "<Thana>";
            // 
            // btnClsoe
            // 
            this.btnClsoe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClsoe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnClsoe.FlatAppearance.BorderSize = 0;
            this.btnClsoe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClsoe.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClsoe.ForeColor = System.Drawing.Color.White;
            this.btnClsoe.Location = new System.Drawing.Point(690, 327);
            this.btnClsoe.Name = "btnClsoe";
            this.btnClsoe.Size = new System.Drawing.Size(110, 30);
            this.btnClsoe.TabIndex = 16;
            this.btnClsoe.Text = "Clsoe";
            this.btnClsoe.UseVisualStyleBackColor = false;
            this.btnClsoe.Click += new System.EventHandler(this.btnClsoe_Click);
            // 
            // btnTransfer
            // 
            this.btnTransfer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTransfer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnTransfer.FlatAppearance.BorderSize = 0;
            this.btnTransfer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTransfer.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTransfer.ForeColor = System.Drawing.Color.White;
            this.btnTransfer.Location = new System.Drawing.Point(690, 291);
            this.btnTransfer.Name = "btnTransfer";
            this.btnTransfer.Size = new System.Drawing.Size(110, 30);
            this.btnTransfer.TabIndex = 16;
            this.btnTransfer.Text = "Transfer";
            this.btnTransfer.UseVisualStyleBackColor = false;
            this.btnTransfer.Click += new System.EventHandler(this.btnTransfer_Click);
            // 
            // cmbTransferOutlet
            // 
            this.cmbTransferOutlet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTransferOutlet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbTransferOutlet.FormattingEnabled = true;
            this.cmbTransferOutlet.InputScopeAllowEmpty = false;
            this.cmbTransferOutlet.IsValid = null;
            this.cmbTransferOutlet.Location = new System.Drawing.Point(132, 234);
            this.cmbTransferOutlet.Name = "cmbTransferOutlet";
            this.cmbTransferOutlet.PromptText = "(Select)";
            this.cmbTransferOutlet.ReadOnly = false;
            this.cmbTransferOutlet.ShowMandatoryMark = false;
            this.cmbTransferOutlet.Size = new System.Drawing.Size(373, 21);
            this.cmbTransferOutlet.TabIndex = 17;
            this.cmbTransferOutlet.ValidationErrorMessage = "Validation Error!";
            this.cmbTransferOutlet.SelectedIndexChanged += new System.EventHandler(this.cmbTransferOutlet_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(14, 237);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(97, 13);
            this.label13.TabIndex = 20;
            this.label13.Text = "Transfer To Outlet";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.White;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(618, 118);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(10, 13);
            this.label14.TabIndex = 8;
            this.label14.Text = ":";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.White;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(618, 144);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(10, 13);
            this.label15.TabIndex = 8;
            this.label15.Text = ":";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.White;
            this.label16.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(618, 169);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(10, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = ":";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.White;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(618, 66);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(10, 13);
            this.label17.TabIndex = 8;
            this.label17.Text = ":";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.White;
            this.label18.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(618, 92);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(10, 13);
            this.label18.TabIndex = 8;
            this.label18.Text = ":";
            // 
            // lblTransferDivision
            // 
            this.lblTransferDivision.AutoSize = true;
            this.lblTransferDivision.BackColor = System.Drawing.Color.White;
            this.lblTransferDivision.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTransferDivision.ForeColor = System.Drawing.Color.Black;
            this.lblTransferDivision.Location = new System.Drawing.Point(126, 289);
            this.lblTransferDivision.Name = "lblTransferDivision";
            this.lblTransferDivision.Size = new System.Drawing.Size(64, 13);
            this.lblTransferDivision.TabIndex = 8;
            this.lblTransferDivision.Text = "<Division>";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.White;
            this.label21.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(115, 289);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(10, 13);
            this.label21.TabIndex = 8;
            this.label21.Text = ":";
            // 
            // lblTransferDistrict
            // 
            this.lblTransferDistrict.AutoSize = true;
            this.lblTransferDistrict.BackColor = System.Drawing.Color.White;
            this.lblTransferDistrict.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTransferDistrict.ForeColor = System.Drawing.Color.Black;
            this.lblTransferDistrict.Location = new System.Drawing.Point(126, 316);
            this.lblTransferDistrict.Name = "lblTransferDistrict";
            this.lblTransferDistrict.Size = new System.Drawing.Size(59, 13);
            this.lblTransferDistrict.TabIndex = 8;
            this.lblTransferDistrict.Text = "<District>";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.White;
            this.label23.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(115, 316);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(10, 13);
            this.label23.TabIndex = 8;
            this.label23.Text = ":";
            // 
            // lblTransferThana
            // 
            this.lblTransferThana.AutoSize = true;
            this.lblTransferThana.BackColor = System.Drawing.Color.White;
            this.lblTransferThana.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTransferThana.ForeColor = System.Drawing.Color.Black;
            this.lblTransferThana.Location = new System.Drawing.Point(126, 343);
            this.lblTransferThana.Name = "lblTransferThana";
            this.lblTransferThana.Size = new System.Drawing.Size(54, 13);
            this.lblTransferThana.TabIndex = 8;
            this.lblTransferThana.Text = "<Thana>";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.White;
            this.label25.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(115, 343);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(10, 13);
            this.label25.TabIndex = 8;
            this.label25.Text = ":";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.White;
            this.label26.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(14, 289);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(48, 13);
            this.label26.TabIndex = 11;
            this.label26.Text = "Division";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.White;
            this.label27.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(14, 316);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(43, 13);
            this.label27.TabIndex = 13;
            this.label27.Text = "District";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.White;
            this.label28.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(14, 343);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(38, 13);
            this.label28.TabIndex = 12;
            this.label28.Text = "Thana";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.White;
            this.label29.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(115, 237);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(10, 13);
            this.label29.TabIndex = 8;
            this.label29.Text = ":";
            // 
            // lblTransferAgent
            // 
            this.lblTransferAgent.AutoSize = true;
            this.lblTransferAgent.BackColor = System.Drawing.Color.White;
            this.lblTransferAgent.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTransferAgent.ForeColor = System.Drawing.Color.Black;
            this.lblTransferAgent.Location = new System.Drawing.Point(126, 264);
            this.lblTransferAgent.Name = "lblTransferAgent";
            this.lblTransferAgent.Size = new System.Drawing.Size(54, 13);
            this.lblTransferAgent.TabIndex = 8;
            this.lblTransferAgent.Text = "<Agent>";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.White;
            this.label31.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(115, 264);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(10, 13);
            this.label31.TabIndex = 8;
            this.label31.Text = ":";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.White;
            this.label32.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(14, 264);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(38, 13);
            this.label32.TabIndex = 12;
            this.label32.Text = "Agent";
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(812, 26);
            this.customTitlebar1.TabIndex = 21;
            this.customTitlebar1.TabStop = false;
            // 
            // pbxUser
            // 
            this.pbxUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbxUser.Location = new System.Drawing.Point(17, 60);
            this.pbxUser.Name = "pbxUser";
            this.pbxUser.Size = new System.Drawing.Size(113, 127);
            this.pbxUser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxUser.TabIndex = 22;
            this.pbxUser.TabStop = false;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.BackColor = System.Drawing.Color.White;
            this.lblName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ForeColor = System.Drawing.Color.Black;
            this.lblName.Location = new System.Drawing.Point(246, 118);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(52, 13);
            this.lblName.TabIndex = 8;
            this.lblName.Text = "<Name>";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.White;
            this.label34.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(237, 118);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(10, 13);
            this.label34.TabIndex = 8;
            this.label34.Text = ":";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.White;
            this.label35.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(136, 118);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(36, 13);
            this.label35.TabIndex = 9;
            this.label35.Text = "Name";
            // 
            // lblFatherName
            // 
            this.lblFatherName.AutoSize = true;
            this.lblFatherName.BackColor = System.Drawing.Color.White;
            this.lblFatherName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFatherName.ForeColor = System.Drawing.Color.Black;
            this.lblFatherName.Location = new System.Drawing.Point(246, 144);
            this.lblFatherName.Name = "lblFatherName";
            this.lblFatherName.Size = new System.Drawing.Size(96, 13);
            this.lblFatherName.TabIndex = 8;
            this.lblFatherName.Text = "<Father\'s Name>";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.White;
            this.label37.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(237, 144);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(10, 13);
            this.label37.TabIndex = 8;
            this.label37.Text = ":";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.White;
            this.label38.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(136, 144);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(80, 13);
            this.label38.TabIndex = 9;
            this.label38.Text = "Father\'s Name";
            // 
            // lblMobileNumber
            // 
            this.lblMobileNumber.AutoSize = true;
            this.lblMobileNumber.BackColor = System.Drawing.Color.White;
            this.lblMobileNumber.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMobileNumber.ForeColor = System.Drawing.Color.Black;
            this.lblMobileNumber.Location = new System.Drawing.Point(246, 169);
            this.lblMobileNumber.Name = "lblMobileNumber";
            this.lblMobileNumber.Size = new System.Drawing.Size(100, 13);
            this.lblMobileNumber.TabIndex = 8;
            this.lblMobileNumber.Text = "<MobileNumber>";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.White;
            this.label40.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(237, 169);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(10, 13);
            this.label40.TabIndex = 8;
            this.label40.Text = ":";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.White;
            this.label41.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(136, 169);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(87, 13);
            this.label41.TabIndex = 9;
            this.label41.Text = "Mobile Number";
            // 
            // customHeaderControl1
            // 
            this.customHeaderControl1.BackColor = System.Drawing.Color.White;
            this.customHeaderControl1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.customHeaderControl1.Location = new System.Drawing.Point(17, 38);
            this.customHeaderControl1.MinimumSize = new System.Drawing.Size(0, 16);
            this.customHeaderControl1.Name = "customHeaderControl1";
            this.customHeaderControl1.Size = new System.Drawing.Size(488, 16);
            this.customHeaderControl1.TabIndex = 23;
            this.customHeaderControl1.Text = " User Information ";
            // 
            // customHeaderControl2
            // 
            this.customHeaderControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customHeaderControl2.BackColor = System.Drawing.Color.White;
            this.customHeaderControl2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.customHeaderControl2.Location = new System.Drawing.Point(520, 38);
            this.customHeaderControl2.MinimumSize = new System.Drawing.Size(0, 16);
            this.customHeaderControl2.Name = "customHeaderControl2";
            this.customHeaderControl2.Size = new System.Drawing.Size(280, 16);
            this.customHeaderControl2.TabIndex = 23;
            this.customHeaderControl2.Text = " Present Outlet ";
            // 
            // customHeaderControl3
            // 
            this.customHeaderControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customHeaderControl3.BackColor = System.Drawing.Color.White;
            this.customHeaderControl3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.customHeaderControl3.Location = new System.Drawing.Point(17, 205);
            this.customHeaderControl3.MinimumSize = new System.Drawing.Size(0, 16);
            this.customHeaderControl3.Name = "customHeaderControl3";
            this.customHeaderControl3.Size = new System.Drawing.Size(783, 16);
            this.customHeaderControl3.TabIndex = 23;
            this.customHeaderControl3.Text = " Transfer Information ";
            // 
            // frmSubagentTransfer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(812, 369);
            this.Controls.Add(this.cmbTransferOutlet);
            this.Controls.Add(this.customHeaderControl3);
            this.Controls.Add(this.customHeaderControl2);
            this.Controls.Add(this.customHeaderControl1);
            this.Controls.Add(this.pbxUser);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.btnTransfer);
            this.Controls.Add(this.btnClsoe);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.lblTransferAgent);
            this.Controls.Add(this.lblTransferThana);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.lblPresentThana);
            this.Controls.Add(this.lblMobileNumber);
            this.Controls.Add(this.lblFatherName);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.lblTransferDistrict);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.lblPresentDistrict);
            this.Controls.Add(this.lblPresentOutlet);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.lblTransferDivision);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.lblPresentDivision);
            this.Controls.Add(this.lblPresentAgent);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSubagentTransfer";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Transfer Outlet User";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSubagentTransfer_FormClosing);
            this.Load += new System.EventHandler(this.frmSubagentTransfer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbxUser)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblPresentAgent;
        private System.Windows.Forms.Label lblPresentOutlet;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label lblPresentDivision;
        private System.Windows.Forms.Label lblPresentDistrict;
        private System.Windows.Forms.Label lblPresentThana;
        private System.Windows.Forms.Button btnClsoe;
        private System.Windows.Forms.Button btnTransfer;
        private Agent.CustomControls.CustomComboBoxDropDownList cmbTransferOutlet;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lblTransferDivision;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label lblTransferDistrict;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label lblTransferThana;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label lblTransferAgent;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private Agent.CustomControls.CustomTitlebar customTitlebar1;
        private System.Windows.Forms.PictureBox pbxUser;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label lblMobileNumber;
        private System.Windows.Forms.Label lblFatherName;
        private Agent.CustomControls.CustomHeaderControl customHeaderControl1;
        private Agent.CustomControls.CustomHeaderControl customHeaderControl2;
        private Agent.CustomControls.CustomHeaderControl customHeaderControl3;
    }
}