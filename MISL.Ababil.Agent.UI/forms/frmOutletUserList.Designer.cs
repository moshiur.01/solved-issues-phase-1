﻿using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.UI.forms
{
    partial class frmOutletUserList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblOutletCodeHeader = new System.Windows.Forms.Label();
            this.lblOutletCodeValue = new System.Windows.Forms.Label();
            this.lblOutletNameValue = new System.Windows.Forms.Label();
            this.lblOutletNameHeader = new System.Windows.Forms.Label();
            this.panelUserList = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.customTitlebar1 = new CustomTitlebar();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(11, 32);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1021, 49);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Outlet Information";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Gainsboro;
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.33333F));
            this.tableLayoutPanel1.Controls.Add(this.lblOutletCodeHeader, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblOutletCodeValue, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblOutletNameValue, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblOutletNameHeader, 2, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(8, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1006, 28);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // lblOutletCodeHeader
            // 
            this.lblOutletCodeHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblOutletCodeHeader.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.lblOutletCodeHeader.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblOutletCodeHeader.Location = new System.Drawing.Point(3, 0);
            this.lblOutletCodeHeader.Name = "lblOutletCodeHeader";
            this.lblOutletCodeHeader.Size = new System.Drawing.Size(114, 28);
            this.lblOutletCodeHeader.TabIndex = 2;
            this.lblOutletCodeHeader.Text = "Outlet Code :";
            this.lblOutletCodeHeader.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblOutletCodeValue
            // 
            this.lblOutletCodeValue.AutoSize = true;
            this.lblOutletCodeValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblOutletCodeValue.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOutletCodeValue.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblOutletCodeValue.Location = new System.Drawing.Point(123, 0);
            this.lblOutletCodeValue.Name = "lblOutletCodeValue";
            this.lblOutletCodeValue.Size = new System.Drawing.Size(208, 28);
            this.lblOutletCodeValue.TabIndex = 2;
            this.lblOutletCodeValue.Text = "0000000000";
            this.lblOutletCodeValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblOutletNameValue
            // 
            this.lblOutletNameValue.AutoSize = true;
            this.lblOutletNameValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblOutletNameValue.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOutletNameValue.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblOutletNameValue.Location = new System.Drawing.Point(457, 0);
            this.lblOutletNameValue.Name = "lblOutletNameValue";
            this.lblOutletNameValue.Size = new System.Drawing.Size(208, 28);
            this.lblOutletNameValue.TabIndex = 2;
            this.lblOutletNameValue.Text = "Outlet Name Sample";
            this.lblOutletNameValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblOutletNameHeader
            // 
            this.lblOutletNameHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblOutletNameHeader.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.lblOutletNameHeader.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblOutletNameHeader.Location = new System.Drawing.Point(337, 0);
            this.lblOutletNameHeader.Name = "lblOutletNameHeader";
            this.lblOutletNameHeader.Size = new System.Drawing.Size(114, 28);
            this.lblOutletNameHeader.TabIndex = 2;
            this.lblOutletNameHeader.Text = "Outlet Name :";
            this.lblOutletNameHeader.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panelUserList
            // 
            this.panelUserList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelUserList.AutoScroll = true;
            this.panelUserList.Location = new System.Drawing.Point(19, 94);
            this.panelUserList.Name = "panelUserList";
            this.panelUserList.Size = new System.Drawing.Size(1005, 427);
            this.panelUserList.TabIndex = 9;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(949, 532);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 24);
            this.btnClose.TabIndex = 10;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(1043, 26);
            this.customTitlebar1.TabIndex = 11;
            // 
            // frmOutletUserList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1043, 571);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.panelUserList);
            this.Controls.Add(this.groupBox1);
            this.DisplayHeader = false;
            this.Name = "frmOutletUserList";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.Resizable = false;
            this.ShowInTaskbar = false;
            this.Text = "Outlet User Limit Setting";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmOutletUserList_FormClosing);
            this.Leave += new System.EventHandler(this.frmOutletUserList_Leave);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblOutletCodeHeader;
        private System.Windows.Forms.Label lblOutletCodeValue;
        private System.Windows.Forms.Label lblOutletNameValue;
        private System.Windows.Forms.Label lblOutletNameHeader;
        private System.Windows.Forms.Panel panelUserList;
        private System.Windows.Forms.Button btnClose;
        private CustomTitlebar customTitlebar1;
    }
}