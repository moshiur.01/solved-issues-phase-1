﻿namespace MISL.Ababil.Agent.UI.forms
{
    partial class frmOutletCashInfoAll
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbSubAgnetName = new MetroFramework.Controls.MetroComboBox();
            this.cmbAgentName = new MetroFramework.Controls.MetroComboBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnShow = new System.Windows.Forms.Button();
            this.lblFromDate = new System.Windows.Forms.Label();
            this.lblSubAgent = new System.Windows.Forms.Label();
            this.lblAgent = new System.Windows.Forms.Label();
            this.dtpFromDate = new MISL.Ababil.Agent.UI.forms.CustomControls.CustomDateTimePicker();
            this.flowPanelHeader = new System.Windows.Forms.FlowLayoutPanel();
            this.lblUser = new System.Windows.Forms.Label();
            this.flowPanelUser = new System.Windows.Forms.FlowLayoutPanel();
            this.cmbUser = new MetroFramework.Controls.MetroComboBox();
            this.mandatoryMark1 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark2 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark3 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark4 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.flowPanelHeader.SuspendLayout();
            this.flowPanelUser.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbSubAgnetName
            // 
            this.cmbSubAgnetName.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cmbSubAgnetName.FormattingEnabled = true;
            this.cmbSubAgnetName.ItemHeight = 19;
            this.cmbSubAgnetName.Location = new System.Drawing.Point(3, 40);
            this.cmbSubAgnetName.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.cmbSubAgnetName.Name = "cmbSubAgnetName";
            this.cmbSubAgnetName.Size = new System.Drawing.Size(235, 25);
            this.cmbSubAgnetName.TabIndex = 19;
            this.cmbSubAgnetName.UseSelectable = true;
            this.cmbSubAgnetName.SelectedIndexChanged += new System.EventHandler(this.cmbSubAgnetName_SelectedIndexChanged);
            // 
            // cmbAgentName
            // 
            this.cmbAgentName.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cmbAgentName.FormattingEnabled = true;
            this.cmbAgentName.ItemHeight = 19;
            this.cmbAgentName.Location = new System.Drawing.Point(3, 6);
            this.cmbAgentName.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.cmbAgentName.Name = "cmbAgentName";
            this.cmbAgentName.Size = new System.Drawing.Size(235, 25);
            this.cmbAgentName.TabIndex = 17;
            this.cmbAgentName.UseSelectable = true;
            this.cmbAgentName.SelectedIndexChanged += new System.EventHandler(this.cmbAgentName_SelectedIndexChanged);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(471, 287);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(110, 31);
            this.btnClose.TabIndex = 23;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnShow
            // 
            this.btnShow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnShow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShow.ForeColor = System.Drawing.Color.White;
            this.btnShow.Location = new System.Drawing.Point(350, 287);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(110, 31);
            this.btnShow.TabIndex = 22;
            this.btnShow.Text = "Show";
            this.btnShow.UseVisualStyleBackColor = false;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // lblFromDate
            // 
            this.lblFromDate.AutoSize = true;
            this.lblFromDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lblFromDate.Location = new System.Drawing.Point(3, 112);
            this.lblFromDate.Margin = new System.Windows.Forms.Padding(3, 13, 3, 3);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(46, 17);
            this.lblFromDate.TabIndex = 20;
            this.lblFromDate.Text = "Date :";
            // 
            // lblSubAgent
            // 
            this.lblSubAgent.AutoSize = true;
            this.lblSubAgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lblSubAgent.Location = new System.Drawing.Point(3, 46);
            this.lblSubAgent.Margin = new System.Windows.Forms.Padding(3, 13, 3, 3);
            this.lblSubAgent.Name = "lblSubAgent";
            this.lblSubAgent.Size = new System.Drawing.Size(54, 17);
            this.lblSubAgent.TabIndex = 18;
            this.lblSubAgent.Text = "Outlet :";
            // 
            // lblAgent
            // 
            this.lblAgent.AutoSize = true;
            this.lblAgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lblAgent.Location = new System.Drawing.Point(3, 13);
            this.lblAgent.Margin = new System.Windows.Forms.Padding(3, 13, 3, 3);
            this.lblAgent.Name = "lblAgent";
            this.lblAgent.Size = new System.Drawing.Size(53, 17);
            this.lblAgent.TabIndex = 16;
            this.lblAgent.Text = "Agent :";
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.Date = "17/09/2015";
            this.dtpFromDate.Location = new System.Drawing.Point(3, 108);
            this.dtpFromDate.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.dtpFromDate.MaximumSize = new System.Drawing.Size(400, 25);
            this.dtpFromDate.MinimumSize = new System.Drawing.Size(60, 25);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.PresetServerDate = true;
            this.dtpFromDate.Size = new System.Drawing.Size(235, 25);
            this.dtpFromDate.TabIndex = 21;
            this.dtpFromDate.Value = new System.DateTime(2015, 9, 17, 14, 24, 55, 74);
            // 
            // flowPanelHeader
            // 
            this.flowPanelHeader.AutoScrollMargin = new System.Drawing.Size(0, 10);
            this.flowPanelHeader.Controls.Add(this.lblAgent);
            this.flowPanelHeader.Controls.Add(this.lblSubAgent);
            this.flowPanelHeader.Controls.Add(this.lblUser);
            this.flowPanelHeader.Controls.Add(this.lblFromDate);
            this.flowPanelHeader.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowPanelHeader.Location = new System.Drawing.Point(131, 92);
            this.flowPanelHeader.Name = "flowPanelHeader";
            this.flowPanelHeader.Size = new System.Drawing.Size(74, 147);
            this.flowPanelHeader.TabIndex = 24;
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lblUser.Location = new System.Drawing.Point(3, 79);
            this.lblUser.Margin = new System.Windows.Forms.Padding(3, 13, 3, 3);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(46, 17);
            this.lblUser.TabIndex = 26;
            this.lblUser.Text = "User :";
            // 
            // flowPanelUser
            // 
            this.flowPanelUser.Controls.Add(this.cmbAgentName);
            this.flowPanelUser.Controls.Add(this.cmbSubAgnetName);
            this.flowPanelUser.Controls.Add(this.cmbUser);
            this.flowPanelUser.Controls.Add(this.dtpFromDate);
            this.flowPanelUser.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowPanelUser.Location = new System.Drawing.Point(205, 92);
            this.flowPanelUser.Name = "flowPanelUser";
            this.flowPanelUser.Size = new System.Drawing.Size(245, 147);
            this.flowPanelUser.TabIndex = 25;
            // 
            // cmbUser
            // 
            this.cmbUser.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cmbUser.FormattingEnabled = true;
            this.cmbUser.ItemHeight = 19;
            this.cmbUser.Location = new System.Drawing.Point(3, 74);
            this.cmbUser.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.cmbUser.Name = "cmbUser";
            this.cmbUser.Size = new System.Drawing.Size(235, 25);
            this.cmbUser.TabIndex = 20;
            this.cmbUser.UseSelectable = true;
            // 
            // mandatoryMark1
            // 
            this.mandatoryMark1.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark1.Location = new System.Drawing.Point(454, 97);
            this.mandatoryMark1.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.Name = "mandatoryMark1";
            this.mandatoryMark1.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.TabIndex = 26;
            // 
            // mandatoryMark2
            // 
            this.mandatoryMark2.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark2.Location = new System.Drawing.Point(454, 131);
            this.mandatoryMark2.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.Name = "mandatoryMark2";
            this.mandatoryMark2.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.TabIndex = 27;
            // 
            // mandatoryMark3
            // 
            this.mandatoryMark3.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark3.Location = new System.Drawing.Point(454, 159);
            this.mandatoryMark3.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark3.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark3.Name = "mandatoryMark3";
            this.mandatoryMark3.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark3.TabIndex = 28;
            // 
            // mandatoryMark4
            // 
            this.mandatoryMark4.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark4.Location = new System.Drawing.Point(454, 200);
            this.mandatoryMark4.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark4.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark4.Name = "mandatoryMark4";
            this.mandatoryMark4.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark4.TabIndex = 29;
            // 
            // frmOutletCashInfoAll
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 341);
            this.Controls.Add(this.mandatoryMark4);
            this.Controls.Add(this.mandatoryMark3);
            this.Controls.Add(this.mandatoryMark2);
            this.Controls.Add(this.mandatoryMark1);
            this.Controls.Add(this.flowPanelUser);
            this.Controls.Add(this.flowPanelHeader);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnShow);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmOutletCashInfoAll";
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.DropShadow;
            this.Text = "Cash Information";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmOutletCashInfoAll_FormClosing);
            this.Load += new System.EventHandler(this.frmOutletCashInfoAll_Load);
            this.flowPanelHeader.ResumeLayout(false);
            this.flowPanelHeader.PerformLayout();
            this.flowPanelUser.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private CustomControls.CustomDateTimePicker dtpFromDate;
        private MetroFramework.Controls.MetroComboBox cmbSubAgnetName;
        private MetroFramework.Controls.MetroComboBox cmbAgentName;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.Label lblFromDate;
        private System.Windows.Forms.Label lblSubAgent;
        private System.Windows.Forms.Label lblAgent;
        private System.Windows.Forms.FlowLayoutPanel flowPanelHeader;
        private System.Windows.Forms.FlowLayoutPanel flowPanelUser;
        private System.Windows.Forms.Label lblUser;
        private MetroFramework.Controls.MetroComboBox cmbUser;
        private CustomControls.MandatoryMark mandatoryMark1;
        private CustomControls.MandatoryMark mandatoryMark2;
        private CustomControls.MandatoryMark mandatoryMark3;
        private CustomControls.MandatoryMark mandatoryMark4;
    }
}