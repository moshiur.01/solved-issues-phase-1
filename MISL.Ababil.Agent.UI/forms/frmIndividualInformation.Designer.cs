﻿namespace MISL.Ababil.Agent.UI.forms
{
    partial class frmIndividualInformation
 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmIndividualInformation));
            this.grbIndividualInfo = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnDocument = new System.Windows.Forms.Button();
            this.grbPermanentAddress = new System.Windows.Forms.GroupBox();
            this.mandatoryMark17 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark18 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.cmbPermaAddressCopy = new System.Windows.Forms.ComboBox();
            this.mandatoryMark19 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.cmbPermanentDivision = new System.Windows.Forms.ComboBox();
            this.mandatoryMark20 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.label26 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.chkUseAsPermanent = new System.Windows.Forms.CheckBox();
            this.cmbPermanentCountryCode = new System.Windows.Forms.ComboBox();
            this.cmbPermanentPostalCode = new System.Windows.Forms.ComboBox();
            this.cmbPermanentDistrict = new System.Windows.Forms.ComboBox();
            this.cmbPermanentThana = new System.Windows.Forms.ComboBox();
            this.txtPermanentAddress2 = new System.Windows.Forms.TextBox();
            this.txtPermanentAddress1 = new System.Windows.Forms.TextBox();
            this.grbBusinessAddress = new System.Windows.Forms.GroupBox();
            this.mandatoryMark12 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark8 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark9 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.cmbContactAddressCopy = new System.Windows.Forms.ComboBox();
            this.mandatoryMark10 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.cmbContactDivision = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.cmbContactCountryCode = new System.Windows.Forms.ComboBox();
            this.cmbContactPostCode = new System.Windows.Forms.ComboBox();
            this.cmbContactDistrict = new System.Windows.Forms.ComboBox();
            this.cmbContactThana = new System.Windows.Forms.ComboBox();
            this.txtContactAddress2 = new System.Windows.Forms.TextBox();
            this.txtContactAddress1 = new System.Windows.Forms.TextBox();
            this.grbPersonalData = new System.Windows.Forms.GroupBox();
            this.mandatoryMark13 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark2 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark14 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark15 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark7 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark16 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark6 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark5 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark4 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark3 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark1 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.label31 = new System.Windows.Forms.Label();
            this.mtbDOB = new System.Windows.Forms.MaskedTextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.cmbGender = new System.Windows.Forms.ComboBox();
            this.cmbBirthPlace = new System.Windows.Forms.ComboBox();
            this.cmbOccupation = new System.Windows.Forms.ComboBox();
            this.cmbBirthCountry = new System.Windows.Forms.ComboBox();
            this.dtpDOB = new System.Windows.Forms.DateTimePicker();
            this.cmbMaritalStatus = new System.Windows.Forms.ComboBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtFax = new System.Windows.Forms.TextBox();
            this.txtMobileNo = new System.Windows.Forms.TextBox();
            this.txtTelephoneNo = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtSpouseName = new System.Windows.Forms.TextBox();
            this.txtMotherLastName = new System.Windows.Forms.TextBox();
            this.txtMothersFirstName = new System.Windows.Forms.TextBox();
            this.txtFatherLastName = new System.Windows.Forms.TextBox();
            this.txtFathersFirstName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.grbIndividualInfo.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.grbPermanentAddress.SuspendLayout();
            this.grbBusinessAddress.SuspendLayout();
            this.grbPersonalData.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbIndividualInfo
            // 
            this.grbIndividualInfo.Controls.Add(this.flowLayoutPanel1);
            this.grbIndividualInfo.Controls.Add(this.grbPermanentAddress);
            this.grbIndividualInfo.Controls.Add(this.grbBusinessAddress);
            this.grbIndividualInfo.Controls.Add(this.grbPersonalData);
            this.grbIndividualInfo.Location = new System.Drawing.Point(12, 35);
            this.grbIndividualInfo.Name = "grbIndividualInfo";
            this.grbIndividualInfo.Size = new System.Drawing.Size(1027, 548);
            this.grbIndividualInfo.TabIndex = 0;
            this.grbIndividualInfo.TabStop = false;
            this.grbIndividualInfo.Text = "Individual Info";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnClose);
            this.flowLayoutPanel1.Controls.Add(this.btnClear);
            this.flowLayoutPanel1.Controls.Add(this.btnSave);
            this.flowLayoutPanel1.Controls.Add(this.btnDocument);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(595, 514);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(369, 33);
            this.flowLayoutPanel1.TabIndex = 7;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(291, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 28);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(210, 3);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 28);
            this.btnClear.TabIndex = 4;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(110, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(94, 27);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDocument
            // 
            this.btnDocument.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnDocument.FlatAppearance.BorderSize = 0;
            this.btnDocument.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDocument.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDocument.ForeColor = System.Drawing.Color.White;
            this.btnDocument.Location = new System.Drawing.Point(9, 3);
            this.btnDocument.Name = "btnDocument";
            this.btnDocument.Size = new System.Drawing.Size(95, 28);
            this.btnDocument.TabIndex = 5;
            this.btnDocument.Text = "Document";
            this.btnDocument.UseVisualStyleBackColor = false;
            this.btnDocument.Click += new System.EventHandler(this.btnDocument_Click);
            // 
            // grbPermanentAddress
            // 
            this.grbPermanentAddress.Controls.Add(this.mandatoryMark17);
            this.grbPermanentAddress.Controls.Add(this.mandatoryMark18);
            this.grbPermanentAddress.Controls.Add(this.cmbPermaAddressCopy);
            this.grbPermanentAddress.Controls.Add(this.mandatoryMark19);
            this.grbPermanentAddress.Controls.Add(this.cmbPermanentDivision);
            this.grbPermanentAddress.Controls.Add(this.mandatoryMark20);
            this.grbPermanentAddress.Controls.Add(this.label26);
            this.grbPermanentAddress.Controls.Add(this.label19);
            this.grbPermanentAddress.Controls.Add(this.label20);
            this.grbPermanentAddress.Controls.Add(this.label22);
            this.grbPermanentAddress.Controls.Add(this.label23);
            this.grbPermanentAddress.Controls.Add(this.label30);
            this.grbPermanentAddress.Controls.Add(this.label24);
            this.grbPermanentAddress.Controls.Add(this.chkUseAsPermanent);
            this.grbPermanentAddress.Controls.Add(this.cmbPermanentCountryCode);
            this.grbPermanentAddress.Controls.Add(this.cmbPermanentPostalCode);
            this.grbPermanentAddress.Controls.Add(this.cmbPermanentDistrict);
            this.grbPermanentAddress.Controls.Add(this.cmbPermanentThana);
            this.grbPermanentAddress.Controls.Add(this.txtPermanentAddress2);
            this.grbPermanentAddress.Controls.Add(this.txtPermanentAddress1);
            this.grbPermanentAddress.Location = new System.Drawing.Point(510, 250);
            this.grbPermanentAddress.Name = "grbPermanentAddress";
            this.grbPermanentAddress.Size = new System.Drawing.Size(492, 258);
            this.grbPermanentAddress.TabIndex = 2;
            this.grbPermanentAddress.TabStop = false;
            this.grbPermanentAddress.Text = "Permanent Address";
            // 
            // mandatoryMark17
            // 
            this.mandatoryMark17.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark17.Location = new System.Drawing.Point(455, 174);
            this.mandatoryMark17.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark17.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark17.Name = "mandatoryMark17";
            this.mandatoryMark17.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark17.TabIndex = 22;
            // 
            // mandatoryMark18
            // 
            this.mandatoryMark18.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark18.Location = new System.Drawing.Point(455, 150);
            this.mandatoryMark18.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark18.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark18.Name = "mandatoryMark18";
            this.mandatoryMark18.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark18.TabIndex = 21;
            // 
            // cmbPermaAddressCopy
            // 
            this.cmbPermaAddressCopy.Enabled = false;
            this.cmbPermaAddressCopy.FormattingEnabled = true;
            this.cmbPermaAddressCopy.Location = new System.Drawing.Point(139, 16);
            this.cmbPermaAddressCopy.Name = "cmbPermaAddressCopy";
            this.cmbPermaAddressCopy.Size = new System.Drawing.Size(315, 21);
            this.cmbPermaAddressCopy.TabIndex = 1;
            this.cmbPermaAddressCopy.Visible = false;
            this.cmbPermaAddressCopy.SelectedIndexChanged += new System.EventHandler(this.cmbPermaAddressCopy_SelectedIndexChanged);
            // 
            // mandatoryMark19
            // 
            this.mandatoryMark19.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark19.Location = new System.Drawing.Point(455, 122);
            this.mandatoryMark19.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark19.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark19.Name = "mandatoryMark19";
            this.mandatoryMark19.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark19.TabIndex = 20;
            // 
            // cmbPermanentDivision
            // 
            this.cmbPermanentDivision.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPermanentDivision.FormattingEnabled = true;
            this.cmbPermanentDivision.Location = new System.Drawing.Point(139, 123);
            this.cmbPermanentDivision.Name = "cmbPermanentDivision";
            this.cmbPermanentDivision.Size = new System.Drawing.Size(315, 21);
            this.cmbPermanentDivision.TabIndex = 7;
            this.cmbPermanentDivision.SelectedIndexChanged += new System.EventHandler(this.cmbPermanentDivision_SelectedIndexChanged);
            // 
            // mandatoryMark20
            // 
            this.mandatoryMark20.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark20.Location = new System.Drawing.Point(455, 62);
            this.mandatoryMark20.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark20.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark20.Name = "mandatoryMark20";
            this.mandatoryMark20.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark20.TabIndex = 19;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(86, 126);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(50, 13);
            this.label26.TabIndex = 6;
            this.label26.Text = "Division :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(59, 235);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 13);
            this.label19.TabIndex = 14;
            this.label19.Text = "Country Code :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(66, 208);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(70, 13);
            this.label20.TabIndex = 12;
            this.label20.Text = "Postal Code :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(92, 181);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(44, 13);
            this.label22.TabIndex = 10;
            this.label22.Text = "Thana :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(91, 155);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(45, 13);
            this.label23.TabIndex = 8;
            this.label23.Text = "District :";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Enabled = false;
            this.label30.Location = new System.Drawing.Point(29, 19);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(104, 13);
            this.label30.TabIndex = 0;
            this.label30.Text = "Copy Address From :";
            this.label30.Visible = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(85, 65);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(51, 13);
            this.label24.TabIndex = 4;
            this.label24.Text = "Address :";
            // 
            // chkUseAsPermanent
            // 
            this.chkUseAsPermanent.AutoSize = true;
            this.chkUseAsPermanent.Location = new System.Drawing.Point(139, 43);
            this.chkUseAsPermanent.Name = "chkUseAsPermanent";
            this.chkUseAsPermanent.Size = new System.Drawing.Size(131, 17);
            this.chkUseAsPermanent.TabIndex = 2;
            this.chkUseAsPermanent.Text = "Copy Contact Address";
            this.chkUseAsPermanent.UseVisualStyleBackColor = true;
            this.chkUseAsPermanent.CheckedChanged += new System.EventHandler(this.chkUseAsPermanent_CheckedChanged);
            // 
            // cmbPermanentCountryCode
            // 
            this.cmbPermanentCountryCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPermanentCountryCode.Enabled = false;
            this.cmbPermanentCountryCode.FormattingEnabled = true;
            this.cmbPermanentCountryCode.Location = new System.Drawing.Point(139, 231);
            this.cmbPermanentCountryCode.Name = "cmbPermanentCountryCode";
            this.cmbPermanentCountryCode.Size = new System.Drawing.Size(315, 21);
            this.cmbPermanentCountryCode.TabIndex = 15;
            // 
            // cmbPermanentPostalCode
            // 
            this.cmbPermanentPostalCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPermanentPostalCode.FormattingEnabled = true;
            this.cmbPermanentPostalCode.Location = new System.Drawing.Point(139, 204);
            this.cmbPermanentPostalCode.Name = "cmbPermanentPostalCode";
            this.cmbPermanentPostalCode.Size = new System.Drawing.Size(315, 21);
            this.cmbPermanentPostalCode.TabIndex = 13;
            // 
            // cmbPermanentDistrict
            // 
            this.cmbPermanentDistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPermanentDistrict.FormattingEnabled = true;
            this.cmbPermanentDistrict.Location = new System.Drawing.Point(139, 150);
            this.cmbPermanentDistrict.Name = "cmbPermanentDistrict";
            this.cmbPermanentDistrict.Size = new System.Drawing.Size(315, 21);
            this.cmbPermanentDistrict.TabIndex = 9;
            this.cmbPermanentDistrict.SelectedIndexChanged += new System.EventHandler(this.cmbPermanentDistrict_SelectedIndexChanged);
            // 
            // cmbPermanentThana
            // 
            this.cmbPermanentThana.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPermanentThana.FormattingEnabled = true;
            this.cmbPermanentThana.Location = new System.Drawing.Point(139, 177);
            this.cmbPermanentThana.Name = "cmbPermanentThana";
            this.cmbPermanentThana.Size = new System.Drawing.Size(315, 21);
            this.cmbPermanentThana.TabIndex = 11;
            // 
            // txtPermanentAddress2
            // 
            this.txtPermanentAddress2.Location = new System.Drawing.Point(139, 93);
            this.txtPermanentAddress2.MaxLength = 100;
            this.txtPermanentAddress2.Multiline = true;
            this.txtPermanentAddress2.Name = "txtPermanentAddress2";
            this.txtPermanentAddress2.Size = new System.Drawing.Size(315, 22);
            this.txtPermanentAddress2.TabIndex = 5;
            // 
            // txtPermanentAddress1
            // 
            this.txtPermanentAddress1.Location = new System.Drawing.Point(139, 61);
            this.txtPermanentAddress1.MaxLength = 100;
            this.txtPermanentAddress1.Multiline = true;
            this.txtPermanentAddress1.Name = "txtPermanentAddress1";
            this.txtPermanentAddress1.Size = new System.Drawing.Size(315, 22);
            this.txtPermanentAddress1.TabIndex = 3;
            // 
            // grbBusinessAddress
            // 
            this.grbBusinessAddress.Controls.Add(this.mandatoryMark12);
            this.grbBusinessAddress.Controls.Add(this.mandatoryMark8);
            this.grbBusinessAddress.Controls.Add(this.mandatoryMark9);
            this.grbBusinessAddress.Controls.Add(this.cmbContactAddressCopy);
            this.grbBusinessAddress.Controls.Add(this.mandatoryMark10);
            this.grbBusinessAddress.Controls.Add(this.cmbContactDivision);
            this.grbBusinessAddress.Controls.Add(this.label25);
            this.grbBusinessAddress.Controls.Add(this.label15);
            this.grbBusinessAddress.Controls.Add(this.label16);
            this.grbBusinessAddress.Controls.Add(this.label17);
            this.grbBusinessAddress.Controls.Add(this.label18);
            this.grbBusinessAddress.Controls.Add(this.label29);
            this.grbBusinessAddress.Controls.Add(this.label21);
            this.grbBusinessAddress.Controls.Add(this.cmbContactCountryCode);
            this.grbBusinessAddress.Controls.Add(this.cmbContactPostCode);
            this.grbBusinessAddress.Controls.Add(this.cmbContactDistrict);
            this.grbBusinessAddress.Controls.Add(this.cmbContactThana);
            this.grbBusinessAddress.Controls.Add(this.txtContactAddress2);
            this.grbBusinessAddress.Controls.Add(this.txtContactAddress1);
            this.grbBusinessAddress.Location = new System.Drawing.Point(24, 250);
            this.grbBusinessAddress.Name = "grbBusinessAddress";
            this.grbBusinessAddress.Size = new System.Drawing.Size(467, 258);
            this.grbBusinessAddress.TabIndex = 1;
            this.grbBusinessAddress.TabStop = false;
            this.grbBusinessAddress.Text = "Contact Address";
            // 
            // mandatoryMark12
            // 
            this.mandatoryMark12.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark12.Location = new System.Drawing.Point(439, 172);
            this.mandatoryMark12.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark12.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark12.Name = "mandatoryMark12";
            this.mandatoryMark12.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark12.TabIndex = 48;
            // 
            // mandatoryMark8
            // 
            this.mandatoryMark8.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark8.Location = new System.Drawing.Point(439, 142);
            this.mandatoryMark8.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark8.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark8.Name = "mandatoryMark8";
            this.mandatoryMark8.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark8.TabIndex = 47;
            // 
            // mandatoryMark9
            // 
            this.mandatoryMark9.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark9.Location = new System.Drawing.Point(439, 116);
            this.mandatoryMark9.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark9.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark9.Name = "mandatoryMark9";
            this.mandatoryMark9.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark9.TabIndex = 46;
            // 
            // cmbContactAddressCopy
            // 
            this.cmbContactAddressCopy.Enabled = false;
            this.cmbContactAddressCopy.FormattingEnabled = true;
            this.cmbContactAddressCopy.Location = new System.Drawing.Point(138, 16);
            this.cmbContactAddressCopy.Name = "cmbContactAddressCopy";
            this.cmbContactAddressCopy.Size = new System.Drawing.Size(299, 21);
            this.cmbContactAddressCopy.TabIndex = 1;
            this.cmbContactAddressCopy.Visible = false;
            this.cmbContactAddressCopy.SelectedIndexChanged += new System.EventHandler(this.cmbContactAddressCopy_SelectedIndexChanged);
            // 
            // mandatoryMark10
            // 
            this.mandatoryMark10.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark10.Location = new System.Drawing.Point(439, 62);
            this.mandatoryMark10.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark10.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark10.Name = "mandatoryMark10";
            this.mandatoryMark10.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark10.TabIndex = 45;
            // 
            // cmbContactDivision
            // 
            this.cmbContactDivision.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbContactDivision.FormattingEnabled = true;
            this.cmbContactDivision.Location = new System.Drawing.Point(135, 115);
            this.cmbContactDivision.Name = "cmbContactDivision";
            this.cmbContactDivision.Size = new System.Drawing.Size(302, 21);
            this.cmbContactDivision.TabIndex = 6;
            this.cmbContactDivision.SelectedIndexChanged += new System.EventHandler(this.cmbContactDivision_SelectedIndexChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(82, 118);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(50, 13);
            this.label25.TabIndex = 5;
            this.label25.Text = "Division :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(55, 226);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 13);
            this.label15.TabIndex = 13;
            this.label15.Text = "Country Code :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(62, 199);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(70, 13);
            this.label16.TabIndex = 11;
            this.label16.Text = "Postal Code :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(88, 172);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(44, 13);
            this.label17.TabIndex = 9;
            this.label17.Text = "Thana :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(87, 146);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(45, 13);
            this.label18.TabIndex = 7;
            this.label18.Text = "District :";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Enabled = false;
            this.label29.Location = new System.Drawing.Point(28, 19);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(104, 13);
            this.label29.TabIndex = 0;
            this.label29.Text = "Copy Address From :";
            this.label29.Visible = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(81, 64);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(51, 13);
            this.label21.TabIndex = 2;
            this.label21.Text = "Address :";
            // 
            // cmbContactCountryCode
            // 
            this.cmbContactCountryCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbContactCountryCode.Enabled = false;
            this.cmbContactCountryCode.FormattingEnabled = true;
            this.cmbContactCountryCode.Location = new System.Drawing.Point(135, 223);
            this.cmbContactCountryCode.Name = "cmbContactCountryCode";
            this.cmbContactCountryCode.Size = new System.Drawing.Size(302, 21);
            this.cmbContactCountryCode.TabIndex = 14;
            // 
            // cmbContactPostCode
            // 
            this.cmbContactPostCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbContactPostCode.FormattingEnabled = true;
            this.cmbContactPostCode.Location = new System.Drawing.Point(135, 196);
            this.cmbContactPostCode.Name = "cmbContactPostCode";
            this.cmbContactPostCode.Size = new System.Drawing.Size(302, 21);
            this.cmbContactPostCode.TabIndex = 12;
            // 
            // cmbContactDistrict
            // 
            this.cmbContactDistrict.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbContactDistrict.FormattingEnabled = true;
            this.cmbContactDistrict.Location = new System.Drawing.Point(135, 142);
            this.cmbContactDistrict.Name = "cmbContactDistrict";
            this.cmbContactDistrict.Size = new System.Drawing.Size(302, 21);
            this.cmbContactDistrict.TabIndex = 8;
            this.cmbContactDistrict.SelectedIndexChanged += new System.EventHandler(this.cmbContactDistrict_SelectedIndexChanged);
            // 
            // cmbContactThana
            // 
            this.cmbContactThana.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbContactThana.FormattingEnabled = true;
            this.cmbContactThana.Location = new System.Drawing.Point(135, 169);
            this.cmbContactThana.Name = "cmbContactThana";
            this.cmbContactThana.Size = new System.Drawing.Size(302, 21);
            this.cmbContactThana.TabIndex = 10;
            this.cmbContactThana.SelectedIndexChanged += new System.EventHandler(this.cmbContactThana_SelectedIndexChanged);
            // 
            // txtContactAddress2
            // 
            this.txtContactAddress2.Location = new System.Drawing.Point(135, 87);
            this.txtContactAddress2.MaxLength = 100;
            this.txtContactAddress2.Multiline = true;
            this.txtContactAddress2.Name = "txtContactAddress2";
            this.txtContactAddress2.Size = new System.Drawing.Size(302, 21);
            this.txtContactAddress2.TabIndex = 4;
            // 
            // txtContactAddress1
            // 
            this.txtContactAddress1.Location = new System.Drawing.Point(135, 61);
            this.txtContactAddress1.MaxLength = 100;
            this.txtContactAddress1.Multiline = true;
            this.txtContactAddress1.Name = "txtContactAddress1";
            this.txtContactAddress1.Size = new System.Drawing.Size(302, 21);
            this.txtContactAddress1.TabIndex = 3;
            // 
            // grbPersonalData
            // 
            this.grbPersonalData.Controls.Add(this.mandatoryMark13);
            this.grbPersonalData.Controls.Add(this.mandatoryMark2);
            this.grbPersonalData.Controls.Add(this.mandatoryMark14);
            this.grbPersonalData.Controls.Add(this.mandatoryMark15);
            this.grbPersonalData.Controls.Add(this.mandatoryMark7);
            this.grbPersonalData.Controls.Add(this.mandatoryMark16);
            this.grbPersonalData.Controls.Add(this.mandatoryMark6);
            this.grbPersonalData.Controls.Add(this.mandatoryMark5);
            this.grbPersonalData.Controls.Add(this.mandatoryMark4);
            this.grbPersonalData.Controls.Add(this.mandatoryMark3);
            this.grbPersonalData.Controls.Add(this.mandatoryMark1);
            this.grbPersonalData.Controls.Add(this.label31);
            this.grbPersonalData.Controls.Add(this.mtbDOB);
            this.grbPersonalData.Controls.Add(this.label28);
            this.grbPersonalData.Controls.Add(this.label27);
            this.grbPersonalData.Controls.Add(this.cmbGender);
            this.grbPersonalData.Controls.Add(this.cmbBirthPlace);
            this.grbPersonalData.Controls.Add(this.cmbOccupation);
            this.grbPersonalData.Controls.Add(this.cmbBirthCountry);
            this.grbPersonalData.Controls.Add(this.dtpDOB);
            this.grbPersonalData.Controls.Add(this.cmbMaritalStatus);
            this.grbPersonalData.Controls.Add(this.txtEmail);
            this.grbPersonalData.Controls.Add(this.txtFax);
            this.grbPersonalData.Controls.Add(this.txtMobileNo);
            this.grbPersonalData.Controls.Add(this.txtTelephoneNo);
            this.grbPersonalData.Controls.Add(this.label9);
            this.grbPersonalData.Controls.Add(this.label10);
            this.grbPersonalData.Controls.Add(this.label11);
            this.grbPersonalData.Controls.Add(this.label12);
            this.grbPersonalData.Controls.Add(this.label13);
            this.grbPersonalData.Controls.Add(this.label14);
            this.grbPersonalData.Controls.Add(this.txtSpouseName);
            this.grbPersonalData.Controls.Add(this.txtMotherLastName);
            this.grbPersonalData.Controls.Add(this.txtMothersFirstName);
            this.grbPersonalData.Controls.Add(this.txtFatherLastName);
            this.grbPersonalData.Controls.Add(this.txtFathersFirstName);
            this.grbPersonalData.Controls.Add(this.label8);
            this.grbPersonalData.Controls.Add(this.label7);
            this.grbPersonalData.Controls.Add(this.label6);
            this.grbPersonalData.Controls.Add(this.label5);
            this.grbPersonalData.Controls.Add(this.label4);
            this.grbPersonalData.Controls.Add(this.label3);
            this.grbPersonalData.Controls.Add(this.label2);
            this.grbPersonalData.Controls.Add(this.txtLastName);
            this.grbPersonalData.Controls.Add(this.txtFirstName);
            this.grbPersonalData.Controls.Add(this.label1);
            this.grbPersonalData.Location = new System.Drawing.Point(24, 21);
            this.grbPersonalData.Name = "grbPersonalData";
            this.grbPersonalData.Size = new System.Drawing.Size(978, 225);
            this.grbPersonalData.TabIndex = 0;
            this.grbPersonalData.TabStop = false;
            this.grbPersonalData.Text = "Personal Data";
            // 
            // mandatoryMark13
            // 
            this.mandatoryMark13.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark13.Location = new System.Drawing.Point(942, 137);
            this.mandatoryMark13.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark13.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark13.Name = "mandatoryMark13";
            this.mandatoryMark13.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark13.TabIndex = 22;
            // 
            // mandatoryMark2
            // 
            this.mandatoryMark2.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark2.Location = new System.Drawing.Point(440, 161);
            this.mandatoryMark2.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.Name = "mandatoryMark2";
            this.mandatoryMark2.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.TabIndex = 43;
            // 
            // mandatoryMark14
            // 
            this.mandatoryMark14.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark14.Location = new System.Drawing.Point(942, 84);
            this.mandatoryMark14.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark14.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark14.Name = "mandatoryMark14";
            this.mandatoryMark14.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark14.TabIndex = 21;
            // 
            // mandatoryMark15
            // 
            this.mandatoryMark15.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark15.Location = new System.Drawing.Point(942, 56);
            this.mandatoryMark15.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark15.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark15.Name = "mandatoryMark15";
            this.mandatoryMark15.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark15.TabIndex = 20;
            // 
            // mandatoryMark7
            // 
            this.mandatoryMark7.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark7.Location = new System.Drawing.Point(337, 188);
            this.mandatoryMark7.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark7.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark7.Name = "mandatoryMark7";
            this.mandatoryMark7.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark7.TabIndex = 42;
            // 
            // mandatoryMark16
            // 
            this.mandatoryMark16.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark16.Location = new System.Drawing.Point(942, 29);
            this.mandatoryMark16.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark16.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark16.Name = "mandatoryMark16";
            this.mandatoryMark16.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark16.TabIndex = 19;
            // 
            // mandatoryMark6
            // 
            this.mandatoryMark6.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark6.Location = new System.Drawing.Point(440, 137);
            this.mandatoryMark6.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark6.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark6.Name = "mandatoryMark6";
            this.mandatoryMark6.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark6.TabIndex = 39;
            // 
            // mandatoryMark5
            // 
            this.mandatoryMark5.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark5.Location = new System.Drawing.Point(440, 110);
            this.mandatoryMark5.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark5.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark5.Name = "mandatoryMark5";
            this.mandatoryMark5.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark5.TabIndex = 38;
            // 
            // mandatoryMark4
            // 
            this.mandatoryMark4.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark4.Location = new System.Drawing.Point(440, 83);
            this.mandatoryMark4.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark4.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark4.Name = "mandatoryMark4";
            this.mandatoryMark4.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark4.TabIndex = 37;
            // 
            // mandatoryMark3
            // 
            this.mandatoryMark3.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark3.Location = new System.Drawing.Point(440, 56);
            this.mandatoryMark3.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark3.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark3.Name = "mandatoryMark3";
            this.mandatoryMark3.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark3.TabIndex = 36;
            // 
            // mandatoryMark1
            // 
            this.mandatoryMark1.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark1.Location = new System.Drawing.Point(440, 28);
            this.mandatoryMark1.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.Name = "mandatoryMark1";
            this.mandatoryMark1.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.TabIndex = 35;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.ForeColor = System.Drawing.Color.Gray;
            this.label31.Location = new System.Drawing.Point(359, 192);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(63, 13);
            this.label31.TabIndex = 20;
            this.label31.Text = "dd-MM-yyyy";
            // 
            // mtbDOB
            // 
            this.mtbDOB.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this.mtbDOB.Location = new System.Drawing.Point(135, 188);
            this.mtbDOB.Mask = "00-00-0000";
            this.mtbDOB.Name = "mtbDOB";
            this.mtbDOB.Size = new System.Drawing.Size(181, 20);
            this.mtbDOB.TabIndex = 18;
            this.mtbDOB.KeyUp += new System.Windows.Forms.KeyEventHandler(this.mtbDOB_KeyUp);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(337, 12);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(58, 13);
            this.label28.TabIndex = 1;
            this.label28.Text = "Last Name";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(177, 12);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(57, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "First Name";
            // 
            // cmbGender
            // 
            this.cmbGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGender.FormattingEnabled = true;
            this.cmbGender.Location = new System.Drawing.Point(135, 109);
            this.cmbGender.Name = "cmbGender";
            this.cmbGender.Size = new System.Drawing.Size(302, 21);
            this.cmbGender.TabIndex = 12;
            // 
            // cmbBirthPlace
            // 
            this.cmbBirthPlace.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBirthPlace.FormattingEnabled = true;
            this.cmbBirthPlace.Location = new System.Drawing.Point(625, 28);
            this.cmbBirthPlace.Name = "cmbBirthPlace";
            this.cmbBirthPlace.Size = new System.Drawing.Size(315, 21);
            this.cmbBirthPlace.TabIndex = 22;
            // 
            // cmbOccupation
            // 
            this.cmbOccupation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOccupation.FormattingEnabled = true;
            this.cmbOccupation.Location = new System.Drawing.Point(625, 82);
            this.cmbOccupation.Name = "cmbOccupation";
            this.cmbOccupation.Size = new System.Drawing.Size(315, 21);
            this.cmbOccupation.TabIndex = 26;
            // 
            // cmbBirthCountry
            // 
            this.cmbBirthCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBirthCountry.Enabled = false;
            this.cmbBirthCountry.FormattingEnabled = true;
            this.cmbBirthCountry.Location = new System.Drawing.Point(625, 55);
            this.cmbBirthCountry.Name = "cmbBirthCountry";
            this.cmbBirthCountry.Size = new System.Drawing.Size(315, 21);
            this.cmbBirthCountry.TabIndex = 24;
            // 
            // dtpDOB
            // 
            this.dtpDOB.CustomFormat = "dd-MM-yyyy";
            this.dtpDOB.Location = new System.Drawing.Point(315, 188);
            this.dtpDOB.Name = "dtpDOB";
            this.dtpDOB.Size = new System.Drawing.Size(20, 20);
            this.dtpDOB.TabIndex = 19;
            this.dtpDOB.Value = new System.DateTime(2015, 6, 20, 13, 33, 59, 0);
            this.dtpDOB.ValueChanged += new System.EventHandler(this.dtpDOB_ValueChanged);
            // 
            // cmbMaritalStatus
            // 
            this.cmbMaritalStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMaritalStatus.FormattingEnabled = true;
            this.cmbMaritalStatus.Location = new System.Drawing.Point(135, 135);
            this.cmbMaritalStatus.Name = "cmbMaritalStatus";
            this.cmbMaritalStatus.Size = new System.Drawing.Size(302, 21);
            this.cmbMaritalStatus.TabIndex = 14;
            this.cmbMaritalStatus.SelectedIndexChanged += new System.EventHandler(this.cmbMaritalStatus_SelectedIndexChanged);
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(625, 189);
            this.txtEmail.MaxLength = 100;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(315, 20);
            this.txtEmail.TabIndex = 34;
            // 
            // txtFax
            // 
            this.txtFax.Location = new System.Drawing.Point(625, 162);
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(315, 20);
            this.txtFax.TabIndex = 32;
            // 
            // txtMobileNo
            // 
            this.txtMobileNo.Location = new System.Drawing.Point(625, 135);
            this.txtMobileNo.Name = "txtMobileNo";
            this.txtMobileNo.Size = new System.Drawing.Size(315, 20);
            this.txtMobileNo.TabIndex = 30;
            this.txtMobileNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMobileNo_KeyPress);
            // 
            // txtTelephoneNo
            // 
            this.txtTelephoneNo.Location = new System.Drawing.Point(625, 109);
            this.txtTelephoneNo.MaxLength = 30;
            this.txtTelephoneNo.Name = "txtTelephoneNo";
            this.txtTelephoneNo.Size = new System.Drawing.Size(315, 20);
            this.txtTelephoneNo.TabIndex = 28;
            this.txtTelephoneNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTelephoneNo_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(584, 192);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "Email :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(592, 165);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 13);
            this.label10.TabIndex = 31;
            this.label10.Text = "Fax :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(558, 138);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "Mobile No. :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(538, 112);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "Telephone No. :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(554, 85);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 13);
            this.label13.TabIndex = 25;
            this.label13.Text = "Occupation :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(549, 58);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 13);
            this.label14.TabIndex = 23;
            this.label14.Text = "Birth Country :";
            // 
            // txtSpouseName
            // 
            this.txtSpouseName.Location = new System.Drawing.Point(135, 162);
            this.txtSpouseName.MaxLength = 80;
            this.txtSpouseName.Name = "txtSpouseName";
            this.txtSpouseName.Size = new System.Drawing.Size(302, 20);
            this.txtSpouseName.TabIndex = 16;
            // 
            // txtMotherLastName
            // 
            this.txtMotherLastName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMotherLastName.Location = new System.Drawing.Point(289, 82);
            this.txtMotherLastName.MaxLength = 64;
            this.txtMotherLastName.Name = "txtMotherLastName";
            this.txtMotherLastName.Size = new System.Drawing.Size(148, 20);
            this.txtMotherLastName.TabIndex = 10;
            // 
            // txtMothersFirstName
            // 
            this.txtMothersFirstName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMothersFirstName.Location = new System.Drawing.Point(135, 82);
            this.txtMothersFirstName.MaxLength = 15;
            this.txtMothersFirstName.Name = "txtMothersFirstName";
            this.txtMothersFirstName.Size = new System.Drawing.Size(135, 20);
            this.txtMothersFirstName.TabIndex = 9;
            // 
            // txtFatherLastName
            // 
            this.txtFatherLastName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFatherLastName.Location = new System.Drawing.Point(289, 55);
            this.txtFatherLastName.MaxLength = 80;
            this.txtFatherLastName.Name = "txtFatherLastName";
            this.txtFatherLastName.Size = new System.Drawing.Size(148, 20);
            this.txtFatherLastName.TabIndex = 7;
            // 
            // txtFathersFirstName
            // 
            this.txtFathersFirstName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFathersFirstName.Location = new System.Drawing.Point(135, 55);
            this.txtFathersFirstName.MaxLength = 15;
            this.txtFathersFirstName.Name = "txtFathersFirstName";
            this.txtFathersFirstName.Size = new System.Drawing.Size(135, 20);
            this.txtFathersFirstName.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(62, 192);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Date of Birth :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(54, 165);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Spouse Name :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(57, 138);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Marital Status :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(86, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Gender :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(52, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Mothers Name :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(53, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Father\'s Name :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(558, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Birth Place :";
            // 
            // txtLastName
            // 
            this.txtLastName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLastName.Location = new System.Drawing.Point(289, 28);
            this.txtLastName.MaxLength = 80;
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(148, 20);
            this.txtLastName.TabIndex = 4;
            this.txtLastName.TextChanged += new System.EventHandler(this.txtLastName_TextChanged);
            // 
            // txtFirstName
            // 
            this.txtFirstName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtFirstName.Location = new System.Drawing.Point(135, 28);
            this.txtFirstName.MaxLength = 15;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(135, 20);
            this.txtFirstName.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(93, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Name :";
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(1051, 26);
            this.customTitlebar1.TabIndex = 1;
            this.customTitlebar1.TabStop = false;
            // 
            // frmIndividualInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1051, 596);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.grbIndividualInfo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmIndividualInformation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Individual Information";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmIndividualInformation_FormClosing);
            this.grbIndividualInfo.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.grbPermanentAddress.ResumeLayout(false);
            this.grbPermanentAddress.PerformLayout();
            this.grbBusinessAddress.ResumeLayout(false);
            this.grbBusinessAddress.PerformLayout();
            this.grbPersonalData.ResumeLayout(false);
            this.grbPersonalData.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbIndividualInfo;
        private System.Windows.Forms.GroupBox grbPersonalData;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtFax;
        private System.Windows.Forms.TextBox txtMobileNo;
        private System.Windows.Forms.TextBox txtTelephoneNo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtSpouseName;
        private System.Windows.Forms.TextBox txtMothersFirstName;
        private System.Windows.Forms.TextBox txtFathersFirstName;
        private System.Windows.Forms.ComboBox cmbBirthPlace;
        private System.Windows.Forms.ComboBox cmbBirthCountry;
        private System.Windows.Forms.DateTimePicker dtpDOB;
        private System.Windows.Forms.ComboBox cmbMaritalStatus;
        private System.Windows.Forms.GroupBox grbPermanentAddress;
        private System.Windows.Forms.GroupBox grbBusinessAddress;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.CheckBox chkUseAsPermanent;
        private System.Windows.Forms.ComboBox cmbContactCountryCode;
        private System.Windows.Forms.ComboBox cmbContactPostCode;
        private System.Windows.Forms.ComboBox cmbContactDistrict;
        private System.Windows.Forms.ComboBox cmbContactThana;
        private System.Windows.Forms.TextBox txtContactAddress1;
        private System.Windows.Forms.ComboBox cmbPermanentCountryCode;
        private System.Windows.Forms.ComboBox cmbPermanentPostalCode;
        private System.Windows.Forms.ComboBox cmbPermanentDistrict;
        private System.Windows.Forms.ComboBox cmbPermanentThana;
        private System.Windows.Forms.TextBox txtPermanentAddress1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cmbPermanentDivision;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox cmbContactDivision;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox cmbGender;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtMotherLastName;
        private System.Windows.Forms.TextBox txtFatherLastName;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.ComboBox cmbOccupation;
        private System.Windows.Forms.Button btnDocument;
        private System.Windows.Forms.TextBox txtPermanentAddress2;
        private System.Windows.Forms.TextBox txtContactAddress2;
        private System.Windows.Forms.ComboBox cmbPermaAddressCopy;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox cmbContactAddressCopy;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.MaskedTextBox mtbDOB;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private CustomControls.MandatoryMark mandatoryMark2;
        private CustomControls.MandatoryMark mandatoryMark7;
        private CustomControls.MandatoryMark mandatoryMark6;
        private CustomControls.MandatoryMark mandatoryMark5;
        private CustomControls.MandatoryMark mandatoryMark4;
        private CustomControls.MandatoryMark mandatoryMark3;
        private CustomControls.MandatoryMark mandatoryMark1;
        private CustomControls.MandatoryMark mandatoryMark8;
        private CustomControls.MandatoryMark mandatoryMark9;
        private CustomControls.MandatoryMark mandatoryMark10;
        private CustomControls.MandatoryMark mandatoryMark12;
        private CustomControls.MandatoryMark mandatoryMark17;
        private CustomControls.MandatoryMark mandatoryMark18;
        private CustomControls.MandatoryMark mandatoryMark19;
        private CustomControls.MandatoryMark mandatoryMark20;
        private CustomControls.MandatoryMark mandatoryMark13;
        private CustomControls.MandatoryMark mandatoryMark14;
        private CustomControls.MandatoryMark mandatoryMark15;
        private CustomControls.MandatoryMark mandatoryMark16;
        private Agent.CustomControls.CustomTitlebar customTitlebar1;
    }
}