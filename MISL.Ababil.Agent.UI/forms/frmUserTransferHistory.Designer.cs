﻿namespace MISL.Ababil.Agent.UI.forms
{
    partial class frmUserTransferHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvUserList = new System.Windows.Forms.DataGridView();
            this.gridColUserName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridColFullName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridColUserType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridColMobileNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridColUserStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdColShowDetails = new System.Windows.Forms.DataGridViewLinkColumn();
            this.gridColUserId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridColIndividualId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnTransfer = new System.Windows.Forms.DataGridViewLinkColumn();
            this.customDataGridViewHeader1 = new MISL.Ababil.Agent.Module.Common.Exporter.CustomDataGridViewHeader();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.label1 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.customComboBoxDropDownList1 = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.label3 = new System.Windows.Forms.Label();
            this.customTextBox1 = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.customDateTimePicker1 = new MISL.Ababil.Agent.UI.forms.CustomControls.CustomDateTimePicker();
            this.customDateTimePicker2 = new MISL.Ababil.Agent.UI.forms.CustomControls.CustomDateTimePicker();
            this.button2 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserList)).BeginInit();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvUserList
            // 
            this.dgvUserList.AllowUserToAddRows = false;
            this.dgvUserList.AllowUserToDeleteRows = false;
            this.dgvUserList.AllowUserToResizeColumns = false;
            this.dgvUserList.AllowUserToResizeRows = false;
            this.dgvUserList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvUserList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvUserList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvUserList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgvUserList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvUserList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgvUserList.ColumnHeadersHeight = 30;
            this.dgvUserList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gridColUserName,
            this.gridColFullName,
            this.gridColUserType,
            this.gridColMobileNo,
            this.gridColUserStatus,
            this.grdColShowDetails,
            this.gridColUserId,
            this.gridColIndividualId,
            this.columnTransfer});
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.Padding = new System.Windows.Forms.Padding(0, 1, 0, 1);
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(231)))), ((int)(((byte)(254)))));
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvUserList.DefaultCellStyle = dataGridViewCellStyle12;
            this.dgvUserList.EnableHeadersVisualStyles = false;
            this.dgvUserList.Location = new System.Drawing.Point(12, 172);
            this.dgvUserList.Name = "dgvUserList";
            this.dgvUserList.RowHeadersVisible = false;
            this.dgvUserList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvUserList.Size = new System.Drawing.Size(919, 309);
            this.dgvUserList.TabIndex = 13;
            // 
            // gridColUserName
            // 
            this.gridColUserName.HeaderText = "User Name";
            this.gridColUserName.Name = "gridColUserName";
            this.gridColUserName.ReadOnly = true;
            this.gridColUserName.Width = 125;
            // 
            // gridColFullName
            // 
            this.gridColFullName.HeaderText = "Full Name";
            this.gridColFullName.Name = "gridColFullName";
            this.gridColFullName.ReadOnly = true;
            this.gridColFullName.Width = 250;
            // 
            // gridColUserType
            // 
            this.gridColUserType.HeaderText = "User Type";
            this.gridColUserType.Name = "gridColUserType";
            this.gridColUserType.ReadOnly = true;
            this.gridColUserType.Width = 120;
            // 
            // gridColMobileNo
            // 
            this.gridColMobileNo.HeaderText = "Contact No.";
            this.gridColMobileNo.Name = "gridColMobileNo";
            this.gridColMobileNo.ReadOnly = true;
            this.gridColMobileNo.Width = 150;
            // 
            // gridColUserStatus
            // 
            this.gridColUserStatus.HeaderText = "User Status";
            this.gridColUserStatus.Name = "gridColUserStatus";
            this.gridColUserStatus.ReadOnly = true;
            this.gridColUserStatus.Width = 120;
            // 
            // grdColShowDetails
            // 
            this.grdColShowDetails.HeaderText = "";
            this.grdColShowDetails.Name = "grdColShowDetails";
            this.grdColShowDetails.Text = "View";
            this.grdColShowDetails.ToolTipText = "View user details.";
            this.grdColShowDetails.Width = 90;
            // 
            // gridColUserId
            // 
            this.gridColUserId.HeaderText = "UserId";
            this.gridColUserId.MinimumWidth = 2;
            this.gridColUserId.Name = "gridColUserId";
            this.gridColUserId.ReadOnly = true;
            this.gridColUserId.Visible = false;
            this.gridColUserId.Width = 2;
            // 
            // gridColIndividualId
            // 
            this.gridColIndividualId.HeaderText = "IndividualId";
            this.gridColIndividualId.MinimumWidth = 2;
            this.gridColIndividualId.Name = "gridColIndividualId";
            this.gridColIndividualId.ReadOnly = true;
            this.gridColIndividualId.Visible = false;
            this.gridColIndividualId.Width = 2;
            // 
            // columnTransfer
            // 
            this.columnTransfer.HeaderText = "";
            this.columnTransfer.Name = "columnTransfer";
            this.columnTransfer.ReadOnly = true;
            this.columnTransfer.Text = "Transfer";
            this.columnTransfer.UseColumnTextForLinkValue = true;
            // 
            // customDataGridViewHeader1
            // 
            this.customDataGridViewHeader1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customDataGridViewHeader1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(123)))), ((int)(((byte)(208)))));
            this.customDataGridViewHeader1.ExportReportSubtitleOne = "Subtitle One";
            this.customDataGridViewHeader1.ExportReportSubtitleThree = "Subtitle Three";
            this.customDataGridViewHeader1.ExportReportSubtitleTwo = "Subtitle Two";
            this.customDataGridViewHeader1.ExportReportTitle = "Title";
            this.customDataGridViewHeader1.Filter = this.dgvUserList;
            this.customDataGridViewHeader1.HeaderText = "HeaderText";
            this.customDataGridViewHeader1.Location = new System.Drawing.Point(12, 144);
            this.customDataGridViewHeader1.Name = "customDataGridViewHeader1";
            this.customDataGridViewHeader1.Size = new System.Drawing.Size(919, 28);
            this.customDataGridViewHeader1.TabIndex = 14;
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = this.label1;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(943, 35);
            this.customTitlebar1.TabIndex = 15;
            this.customTitlebar1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.label1.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label1.Location = new System.Drawing.Point(7, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(186, 25);
            this.label1.TabIndex = 16;
            this.label1.Text = "User Transfer History";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton1.Location = new System.Drawing.Point(9, 3);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(117, 17);
            this.radioButton1.TabIndex = 17;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Workstation Wise";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton2.Location = new System.Drawing.Point(129, 3);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(76, 17);
            this.radioButton2.TabIndex = 18;
            this.radioButton2.Text = "User Wise";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.Controls.Add(this.customDateTimePicker2);
            this.panel1.Controls.Add(this.customDateTimePicker1);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Controls.Add(this.radioButton2);
            this.panel1.Controls.Add(this.radioButton1);
            this.panel1.Location = new System.Drawing.Point(98, 38);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(745, 99);
            this.panel1.TabIndex = 19;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.panel2);
            this.flowLayoutPanel1.Controls.Add(this.panel3);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 22);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(628, 37);
            this.flowLayoutPanel1.TabIndex = 19;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.customComboBoxDropDownList1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(312, 31);
            this.panel2.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.customTextBox1);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(321, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(304, 31);
            this.panel3.TabIndex = 0;
            this.panel3.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Outlet";
            // 
            // customComboBoxDropDownList1
            // 
            this.customComboBoxDropDownList1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.customComboBoxDropDownList1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.customComboBoxDropDownList1.FormattingEnabled = true;
            this.customComboBoxDropDownList1.InputScopeAllowEmpty = false;
            this.customComboBoxDropDownList1.IsValid = null;
            this.customComboBoxDropDownList1.Location = new System.Drawing.Point(65, 5);
            this.customComboBoxDropDownList1.Name = "customComboBoxDropDownList1";
            this.customComboBoxDropDownList1.PromptText = "(Select)";
            this.customComboBoxDropDownList1.ReadOnly = false;
            this.customComboBoxDropDownList1.ShowMandatoryMark = false;
            this.customComboBoxDropDownList1.Size = new System.Drawing.Size(234, 21);
            this.customComboBoxDropDownList1.TabIndex = 1;
            this.customComboBoxDropDownList1.ValidationErrorMessage = "Validation Error!";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "User";
            // 
            // customTextBox1
            // 
            this.customTextBox1.BackColor = System.Drawing.SystemColors.Window;
            this.customTextBox1.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.customTextBox1.InputScopeAllowEmpty = false;
            this.customTextBox1.InputScopeCustomString = null;
            this.customTextBox1.IsValid = null;
            this.customTextBox1.Location = new System.Drawing.Point(65, 5);
            this.customTextBox1.MaxLength = 32767;
            this.customTextBox1.MinimumSize = new System.Drawing.Size(0, 21);
            this.customTextBox1.Name = "customTextBox1";
            this.customTextBox1.PromptText = "(Type here...)";
            this.customTextBox1.ReadOnly = false;
            this.customTextBox1.ShowMandatoryMark = false;
            this.customTextBox1.Size = new System.Drawing.Size(234, 21);
            this.customTextBox1.TabIndex = 1;
            this.customTextBox1.Text = "customTextBox1";
            this.customTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.customTextBox1.UpperCaseOnly = false;
            this.customTextBox1.ValidationErrorMessage = "Validation Error!";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(635, 28);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(102, 25);
            this.button1.TabIndex = 20;
            this.button1.Text = "Search";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Form Date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(326, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "To Date";
            // 
            // customDateTimePicker1
            // 
            this.customDateTimePicker1.Date = "";
            this.customDateTimePicker1.Location = new System.Drawing.Point(68, 64);
            this.customDateTimePicker1.MaximumSize = new System.Drawing.Size(400, 25);
            this.customDateTimePicker1.MinimumSize = new System.Drawing.Size(60, 25);
            this.customDateTimePicker1.Name = "customDateTimePicker1";
            this.customDateTimePicker1.PresetServerDate = true;
            this.customDateTimePicker1.Size = new System.Drawing.Size(238, 25);
            this.customDateTimePicker1.TabIndex = 23;
            this.customDateTimePicker1.Value = new System.DateTime(2016, 12, 1, 17, 43, 26, 186);
            // 
            // customDateTimePicker2
            // 
            this.customDateTimePicker2.Date = "";
            this.customDateTimePicker2.Location = new System.Drawing.Point(386, 64);
            this.customDateTimePicker2.MaximumSize = new System.Drawing.Size(400, 25);
            this.customDateTimePicker2.MinimumSize = new System.Drawing.Size(60, 25);
            this.customDateTimePicker2.Name = "customDateTimePicker2";
            this.customDateTimePicker2.PresetServerDate = true;
            this.customDateTimePicker2.Size = new System.Drawing.Size(238, 25);
            this.customDateTimePicker2.TabIndex = 24;
            this.customDateTimePicker2.Value = new System.DateTime(2016, 12, 1, 17, 43, 29, 851);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(635, 64);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(102, 25);
            this.button2.TabIndex = 20;
            this.button2.Text = "Clsoe";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.label6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(0, 491);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(943, 24);
            this.label6.TabIndex = 21;
            this.label6.Text = "   Count: 0";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmUserTransferHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(943, 515);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.customDataGridViewHeader1);
            this.Controls.Add(this.dgvUserList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmUserTransferHistory";
            this.Text = "frmUserTransferHistory";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmUserTransferHistory_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserList)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvUserList;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColFullName;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColUserType;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColMobileNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColUserStatus;
        private System.Windows.Forms.DataGridViewLinkColumn grdColShowDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColUserId;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColIndividualId;
        private System.Windows.Forms.DataGridViewLinkColumn columnTransfer;
        private Module.Common.Exporter.CustomDataGridViewHeader customDataGridViewHeader1;
        private Agent.CustomControls.CustomTitlebar customTitlebar1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private Agent.CustomControls.CustomComboBoxDropDownList customComboBoxDropDownList1;
        private System.Windows.Forms.Label label2;
        private Agent.CustomControls.CustomTextBox customTextBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private CustomControls.CustomDateTimePicker customDateTimePicker2;
        private CustomControls.CustomDateTimePicker customDateTimePicker1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label6;
    }
}