﻿using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.UI.forms
{
    partial class frmAgentBalance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint3 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 40D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint4 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 60D);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblAgentCode = new System.Windows.Forms.Label();
            this.lblAgentName = new System.Windows.Forms.Label();
            this.dgvOutletCashInfo = new System.Windows.Forms.DataGridView();
            this.gvColSubAgentCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gvColSubAgentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gvColMobleNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gvColCurrentBalance = new System.Windows.Forms.DataGridViewLinkColumn();
            this.gvColSubAgentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblCurrentBalanceValue = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblDate = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblOutletTotal = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblAgentAccBalance = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblGrandTotal = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblPieColorOutlet = new System.Windows.Forms.Label();
            this.lblPieColorAgent = new System.Windows.Forms.Label();
            this.lblPieOutletTotal = new System.Windows.Forms.Label();
            this.lblPieAgentBalance = new System.Windows.Forms.Label();
            this.chartBalance = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOutletCashInfo)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartBalance)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label1.Location = new System.Drawing.Point(4, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 21);
            this.label1.TabIndex = 101;
            this.label1.Text = "Agent Code :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label2.Location = new System.Drawing.Point(277, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 21);
            this.label2.TabIndex = 102;
            this.label2.Text = "Agent Name :";
            // 
            // lblAgentCode
            // 
            this.lblAgentCode.AutoSize = true;
            this.lblAgentCode.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.lblAgentCode.Location = new System.Drawing.Point(108, 13);
            this.lblAgentCode.Name = "lblAgentCode";
            this.lblAgentCode.Size = new System.Drawing.Size(95, 21);
            this.lblAgentCode.TabIndex = 103;
            this.lblAgentCode.Text = "AgentCode";
            // 
            // lblAgentName
            // 
            this.lblAgentName.AutoSize = true;
            this.lblAgentName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.lblAgentName.Location = new System.Drawing.Point(387, 13);
            this.lblAgentName.Name = "lblAgentName";
            this.lblAgentName.Size = new System.Drawing.Size(102, 21);
            this.lblAgentName.TabIndex = 104;
            this.lblAgentName.Text = "AgentName";
            // 
            // dgvOutletCashInfo
            // 
            this.dgvOutletCashInfo.AllowUserToAddRows = false;
            this.dgvOutletCashInfo.AllowUserToDeleteRows = false;
            this.dgvOutletCashInfo.AllowUserToOrderColumns = true;
            this.dgvOutletCashInfo.AllowUserToResizeColumns = false;
            this.dgvOutletCashInfo.AllowUserToResizeRows = false;
            this.dgvOutletCashInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvOutletCashInfo.BackgroundColor = System.Drawing.Color.White;
            this.dgvOutletCashInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvOutletCashInfo.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgvOutletCashInfo.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.SkyBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(1, 5, 1, 5);
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvOutletCashInfo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvOutletCashInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOutletCashInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gvColSubAgentCode,
            this.gvColSubAgentName,
            this.gvColMobleNumber,
            this.gvColCurrentBalance,
            this.gvColSubAgentID});
            this.dgvOutletCashInfo.EnableHeadersVisualStyles = false;
            this.dgvOutletCashInfo.Location = new System.Drawing.Point(8, 44);
            this.dgvOutletCashInfo.Name = "dgvOutletCashInfo";
            this.dgvOutletCashInfo.ReadOnly = true;
            this.dgvOutletCashInfo.RowHeadersVisible = false;
            this.dgvOutletCashInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvOutletCashInfo.ShowEditingIcon = false;
            this.dgvOutletCashInfo.Size = new System.Drawing.Size(824, 282);
            this.dgvOutletCashInfo.TabIndex = 105;
            this.dgvOutletCashInfo.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOutletCashInfo_CellContentClick);
            // 
            // gvColSubAgentCode
            // 
            this.gvColSubAgentCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.gvColSubAgentCode.FillWeight = 71.78219F;
            this.gvColSubAgentCode.HeaderText = "Outlet Code";
            this.gvColSubAgentCode.Name = "gvColSubAgentCode";
            this.gvColSubAgentCode.ReadOnly = true;
            this.gvColSubAgentCode.Width = 145;
            // 
            // gvColSubAgentName
            // 
            this.gvColSubAgentName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.gvColSubAgentName.FillWeight = 123.0431F;
            this.gvColSubAgentName.HeaderText = "Outlet Name";
            this.gvColSubAgentName.Name = "gvColSubAgentName";
            this.gvColSubAgentName.ReadOnly = true;
            // 
            // gvColMobleNumber
            // 
            this.gvColMobleNumber.FillWeight = 72.00413F;
            this.gvColMobleNumber.HeaderText = "Contact No.";
            this.gvColMobleNumber.Name = "gvColMobleNumber";
            this.gvColMobleNumber.ReadOnly = true;
            this.gvColMobleNumber.Width = 145;
            // 
            // gvColCurrentBalance
            // 
            this.gvColCurrentBalance.FillWeight = 133.1707F;
            this.gvColCurrentBalance.HeaderText = "Current Balance";
            this.gvColCurrentBalance.Name = "gvColCurrentBalance";
            this.gvColCurrentBalance.ReadOnly = true;
            this.gvColCurrentBalance.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.gvColCurrentBalance.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.gvColCurrentBalance.Width = 269;
            // 
            // gvColSubAgentID
            // 
            this.gvColSubAgentID.HeaderText = "Outlet Id";
            this.gvColSubAgentID.Name = "gvColSubAgentID";
            this.gvColSubAgentID.ReadOnly = true;
            this.gvColSubAgentID.Visible = false;
            this.gvColSubAgentID.Width = 5;
            // 
            // lblCurrentBalanceValue
            // 
            this.lblCurrentBalanceValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCurrentBalanceValue.AutoSize = true;
            this.lblCurrentBalanceValue.Font = new System.Drawing.Font("Segoe UI", 22F, System.Drawing.FontStyle.Bold);
            this.lblCurrentBalanceValue.Location = new System.Drawing.Point(249, 28);
            this.lblCurrentBalanceValue.Name = "lblCurrentBalanceValue";
            this.lblCurrentBalanceValue.Size = new System.Drawing.Size(325, 41);
            this.lblCurrentBalanceValue.TabIndex = 113;
            this.lblCurrentBalanceValue.Text = "Agent Balance Details";
            this.lblCurrentBalanceValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblAgentCode);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lblAgentName);
            this.groupBox1.Controls.Add(this.dgvOutletCashInfo);
            this.groupBox1.Location = new System.Drawing.Point(12, 71);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(838, 335);
            this.groupBox1.TabIndex = 114;
            this.groupBox1.TabStop = false;
            // 
            // lblDate
            // 
            this.lblDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Bold);
            this.lblDate.Location = new System.Drawing.Point(765, 44);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(85, 20);
            this.lblDate.TabIndex = 115;
            this.lblDate.Text = "00/00/0000";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.label9.Location = new System.Drawing.Point(716, 44);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 20);
            this.label9.TabIndex = 116;
            this.label9.Text = "Date :";
            // 
            // lblOutletTotal
            // 
            this.lblOutletTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOutletTotal.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Bold);
            this.lblOutletTotal.Location = new System.Drawing.Point(590, 16);
            this.lblOutletTotal.Name = "lblOutletTotal";
            this.lblOutletTotal.Size = new System.Drawing.Size(233, 28);
            this.lblOutletTotal.TabIndex = 117;
            this.lblOutletTotal.Text = "00000000.00 BDT";
            this.lblOutletTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 15F);
            this.label15.Location = new System.Drawing.Point(413, 16);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(169, 28);
            this.label15.TabIndex = 118;
            this.label15.Text = "Outlet Total Cash :";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAgentAccBalance
            // 
            this.lblAgentAccBalance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAgentAccBalance.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Bold);
            this.lblAgentAccBalance.Location = new System.Drawing.Point(590, 50);
            this.lblAgentAccBalance.Name = "lblAgentAccBalance";
            this.lblAgentAccBalance.Size = new System.Drawing.Size(233, 28);
            this.lblAgentAccBalance.TabIndex = 119;
            this.lblAgentAccBalance.Text = "00000000.00 BDT";
            this.lblAgentAccBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 15F);
            this.label7.Location = new System.Drawing.Point(360, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(222, 28);
            this.label7.TabIndex = 120;
            this.label7.Text = "Agent Account Balance :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGrandTotal
            // 
            this.lblGrandTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGrandTotal.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold);
            this.lblGrandTotal.Location = new System.Drawing.Point(589, 87);
            this.lblGrandTotal.Name = "lblGrandTotal";
            this.lblGrandTotal.Size = new System.Drawing.Size(234, 32);
            this.lblGrandTotal.TabIndex = 121;
            this.lblGrandTotal.Text = "00000000.00 BDT";
            this.lblGrandTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 18F);
            this.label10.Location = new System.Drawing.Point(435, 87);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(149, 32);
            this.label10.TabIndex = 122;
            this.label10.Text = "Grand Total :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblPieColorOutlet);
            this.groupBox2.Controls.Add(this.lblPieColorAgent);
            this.groupBox2.Controls.Add(this.lblPieOutletTotal);
            this.groupBox2.Controls.Add(this.lblPieAgentBalance);
            this.groupBox2.Controls.Add(this.chartBalance);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.lblGrandTotal);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.lblAgentAccBalance);
            this.groupBox2.Controls.Add(this.lblOutletTotal);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Location = new System.Drawing.Point(12, 408);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(838, 140);
            this.groupBox2.TabIndex = 123;
            this.groupBox2.TabStop = false;
            // 
            // lblPieColorOutlet
            // 
            this.lblPieColorOutlet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPieColorOutlet.BackColor = System.Drawing.Color.SandyBrown;
            this.lblPieColorOutlet.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.lblPieColorOutlet.Location = new System.Drawing.Point(190, 45);
            this.lblPieColorOutlet.Name = "lblPieColorOutlet";
            this.lblPieColorOutlet.Size = new System.Drawing.Size(25, 8);
            this.lblPieColorOutlet.TabIndex = 129;
            // 
            // lblPieColorAgent
            // 
            this.lblPieColorAgent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPieColorAgent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lblPieColorAgent.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.lblPieColorAgent.Location = new System.Drawing.Point(190, 19);
            this.lblPieColorAgent.Name = "lblPieColorAgent";
            this.lblPieColorAgent.Size = new System.Drawing.Size(25, 8);
            this.lblPieColorAgent.TabIndex = 128;
            // 
            // lblPieOutletTotal
            // 
            this.lblPieOutletTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPieOutletTotal.AutoSize = true;
            this.lblPieOutletTotal.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.lblPieOutletTotal.Location = new System.Drawing.Point(219, 43);
            this.lblPieOutletTotal.Name = "lblPieOutletTotal";
            this.lblPieOutletTotal.Size = new System.Drawing.Size(83, 13);
            this.lblPieOutletTotal.TabIndex = 127;
            this.lblPieOutletTotal.Text = "Outlet Balance";
            // 
            // lblPieAgentBalance
            // 
            this.lblPieAgentBalance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPieAgentBalance.AutoSize = true;
            this.lblPieAgentBalance.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.lblPieAgentBalance.Location = new System.Drawing.Point(219, 16);
            this.lblPieAgentBalance.Name = "lblPieAgentBalance";
            this.lblPieAgentBalance.Size = new System.Drawing.Size(81, 13);
            this.lblPieAgentBalance.TabIndex = 126;
            this.lblPieAgentBalance.Text = "Agent Balance";
            // 
            // chartBalance
            // 
            chartArea2.Name = "ChartArea1";
            this.chartBalance.ChartAreas.Add(chartArea2);
            this.chartBalance.Location = new System.Drawing.Point(13, 10);
            this.chartBalance.Name = "chartBalance";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series2.IsVisibleInLegend = false;
            series2.Name = "Series2";
            series2.Points.Add(dataPoint3);
            series2.Points.Add(dataPoint4);
            this.chartBalance.Series.Add(series2);
            this.chartBalance.Size = new System.Drawing.Size(158, 129);
            this.chartBalance.TabIndex = 11;
            this.chartBalance.Text = "chart3";
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(768, 555);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(82, 28);
            this.btnClose.TabIndex = 124;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnPrint.FlatAppearance.BorderSize = 0;
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btnPrint.ForeColor = System.Drawing.Color.White;
            this.btnPrint.Location = new System.Drawing.Point(677, 555);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(82, 28);
            this.btnPrint.TabIndex = 125;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(862, 26);
            this.customTitlebar1.TabIndex = 126;
            this.customTitlebar1.TabStop = false;
            // 
            // frmAgentBalance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(862, 595);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblCurrentBalanceValue);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmAgentBalance";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agent Balance";
            ((System.ComponentModel.ISupportInitialize)(this.dgvOutletCashInfo)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartBalance)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblAgentCode;
        private System.Windows.Forms.Label lblAgentName;
        private System.Windows.Forms.DataGridView dgvOutletCashInfo;
        private System.Windows.Forms.Label lblCurrentBalanceValue;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblOutletTotal;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblAgentAccBalance;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblGrandTotal;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnPrint;
        public System.Windows.Forms.DataVisualization.Charting.Chart chartBalance;
        private System.Windows.Forms.Label lblPieAgentBalance;
        private System.Windows.Forms.Label lblPieOutletTotal;
        private System.Windows.Forms.Label lblPieColorAgent;
        private System.Windows.Forms.Label lblPieColorOutlet;
        private System.Windows.Forms.DataGridViewTextBoxColumn gvColSubAgentID;
        private System.Windows.Forms.DataGridViewLinkColumn gvColCurrentBalance;
        private System.Windows.Forms.DataGridViewTextBoxColumn gvColMobleNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn gvColSubAgentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn gvColSubAgentCode;
        private CustomTitlebar customTitlebar1;
    }
}