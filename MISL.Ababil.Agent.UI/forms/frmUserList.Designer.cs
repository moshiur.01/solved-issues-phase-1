﻿using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.UI.forms
{
    partial class frmUserList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.btnClose = new System.Windows.Forms.Button();
            this.cmbOutlet = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.lblOutlet = new System.Windows.Forms.Label();
            this.cmbAgent = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.lblAgent = new System.Windows.Forms.Label();
            this.cmbUserType = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.lblUserType = new System.Windows.Forms.Label();
            this.cmbUserSubType = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.lblUserSubtype = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAddBranchUser = new System.Windows.Forms.Button();
            this.btnAddAgentUser = new System.Windows.Forms.Button();
            this.btnAddSubAgentUser = new System.Windows.Forms.Button();
            this.pnlUserSubType = new System.Windows.Forms.Panel();
            this.pnlAgent = new System.Windows.Forms.Panel();
            this.pnlSubAgent = new System.Windows.Forms.Panel();
            this.dgvUserList = new System.Windows.Forms.DataGridView();
            this.gridColUserName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridColFullName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridColUserType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridColMobileNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridColUserStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdColShowDetails = new System.Windows.Forms.DataGridViewLinkColumn();
            this.gridColUserId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridColIndividualId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnTransfer = new System.Windows.Forms.DataGridViewLinkColumn();
            this.pnlUserType = new System.Windows.Forms.Panel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.customDataGridViewHeader1 = new MISL.Ababil.Agent.Module.Common.Exporter.CustomDataGridViewHeader();
            this.gridColFilter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlUserSubType.SuspendLayout();
            this.pnlAgent.SuspendLayout();
            this.pnlSubAgent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserList)).BeginInit();
            this.pnlUserType.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(1005, 26);
            this.customTitlebar1.TabIndex = 14;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(914, 551);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(79, 25);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // cmbOutlet
            // 
            this.cmbOutlet.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbOutlet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOutlet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbOutlet.FormattingEnabled = true;
            this.cmbOutlet.InputScopeAllowEmpty = false;
            this.cmbOutlet.IntegralHeight = false;
            this.cmbOutlet.IsValid = null;
            this.cmbOutlet.ItemHeight = 13;
            this.cmbOutlet.Location = new System.Drawing.Point(103, 2);
            this.cmbOutlet.Name = "cmbOutlet";
            this.cmbOutlet.PromptText = "(Select)";
            this.cmbOutlet.ReadOnly = false;
            this.cmbOutlet.ShowMandatoryMark = false;
            this.cmbOutlet.Size = new System.Drawing.Size(278, 21);
            this.cmbOutlet.TabIndex = 1;
            this.cmbOutlet.ValidationErrorMessage = "Validation Error!";
            this.cmbOutlet.SelectedIndexChanged += new System.EventHandler(this.cmbOutlet_SelectedIndexChanged);
            // 
            // lblOutlet
            // 
            this.lblOutlet.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblOutlet.AutoSize = true;
            this.lblOutlet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOutlet.Location = new System.Drawing.Point(52, 7);
            this.lblOutlet.Name = "lblOutlet";
            this.lblOutlet.Size = new System.Drawing.Size(45, 15);
            this.lblOutlet.TabIndex = 0;
            this.lblOutlet.Text = "Outlet :";
            // 
            // cmbAgent
            // 
            this.cmbAgent.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbAgent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAgent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbAgent.FormattingEnabled = true;
            this.cmbAgent.InputScopeAllowEmpty = false;
            this.cmbAgent.IntegralHeight = false;
            this.cmbAgent.IsValid = null;
            this.cmbAgent.ItemHeight = 13;
            this.cmbAgent.Location = new System.Drawing.Point(103, 2);
            this.cmbAgent.Name = "cmbAgent";
            this.cmbAgent.PromptText = "(Select)";
            this.cmbAgent.ReadOnly = false;
            this.cmbAgent.ShowMandatoryMark = false;
            this.cmbAgent.Size = new System.Drawing.Size(278, 21);
            this.cmbAgent.TabIndex = 1;
            this.cmbAgent.ValidationErrorMessage = "Validation Error!";
            this.cmbAgent.SelectedIndexChanged += new System.EventHandler(this.cmbAgent_SelectedIndexChanged);
            // 
            // lblAgent
            // 
            this.lblAgent.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblAgent.AutoSize = true;
            this.lblAgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAgent.Location = new System.Drawing.Point(53, 7);
            this.lblAgent.Name = "lblAgent";
            this.lblAgent.Size = new System.Drawing.Size(44, 15);
            this.lblAgent.TabIndex = 0;
            this.lblAgent.Text = "Agent :";
            // 
            // cmbUserType
            // 
            this.cmbUserType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbUserType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUserType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbUserType.FormattingEnabled = true;
            this.cmbUserType.InputScopeAllowEmpty = false;
            this.cmbUserType.IntegralHeight = false;
            this.cmbUserType.IsValid = null;
            this.cmbUserType.ItemHeight = 13;
            this.cmbUserType.Location = new System.Drawing.Point(103, 4);
            this.cmbUserType.Name = "cmbUserType";
            this.cmbUserType.PromptText = "(Select)";
            this.cmbUserType.ReadOnly = false;
            this.cmbUserType.ShowMandatoryMark = false;
            this.cmbUserType.Size = new System.Drawing.Size(278, 21);
            this.cmbUserType.TabIndex = 2;
            this.cmbUserType.ValidationErrorMessage = "Validation Error!";
            this.cmbUserType.SelectedIndexChanged += new System.EventHandler(this.cmbUserType_SelectedIndexChanged);
            // 
            // lblUserType
            // 
            this.lblUserType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblUserType.AutoSize = true;
            this.lblUserType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserType.Location = new System.Drawing.Point(29, 7);
            this.lblUserType.Name = "lblUserType";
            this.lblUserType.Size = new System.Drawing.Size(68, 15);
            this.lblUserType.TabIndex = 1;
            this.lblUserType.Text = "User Type :";
            // 
            // cmbUserSubType
            // 
            this.cmbUserSubType.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbUserSubType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUserSubType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbUserSubType.FormattingEnabled = true;
            this.cmbUserSubType.InputScopeAllowEmpty = false;
            this.cmbUserSubType.IntegralHeight = false;
            this.cmbUserSubType.IsValid = null;
            this.cmbUserSubType.ItemHeight = 13;
            this.cmbUserSubType.Location = new System.Drawing.Point(103, 2);
            this.cmbUserSubType.Name = "cmbUserSubType";
            this.cmbUserSubType.PromptText = "(Select)";
            this.cmbUserSubType.ReadOnly = false;
            this.cmbUserSubType.ShowMandatoryMark = false;
            this.cmbUserSubType.Size = new System.Drawing.Size(278, 21);
            this.cmbUserSubType.TabIndex = 1;
            this.cmbUserSubType.ValidationErrorMessage = "Validation Error!";
            this.cmbUserSubType.SelectedIndexChanged += new System.EventHandler(this.cmbUserSubType_SelectedIndexChanged);
            // 
            // lblUserSubtype
            // 
            this.lblUserSubtype.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblUserSubtype.AutoSize = true;
            this.lblUserSubtype.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserSubtype.Location = new System.Drawing.Point(11, 6);
            this.lblUserSubtype.Name = "lblUserSubtype";
            this.lblUserSubtype.Size = new System.Drawing.Size(86, 15);
            this.lblUserSubtype.TabIndex = 0;
            this.lblUserSubtype.Text = "User Subtype :";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(120, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(748, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "User Management";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAddBranchUser
            // 
            this.btnAddBranchUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddBranchUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnAddBranchUser.FlatAppearance.BorderSize = 0;
            this.btnAddBranchUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddBranchUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btnAddBranchUser.ForeColor = System.Drawing.Color.White;
            this.btnAddBranchUser.Location = new System.Drawing.Point(392, 2);
            this.btnAddBranchUser.Name = "btnAddBranchUser";
            this.btnAddBranchUser.Size = new System.Drawing.Size(138, 24);
            this.btnAddBranchUser.TabIndex = 2;
            this.btnAddBranchUser.Text = "Add Bank User";
            this.btnAddBranchUser.UseVisualStyleBackColor = false;
            this.btnAddBranchUser.Click += new System.EventHandler(this.btnAddBranchUser_Click);
            // 
            // btnAddAgentUser
            // 
            this.btnAddAgentUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddAgentUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnAddAgentUser.FlatAppearance.BorderSize = 0;
            this.btnAddAgentUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddAgentUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btnAddAgentUser.ForeColor = System.Drawing.Color.White;
            this.btnAddAgentUser.Location = new System.Drawing.Point(392, 2);
            this.btnAddAgentUser.Name = "btnAddAgentUser";
            this.btnAddAgentUser.Size = new System.Drawing.Size(138, 24);
            this.btnAddAgentUser.TabIndex = 2;
            this.btnAddAgentUser.Text = "Add Agent User";
            this.btnAddAgentUser.UseVisualStyleBackColor = false;
            this.btnAddAgentUser.Click += new System.EventHandler(this.btnAddAgentUser_Click);
            // 
            // btnAddSubAgentUser
            // 
            this.btnAddSubAgentUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddSubAgentUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnAddSubAgentUser.FlatAppearance.BorderSize = 0;
            this.btnAddSubAgentUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddSubAgentUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btnAddSubAgentUser.ForeColor = System.Drawing.Color.White;
            this.btnAddSubAgentUser.Location = new System.Drawing.Point(392, 2);
            this.btnAddSubAgentUser.Name = "btnAddSubAgentUser";
            this.btnAddSubAgentUser.Size = new System.Drawing.Size(138, 24);
            this.btnAddSubAgentUser.TabIndex = 2;
            this.btnAddSubAgentUser.Text = "Add Outlet User";
            this.btnAddSubAgentUser.UseVisualStyleBackColor = false;
            this.btnAddSubAgentUser.Click += new System.EventHandler(this.btnAddSubAgentUser_Click);
            // 
            // pnlUserSubType
            // 
            this.pnlUserSubType.Controls.Add(this.cmbUserSubType);
            this.pnlUserSubType.Controls.Add(this.btnAddBranchUser);
            this.pnlUserSubType.Controls.Add(this.lblUserSubtype);
            this.pnlUserSubType.Location = new System.Drawing.Point(3, 108);
            this.pnlUserSubType.Name = "pnlUserSubType";
            this.pnlUserSubType.Size = new System.Drawing.Size(545, 29);
            this.pnlUserSubType.TabIndex = 3;
            // 
            // pnlAgent
            // 
            this.pnlAgent.Controls.Add(this.cmbAgent);
            this.pnlAgent.Controls.Add(this.btnAddAgentUser);
            this.pnlAgent.Controls.Add(this.lblAgent);
            this.pnlAgent.Location = new System.Drawing.Point(3, 38);
            this.pnlAgent.Name = "pnlAgent";
            this.pnlAgent.Size = new System.Drawing.Size(545, 29);
            this.pnlAgent.TabIndex = 1;
            // 
            // pnlSubAgent
            // 
            this.pnlSubAgent.Controls.Add(this.cmbOutlet);
            this.pnlSubAgent.Controls.Add(this.lblOutlet);
            this.pnlSubAgent.Controls.Add(this.btnAddSubAgentUser);
            this.pnlSubAgent.Location = new System.Drawing.Point(3, 73);
            this.pnlSubAgent.Name = "pnlSubAgent";
            this.pnlSubAgent.Size = new System.Drawing.Size(545, 29);
            this.pnlSubAgent.TabIndex = 2;
            // 
            // dgvUserList
            // 
            this.dgvUserList.AllowUserToAddRows = false;
            this.dgvUserList.AllowUserToDeleteRows = false;
            this.dgvUserList.AllowUserToResizeColumns = false;
            this.dgvUserList.AllowUserToResizeRows = false;
            this.dgvUserList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvUserList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvUserList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvUserList.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvUserList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvUserList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgvUserList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvUserList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvUserList.ColumnHeadersHeight = 30;
            this.dgvUserList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gridColUserName,
            this.gridColFullName,
            this.gridColUserType,
            this.gridColMobileNo,
            this.gridColUserStatus,
            this.grdColShowDetails,
            this.gridColUserId,
            this.gridColIndividualId,
            this.columnTransfer,
            this.gridColFilter});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(0, 1, 0, 1);
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(231)))), ((int)(((byte)(254)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvUserList.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvUserList.EnableHeadersVisualStyles = false;
            this.dgvUserList.GridColor = System.Drawing.Color.LightGray;
            this.dgvUserList.Location = new System.Drawing.Point(11, 255);
            this.dgvUserList.Name = "dgvUserList";
            this.dgvUserList.RowHeadersVisible = false;
            this.dgvUserList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvUserList.Size = new System.Drawing.Size(982, 287);
            this.dgvUserList.TabIndex = 12;
            this.dgvUserList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUserList_CellContentClick);
            this.dgvUserList.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUserList_CellContentDoubleClick);
            // 
            // gridColUserName
            // 
            this.gridColUserName.HeaderText = "User Name";
            this.gridColUserName.Name = "gridColUserName";
            this.gridColUserName.ReadOnly = true;
            // 
            // gridColFullName
            // 
            this.gridColFullName.HeaderText = "Full Name";
            this.gridColFullName.Name = "gridColFullName";
            this.gridColFullName.ReadOnly = true;
            // 
            // gridColUserType
            // 
            this.gridColUserType.HeaderText = "User Type";
            this.gridColUserType.Name = "gridColUserType";
            this.gridColUserType.ReadOnly = true;
            // 
            // gridColMobileNo
            // 
            this.gridColMobileNo.HeaderText = "Contact No.";
            this.gridColMobileNo.Name = "gridColMobileNo";
            this.gridColMobileNo.ReadOnly = true;
            // 
            // gridColUserStatus
            // 
            this.gridColUserStatus.HeaderText = "User Status";
            this.gridColUserStatus.Name = "gridColUserStatus";
            this.gridColUserStatus.ReadOnly = true;
            // 
            // grdColShowDetails
            // 
            this.grdColShowDetails.HeaderText = "";
            this.grdColShowDetails.Name = "grdColShowDetails";
            this.grdColShowDetails.Text = "View";
            this.grdColShowDetails.ToolTipText = "View user details.";
            // 
            // gridColUserId
            // 
            this.gridColUserId.HeaderText = "UserId";
            this.gridColUserId.MinimumWidth = 2;
            this.gridColUserId.Name = "gridColUserId";
            this.gridColUserId.ReadOnly = true;
            this.gridColUserId.Visible = false;
            // 
            // gridColIndividualId
            // 
            this.gridColIndividualId.HeaderText = "IndividualId";
            this.gridColIndividualId.MinimumWidth = 2;
            this.gridColIndividualId.Name = "gridColIndividualId";
            this.gridColIndividualId.ReadOnly = true;
            this.gridColIndividualId.Visible = false;
            // 
            // columnTransfer
            // 
            this.columnTransfer.HeaderText = "";
            this.columnTransfer.Name = "columnTransfer";
            this.columnTransfer.ReadOnly = true;
            this.columnTransfer.Text = "Transfer";
            this.columnTransfer.UseColumnTextForLinkValue = true;
            // 
            // pnlUserType
            // 
            this.pnlUserType.Controls.Add(this.cmbUserType);
            this.pnlUserType.Controls.Add(this.lblUserType);
            this.pnlUserType.Location = new System.Drawing.Point(3, 3);
            this.pnlUserType.Name = "pnlUserType";
            this.pnlUserType.Size = new System.Drawing.Size(545, 29);
            this.pnlUserType.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.flowLayoutPanel1.Controls.Add(this.pnlUserType);
            this.flowLayoutPanel1.Controls.Add(this.pnlAgent);
            this.flowLayoutPanel1.Controls.Add(this.pnlSubAgent);
            this.flowLayoutPanel1.Controls.Add(this.pnlUserSubType);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(271, 79);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(556, 143);
            this.flowLayoutPanel1.TabIndex = 16;
            // 
            // customDataGridViewHeader1
            // 
            this.customDataGridViewHeader1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(123)))), ((int)(((byte)(208)))));
            this.customDataGridViewHeader1.ExportReportSubtitleOne = "Subtitle One";
            this.customDataGridViewHeader1.ExportReportSubtitleThree = "Subtitle Three";
            this.customDataGridViewHeader1.ExportReportSubtitleTwo = "Subtitle Two";
            this.customDataGridViewHeader1.ExportReportTitle = "Title";
            this.customDataGridViewHeader1.Filter = this.dgvUserList;
            this.customDataGridViewHeader1.HeaderText = "HeaderText";
            this.customDataGridViewHeader1.Location = new System.Drawing.Point(11, 228);
            this.customDataGridViewHeader1.Name = "customDataGridViewHeader1";
            this.customDataGridViewHeader1.Size = new System.Drawing.Size(982, 28);
            this.customDataGridViewHeader1.TabIndex = 17;
            // 
            // gridColFilter
            // 
            this.gridColFilter.HeaderText = "filter";
            this.gridColFilter.Name = "gridColFilter";
            this.gridColFilter.Visible = false;
            // 
            // frmUserList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1004, 585);
            this.ControlBox = false;
            this.Controls.Add(this.customDataGridViewHeader1);
            this.Controls.Add(this.dgvUserList);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.customTitlebar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmUserList";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "User Management";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmUserList_FormClosing);
            this.pnlUserSubType.ResumeLayout(false);
            this.pnlUserSubType.PerformLayout();
            this.pnlAgent.ResumeLayout(false);
            this.pnlAgent.PerformLayout();
            this.pnlSubAgent.ResumeLayout(false);
            this.pnlSubAgent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserList)).EndInit();
            this.pnlUserType.ResumeLayout(false);
            this.pnlUserType.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private CustomTitlebar customTitlebar1;
        private System.Windows.Forms.Button btnClose;
        private CustomComboBoxDropDownList cmbOutlet;
        private System.Windows.Forms.Label lblOutlet;
        private CustomComboBoxDropDownList cmbAgent;
        private System.Windows.Forms.Label lblAgent;
        private CustomComboBoxDropDownList cmbUserType;
        private System.Windows.Forms.Label lblUserType;
        private CustomComboBoxDropDownList cmbUserSubType;
        private System.Windows.Forms.Label lblUserSubtype;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAddBranchUser;
        private System.Windows.Forms.Button btnAddAgentUser;
        private System.Windows.Forms.Button btnAddSubAgentUser;
        private System.Windows.Forms.Panel pnlUserSubType;
        private System.Windows.Forms.Panel pnlAgent;
        private System.Windows.Forms.Panel pnlSubAgent;
        private System.Windows.Forms.DataGridView dgvUserList;
        private System.Windows.Forms.Panel pnlUserType;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.DataGridViewLinkColumn columnTransfer;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColIndividualId;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColUserId;
        private System.Windows.Forms.DataGridViewLinkColumn grdColShowDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColUserStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColMobileNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColUserType;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColFullName;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColUserName;
        private Module.Common.Exporter.CustomDataGridViewHeader customDataGridViewHeader1;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColFilter;
    }
}