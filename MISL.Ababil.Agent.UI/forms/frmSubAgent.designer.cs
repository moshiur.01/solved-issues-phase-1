﻿namespace MISL.Ababil.Agent.UI.forms
{
    partial class frmSubAgent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSubAgent));
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSubAgentName = new System.Windows.Forms.TextBox();
            this.txtMobile = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.btnWebCam = new System.Windows.Forms.Button();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBoxsubAgent = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbCountryBus = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cmbDistrictBus = new System.Windows.Forms.ComboBox();
            this.cmbDivisionBus = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbThanaBus = new System.Windows.Forms.ComboBox();
            this.cmbPostCodeBus = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.txtAddressLineTwo = new System.Windows.Forms.TextBox();
            this.txtAddressLineOne = new System.Windows.Forms.TextBox();
            this.cmbAgentName = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.lblImageError = new System.Windows.Forms.Label();
            this.cmbAgentAccountNo = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbBranch = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.mandatoryMark6 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark5 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark4 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark3 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark2 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark1 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.mandatoryMark10 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark8 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark7 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.label20 = new System.Windows.Forms.Label();
            this.cmbOutletStatus = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.btnAddFieldOfficer = new System.Windows.Forms.Button();
            this.dgvFieldOfficer = new System.Windows.Forms.DataGridView();
            this.grdColUserName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdColUserFullName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdColMobileNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdColDeleteUser = new System.Windows.Forms.DataGridViewImageColumn();
            this.grdColUserID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbFieldOfficer = new System.Windows.Forms.ComboBox();
            this.lblMonitoringFiendOfficer = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tabPageUsers = new System.Windows.Forms.TabPage();
            this.gvUserInfo = new System.Windows.Forms.DataGridView();
            this.label18 = new System.Windows.Forms.Label();
            this.btnNewUser = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxsubAgent)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFieldOfficer)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tabPageUsers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvUserInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(58, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Outlet Name :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(76, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Address :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(91, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Email :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(447, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Agent account no. :";
            // 
            // txtSubAgentName
            // 
            this.txtSubAgentName.Location = new System.Drawing.Point(137, 82);
            this.txtSubAgentName.Name = "txtSubAgentName";
            this.txtSubAgentName.Size = new System.Drawing.Size(237, 20);
            this.txtSubAgentName.TabIndex = 7;
            // 
            // txtMobile
            // 
            this.txtMobile.Location = new System.Drawing.Point(135, 27);
            this.txtMobile.Name = "txtMobile";
            this.txtMobile.Size = new System.Drawing.Size(237, 20);
            this.txtMobile.TabIndex = 1;
            this.txtMobile.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMobile_KeyPress);
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(135, 52);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(237, 20);
            this.txtEmail.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.btnWebCam);
            this.groupBox1.Controls.Add(this.btnBrowse);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.pictureBoxsubAgent);
            this.groupBox1.Enabled = false;
            this.groupBox1.Location = new System.Drawing.Point(17, 484);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(266, 228);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "hidden";
            this.groupBox1.Visible = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(811, 48);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(17, 24);
            this.label16.TabIndex = 28;
            this.label16.Text = "*";
            // 
            // btnWebCam
            // 
            this.btnWebCam.Location = new System.Drawing.Point(157, 189);
            this.btnWebCam.Name = "btnWebCam";
            this.btnWebCam.Size = new System.Drawing.Size(75, 23);
            this.btnWebCam.TabIndex = 34;
            this.btnWebCam.Text = "Web Cam";
            this.btnWebCam.UseVisualStyleBackColor = true;
            this.btnWebCam.Click += new System.EventHandler(this.btnWebCam_Click);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(60, 189);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(91, 23);
            this.btnBrowse.TabIndex = 33;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "Photo :";
            // 
            // pictureBoxsubAgent
            // 
            this.pictureBoxsubAgent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxsubAgent.Location = new System.Drawing.Point(60, 23);
            this.pictureBoxsubAgent.Name = "pictureBoxsubAgent";
            this.pictureBoxsubAgent.Size = new System.Drawing.Size(173, 161);
            this.pictureBoxsubAgent.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxsubAgent.TabIndex = 15;
            this.pictureBoxsubAgent.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(798, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 24);
            this.label10.TabIndex = 5;
            this.label10.Text = "*";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(378, 58);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 24);
            this.label9.TabIndex = 5;
            this.label9.Text = "*";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(378, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 24);
            this.label8.TabIndex = 2;
            this.label8.Text = "*";
            // 
            // cmbCountryBus
            // 
            this.cmbCountryBus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCountryBus.FormattingEnabled = true;
            this.cmbCountryBus.Location = new System.Drawing.Point(135, 262);
            this.cmbCountryBus.Name = "cmbCountryBus";
            this.cmbCountryBus.Size = new System.Drawing.Size(237, 21);
            this.cmbCountryBus.TabIndex = 17;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(78, 265);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "Country :";
            // 
            // cmbDistrictBus
            // 
            this.cmbDistrictBus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDistrictBus.FormattingEnabled = true;
            this.cmbDistrictBus.Location = new System.Drawing.Point(135, 177);
            this.cmbDistrictBus.Name = "cmbDistrictBus";
            this.cmbDistrictBus.Size = new System.Drawing.Size(237, 21);
            this.cmbDistrictBus.TabIndex = 11;
            this.cmbDistrictBus.SelectedIndexChanged += new System.EventHandler(this.cmbDistrictBus_SelectedIndexChanged);
            // 
            // cmbDivisionBus
            // 
            this.cmbDivisionBus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDivisionBus.FormattingEnabled = true;
            this.cmbDivisionBus.Location = new System.Drawing.Point(135, 149);
            this.cmbDivisionBus.Name = "cmbDivisionBus";
            this.cmbDivisionBus.Size = new System.Drawing.Size(237, 21);
            this.cmbDivisionBus.TabIndex = 9;
            this.cmbDivisionBus.SelectedIndexChanged += new System.EventHandler(this.cmbDivitionBus_SelectedIndexChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(65, 237);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(62, 13);
            this.label15.TabIndex = 14;
            this.label15.Text = "Post Code :";
            // 
            // cmbThanaBus
            // 
            this.cmbThanaBus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbThanaBus.FormattingEnabled = true;
            this.cmbThanaBus.Location = new System.Drawing.Point(135, 205);
            this.cmbThanaBus.Name = "cmbThanaBus";
            this.cmbThanaBus.Size = new System.Drawing.Size(237, 21);
            this.cmbThanaBus.TabIndex = 13;
            this.cmbThanaBus.SelectedIndexChanged += new System.EventHandler(this.cmbThanaBus_SelectedIndexChanged);
            // 
            // cmbPostCodeBus
            // 
            this.cmbPostCodeBus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPostCodeBus.FormattingEnabled = true;
            this.cmbPostCodeBus.Location = new System.Drawing.Point(135, 233);
            this.cmbPostCodeBus.Name = "cmbPostCodeBus";
            this.cmbPostCodeBus.Size = new System.Drawing.Size(237, 21);
            this.cmbPostCodeBus.TabIndex = 15;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(82, 181);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(45, 13);
            this.label30.TabIndex = 10;
            this.label30.Text = "District :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(77, 153);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(50, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "Division :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(83, 210);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "Thana :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(62, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Outlet Code :";
            // 
            // txtCode
            // 
            this.txtCode.BackColor = System.Drawing.Color.White;
            this.txtCode.Location = new System.Drawing.Point(137, 56);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(237, 20);
            this.txtCode.TabIndex = 4;
            // 
            // txtAddressLineTwo
            // 
            this.txtAddressLineTwo.Location = new System.Drawing.Point(135, 121);
            this.txtAddressLineTwo.Name = "txtAddressLineTwo";
            this.txtAddressLineTwo.Size = new System.Drawing.Size(237, 20);
            this.txtAddressLineTwo.TabIndex = 7;
            // 
            // txtAddressLineOne
            // 
            this.txtAddressLineOne.Location = new System.Drawing.Point(135, 93);
            this.txtAddressLineOne.Name = "txtAddressLineOne";
            this.txtAddressLineOne.Size = new System.Drawing.Size(237, 20);
            this.txtAddressLineOne.TabIndex = 6;
            // 
            // cmbAgentName
            // 
            this.cmbAgentName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAgentName.FormattingEnabled = true;
            this.cmbAgentName.Location = new System.Drawing.Point(137, 19);
            this.cmbAgentName.Name = "cmbAgentName";
            this.cmbAgentName.Size = new System.Drawing.Size(237, 21);
            this.cmbAgentName.TabIndex = 1;
            this.cmbAgentName.SelectedIndexChanged += new System.EventHandler(this.cmbAgentName_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(59, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Agent Name :";
            // 
            // lblImageError
            // 
            this.lblImageError.AutoSize = true;
            this.lblImageError.Location = new System.Drawing.Point(131, 61);
            this.lblImageError.Name = "lblImageError";
            this.lblImageError.Size = new System.Drawing.Size(0, 13);
            this.lblImageError.TabIndex = 4;
            // 
            // cmbAgentAccountNo
            // 
            this.cmbAgentAccountNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAgentAccountNo.FormattingEnabled = true;
            this.cmbAgentAccountNo.Location = new System.Drawing.Point(554, 19);
            this.cmbAgentAccountNo.Name = "cmbAgentAccountNo";
            this.cmbAgentAccountNo.Size = new System.Drawing.Size(237, 21);
            this.cmbAgentAccountNo.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(85, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Mobile :";
            // 
            // cmbBranch
            // 
            this.cmbBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBranch.FormattingEnabled = true;
            this.cmbBranch.Location = new System.Drawing.Point(137, 109);
            this.cmbBranch.Name = "cmbBranch";
            this.cmbBranch.Size = new System.Drawing.Size(237, 21);
            this.cmbBranch.Sorted = true;
            this.cmbBranch.TabIndex = 9;
            this.cmbBranch.SelectedIndexChanged += new System.EventHandler(this.cmbBranch_SelectedIndexChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(32, 112);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(99, 13);
            this.label17.TabIndex = 8;
            this.label17.Text = "Monitoring Branch :";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPageUsers);
            this.tabControl1.Location = new System.Drawing.Point(7, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(855, 477);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControl1_Selecting);
            this.tabControl1.TabIndexChanged += new System.EventHandler(this.tabControl1_TabIndexChanged);
            this.tabControl1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tabControl1_MouseClick);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(847, 451);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General Information";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.mandatoryMark6);
            this.groupBox4.Controls.Add(this.mandatoryMark5);
            this.groupBox4.Controls.Add(this.mandatoryMark4);
            this.groupBox4.Controls.Add(this.mandatoryMark3);
            this.groupBox4.Controls.Add(this.mandatoryMark2);
            this.groupBox4.Controls.Add(this.mandatoryMark1);
            this.groupBox4.Controls.Add(this.txtAddressLineOne);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.txtAddressLineTwo);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.lblImageError);
            this.groupBox4.Controls.Add(this.cmbCountryBus);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.cmbPostCodeBus);
            this.groupBox4.Controls.Add(this.cmbDistrictBus);
            this.groupBox4.Controls.Add(this.txtEmail);
            this.groupBox4.Controls.Add(this.cmbThanaBus);
            this.groupBox4.Controls.Add(this.txtMobile);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.cmbDivisionBus);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Location = new System.Drawing.Point(426, 69);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(415, 376);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Contact Info";
            // 
            // mandatoryMark6
            // 
            this.mandatoryMark6.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark6.Location = new System.Drawing.Point(375, 234);
            this.mandatoryMark6.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark6.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark6.Name = "mandatoryMark6";
            this.mandatoryMark6.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark6.TabIndex = 25;
            // 
            // mandatoryMark5
            // 
            this.mandatoryMark5.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark5.Location = new System.Drawing.Point(375, 206);
            this.mandatoryMark5.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark5.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark5.Name = "mandatoryMark5";
            this.mandatoryMark5.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark5.TabIndex = 24;
            // 
            // mandatoryMark4
            // 
            this.mandatoryMark4.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark4.Location = new System.Drawing.Point(375, 180);
            this.mandatoryMark4.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark4.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark4.Name = "mandatoryMark4";
            this.mandatoryMark4.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark4.TabIndex = 23;
            // 
            // mandatoryMark3
            // 
            this.mandatoryMark3.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark3.Location = new System.Drawing.Point(375, 152);
            this.mandatoryMark3.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark3.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark3.Name = "mandatoryMark3";
            this.mandatoryMark3.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark3.TabIndex = 22;
            // 
            // mandatoryMark2
            // 
            this.mandatoryMark2.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark2.Location = new System.Drawing.Point(375, 93);
            this.mandatoryMark2.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.Name = "mandatoryMark2";
            this.mandatoryMark2.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.TabIndex = 21;
            // 
            // mandatoryMark1
            // 
            this.mandatoryMark1.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark1.Location = new System.Drawing.Point(375, 27);
            this.mandatoryMark1.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.Name = "mandatoryMark1";
            this.mandatoryMark1.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.TabIndex = 18;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.mandatoryMark10);
            this.groupBox3.Controls.Add(this.mandatoryMark8);
            this.groupBox3.Controls.Add(this.mandatoryMark7);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.cmbOutletStatus);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.btnAddFieldOfficer);
            this.groupBox3.Controls.Add(this.dgvFieldOfficer);
            this.groupBox3.Controls.Add(this.cmbFieldOfficer);
            this.groupBox3.Controls.Add(this.txtCode);
            this.groupBox3.Controls.Add(this.lblMonitoringFiendOfficer);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.cmbBranch);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.txtSubAgentName);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Location = new System.Drawing.Point(6, 69);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(402, 376);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Outlet";
            // 
            // mandatoryMark10
            // 
            this.mandatoryMark10.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark10.Location = new System.Drawing.Point(377, 206);
            this.mandatoryMark10.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark10.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark10.Name = "mandatoryMark10";
            this.mandatoryMark10.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark10.TabIndex = 26;
            // 
            // mandatoryMark8
            // 
            this.mandatoryMark8.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark8.Location = new System.Drawing.Point(378, 111);
            this.mandatoryMark8.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark8.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark8.Name = "mandatoryMark8";
            this.mandatoryMark8.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark8.TabIndex = 26;
            // 
            // mandatoryMark7
            // 
            this.mandatoryMark7.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark7.Location = new System.Drawing.Point(378, 82);
            this.mandatoryMark7.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark7.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark7.Name = "mandatoryMark7";
            this.mandatoryMark7.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark7.TabIndex = 26;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(378, 27);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(17, 24);
            this.label20.TabIndex = 2;
            this.label20.Text = "*";
            // 
            // cmbOutletStatus
            // 
            this.cmbOutletStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOutletStatus.FormattingEnabled = true;
            this.cmbOutletStatus.Location = new System.Drawing.Point(137, 26);
            this.cmbOutletStatus.Name = "cmbOutletStatus";
            this.cmbOutletStatus.Size = new System.Drawing.Size(237, 21);
            this.cmbOutletStatus.TabIndex = 1;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(56, 29);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(74, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Outlet Status :";
            // 
            // btnAddFieldOfficer
            // 
            this.btnAddFieldOfficer.Location = new System.Drawing.Point(299, 176);
            this.btnAddFieldOfficer.Name = "btnAddFieldOfficer";
            this.btnAddFieldOfficer.Size = new System.Drawing.Size(75, 23);
            this.btnAddFieldOfficer.TabIndex = 12;
            this.btnAddFieldOfficer.Text = "Add to List";
            this.btnAddFieldOfficer.UseVisualStyleBackColor = true;
            this.btnAddFieldOfficer.Click += new System.EventHandler(this.btnAddFieldOfficer_Click);
            // 
            // dgvFieldOfficer
            // 
            this.dgvFieldOfficer.AllowUserToAddRows = false;
            this.dgvFieldOfficer.AllowUserToDeleteRows = false;
            this.dgvFieldOfficer.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvFieldOfficer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFieldOfficer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.grdColUserName,
            this.grdColUserFullName,
            this.grdColMobileNo,
            this.grdColDeleteUser,
            this.grdColUserID});
            this.dgvFieldOfficer.Location = new System.Drawing.Point(13, 206);
            this.dgvFieldOfficer.Name = "dgvFieldOfficer";
            this.dgvFieldOfficer.RowHeadersVisible = false;
            this.dgvFieldOfficer.Size = new System.Drawing.Size(360, 159);
            this.dgvFieldOfficer.TabIndex = 13;
            this.dgvFieldOfficer.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFieldOfficer_CellContentClick);
            // 
            // grdColUserName
            // 
            this.grdColUserName.HeaderText = "User ID";
            this.grdColUserName.Name = "grdColUserName";
            this.grdColUserName.Width = 85;
            // 
            // grdColUserFullName
            // 
            this.grdColUserFullName.HeaderText = "User Name";
            this.grdColUserFullName.Name = "grdColUserFullName";
            this.grdColUserFullName.Width = 140;
            // 
            // grdColMobileNo
            // 
            this.grdColMobileNo.HeaderText = "Contact No.";
            this.grdColMobileNo.Name = "grdColMobileNo";
            this.grdColMobileNo.Width = 110;
            // 
            // grdColDeleteUser
            // 
            this.grdColDeleteUser.HeaderText = "";
            this.grdColDeleteUser.Image = global::MISL.Ababil.Agent.UI.Properties.Resources.del;
            this.grdColDeleteUser.MinimumWidth = 2;
            this.grdColDeleteUser.Name = "grdColDeleteUser";
            this.grdColDeleteUser.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.grdColDeleteUser.Width = 20;
            // 
            // grdColUserID
            // 
            this.grdColUserID.HeaderText = "UserID";
            this.grdColUserID.MinimumWidth = 2;
            this.grdColUserID.Name = "grdColUserID";
            this.grdColUserID.Visible = false;
            this.grdColUserID.Width = 2;
            // 
            // cmbFieldOfficer
            // 
            this.cmbFieldOfficer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFieldOfficer.FormattingEnabled = true;
            this.cmbFieldOfficer.Location = new System.Drawing.Point(137, 148);
            this.cmbFieldOfficer.Name = "cmbFieldOfficer";
            this.cmbFieldOfficer.Size = new System.Drawing.Size(237, 21);
            this.cmbFieldOfficer.Sorted = true;
            this.cmbFieldOfficer.TabIndex = 11;
            // 
            // lblMonitoringFiendOfficer
            // 
            this.lblMonitoringFiendOfficer.AutoSize = true;
            this.lblMonitoringFiendOfficer.Location = new System.Drawing.Point(10, 151);
            this.lblMonitoringFiendOfficer.Name = "lblMonitoringFiendOfficer";
            this.lblMonitoringFiendOfficer.Size = new System.Drawing.Size(121, 13);
            this.lblMonitoringFiendOfficer.TabIndex = 10;
            this.lblMonitoringFiendOfficer.Text = "Monitoring Field Officer :";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmbAgentName);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cmbAgentAccountNo);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Location = new System.Drawing.Point(6, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(835, 52);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Agent";
            // 
            // tabPageUsers
            // 
            this.tabPageUsers.Controls.Add(this.gvUserInfo);
            this.tabPageUsers.Controls.Add(this.label18);
            this.tabPageUsers.Controls.Add(this.btnNewUser);
            this.tabPageUsers.Location = new System.Drawing.Point(4, 22);
            this.tabPageUsers.Name = "tabPageUsers";
            this.tabPageUsers.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageUsers.Size = new System.Drawing.Size(847, 451);
            this.tabPageUsers.TabIndex = 1;
            this.tabPageUsers.Text = "User Information";
            this.tabPageUsers.UseVisualStyleBackColor = true;
            // 
            // gvUserInfo
            // 
            this.gvUserInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvUserInfo.Location = new System.Drawing.Point(6, 41);
            this.gvUserInfo.Name = "gvUserInfo";
            this.gvUserInfo.Size = new System.Drawing.Size(674, 235);
            this.gvUserInfo.TabIndex = 1;
            this.gvUserInfo.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvUserInfo_CellContentClick);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(701, 41);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 24);
            this.label18.TabIndex = 2;
            this.label18.Text = "*";
            // 
            // btnNewUser
            // 
            this.btnNewUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewUser.Location = new System.Drawing.Point(236, 4);
            this.btnNewUser.Name = "btnNewUser";
            this.btnNewUser.Size = new System.Drawing.Size(116, 31);
            this.btnNewUser.TabIndex = 0;
            this.btnNewUser.Text = "New user";
            this.btnNewUser.UseVisualStyleBackColor = true;
            this.btnNewUser.Click += new System.EventHandler(this.btnNewUser_Click);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(612, 485);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(83, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Location = new System.Drawing.Point(701, 485);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 2;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(782, 485);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 250;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmSubAgent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 521);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmSubAgent";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Outlet";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSubAgent_FormClosing);
            this.Load += new System.EventHandler(this.frmSubAgent_Load);
            this.Shown += new System.EventHandler(this.frmSubAgent_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxsubAgent)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFieldOfficer)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPageUsers.ResumeLayout(false);
            this.tabPageUsers.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvUserInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSubAgentName;
        private System.Windows.Forms.TextBox txtMobile;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBoxsubAgent;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPageUsers;
        private System.Windows.Forms.Button btnNewUser;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ComboBox cmbAgentAccountNo;
        private System.Windows.Forms.Button btnWebCam;
        private System.Windows.Forms.Label lblImageError;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbAgentName;
        private System.Windows.Forms.TextBox txtAddressLineTwo;
        private System.Windows.Forms.TextBox txtAddressLineOne;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.ComboBox cmbDistrictBus;
        private System.Windows.Forms.ComboBox cmbDivisionBus;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cmbThanaBus;
        private System.Windows.Forms.ComboBox cmbPostCodeBus;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cmbCountryBus;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cmbBranch;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DataGridView gvUserInfo;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ComboBox cmbFieldOfficer;
        private System.Windows.Forms.Label lblMonitoringFiendOfficer;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView dgvFieldOfficer;
        private System.Windows.Forms.Button btnAddFieldOfficer;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdColUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdColUserFullName;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdColMobileNo;
        private System.Windows.Forms.DataGridViewImageColumn grdColDeleteUser;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdColUserID;
        private System.Windows.Forms.ComboBox cmbOutletStatus;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private CustomControls.MandatoryMark mandatoryMark6;
        private CustomControls.MandatoryMark mandatoryMark5;
        private CustomControls.MandatoryMark mandatoryMark4;
        private CustomControls.MandatoryMark mandatoryMark3;
        private CustomControls.MandatoryMark mandatoryMark2;
        private CustomControls.MandatoryMark mandatoryMark1;
        private CustomControls.MandatoryMark mandatoryMark7;
        private CustomControls.MandatoryMark mandatoryMark10;
        private CustomControls.MandatoryMark mandatoryMark8;
    }
}