﻿using MetroFramework.Forms;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MISL.Ababil.Agent.UI.forms
{
    public partial class frmOutletUserCashLimitInfo : CustomForm
    {
        private Packet _packet;
        private GUI _gui = new GUI();
        private List<AgentInformation> _objAgentInfoList = new List<AgentInformation>();
        private AgentServices _objAgentServices = new AgentServices();
        private AgentInformation _agentInformation = new AgentInformation();
        private AgentServices _agentServices = new AgentServices();

        List<SubAgentInformation> subAgentList = new List<SubAgentInformation>();       // WALI :: 14-Jan-2016
        private SubAgentServices _outletServices = new SubAgentServices();
        List<SubAgentUser> subAgentUserList = new List<SubAgentUser>();
        SubAgentInformation _currentSubagentInfo;
        OutletUserTotalLimit _outletUserTotalLimit;

        public frmOutletUserCashLimitInfo(Packet packet)
        {
            try
            {
                _packet = packet;
                InitializeComponent();
                GetSetupData();
                //controlActivity();
                ConfigUIEnhancement();
                SetControlAccessibility();
            }
            catch (Exception exp) { Message.showError(exp.Message); }
        }

        private void SetControlAccessibility()
        {
            try
            {
                bool isAdminUser = (SessionInfo.userBasicInformation.userType == UserType.Admin) ? true : false;

                if (SessionInfo.userBasicInformation.userCategory == UserCategory.AgentUser)
                {
                    #region For Agent User
                    cmbAgentName.SelectedValue = UtilityServices.getCurrentAgent().id;
                    subAgentList = _agentServices.GetSubagentsByAgentId((long)cmbAgentName.SelectedValue);
                    setSubagent();
                    cmbAgentName.Enabled = false;
                    #endregion
                }
            }
            catch (Exception exp) { throw new Exception(exp.Message); }
        }

        public void ConfigUIEnhancement()
        {
            //_gui = new GUI(this);
            //_gui.Config(ref cmbAgentName, ValidCheck.VALIDATIONTYPES.COMBOBOX_EMPTY, null);
            //_gui.Config(ref cmbSubAgnetName, ValidCheck.VALIDATIONTYPES.COMBOBOX_EMPTY, null);
        }

        private void setSubagent()
        {
            if (subAgentList != null)
            {
                BindingSource bs = new BindingSource();
                bs.DataSource = subAgentList;

                UtilityServices.fillComboBox(cmbSubAgnetName, bs, "name", "id");
                cmbSubAgnetName.SelectedIndex = -1;
            }
        }

        private void GetSetupData()
        {
            try
            {
                setAgentList();
            }
            catch { }
        }

        private void setAgentList()
        {
            _objAgentInfoList = _objAgentServices.getAgentInfoBranchWise();
            if (SessionInfo.userBasicInformation.userCategory == UserCategory.BranchUser &&
              SessionInfo.userBasicInformation.userType == UserType.Operational)
            {
                for (int i = 0; i < _objAgentInfoList.Count; i++)
                {
                    if (_objAgentInfoList[i].id == 1)
                    {
                        _objAgentInfoList.RemoveAt(i);
                    }
                }
            }
            BindingSource bs = new BindingSource();
            bs.DataSource = _objAgentInfoList;

            AgentInformation agSelect = new AgentInformation();
            agSelect.businessName = "(Select)";
            _objAgentInfoList.Insert(0, agSelect);

            UtilityServices.fillComboBox(cmbAgentName, bs, "businessName", "id");
            cmbAgentName.SelectedIndex = 0;
          
        }

        private void cmbAgentName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!cmbAgentName.Focused)
            {
                return;
            }

            try
            {
                if (cmbAgentName.SelectedIndex > 0)
                {
                    //_agentInformation = _agentServices.getAgentInfoById(cmbAgentName.SelectedValue.ToString());
                    subAgentList = _agentServices.GetSubagentsByAgentId((long)cmbAgentName.SelectedValue);         // WALI :: 14-Jan-2016
                }
                if (cmbAgentName.SelectedIndex < 1)
                {
                    cmbSubAgnetName.DataSource = null;
                    cmbSubAgnetName.Items.Clear();
                    return;
                }
            }

            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
            setSubagent();
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            if (cmbAgentName.SelectedIndex < 1)
            {
                Message.showError("Please select an Agent!");
                return;
            }
            if (cmbSubAgnetName.SelectedIndex < 0)
            {
                Message.showError("Please select an Outlet!");
                return;
            }

            SubAgentInformation subAgentInformation = subAgentList[cmbSubAgnetName.SelectedIndex];
           // subAgentInformation.id = long.Parse(cmbSubAgnetName.SelectedValue.ToString());

            frmOutletUserList frm = new frmOutletUserList(null, subAgentInformation);
            frm.ShowDialog(this);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmOutletUserCashLimitInfo_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void cmbSubAgnetName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!cmbSubAgnetName.Focused) return;

            try
            {
                _outletUserTotalLimit = _outletServices.GetSubAgentCashLimitSummaryBySubAgentId(long.Parse(cmbSubAgnetName.SelectedValue.ToString()));

                if (_outletUserTotalLimit != null)
                {
                    //decimal remainingLimit = (_outletUserTotalLimit.totalCashLimit ?? 0) - (_outletUserTotalLimit.totalCashUsage ?? 0);
                    //lblTotalLimitValue.Text = (_outletUserTotalLimit.totalCashLimit ?? 0).ToString("N", new CultureInfo("BN-BD")) + "  BDT";
                    //lblTotalCashinHandValue.Text = (_outletUserTotalLimit.totalCashUsage ?? 0).ToString("N", new CultureInfo("BN-BD")) + "  BDT";
                    //lblRemainingLimitValue.Text = remainingLimit.ToString("N", new CultureInfo("BN-BD")) + "  BDT";

                    decimal totalLimit = (_outletUserTotalLimit.totalCashLimit ?? 0) + (_outletUserTotalLimit.totalCashUsage ?? 0);
                    lblTotalLimitValue.Text = totalLimit.ToString("N", new CultureInfo("BN-BD")) + "  BDT";
                    lblTotalCashinHandValue.Text = (_outletUserTotalLimit.totalCashUsage ?? 0).ToString("N", new CultureInfo("BN-BD")) + "  BDT";
                    lblRemainingLimitValue.Text = (_outletUserTotalLimit.totalCashLimit ?? 0).ToString("N", new CultureInfo("BN-BD")) + "  BDT";
                }
            }
            catch (Exception exp)
            { Message.showError(exp.Message); }
        }
    }
}