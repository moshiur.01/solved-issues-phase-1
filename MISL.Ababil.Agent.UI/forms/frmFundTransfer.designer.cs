﻿namespace MISL.Ababil.Agent.UI.forms
{
    partial class frmFundTransfer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFundTransfer));
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFromAccount = new System.Windows.Forms.TextBox();
            this.txtToAccount = new System.Windows.Forms.TextBox();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.btnDotransfer = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblToAccountName = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblFromAccountName = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblRequiredFingerPrint = new System.Windows.Forms.Label();
            this.fingerPrintGrid = new System.Windows.Forms.DataGridView();
            this.picFromConusmer = new System.Windows.Forms.PictureBox();
            this.btShowDestinationAcc = new System.Windows.Forms.Button();
            this.picToConusmer = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mandatoryMark1 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.lblToMobileNo = new System.Windows.Forms.Label();
            this.lblToConsumerTitle = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.mandatoryMark3 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark2 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.lblBalanceValue = new System.Windows.Forms.Label();
            this.lblBalance = new System.Windows.Forms.Label();
            this.lblInWords = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtrTotal = new System.Windows.Forms.TextBox();
            this.txtrCharge = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblFromMobileNo = new System.Windows.Forms.Label();
            this.lblFromConsumerTitle = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.titleLabel = new System.Windows.Forms.Label();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fingerPrintGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFromConusmer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picToConusmer)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(171, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "From Account :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 191);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Amount :";
            // 
            // txtFromAccount
            // 
            this.txtFromAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFromAccount.Location = new System.Drawing.Point(14, 53);
            this.txtFromAccount.MaxLength = 13;
            this.txtFromAccount.Name = "txtFromAccount";
            this.txtFromAccount.Size = new System.Drawing.Size(285, 38);
            this.txtFromAccount.TabIndex = 1;
            this.txtFromAccount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFromAccount_KeyPress);
            this.txtFromAccount.Leave += new System.EventHandler(this.txtFromAccount_Leave);
            // 
            // txtToAccount
            // 
            this.txtToAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtToAccount.Location = new System.Drawing.Point(13, 53);
            this.txtToAccount.MaxLength = 13;
            this.txtToAccount.Name = "txtToAccount";
            this.txtToAccount.Size = new System.Drawing.Size(285, 38);
            this.txtToAccount.TabIndex = 1;
            this.txtToAccount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtToAccount_KeyPress);
            this.txtToAccount.Leave += new System.EventHandler(this.txtToAccount_Leave);
            // 
            // txtAmount
            // 
            this.txtAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmount.Location = new System.Drawing.Point(93, 184);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtAmount.Size = new System.Drawing.Size(206, 31);
            this.txtAmount.TabIndex = 7;
            this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmount_KeyPress);
            this.txtAmount.Leave += new System.EventHandler(this.txtAmount_Leave);
            // 
            // btnDotransfer
            // 
            this.btnDotransfer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDotransfer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnDotransfer.FlatAppearance.BorderSize = 0;
            this.btnDotransfer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDotransfer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDotransfer.ForeColor = System.Drawing.Color.White;
            this.btnDotransfer.Location = new System.Drawing.Point(597, 600);
            this.btnDotransfer.Name = "btnDotransfer";
            this.btnDotransfer.Size = new System.Drawing.Size(132, 30);
            this.btnDotransfer.TabIndex = 6;
            this.btnDotransfer.Text = "Do Transfer";
            this.btnDotransfer.UseVisualStyleBackColor = false;
            this.btnDotransfer.Click += new System.EventHandler(this.btnDotransfer_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Account Title :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 99);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Account Title:";
            // 
            // lblToAccountName
            // 
            this.lblToAccountName.AutoSize = true;
            this.lblToAccountName.Location = new System.Drawing.Point(143, 90);
            this.lblToAccountName.Name = "lblToAccountName";
            this.lblToAccountName.Size = new System.Drawing.Size(0, 13);
            this.lblToAccountName.TabIndex = 3;
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(735, 600);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(91, 30);
            this.btnClear.TabIndex = 7;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(832, 600);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(91, 30);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblFromAccountName
            // 
            this.lblFromAccountName.AutoSize = true;
            this.lblFromAccountName.Location = new System.Drawing.Point(144, 44);
            this.lblFromAccountName.Name = "lblFromAccountName";
            this.lblFromAccountName.Size = new System.Drawing.Size(0, 13);
            this.lblFromAccountName.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblRequiredFingerPrint);
            this.groupBox2.Controls.Add(this.fingerPrintGrid);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(13, 373);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(538, 158);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "FingerPrint Information";
            // 
            // lblRequiredFingerPrint
            // 
            this.lblRequiredFingerPrint.AutoSize = true;
            this.lblRequiredFingerPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRequiredFingerPrint.Location = new System.Drawing.Point(241, 17);
            this.lblRequiredFingerPrint.Name = "lblRequiredFingerPrint";
            this.lblRequiredFingerPrint.Size = new System.Drawing.Size(41, 15);
            this.lblRequiredFingerPrint.TabIndex = 1;
            this.lblRequiredFingerPrint.Text = "label7";
            // 
            // fingerPrintGrid
            // 
            this.fingerPrintGrid.AllowUserToDeleteRows = false;
            this.fingerPrintGrid.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.fingerPrintGrid.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.fingerPrintGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.fingerPrintGrid.Location = new System.Drawing.Point(12, 38);
            this.fingerPrintGrid.Name = "fingerPrintGrid";
            this.fingerPrintGrid.ReadOnly = true;
            this.fingerPrintGrid.Size = new System.Drawing.Size(515, 110);
            this.fingerPrintGrid.TabIndex = 0;
            this.fingerPrintGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.fingerPrintGrid_CellContentClick);
            // 
            // picFromConusmer
            // 
            this.picFromConusmer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picFromConusmer.Location = new System.Drawing.Point(16, 22);
            this.picFromConusmer.Name = "picFromConusmer";
            this.picFromConusmer.Size = new System.Drawing.Size(204, 223);
            this.picFromConusmer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picFromConusmer.TabIndex = 11;
            this.picFromConusmer.TabStop = false;
            // 
            // btShowDestinationAcc
            // 
            this.btShowDestinationAcc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btShowDestinationAcc.Location = new System.Drawing.Point(854, 76);
            this.btShowDestinationAcc.Name = "btShowDestinationAcc";
            this.btShowDestinationAcc.Size = new System.Drawing.Size(31, 38);
            this.btShowDestinationAcc.TabIndex = 4;
            this.btShowDestinationAcc.Text = "...";
            this.btShowDestinationAcc.UseVisualStyleBackColor = true;
            // 
            // picToConusmer
            // 
            this.picToConusmer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picToConusmer.Location = new System.Drawing.Point(16, 22);
            this.picToConusmer.Name = "picToConusmer";
            this.picToConusmer.Size = new System.Drawing.Size(204, 223);
            this.picToConusmer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picToConusmer.TabIndex = 13;
            this.picToConusmer.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 25);
            this.label2.TabIndex = 0;
            this.label2.Text = "To Account :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.mandatoryMark1);
            this.groupBox1.Controls.Add(this.lblToMobileNo);
            this.groupBox1.Controls.Add(this.lblToConsumerTitle);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtToAccount);
            this.groupBox1.Location = new System.Drawing.Point(597, 50);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(326, 542);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Destination Account";
            // 
            // mandatoryMark1
            // 
            this.mandatoryMark1.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark1.Location = new System.Drawing.Point(300, 58);
            this.mandatoryMark1.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.Name = "mandatoryMark1";
            this.mandatoryMark1.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.TabIndex = 7;
            // 
            // lblToMobileNo
            // 
            this.lblToMobileNo.AutoSize = true;
            this.lblToMobileNo.Location = new System.Drawing.Point(89, 120);
            this.lblToMobileNo.Name = "lblToMobileNo";
            this.lblToMobileNo.Size = new System.Drawing.Size(73, 13);
            this.lblToMobileNo.TabIndex = 5;
            this.lblToMobileNo.Text = "00000000000";
            // 
            // lblToConsumerTitle
            // 
            this.lblToConsumerTitle.AutoSize = true;
            this.lblToConsumerTitle.Location = new System.Drawing.Point(89, 99);
            this.lblToConsumerTitle.Name = "lblToConsumerTitle";
            this.lblToConsumerTitle.Size = new System.Drawing.Size(73, 13);
            this.lblToConsumerTitle.TabIndex = 3;
            this.lblToConsumerTitle.Text = "00000000000";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 120);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Mobile No:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.picToConusmer);
            this.groupBox3.Location = new System.Drawing.Point(38, 148);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(236, 259);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Photo";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.mandatoryMark3);
            this.groupBox4.Controls.Add(this.mandatoryMark2);
            this.groupBox4.Controls.Add(this.lblBalanceValue);
            this.groupBox4.Controls.Add(this.lblBalance);
            this.groupBox4.Controls.Add(this.lblInWords);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.txtrTotal);
            this.groupBox4.Controls.Add(this.txtrCharge);
            this.groupBox4.Controls.Add(this.groupBox2);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.lblFromMobileNo);
            this.groupBox4.Controls.Add(this.lblFromConsumerTitle);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.txtAmount);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.txtFromAccount);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Location = new System.Drawing.Point(11, 50);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(579, 542);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Source Account";
            // 
            // mandatoryMark3
            // 
            this.mandatoryMark3.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark3.Location = new System.Drawing.Point(302, 189);
            this.mandatoryMark3.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark3.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark3.Name = "mandatoryMark3";
            this.mandatoryMark3.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark3.TabIndex = 18;
            // 
            // mandatoryMark2
            // 
            this.mandatoryMark2.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark2.Location = new System.Drawing.Point(302, 57);
            this.mandatoryMark2.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.Name = "mandatoryMark2";
            this.mandatoryMark2.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.TabIndex = 17;
            // 
            // lblBalanceValue
            // 
            this.lblBalanceValue.AutoSize = true;
            this.lblBalanceValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBalanceValue.Location = new System.Drawing.Point(90, 148);
            this.lblBalanceValue.Name = "lblBalanceValue";
            this.lblBalanceValue.Size = new System.Drawing.Size(107, 18);
            this.lblBalanceValue.TabIndex = 16;
            this.lblBalanceValue.Text = "00000000000";
            // 
            // lblBalance
            // 
            this.lblBalance.AutoSize = true;
            this.lblBalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBalance.Location = new System.Drawing.Point(10, 148);
            this.lblBalance.Name = "lblBalance";
            this.lblBalance.Size = new System.Drawing.Size(78, 18);
            this.lblBalance.TabIndex = 15;
            this.lblBalance.Text = "Balance :";
            // 
            // lblInWords
            // 
            this.lblInWords.Location = new System.Drawing.Point(90, 304);
            this.lblInWords.Name = "lblInWords";
            this.lblInWords.Size = new System.Drawing.Size(450, 53);
            this.lblInWords.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(31, 304);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "In Words :";
            // 
            // txtrTotal
            // 
            this.txtrTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrTotal.Location = new System.Drawing.Point(93, 257);
            this.txtrTotal.MaxLength = 10;
            this.txtrTotal.Name = "txtrTotal";
            this.txtrTotal.ReadOnly = true;
            this.txtrTotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtrTotal.Size = new System.Drawing.Size(206, 35);
            this.txtrTotal.TabIndex = 11;
            this.txtrTotal.TextChanged += new System.EventHandler(this.txtrTotal_TextChanged);
            // 
            // txtrCharge
            // 
            this.txtrCharge.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtrCharge.Location = new System.Drawing.Point(93, 221);
            this.txtrCharge.MaxLength = 10;
            this.txtrCharge.Name = "txtrCharge";
            this.txtrCharge.ReadOnly = true;
            this.txtrCharge.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtrCharge.Size = new System.Drawing.Size(206, 29);
            this.txtrCharge.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(29, 230);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 16);
            this.label8.TabIndex = 8;
            this.label8.Text = "Charge :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(11, 263);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 25);
            this.label9.TabIndex = 10;
            this.label9.Text = "Total :";
            // 
            // lblFromMobileNo
            // 
            this.lblFromMobileNo.AutoSize = true;
            this.lblFromMobileNo.Location = new System.Drawing.Point(90, 126);
            this.lblFromMobileNo.Name = "lblFromMobileNo";
            this.lblFromMobileNo.Size = new System.Drawing.Size(73, 13);
            this.lblFromMobileNo.TabIndex = 5;
            this.lblFromMobileNo.Text = "00000000000";
            // 
            // lblFromConsumerTitle
            // 
            this.lblFromConsumerTitle.AutoSize = true;
            this.lblFromConsumerTitle.Location = new System.Drawing.Point(90, 105);
            this.lblFromConsumerTitle.Name = "lblFromConsumerTitle";
            this.lblFromConsumerTitle.Size = new System.Drawing.Size(73, 13);
            this.lblFromConsumerTitle.TabIndex = 3;
            this.lblFromConsumerTitle.Text = "00000000000";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Mobile No :";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.picFromConusmer);
            this.groupBox5.Location = new System.Drawing.Point(331, 16);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(236, 274);
            this.groupBox5.TabIndex = 13;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Photo";
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.titleLabel.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.ForeColor = System.Drawing.Color.White;
            this.titleLabel.Location = new System.Drawing.Point(6, 6);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(126, 25);
            this.titleLabel.TabIndex = 103;
            this.titleLabel.Text = "Fund Transfer";
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = this.titleLabel;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(935, 39);
            this.customTitlebar1.TabIndex = 102;
            this.customTitlebar1.TabStop = false;
            // 
            // frmFundTransfer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(935, 640);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btShowDestinationAcc);
            this.Controls.Add(this.lblFromAccountName);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.lblToAccountName);
            this.Controls.Add(this.btnDotransfer);
            this.Controls.Add(this.groupBox4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmFundTransfer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fund Transfer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmFundTransfer_FormClosing);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fingerPrintGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFromConusmer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picToConusmer)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFromAccount;
        private System.Windows.Forms.TextBox txtToAccount;
        private System.Windows.Forms.TextBox txtAmount;
        private System.Windows.Forms.Button btnDotransfer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblToAccountName;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblFromAccountName;
        //private AxBIOPLUGINACTXLib.AxBioPlugInActX bio;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox picFromConusmer;
        private System.Windows.Forms.Button btShowDestinationAcc;
        private System.Windows.Forms.PictureBox picToConusmer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView fingerPrintGrid;
        private System.Windows.Forms.Label lblRequiredFingerPrint;
        private System.Windows.Forms.Label lblFromMobileNo;
        private System.Windows.Forms.Label lblFromConsumerTitle;
        private System.Windows.Forms.Label lblToMobileNo;
        private System.Windows.Forms.Label lblToConsumerTitle;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtrTotal;
        private System.Windows.Forms.TextBox txtrCharge;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblInWords;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblBalanceValue;
        private System.Windows.Forms.Label lblBalance;
        private CustomControls.MandatoryMark mandatoryMark1;
        private CustomControls.MandatoryMark mandatoryMark3;
        private CustomControls.MandatoryMark mandatoryMark2;
        private System.Windows.Forms.Label titleLabel;
        private Agent.CustomControls.CustomTitlebar customTitlebar1;
    }
}