﻿namespace MISL.Ababil.Agent.UI.forms.OutletUI
{
    partial class CashLimitListItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint5 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 40D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint6 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 60D);
            this.lblUserNameValue = new System.Windows.Forms.Label();
            this.lblCode = new System.Windows.Forms.Label();
            this.lblNameValue = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblDistrict = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.gbxUsage = new System.Windows.Forms.GroupBox();
            this.pbLgRemaining = new System.Windows.Forms.PictureBox();
            this.pbLgUsage = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.tglActive = new MetroFramework.Controls.MetroToggle();
            this.panelFooter = new System.Windows.Forms.Panel();
            this.btnOpen = new System.Windows.Forms.Button();
            this.lblActiveInactiveStatus = new System.Windows.Forms.Label();
            this.groupBoxCash = new System.Windows.Forms.GroupBox();
            this.btnCashLimit = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtCashLimit = new System.Windows.Forms.TextBox();
            this.tglCashLimit = new MetroFramework.Controls.MetroToggle();
            this.lblCHRemaining = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblCHRemainingValue = new System.Windows.Forms.Label();
            this.lblCHDailyLimit = new System.Windows.Forms.Label();
            this.lblCHUsage = new System.Windows.Forms.Label();
            this.lblCHLimit = new System.Windows.Forms.Label();
            this.lblCHUsageValue = new System.Windows.Forms.Label();
            this.crtCash = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.lblCHLimitValue = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.picUserPhoto = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            this.gbxUsage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLgRemaining)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLgUsage)).BeginInit();
            this.panelHeader.SuspendLayout();
            this.groupBoxCash.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.crtCash)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picUserPhoto)).BeginInit();
            this.SuspendLayout();
            // 
            // lblUserNameValue
            // 
            this.lblUserNameValue.AutoSize = true;
            this.lblUserNameValue.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserNameValue.ForeColor = System.Drawing.Color.White;
            this.lblUserNameValue.Location = new System.Drawing.Point(103, 2);
            this.lblUserNameValue.Name = "lblUserNameValue";
            this.lblUserNameValue.Size = new System.Drawing.Size(89, 20);
            this.lblUserNameValue.TabIndex = 1;
            this.lblUserNameValue.Text = "0000000000";
            this.lblUserNameValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCode
            // 
            this.lblCode.AutoSize = true;
            this.lblCode.Font = new System.Drawing.Font("Segoe UI Semilight", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCode.ForeColor = System.Drawing.Color.White;
            this.lblCode.Location = new System.Drawing.Point(9, 2);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(89, 20);
            this.lblCode.TabIndex = 4;
            this.lblCode.Text = "User Name :";
            this.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNameValue
            // 
            this.lblNameValue.AutoSize = true;
            this.lblNameValue.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameValue.ForeColor = System.Drawing.Color.White;
            this.lblNameValue.Location = new System.Drawing.Point(313, 2);
            this.lblNameValue.Name = "lblNameValue";
            this.lblNameValue.Size = new System.Drawing.Size(70, 20);
            this.lblNameValue.TabIndex = 5;
            this.lblNameValue.Text = "<Name>";
            this.lblNameValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Segoe UI Semilight", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ForeColor = System.Drawing.Color.White;
            this.lblName.Location = new System.Drawing.Point(227, 2);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(81, 20);
            this.lblName.TabIndex = 6;
            this.lblName.Text = "Full Name :";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(107, 18);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Address :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddress.Location = new System.Drawing.Point(161, 18);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(63, 13);
            this.lblAddress.TabIndex = 5;
            this.lblAddress.Text = "<address>";
            this.lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDistrict
            // 
            this.lblDistrict.AutoSize = true;
            this.lblDistrict.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDistrict.Location = new System.Drawing.Point(161, 36);
            this.lblDistrict.Name = "lblDistrict";
            this.lblDistrict.Size = new System.Drawing.Size(58, 13);
            this.lblDistrict.TabIndex = 5;
            this.lblDistrict.Text = "<district>";
            this.lblDistrict.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(112, 36);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 13);
            this.label16.TabIndex = 6;
            this.label16.Text = "District :";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhone.Location = new System.Drawing.Point(161, 54);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(55, 13);
            this.lblPhone.TabIndex = 5;
            this.lblPhone.Text = "<phone>";
            this.lblPhone.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(115, 54);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(46, 13);
            this.label18.TabIndex = 6;
            this.label18.Text = "Phone :";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbxUsage
            // 
            this.gbxUsage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxUsage.Controls.Add(this.pbLgRemaining);
            this.gbxUsage.Controls.Add(this.pbLgUsage);
            this.gbxUsage.Controls.Add(this.label8);
            this.gbxUsage.Controls.Add(this.label7);
            this.gbxUsage.Location = new System.Drawing.Point(361, 34);
            this.gbxUsage.Name = "gbxUsage";
            this.gbxUsage.Size = new System.Drawing.Size(126, 91);
            this.gbxUsage.TabIndex = 7;
            this.gbxUsage.TabStop = false;
            this.gbxUsage.Text = "Legend";
            // 
            // pbLgRemaining
            // 
            this.pbLgRemaining.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(140)))), ((int)(((byte)(240)))));
            this.pbLgRemaining.Location = new System.Drawing.Point(8, 38);
            this.pbLgRemaining.Name = "pbLgRemaining";
            this.pbLgRemaining.Size = new System.Drawing.Size(11, 11);
            this.pbLgRemaining.TabIndex = 11;
            this.pbLgRemaining.TabStop = false;
            // 
            // pbLgUsage
            // 
            this.pbLgUsage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(180)))), ((int)(((byte)(65)))));
            this.pbLgUsage.Location = new System.Drawing.Point(8, 22);
            this.pbLgUsage.Name = "pbLgUsage";
            this.pbLgUsage.Size = new System.Drawing.Size(11, 11);
            this.pbLgUsage.TabIndex = 11;
            this.pbLgUsage.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label8.Location = new System.Drawing.Point(21, 37);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 15);
            this.label8.TabIndex = 6;
            this.label8.Text = "Remaining Limit";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label7.Location = new System.Drawing.Point(21, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 15);
            this.label7.TabIndex = 6;
            this.label7.Text = "Cash in Hand";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelHeader
            // 
            this.panelHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(163)))), ((int)(((byte)(237)))));
            this.panelHeader.Controls.Add(this.lblName);
            this.panelHeader.Controls.Add(this.lblUserNameValue);
            this.panelHeader.Controls.Add(this.lblCode);
            this.panelHeader.Controls.Add(this.lblNameValue);
            this.panelHeader.Location = new System.Drawing.Point(5, 7);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(916, 25);
            this.panelHeader.TabIndex = 9;
            // 
            // tglActive
            // 
            this.tglActive.AutoSize = true;
            this.tglActive.BackColor = System.Drawing.Color.White;
            this.tglActive.Location = new System.Drawing.Point(197, 69);
            this.tglActive.Name = "tglActive";
            this.tglActive.Size = new System.Drawing.Size(80, 17);
            this.tglActive.TabIndex = 10;
            this.tglActive.Text = "Off";
            this.tglActive.UseCustomBackColor = true;
            this.tglActive.UseSelectable = true;
            this.tglActive.Visible = false;
            this.tglActive.CheckedChanged += new System.EventHandler(this.tglActive_CheckedChanged);
            // 
            // panelFooter
            // 
            this.panelFooter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelFooter.BackColor = System.Drawing.Color.Gainsboro;
            this.panelFooter.Location = new System.Drawing.Point(5, 128);
            this.panelFooter.Name = "panelFooter";
            this.panelFooter.Size = new System.Drawing.Size(916, 2);
            this.panelFooter.TabIndex = 0;
            // 
            // btnOpen
            // 
            this.btnOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOpen.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btnOpen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpen.Image = global::MISL.Ababil.Agent.UI.Properties.Resources.OpenRequestedPopup;
            this.btnOpen.Location = new System.Drawing.Point(856, 42);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(65, 81);
            this.btnOpen.TabIndex = 8;
            this.btnOpen.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // lblActiveInactiveStatus
            // 
            this.lblActiveInactiveStatus.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold);
            this.lblActiveInactiveStatus.Location = new System.Drawing.Point(160, 67);
            this.lblActiveInactiveStatus.Name = "lblActiveInactiveStatus";
            this.lblActiveInactiveStatus.Size = new System.Drawing.Size(64, 19);
            this.lblActiveInactiveStatus.TabIndex = 6;
            this.lblActiveInactiveStatus.Text = "Inactive";
            this.lblActiveInactiveStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblActiveInactiveStatus.Visible = false;
            this.lblActiveInactiveStatus.Click += new System.EventHandler(this.lblActiveInactiveStatus_Click);
            // 
            // groupBoxCash
            // 
            this.groupBoxCash.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxCash.Controls.Add(this.btnCashLimit);
            this.groupBoxCash.Controls.Add(this.panel1);
            this.groupBoxCash.Controls.Add(this.txtCashLimit);
            this.groupBoxCash.Controls.Add(this.tglCashLimit);
            this.groupBoxCash.Controls.Add(this.lblCHRemaining);
            this.groupBoxCash.Controls.Add(this.label4);
            this.groupBoxCash.Controls.Add(this.lblCHRemainingValue);
            this.groupBoxCash.Controls.Add(this.lblCHDailyLimit);
            this.groupBoxCash.Controls.Add(this.lblCHUsage);
            this.groupBoxCash.Controls.Add(this.lblCHLimit);
            this.groupBoxCash.Controls.Add(this.lblCHUsageValue);
            this.groupBoxCash.Controls.Add(this.crtCash);
            this.groupBoxCash.Controls.Add(this.lblCHLimitValue);
            this.groupBoxCash.Location = new System.Drawing.Point(491, 34);
            this.groupBoxCash.Name = "groupBoxCash";
            this.groupBoxCash.Size = new System.Drawing.Size(362, 91);
            this.groupBoxCash.TabIndex = 10;
            this.groupBoxCash.TabStop = false;
            this.groupBoxCash.Text = "Cash";
            // 
            // btnCashLimit
            // 
            this.btnCashLimit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnCashLimit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCashLimit.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btnCashLimit.FlatAppearance.BorderSize = 0;
            this.btnCashLimit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCashLimit.ForeColor = System.Drawing.Color.White;
            this.btnCashLimit.Location = new System.Drawing.Point(316, 64);
            this.btnCashLimit.Name = "btnCashLimit";
            this.btnCashLimit.Size = new System.Drawing.Size(42, 21);
            this.btnCashLimit.TabIndex = 8;
            this.btnCashLimit.Text = "Edit";
            this.btnCashLimit.UseVisualStyleBackColor = false;
            this.btnCashLimit.EnabledChanged += new System.EventHandler(this.btnCashLimit_EnabledChanged);
            this.btnCashLimit.Click += new System.EventHandler(this.btnCashLimit_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(201, 41);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(155, 1);
            this.panel1.TabIndex = 14;
            // 
            // txtCashLimit
            // 
            this.txtCashLimit.BackColor = System.Drawing.Color.Gainsboro;
            this.txtCashLimit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCashLimit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashLimit.Location = new System.Drawing.Point(230, 64);
            this.txtCashLimit.Name = "txtCashLimit";
            this.txtCashLimit.ReadOnly = true;
            this.txtCashLimit.Size = new System.Drawing.Size(86, 21);
            this.txtCashLimit.TabIndex = 10;
            this.txtCashLimit.Text = "0.00";
            this.txtCashLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tglCashLimit
            // 
            this.tglCashLimit.AutoSize = true;
            this.tglCashLimit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tglCashLimit.Location = new System.Drawing.Point(104, 31);
            this.tglCashLimit.Name = "tglCashLimit";
            this.tglCashLimit.Size = new System.Drawing.Size(80, 17);
            this.tglCashLimit.TabIndex = 10;
            this.tglCashLimit.Text = "Off";
            this.tglCashLimit.UseCustomBackColor = true;
            this.tglCashLimit.UseSelectable = true;
            this.tglCashLimit.CheckedChanged += new System.EventHandler(this.tglCashLimit_CheckedChanged);
            this.tglCashLimit.Click += new System.EventHandler(this.tglCashLimit_Click);
            // 
            // lblCHRemaining
            // 
            this.lblCHRemaining.AutoSize = true;
            this.lblCHRemaining.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.lblCHRemaining.Location = new System.Drawing.Point(197, 44);
            this.lblCHRemaining.Name = "lblCHRemaining";
            this.lblCHRemaining.Size = new System.Drawing.Size(95, 13);
            this.lblCHRemaining.TabIndex = 6;
            this.lblCHRemaining.Text = "Remaining Limit :";
            this.lblCHRemaining.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label4.Location = new System.Drawing.Point(73, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "Limit";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCHRemainingValue
            // 
            this.lblCHRemainingValue.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.lblCHRemainingValue.Location = new System.Drawing.Point(287, 44);
            this.lblCHRemainingValue.Name = "lblCHRemainingValue";
            this.lblCHRemainingValue.Size = new System.Drawing.Size(71, 12);
            this.lblCHRemainingValue.TabIndex = 6;
            this.lblCHRemainingValue.Text = "000000.00";
            this.lblCHRemainingValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCHDailyLimit
            // 
            this.lblCHDailyLimit.AutoSize = true;
            this.lblCHDailyLimit.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCHDailyLimit.Location = new System.Drawing.Point(147, 67);
            this.lblCHDailyLimit.Name = "lblCHDailyLimit";
            this.lblCHDailyLimit.Size = new System.Drawing.Size(40, 15);
            this.lblCHDailyLimit.TabIndex = 6;
            this.lblCHDailyLimit.Text = "Limit :";
            this.lblCHDailyLimit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCHUsage
            // 
            this.lblCHUsage.AutoSize = true;
            this.lblCHUsage.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.lblCHUsage.Location = new System.Drawing.Point(210, 25);
            this.lblCHUsage.Name = "lblCHUsage";
            this.lblCHUsage.Size = new System.Drawing.Size(82, 13);
            this.lblCHUsage.TabIndex = 6;
            this.lblCHUsage.Text = "Cash in Hand :";
            this.lblCHUsage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCHLimit
            // 
            this.lblCHLimit.AutoSize = true;
            this.lblCHLimit.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.lblCHLimit.Location = new System.Drawing.Point(255, 12);
            this.lblCHLimit.Name = "lblCHLimit";
            this.lblCHLimit.Size = new System.Drawing.Size(37, 13);
            this.lblCHLimit.TabIndex = 6;
            this.lblCHLimit.Text = "Limit :";
            this.lblCHLimit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCHUsageValue
            // 
            this.lblCHUsageValue.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.lblCHUsageValue.Location = new System.Drawing.Point(287, 25);
            this.lblCHUsageValue.Name = "lblCHUsageValue";
            this.lblCHUsageValue.Size = new System.Drawing.Size(71, 12);
            this.lblCHUsageValue.TabIndex = 6;
            this.lblCHUsageValue.Text = "000000.00";
            this.lblCHUsageValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // crtCash
            // 
            this.crtCash.BackColor = System.Drawing.Color.Transparent;
            chartArea3.Name = "ChartArea1";
            this.crtCash.ChartAreas.Add(chartArea3);
            this.crtCash.Location = new System.Drawing.Point(4, 14);
            this.crtCash.Name = "crtCash";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series3.IsVisibleInLegend = false;
            series3.Name = "Series2";
            series3.Points.Add(dataPoint5);
            series3.Points.Add(dataPoint6);
            this.crtCash.Series.Add(series3);
            this.crtCash.Size = new System.Drawing.Size(70, 69);
            this.crtCash.TabIndex = 10;
            this.crtCash.Text = "chart3";
            // 
            // lblCHLimitValue
            // 
            this.lblCHLimitValue.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.lblCHLimitValue.Location = new System.Drawing.Point(287, 12);
            this.lblCHLimitValue.Name = "lblCHLimitValue";
            this.lblCHLimitValue.Size = new System.Drawing.Size(71, 12);
            this.lblCHLimitValue.TabIndex = 6;
            this.lblCHLimitValue.Text = "000000.00";
            this.lblCHLimitValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.picUserPhoto);
            this.groupBox4.Controls.Add(this.lblActiveInactiveStatus);
            this.groupBox4.Controls.Add(this.tglActive);
            this.groupBox4.Controls.Add(this.lblAddress);
            this.groupBox4.Controls.Add(this.lblDistrict);
            this.groupBox4.Controls.Add(this.lblPhone);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Location = new System.Drawing.Point(5, 34);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(351, 91);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Info";
            // 
            // picUserPhoto
            // 
            this.picUserPhoto.Image = global::MISL.Ababil.Agent.UI.Properties.Resources.user_account___Copy;
            this.picUserPhoto.Location = new System.Drawing.Point(6, 12);
            this.picUserPhoto.Name = "picUserPhoto";
            this.picUserPhoto.Size = new System.Drawing.Size(75, 76);
            this.picUserPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picUserPhoto.TabIndex = 11;
            this.picUserPhoto.TabStop = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(116, 71);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "Status :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label14.Visible = false;
            // 
            // CashLimitListItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panelHeader);
            this.Controls.Add(this.groupBoxCash);
            this.Controls.Add(this.panelFooter);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.gbxUsage);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MinimumSize = new System.Drawing.Size(756, 101);
            this.Name = "CashLimitListItem";
            this.Size = new System.Drawing.Size(926, 133);
            this.gbxUsage.ResumeLayout(false);
            this.gbxUsage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLgRemaining)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLgUsage)).EndInit();
            this.panelHeader.ResumeLayout(false);
            this.panelHeader.PerformLayout();
            this.groupBoxCash.ResumeLayout(false);
            this.groupBoxCash.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.crtCash)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picUserPhoto)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox gbxUsage;
        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Panel panelFooter;
        public System.Windows.Forms.Label lblNameValue;
        public System.Windows.Forms.Label lblUserNameValue;
        public System.Windows.Forms.Label lblAddress;
        public System.Windows.Forms.Label lblDistrict;
        public System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pbLgUsage;
        private System.Windows.Forms.PictureBox pbLgRemaining;
        private System.Windows.Forms.Label label8;
        public MetroFramework.Controls.MetroToggle tglActive;
        private System.Windows.Forms.Label lblActiveInactiveStatus;
        private System.Windows.Forms.GroupBox groupBoxCash;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCashLimit;
        public System.Windows.Forms.TextBox txtCashLimit;
        public MetroFramework.Controls.MetroToggle tglCashLimit;
        private System.Windows.Forms.Label lblCHRemaining;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label lblCHRemainingValue;
        private System.Windows.Forms.Label lblCHDailyLimit;
        private System.Windows.Forms.Label lblCHUsage;
        private System.Windows.Forms.Label lblCHLimit;
        public System.Windows.Forms.Label lblCHUsageValue;
        public System.Windows.Forms.DataVisualization.Charting.Chart crtCash;
        public System.Windows.Forms.Label lblCHLimitValue;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label14;
        public System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.PictureBox picUserPhoto;
    }
}
