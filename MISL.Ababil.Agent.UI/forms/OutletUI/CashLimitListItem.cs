﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MetroFramework.Controls;
using MISL.Ababil.Agent.Communication;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using System.Windows.Forms.DataVisualization.Charting;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.UI.forms.ProgressUI;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.LocalStorageService;

namespace MISL.Ababil.Agent.UI.forms.OutletUI
{
    public partial class CashLimitListItem : UserControl
    {
        public string OutletId { get; set; }
        public string OutletUserId { get; set; }

        private SubAgentInformation _subAgentInformation;
        //private SubAgentUser _outletUserinformation;
        private SubAgentUserLimitDto _outletUserLimitDto;
        LimitUserType _limitUserType;


        //private bool isEditCredit = false;
        //private bool isEditDebit = false;
        private bool isEditCash = false;
        private object userService;
        
        public CashLimitListItem(Packet packet, LimitUserType limitUserType, object receivedObj)
        {
            InitializeComponent();
            SetControlSelectionEvents();

            _limitUserType = limitUserType;
            if (_limitUserType == LimitUserType.Outlet)
            {
                #region Outlet 
                _subAgentInformation = receivedObj as SubAgentInformation;

                lblUserNameValue.Text = _subAgentInformation.subAgentCode;
                lblNameValue.Text = _subAgentInformation.name;
                lblAddress.Text = _subAgentInformation.businessAddress.addressLineOne;
                lblDistrict.Text = _subAgentInformation.businessAddress.district.title;
                lblPhone.Text = _subAgentInformation.mobleNumber;
                OutletId = _subAgentInformation.id.ToString();

                //tglCreditLimit.Checked = _subAgentInformation.creditLimitApplicable ?? false;
                //tglDebitLimit.Checked = _subAgentInformation.debitLimitApplicable ?? false;
                tglCashLimit.Checked = _subAgentInformation.cashLimitApplicable ?? false;

                //SetCreditLimitEnable(_subAgentInformation.creditLimitApplicable ?? false);
                //SetDebitLimitEnable(_subAgentInformation.debitLimitApplicable ?? false);
                SetCashLimitEnable(_subAgentInformation.cashLimitApplicable ?? false);
                #endregion
            }
            else if (_limitUserType == LimitUserType.User)
            {
                #region Outlet User
                _outletUserLimitDto = receivedObj as SubAgentUserLimitDto;

                lblUserNameValue.Text = _outletUserLimitDto.userName;
                lblNameValue.Text = _outletUserLimitDto.fullName;
                if (_outletUserLimitDto.address != null)
                {
                    lblAddress.Text = _outletUserLimitDto.address.addressLineOne;
                    lblDistrict.Text = _outletUserLimitDto.address.district.title;
                }
                lblPhone.Text = _outletUserLimitDto.mobileNumber ?? "";
                OutletUserId = _outletUserLimitDto.id.ToString();
                
                tglCashLimit.Checked = _outletUserLimitDto.cashLimitApplicable ?? false;
                
                SetCashLimitEnable(_outletUserLimitDto.cashLimitApplicable ?? false);

                Image userImage = LocalCache.GetUserProfilePicture(_outletUserLimitDto.userName);
                if (userImage != null) picUserPhoto.Image = userImage;
                #endregion
            }
        }

        private void SetControlSelectionEvents()
        {
            SetEvents(this);
            for (int i = 0; i < Controls.Count; i++)
            {
                SetEvents(Controls[i]);
                if (Controls[i].HasChildren)
                {
                    for (int j = 0; j < Controls[i].Controls.Count; j++)
                    {
                        SetEvents(Controls[i].Controls[j]);
                    }
                }
            }
        }

        private void SetEvents(Control control)
        {
            control.MouseEnter += CashLimitListItem_MouseEnter;
            control.MouseLeave += CashLimitListItem_MouseLeave;
        }

        private void Control_MouseMove(object sender, MouseEventArgs e)
        {
            ChangeControlSelectState(true);
        }

        private void Control_Enter(object sender, EventArgs e)
        {
            ChangeControlSelectState(true);
        }

        private void CashLimitListItem_MouseEnter(object sender, EventArgs e)
        {
            ChangeControlSelectState(true);
        }

        private void CashLimitListItem_MouseLeave(object sender, EventArgs e)
        {
            ChangeControlSelectState(false);
        }

        private void ChangeControlSelectState(bool isSelected)
        {
            if (isSelected)
            {
                this.BackColor = Color.LightGray;
                for (int i = 0; i < Controls.Count; i++)
                {
                    Controls[i].BackColor = Color.LightGray;
                    if (Controls[i].HasChildren)
                    {
                        for (int j = 0; j < Controls[i].Controls.Count; j++)
                        {
                            Controls[i].Controls[j].BackColor = Color.LightGray;
                        }
                    }
                }
                panelHeader.BackColor = panelFooter.BackColor = Color.FromArgb(0, 122, 170);
                lblUserNameValue.BackColor = lblNameValue.BackColor = lblName.BackColor = lblCode.BackColor = Color.FromArgb(0, 122, 170);
                lblUserNameValue.ForeColor = lblNameValue.ForeColor = lblName.ForeColor = lblCode.ForeColor = Color.White;
                tglActive.BackColor = Color.LightGray;

                if (isEditCash == false) txtCashLimit.BackColor = Color.Silver;
                else txtCashLimit.BackColor = Color.White;
            }
            else
            {
                this.BackColor = Color.White;
                for (int i = 0; i < Controls.Count; i++)
                {
                    Controls[i].BackColor = Color.White;
                    if (Controls[i].HasChildren)
                    {
                        for (int j = 0; j < Controls[i].Controls.Count; j++)
                        {
                            Controls[i].Controls[j].BackColor = Color.White;
                        }
                    }
                }
                panelHeader.BackColor = panelFooter.BackColor = Color.FromArgb(122, 163, 237);
                lblUserNameValue.BackColor = lblNameValue.BackColor = lblName.BackColor = lblCode.BackColor = Color.FromArgb(122, 163, 237);
                lblUserNameValue.ForeColor = lblNameValue.ForeColor = lblName.ForeColor = lblCode.ForeColor = Color.White;
                tglActive.BackColor = Color.Gainsboro;
                //txtCreditLimit.BackColor = txtDebitLimit.BackColor = Color.Gainsboro;
                txtCashLimit.BackColor = Color.Gainsboro;
                tglActive.BackColor = Color.White;
            }

            //panel1.BackColor = panel3.BackColor = panel5.BackColor = Color.Gainsboro; //panel2.BackColor 
            pbLgUsage.BackColor = Color.FromArgb(252, 180, 65);
            pbLgRemaining.BackColor = Color.FromArgb(65, 140, 240);
            
            if (btnCashLimit.Enabled == false)
            {
                btnCashLimit.BackColor = Color.Gray;
            }
            else
            {
                btnCashLimit.BackColor = Color.FromArgb(0, 122, 170);
            }
        }

        private void tglActive_CheckedChanged(object sender, EventArgs e)
        {
            if (tglActive.Checked)
            {
                lblActiveInactiveStatus.Text = "Active";
            }
            else
            {
                lblActiveInactiveStatus.Text = "Inactive";
            }
        }

        private void lblActiveInactiveStatus_Click(object sender, EventArgs e)
        {

        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            try
            {
                if (_limitUserType == LimitUserType.Outlet)
                {
                    frmOutletUserList frm = new frmOutletUserList(null, _subAgentInformation);
                    frm.ShowDialog();
                }
                else if (_limitUserType == LimitUserType.User)
                {
                    ShowUserDetails(_outletUserLimitDto.id, _outletUserLimitDto.individualId ?? 0);
                }
            }
            catch (Exception exp)
            { Message.showError(exp.Message); }
        }

        public void SetCashChartAndLabelValues(decimal? limit, decimal? usage, bool enabled)
        {
            decimal remaining = ((limit == null || limit.ToString() == "") ? 0 : limit ?? 0) - ((usage == null || usage.ToString() == "") ? 0 : usage ?? 0);

            lblCHLimitValue.Text = ((limit == null || limit.ToString() == "") ? 0 : limit ?? 0).ToString();
            lblCHUsageValue.Text = ((usage == null || usage.ToString() == "") ? 0 : usage ?? 0).ToString();

            lblCHRemainingValue.Text = remaining.ToString();

            crtCash.Series[0].Points.Clear();
            DataPoint dataPointRem = new DataPoint();
            dataPointRem.SetValueY(double.Parse(lblCHRemainingValue.Text));
            crtCash.Series[0].Points.Add(dataPointRem);

            DataPoint dataPointUsage = new DataPoint();
            dataPointUsage.SetValueY(double.Parse(lblCHUsageValue.Text));
            crtCash.Series[0].Points.Add(dataPointUsage);

            lblCHLimitValue.Enabled = lblCHUsageValue.Enabled = lblCHRemainingValue.Enabled = enabled;
            lblCHLimit.Enabled = lblCHUsage.Enabled = lblCHRemaining.Enabled = enabled;
            crtCash.Visible = enabled;

            if (enabled)
            {
                txtCashLimit.Text = limit == null ? "0.00" : limit.ToString();
            }
            else
            {
                txtCashLimit.Text = "0.00";
            }
        }
        public void SetCashLimitEnable(bool val)
        {
            btnCashLimit.Enabled = txtCashLimit.Enabled = lblCHDailyLimit.Enabled = val;

            if (val)
            {
                if (_limitUserType == LimitUserType.Outlet)
                {
                    SetCashChartAndLabelValues
                    (
                        ((_subAgentInformation.cashLimit ?? 0) + (_subAgentInformation.cashBalance ?? 0)),    // Because the limit gets only remaining limit.
                        (_subAgentInformation.cashBalance ?? 0),
                        true
                    );
                }
                else if (_limitUserType == LimitUserType.User)
                {
                    SetCashChartAndLabelValues
                    (
                        ((_outletUserLimitDto.cashLimit ?? 0) + (_outletUserLimitDto.cashBalance ?? 0)),    // Because the limit gets only remaining limit.
                        (_outletUserLimitDto.cashBalance ?? 0),
                        true
                    );
                }
            }
            else
            {
                SetCashChartAndLabelValues
                (
                    0,
                    0,
                    false
                );
            }
        }
        private void tglCashLimit_CheckedChanged(object sender, EventArgs e)
        {
            SetCashLimitEnable(tglCashLimit.Checked);
        }
        private void tglCashLimit_Click(object sender, EventArgs e)
        {
            bool tmpStatus = tglCashLimit.Checked;
            try
            {
                SubAgentService subAgentService = new SubAgentService();
                ServiceResult result = new ServiceResult();
                if (_limitUserType == LimitUserType.Outlet)
                {
                    subAgentService.SetOutletCashLimit(OutletId, tglCashLimit.Checked);
                    SetCashLimitEnable(tglCashLimit.Checked);
                }
                else if (_limitUserType == LimitUserType.User)
                {
                    result = subAgentService.SetOutletUserCashLimit(OutletUserId, tglCashLimit.Checked);
                    if (result.Success) SetCashLimitEnable(tglCashLimit.Checked);
                    else Message.showError(result.Message);
                }
            }
            catch
            {
                tglCashLimit.Checked = tmpStatus;
                SetCashLimitEnable(tmpStatus);
                Message.showError("Could not set cash limit status!");
            }
        }

        private void btnCashLimit_Click(object sender, EventArgs e)
        {
            if (btnCashLimit.Text == "Edit")
            {
                isEditCash = true;
                txtCashLimit.BackColor = Color.White;
                txtCashLimit.ReadOnly = false;
                btnCashLimit.Text = "Save";
                txtCashLimit.Focus();
                txtCashLimit.SelectAll();
            }
            else
            {
                try
                {
                    ServiceResult result = new ServiceResult();
                    SubAgentService subAgentCom = new SubAgentService();
                    if (_limitUserType == LimitUserType.Outlet)
                    {
                        subAgentCom.SaveOutletCashLimit(OutletId, decimal.Parse(txtCashLimit.Text.Trim()));
                        _subAgentInformation.cashLimit = decimal.Parse(txtCashLimit.Text.Trim());

                        SetCashChartAndLabelValues(decimal.Parse(txtCashLimit.Text.Trim()), (_subAgentInformation.cashBalance ?? 0), true);
                    }
                    else if (_limitUserType == LimitUserType.User)
                    {
                        result = subAgentCom.SaveOutletUserCashLimit(OutletUserId, decimal.Parse(txtCashLimit.Text.Trim()));
                        if (result.Success)
                        {
                            SetCashChartAndLabelValues(decimal.Parse(txtCashLimit.Text.Trim()), (_outletUserLimitDto.cashBalance ?? 0), true);

                            _outletUserLimitDto.cashLimit = decimal.Parse(txtCashLimit.Text.Trim());
                        }
                        else Message.showError(result.Message);
                    }

                    isEditCash = false;
                    txtCashLimit.BackColor = Color.Gainsboro;
                    txtCashLimit.ReadOnly = true;
                    btnCashLimit.Text = "Edit";
                }
                catch (Exception exp) { Message.showError(exp.Message); }
            }
            crtCash.Update();
        }
        private void btnCashLimit_EnabledChanged(object sender, EventArgs e)
        {
            if (btnCashLimit.Enabled == false)
            {
                btnCashLimit.BackColor = Color.Gray;
            }
            else
            {
                btnCashLimit.BackColor = Color.FromArgb(0, 122, 170);
            }
        }

        private void ShowUserDetails(long userId, long individualId)
        {
            try
            {
                UserService userService = new UserService();
                AgentUserDetilSearchDto userSearchDto = new AgentUserDetilSearchDto();
                userSearchDto.userCategory = UserCategory.SubAgentUser;
                userSearchDto.userId = userId;
                userSearchDto.individualId = individualId;

                ServiceResult serviceResult = ServiceResult.CreateServiceResult();
                serviceResult = userService.GetUserDetailsByUserId(userSearchDto);

                if (!serviceResult.Success)
                {
                    MessageBox.Show(serviceResult.Message);
                    return;
                }

                AgentUserDto userDetails = serviceResult.ReturnedObject as AgentUserDto;
                userDetails.subAgentUser.subAgentInformation = new SubAgentInformation();
                //userDetails.subAgentUser.subAgentInformation.id = long.Parse(OutletId);

                Packet packet = new Packet();
                packet.actionType = FormActionType.View;

                frmUserCreationWithPersonalInfo userForm = new frmUserCreationWithPersonalInfo(packet, userDetails);
                userForm.ShowDialog();
            }
            catch (Exception exp)
            { throw new Exception(exp.Message); }
        }

    }
}