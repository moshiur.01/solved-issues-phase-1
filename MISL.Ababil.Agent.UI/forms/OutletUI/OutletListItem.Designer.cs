﻿namespace MISL.Ababil.Agent.UI.forms.OutletUI
{
    partial class OutletListItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint1 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 40D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint2 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 60D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint3 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 40D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint4 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 60D);
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint5 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 40D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint6 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 60D);
            this.lblCodeValue = new System.Windows.Forms.Label();
            this.lblCode = new System.Windows.Forms.Label();
            this.lblNameValue = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblDistrict = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.gbxUsage = new System.Windows.Forms.GroupBox();
            this.pbLgRemaining = new System.Windows.Forms.PictureBox();
            this.pbLgUsage = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblDRRemaining = new System.Windows.Forms.Label();
            this.lblCRRemaining = new System.Windows.Forms.Label();
            this.lblDRRemainingValue = new System.Windows.Forms.Label();
            this.lblCRRemainingValue = new System.Windows.Forms.Label();
            this.lblDRUsage = new System.Windows.Forms.Label();
            this.lblCRUsage = new System.Windows.Forms.Label();
            this.lblDRUsageValue = new System.Windows.Forms.Label();
            this.lblCRUsageValue = new System.Windows.Forms.Label();
            this.lblDRLimit = new System.Windows.Forms.Label();
            this.lblCRLimit = new System.Windows.Forms.Label();
            this.lblDRLimitValue = new System.Windows.Forms.Label();
            this.lblCRLimitValue = new System.Windows.Forms.Label();
            this.crtDebit = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.crtCredit = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnCreditLimit = new System.Windows.Forms.Button();
            this.txtCreditLimit = new System.Windows.Forms.TextBox();
            this.lblCRDailyLimit = new System.Windows.Forms.Label();
            this.tglCreditLimit = new MetroFramework.Controls.MetroToggle();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDebitLimit = new System.Windows.Forms.Button();
            this.txtDebitLimit = new System.Windows.Forms.TextBox();
            this.lblDRDailyLimit = new System.Windows.Forms.Label();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.tglActive = new MetroFramework.Controls.MetroToggle();
            this.panelFooter = new System.Windows.Forms.Panel();
            this.btnOpen = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tglDebitLimit = new MetroFramework.Controls.MetroToggle();
            this.label3 = new System.Windows.Forms.Label();
            this.lblActiveInactiveStatus = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnCashLimit = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtCashLimit = new System.Windows.Forms.TextBox();
            this.tglCashLimit = new MetroFramework.Controls.MetroToggle();
            this.lblCHRemaining = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblCHRemainingValue = new System.Windows.Forms.Label();
            this.lblCHDailyLimit = new System.Windows.Forms.Label();
            this.lblCHUsage = new System.Windows.Forms.Label();
            this.lblCHLimit = new System.Windows.Forms.Label();
            this.lblCHUsageValue = new System.Windows.Forms.Label();
            this.crtCash = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.lblCHLimitValue = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.gbxUsage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLgRemaining)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLgUsage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.crtDebit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.crtCredit)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.panelHeader.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.crtCash)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCodeValue
            // 
            this.lblCodeValue.AutoSize = true;
            this.lblCodeValue.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodeValue.ForeColor = System.Drawing.Color.White;
            this.lblCodeValue.Location = new System.Drawing.Point(66, 2);
            this.lblCodeValue.Name = "lblCodeValue";
            this.lblCodeValue.Size = new System.Drawing.Size(89, 20);
            this.lblCodeValue.TabIndex = 1;
            this.lblCodeValue.Text = "0000000000";
            this.lblCodeValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCode
            // 
            this.lblCode.AutoSize = true;
            this.lblCode.Font = new System.Drawing.Font("Segoe UI Semilight", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCode.ForeColor = System.Drawing.Color.White;
            this.lblCode.Location = new System.Drawing.Point(9, 2);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(51, 20);
            this.lblCode.TabIndex = 4;
            this.lblCode.Text = "Code :";
            this.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNameValue
            // 
            this.lblNameValue.AutoSize = true;
            this.lblNameValue.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameValue.ForeColor = System.Drawing.Color.White;
            this.lblNameValue.Location = new System.Drawing.Point(249, 2);
            this.lblNameValue.Name = "lblNameValue";
            this.lblNameValue.Size = new System.Drawing.Size(90, 20);
            this.lblNameValue.TabIndex = 5;
            this.lblNameValue.Text = "outletName";
            this.lblNameValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Segoe UI Semilight", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ForeColor = System.Drawing.Color.White;
            this.lblName.Location = new System.Drawing.Point(187, 2);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(56, 20);
            this.lblName.TabIndex = 6;
            this.lblName.Text = "Name :";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(5, 18);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Address :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddress.Location = new System.Drawing.Point(59, 18);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(63, 13);
            this.lblAddress.TabIndex = 5;
            this.lblAddress.Text = "<address>";
            this.lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDistrict
            // 
            this.lblDistrict.AutoSize = true;
            this.lblDistrict.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDistrict.Location = new System.Drawing.Point(59, 36);
            this.lblDistrict.Name = "lblDistrict";
            this.lblDistrict.Size = new System.Drawing.Size(58, 13);
            this.lblDistrict.TabIndex = 5;
            this.lblDistrict.Text = "<district>";
            this.lblDistrict.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(10, 36);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 13);
            this.label16.TabIndex = 6;
            this.label16.Text = "District :";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhone.Location = new System.Drawing.Point(59, 54);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(55, 13);
            this.lblPhone.TabIndex = 5;
            this.lblPhone.Text = "<phone>";
            this.lblPhone.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(13, 54);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(46, 13);
            this.label18.TabIndex = 6;
            this.label18.Text = "Phone :";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbxUsage
            // 
            this.gbxUsage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxUsage.Controls.Add(this.pbLgRemaining);
            this.gbxUsage.Controls.Add(this.pbLgUsage);
            this.gbxUsage.Controls.Add(this.label8);
            this.gbxUsage.Controls.Add(this.label7);
            this.gbxUsage.Location = new System.Drawing.Point(246, 34);
            this.gbxUsage.Name = "gbxUsage";
            this.gbxUsage.Size = new System.Drawing.Size(97, 118);
            this.gbxUsage.TabIndex = 7;
            this.gbxUsage.TabStop = false;
            this.gbxUsage.Text = "Legend";
            // 
            // pbLgRemaining
            // 
            this.pbLgRemaining.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(140)))), ((int)(((byte)(240)))));
            this.pbLgRemaining.Location = new System.Drawing.Point(8, 38);
            this.pbLgRemaining.Name = "pbLgRemaining";
            this.pbLgRemaining.Size = new System.Drawing.Size(11, 11);
            this.pbLgRemaining.TabIndex = 11;
            this.pbLgRemaining.TabStop = false;
            // 
            // pbLgUsage
            // 
            this.pbLgUsage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(180)))), ((int)(((byte)(65)))));
            this.pbLgUsage.Location = new System.Drawing.Point(8, 22);
            this.pbLgUsage.Name = "pbLgUsage";
            this.pbLgUsage.Size = new System.Drawing.Size(11, 11);
            this.pbLgUsage.TabIndex = 11;
            this.pbLgUsage.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.label8.Location = new System.Drawing.Point(21, 37);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 12);
            this.label8.TabIndex = 6;
            this.label8.Text = "Remaining";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.label7.Location = new System.Drawing.Point(21, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 12);
            this.label7.TabIndex = 6;
            this.label7.Text = "Usage";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Black;
            this.panel5.Location = new System.Drawing.Point(63, 41);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(108, 1);
            this.panel5.TabIndex = 14;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Black;
            this.panel3.Location = new System.Drawing.Point(63, 41);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(108, 1);
            this.panel3.TabIndex = 14;
            // 
            // lblDRRemaining
            // 
            this.lblDRRemaining.AutoSize = true;
            this.lblDRRemaining.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.lblDRRemaining.Location = new System.Drawing.Point(67, 44);
            this.lblDRRemaining.Name = "lblDRRemaining";
            this.lblDRRemaining.Size = new System.Drawing.Size(32, 12);
            this.lblDRRemaining.TabIndex = 6;
            this.lblDRRemaining.Text = "Rem. :";
            this.lblDRRemaining.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCRRemaining
            // 
            this.lblCRRemaining.AutoSize = true;
            this.lblCRRemaining.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.lblCRRemaining.Location = new System.Drawing.Point(67, 44);
            this.lblCRRemaining.Name = "lblCRRemaining";
            this.lblCRRemaining.Size = new System.Drawing.Size(32, 12);
            this.lblCRRemaining.TabIndex = 6;
            this.lblCRRemaining.Text = "Rem. :";
            this.lblCRRemaining.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDRRemainingValue
            // 
            this.lblDRRemainingValue.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.lblDRRemainingValue.Location = new System.Drawing.Point(100, 44);
            this.lblDRRemainingValue.Name = "lblDRRemainingValue";
            this.lblDRRemainingValue.Size = new System.Drawing.Size(71, 12);
            this.lblDRRemainingValue.TabIndex = 6;
            this.lblDRRemainingValue.Text = "000000.00";
            this.lblDRRemainingValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCRRemainingValue
            // 
            this.lblCRRemainingValue.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.lblCRRemainingValue.Location = new System.Drawing.Point(100, 44);
            this.lblCRRemainingValue.Name = "lblCRRemainingValue";
            this.lblCRRemainingValue.Size = new System.Drawing.Size(71, 12);
            this.lblCRRemainingValue.TabIndex = 6;
            this.lblCRRemainingValue.Text = "00000000.00";
            this.lblCRRemainingValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDRUsage
            // 
            this.lblDRUsage.AutoSize = true;
            this.lblDRUsage.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.lblDRUsage.Location = new System.Drawing.Point(62, 25);
            this.lblDRUsage.Name = "lblDRUsage";
            this.lblDRUsage.Size = new System.Drawing.Size(37, 12);
            this.lblDRUsage.TabIndex = 6;
            this.lblDRUsage.Text = "Usage :";
            this.lblDRUsage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCRUsage
            // 
            this.lblCRUsage.AutoSize = true;
            this.lblCRUsage.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.lblCRUsage.Location = new System.Drawing.Point(62, 25);
            this.lblCRUsage.Name = "lblCRUsage";
            this.lblCRUsage.Size = new System.Drawing.Size(37, 12);
            this.lblCRUsage.TabIndex = 6;
            this.lblCRUsage.Text = "Usage :";
            this.lblCRUsage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDRUsageValue
            // 
            this.lblDRUsageValue.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.lblDRUsageValue.Location = new System.Drawing.Point(100, 25);
            this.lblDRUsageValue.Name = "lblDRUsageValue";
            this.lblDRUsageValue.Size = new System.Drawing.Size(71, 12);
            this.lblDRUsageValue.TabIndex = 6;
            this.lblDRUsageValue.Text = "000000.00";
            this.lblDRUsageValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCRUsageValue
            // 
            this.lblCRUsageValue.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.lblCRUsageValue.Location = new System.Drawing.Point(100, 25);
            this.lblCRUsageValue.Name = "lblCRUsageValue";
            this.lblCRUsageValue.Size = new System.Drawing.Size(71, 12);
            this.lblCRUsageValue.TabIndex = 6;
            this.lblCRUsageValue.Text = "00000000.00";
            this.lblCRUsageValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDRLimit
            // 
            this.lblDRLimit.AutoSize = true;
            this.lblDRLimit.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.lblDRLimit.Location = new System.Drawing.Point(68, 12);
            this.lblDRLimit.Name = "lblDRLimit";
            this.lblDRLimit.Size = new System.Drawing.Size(31, 12);
            this.lblDRLimit.TabIndex = 6;
            this.lblDRLimit.Text = "Limit :";
            this.lblDRLimit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCRLimit
            // 
            this.lblCRLimit.AutoSize = true;
            this.lblCRLimit.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.lblCRLimit.Location = new System.Drawing.Point(68, 12);
            this.lblCRLimit.Name = "lblCRLimit";
            this.lblCRLimit.Size = new System.Drawing.Size(31, 12);
            this.lblCRLimit.TabIndex = 6;
            this.lblCRLimit.Text = "Limit :";
            this.lblCRLimit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDRLimitValue
            // 
            this.lblDRLimitValue.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.lblDRLimitValue.Location = new System.Drawing.Point(100, 12);
            this.lblDRLimitValue.Name = "lblDRLimitValue";
            this.lblDRLimitValue.Size = new System.Drawing.Size(71, 12);
            this.lblDRLimitValue.TabIndex = 6;
            this.lblDRLimitValue.Text = "000000.00";
            this.lblDRLimitValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCRLimitValue
            // 
            this.lblCRLimitValue.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.lblCRLimitValue.Location = new System.Drawing.Point(100, 12);
            this.lblCRLimitValue.Name = "lblCRLimitValue";
            this.lblCRLimitValue.Size = new System.Drawing.Size(71, 12);
            this.lblCRLimitValue.TabIndex = 6;
            this.lblCRLimitValue.Text = "00000000.00";
            this.lblCRLimitValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // crtDebit
            // 
            chartArea1.Name = "ChartArea1";
            this.crtDebit.ChartAreas.Add(chartArea1);
            this.crtDebit.Location = new System.Drawing.Point(7, 16);
            this.crtDebit.Name = "crtDebit";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series1.IsVisibleInLegend = false;
            series1.Name = "Series2";
            series1.Points.Add(dataPoint1);
            series1.Points.Add(dataPoint2);
            this.crtDebit.Series.Add(series1);
            this.crtDebit.Size = new System.Drawing.Size(47, 42);
            this.crtDebit.TabIndex = 10;
            this.crtDebit.Text = "chart3";
            // 
            // crtCredit
            // 
            chartArea2.Name = "ChartArea1";
            this.crtCredit.ChartAreas.Add(chartArea2);
            this.crtCredit.Location = new System.Drawing.Point(7, 16);
            this.crtCredit.Name = "crtCredit";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series2.IsVisibleInLegend = false;
            series2.Name = "Series2";
            series2.Points.Add(dataPoint3);
            series2.Points.Add(dataPoint4);
            this.crtCredit.Series.Add(series2);
            this.crtCredit.Size = new System.Drawing.Size(47, 42);
            this.crtCredit.TabIndex = 10;
            this.crtCredit.Text = "chart3";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnCreditLimit);
            this.groupBox2.Controls.Add(this.panel3);
            this.groupBox2.Controls.Add(this.txtCreditLimit);
            this.groupBox2.Controls.Add(this.lblCRDailyLimit);
            this.groupBox2.Controls.Add(this.tglCreditLimit);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.lblCRRemaining);
            this.groupBox2.Controls.Add(this.lblCRLimit);
            this.groupBox2.Controls.Add(this.crtCredit);
            this.groupBox2.Controls.Add(this.lblCRRemainingValue);
            this.groupBox2.Controls.Add(this.lblCRLimitValue);
            this.groupBox2.Controls.Add(this.lblCRUsageValue);
            this.groupBox2.Controls.Add(this.lblCRUsage);
            this.groupBox2.Location = new System.Drawing.Point(345, 34);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(176, 118);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Credit";
            // 
            // btnCreditLimit
            // 
            this.btnCreditLimit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnCreditLimit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCreditLimit.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btnCreditLimit.FlatAppearance.BorderSize = 0;
            this.btnCreditLimit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreditLimit.ForeColor = System.Drawing.Color.White;
            this.btnCreditLimit.Location = new System.Drawing.Point(128, 90);
            this.btnCreditLimit.Name = "btnCreditLimit";
            this.btnCreditLimit.Size = new System.Drawing.Size(42, 21);
            this.btnCreditLimit.TabIndex = 8;
            this.btnCreditLimit.Text = "Edit";
            this.btnCreditLimit.UseVisualStyleBackColor = false;
            this.btnCreditLimit.EnabledChanged += new System.EventHandler(this.btnCreditLimit_EnabledChanged);
            this.btnCreditLimit.Click += new System.EventHandler(this.btnCreditLimit_Click);
            // 
            // txtCreditLimit
            // 
            this.txtCreditLimit.BackColor = System.Drawing.Color.Gainsboro;
            this.txtCreditLimit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCreditLimit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCreditLimit.Location = new System.Drawing.Point(42, 90);
            this.txtCreditLimit.Name = "txtCreditLimit";
            this.txtCreditLimit.ReadOnly = true;
            this.txtCreditLimit.Size = new System.Drawing.Size(86, 21);
            this.txtCreditLimit.TabIndex = 9;
            this.txtCreditLimit.Text = "0.00";
            this.txtCreditLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblCRDailyLimit
            // 
            this.lblCRDailyLimit.AutoSize = true;
            this.lblCRDailyLimit.Location = new System.Drawing.Point(5, 93);
            this.lblCRDailyLimit.Name = "lblCRDailyLimit";
            this.lblCRDailyLimit.Size = new System.Drawing.Size(37, 13);
            this.lblCRDailyLimit.TabIndex = 6;
            this.lblCRDailyLimit.Text = "Limit :";
            this.lblCRDailyLimit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tglCreditLimit
            // 
            this.tglCreditLimit.AutoSize = true;
            this.tglCreditLimit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tglCreditLimit.Location = new System.Drawing.Point(90, 69);
            this.tglCreditLimit.Name = "tglCreditLimit";
            this.tglCreditLimit.Size = new System.Drawing.Size(80, 17);
            this.tglCreditLimit.TabIndex = 10;
            this.tglCreditLimit.Text = "Off";
            this.tglCreditLimit.UseCustomBackColor = true;
            this.tglCreditLimit.UseSelectable = true;
            this.tglCreditLimit.CheckedChanged += new System.EventHandler(this.tglCreditLimit_CheckedChanged);
            this.tglCreditLimit.Click += new System.EventHandler(this.tglCreditLimit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label1.Location = new System.Drawing.Point(59, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 15);
            this.label1.TabIndex = 6;
            this.label1.Text = "Limit";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnDebitLimit
            // 
            this.btnDebitLimit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnDebitLimit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDebitLimit.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btnDebitLimit.FlatAppearance.BorderSize = 0;
            this.btnDebitLimit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDebitLimit.ForeColor = System.Drawing.Color.White;
            this.btnDebitLimit.Location = new System.Drawing.Point(128, 90);
            this.btnDebitLimit.Name = "btnDebitLimit";
            this.btnDebitLimit.Size = new System.Drawing.Size(42, 21);
            this.btnDebitLimit.TabIndex = 8;
            this.btnDebitLimit.Text = "Edit";
            this.btnDebitLimit.UseVisualStyleBackColor = false;
            this.btnDebitLimit.EnabledChanged += new System.EventHandler(this.btnDebitLimit_EnabledChanged);
            this.btnDebitLimit.Click += new System.EventHandler(this.btnDebitLimit_Click);
            // 
            // txtDebitLimit
            // 
            this.txtDebitLimit.BackColor = System.Drawing.Color.Gainsboro;
            this.txtDebitLimit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDebitLimit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDebitLimit.Location = new System.Drawing.Point(42, 90);
            this.txtDebitLimit.Name = "txtDebitLimit";
            this.txtDebitLimit.ReadOnly = true;
            this.txtDebitLimit.Size = new System.Drawing.Size(86, 21);
            this.txtDebitLimit.TabIndex = 10;
            this.txtDebitLimit.Text = "0.00";
            this.txtDebitLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDRDailyLimit
            // 
            this.lblDRDailyLimit.AutoSize = true;
            this.lblDRDailyLimit.Location = new System.Drawing.Point(5, 93);
            this.lblDRDailyLimit.Name = "lblDRDailyLimit";
            this.lblDRDailyLimit.Size = new System.Drawing.Size(37, 13);
            this.lblDRDailyLimit.TabIndex = 6;
            this.lblDRDailyLimit.Text = "Limit :";
            this.lblDRDailyLimit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelHeader
            // 
            this.panelHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(163)))), ((int)(((byte)(237)))));
            this.panelHeader.Controls.Add(this.lblName);
            this.panelHeader.Controls.Add(this.lblCodeValue);
            this.panelHeader.Controls.Add(this.lblCode);
            this.panelHeader.Controls.Add(this.lblNameValue);
            this.panelHeader.Location = new System.Drawing.Point(5, 7);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(916, 25);
            this.panelHeader.TabIndex = 9;
            // 
            // tglActive
            // 
            this.tglActive.AutoSize = true;
            this.tglActive.BackColor = System.Drawing.Color.White;
            this.tglActive.Location = new System.Drawing.Point(95, 94);
            this.tglActive.Name = "tglActive";
            this.tglActive.Size = new System.Drawing.Size(80, 17);
            this.tglActive.TabIndex = 10;
            this.tglActive.Text = "Off";
            this.tglActive.UseCustomBackColor = true;
            this.tglActive.UseSelectable = true;
            this.tglActive.Visible = false;
            this.tglActive.CheckedChanged += new System.EventHandler(this.tglActive_CheckedChanged);
            // 
            // panelFooter
            // 
            this.panelFooter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelFooter.BackColor = System.Drawing.Color.Gainsboro;
            this.panelFooter.Location = new System.Drawing.Point(5, 155);
            this.panelFooter.Name = "panelFooter";
            this.panelFooter.Size = new System.Drawing.Size(916, 2);
            this.panelFooter.TabIndex = 0;
            // 
            // btnOpen
            // 
            this.btnOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOpen.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btnOpen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpen.Image = global::MISL.Ababil.Agent.UI.Properties.Resources.OpenRequestedPopup;
            this.btnOpen.Location = new System.Drawing.Point(879, 41);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(42, 108);
            this.btnOpen.TabIndex = 8;
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.panel5);
            this.groupBox3.Controls.Add(this.btnDebitLimit);
            this.groupBox3.Controls.Add(this.txtDebitLimit);
            this.groupBox3.Controls.Add(this.tglDebitLimit);
            this.groupBox3.Controls.Add(this.lblDRRemaining);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.lblDRRemainingValue);
            this.groupBox3.Controls.Add(this.lblDRDailyLimit);
            this.groupBox3.Controls.Add(this.lblDRUsage);
            this.groupBox3.Controls.Add(this.lblDRLimit);
            this.groupBox3.Controls.Add(this.lblDRUsageValue);
            this.groupBox3.Controls.Add(this.crtDebit);
            this.groupBox3.Controls.Add(this.lblDRLimitValue);
            this.groupBox3.Location = new System.Drawing.Point(523, 34);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(176, 118);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Debit";
            // 
            // tglDebitLimit
            // 
            this.tglDebitLimit.AutoSize = true;
            this.tglDebitLimit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tglDebitLimit.Location = new System.Drawing.Point(90, 69);
            this.tglDebitLimit.Name = "tglDebitLimit";
            this.tglDebitLimit.Size = new System.Drawing.Size(80, 17);
            this.tglDebitLimit.TabIndex = 10;
            this.tglDebitLimit.Text = "Off";
            this.tglDebitLimit.UseCustomBackColor = true;
            this.tglDebitLimit.UseSelectable = true;
            this.tglDebitLimit.CheckedChanged += new System.EventHandler(this.tglDebitLimit_CheckedChanged);
            this.tglDebitLimit.Click += new System.EventHandler(this.tglDebitLimit_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label3.Location = new System.Drawing.Point(59, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Limit";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblActiveInactiveStatus
            // 
            this.lblActiveInactiveStatus.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold);
            this.lblActiveInactiveStatus.Location = new System.Drawing.Point(58, 92);
            this.lblActiveInactiveStatus.Name = "lblActiveInactiveStatus";
            this.lblActiveInactiveStatus.Size = new System.Drawing.Size(64, 19);
            this.lblActiveInactiveStatus.TabIndex = 6;
            this.lblActiveInactiveStatus.Text = "Inactive";
            this.lblActiveInactiveStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblActiveInactiveStatus.Visible = false;
            this.lblActiveInactiveStatus.Click += new System.EventHandler(this.lblActiveInactiveStatus_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btnCashLimit);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.txtCashLimit);
            this.groupBox1.Controls.Add(this.tglCashLimit);
            this.groupBox1.Controls.Add(this.lblCHRemaining);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lblCHRemainingValue);
            this.groupBox1.Controls.Add(this.lblCHDailyLimit);
            this.groupBox1.Controls.Add(this.lblCHUsage);
            this.groupBox1.Controls.Add(this.lblCHLimit);
            this.groupBox1.Controls.Add(this.lblCHUsageValue);
            this.groupBox1.Controls.Add(this.crtCash);
            this.groupBox1.Controls.Add(this.lblCHLimitValue);
            this.groupBox1.Location = new System.Drawing.Point(701, 34);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(176, 118);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cash";
            // 
            // btnCashLimit
            // 
            this.btnCashLimit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnCashLimit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCashLimit.FlatAppearance.BorderColor = System.Drawing.Color.DarkGray;
            this.btnCashLimit.FlatAppearance.BorderSize = 0;
            this.btnCashLimit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCashLimit.ForeColor = System.Drawing.Color.White;
            this.btnCashLimit.Location = new System.Drawing.Point(128, 90);
            this.btnCashLimit.Name = "btnCashLimit";
            this.btnCashLimit.Size = new System.Drawing.Size(42, 21);
            this.btnCashLimit.TabIndex = 8;
            this.btnCashLimit.Text = "Edit";
            this.btnCashLimit.UseVisualStyleBackColor = false;
            this.btnCashLimit.EnabledChanged += new System.EventHandler(this.btnCashLimit_EnabledChanged);
            this.btnCashLimit.Click += new System.EventHandler(this.btnCashLimit_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(63, 41);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(108, 1);
            this.panel1.TabIndex = 14;
            // 
            // txtCashLimit
            // 
            this.txtCashLimit.BackColor = System.Drawing.Color.Gainsboro;
            this.txtCashLimit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCashLimit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashLimit.Location = new System.Drawing.Point(42, 90);
            this.txtCashLimit.Name = "txtCashLimit";
            this.txtCashLimit.ReadOnly = true;
            this.txtCashLimit.Size = new System.Drawing.Size(86, 21);
            this.txtCashLimit.TabIndex = 10;
            this.txtCashLimit.Text = "0.00";
            this.txtCashLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tglCashLimit
            // 
            this.tglCashLimit.AutoSize = true;
            this.tglCashLimit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tglCashLimit.Location = new System.Drawing.Point(90, 69);
            this.tglCashLimit.Name = "tglCashLimit";
            this.tglCashLimit.Size = new System.Drawing.Size(80, 17);
            this.tglCashLimit.TabIndex = 10;
            this.tglCashLimit.Text = "Off";
            this.tglCashLimit.UseCustomBackColor = true;
            this.tglCashLimit.UseSelectable = true;
            this.tglCashLimit.CheckedChanged += new System.EventHandler(this.tglCashLimit_CheckedChanged);
            this.tglCashLimit.Click += new System.EventHandler(this.tglCashLimit_Click);
            // 
            // lblCHRemaining
            // 
            this.lblCHRemaining.AutoSize = true;
            this.lblCHRemaining.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.lblCHRemaining.Location = new System.Drawing.Point(67, 44);
            this.lblCHRemaining.Name = "lblCHRemaining";
            this.lblCHRemaining.Size = new System.Drawing.Size(32, 12);
            this.lblCHRemaining.TabIndex = 6;
            this.lblCHRemaining.Text = "Rem. :";
            this.lblCHRemaining.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label4.Location = new System.Drawing.Point(59, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "Limit";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCHRemainingValue
            // 
            this.lblCHRemainingValue.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.lblCHRemainingValue.Location = new System.Drawing.Point(100, 44);
            this.lblCHRemainingValue.Name = "lblCHRemainingValue";
            this.lblCHRemainingValue.Size = new System.Drawing.Size(71, 12);
            this.lblCHRemainingValue.TabIndex = 6;
            this.lblCHRemainingValue.Text = "000000.00";
            this.lblCHRemainingValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCHDailyLimit
            // 
            this.lblCHDailyLimit.AutoSize = true;
            this.lblCHDailyLimit.Location = new System.Drawing.Point(5, 93);
            this.lblCHDailyLimit.Name = "lblCHDailyLimit";
            this.lblCHDailyLimit.Size = new System.Drawing.Size(37, 13);
            this.lblCHDailyLimit.TabIndex = 6;
            this.lblCHDailyLimit.Text = "Limit :";
            this.lblCHDailyLimit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCHUsage
            // 
            this.lblCHUsage.AutoSize = true;
            this.lblCHUsage.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.lblCHUsage.Location = new System.Drawing.Point(62, 25);
            this.lblCHUsage.Name = "lblCHUsage";
            this.lblCHUsage.Size = new System.Drawing.Size(37, 12);
            this.lblCHUsage.TabIndex = 6;
            this.lblCHUsage.Text = "Usage :";
            this.lblCHUsage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCHLimit
            // 
            this.lblCHLimit.AutoSize = true;
            this.lblCHLimit.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.lblCHLimit.Location = new System.Drawing.Point(68, 12);
            this.lblCHLimit.Name = "lblCHLimit";
            this.lblCHLimit.Size = new System.Drawing.Size(31, 12);
            this.lblCHLimit.TabIndex = 6;
            this.lblCHLimit.Text = "Limit :";
            this.lblCHLimit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCHUsageValue
            // 
            this.lblCHUsageValue.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.lblCHUsageValue.Location = new System.Drawing.Point(100, 25);
            this.lblCHUsageValue.Name = "lblCHUsageValue";
            this.lblCHUsageValue.Size = new System.Drawing.Size(71, 12);
            this.lblCHUsageValue.TabIndex = 6;
            this.lblCHUsageValue.Text = "000000.00";
            this.lblCHUsageValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // crtCash
            // 
            chartArea3.Name = "ChartArea1";
            this.crtCash.ChartAreas.Add(chartArea3);
            this.crtCash.Location = new System.Drawing.Point(7, 16);
            this.crtCash.Name = "crtCash";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series3.IsVisibleInLegend = false;
            series3.Name = "Series2";
            series3.Points.Add(dataPoint5);
            series3.Points.Add(dataPoint6);
            this.crtCash.Series.Add(series3);
            this.crtCash.Size = new System.Drawing.Size(47, 42);
            this.crtCash.TabIndex = 10;
            this.crtCash.Text = "chart3";
            // 
            // lblCHLimitValue
            // 
            this.lblCHLimitValue.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.lblCHLimitValue.Location = new System.Drawing.Point(100, 12);
            this.lblCHLimitValue.Name = "lblCHLimitValue";
            this.lblCHLimitValue.Size = new System.Drawing.Size(71, 12);
            this.lblCHLimitValue.TabIndex = 6;
            this.lblCHLimitValue.Text = "000000.00";
            this.lblCHLimitValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.lblActiveInactiveStatus);
            this.groupBox4.Controls.Add(this.tglActive);
            this.groupBox4.Controls.Add(this.lblAddress);
            this.groupBox4.Controls.Add(this.lblDistrict);
            this.groupBox4.Controls.Add(this.lblPhone);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Location = new System.Drawing.Point(5, 34);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(239, 118);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Info";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(14, 96);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "Status :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label14.Visible = false;
            // 
            // OutletListItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panelHeader);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.panelFooter);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.gbxUsage);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MinimumSize = new System.Drawing.Size(756, 101);
            this.Name = "OutletListItem";
            this.Size = new System.Drawing.Size(926, 160);
            this.gbxUsage.ResumeLayout(false);
            this.gbxUsage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLgRemaining)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLgUsage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.crtDebit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.crtCredit)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panelHeader.ResumeLayout(false);
            this.panelHeader.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.crtCash)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox gbxUsage;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblDRDailyLimit;
        private System.Windows.Forms.Button btnDebitLimit;
        private System.Windows.Forms.Button btnCreditLimit;
        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Panel panelFooter;
        private System.Windows.Forms.Label lblCRDailyLimit;
        public System.Windows.Forms.TextBox txtCreditLimit;
        public System.Windows.Forms.TextBox txtDebitLimit;
        public System.Windows.Forms.Label lblNameValue;
        public System.Windows.Forms.Label lblCodeValue;
        public System.Windows.Forms.Label lblAddress;
        public System.Windows.Forms.Label lblDistrict;
        public System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pbLgUsage;
        private System.Windows.Forms.PictureBox pbLgRemaining;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblCRRemaining;
        private System.Windows.Forms.Label lblCRUsage;
        private System.Windows.Forms.Label lblCRLimit;
        public System.Windows.Forms.Label lblCRRemainingValue;
        public System.Windows.Forms.Label lblCRUsageValue;
        public System.Windows.Forms.Label lblCRLimitValue;
        public System.Windows.Forms.DataVisualization.Charting.Chart crtCredit;
        public MetroFramework.Controls.MetroToggle tglActive;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        public MetroFramework.Controls.MetroToggle tglCreditLimit;
        public MetroFramework.Controls.MetroToggle tglDebitLimit;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblDRRemaining;
        public System.Windows.Forms.Label lblDRRemainingValue;
        private System.Windows.Forms.Label lblDRUsage;
        public System.Windows.Forms.Label lblDRUsageValue;
        private System.Windows.Forms.Label lblDRLimit;
        public System.Windows.Forms.Label lblDRLimitValue;
        public System.Windows.Forms.DataVisualization.Charting.Chart crtDebit;
        private System.Windows.Forms.Label lblActiveInactiveStatus;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCashLimit;
        public System.Windows.Forms.TextBox txtCashLimit;
        public MetroFramework.Controls.MetroToggle tglCashLimit;
        private System.Windows.Forms.Label lblCHRemaining;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label lblCHRemainingValue;
        private System.Windows.Forms.Label lblCHDailyLimit;
        private System.Windows.Forms.Label lblCHUsage;
        private System.Windows.Forms.Label lblCHLimit;
        public System.Windows.Forms.Label lblCHUsageValue;
        public System.Windows.Forms.DataVisualization.Charting.Chart crtCash;
        public System.Windows.Forms.Label lblCHLimitValue;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label14;
    }
}
