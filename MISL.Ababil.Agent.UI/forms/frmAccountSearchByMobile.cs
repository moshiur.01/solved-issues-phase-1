﻿//using MetroFramework.Forms;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.UI.forms.ProgressUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MISL.Ababil.Agent.UI.forms
{
    public partial class frmAccountSearchByMobile : CustomForm
    {
        List<AccountResultSearchDto> _accountList;
        ServiceResult svcResult;

        public frmAccountSearchByMobile()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                bool retVal = CustomControlValidation.IsAllValid(true, true, txtMobileNumber);

                if (retVal == false)
                    return;

                dgvAccountList.Rows.Clear();
                _accountList = new List<AccountResultSearchDto>();
                int rowIndex = 0;

                ProgressUIManager.ShowProgress(this);
                svcResult = AccountInformationService.GetAccountListByMobileNumber(txtMobileNumber.Text.Trim());
                ProgressUIManager.CloseProgress();

                if (svcResult.Success)
                {
                    _accountList = svcResult.ReturnedObject as List<AccountResultSearchDto>;

                    foreach (AccountResultSearchDto account in _accountList)
                    {
                        dgvAccountList.Rows.Add();
                        dgvAccountList["gridColAccountType", rowIndex].Value = account.accountType;
                        dgvAccountList["gridColAccountNo", rowIndex].Value = account.accountNumber;
                        dgvAccountList["gridColAccountTitle", rowIndex].Value = account.accountTitle;
                        rowIndex++;
                    }

                    dgvAccountList.Columns[0].DefaultCellStyle.Alignment =
                        dgvAccountList.Columns[1].DefaultCellStyle.Alignment =
                            dgvAccountList.Columns[2].DefaultCellStyle.Alignment =
                                DataGridViewContentAlignment.MiddleCenter;

                    dgvAccountList.Columns[0].HeaderCell.Style.Alignment =
                        dgvAccountList.Columns[1].HeaderCell.Style.Alignment =
                            dgvAccountList.Columns[2].HeaderCell.Style.Alignment =
                                DataGridViewContentAlignment.MiddleCenter;
                }
                else
                {
                    Message.showError(svcResult.Message);
                }
            }
            catch (Exception exp)
            {
                ProgressUIManager.CloseProgress();
                Message.showError(exp.Message);
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmAccountSearchByMobile_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }
    }
}
