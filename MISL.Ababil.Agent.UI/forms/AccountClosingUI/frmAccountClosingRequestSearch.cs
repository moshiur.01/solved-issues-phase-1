﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.UI.forms.ProgressUI;

namespace MISL.Ababil.Agent.UI.forms.AccountClosingUI
{
    public partial class frmAccountClosingRequestSearch : CustomForm
    {
        ConsumerServices _objConsumerServices = new ConsumerServices();
        List<AccountClosingSearchResultDto> _accountClosingSearchResultDtos = new List<AccountClosingSearchResultDto>();
        int _columnLoaded = 0;
        private string _customerTitleQuickFix;

        public frmAccountClosingRequestSearch(Packet packet)
        {
            InitializeComponent();
            //ConfigureValidation();

            SetupDataLoad();
            fillSetupData();
            this.MinimizeBox = false;
        }

        private void SetupDataLoad()
        {
            SetAgent();
            SetOutlet();
        }

        private void SetAgent()
        {
            try
            {                
                BindingSource bs = new BindingSource { DataSource = new AgentServices().getAgentInfoBranchWise() };
                UtilityServices.fillComboBox(cmbAgent, bs, "businessName", "id");
                if (SessionInfo.userBasicInformation.userCategory == UserCategory.SubAgentUser)
                {
                    cmbAgent.SelectedValue = SessionInfo.userBasicInformation.agent.id;
                    cmbAgent.Enabled = false;
                }
                else
                {
                    cmbAgent.SelectedIndex = -1;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
                this.Close();
            }
        }

        private void fillSetupData()
        {
            List<AppStatus> ds = Enum.GetValues(typeof(AppStatus)).Cast<AppStatus>().ToList();
            ds.RemoveAt(ds.Count - 1);
            cmbApplicationStatus.DataSource = ds;

            //if
            //    (
            //        SessionInfo.userBasicInformation.userType == AgentUserType.Branch
            //        ||
            //        SessionInfo.userBasicInformation.userType == AgentUserType.Admin
            //    )
            if
                (
                    SessionInfo.userBasicInformation.userCategory == UserCategory.BranchUser
                    &&
                    SessionInfo.userBasicInformation.bankUserType == BankUserType.BranchUser
                )
            {
                cmbApplicationStatus.SelectedItem = ApplicationStatus.verified;
            }

            //if
            //    (
            //        SessionInfo.userBasicInformation.userType == AgentUserType.FieldOfficer
            //    )
            else if
                (
                    SessionInfo.userBasicInformation.userCategory == UserCategory.BranchUser
                    &&
                    SessionInfo.userBasicInformation.bankUserType == BankUserType.FieldUser
                )
            {
                cmbApplicationStatus.SelectedItem = ApplicationStatus.submitted;
                cmbApplicationStatus.Enabled = false;
            }

            //if
            //    (
            //        SessionInfo.userBasicInformation.userType == AgentUserType.Outlet
            //    )
            else if
                (
                    SessionInfo.userBasicInformation.userCategory == UserCategory.SubAgentUser
                 )
            {
                cmbApplicationStatus.SelectedItem = ApplicationStatus.draft;
            }
        }

        private void SetOutlet()
        {
            try
            {
                AgentServices agentServices = new AgentServices();
                AgentInformation agentInformation = new AgentInformation();
                if (SessionInfo.userBasicInformation.userCategory == UserCategory.SubAgentUser)
                {
                    agentInformation = agentServices.getAgentInfoById(SessionInfo.userBasicInformation.agent.id.ToString());
                    if (agentInformation != null)
                    {
                        BindingSource bs = new BindingSource { DataSource = agentInformation.subAgents };
                        {
                            try
                            {
                                SubAgentInformation saiSelect = new SubAgentInformation { name = "(Select)" };
                                agentInformation.subAgents.Insert(0, saiSelect);
                            }
                            catch
                            {
                                // ignored
                            }
                        }
                        UtilityServices.fillComboBox(cmbOutlet, bs, "name", "id");
                        if (cmbOutlet.Items.Count > 0)
                        {
                            cmbOutlet.SelectedValue = SessionInfo.userBasicInformation.outlet.id;
                            cmbOutlet.Enabled = false;
                            return;
                        }
                    }
                }
                else
                {
                    SubAgentServices subAgentServices = new SubAgentServices();
                    BindingSource bs = new BindingSource { DataSource = subAgentServices.GetAllSubAgents() };
                    {
                        try
                        {
                            SubAgentInformation saiSelect = new SubAgentInformation { name = "(Select)" };
                            agentInformation.subAgents.Insert(0, saiSelect);
                        }
                        catch
                        {
                            // ignored
                        }
                    }
                    UtilityServices.fillComboBox(cmbOutlet, bs, "name", "id");
                    if (cmbOutlet.Items.Count > 0)
                    {
                        cmbOutlet.SelectedIndex = -1;
                    }
                }
            }
            catch
            {
                // ignored
            }
        }

        private void frmPendingApplication_Load(object sender, EventArgs e)
        {
            txtMobileNo.MaxLength = CommonRules.mobileNoLength;
        }

        //~//~//~//class ConsumerApplicationComparer : IComparer<ConsumerApplication>
        //~//~//~//{
        //~//~//~//    private readonly string _memberName = string.Empty; // the member name to be sorted
        //~//~//~//    private readonly SortOrder _sortOrder = SortOrder.None;
        //~//~//~//
        //~//~//~//    public ConsumerApplicationComparer(string memberName, SortOrder sortingOrder)
        //~//~//~//    {
        //~//~//~//        _memberName = memberName;
        //~//~//~//        _sortOrder = sortingOrder;
        //~//~//~//    }
        //~//~//~//
        //~//~//~//    public int Compare(ConsumerApplication consumerApplication1, ConsumerApplication consumerApplication2)
        //~//~//~//    {
        //~//~//~//        if (_sortOrder != SortOrder.Ascending)
        //~//~//~//        {
        //~//~//~//            var tmp = consumerApplication1;
        //~//~//~//            consumerApplication1 = consumerApplication2;
        //~//~//~//            consumerApplication2 = tmp;
        //~//~//~//        }
        //~//~//~//
        //~//~//~//        switch (_memberName)
        //~//~//~//        {
        //~//~//~//            case "creationDate":
        //~//~//~//                return consumerApplication1.creationDate.Value.CompareTo(consumerApplication2.creationDate.Value);
        //~//~//~//            default:
        //~//~//~//                return consumerApplication1.creationDate.Value.CompareTo(consumerApplication2.creationDate.Value);
        //~//~//~//        }
        //~//~//~//    }
        //~//~//~//}

        private void LoadAllApplications(AccountClosingSearchDto accountClosingSearchDto)
        {
            try
            {
                _accountClosingSearchResultDtos = new AccountClosingServices().GetAccountClosingRequests(accountClosingSearchDto);
                if (_accountClosingSearchResultDtos != null)
                {
                    dataGridView1.DataSource = null;
                    dataGridView1.Columns.Clear();
                    dataGridView1.DataSource =
                        _accountClosingSearchResultDtos.Select(o => new AccountClosingSearchResultGrid(o)
                        {
                            requestDate = o.requestDate,
                            refNo = o.refNo,
                            agentName = o.agentName,
                            outletName = o.outletName,
                            accountType = o.accountType,
                            accountNo = o.accountNo,
                            //accountTitle = o.accountTitle,
                            appStatus = o.appStatus,
                            filter =
                                (
                                    o.requestDate +
                                    o.refNo +
                                    o.agentName +
                                    o.outletName +
                                    o.accountType +
                                    o.accountNo +
                                    o.accountTitle +
                                    o.appStatus.ToString()
                                ).ToLower()

                        }).ToList();
                    dataGridView1.Columns.Add(new DataGridViewButtonColumn
                    {
                        Text = "Open",
                        UseColumnTextForButtonValue = true
                    });

                    DataGridViewColumn dataGridViewColumn = dataGridView1.Columns["filter"];
                    if (dataGridViewColumn != null)
                    {
                        dataGridViewColumn.Visible = false;
                    }
                }
                else
                {
                    MsgBox.showConfirmation("No request available");
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            btnSearch.Enabled = false;
            Search();
            btnSearch.Enabled = true;
        }

        private void Search()
        {
            //if (validationCheck())
            //{
            AccountClosingSearchDto accountClosingSearchDto = new AccountClosingSearchDto(); //SessionInfo.rights.Any(p => p == Rights.CREATE_BANK_USER.ToString()

            //accountClosingSearchDto.agentUserType = SessionInfo.userBasicInformation.userType;

            if (cmbOutlet.SelectedValue != null)
            {
                accountClosingSearchDto.outletId = (long)cmbOutlet.SelectedValue;
            }
            else
            {
                accountClosingSearchDto.outletId = null;
            }
            if (cmbAgent.SelectedValue != null)
            {
                accountClosingSearchDto.agentId = (long)cmbOutlet.SelectedValue;
            }
            else
            {
                accountClosingSearchDto.agentId = null;
            }
            accountClosingSearchDto.mobileNumber = txtMobileNo.Text;
            accountClosingSearchDto.refNo = texReferenceNo.Text;
            accountClosingSearchDto.fromDate = UtilityServices.GetLongDate(dtpFromDate.Value);
            accountClosingSearchDto.toDate = UtilityServices.GetLongDate(dtpToDate.Value);
            accountClosingSearchDto.appStatus = (AppStatus?)cmbApplicationStatus.SelectedItem;

            ProgressUIManager.ShowProgress(this);
            LoadAllApplications(accountClosingSearchDto);
            ProgressUIManager.CloseProgress();

            lblItemsFound.Text = "   Item(s) Found: " + dataGridView1.Rows.Count.ToString();
            //}                         

            try
            {
                customDataGridViewHeader.ExportReportSubtitleThree = "Consumer Application List";
                DataGridHeaderUtility.ReportHederDataExportToPdf(customDataGridViewHeader);
            }
            catch (Exception ex)
            {
                //suppressed
            }
        }


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int selRow = e.RowIndex;
                DataGridView senderGrid = (DataGridView)sender;

                if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
                {
                    AccountClosingSearchResultDto accountClosingSearchResultDto =
                        _accountClosingSearchResultDtos[e.RowIndex];
                    if (accountClosingSearchResultDto.appStatus == AppStatus.Apply)
                    {
                        //AccountClosingRequestDto accountClosingRequestDto =
                        //    new AccountClosingServices().GetAccountClosingRequestDtoByRefNumber(
                        //        accountClosingSearchResultDto.refNo);
                        //Packet packet = new Packet() { otherObj = accountClosingRequestDto };
                        Packet packet = new Packet() { otherObj = accountClosingSearchResultDto };
                        //Packet packet = new Packet();

                        switch (SessionInfo.userBasicInformation.userCategory)
                        {
                            case UserCategory.BranchUser:
                                packet.actionType = FormActionType.Verify;
                                break;
                            case UserCategory.AgentUser:

                                break;
                            case UserCategory.SubAgentUser:
                                packet.actionType = accountClosingSearchResultDto.appStatus != AppStatus.Correction
                                    ? FormActionType.View
                                    : FormActionType.Edit;
                                break;
                        }

                        frmAccountClosingRequest frm = new frmAccountClosingRequest(packet);
                        if (frm.ShowDialog(this) != DialogResult.No)
                        {
                            btnSearch.Enabled = false;
                            btnSearch.Enabled = true;
                        }

                        Search();

                        try
                        {
                            dataGridView1.Rows[selRow].Selected = true;
                        }
                        catch
                        {
                            // ignored
                        }
                    }
                    else
                    {
                        MsgBox.showInformation("This request is already processed!");
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void txtNationalId_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) && (Keys)e.KeyChar != Keys.Back && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            base.OnKeyPress(e);
        }

        private void txtMobileNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) && (Keys)e.KeyChar != Keys.Back && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            base.OnKeyPress(e);
        }

        private void frmPendingApplication_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void ClearAll()
        {
            _accountClosingSearchResultDtos = new List<AccountClosingSearchResultDto>();
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
            texReferenceNo.Clear();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ClearAll();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Focused && e.RowIndex >= 0)
            {
                (dataGridView1.Rows[e.RowIndex].Cells[dataGridView1.ColumnCount - 1]).Value = imageList1.Images[1];
            }
        }

        private void dataGridView1_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Focused)
            {
                (dataGridView1.Rows[e.RowIndex].Cells[dataGridView1.ColumnCount - 1]).Value = imageList1.Images[0];
            }
        }
        private void dataGridView1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1_CellClick(sender, e);
        }

        public class AccountClosingSearchResultGrid
        {
            //public string requestDate { get; set; }
            //public string refNo { get; set; }
            //public string outlet { get; set; }
            //public string customerTitle { get; set; }
            //public AppStatus appStatus { get; set; }


            public string requestDate { get; set; }
            public string refNo { get; set; }
            public string agentName { get; set; }
            public string outletName { get; set; }
            public AccountType accountType { get; set; }
            public string accountNo { get; set; }
            //public string accountTitle { get; set; }
            public AppStatus appStatus { get; set; }


            private AccountClosingSearchResultDto _obj;
            public string filter { get; set; }



            public AccountClosingSearchResultGrid(AccountClosingSearchResultDto obj)
            {
                _obj = obj;
            }

            public AccountClosingSearchResultDto GetModel()
            {
                return _obj;
            }
        }
    }
}