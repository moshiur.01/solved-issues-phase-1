﻿namespace MISL.Ababil.Agent.UI.forms.AccountClosingUI
{
    partial class frmAccountClosingRequest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.rbtnDeposit = new System.Windows.Forms.RadioButton();
            this.rBtnITD = new System.Windows.Forms.RadioButton();
            this.rBtnMTDR = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.lblAccountNumber = new System.Windows.Forms.Label();
            this.pbxPicture = new System.Windows.Forms.PictureBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblAuthenticationWay = new System.Windows.Forms.Label();
            this.gbx = new System.Windows.Forms.GroupBox();
            this.txtAccountNumber = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.gbxAuthenticationWay = new System.Windows.Forms.GroupBox();
            this.rBtnAuthOTP = new System.Windows.Forms.RadioButton();
            this.rBtnAuthBiometric = new System.Windows.Forms.RadioButton();
            this.gbxAccountInformation = new System.Windows.Forms.GroupBox();
            this.lblNameValue = new System.Windows.Forms.Label();
            this.lblBalanceValue = new System.Windows.Forms.Label();
            this.lblBalance = new System.Windows.Forms.Label();
            this.gbxFingerprintInformation = new System.Windows.Forms.GroupBox();
            this.lblRequiredFingerPrint = new System.Windows.Forms.Label();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnComment = new System.Windows.Forms.Button();
            this.btnReject = new System.Windows.Forms.Button();
            this.btnAccept = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnClear = new System.Windows.Forms.Button();
            this.secondaryTitleLabel = new System.Windows.Forms.Label();
            this.btnManageDoc = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            ((System.ComponentModel.ISupportInitialize)(this.pbxPicture)).BeginInit();
            this.gbx.SuspendLayout();
            this.gbxAuthenticationWay.SuspendLayout();
            this.gbxAccountInformation.SuspendLayout();
            this.gbxFingerprintInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // rbtnDeposit
            // 
            this.rbtnDeposit.AutoSize = true;
            this.rbtnDeposit.Checked = true;
            this.rbtnDeposit.Location = new System.Drawing.Point(144, 19);
            this.rbtnDeposit.Name = "rbtnDeposit";
            this.rbtnDeposit.Size = new System.Drawing.Size(61, 17);
            this.rbtnDeposit.TabIndex = 1;
            this.rbtnDeposit.TabStop = true;
            this.rbtnDeposit.Text = "Deposit";
            this.rbtnDeposit.UseVisualStyleBackColor = true;
            // 
            // rBtnITD
            // 
            this.rBtnITD.AutoSize = true;
            this.rBtnITD.Location = new System.Drawing.Point(217, 19);
            this.rBtnITD.Name = "rBtnITD";
            this.rBtnITD.Size = new System.Drawing.Size(43, 17);
            this.rBtnITD.TabIndex = 2;
            this.rBtnITD.Text = "ITD";
            this.rBtnITD.UseVisualStyleBackColor = true;
            // 
            // rBtnMTDR
            // 
            this.rBtnMTDR.AutoSize = true;
            this.rBtnMTDR.Location = new System.Drawing.Point(272, 19);
            this.rBtnMTDR.Name = "rBtnMTDR";
            this.rBtnMTDR.Size = new System.Drawing.Size(57, 17);
            this.rBtnMTDR.TabIndex = 3;
            this.rBtnMTDR.Text = "MTDR";
            this.rBtnMTDR.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Account Type :";
            // 
            // lblAccountNumber
            // 
            this.lblAccountNumber.AutoSize = true;
            this.lblAccountNumber.Location = new System.Drawing.Point(44, 51);
            this.lblAccountNumber.Name = "lblAccountNumber";
            this.lblAccountNumber.Size = new System.Drawing.Size(93, 13);
            this.lblAccountNumber.TabIndex = 5;
            this.lblAccountNumber.Text = "Account Number :";
            // 
            // pbxPicture
            // 
            this.pbxPicture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbxPicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbxPicture.Image = global::MISL.Ababil.Agent.UI.Properties.Resources.user_account___Copy;
            this.pbxPicture.Location = new System.Drawing.Point(521, 19);
            this.pbxPicture.Name = "pbxPicture";
            this.pbxPicture.Size = new System.Drawing.Size(106, 120);
            this.pbxPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxPicture.TabIndex = 9;
            this.pbxPicture.TabStop = false;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(81, 22);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(56, 20);
            this.lblName.TabIndex = 5;
            this.lblName.Text = "Name :";
            // 
            // lblAuthenticationWay
            // 
            this.lblAuthenticationWay.AutoSize = true;
            this.lblAuthenticationWay.Location = new System.Drawing.Point(31, 19);
            this.lblAuthenticationWay.Name = "lblAuthenticationWay";
            this.lblAuthenticationWay.Size = new System.Drawing.Size(106, 13);
            this.lblAuthenticationWay.TabIndex = 5;
            this.lblAuthenticationWay.Text = "Authentication Way :";
            // 
            // gbx
            // 
            this.gbx.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbx.Controls.Add(this.txtAccountNumber);
            this.gbx.Controls.Add(this.lblAccountNumber);
            this.gbx.Controls.Add(this.gbxAuthenticationWay);
            this.gbx.Controls.Add(this.label1);
            this.gbx.Controls.Add(this.rBtnMTDR);
            this.gbx.Controls.Add(this.rBtnITD);
            this.gbx.Controls.Add(this.rbtnDeposit);
            this.gbx.Location = new System.Drawing.Point(12, 44);
            this.gbx.Name = "gbx";
            this.gbx.Size = new System.Drawing.Size(638, 83);
            this.gbx.TabIndex = 12;
            this.gbx.TabStop = false;
            // 
            // txtAccountNumber
            // 
            this.txtAccountNumber.BackColor = System.Drawing.Color.White;
            this.txtAccountNumber.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtAccountNumber.InputScopeAllowEmpty = false;
            this.txtAccountNumber.InputScopeCustomString = null;
            this.txtAccountNumber.IsValid = null;
            this.txtAccountNumber.Location = new System.Drawing.Point(144, 48);
            this.txtAccountNumber.MaxLength = 32767;
            this.txtAccountNumber.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtAccountNumber.Name = "txtAccountNumber";
            this.txtAccountNumber.PromptText = "(Type here...)";
            this.txtAccountNumber.ReadOnly = false;
            this.txtAccountNumber.ShowMandatoryMark = false;
            this.txtAccountNumber.Size = new System.Drawing.Size(183, 21);
            this.txtAccountNumber.TabIndex = 6;
            this.txtAccountNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtAccountNumber.UpperCaseOnly = false;
            this.txtAccountNumber.ValidationErrorMessage = "Validation Error!";
            this.txtAccountNumber.Leave += new System.EventHandler(this.txtAccountNumber_Leave);
            // 
            // gbxAuthenticationWay
            // 
            this.gbxAuthenticationWay.Controls.Add(this.rBtnAuthOTP);
            this.gbxAuthenticationWay.Controls.Add(this.rBtnAuthBiometric);
            this.gbxAuthenticationWay.Controls.Add(this.lblAuthenticationWay);
            this.gbxAuthenticationWay.Location = new System.Drawing.Point(593, 19);
            this.gbxAuthenticationWay.Name = "gbxAuthenticationWay";
            this.gbxAuthenticationWay.Size = new System.Drawing.Size(638, 45);
            this.gbxAuthenticationWay.TabIndex = 13;
            this.gbxAuthenticationWay.TabStop = false;
            this.gbxAuthenticationWay.Visible = false;
            // 
            // rBtnAuthOTP
            // 
            this.rBtnAuthOTP.AutoSize = true;
            this.rBtnAuthOTP.Location = new System.Drawing.Point(231, 17);
            this.rBtnAuthOTP.Name = "rBtnAuthOTP";
            this.rBtnAuthOTP.Size = new System.Drawing.Size(47, 17);
            this.rBtnAuthOTP.TabIndex = 7;
            this.rBtnAuthOTP.Text = "OTP";
            this.rBtnAuthOTP.UseVisualStyleBackColor = true;
            // 
            // rBtnAuthBiometric
            // 
            this.rBtnAuthBiometric.AutoSize = true;
            this.rBtnAuthBiometric.Checked = true;
            this.rBtnAuthBiometric.Location = new System.Drawing.Point(144, 17);
            this.rBtnAuthBiometric.Name = "rBtnAuthBiometric";
            this.rBtnAuthBiometric.Size = new System.Drawing.Size(74, 17);
            this.rBtnAuthBiometric.TabIndex = 6;
            this.rBtnAuthBiometric.TabStop = true;
            this.rBtnAuthBiometric.Text = "Fingerprint";
            this.rBtnAuthBiometric.UseVisualStyleBackColor = true;
            // 
            // gbxAccountInformation
            // 
            this.gbxAccountInformation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxAccountInformation.Controls.Add(this.pbxPicture);
            this.gbxAccountInformation.Controls.Add(this.lblNameValue);
            this.gbxAccountInformation.Controls.Add(this.lblName);
            this.gbxAccountInformation.Controls.Add(this.lblBalanceValue);
            this.gbxAccountInformation.Controls.Add(this.lblBalance);
            this.gbxAccountInformation.Location = new System.Drawing.Point(12, 133);
            this.gbxAccountInformation.Name = "gbxAccountInformation";
            this.gbxAccountInformation.Size = new System.Drawing.Size(638, 150);
            this.gbxAccountInformation.TabIndex = 14;
            this.gbxAccountInformation.TabStop = false;
            this.gbxAccountInformation.Text = "Account Information";
            // 
            // lblNameValue
            // 
            this.lblNameValue.AutoSize = true;
            this.lblNameValue.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameValue.Location = new System.Drawing.Point(138, 22);
            this.lblNameValue.Name = "lblNameValue";
            this.lblNameValue.Size = new System.Drawing.Size(66, 20);
            this.lblNameValue.TabIndex = 5;
            this.lblNameValue.Text = "<name>";
            // 
            // lblBalanceValue
            // 
            this.lblBalanceValue.AutoSize = true;
            this.lblBalanceValue.Location = new System.Drawing.Point(138, 62);
            this.lblBalanceValue.Name = "lblBalanceValue";
            this.lblBalanceValue.Size = new System.Drawing.Size(57, 13);
            this.lblBalanceValue.TabIndex = 5;
            this.lblBalanceValue.Text = "<balance>";
            // 
            // lblBalance
            // 
            this.lblBalance.AutoSize = true;
            this.lblBalance.Location = new System.Drawing.Point(85, 62);
            this.lblBalance.Name = "lblBalance";
            this.lblBalance.Size = new System.Drawing.Size(52, 13);
            this.lblBalance.TabIndex = 5;
            this.lblBalance.Text = "Balance :";
            // 
            // gbxFingerprintInformation
            // 
            this.gbxFingerprintInformation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxFingerprintInformation.Controls.Add(this.lblRequiredFingerPrint);
            this.gbxFingerprintInformation.Controls.Add(this.dgv);
            this.gbxFingerprintInformation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.gbxFingerprintInformation.Location = new System.Drawing.Point(12, 347);
            this.gbxFingerprintInformation.Name = "gbxFingerprintInformation";
            this.gbxFingerprintInformation.Size = new System.Drawing.Size(638, 153);
            this.gbxFingerprintInformation.TabIndex = 18;
            this.gbxFingerprintInformation.TabStop = false;
            this.gbxFingerprintInformation.Tag = "538, 172";
            this.gbxFingerprintInformation.Text = "Fingerprint Information";
            // 
            // lblRequiredFingerPrint
            // 
            this.lblRequiredFingerPrint.AutoSize = true;
            this.lblRequiredFingerPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.lblRequiredFingerPrint.Location = new System.Drawing.Point(291, 0);
            this.lblRequiredFingerPrint.Name = "lblRequiredFingerPrint";
            this.lblRequiredFingerPrint.Size = new System.Drawing.Size(122, 13);
            this.lblRequiredFingerPrint.TabIndex = 1;
            this.lblRequiredFingerPrint.Text = "<lblRequiredFingerPrint>";
            // 
            // dgv
            // 
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.ColumnHeadersHeight = 28;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id});
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Location = new System.Drawing.Point(10, 18);
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.ShowEditingIcon = false;
            this.dgv.Size = new System.Drawing.Size(618, 124);
            this.dgv.TabIndex = 0;
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick);
            // 
            // id
            // 
            this.id.HeaderText = "Headid";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSubmit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnSubmit.FlatAppearance.BorderSize = 0;
            this.btnSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubmit.ForeColor = System.Drawing.Color.White;
            this.btnSubmit.Location = new System.Drawing.Point(251, 3);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(110, 28);
            this.btnSubmit.TabIndex = 10;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = false;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(483, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(110, 28);
            this.btnClose.TabIndex = 10;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnComment
            // 
            this.btnComment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnComment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnComment.FlatAppearance.BorderSize = 0;
            this.btnComment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnComment.ForeColor = System.Drawing.Color.White;
            this.btnComment.Location = new System.Drawing.Point(12, 510);
            this.btnComment.Name = "btnComment";
            this.btnComment.Size = new System.Drawing.Size(110, 28);
            this.btnComment.TabIndex = 10;
            this.btnComment.Text = "Comment (0)";
            this.btnComment.UseVisualStyleBackColor = false;
            this.btnComment.Click += new System.EventHandler(this.btnComment_Click);
            // 
            // btnReject
            // 
            this.btnReject.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReject.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnReject.FlatAppearance.BorderSize = 0;
            this.btnReject.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReject.ForeColor = System.Drawing.Color.White;
            this.btnReject.Location = new System.Drawing.Point(135, 3);
            this.btnReject.Name = "btnReject";
            this.btnReject.Size = new System.Drawing.Size(110, 28);
            this.btnReject.TabIndex = 10;
            this.btnReject.Text = "Reject";
            this.btnReject.UseVisualStyleBackColor = false;
            this.btnReject.Click += new System.EventHandler(this.btnReject_Click);
            // 
            // btnAccept
            // 
            this.btnAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAccept.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnAccept.FlatAppearance.BorderSize = 0;
            this.btnAccept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAccept.ForeColor = System.Drawing.Color.White;
            this.btnAccept.Location = new System.Drawing.Point(19, 3);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(110, 28);
            this.btnAccept.TabIndex = 10;
            this.btnAccept.Text = "Accept";
            this.btnAccept.UseVisualStyleBackColor = false;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.btnClose);
            this.flowLayoutPanel1.Controls.Add(this.btnClear);
            this.flowLayoutPanel1.Controls.Add(this.btnSubmit);
            this.flowLayoutPanel1.Controls.Add(this.btnReject);
            this.flowLayoutPanel1.Controls.Add(this.btnAccept);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(59, 507);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(596, 34);
            this.flowLayoutPanel1.TabIndex = 19;
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(367, 3);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(110, 28);
            this.btnClear.TabIndex = 11;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // secondaryTitleLabel
            // 
            this.secondaryTitleLabel.AutoSize = true;
            this.secondaryTitleLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.secondaryTitleLabel.Font = new System.Drawing.Font("Segoe UI Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondaryTitleLabel.ForeColor = System.Drawing.Color.White;
            this.secondaryTitleLabel.Location = new System.Drawing.Point(3, 4);
            this.secondaryTitleLabel.Name = "secondaryTitleLabel";
            this.secondaryTitleLabel.Size = new System.Drawing.Size(211, 25);
            this.secondaryTitleLabel.TabIndex = 20;
            this.secondaryTitleLabel.Text = "Account Closing Request";
            // 
            // btnManageDoc
            // 
            this.btnManageDoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnManageDoc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnManageDoc.FlatAppearance.BorderSize = 0;
            this.btnManageDoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnManageDoc.ForeColor = System.Drawing.Color.White;
            this.btnManageDoc.Location = new System.Drawing.Point(141, 15);
            this.btnManageDoc.Name = "btnManageDoc";
            this.btnManageDoc.Size = new System.Drawing.Size(151, 28);
            this.btnManageDoc.TabIndex = 10;
            this.btnManageDoc.Text = "Manage Document";
            this.btnManageDoc.UseVisualStyleBackColor = false;
            this.btnManageDoc.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnManageDoc);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 289);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(638, 52);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Attachments";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(70, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Documents :";
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = this.secondaryTitleLabel;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(663, 36);
            this.customTitlebar1.TabIndex = 21;
            this.customTitlebar1.TabStop = false;
            // 
            // frmAccountClosingRequest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(663, 550);
            this.ControlBox = false;
            this.Controls.Add(this.btnComment);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.secondaryTitleLabel);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.gbx);
            this.Controls.Add(this.gbxAccountInformation);
            this.Controls.Add(this.gbxFingerprintInformation);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAccountClosingRequest";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "663, 550";
            this.Text = "Account Closing Request";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmAccountClosingRequest_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pbxPicture)).EndInit();
            this.gbx.ResumeLayout(false);
            this.gbx.PerformLayout();
            this.gbxAuthenticationWay.ResumeLayout(false);
            this.gbxAuthenticationWay.PerformLayout();
            this.gbxAccountInformation.ResumeLayout(false);
            this.gbxAccountInformation.PerformLayout();
            this.gbxFingerprintInformation.ResumeLayout(false);
            this.gbxFingerprintInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RadioButton rbtnDeposit;
        private System.Windows.Forms.RadioButton rBtnITD;
        private System.Windows.Forms.RadioButton rBtnMTDR;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblAccountNumber;
        private Agent.CustomControls.CustomTextBox txtAccountNumber;
        private System.Windows.Forms.PictureBox pbxPicture;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblAuthenticationWay;
        private System.Windows.Forms.GroupBox gbx;
        private System.Windows.Forms.GroupBox gbxAuthenticationWay;
        private System.Windows.Forms.RadioButton rBtnAuthOTP;
        private System.Windows.Forms.RadioButton rBtnAuthBiometric;
        private System.Windows.Forms.GroupBox gbxAccountInformation;
        private System.Windows.Forms.GroupBox gbxFingerprintInformation;
        private System.Windows.Forms.Label lblRequiredFingerPrint;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Label lblBalance;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblNameValue;
        private System.Windows.Forms.Label lblBalanceValue;
        private System.Windows.Forms.Button btnComment;
        private System.Windows.Forms.Button btnReject;
        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label secondaryTitleLabel;
        private Agent.CustomControls.CustomTitlebar customTitlebar1;
        private System.Windows.Forms.Button btnManageDoc;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.Button btnClear;
    }
}