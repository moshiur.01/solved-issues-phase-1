﻿namespace MISL.Ababil.Agent.UI.forms.AccountClosingUI
{
    partial class frmTermAccountClosing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtLienAmount = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtQuardAmount = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtAccType = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtName = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtLastRenewalDate = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtAcMaturityDate = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtAccClosingDate = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtPeriod = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtLastExciseDuty = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtCurrentAddressline2 = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtCurrentAddressLine1 = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtAccOpendate = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtAccNo = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.txtClosingVoucherNetPayable = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtClosingVoucherVat = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtClosingVoucherClosingChg = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtClosingVoucherExciseDuty = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtClosingVoucherTaxAdjustedDr = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtClosingVoucherTaxAdjustedCr = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtClosingVoucherProfitDr = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtClosingVoucherProfitCr = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtClosingVoucherAccBalanceCr = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ChkBoxEnableProfit = new System.Windows.Forms.CheckBox();
            this.txtProfitToClientProvisionalProfit = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtProfitToClientActualProfit = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rbAgentAccount = new System.Windows.Forms.RadioButton();
            this.rbLinkAccount = new System.Windows.Forms.RadioButton();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnCloseAcc = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtTransactionInfoPrincipalCr = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtTransactionInfoProfitCr = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtTransactionInfoProfitDr = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtTransactionInfoPrincipalDr = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtTransactionInfoTaxCr = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtTransactionInfoExciseDutyCr = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtTransactionInfoOtherCr = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtTransactionInfoTaxDr = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.txtTransactionInfoExciseDutyDr = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.txtTransactionInfoOtherDr = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.txtTransactionInfoTotalBalance = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(1043, 26);
            this.customTitlebar1.TabIndex = 0;
            this.customTitlebar1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtLienAmount);
            this.groupBox2.Controls.Add(this.txtQuardAmount);
            this.groupBox2.Controls.Add(this.txtAccType);
            this.groupBox2.Controls.Add(this.txtName);
            this.groupBox2.Controls.Add(this.txtLastRenewalDate);
            this.groupBox2.Controls.Add(this.txtAcMaturityDate);
            this.groupBox2.Controls.Add(this.txtAccClosingDate);
            this.groupBox2.Controls.Add(this.txtPeriod);
            this.groupBox2.Controls.Add(this.txtLastExciseDuty);
            this.groupBox2.Controls.Add(this.txtCurrentAddressline2);
            this.groupBox2.Controls.Add(this.txtCurrentAddressLine1);
            this.groupBox2.Controls.Add(this.txtAccOpendate);
            this.groupBox2.Controls.Add(this.txtAccNo);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(12, 32);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1022, 138);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Account Information";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // txtLienAmount
            // 
            this.txtLienAmount.BackColor = System.Drawing.SystemColors.Window;
            this.txtLienAmount.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtLienAmount.InputScopeAllowEmpty = false;
            this.txtLienAmount.InputScopeCustomString = null;
            this.txtLienAmount.IsValid = null;
            this.txtLienAmount.Location = new System.Drawing.Point(285, 105);
            this.txtLienAmount.MaxLength = 32767;
            this.txtLienAmount.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtLienAmount.Name = "txtLienAmount";
            this.txtLienAmount.PromptText = "(Type here...)";
            this.txtLienAmount.ReadOnly = true;
            this.txtLienAmount.ShowMandatoryMark = false;
            this.txtLienAmount.Size = new System.Drawing.Size(121, 21);
            this.txtLienAmount.TabIndex = 0;
            this.txtLienAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtLienAmount.UpperCaseOnly = false;
            this.txtLienAmount.ValidationErrorMessage = "Validation Error!";
            // 
            // txtQuardAmount
            // 
            this.txtQuardAmount.BackColor = System.Drawing.SystemColors.Window;
            this.txtQuardAmount.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtQuardAmount.InputScopeAllowEmpty = false;
            this.txtQuardAmount.InputScopeCustomString = null;
            this.txtQuardAmount.IsValid = null;
            this.txtQuardAmount.Location = new System.Drawing.Point(87, 105);
            this.txtQuardAmount.MaxLength = 32767;
            this.txtQuardAmount.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtQuardAmount.Name = "txtQuardAmount";
            this.txtQuardAmount.PromptText = "(Type here...)";
            this.txtQuardAmount.ReadOnly = true;
            this.txtQuardAmount.ShowMandatoryMark = false;
            this.txtQuardAmount.Size = new System.Drawing.Size(121, 21);
            this.txtQuardAmount.TabIndex = 0;
            this.txtQuardAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtQuardAmount.UpperCaseOnly = false;
            this.txtQuardAmount.ValidationErrorMessage = "Validation Error!";
            // 
            // txtAccType
            // 
            this.txtAccType.BackColor = System.Drawing.SystemColors.Window;
            this.txtAccType.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtAccType.InputScopeAllowEmpty = false;
            this.txtAccType.InputScopeCustomString = null;
            this.txtAccType.IsValid = null;
            this.txtAccType.Location = new System.Drawing.Point(87, 77);
            this.txtAccType.MaxLength = 32767;
            this.txtAccType.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtAccType.Name = "txtAccType";
            this.txtAccType.PromptText = "(Type here...)";
            this.txtAccType.ReadOnly = true;
            this.txtAccType.ShowMandatoryMark = false;
            this.txtAccType.Size = new System.Drawing.Size(319, 21);
            this.txtAccType.TabIndex = 0;
            this.txtAccType.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtAccType.UpperCaseOnly = false;
            this.txtAccType.ValidationErrorMessage = "Validation Error!";
            this.txtAccType.Load += new System.EventHandler(this.customTextBox4_Load);
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.SystemColors.Window;
            this.txtName.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtName.InputScopeAllowEmpty = false;
            this.txtName.InputScopeCustomString = null;
            this.txtName.IsValid = null;
            this.txtName.Location = new System.Drawing.Point(87, 50);
            this.txtName.MaxLength = 32767;
            this.txtName.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtName.Name = "txtName";
            this.txtName.PromptText = "(Type here...)";
            this.txtName.ReadOnly = true;
            this.txtName.ShowMandatoryMark = false;
            this.txtName.Size = new System.Drawing.Size(319, 21);
            this.txtName.TabIndex = 0;
            this.txtName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtName.UpperCaseOnly = false;
            this.txtName.ValidationErrorMessage = "Validation Error!";
            this.txtName.Load += new System.EventHandler(this.customTextBox3_Load);
            // 
            // txtLastRenewalDate
            // 
            this.txtLastRenewalDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtLastRenewalDate.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtLastRenewalDate.InputScopeAllowEmpty = false;
            this.txtLastRenewalDate.InputScopeCustomString = null;
            this.txtLastRenewalDate.IsValid = null;
            this.txtLastRenewalDate.Location = new System.Drawing.Point(539, 50);
            this.txtLastRenewalDate.MaxLength = 32767;
            this.txtLastRenewalDate.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtLastRenewalDate.Name = "txtLastRenewalDate";
            this.txtLastRenewalDate.PromptText = "(Type here...)";
            this.txtLastRenewalDate.ReadOnly = true;
            this.txtLastRenewalDate.ShowMandatoryMark = false;
            this.txtLastRenewalDate.Size = new System.Drawing.Size(123, 21);
            this.txtLastRenewalDate.TabIndex = 0;
            this.txtLastRenewalDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtLastRenewalDate.UpperCaseOnly = false;
            this.txtLastRenewalDate.ValidationErrorMessage = "Validation Error!";
            this.txtLastRenewalDate.Load += new System.EventHandler(this.customTextBox2_Load);
            // 
            // txtAcMaturityDate
            // 
            this.txtAcMaturityDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtAcMaturityDate.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtAcMaturityDate.InputScopeAllowEmpty = false;
            this.txtAcMaturityDate.InputScopeCustomString = null;
            this.txtAcMaturityDate.IsValid = null;
            this.txtAcMaturityDate.Location = new System.Drawing.Point(539, 77);
            this.txtAcMaturityDate.MaxLength = 32767;
            this.txtAcMaturityDate.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtAcMaturityDate.Name = "txtAcMaturityDate";
            this.txtAcMaturityDate.PromptText = "(Type here...)";
            this.txtAcMaturityDate.ReadOnly = true;
            this.txtAcMaturityDate.ShowMandatoryMark = false;
            this.txtAcMaturityDate.Size = new System.Drawing.Size(123, 21);
            this.txtAcMaturityDate.TabIndex = 0;
            this.txtAcMaturityDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtAcMaturityDate.UpperCaseOnly = false;
            this.txtAcMaturityDate.ValidationErrorMessage = "Validation Error!";
            this.txtAcMaturityDate.Load += new System.EventHandler(this.customTextBox2_Load);
            // 
            // txtAccClosingDate
            // 
            this.txtAccClosingDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtAccClosingDate.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtAccClosingDate.InputScopeAllowEmpty = false;
            this.txtAccClosingDate.InputScopeCustomString = null;
            this.txtAccClosingDate.IsValid = null;
            this.txtAccClosingDate.Location = new System.Drawing.Point(773, 105);
            this.txtAccClosingDate.MaxLength = 32767;
            this.txtAccClosingDate.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtAccClosingDate.Name = "txtAccClosingDate";
            this.txtAccClosingDate.PromptText = "(Type here...)";
            this.txtAccClosingDate.ReadOnly = false;
            this.txtAccClosingDate.ShowMandatoryMark = false;
            this.txtAccClosingDate.Size = new System.Drawing.Size(123, 21);
            this.txtAccClosingDate.TabIndex = 0;
            this.txtAccClosingDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtAccClosingDate.UpperCaseOnly = false;
            this.txtAccClosingDate.ValidationErrorMessage = "Validation Error!";
            this.txtAccClosingDate.Load += new System.EventHandler(this.customTextBox2_Load);
            // 
            // txtPeriod
            // 
            this.txtPeriod.BackColor = System.Drawing.SystemColors.Window;
            this.txtPeriod.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtPeriod.InputScopeAllowEmpty = false;
            this.txtPeriod.InputScopeCustomString = null;
            this.txtPeriod.IsValid = null;
            this.txtPeriod.Location = new System.Drawing.Point(773, 77);
            this.txtPeriod.MaxLength = 32767;
            this.txtPeriod.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtPeriod.Name = "txtPeriod";
            this.txtPeriod.PromptText = "(Type here...)";
            this.txtPeriod.ReadOnly = true;
            this.txtPeriod.ShowMandatoryMark = false;
            this.txtPeriod.Size = new System.Drawing.Size(123, 21);
            this.txtPeriod.TabIndex = 0;
            this.txtPeriod.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtPeriod.UpperCaseOnly = false;
            this.txtPeriod.ValidationErrorMessage = "Validation Error!";
            this.txtPeriod.Load += new System.EventHandler(this.customTextBox2_Load);
            // 
            // txtLastExciseDuty
            // 
            this.txtLastExciseDuty.BackColor = System.Drawing.SystemColors.Window;
            this.txtLastExciseDuty.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtLastExciseDuty.InputScopeAllowEmpty = false;
            this.txtLastExciseDuty.InputScopeCustomString = null;
            this.txtLastExciseDuty.IsValid = null;
            this.txtLastExciseDuty.Location = new System.Drawing.Point(539, 105);
            this.txtLastExciseDuty.MaxLength = 32767;
            this.txtLastExciseDuty.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtLastExciseDuty.Name = "txtLastExciseDuty";
            this.txtLastExciseDuty.PromptText = "(Type here...)";
            this.txtLastExciseDuty.ReadOnly = true;
            this.txtLastExciseDuty.ShowMandatoryMark = false;
            this.txtLastExciseDuty.Size = new System.Drawing.Size(123, 21);
            this.txtLastExciseDuty.TabIndex = 0;
            this.txtLastExciseDuty.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtLastExciseDuty.UpperCaseOnly = false;
            this.txtLastExciseDuty.ValidationErrorMessage = "Validation Error!";
            this.txtLastExciseDuty.Load += new System.EventHandler(this.customTextBox2_Load);
            // 
            // txtCurrentAddressline2
            // 
            this.txtCurrentAddressline2.BackColor = System.Drawing.SystemColors.Window;
            this.txtCurrentAddressline2.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtCurrentAddressline2.InputScopeAllowEmpty = false;
            this.txtCurrentAddressline2.InputScopeCustomString = null;
            this.txtCurrentAddressline2.IsValid = null;
            this.txtCurrentAddressline2.Location = new System.Drawing.Point(773, 50);
            this.txtCurrentAddressline2.MaxLength = 32767;
            this.txtCurrentAddressline2.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtCurrentAddressline2.Name = "txtCurrentAddressline2";
            this.txtCurrentAddressline2.PromptText = "(Type here...)";
            this.txtCurrentAddressline2.ReadOnly = true;
            this.txtCurrentAddressline2.ShowMandatoryMark = false;
            this.txtCurrentAddressline2.Size = new System.Drawing.Size(242, 21);
            this.txtCurrentAddressline2.TabIndex = 0;
            this.txtCurrentAddressline2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtCurrentAddressline2.UpperCaseOnly = false;
            this.txtCurrentAddressline2.ValidationErrorMessage = "Validation Error!";
            this.txtCurrentAddressline2.Load += new System.EventHandler(this.customTextBox2_Load);
            // 
            // txtCurrentAddressLine1
            // 
            this.txtCurrentAddressLine1.BackColor = System.Drawing.SystemColors.Window;
            this.txtCurrentAddressLine1.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtCurrentAddressLine1.InputScopeAllowEmpty = false;
            this.txtCurrentAddressLine1.InputScopeCustomString = null;
            this.txtCurrentAddressLine1.IsValid = null;
            this.txtCurrentAddressLine1.Location = new System.Drawing.Point(773, 23);
            this.txtCurrentAddressLine1.MaxLength = 32767;
            this.txtCurrentAddressLine1.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtCurrentAddressLine1.Name = "txtCurrentAddressLine1";
            this.txtCurrentAddressLine1.PromptText = "(Type here...)";
            this.txtCurrentAddressLine1.ReadOnly = true;
            this.txtCurrentAddressLine1.ShowMandatoryMark = false;
            this.txtCurrentAddressLine1.Size = new System.Drawing.Size(242, 21);
            this.txtCurrentAddressLine1.TabIndex = 0;
            this.txtCurrentAddressLine1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtCurrentAddressLine1.UpperCaseOnly = false;
            this.txtCurrentAddressLine1.ValidationErrorMessage = "Validation Error!";
            this.txtCurrentAddressLine1.Load += new System.EventHandler(this.customTextBox2_Load);
            // 
            // txtAccOpendate
            // 
            this.txtAccOpendate.BackColor = System.Drawing.SystemColors.Window;
            this.txtAccOpendate.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtAccOpendate.InputScopeAllowEmpty = false;
            this.txtAccOpendate.InputScopeCustomString = null;
            this.txtAccOpendate.IsValid = null;
            this.txtAccOpendate.Location = new System.Drawing.Point(539, 23);
            this.txtAccOpendate.MaxLength = 32767;
            this.txtAccOpendate.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtAccOpendate.Name = "txtAccOpendate";
            this.txtAccOpendate.PromptText = "(Type here...)";
            this.txtAccOpendate.ReadOnly = true;
            this.txtAccOpendate.ShowMandatoryMark = false;
            this.txtAccOpendate.Size = new System.Drawing.Size(123, 21);
            this.txtAccOpendate.TabIndex = 0;
            this.txtAccOpendate.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtAccOpendate.UpperCaseOnly = false;
            this.txtAccOpendate.ValidationErrorMessage = "Validation Error!";
            this.txtAccOpendate.Load += new System.EventHandler(this.customTextBox2_Load);
            // 
            // txtAccNo
            // 
            this.txtAccNo.BackColor = System.Drawing.SystemColors.Window;
            this.txtAccNo.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtAccNo.InputScopeAllowEmpty = false;
            this.txtAccNo.InputScopeCustomString = null;
            this.txtAccNo.IsValid = null;
            this.txtAccNo.Location = new System.Drawing.Point(87, 23);
            this.txtAccNo.MaxLength = 32767;
            this.txtAccNo.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtAccNo.Name = "txtAccNo";
            this.txtAccNo.PromptText = "(Type here...)";
            this.txtAccNo.ReadOnly = true;
            this.txtAccNo.ShowMandatoryMark = false;
            this.txtAccNo.Size = new System.Drawing.Size(319, 21);
            this.txtAccNo.TabIndex = 0;
            this.txtAccNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtAccNo.UpperCaseOnly = false;
            this.txtAccNo.ValidationErrorMessage = "Validation Error!";
            this.txtAccNo.Load += new System.EventHandler(this.customTextBox2_Load);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(213, 107);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Lien Amount";
            this.label9.Click += new System.EventHandler(this.label5_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 107);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Quard Amount";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 79);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Account Type";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(47, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Name";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(435, 52);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(98, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Last Renewal Date";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(421, 107);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(112, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Last Excise Duty Date";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(441, 79);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(92, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "A/C Maturity Date";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(698, 79);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(73, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Period(Month)";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(689, 26);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(82, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Current Address";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(682, 107);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(89, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "A/C Closing Date";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(438, 26);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "A/C Opening Date";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Account No";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label36);
            this.groupBox1.Controls.Add(this.label35);
            this.groupBox1.Controls.Add(this.label37);
            this.groupBox1.Controls.Add(this.label34);
            this.groupBox1.Controls.Add(this.label33);
            this.groupBox1.Controls.Add(this.label40);
            this.groupBox1.Controls.Add(this.label39);
            this.groupBox1.Controls.Add(this.label38);
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Controls.Add(this.txtClosingVoucherNetPayable);
            this.groupBox1.Controls.Add(this.txtClosingVoucherVat);
            this.groupBox1.Controls.Add(this.txtClosingVoucherClosingChg);
            this.groupBox1.Controls.Add(this.txtClosingVoucherExciseDuty);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.txtClosingVoucherTaxAdjustedDr);
            this.groupBox1.Controls.Add(this.txtClosingVoucherTaxAdjustedCr);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.txtClosingVoucherProfitDr);
            this.groupBox1.Controls.Add(this.txtClosingVoucherProfitCr);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtClosingVoucherAccBalanceCr);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(12, 176);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(370, 222);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Closing Voucher";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(202, 138);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(20, 13);
            this.label36.TabIndex = 48;
            this.label36.Text = "Tk";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(202, 192);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(20, 13);
            this.label35.TabIndex = 48;
            this.label35.Text = "Tk";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(202, 166);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(20, 13);
            this.label37.TabIndex = 48;
            this.label37.Text = "Tk";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(202, 112);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(20, 13);
            this.label34.TabIndex = 48;
            this.label34.Text = "Tk";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(202, 86);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(20, 13);
            this.label33.TabIndex = 48;
            this.label33.Text = "Tk";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(346, 86);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(20, 13);
            this.label40.TabIndex = 48;
            this.label40.Text = "Tk";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(346, 58);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(20, 13);
            this.label39.TabIndex = 48;
            this.label39.Text = "Tk";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(346, 29);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(20, 13);
            this.label38.TabIndex = 48;
            this.label38.Text = "Tk";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(202, 58);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(20, 13);
            this.label32.TabIndex = 48;
            this.label32.Text = "Tk";
            // 
            // txtClosingVoucherNetPayable
            // 
            this.txtClosingVoucherNetPayable.BackColor = System.Drawing.SystemColors.Window;
            this.txtClosingVoucherNetPayable.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtClosingVoucherNetPayable.InputScopeAllowEmpty = false;
            this.txtClosingVoucherNetPayable.InputScopeCustomString = null;
            this.txtClosingVoucherNetPayable.IsValid = null;
            this.txtClosingVoucherNetPayable.Location = new System.Drawing.Point(82, 189);
            this.txtClosingVoucherNetPayable.MaxLength = 32767;
            this.txtClosingVoucherNetPayable.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtClosingVoucherNetPayable.Name = "txtClosingVoucherNetPayable";
            this.txtClosingVoucherNetPayable.PromptText = "(Type here...)";
            this.txtClosingVoucherNetPayable.ReadOnly = true;
            this.txtClosingVoucherNetPayable.ShowMandatoryMark = false;
            this.txtClosingVoucherNetPayable.Size = new System.Drawing.Size(114, 21);
            this.txtClosingVoucherNetPayable.TabIndex = 29;
            this.txtClosingVoucherNetPayable.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtClosingVoucherNetPayable.UpperCaseOnly = false;
            this.txtClosingVoucherNetPayable.ValidationErrorMessage = "Validation Error!";
            // 
            // txtClosingVoucherVat
            // 
            this.txtClosingVoucherVat.BackColor = System.Drawing.SystemColors.Window;
            this.txtClosingVoucherVat.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Numeric;
            this.txtClosingVoucherVat.InputScopeAllowEmpty = false;
            this.txtClosingVoucherVat.InputScopeCustomString = null;
            this.txtClosingVoucherVat.IsValid = null;
            this.txtClosingVoucherVat.Location = new System.Drawing.Point(82, 162);
            this.txtClosingVoucherVat.MaxLength = 32767;
            this.txtClosingVoucherVat.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtClosingVoucherVat.Name = "txtClosingVoucherVat";
            this.txtClosingVoucherVat.PromptText = "(Type here...)";
            this.txtClosingVoucherVat.ReadOnly = false;
            this.txtClosingVoucherVat.ShowMandatoryMark = false;
            this.txtClosingVoucherVat.Size = new System.Drawing.Size(114, 21);
            this.txtClosingVoucherVat.TabIndex = 29;
            this.txtClosingVoucherVat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtClosingVoucherVat.UpperCaseOnly = false;
            this.txtClosingVoucherVat.ValidationErrorMessage = "Validation Error!";
            // 
            // txtClosingVoucherClosingChg
            // 
            this.txtClosingVoucherClosingChg.BackColor = System.Drawing.SystemColors.Window;
            this.txtClosingVoucherClosingChg.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Numeric;
            this.txtClosingVoucherClosingChg.InputScopeAllowEmpty = false;
            this.txtClosingVoucherClosingChg.InputScopeCustomString = null;
            this.txtClosingVoucherClosingChg.IsValid = null;
            this.txtClosingVoucherClosingChg.Location = new System.Drawing.Point(82, 135);
            this.txtClosingVoucherClosingChg.MaxLength = 32767;
            this.txtClosingVoucherClosingChg.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtClosingVoucherClosingChg.Name = "txtClosingVoucherClosingChg";
            this.txtClosingVoucherClosingChg.PromptText = "(Type here...)";
            this.txtClosingVoucherClosingChg.ReadOnly = false;
            this.txtClosingVoucherClosingChg.ShowMandatoryMark = false;
            this.txtClosingVoucherClosingChg.Size = new System.Drawing.Size(114, 21);
            this.txtClosingVoucherClosingChg.TabIndex = 29;
            this.txtClosingVoucherClosingChg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtClosingVoucherClosingChg.UpperCaseOnly = false;
            this.txtClosingVoucherClosingChg.ValidationErrorMessage = "Validation Error!";
            // 
            // txtClosingVoucherExciseDuty
            // 
            this.txtClosingVoucherExciseDuty.BackColor = System.Drawing.SystemColors.Window;
            this.txtClosingVoucherExciseDuty.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Numeric;
            this.txtClosingVoucherExciseDuty.InputScopeAllowEmpty = false;
            this.txtClosingVoucherExciseDuty.InputScopeCustomString = null;
            this.txtClosingVoucherExciseDuty.IsValid = null;
            this.txtClosingVoucherExciseDuty.Location = new System.Drawing.Point(82, 108);
            this.txtClosingVoucherExciseDuty.MaxLength = 32767;
            this.txtClosingVoucherExciseDuty.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtClosingVoucherExciseDuty.Name = "txtClosingVoucherExciseDuty";
            this.txtClosingVoucherExciseDuty.PromptText = "(Type here...)";
            this.txtClosingVoucherExciseDuty.ReadOnly = false;
            this.txtClosingVoucherExciseDuty.ShowMandatoryMark = false;
            this.txtClosingVoucherExciseDuty.Size = new System.Drawing.Size(114, 21);
            this.txtClosingVoucherExciseDuty.TabIndex = 29;
            this.txtClosingVoucherExciseDuty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtClosingVoucherExciseDuty.UpperCaseOnly = false;
            this.txtClosingVoucherExciseDuty.ValidationErrorMessage = "Validation Error!";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(55, 166);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(23, 13);
            this.label21.TabIndex = 31;
            this.label21.Text = "Vat";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(13, 192);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(65, 13);
            this.label20.TabIndex = 31;
            this.label20.Text = "Net Payable";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(15, 138);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(63, 13);
            this.label18.TabIndex = 31;
            this.label18.Text = "Closing Chg";
            // 
            // txtClosingVoucherTaxAdjustedDr
            // 
            this.txtClosingVoucherTaxAdjustedDr.BackColor = System.Drawing.SystemColors.Window;
            this.txtClosingVoucherTaxAdjustedDr.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Numeric;
            this.txtClosingVoucherTaxAdjustedDr.InputScopeAllowEmpty = false;
            this.txtClosingVoucherTaxAdjustedDr.InputScopeCustomString = null;
            this.txtClosingVoucherTaxAdjustedDr.IsValid = null;
            this.txtClosingVoucherTaxAdjustedDr.Location = new System.Drawing.Point(82, 81);
            this.txtClosingVoucherTaxAdjustedDr.MaxLength = 32767;
            this.txtClosingVoucherTaxAdjustedDr.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtClosingVoucherTaxAdjustedDr.Name = "txtClosingVoucherTaxAdjustedDr";
            this.txtClosingVoucherTaxAdjustedDr.PromptText = "(Type here...)";
            this.txtClosingVoucherTaxAdjustedDr.ReadOnly = false;
            this.txtClosingVoucherTaxAdjustedDr.ShowMandatoryMark = false;
            this.txtClosingVoucherTaxAdjustedDr.Size = new System.Drawing.Size(114, 21);
            this.txtClosingVoucherTaxAdjustedDr.TabIndex = 22;
            this.txtClosingVoucherTaxAdjustedDr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtClosingVoucherTaxAdjustedDr.UpperCaseOnly = false;
            this.txtClosingVoucherTaxAdjustedDr.ValidationErrorMessage = "Validation Error!";
            // 
            // txtClosingVoucherTaxAdjustedCr
            // 
            this.txtClosingVoucherTaxAdjustedCr.BackColor = System.Drawing.SystemColors.Window;
            this.txtClosingVoucherTaxAdjustedCr.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Numeric;
            this.txtClosingVoucherTaxAdjustedCr.InputScopeAllowEmpty = false;
            this.txtClosingVoucherTaxAdjustedCr.InputScopeCustomString = null;
            this.txtClosingVoucherTaxAdjustedCr.IsValid = null;
            this.txtClosingVoucherTaxAdjustedCr.Location = new System.Drawing.Point(229, 81);
            this.txtClosingVoucherTaxAdjustedCr.MaxLength = 32767;
            this.txtClosingVoucherTaxAdjustedCr.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtClosingVoucherTaxAdjustedCr.Name = "txtClosingVoucherTaxAdjustedCr";
            this.txtClosingVoucherTaxAdjustedCr.PromptText = "(Type here...)";
            this.txtClosingVoucherTaxAdjustedCr.ReadOnly = false;
            this.txtClosingVoucherTaxAdjustedCr.ShowMandatoryMark = false;
            this.txtClosingVoucherTaxAdjustedCr.Size = new System.Drawing.Size(114, 21);
            this.txtClosingVoucherTaxAdjustedCr.TabIndex = 23;
            this.txtClosingVoucherTaxAdjustedCr.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtClosingVoucherTaxAdjustedCr.UpperCaseOnly = false;
            this.txtClosingVoucherTaxAdjustedCr.ValidationErrorMessage = "Validation Error!";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(9, 86);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(69, 13);
            this.label19.TabIndex = 24;
            this.label19.Text = "Tax Adjusted";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(15, 112);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(63, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "Excise Duty";
            // 
            // txtClosingVoucherProfitDr
            // 
            this.txtClosingVoucherProfitDr.BackColor = System.Drawing.SystemColors.Window;
            this.txtClosingVoucherProfitDr.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Numeric;
            this.txtClosingVoucherProfitDr.InputScopeAllowEmpty = false;
            this.txtClosingVoucherProfitDr.InputScopeCustomString = null;
            this.txtClosingVoucherProfitDr.IsValid = null;
            this.txtClosingVoucherProfitDr.Location = new System.Drawing.Point(82, 54);
            this.txtClosingVoucherProfitDr.MaxLength = 32767;
            this.txtClosingVoucherProfitDr.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtClosingVoucherProfitDr.Name = "txtClosingVoucherProfitDr";
            this.txtClosingVoucherProfitDr.PromptText = "(Type here...)";
            this.txtClosingVoucherProfitDr.ReadOnly = false;
            this.txtClosingVoucherProfitDr.ShowMandatoryMark = false;
            this.txtClosingVoucherProfitDr.Size = new System.Drawing.Size(114, 21);
            this.txtClosingVoucherProfitDr.TabIndex = 11;
            this.txtClosingVoucherProfitDr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtClosingVoucherProfitDr.UpperCaseOnly = false;
            this.txtClosingVoucherProfitDr.ValidationErrorMessage = "Validation Error!";
            // 
            // txtClosingVoucherProfitCr
            // 
            this.txtClosingVoucherProfitCr.BackColor = System.Drawing.SystemColors.Window;
            this.txtClosingVoucherProfitCr.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Numeric;
            this.txtClosingVoucherProfitCr.InputScopeAllowEmpty = false;
            this.txtClosingVoucherProfitCr.InputScopeCustomString = null;
            this.txtClosingVoucherProfitCr.IsValid = null;
            this.txtClosingVoucherProfitCr.Location = new System.Drawing.Point(229, 54);
            this.txtClosingVoucherProfitCr.MaxLength = 32767;
            this.txtClosingVoucherProfitCr.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtClosingVoucherProfitCr.Name = "txtClosingVoucherProfitCr";
            this.txtClosingVoucherProfitCr.PromptText = "(Type here...)";
            this.txtClosingVoucherProfitCr.ReadOnly = false;
            this.txtClosingVoucherProfitCr.ShowMandatoryMark = false;
            this.txtClosingVoucherProfitCr.Size = new System.Drawing.Size(114, 21);
            this.txtClosingVoucherProfitCr.TabIndex = 12;
            this.txtClosingVoucherProfitCr.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtClosingVoucherProfitCr.UpperCaseOnly = false;
            this.txtClosingVoucherProfitCr.ValidationErrorMessage = "Validation Error!";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(47, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Profit";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(126, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Dr.";
            // 
            // txtClosingVoucherAccBalanceCr
            // 
            this.txtClosingVoucherAccBalanceCr.BackColor = System.Drawing.SystemColors.Window;
            this.txtClosingVoucherAccBalanceCr.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtClosingVoucherAccBalanceCr.InputScopeAllowEmpty = false;
            this.txtClosingVoucherAccBalanceCr.InputScopeCustomString = null;
            this.txtClosingVoucherAccBalanceCr.IsValid = null;
            this.txtClosingVoucherAccBalanceCr.Location = new System.Drawing.Point(229, 27);
            this.txtClosingVoucherAccBalanceCr.MaxLength = 32767;
            this.txtClosingVoucherAccBalanceCr.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtClosingVoucherAccBalanceCr.Name = "txtClosingVoucherAccBalanceCr";
            this.txtClosingVoucherAccBalanceCr.PromptText = "(Type here...)";
            this.txtClosingVoucherAccBalanceCr.ReadOnly = true;
            this.txtClosingVoucherAccBalanceCr.ShowMandatoryMark = false;
            this.txtClosingVoucherAccBalanceCr.Size = new System.Drawing.Size(114, 21);
            this.txtClosingVoucherAccBalanceCr.TabIndex = 0;
            this.txtClosingVoucherAccBalanceCr.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtClosingVoucherAccBalanceCr.UpperCaseOnly = false;
            this.txtClosingVoucherAccBalanceCr.ValidationErrorMessage = "Validation Error!";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(279, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Cr.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "A/C Balance";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ChkBoxEnableProfit);
            this.groupBox4.Controls.Add(this.txtProfitToClientProvisionalProfit);
            this.groupBox4.Controls.Add(this.txtProfitToClientActualProfit);
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.label53);
            this.groupBox4.Controls.Add(this.label54);
            this.groupBox4.Location = new System.Drawing.Point(764, 176);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(270, 121);
            this.groupBox4.TabIndex = 55;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Profit To Client";
            // 
            // ChkBoxEnableProfit
            // 
            this.ChkBoxEnableProfit.AutoSize = true;
            this.ChkBoxEnableProfit.Location = new System.Drawing.Point(105, 88);
            this.ChkBoxEnableProfit.Name = "ChkBoxEnableProfit";
            this.ChkBoxEnableProfit.Size = new System.Drawing.Size(111, 17);
            this.ChkBoxEnableProfit.TabIndex = 21;
            this.ChkBoxEnableProfit.Text = "Enable Prov Profit";
            this.ChkBoxEnableProfit.UseVisualStyleBackColor = true;
            this.ChkBoxEnableProfit.Visible = false;
            // 
            // txtProfitToClientProvisionalProfit
            // 
            this.txtProfitToClientProvisionalProfit.BackColor = System.Drawing.SystemColors.Window;
            this.txtProfitToClientProvisionalProfit.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Numeric;
            this.txtProfitToClientProvisionalProfit.InputScopeAllowEmpty = false;
            this.txtProfitToClientProvisionalProfit.InputScopeCustomString = null;
            this.txtProfitToClientProvisionalProfit.IsValid = null;
            this.txtProfitToClientProvisionalProfit.Location = new System.Drawing.Point(105, 58);
            this.txtProfitToClientProvisionalProfit.MaxLength = 32767;
            this.txtProfitToClientProvisionalProfit.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtProfitToClientProvisionalProfit.Name = "txtProfitToClientProvisionalProfit";
            this.txtProfitToClientProvisionalProfit.PromptText = "(Type here...)";
            this.txtProfitToClientProvisionalProfit.ReadOnly = false;
            this.txtProfitToClientProvisionalProfit.ShowMandatoryMark = false;
            this.txtProfitToClientProvisionalProfit.Size = new System.Drawing.Size(114, 21);
            this.txtProfitToClientProvisionalProfit.TabIndex = 16;
            this.txtProfitToClientProvisionalProfit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtProfitToClientProvisionalProfit.UpperCaseOnly = false;
            this.txtProfitToClientProvisionalProfit.ValidationErrorMessage = "Validation Error!";
            // 
            // txtProfitToClientActualProfit
            // 
            this.txtProfitToClientActualProfit.BackColor = System.Drawing.SystemColors.Window;
            this.txtProfitToClientActualProfit.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtProfitToClientActualProfit.InputScopeAllowEmpty = false;
            this.txtProfitToClientActualProfit.InputScopeCustomString = null;
            this.txtProfitToClientActualProfit.IsValid = null;
            this.txtProfitToClientActualProfit.Location = new System.Drawing.Point(105, 24);
            this.txtProfitToClientActualProfit.MaxLength = 32767;
            this.txtProfitToClientActualProfit.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtProfitToClientActualProfit.Name = "txtProfitToClientActualProfit";
            this.txtProfitToClientActualProfit.PromptText = "(Type here...)";
            this.txtProfitToClientActualProfit.ReadOnly = true;
            this.txtProfitToClientActualProfit.ShowMandatoryMark = false;
            this.txtProfitToClientActualProfit.Size = new System.Drawing.Size(114, 21);
            this.txtProfitToClientActualProfit.TabIndex = 16;
            this.txtProfitToClientActualProfit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtProfitToClientActualProfit.UpperCaseOnly = false;
            this.txtProfitToClientActualProfit.ValidationErrorMessage = "Validation Error!";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(36, 28);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(64, 13);
            this.label31.TabIndex = 13;
            this.label31.Text = "Actual Profit";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(15, 62);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(85, 13);
            this.label23.TabIndex = 13;
            this.label23.Text = "Provisional Profit";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(225, 28);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(20, 13);
            this.label53.TabIndex = 48;
            this.label53.Text = "Tk";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(225, 62);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(20, 13);
            this.label54.TabIndex = 48;
            this.label54.Text = "Tk";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.rbAgentAccount);
            this.groupBox5.Controls.Add(this.rbLinkAccount);
            this.groupBox5.Location = new System.Drawing.Point(764, 304);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(270, 94);
            this.groupBox5.TabIndex = 56;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Transaction";
            // 
            // rbAgentAccount
            // 
            this.rbAgentAccount.AutoSize = true;
            this.rbAgentAccount.Location = new System.Drawing.Point(131, 35);
            this.rbAgentAccount.Name = "rbAgentAccount";
            this.rbAgentAccount.Size = new System.Drawing.Size(96, 17);
            this.rbAgentAccount.TabIndex = 0;
            this.rbAgentAccount.TabStop = true;
            this.rbAgentAccount.Text = "Agent Account";
            this.rbAgentAccount.UseVisualStyleBackColor = true;
            // 
            // rbLinkAccount
            // 
            this.rbLinkAccount.AutoSize = true;
            this.rbLinkAccount.Location = new System.Drawing.Point(18, 35);
            this.rbLinkAccount.Name = "rbLinkAccount";
            this.rbLinkAccount.Size = new System.Drawing.Size(88, 17);
            this.rbLinkAccount.TabIndex = 0;
            this.rbLinkAccount.TabStop = true;
            this.rbLinkAccount.Text = "Link Account";
            this.rbLinkAccount.UseVisualStyleBackColor = true;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSubmit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnSubmit.FlatAppearance.BorderSize = 0;
            this.btnSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubmit.ForeColor = System.Drawing.Color.White;
            this.btnSubmit.Location = new System.Drawing.Point(927, 404);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(110, 28);
            this.btnSubmit.TabIndex = 58;
            this.btnSubmit.Text = "Close";
            this.btnSubmit.UseVisualStyleBackColor = false;
            // 
            // btnCloseAcc
            // 
            this.btnCloseAcc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseAcc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnCloseAcc.FlatAppearance.BorderSize = 0;
            this.btnCloseAcc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseAcc.ForeColor = System.Drawing.Color.White;
            this.btnCloseAcc.Location = new System.Drawing.Point(811, 404);
            this.btnCloseAcc.Name = "btnCloseAcc";
            this.btnCloseAcc.Size = new System.Drawing.Size(110, 28);
            this.btnCloseAcc.TabIndex = 58;
            this.btnCloseAcc.Text = "Close Account";
            this.btnCloseAcc.UseVisualStyleBackColor = false;
            this.btnCloseAcc.Click += new System.EventHandler(this.btnCloseAcc_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(29, 35);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(47, 13);
            this.label30.TabIndex = 0;
            this.label30.Text = "Principal";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(284, 15);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(23, 13);
            this.label29.TabIndex = 0;
            this.label29.Text = "Cr.";
            // 
            // txtTransactionInfoPrincipalCr
            // 
            this.txtTransactionInfoPrincipalCr.BackColor = System.Drawing.SystemColors.Window;
            this.txtTransactionInfoPrincipalCr.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtTransactionInfoPrincipalCr.InputScopeAllowEmpty = false;
            this.txtTransactionInfoPrincipalCr.InputScopeCustomString = null;
            this.txtTransactionInfoPrincipalCr.IsValid = null;
            this.txtTransactionInfoPrincipalCr.Location = new System.Drawing.Point(229, 32);
            this.txtTransactionInfoPrincipalCr.MaxLength = 32767;
            this.txtTransactionInfoPrincipalCr.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtTransactionInfoPrincipalCr.Name = "txtTransactionInfoPrincipalCr";
            this.txtTransactionInfoPrincipalCr.PromptText = "(Type here...)";
            this.txtTransactionInfoPrincipalCr.ReadOnly = true;
            this.txtTransactionInfoPrincipalCr.ShowMandatoryMark = false;
            this.txtTransactionInfoPrincipalCr.Size = new System.Drawing.Size(114, 21);
            this.txtTransactionInfoPrincipalCr.TabIndex = 0;
            this.txtTransactionInfoPrincipalCr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTransactionInfoPrincipalCr.UpperCaseOnly = false;
            this.txtTransactionInfoPrincipalCr.ValidationErrorMessage = "Validation Error!";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(133, 15);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(24, 13);
            this.label28.TabIndex = 0;
            this.label28.Text = "Dr.";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(45, 63);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(31, 13);
            this.label27.TabIndex = 13;
            this.label27.Text = "Profit";
            // 
            // txtTransactionInfoProfitCr
            // 
            this.txtTransactionInfoProfitCr.BackColor = System.Drawing.SystemColors.Window;
            this.txtTransactionInfoProfitCr.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtTransactionInfoProfitCr.InputScopeAllowEmpty = false;
            this.txtTransactionInfoProfitCr.InputScopeCustomString = null;
            this.txtTransactionInfoProfitCr.IsValid = null;
            this.txtTransactionInfoProfitCr.Location = new System.Drawing.Point(229, 59);
            this.txtTransactionInfoProfitCr.MaxLength = 32767;
            this.txtTransactionInfoProfitCr.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtTransactionInfoProfitCr.Name = "txtTransactionInfoProfitCr";
            this.txtTransactionInfoProfitCr.PromptText = "(Type here...)";
            this.txtTransactionInfoProfitCr.ReadOnly = true;
            this.txtTransactionInfoProfitCr.ShowMandatoryMark = false;
            this.txtTransactionInfoProfitCr.Size = new System.Drawing.Size(114, 21);
            this.txtTransactionInfoProfitCr.TabIndex = 12;
            this.txtTransactionInfoProfitCr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTransactionInfoProfitCr.UpperCaseOnly = false;
            this.txtTransactionInfoProfitCr.ValidationErrorMessage = "Validation Error!";
            // 
            // txtTransactionInfoProfitDr
            // 
            this.txtTransactionInfoProfitDr.BackColor = System.Drawing.SystemColors.Window;
            this.txtTransactionInfoProfitDr.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtTransactionInfoProfitDr.InputScopeAllowEmpty = false;
            this.txtTransactionInfoProfitDr.InputScopeCustomString = null;
            this.txtTransactionInfoProfitDr.IsValid = null;
            this.txtTransactionInfoProfitDr.Location = new System.Drawing.Point(82, 59);
            this.txtTransactionInfoProfitDr.MaxLength = 32767;
            this.txtTransactionInfoProfitDr.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtTransactionInfoProfitDr.Name = "txtTransactionInfoProfitDr";
            this.txtTransactionInfoProfitDr.PromptText = "(Type here...)";
            this.txtTransactionInfoProfitDr.ReadOnly = true;
            this.txtTransactionInfoProfitDr.ShowMandatoryMark = false;
            this.txtTransactionInfoProfitDr.Size = new System.Drawing.Size(114, 21);
            this.txtTransactionInfoProfitDr.TabIndex = 11;
            this.txtTransactionInfoProfitDr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTransactionInfoProfitDr.UpperCaseOnly = false;
            this.txtTransactionInfoProfitDr.ValidationErrorMessage = "Validation Error!";
            // 
            // txtTransactionInfoPrincipalDr
            // 
            this.txtTransactionInfoPrincipalDr.BackColor = System.Drawing.SystemColors.Window;
            this.txtTransactionInfoPrincipalDr.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtTransactionInfoPrincipalDr.InputScopeAllowEmpty = false;
            this.txtTransactionInfoPrincipalDr.InputScopeCustomString = null;
            this.txtTransactionInfoPrincipalDr.IsValid = null;
            this.txtTransactionInfoPrincipalDr.Location = new System.Drawing.Point(82, 32);
            this.txtTransactionInfoPrincipalDr.MaxLength = 32767;
            this.txtTransactionInfoPrincipalDr.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtTransactionInfoPrincipalDr.Name = "txtTransactionInfoPrincipalDr";
            this.txtTransactionInfoPrincipalDr.PromptText = "(Type here...)";
            this.txtTransactionInfoPrincipalDr.ReadOnly = true;
            this.txtTransactionInfoPrincipalDr.ShowMandatoryMark = false;
            this.txtTransactionInfoPrincipalDr.Size = new System.Drawing.Size(114, 21);
            this.txtTransactionInfoPrincipalDr.TabIndex = 11;
            this.txtTransactionInfoPrincipalDr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTransactionInfoPrincipalDr.UpperCaseOnly = false;
            this.txtTransactionInfoPrincipalDr.ValidationErrorMessage = "Validation Error!";
            this.txtTransactionInfoPrincipalDr.Load += new System.EventHandler(this.customTextBox32_Load);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(13, 116);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(63, 13);
            this.label26.TabIndex = 24;
            this.label26.Text = "Excise Duty";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(48, 89);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(28, 13);
            this.label25.TabIndex = 24;
            this.label25.Text = "Tax ";
            // 
            // txtTransactionInfoTaxCr
            // 
            this.txtTransactionInfoTaxCr.BackColor = System.Drawing.SystemColors.Window;
            this.txtTransactionInfoTaxCr.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtTransactionInfoTaxCr.InputScopeAllowEmpty = false;
            this.txtTransactionInfoTaxCr.InputScopeCustomString = null;
            this.txtTransactionInfoTaxCr.IsValid = null;
            this.txtTransactionInfoTaxCr.Location = new System.Drawing.Point(229, 86);
            this.txtTransactionInfoTaxCr.MaxLength = 32767;
            this.txtTransactionInfoTaxCr.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtTransactionInfoTaxCr.Name = "txtTransactionInfoTaxCr";
            this.txtTransactionInfoTaxCr.PromptText = "(Type here...)";
            this.txtTransactionInfoTaxCr.ReadOnly = true;
            this.txtTransactionInfoTaxCr.ShowMandatoryMark = false;
            this.txtTransactionInfoTaxCr.Size = new System.Drawing.Size(114, 21);
            this.txtTransactionInfoTaxCr.TabIndex = 23;
            this.txtTransactionInfoTaxCr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTransactionInfoTaxCr.UpperCaseOnly = false;
            this.txtTransactionInfoTaxCr.ValidationErrorMessage = "Validation Error!";
            // 
            // txtTransactionInfoExciseDutyCr
            // 
            this.txtTransactionInfoExciseDutyCr.BackColor = System.Drawing.SystemColors.Window;
            this.txtTransactionInfoExciseDutyCr.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtTransactionInfoExciseDutyCr.InputScopeAllowEmpty = false;
            this.txtTransactionInfoExciseDutyCr.InputScopeCustomString = null;
            this.txtTransactionInfoExciseDutyCr.IsValid = null;
            this.txtTransactionInfoExciseDutyCr.Location = new System.Drawing.Point(229, 113);
            this.txtTransactionInfoExciseDutyCr.MaxLength = 32767;
            this.txtTransactionInfoExciseDutyCr.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtTransactionInfoExciseDutyCr.Name = "txtTransactionInfoExciseDutyCr";
            this.txtTransactionInfoExciseDutyCr.PromptText = "(Type here...)";
            this.txtTransactionInfoExciseDutyCr.ReadOnly = true;
            this.txtTransactionInfoExciseDutyCr.ShowMandatoryMark = false;
            this.txtTransactionInfoExciseDutyCr.Size = new System.Drawing.Size(114, 21);
            this.txtTransactionInfoExciseDutyCr.TabIndex = 23;
            this.txtTransactionInfoExciseDutyCr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTransactionInfoExciseDutyCr.UpperCaseOnly = false;
            this.txtTransactionInfoExciseDutyCr.ValidationErrorMessage = "Validation Error!";
            // 
            // txtTransactionInfoOtherCr
            // 
            this.txtTransactionInfoOtherCr.BackColor = System.Drawing.SystemColors.Window;
            this.txtTransactionInfoOtherCr.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtTransactionInfoOtherCr.InputScopeAllowEmpty = false;
            this.txtTransactionInfoOtherCr.InputScopeCustomString = null;
            this.txtTransactionInfoOtherCr.IsValid = null;
            this.txtTransactionInfoOtherCr.Location = new System.Drawing.Point(229, 140);
            this.txtTransactionInfoOtherCr.MaxLength = 32767;
            this.txtTransactionInfoOtherCr.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtTransactionInfoOtherCr.Name = "txtTransactionInfoOtherCr";
            this.txtTransactionInfoOtherCr.PromptText = "(Type here...)";
            this.txtTransactionInfoOtherCr.ReadOnly = true;
            this.txtTransactionInfoOtherCr.ShowMandatoryMark = false;
            this.txtTransactionInfoOtherCr.Size = new System.Drawing.Size(114, 21);
            this.txtTransactionInfoOtherCr.TabIndex = 23;
            this.txtTransactionInfoOtherCr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTransactionInfoOtherCr.UpperCaseOnly = false;
            this.txtTransactionInfoOtherCr.ValidationErrorMessage = "Validation Error!";
            // 
            // txtTransactionInfoTaxDr
            // 
            this.txtTransactionInfoTaxDr.BackColor = System.Drawing.SystemColors.Window;
            this.txtTransactionInfoTaxDr.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtTransactionInfoTaxDr.InputScopeAllowEmpty = false;
            this.txtTransactionInfoTaxDr.InputScopeCustomString = null;
            this.txtTransactionInfoTaxDr.IsValid = null;
            this.txtTransactionInfoTaxDr.Location = new System.Drawing.Point(82, 86);
            this.txtTransactionInfoTaxDr.MaxLength = 32767;
            this.txtTransactionInfoTaxDr.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtTransactionInfoTaxDr.Name = "txtTransactionInfoTaxDr";
            this.txtTransactionInfoTaxDr.PromptText = "(Type here...)";
            this.txtTransactionInfoTaxDr.ReadOnly = true;
            this.txtTransactionInfoTaxDr.ShowMandatoryMark = false;
            this.txtTransactionInfoTaxDr.Size = new System.Drawing.Size(114, 21);
            this.txtTransactionInfoTaxDr.TabIndex = 22;
            this.txtTransactionInfoTaxDr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTransactionInfoTaxDr.UpperCaseOnly = false;
            this.txtTransactionInfoTaxDr.ValidationErrorMessage = "Validation Error!";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(11, 144);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(65, 13);
            this.label24.TabIndex = 31;
            this.label24.Text = "Others/Misc";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(198, 35);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(20, 13);
            this.label41.TabIndex = 48;
            this.label41.Text = "Tk";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(3, 170);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(73, 13);
            this.label22.TabIndex = 31;
            this.label22.Text = "Total Balance";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(345, 35);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(20, 13);
            this.label47.TabIndex = 48;
            this.label47.Text = "Tk";
            // 
            // txtTransactionInfoExciseDutyDr
            // 
            this.txtTransactionInfoExciseDutyDr.BackColor = System.Drawing.SystemColors.Window;
            this.txtTransactionInfoExciseDutyDr.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtTransactionInfoExciseDutyDr.InputScopeAllowEmpty = false;
            this.txtTransactionInfoExciseDutyDr.InputScopeCustomString = null;
            this.txtTransactionInfoExciseDutyDr.IsValid = null;
            this.txtTransactionInfoExciseDutyDr.Location = new System.Drawing.Point(82, 113);
            this.txtTransactionInfoExciseDutyDr.MaxLength = 32767;
            this.txtTransactionInfoExciseDutyDr.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtTransactionInfoExciseDutyDr.Name = "txtTransactionInfoExciseDutyDr";
            this.txtTransactionInfoExciseDutyDr.PromptText = "(Type here...)";
            this.txtTransactionInfoExciseDutyDr.ReadOnly = true;
            this.txtTransactionInfoExciseDutyDr.ShowMandatoryMark = false;
            this.txtTransactionInfoExciseDutyDr.Size = new System.Drawing.Size(114, 21);
            this.txtTransactionInfoExciseDutyDr.TabIndex = 29;
            this.txtTransactionInfoExciseDutyDr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTransactionInfoExciseDutyDr.UpperCaseOnly = false;
            this.txtTransactionInfoExciseDutyDr.ValidationErrorMessage = "Validation Error!";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(198, 63);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(20, 13);
            this.label42.TabIndex = 48;
            this.label42.Text = "Tk";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(198, 89);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(20, 13);
            this.label43.TabIndex = 48;
            this.label43.Text = "Tk";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(345, 63);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(20, 13);
            this.label48.TabIndex = 48;
            this.label48.Text = "Tk";
            // 
            // txtTransactionInfoOtherDr
            // 
            this.txtTransactionInfoOtherDr.BackColor = System.Drawing.SystemColors.Window;
            this.txtTransactionInfoOtherDr.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtTransactionInfoOtherDr.InputScopeAllowEmpty = false;
            this.txtTransactionInfoOtherDr.InputScopeCustomString = null;
            this.txtTransactionInfoOtherDr.IsValid = null;
            this.txtTransactionInfoOtherDr.Location = new System.Drawing.Point(82, 140);
            this.txtTransactionInfoOtherDr.MaxLength = 32767;
            this.txtTransactionInfoOtherDr.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtTransactionInfoOtherDr.Name = "txtTransactionInfoOtherDr";
            this.txtTransactionInfoOtherDr.PromptText = "(Type here...)";
            this.txtTransactionInfoOtherDr.ReadOnly = true;
            this.txtTransactionInfoOtherDr.ShowMandatoryMark = false;
            this.txtTransactionInfoOtherDr.Size = new System.Drawing.Size(114, 21);
            this.txtTransactionInfoOtherDr.TabIndex = 29;
            this.txtTransactionInfoOtherDr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTransactionInfoOtherDr.UpperCaseOnly = false;
            this.txtTransactionInfoOtherDr.ValidationErrorMessage = "Validation Error!";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(345, 89);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(20, 13);
            this.label49.TabIndex = 48;
            this.label49.Text = "Tk";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(198, 144);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(20, 13);
            this.label44.TabIndex = 48;
            this.label44.Text = "Tk";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(345, 144);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(20, 13);
            this.label50.TabIndex = 48;
            this.label50.Text = "Tk";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(345, 170);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(20, 13);
            this.label51.TabIndex = 48;
            this.label51.Text = "Tk";
            // 
            // txtTransactionInfoTotalBalance
            // 
            this.txtTransactionInfoTotalBalance.BackColor = System.Drawing.SystemColors.Window;
            this.txtTransactionInfoTotalBalance.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtTransactionInfoTotalBalance.InputScopeAllowEmpty = false;
            this.txtTransactionInfoTotalBalance.InputScopeCustomString = null;
            this.txtTransactionInfoTotalBalance.IsValid = null;
            this.txtTransactionInfoTotalBalance.Location = new System.Drawing.Point(82, 167);
            this.txtTransactionInfoTotalBalance.MaxLength = 32767;
            this.txtTransactionInfoTotalBalance.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtTransactionInfoTotalBalance.Name = "txtTransactionInfoTotalBalance";
            this.txtTransactionInfoTotalBalance.PromptText = "(Type here...)";
            this.txtTransactionInfoTotalBalance.ReadOnly = true;
            this.txtTransactionInfoTotalBalance.ShowMandatoryMark = false;
            this.txtTransactionInfoTotalBalance.Size = new System.Drawing.Size(261, 21);
            this.txtTransactionInfoTotalBalance.TabIndex = 29;
            this.txtTransactionInfoTotalBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTransactionInfoTotalBalance.UpperCaseOnly = false;
            this.txtTransactionInfoTotalBalance.ValidationErrorMessage = "Validation Error!";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(198, 116);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(20, 13);
            this.label46.TabIndex = 48;
            this.label46.Text = "Tk";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(345, 116);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(20, 13);
            this.label52.TabIndex = 48;
            this.label52.Text = "Tk";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label52);
            this.groupBox3.Controls.Add(this.label46);
            this.groupBox3.Controls.Add(this.txtTransactionInfoTotalBalance);
            this.groupBox3.Controls.Add(this.label51);
            this.groupBox3.Controls.Add(this.label50);
            this.groupBox3.Controls.Add(this.label44);
            this.groupBox3.Controls.Add(this.label49);
            this.groupBox3.Controls.Add(this.txtTransactionInfoOtherDr);
            this.groupBox3.Controls.Add(this.label48);
            this.groupBox3.Controls.Add(this.label43);
            this.groupBox3.Controls.Add(this.label42);
            this.groupBox3.Controls.Add(this.txtTransactionInfoExciseDutyDr);
            this.groupBox3.Controls.Add(this.label47);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.label41);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.txtTransactionInfoTaxDr);
            this.groupBox3.Controls.Add(this.txtTransactionInfoOtherCr);
            this.groupBox3.Controls.Add(this.txtTransactionInfoExciseDutyCr);
            this.groupBox3.Controls.Add(this.txtTransactionInfoTaxCr);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.txtTransactionInfoPrincipalDr);
            this.groupBox3.Controls.Add(this.txtTransactionInfoProfitDr);
            this.groupBox3.Controls.Add(this.txtTransactionInfoProfitCr);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.txtTransactionInfoPrincipalCr);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Location = new System.Drawing.Point(388, 176);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(370, 222);
            this.groupBox3.TabIndex = 54;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Transaction Info";
            // 
            // frmTermAccountClosing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1043, 439);
            this.Controls.Add(this.btnCloseAcc);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.customTitlebar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmTermAccountClosing";
            this.Text = "Term Account Closing";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Agent.CustomControls.CustomTitlebar customTitlebar1;
        private System.Windows.Forms.GroupBox groupBox2;
        private Agent.CustomControls.CustomTextBox txtQuardAmount;
        private Agent.CustomControls.CustomTextBox txtAccType;
        private Agent.CustomControls.CustomTextBox txtName;
        private Agent.CustomControls.CustomTextBox txtAccNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private Agent.CustomControls.CustomTextBox txtLienAmount;
        private Agent.CustomControls.CustomTextBox txtLastRenewalDate;
        private Agent.CustomControls.CustomTextBox txtAcMaturityDate;
        private Agent.CustomControls.CustomTextBox txtAccClosingDate;
        private Agent.CustomControls.CustomTextBox txtPeriod;
        private Agent.CustomControls.CustomTextBox txtLastExciseDuty;
        private Agent.CustomControls.CustomTextBox txtCurrentAddressline2;
        private Agent.CustomControls.CustomTextBox txtCurrentAddressLine1;
        private Agent.CustomControls.CustomTextBox txtAccOpendate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label10;
        private Agent.CustomControls.CustomTextBox txtClosingVoucherExciseDuty;
        private System.Windows.Forms.Label label18;
        private Agent.CustomControls.CustomTextBox txtClosingVoucherTaxAdjustedDr;
        private Agent.CustomControls.CustomTextBox txtClosingVoucherTaxAdjustedCr;
        private System.Windows.Forms.Label label17;
        private Agent.CustomControls.CustomTextBox txtClosingVoucherProfitDr;
        private Agent.CustomControls.CustomTextBox txtClosingVoucherProfitCr;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Agent.CustomControls.CustomTextBox txtClosingVoucherAccBalanceCr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private Agent.CustomControls.CustomTextBox txtClosingVoucherVat;
        private Agent.CustomControls.CustomTextBox txtClosingVoucherClosingChg;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private Agent.CustomControls.CustomTextBox txtClosingVoucherNetPayable;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.CheckBox ChkBoxEnableProfit;
        private Agent.CustomControls.CustomTextBox txtProfitToClientProvisionalProfit;
        private Agent.CustomControls.CustomTextBox txtProfitToClientActualProfit;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton rbAgentAccount;
        private System.Windows.Forms.RadioButton rbLinkAccount;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnCloseAcc;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private Agent.CustomControls.CustomTextBox txtTransactionInfoTotalBalance;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private Agent.CustomControls.CustomTextBox txtTransactionInfoPrincipalCr;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private Agent.CustomControls.CustomTextBox txtTransactionInfoProfitCr;
        private Agent.CustomControls.CustomTextBox txtTransactionInfoProfitDr;
        private Agent.CustomControls.CustomTextBox txtTransactionInfoPrincipalDr;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private Agent.CustomControls.CustomTextBox txtTransactionInfoTaxCr;
        private Agent.CustomControls.CustomTextBox txtTransactionInfoExciseDutyCr;
        private Agent.CustomControls.CustomTextBox txtTransactionInfoOtherCr;
        private Agent.CustomControls.CustomTextBox txtTransactionInfoTaxDr;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label47;
        private Agent.CustomControls.CustomTextBox txtTransactionInfoExciseDutyDr;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label48;
        private Agent.CustomControls.CustomTextBox txtTransactionInfoOtherDr;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.GroupBox groupBox3;
    }
}