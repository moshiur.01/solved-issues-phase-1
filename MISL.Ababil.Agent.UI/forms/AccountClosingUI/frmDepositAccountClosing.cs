﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure.Models.common;

namespace MISL.Ababil.Agent.UI.forms.AccountClosingUI
{
    public partial class frmDepositAccountClosing : CustomForm
    {
        private Packet _receivedPacket;

        public frmDepositAccountClosing(Packet packet)
        {
            _receivedPacket = packet;
            InitializeComponent();

            PreparedUI();

        }

        private void PreparedUI()
        {
            SetupDataLoad();
            SetupComponent();
            FillComponentWithObjectValue();
        }

        private void SetupDataLoad()
        {

        }

        private void SetupComponent()
        {
            switch (_receivedPacket.actionType)
            {
                case FormActionType.Edit:

                    break;
                case FormActionType.View:
                    SetReadOnlyMode();
                    break;
            }
        }

        private void FillComponentWithObjectValue()
        {

        }

        private void SetReadOnlyMode()
        {
            new GUI().SetControlState
                (GUI.CONTROLSTATES.CS_READONLY,
                    new Control[]
                    {
                        txtAccountNumber,
                        dtpOpeningDate,
                        txtCustomerName,
                        txtLienAmount,
                        txtBlockAmount,

                        txtBalance,
                        txtProvisionedProfit,
                        txtUncalculatedProfit,
                        txtPaymentInformationTotal,

                        txtTaxOnProfit,
                        txtClosingCharge,
                        txtVAT,
                        txtExciseDuty,
                        txtDeductionInformationTotal,
                        txtNetPayment
                    }
                );
        }

        private void btnCloseAccount_Click(object sender, EventArgs e)
        {
            FillObjectWithComponentValue();

        }

        private void FillObjectWithComponentValue()
        {

        }
    }
}