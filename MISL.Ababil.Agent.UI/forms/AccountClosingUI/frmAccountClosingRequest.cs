﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.GenericFingerprintServices;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.consumer;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.UI.forms.CommentUI;
using MISL.Ababil.Agent.UI.forms.ProgressUI;

namespace MISL.Ababil.Agent.UI.forms.AccountClosingUI
{
    public partial class frmAccountClosingRequest : CustomForm, FingerprintEventObserver
    {
        private Packet _packet;
        private ConsumerInformationDto _consumerInformationDto;
        private TermAccInforamationDto _termAccInforamationDto;
        private AccountClosingRequestDto _accountClosingRequest = null;
        private AccountClosingSearchResultDto _accountClosingSearchResultDto;
        private int _captureIndexNo;
        private int _noOfCapturefinger;
        private string _capturefingerData;
        private string _captureFor;
        private string _subagentFingerData;
        private List<CommentDto> _comments;
        private CommentDto _commentDraft;
        private long? _documentId;

        public frmAccountClosingRequest(Packet packet)
        {
            _packet = packet;
            InitializeComponent();
            //_accountClosingRequest = (AccountClosingRequestDto)packet.otherObj;
            _accountClosingSearchResultDto = (AccountClosingSearchResultDto)packet.otherObj;
            ClearPreloadingControls();
            SwitchMode();
            if (_packet.actionType == FormActionType.Verify || _packet.actionType == FormActionType.View)
            {
                FillPackectObject();
            }
        }

        private void FillPackectObject()
        {
            switch (_accountClosingSearchResultDto.accountType)
            {
                case AccountType.Deposit:
                    rbtnDeposit.Checked = true;
                    break;
                case AccountType.SSP:
                    rBtnITD.Checked = true;
                    break;
                case AccountType.MTDR:
                    rBtnMTDR.Checked = true;
                    break;
            }
            txtAccountNumber.Text = _accountClosingSearchResultDto.accountNo;

            switch (_accountClosingSearchResultDto.accountType)
            {
                case AccountType.Deposit:
                    rbtnDeposit.Checked = true;
                    break;
                case AccountType.SSP:
                    rBtnITD.Checked = true;
                    break;
                case AccountType.MTDR:
                    rBtnMTDR.Checked = true;
                    break;
            }
            GetAccInfoByAccNumber(_accountClosingSearchResultDto.accountNo);
            _documentId = _accountClosingSearchResultDto.docId;
        }

        private void SwitchMode()
        {
            switch (_packet.actionType)
            {
                case FormActionType.New:
                    {
                        btnReject.Visible = btnAccept.Visible = false;
                        btnSubmit.Visible = true;
                    }
                    break;
                case FormActionType.Edit:
                    {
                        btnReject.Visible = btnAccept.Visible = false;
                        btnSubmit.Visible = true;
                    }
                    break;
                case FormActionType.View:
                    {
                        btnReject.Visible = btnAccept.Visible = false;
                        btnSubmit.Visible = false;
                        dgv.ReadOnly = true;
                        txtAccountNumber.ReadOnly = true;
                        rbtnDeposit.Enabled = rBtnITD.Enabled = rBtnMTDR.Enabled = false;
                    }
                    break;
                case FormActionType.Verify:
                    {
                        rbtnDeposit.Enabled = rBtnITD.Enabled = rBtnMTDR.Enabled = false;
                        btnSubmit.Visible = false;
                        gbxFingerprintInformation.Visible = false;
                        dgv.ReadOnly = true;
                        dgv.Columns.RemoveAt(0);
                        txtAccountNumber.ReadOnly = true;
                        this.Height = 392;
                    }
                    break;
            }
        }

        private void btnAccountInformationDetails_Click(object sender, EventArgs e)
        {
            if (rbtnDeposit.Checked)
            {
                new frmDepositAccountClosing(new Packet() { actionType = _packet.actionType });
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (MsgBox.showConfirmation("Are you sure to submit the Account Closing request?") != MsgBoxResults.Yes)
            {
                return;
            }
            if (!IsValid())
            {
                return;
            }
            if (!FillObjectWithComponentValue())
            {
                MsgBox.showWarning("Velidation failed! Please check all input values and try again.");
                return;
            }
            //if (rbtnDeposit.Checked)
            {
                try
                {
                    _captureFor = "subagent";
                    //bio.CaptureFingerData();
                    FingerprintServiceFactory.FingerprintServiceFactory factory = new FingerprintServiceFactory.FingerprintServiceFactory();
                    FingerprintDevice device = factory.getFingerprintDevice();
                    device.registerEventObserver(this);
                    device.capture();
                }
                catch (Exception ex)
                {
                    ProgressUIManager.CloseProgress();
                    MsgBox.ShowError(ex.Message);
                }
            }
        }

        //_______________________quick_fix_for_double_error_msg_______________________//
        private int _leave = 0;
        private AccountOperatorDto _accountOperatorDto;

        private void txtAccountNumber_Leave(object sender, EventArgs e)
        {
            if (_packet.actionType == FormActionType.Verify || _packet.actionType == FormActionType.View || txtAccountNumber.Text == string.Empty)
            {
                return;
            }
            GetAccInfoByAccNumber(txtAccountNumber.Text);
        }

        private void GetAccInfoByAccNumber(string accountNumber)
        {
            try
            {
                _leave++;
                LoadConsumerInformation(accountNumber);
            }
            catch (Exception ex)
            {
                ProgressUIManager.CloseProgress();
                if (_leave == 2)
                {
                    _leave = 0;
                    return;
                }
                lblNameValue.Text = "";
                pbxPicture.Image = null;
                dgv.DataSource = null;
                lblRequiredFingerPrint.Text = null;
                Message.showError(ex.Message);
            }
        }

        private void LoadConsumerInformation(string accountNumber)
        {
            if (CustomControlValidation.IsAllValid(false, true, accountNumber))
            {
                if (rbtnDeposit.Checked)
                {
                    LoadDepositAccountInfo(accountNumber);
                }
                if (rBtnITD.Checked || rBtnMTDR.Checked)
                {
                    LoadTermAccountInfo(accountNumber);
                }
            }
        }

        private void LoadTermAccountInfo(string accountNumber)
        {
            _termAccInforamationDto = (new TermAccountInqueryService()).GetTermAccountInformationByAccountNo(accountNumber);
            lblNameValue.Text = _termAccInforamationDto.customerName;
            lblBalanceValue.Text = (_termAccInforamationDto.currentbalance ?? 0).ToString("N", new CultureInfo("BN-BD"));

            dgv.DataSource = null;
            dgv.Rows.Clear();
            dgv.Columns.Clear();

            if (_packet.actionType != FormActionType.Verify && _packet.actionType != FormActionType.View)
            {
                dgv.Columns.Add(new DataGridViewButtonColumn
                {
                    Text = "Capture",
                    UseColumnTextForButtonValue = true
                });
            }


            if (rBtnITD.Checked)
            {
                _accountOperatorDto = new AccountInformationService().GetAccountOperatorDtoByAccountNumber(accountNumber, AccountType.SSP);
            }
            if (rBtnMTDR.Checked)
            {
                _accountOperatorDto = new AccountInformationService().GetAccountOperatorDtoByAccountNumber(accountNumber, AccountType.MTDR);
            }

            if (_accountOperatorDto?.operators != null && _accountOperatorDto.operators.Count > 0)
            {
                dgv.DataSource = _accountOperatorDto.operators.Select(o => new OperatorfingerPrintGrid(o)
                {
                    identity = o.individualId.ToString(),
                    identityName = o.individualName
                }).ToList();
            }
            else
            {
                rBtnAuthOTP.Checked = true;
                dgv.Columns.Add(new DataGridViewButtonColumn());
            }
        }

        private void LoadDepositAccountInfo(string accountNumber)
        {
            _consumerInformationDto = new ConsumerServices().getConsumerInformationDtoByAcc(accountNumber);
            lblNameValue.Text = _consumerInformationDto.consumerTitle;
            lblBalanceValue.Text = (_consumerInformationDto.balance ?? 0).ToString("N", new CultureInfo("BN-BD"));
            lblRequiredFingerPrint.Text = "At least " + _consumerInformationDto.numberOfOperator + " operator's finger print required.";
            if (_consumerInformationDto.photo != null)
            {
                byte[] bytes = Convert.FromBase64String(_consumerInformationDto.photo);
                Image image = UtilityServices.byteArrayToImage(bytes);
                pbxPicture.Image = image;
            }

            dgv.DataSource = null;
            dgv.Rows.Clear();
            dgv.Columns.Clear();

            if (_packet.actionType != FormActionType.Verify && _packet.actionType != FormActionType.View)
            {
                dgv.Columns.Add(new DataGridViewButtonColumn
                {
                    Text = "Capture",
                    UseColumnTextForButtonValue = true
                });
            }

            _accountOperatorDto = new AccountInformationService().GetAccountOperatorDtoByAccountNumber(accountNumber);
            if (_accountOperatorDto?.operators != null && _accountOperatorDto.operators.Count > 0)
            {
                dgv.DataSource = _accountOperatorDto.operators.Select(o => new OperatorfingerPrintGrid(o)
                {
                    identity = o.individualId.ToString(),
                    identityName = o.individualName
                }).ToList();
            }
            else
            {
                rBtnAuthOTP.Checked = true;
                dgv.Columns.Add(new DataGridViewButtonColumn());
            }

            #region temp

            //if ( _accountOperatorDto. != null)
            //{
            //    fingerPrintGrid.DataSource =  _accountOperatorDto..Select(o => new OperatorfingerPrintGrid(o) { identity = o.identity, identityName = o.identityName }).ToList();
            //}

            //_consumerInformationDto = new ConsumerServices().getConsumerInformationDtoByAcc(txtAccountNumber.Text);
            //lblNameValue.Text = _consumerInformationDto.consumerTitle;
            //lblBalanceValue.Text = (_consumerInformationDto.balance ?? 0).ToString("N", new CultureInfo("BN-BD"));
            //lblRequiredFingerPrint.Text = "At least " + _consumerInformationDto.numberOfOperator + " operator's finger print required.";
            //if (_consumerInformationDto.photo != null)
            //{
            //    byte[] bytes = Convert.FromBase64String(_consumerInformationDto.photo);
            //    Image image = UtilityServices.byteArrayToImage(bytes);
            //    pbxPicture.Image = image;
            //}

            //dgv.DataSource = null;
            //dgv.Rows.Clear();
            //dgv.Columns.Clear();
            //if (_consumerInformationDto.accountOperators != null && _consumerInformationDto?.accountOperators.Count > 0)
            //{
            //    rBtnAuthBiometric.Checked = true;
            //    dgv.DataSource =
            //        _consumerInformationDto.accountOperators.Select(o => new operatorfingerPrintGrid(o)
            //        {
            //            identity = o.identity,
            //            identityName = o.identityName
            //        }).ToList();
            //    dgv.Columns.Add(new DataGridViewButtonColumn());
            //}
            //else
            //{
            //    rBtnAuthOTP.Checked = true;
            //    dgv.Columns.Add(new DataGridViewButtonColumn());
            //}

            #endregion
        }

        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            {
                try
                {
                    if (!string.IsNullOrEmpty(txtAccountNumber.Text))
                    {
                        _captureIndexNo = e.RowIndex;
                        _captureFor = "consumer";

                        FingerprintServiceFactory.FingerprintServiceFactory factory = new FingerprintServiceFactory.FingerprintServiceFactory();
                        FingerprintDevice device = factory.getFingerprintDevice();
                        device.registerEventObserver(this);
                        device.capture();
                    }
                    else
                    {
                        Message.showInformation("Account number or amount may be blank.");
                    }
                }
                catch (Exception ex)
                {
                    Message.showWarning("No operator found for capture.");
                }
            }
        }

        public void FingerPrintEventOccured(FingerprintEvents eventSpec, object EventData)
        {
            if (EventData == null)
            {
                return;
            }
            if (_captureFor == "subagent")
            {
                _subagentFingerData = EventData.ToString();
                if (_subagentFingerData == null)
                {
                    MessageBox.Show("Please capture agent fingerprint.");
                    _captureFor = "consumer";
                    FingerprintServiceFactory.FingerprintServiceFactory factory = new FingerprintServiceFactory.FingerprintServiceFactory();
                    FingerprintDevice device = factory.getFingerprintDevice();
                    device.registerEventObserver(this);
                    device.capture();
                }
                else
                {
                    if (_noOfCapturefinger >= _accountOperatorDto.noOfRequireOperator)
                    {
                        if (txtAccountNumber.Text != "")
                        {
                            ProgressUIManager.ShowProgress(this);

                            AccountType accountTypeTemp = AccountType.Deposit;
                            if (rBtnITD.Checked) { accountTypeTemp = AccountType.SSP; }
                            if (rBtnMTDR.Checked) { accountTypeTemp = AccountType.MTDR; }

                            string response = new AccountClosingServices().SubmitAccountClosingRequest(new AccountClosingRequestDto
                            {
                                accountOperatorDto = _accountOperatorDto,
                                accountClosingRequestInfo = new AccountClosingRequestInfo
                                {
                                    accountNumber = txtAccountNumber.Text,
                                    accountType = accountTypeTemp,
                                    authenticationWay = AuthenticationWay.Biometric,
                                    docId = _documentId
                                },
                                userFingerData = _subagentFingerData,
                            });
                            ProgressUIManager.CloseProgress();
                            MsgBox.showInformation("Request is successfully submitted.\n\nReference Number: " + response);
                            this.Close();
                        }
                    }
                    else
                    {
                        ProgressUIManager.CloseProgress();
                        MessageBox.Show("Number of required fingerprint is " + _accountOperatorDto.noOfRequireOperator + ". You have captured only " + _noOfCapturefinger.ToString());
                    }
                }
            }
            else if (_captureFor == "consumer")
            {
                _capturefingerData = EventData.ToString();
                string previousFingerData = _accountOperatorDto.operators[_captureIndexNo].fingerData;
                _accountOperatorDto.operators[_captureIndexNo].fingerData = _capturefingerData;
                dgv.Rows[_captureIndexNo].Cells[2].Style.BackColor = Color.Green;
                dgv.Rows[_captureIndexNo].Cells[1].Style.BackColor = Color.Green;
                if (previousFingerData == null)
                {
                    _noOfCapturefinger++;
                    if (_noOfCapturefinger == 1)
                    {
                        disableAllComponent();
                    }
                    lblRequiredFingerPrint.Text = "Caputured " + _noOfCapturefinger + " operator's finger prints out of " + _consumerInformationDto?.numberOfOperator;
                }
            }
        }

        private bool ValidateFingerprint(AccountOperatorDto accountOperatorDto, int? noOfOperator)
        {
            int noOfBioData = 0;
            int noOfBioDataChecked = 0;
            for (int i = 0; i < accountOperatorDto.operators.Count; i++)
            {
                if (accountOperatorDto.operators[i].mandatory == true
//&&
//accountOperatorDto.operators[i].== false

)
                {
                    MsgBox.showWarning("Account Operator required!");
                    return false;
                }
                //if (accountOperatorDto.operators[i].accountOperator == true
                //    &&
                //    accountOperatorDto.operators[i].individualInformation.fingerInfos != null
                //    &&
                //    accountOperatorDto.operators[i].individualInformation.fingerInfos?.Count > 0)
                //{
                //    noOfBioDataChecked++;
                //}
                //else
                //{

                //}

                //if (accountOperatorDto.ownerInformations[i].individualInformation.fingerInfos != null
                //    &&
                //    accountOperatorDto.ownerInformations[i].individualInformation.fingerInfos?.Count > 0)
                //{
                //    noOfBioData++;
                //}
                //else
                //{

                //}
            }
            //if (noOfBioDataChecked >= noOfOperator && noOfBioData == accountOperatorDto.ownerInformations.Count)
            {
                return true;
            }
            //else
            {
                MsgBox.showWarning("Account Operator Fingerprint required!");
            }
            return false;
        }

        private void disableAllComponent()
        {
        }

        private void ClearPreloadingControls()
        {
            txtAccountNumber.Text = lblNameValue.Text = lblBalanceValue.Text = string.Empty;
        }

        public bool FillObjectWithComponentValue()
        {
            if (!IsValid())
            {
                return false;
            }
            else
            {
                if (_accountClosingRequest == null)
                {
                    _accountClosingRequest = new AccountClosingRequestDto();
                }
                if (_accountClosingRequest.accountClosingRequestInfo == null)
                {
                    _accountClosingRequest.accountClosingRequestInfo = new AccountClosingRequestInfo();
                }
                _accountClosingRequest.accountClosingRequestInfo.applicantName = _consumerInformationDto == null ? "" : _consumerInformationDto.consumerTitle;
                _accountClosingRequest.accountClosingRequestInfo.accountNumber = txtAccountNumber.Text;
                _accountClosingRequest.accountClosingRequestInfo.docId = _documentId;
                if (rBtnAuthBiometric.Checked && !rBtnAuthOTP.Checked)
                {
                    _accountClosingRequest.accountClosingRequestInfo.authenticationWay = AuthenticationWay.Biometric;
                    _accountClosingRequest.accountOperatorDto = _accountOperatorDto;
                }
                if (rBtnAuthOTP.Checked && !rBtnAuthBiometric.Checked)
                {
                    _accountClosingRequest.accountClosingRequestInfo.authenticationWay = AuthenticationWay.Token;
                }
            }
            return true;
        }

        public bool IsValid()
        {
            if (string.IsNullOrEmpty(txtAccountNumber.Text))
            {
                MsgBox.showWarning("Account Number not found!");
                return false;
            }
            if (rbtnDeposit.Checked == rBtnITD.Checked == rBtnMTDR.Checked == false)
            {
                MsgBox.showWarning("Account Type not selected!");
                return false;
            }
            if (rBtnAuthBiometric.Checked == rBtnAuthOTP.Checked == false)
            {
                //return false;
            }

            return true;
            //if (rBtnAuthBiometric.Checked && (dgv.Rows.Count <= 0 || _consumerInformationDto.accountOperators.Count == 0))
            //{
            //    return false;
            //}
            //else
            //{
            //    bool flag = false;
            //    for (int i = 0; i < _consumerInformationDto.accountOperators.Count; i++)
            //    {
            //        if (!string.IsNullOrEmpty(_consumerInformationDto.accountOperators[i].fingerData))
            //        {
            //            flag = true;
            //        }
            //    }
            //    return flag;
            //}
        }

        private void btnReject_Click(object sender, EventArgs e)
        {
            if (MsgBox.showConfirmation("Are you sure to reject this Account Closing request?") != MsgBoxResults.Yes)
            {
                return;
            }
            try
            {
                try
                {
                    ProgressUIManager.ShowProgress(this);
                    if (_accountClosingRequest == null)
                    {
                        _accountClosingRequest = new AccountClosingRequestDto();
                        _accountClosingRequest.accountClosingRequestInfo = new AccountClosingRequestInfo();
                    }
                    _accountClosingRequest.comment = _commentDraft;
                    _accountClosingRequest.accountClosingRequestInfo.refNo = _accountClosingSearchResultDto.refNo;
                    new AccountClosingServices().RejectAccountClosingRequest(_accountClosingRequest);
                    ProgressUIManager.CloseProgress();
                    Message.showInformation("Request is rejected successfully!");
                    this.Close();
                }
                catch (Exception ex)
                {
                    ProgressUIManager.CloseProgress();
                    Message.showError(ex.Message);
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        private void btnCorrection_Click(object sender, EventArgs e)
        {
            if (MsgBox.showConfirmation("Are you sure to cancel this Account Closing request?") != MsgBoxResults.Yes)
            {
                return;
            }
            try
            {
                try
                {
                    ProgressUIManager.ShowProgress(this);
                    new AccountClosingServices().CorrectAccountClosingRequest(_accountClosingRequest);
                    ProgressUIManager.CloseProgress();
                    Message.showInformation("Account Closing request is cancelled.");
                }
                catch (Exception ex)
                {
                    ProgressUIManager.CloseProgress();
                    Message.showError(ex.Message);
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        private void btnComment_Click(object sender, EventArgs e)
        {
            if (_packet == null)
            {
                return;
            }
            Packet packetTmp = new Packet
            {
                actionType = _packet.actionType
            };

            string stageTmp = "";
            if (SessionInfo.userBasicInformation.userCategory == UserCategory.SubAgentUser)
            {
                stageTmp = "Submission";
            }
            else
            {
                stageTmp = "Approval";
            }


            if (_accountClosingSearchResultDto != null && _accountClosingSearchResultDto.commentId != 0 && _accountClosingSearchResultDto.commentId != null)
            {
                _comments = (List<CommentDto>)TermService.GetCommentsID((_accountClosingSearchResultDto.commentId ?? 0).ToString()).ReturnedObject;
            }

            try
            {

                frmCommentUI frm = new frmCommentUI(packetTmp, _commentDraft, _comments, stageTmp);
                frm.Top = this.Top + btnComment.Top - frm.Height + 40;
                frm.Left = btnComment.Left + this.Left + 10;
                frm.ShowDialog(this);
                _commentDraft = frm._commentDraft;
                CommentText();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }

        }

        private void CommentText()
        {
            int count = 0;
            if (_comments != null)
            {
                count = _comments.Count;
                if (_commentDraft != null)
                {
                    count++;
                    btnComment.BackColor = Color.LightGreen;
                }
            }
            btnComment.Text = "Comment (" + count.ToString() + ")";
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmAccountClosingRequest_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (_documentId != null)
            {
                frmDocument frm;
                if (_packet.actionType == FormActionType.Edit || _packet.actionType == FormActionType.New)
                {
                    frm = new frmDocument((int?)_documentId ?? 0, ActionType.update);
                }
                else
                {
                    frm = new frmDocument((int?)_documentId ?? 0, ActionType.view);
                }
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    _documentId = int.Parse(frm.retrnMsg);
                }
            }
            else
            {
                frmDocument frm = new frmDocument();
                if (frm.ShowDialog() == DialogResult.OK && frm.retrnMsg != "")
                {
                    _documentId = int.Parse(frm.retrnMsg);
                }
            }
        }

        public class OperatorfingerPrintGrid
        {
            public string identity { get; set; }
            public string identityName { get; set; }

            private AccountOperatorDetailsDto _obj;

            public OperatorfingerPrintGrid(AccountOperatorDetailsDto obj)
            {
                _obj = obj;
            }

            public AccountOperatorDetailsDto GetModel()
            {
                return _obj;
            }
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            if (MsgBox.showConfirmation("Are you sure to accept this Account Closing request?") != MsgBoxResults.Yes)
            {
                return;
            }
            try
            {
                try
                {
                    ProgressUIManager.ShowProgress(this);
                    if (_accountClosingRequest == null)
                    {
                        _accountClosingRequest = new AccountClosingRequestDto
                        {
                            accountClosingRequestInfo = new AccountClosingRequestInfo()
                        };
                    }
                    _accountClosingRequest.comment = _commentDraft;
                    _accountClosingRequest.accountClosingRequestInfo.refNo = _accountClosingSearchResultDto.refNo;
                    new AccountClosingServices().AcceptAccountClosingRequest(_accountClosingRequest);
                    ProgressUIManager.CloseProgress();
                    Message.showInformation("Request is accepted successfully!");
                    this.Close();
                }
                catch (Exception ex)
                {
                    ProgressUIManager.CloseProgress();
                    Message.showError(ex.Message);
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtAccountNumber.Text = string.Empty;
            ClearPreloadingControls();
            pbxPicture.Image = null;
            dgv.DataSource = null;
            dgv.Rows.Clear();
            dgv.Columns.Clear();
            _noOfCapturefinger = 0;
        }
    }
}