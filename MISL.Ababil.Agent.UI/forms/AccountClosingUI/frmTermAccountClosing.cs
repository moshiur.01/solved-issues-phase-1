﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.UI.forms.ProgressUI;

namespace MISL.Ababil.Agent.UI.forms.AccountClosingUI
{
    public partial class frmTermAccountClosing : CustomForm
    {
        TermAccountClosingDto _termAccountClosingDto = new TermAccountClosingDto();
        

        private Packet _packet;
        public frmTermAccountClosing(Packet packet)
        {
            _packet = packet;
            InitializeComponent();
            //_termAccountClosingDto = (TermAccountClosingDto)packet.otherObj;

        }

        private void FillComponentWithObjectValue()
        {
            txtAccNo.Text = _termAccountClosingDto.accountNo;
            txtName.Text = _termAccountClosingDto.name;
            txtAccType.Text = _termAccountClosingDto.accounttype.ToString();
            txtQuardAmount.Text = _termAccountClosingDto.quardAmmount.ToString();
            txtLienAmount.Text = _termAccountClosingDto.lienAmmount.ToString();
            txtAccOpendate.Text = _termAccountClosingDto.accOpenDate.ToString();
            txtLastRenewalDate.Text = _termAccountClosingDto.lastRenewaldate.ToString();
            txtAcMaturityDate.Text = _termAccountClosingDto.accMaturityDate.ToString();
            txtLastExciseDuty.Text = _termAccountClosingDto.lastExciseDutyDate.ToString();
            txtCurrentAddressLine1.Text = _termAccountClosingDto.curAddressLine1;
            txtCurrentAddressline2.Text = _termAccountClosingDto.curAddressLine2;
            txtPeriod.Text = _termAccountClosingDto.period.ToString();
            txtAccClosingDate.Text = _termAccountClosingDto.accClosingDate.ToString();



            txtClosingVoucherAccBalanceCr.Text = _termAccountClosingDto.closingvoucherAccBalanceCr.ToString();
            txtClosingVoucherProfitDr.Text = _termAccountClosingDto.closingvoucherProfitDr.ToString();
            txtClosingVoucherProfitCr.Text = _termAccountClosingDto.closingvoucherProfitCr.ToString();
            txtClosingVoucherTaxAdjustedDr.Text = _termAccountClosingDto.closingvoucherTaxAdjustedDr.ToString();
            txtClosingVoucherTaxAdjustedCr.Text = _termAccountClosingDto.closingvoucherTaxAdjustedCr.ToString();
            txtClosingVoucherExciseDuty.Text = _termAccountClosingDto.closingvoucherExciseDutyDr.ToString();
            txtClosingVoucherClosingChg.Text = _termAccountClosingDto.closingvoucherClosingChgDr.ToString();
            txtClosingVoucherVat.Text = _termAccountClosingDto.closingvoucherVatDr.ToString();
            txtClosingVoucherNetPayable.Text = _termAccountClosingDto.closingvoucherNetPayableDr.ToString();


            txtTransactionInfoPrincipalDr.Text = _termAccountClosingDto.transactionInfoPrincipalDr.ToString();
            txtTransactionInfoPrincipalCr.Text = _termAccountClosingDto.transactionInfoPrincipalCr.ToString();

            txtTransactionInfoProfitDr.Text = _termAccountClosingDto.TransactionInfoProfitDr.ToString();
            txtTransactionInfoProfitCr.Text = _termAccountClosingDto.TransactionInfoProfitCr.ToString();

            txtTransactionInfoTaxDr.Text = _termAccountClosingDto.TransactionInfoTaxDr.ToString();
            txtTransactionInfoTaxCr.Text = _termAccountClosingDto.TransactionInfoTaxCr.ToString();

            txtTransactionInfoExciseDutyDr.Text = _termAccountClosingDto.TransactionInfoExciseDutyDr.ToString();
            txtTransactionInfoExciseDutyCr.Text = _termAccountClosingDto.TransactionInfoExciseDutyCr.ToString();

            txtTransactionInfoOtherDr.Text = _termAccountClosingDto.TransactionInfoOtherDr.ToString();
            txtTransactionInfoOtherCr.Text = _termAccountClosingDto.TransactionInfoOtherCr.ToString();

            txtTransactionInfoTotalBalance.Text = _termAccountClosingDto.TransactionInfoTotalBalance.ToString();

            txtProfitToClientActualProfit.Text = _termAccountClosingDto.profitToClientActualProfit.ToString();
            txtProfitToClientProvisionalProfit.Text = _termAccountClosingDto.profitToClientActualProvisionalProfit.ToString();

        }


        private void FillObjectWithComponentt()
        {



            _termAccountClosingDto.closingvoucherProfitDr = long.Parse(txtClosingVoucherProfitDr.Text);
            _termAccountClosingDto.closingvoucherProfitCr = long.Parse(txtClosingVoucherProfitCr.Text);
            _termAccountClosingDto.closingvoucherTaxAdjustedDr = long.Parse(txtClosingVoucherTaxAdjustedDr.Text);
            _termAccountClosingDto.closingvoucherTaxAdjustedCr = long.Parse(txtClosingVoucherTaxAdjustedCr.Text);
            _termAccountClosingDto.closingvoucherExciseDutyDr = long.Parse(txtClosingVoucherExciseDuty.Text);
            _termAccountClosingDto.closingvoucherClosingChgDr = long.Parse(txtClosingVoucherClosingChg.Text);
            _termAccountClosingDto.closingvoucherVatDr = long.Parse(txtClosingVoucherVat.Text);

            if (rbLinkAccount.Checked == true)
            {
                _termAccountClosingDto.SetteledAccountType = SetteledAccountType.LinkAccount;
            }
            if (rbAgentAccount.Checked == true)
            {
                _termAccountClosingDto.SetteledAccountType = SetteledAccountType.AgentAccount;
            }





        }

        private bool CheckValidation()
        {
            if (string.IsNullOrEmpty(txtClosingVoucherProfitDr.Text))
            {
                MsgBox.showWarning("Profit Dr Should not be blank");
                return false;
            }
            if (string.IsNullOrEmpty(txtClosingVoucherProfitCr.Text))
            {
                MsgBox.showWarning("Profit Cr Should not be blank");
                return false;
            }
            if (string.IsNullOrEmpty(txtClosingVoucherTaxAdjustedDr.Text))
            {
                MsgBox.showWarning("Tax Adjusted Dr Should not be blank");
                return false;
            }
            if (string.IsNullOrEmpty(txtClosingVoucherTaxAdjustedCr.Text))
            {
                MsgBox.showWarning("Tax Adjusted Cr Should not be blank");
                return false;
            }
            if (string.IsNullOrEmpty(txtClosingVoucherExciseDuty.Text))
            {
                MsgBox.showWarning("Excise Duty Should not be blank");
                return false;
            }
            if (string.IsNullOrEmpty(txtClosingVoucherClosingChg.Text))
            {
                MsgBox.showWarning("Closing Chg Should not be blank");
                return false;
            }
            if (string.IsNullOrEmpty(txtClosingVoucherVat.Text))
            {
                MsgBox.showWarning("Vat Should not be blank");
                return false;
            }
            return true;

        }
        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void customTextBox4_Load(object sender, EventArgs e)
        {

        }

        private void customTextBox3_Load(object sender, EventArgs e)
        {

        }

        private void customTextBox2_Load(object sender, EventArgs e)
        {

        }

        private void customTextBox32_Load(object sender, EventArgs e)
        {

        }

        private void label35_Click(object sender, EventArgs e)
        {
        }

        private void btnCloseAcc_Click(object sender, EventArgs e)
        {
            if (CheckValidation())
            {
                FillObjectWithComponentt();
                try
                {
                    ProgressUIManager.ShowProgress(this);
                    new AccountClosingServices().CloseTermAccount(_termAccountClosingDto);
                    ProgressUIManager.CloseProgress();
                }
                catch (Exception ex)
                {
                    ProgressUIManager.CloseProgress();
                    MsgBox.ShowError(ex.Message);
                }
                
               
            }

        }
    }
}
