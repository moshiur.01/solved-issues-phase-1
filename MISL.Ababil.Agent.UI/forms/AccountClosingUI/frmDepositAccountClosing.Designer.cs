﻿namespace MISL.Ababil.Agent.UI.forms.AccountClosingUI
{
    partial class frmDepositAccountClosing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtAccountNumber = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.dtpOpeningDate = new MISL.Ababil.Agent.UI.forms.CustomControls.CustomDateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCustomerName = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLienAmount = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBlockAmount = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.btnClose = new System.Windows.Forms.Button();
            this.secondaryTitleLabel = new System.Windows.Forms.Label();
            this.customHeaderControl1 = new MISL.Ababil.Agent.CustomControls.CustomHeaderControl();
            this.customHeaderControl2 = new MISL.Ababil.Agent.CustomControls.CustomHeaderControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtPaymentInformationTotal = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtUncalculatedProfit = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtBalance = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtProvisionedProfit = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtExciseDuty = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtVAT = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtDeductionInformationTotal = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtTaxOnProfit = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtClosingCharge = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtNetPayment = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.btnCloseAccount = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Account Number :";
            // 
            // txtAccountNumber
            // 
            this.txtAccountNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtAccountNumber.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtAccountNumber.InputScopeAllowEmpty = false;
            this.txtAccountNumber.InputScopeCustomString = null;
            this.txtAccountNumber.IsValid = null;
            this.txtAccountNumber.Location = new System.Drawing.Point(145, 75);
            this.txtAccountNumber.MaxLength = 32767;
            this.txtAccountNumber.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtAccountNumber.Name = "txtAccountNumber";
            this.txtAccountNumber.PromptText = "(Type here...)";
            this.txtAccountNumber.ReadOnly = false;
            this.txtAccountNumber.ShowMandatoryMark = false;
            this.txtAccountNumber.Size = new System.Drawing.Size(255, 21);
            this.txtAccountNumber.TabIndex = 1;
            this.txtAccountNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtAccountNumber.UpperCaseOnly = false;
            this.txtAccountNumber.ValidationErrorMessage = "Validation Error!";
            // 
            // dtpOpeningDate
            // 
            this.dtpOpeningDate.Date = "19/01/2017";
            this.dtpOpeningDate.Location = new System.Drawing.Point(143, 135);
            this.dtpOpeningDate.MaximumSize = new System.Drawing.Size(400, 25);
            this.dtpOpeningDate.MinimumSize = new System.Drawing.Size(60, 25);
            this.dtpOpeningDate.Name = "dtpOpeningDate";
            this.dtpOpeningDate.PresetServerDate = true;
            this.dtpOpeningDate.Size = new System.Drawing.Size(259, 25);
            this.dtpOpeningDate.TabIndex = 4;
            this.dtpOpeningDate.Value = new System.DateTime(2017, 1, 19, 16, 38, 25, 896);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(58, 142);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Opening Date :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(49, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Customer Name :";
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.BackColor = System.Drawing.SystemColors.Window;
            this.txtCustomerName.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtCustomerName.InputScopeAllowEmpty = false;
            this.txtCustomerName.InputScopeCustomString = null;
            this.txtCustomerName.IsValid = null;
            this.txtCustomerName.Location = new System.Drawing.Point(145, 106);
            this.txtCustomerName.MaxLength = 32767;
            this.txtCustomerName.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.PromptText = "(Type here...)";
            this.txtCustomerName.ReadOnly = false;
            this.txtCustomerName.ShowMandatoryMark = false;
            this.txtCustomerName.Size = new System.Drawing.Size(255, 21);
            this.txtCustomerName.TabIndex = 1;
            this.txtCustomerName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtCustomerName.UpperCaseOnly = false;
            this.txtCustomerName.ValidationErrorMessage = "Validation Error!";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(468, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Lien Amount :";
            // 
            // txtLienAmount
            // 
            this.txtLienAmount.BackColor = System.Drawing.SystemColors.Window;
            this.txtLienAmount.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtLienAmount.InputScopeAllowEmpty = false;
            this.txtLienAmount.InputScopeCustomString = null;
            this.txtLienAmount.IsValid = null;
            this.txtLienAmount.Location = new System.Drawing.Point(548, 75);
            this.txtLienAmount.MaxLength = 32767;
            this.txtLienAmount.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtLienAmount.Name = "txtLienAmount";
            this.txtLienAmount.PromptText = "(Type here...)";
            this.txtLienAmount.ReadOnly = false;
            this.txtLienAmount.ShowMandatoryMark = false;
            this.txtLienAmount.Size = new System.Drawing.Size(255, 21);
            this.txtLienAmount.TabIndex = 1;
            this.txtLienAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtLienAmount.UpperCaseOnly = false;
            this.txtLienAmount.ValidationErrorMessage = "Validation Error!";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(461, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Block Amount :";
            // 
            // txtBlockAmount
            // 
            this.txtBlockAmount.BackColor = System.Drawing.SystemColors.Window;
            this.txtBlockAmount.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtBlockAmount.InputScopeAllowEmpty = false;
            this.txtBlockAmount.InputScopeCustomString = null;
            this.txtBlockAmount.IsValid = null;
            this.txtBlockAmount.Location = new System.Drawing.Point(548, 106);
            this.txtBlockAmount.MaxLength = 32767;
            this.txtBlockAmount.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtBlockAmount.Name = "txtBlockAmount";
            this.txtBlockAmount.PromptText = "(Type here...)";
            this.txtBlockAmount.ReadOnly = false;
            this.txtBlockAmount.ShowMandatoryMark = false;
            this.txtBlockAmount.Size = new System.Drawing.Size(255, 21);
            this.txtBlockAmount.TabIndex = 1;
            this.txtBlockAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtBlockAmount.UpperCaseOnly = false;
            this.txtBlockAmount.ValidationErrorMessage = "Validation Error!";
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(833, 36);
            this.customTitlebar1.TabIndex = 11;
            this.customTitlebar1.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(707, 454);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(110, 28);
            this.btnClose.TabIndex = 12;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            // 
            // secondaryTitleLabel
            // 
            this.secondaryTitleLabel.AutoSize = true;
            this.secondaryTitleLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.secondaryTitleLabel.Font = new System.Drawing.Font("Segoe UI Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondaryTitleLabel.ForeColor = System.Drawing.Color.White;
            this.secondaryTitleLabel.Location = new System.Drawing.Point(3, 4);
            this.secondaryTitleLabel.Name = "secondaryTitleLabel";
            this.secondaryTitleLabel.Size = new System.Drawing.Size(208, 25);
            this.secondaryTitleLabel.TabIndex = 0;
            this.secondaryTitleLabel.Text = "Deposit Account Closing";
            // 
            // customHeaderControl1
            // 
            this.customHeaderControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customHeaderControl1.BackColor = System.Drawing.Color.White;
            this.customHeaderControl1.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.customHeaderControl1.Location = new System.Drawing.Point(12, 43);
            this.customHeaderControl1.Margin = new System.Windows.Forms.Padding(4);
            this.customHeaderControl1.MinimumSize = new System.Drawing.Size(0, 16);
            this.customHeaderControl1.Name = "customHeaderControl1";
            this.customHeaderControl1.Size = new System.Drawing.Size(809, 25);
            this.customHeaderControl1.TabIndex = 13;
            this.customHeaderControl1.Text = " Customer Information ";
            // 
            // customHeaderControl2
            // 
            this.customHeaderControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customHeaderControl2.BackColor = System.Drawing.Color.White;
            this.customHeaderControl2.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.customHeaderControl2.Location = new System.Drawing.Point(12, 178);
            this.customHeaderControl2.Margin = new System.Windows.Forms.Padding(4);
            this.customHeaderControl2.MinimumSize = new System.Drawing.Size(0, 16);
            this.customHeaderControl2.Name = "customHeaderControl2";
            this.customHeaderControl2.Size = new System.Drawing.Size(809, 25);
            this.customHeaderControl2.TabIndex = 13;
            this.customHeaderControl2.Text = " Transaction Information ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.txtPaymentInformationTotal);
            this.groupBox1.Controls.Add(this.txtUncalculatedProfit);
            this.groupBox1.Controls.Add(this.txtBalance);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtProvisionedProfit);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(17, 205);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(396, 194);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Payment Information";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Location = new System.Drawing.Point(18, 119);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(373, 1);
            this.panel1.TabIndex = 8;
            // 
            // txtPaymentInformationTotal
            // 
            this.txtPaymentInformationTotal.BackColor = System.Drawing.SystemColors.Window;
            this.txtPaymentInformationTotal.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtPaymentInformationTotal.InputScopeAllowEmpty = false;
            this.txtPaymentInformationTotal.InputScopeCustomString = null;
            this.txtPaymentInformationTotal.IsValid = null;
            this.txtPaymentInformationTotal.Location = new System.Drawing.Point(132, 130);
            this.txtPaymentInformationTotal.MaxLength = 32767;
            this.txtPaymentInformationTotal.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtPaymentInformationTotal.Name = "txtPaymentInformationTotal";
            this.txtPaymentInformationTotal.PromptText = "(Type here...)";
            this.txtPaymentInformationTotal.ReadOnly = false;
            this.txtPaymentInformationTotal.ShowMandatoryMark = false;
            this.txtPaymentInformationTotal.Size = new System.Drawing.Size(209, 21);
            this.txtPaymentInformationTotal.TabIndex = 1;
            this.txtPaymentInformationTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtPaymentInformationTotal.UpperCaseOnly = false;
            this.txtPaymentInformationTotal.ValidationErrorMessage = "Validation Error!";
            // 
            // txtUncalculatedProfit
            // 
            this.txtUncalculatedProfit.BackColor = System.Drawing.SystemColors.Window;
            this.txtUncalculatedProfit.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtUncalculatedProfit.InputScopeAllowEmpty = false;
            this.txtUncalculatedProfit.InputScopeCustomString = null;
            this.txtUncalculatedProfit.IsValid = null;
            this.txtUncalculatedProfit.Location = new System.Drawing.Point(132, 88);
            this.txtUncalculatedProfit.MaxLength = 32767;
            this.txtUncalculatedProfit.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtUncalculatedProfit.Name = "txtUncalculatedProfit";
            this.txtUncalculatedProfit.PromptText = "(Type here...)";
            this.txtUncalculatedProfit.ReadOnly = false;
            this.txtUncalculatedProfit.ShowMandatoryMark = false;
            this.txtUncalculatedProfit.Size = new System.Drawing.Size(209, 21);
            this.txtUncalculatedProfit.TabIndex = 1;
            this.txtUncalculatedProfit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtUncalculatedProfit.UpperCaseOnly = false;
            this.txtUncalculatedProfit.ValidationErrorMessage = "Validation Error!";
            // 
            // txtBalance
            // 
            this.txtBalance.BackColor = System.Drawing.SystemColors.Window;
            this.txtBalance.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtBalance.InputScopeAllowEmpty = false;
            this.txtBalance.InputScopeCustomString = null;
            this.txtBalance.IsValid = null;
            this.txtBalance.Location = new System.Drawing.Point(132, 23);
            this.txtBalance.MaxLength = 32767;
            this.txtBalance.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtBalance.Name = "txtBalance";
            this.txtBalance.PromptText = "(Type here...)";
            this.txtBalance.ReadOnly = false;
            this.txtBalance.ShowMandatoryMark = false;
            this.txtBalance.Size = new System.Drawing.Size(209, 21);
            this.txtBalance.TabIndex = 1;
            this.txtBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtBalance.UpperCaseOnly = false;
            this.txtBalance.ValidationErrorMessage = "Validation Error!";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(68, 134);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Total :";
            // 
            // txtProvisionedProfit
            // 
            this.txtProvisionedProfit.BackColor = System.Drawing.SystemColors.Window;
            this.txtProvisionedProfit.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtProvisionedProfit.InputScopeAllowEmpty = false;
            this.txtProvisionedProfit.InputScopeCustomString = null;
            this.txtProvisionedProfit.IsValid = null;
            this.txtProvisionedProfit.Location = new System.Drawing.Point(132, 55);
            this.txtProvisionedProfit.MaxLength = 32767;
            this.txtProvisionedProfit.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtProvisionedProfit.Name = "txtProvisionedProfit";
            this.txtProvisionedProfit.PromptText = "(Type here...)";
            this.txtProvisionedProfit.ReadOnly = false;
            this.txtProvisionedProfit.ShowMandatoryMark = false;
            this.txtProvisionedProfit.Size = new System.Drawing.Size(209, 21);
            this.txtProvisionedProfit.TabIndex = 1;
            this.txtProvisionedProfit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtProvisionedProfit.UpperCaseOnly = false;
            this.txtProvisionedProfit.ValidationErrorMessage = "Validation Error!";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 92);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Uncalculated Profit :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(349, 134);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(23, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Tk.";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(349, 92);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(23, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Tk.";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(349, 59);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(23, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Tk.";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(349, 27);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(23, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Tk.";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(72, 27);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Balance :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(29, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Provisioned Profit :";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panel2);
            this.groupBox2.Controls.Add(this.txtExciseDuty);
            this.groupBox2.Controls.Add(this.txtVAT);
            this.groupBox2.Controls.Add(this.txtDeductionInformationTotal);
            this.groupBox2.Controls.Add(this.txtTaxOnProfit);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txtClosingCharge);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Location = new System.Drawing.Point(420, 205);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(396, 194);
            this.groupBox2.TabIndex = 22;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Deduction Information";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            this.panel2.Location = new System.Drawing.Point(21, 150);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(373, 1);
            this.panel2.TabIndex = 8;
            // 
            // txtExciseDuty
            // 
            this.txtExciseDuty.BackColor = System.Drawing.SystemColors.Window;
            this.txtExciseDuty.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtExciseDuty.InputScopeAllowEmpty = false;
            this.txtExciseDuty.InputScopeCustomString = null;
            this.txtExciseDuty.IsValid = null;
            this.txtExciseDuty.Location = new System.Drawing.Point(133, 119);
            this.txtExciseDuty.MaxLength = 32767;
            this.txtExciseDuty.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtExciseDuty.Name = "txtExciseDuty";
            this.txtExciseDuty.PromptText = "(Type here...)";
            this.txtExciseDuty.ReadOnly = false;
            this.txtExciseDuty.ShowMandatoryMark = false;
            this.txtExciseDuty.Size = new System.Drawing.Size(209, 21);
            this.txtExciseDuty.TabIndex = 1;
            this.txtExciseDuty.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtExciseDuty.UpperCaseOnly = false;
            this.txtExciseDuty.ValidationErrorMessage = "Validation Error!";
            // 
            // txtVAT
            // 
            this.txtVAT.BackColor = System.Drawing.SystemColors.Window;
            this.txtVAT.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtVAT.InputScopeAllowEmpty = false;
            this.txtVAT.InputScopeCustomString = null;
            this.txtVAT.IsValid = null;
            this.txtVAT.Location = new System.Drawing.Point(133, 88);
            this.txtVAT.MaxLength = 32767;
            this.txtVAT.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtVAT.Name = "txtVAT";
            this.txtVAT.PromptText = "(Type here...)";
            this.txtVAT.ReadOnly = false;
            this.txtVAT.ShowMandatoryMark = false;
            this.txtVAT.Size = new System.Drawing.Size(209, 21);
            this.txtVAT.TabIndex = 1;
            this.txtVAT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtVAT.UpperCaseOnly = false;
            this.txtVAT.ValidationErrorMessage = "Validation Error!";
            // 
            // txtDeductionInformationTotal
            // 
            this.txtDeductionInformationTotal.BackColor = System.Drawing.SystemColors.Window;
            this.txtDeductionInformationTotal.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtDeductionInformationTotal.InputScopeAllowEmpty = false;
            this.txtDeductionInformationTotal.InputScopeCustomString = null;
            this.txtDeductionInformationTotal.IsValid = null;
            this.txtDeductionInformationTotal.Location = new System.Drawing.Point(133, 160);
            this.txtDeductionInformationTotal.MaxLength = 32767;
            this.txtDeductionInformationTotal.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtDeductionInformationTotal.Name = "txtDeductionInformationTotal";
            this.txtDeductionInformationTotal.PromptText = "(Type here...)";
            this.txtDeductionInformationTotal.ReadOnly = false;
            this.txtDeductionInformationTotal.ShowMandatoryMark = false;
            this.txtDeductionInformationTotal.Size = new System.Drawing.Size(209, 21);
            this.txtDeductionInformationTotal.TabIndex = 1;
            this.txtDeductionInformationTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtDeductionInformationTotal.UpperCaseOnly = false;
            this.txtDeductionInformationTotal.ValidationErrorMessage = "Validation Error!";
            // 
            // txtTaxOnProfit
            // 
            this.txtTaxOnProfit.BackColor = System.Drawing.SystemColors.Window;
            this.txtTaxOnProfit.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtTaxOnProfit.InputScopeAllowEmpty = false;
            this.txtTaxOnProfit.InputScopeCustomString = null;
            this.txtTaxOnProfit.IsValid = null;
            this.txtTaxOnProfit.Location = new System.Drawing.Point(133, 23);
            this.txtTaxOnProfit.MaxLength = 32767;
            this.txtTaxOnProfit.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtTaxOnProfit.Name = "txtTaxOnProfit";
            this.txtTaxOnProfit.PromptText = "(Type here...)";
            this.txtTaxOnProfit.ReadOnly = false;
            this.txtTaxOnProfit.ShowMandatoryMark = false;
            this.txtTaxOnProfit.Size = new System.Drawing.Size(209, 21);
            this.txtTaxOnProfit.TabIndex = 1;
            this.txtTaxOnProfit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtTaxOnProfit.UpperCaseOnly = false;
            this.txtTaxOnProfit.ValidationErrorMessage = "Validation Error!";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(41, 59);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Closing Charge :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(52, 27);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Tax on Profit :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(88, 164);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(37, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Total :";
            // 
            // txtClosingCharge
            // 
            this.txtClosingCharge.BackColor = System.Drawing.SystemColors.Window;
            this.txtClosingCharge.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtClosingCharge.InputScopeAllowEmpty = false;
            this.txtClosingCharge.InputScopeCustomString = null;
            this.txtClosingCharge.IsValid = null;
            this.txtClosingCharge.Location = new System.Drawing.Point(133, 55);
            this.txtClosingCharge.MaxLength = 32767;
            this.txtClosingCharge.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtClosingCharge.Name = "txtClosingCharge";
            this.txtClosingCharge.PromptText = "(Type here...)";
            this.txtClosingCharge.ReadOnly = false;
            this.txtClosingCharge.ShowMandatoryMark = false;
            this.txtClosingCharge.Size = new System.Drawing.Size(209, 21);
            this.txtClosingCharge.TabIndex = 1;
            this.txtClosingCharge.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtClosingCharge.UpperCaseOnly = false;
            this.txtClosingCharge.ValidationErrorMessage = "Validation Error!";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(56, 123);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(69, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Excise Duty :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(350, 164);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(23, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Tk.";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(350, 123);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(23, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "Tk.";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(350, 92);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(23, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "Tk.";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(350, 59);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(23, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "Tk.";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(350, 27);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(23, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Tk.";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(91, 92);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "VAT :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(471, 424);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(74, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Net Payment :";
            // 
            // txtNetPayment
            // 
            this.txtNetPayment.BackColor = System.Drawing.SystemColors.Window;
            this.txtNetPayment.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtNetPayment.InputScopeAllowEmpty = false;
            this.txtNetPayment.InputScopeCustomString = null;
            this.txtNetPayment.IsValid = null;
            this.txtNetPayment.Location = new System.Drawing.Point(553, 420);
            this.txtNetPayment.MaxLength = 32767;
            this.txtNetPayment.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtNetPayment.Name = "txtNetPayment";
            this.txtNetPayment.PromptText = "(Type here...)";
            this.txtNetPayment.ReadOnly = false;
            this.txtNetPayment.ShowMandatoryMark = false;
            this.txtNetPayment.Size = new System.Drawing.Size(209, 21);
            this.txtNetPayment.TabIndex = 1;
            this.txtNetPayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtNetPayment.UpperCaseOnly = false;
            this.txtNetPayment.ValidationErrorMessage = "Validation Error!";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.DarkGray;
            this.panel3.Location = new System.Drawing.Point(13, 408);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(808, 1);
            this.panel3.TabIndex = 8;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(770, 424);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(23, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "Tk.";
            // 
            // btnCloseAccount
            // 
            this.btnCloseAccount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseAccount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnCloseAccount.FlatAppearance.BorderSize = 0;
            this.btnCloseAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseAccount.ForeColor = System.Drawing.Color.White;
            this.btnCloseAccount.Location = new System.Drawing.Point(569, 454);
            this.btnCloseAccount.Name = "btnCloseAccount";
            this.btnCloseAccount.Size = new System.Drawing.Size(132, 28);
            this.btnCloseAccount.TabIndex = 12;
            this.btnCloseAccount.Text = "Close Account";
            this.btnCloseAccount.UseVisualStyleBackColor = false;
            this.btnCloseAccount.Click += new System.EventHandler(this.btnCloseAccount_Click);
            // 
            // DepositAccountClosing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(833, 493);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.txtNetPayment);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.customHeaderControl2);
            this.Controls.Add(this.customHeaderControl1);
            this.Controls.Add(this.secondaryTitleLabel);
            this.Controls.Add(this.btnCloseAccount);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.dtpOpeningDate);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.txtCustomerName);
            this.Controls.Add(this.txtBlockAmount);
            this.Controls.Add(this.txtLienAmount);
            this.Controls.Add(this.txtAccountNumber);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "DepositAccountClosing";
            this.Tag = "43, 87, 154";
            this.Text = "DepositAccountClosing";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private Agent.CustomControls.CustomTextBox txtAccountNumber;
        private CustomControls.CustomDateTimePicker dtpOpeningDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Agent.CustomControls.CustomTextBox txtCustomerName;
        private System.Windows.Forms.Label label4;
        private Agent.CustomControls.CustomTextBox txtLienAmount;
        private System.Windows.Forms.Label label5;
        private Agent.CustomControls.CustomTextBox txtBlockAmount;
        private Agent.CustomControls.CustomTitlebar customTitlebar1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label secondaryTitleLabel;
        private Agent.CustomControls.CustomHeaderControl customHeaderControl1;
        private Agent.CustomControls.CustomHeaderControl customHeaderControl2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private Agent.CustomControls.CustomTextBox txtProvisionedProfit;
        private Agent.CustomControls.CustomTextBox txtUncalculatedProfit;
        private System.Windows.Forms.Label label7;
        private Agent.CustomControls.CustomTextBox txtBalance;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel1;
        private Agent.CustomControls.CustomTextBox txtPaymentInformationTotal;
        private System.Windows.Forms.Label label9;
        private Agent.CustomControls.CustomTextBox txtVAT;
        private Agent.CustomControls.CustomTextBox txtTaxOnProfit;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private Agent.CustomControls.CustomTextBox txtClosingCharge;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel2;
        private Agent.CustomControls.CustomTextBox txtDeductionInformationTotal;
        private System.Windows.Forms.Label label13;
        private Agent.CustomControls.CustomTextBox txtNetPayment;
        private System.Windows.Forms.Label label14;
        private Agent.CustomControls.CustomTextBox txtExciseDuty;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button btnCloseAccount;
    }
}