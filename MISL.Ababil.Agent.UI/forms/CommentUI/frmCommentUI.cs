﻿using MetroFramework.Forms;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;

namespace MISL.Ababil.Agent.UI.forms.CommentUI
{
    public partial class frmCommentUI : MetroForm
    {
        public List<CommentDto> _comments = new List<CommentDto>();
        public CommentDto _commentDraft = new CommentDto();
        private CommentDto _commentDtoDraft = new CommentDto();
        Packet _receivedPacket;
        string _stage = "";
        private Packet _packet;

        public frmCommentUI()
        {
            InitializeComponent();
        }

        public frmCommentUI(Packet packet, CommentDto commentDraft, List<CommentDto> comments, string stage)
        {
            _packet = packet;
            InitializeComponent();
            _commentDraft = commentDraft;
            _comments = comments;
            _stage = stage;
            LoadCommentFromDto();

            //if (_packet.actionType == FormActionType.View && SessionInfo.userBasicInformation.userCategory != UserCategory.SubAgentUser)
            //{
            //    txtComment.Enabled = btnPost.Enabled = false;
            //}
        }

        private void LoadCommentFromDto()
        {
            pnlComments.Controls.Clear();
            if (_comments == null)
            {
                _comments = new List<CommentDto>();
            }
            for (int i = 0; i < _comments.Count; i++)
            {
                CommentItem commentItemTmp = new CommentItem
                {
                    Editable = false,
                    lblComment = { Text = _comments[i].comment },
                    lblUserName = { Text = _comments[i].commentUser },
                    lblStage = { Text = _comments[i].stage }
                };

                if (_comments[i].commentDate == null)
                {
                    commentItemTmp.lblDateTime.Text = SessionInfo.currentDate.ToString("dd-MM-yyyy h:mm:ss tt");
                }
                else
                {
                    commentItemTmp.lblDateTime.Text =
                        (_comments[i].commentDate)
                        +
                        " at "
                        +
                        (_comments[i].commentTime);
                }
                //commentItemTmp.lblDateTime.Text = _comments[i].commentDate + " at " + _comments[i].commentTime;

                pnlComments.Controls.Add(commentItemTmp);
            }

            if (_commentDraft != null)
            {
                if (!string.IsNullOrEmpty(_commentDraft.comment))
                {
                    CommentItem commentItem = new CommentItem();
                    commentItem.Editable = true;
                    commentItem.lblComment.Text = _commentDraft.comment;
                    commentItem.lblUserName.Text = _commentDraft.commentUser;
                    commentItem.lblStage.Text = _commentDraft.stage;

                    if (_commentDraft.commentDate == null)
                    {
                        commentItem.lblDateTime.Text = SessionInfo.currentDate.ToString("dd-MM-yyyy") + " at " + DateTime.Now.ToString("dd-MM-yyyy h:mm:ss tt");
                    }
                    else
                    {
                        commentItem.lblDateTime.Text =
                            _commentDraft.commentDate ?? SessionInfo.currentDate.ToString("dd-MM-yyyy")
                            +
                            " at "
                            +
                            _commentDraft.commentTime ?? SessionInfo.currentDate.ToString("hh:mm:ss tt");
                    }

                    commentItem._parent = this;
                    pnlComments.Controls.Add(commentItem);
                }
            }
        }

        private void btnPost_Click(object sender, EventArgs e)
        {
            CommentDto commentDto = new CommentDto();
            commentDto.comment = txtComment.Text;
            commentDto.commentUser = SessionInfo.username;
            commentDto.stage = _stage;
            //commentDto.commentDate = 

            _commentDraft = commentDto;

            LoadCommentFromDto();

            txtComment.Text = "";
            txtComment.Focus();
        }

        private void frmCommentUI_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtComment.Text))
            {
                this.Close();
            }
        }

        private void frmCommentUI_Deactivate(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtComment.Text))
            {
                this.Close();
            }
        }

        private void frmCommentUI_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }
    }
}