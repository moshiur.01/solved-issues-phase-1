﻿using MISL.Ababil.Agent.Communication;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.Remittance;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.UI.forms.ProgressUI;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Module.Common.Exporter;
using MISL.Ababil.Agent.Module.Common.Exportor;

namespace MISL.Ababil.Agent.UI.forms
{
    public partial class frmRemittanceAdmin : CustomForm
    {
        int columnLoaded = 0;
        private List<Remittance> remittances;

        public frmRemittanceAdmin()
        {
            InitializeComponent();
            cmbNameofExchangeHouse.SelectedItem = null;
            cmbRemittanceStatus.SelectedItem = null;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Search();
            customDataGridViewHeader.ExportReportSubtitleThree = "Remittance";
            try
            {
                DataGridHeaderUtility.ReportHederDataExportToPdf(customDataGridViewHeader);
            }
            catch (Exception ex)
            {
                // ignored
            }
        }

        //private string getOutletName(string voucherNumber)
        //{
        //    ConsumerServices consumerService = new ConsumerServices();
        //    RemittanceReportDto remittanceReportDto = consumerService.GetRemittanceReportDto(voucherNumber);
        //    if (remittanceReportDto.outletName != "")
        //    {
        //        return remittanceReportDto.outletName;
        //    }
        //    return "";
        //    //return "_";
        //}

        private void Search()
        {
            dgvRemittance.DataSource = null;
            dgvRemittance.Rows.Clear();
            dgvRemittance.Columns.Clear();
            Remittance remittance = FillRemittance();
            RemittanceCom objRemittanceCom = new RemittanceCom();

            try
            {
                ProgressUIManager.ShowProgress(this);
                remittances = objRemittanceCom.getListOfRemittance(remittance);
                ProgressUIManager.CloseProgress();

                if (remittances != null && remittances.Count > 0)
                {

                    DataGridViewButtonColumn buttonColumn = new DataGridViewButtonColumn();
                    buttonColumn.Text = "Open";
                    buttonColumn.Width = 60;
                    buttonColumn.UseColumnTextForButtonValue = true;
                    dgvRemittance.Columns.Add(buttonColumn);
                    //columnLoaded = 1;

                    if (SessionInfo.rights.Contains("REMITANCE_FIRST_APPROVE"))
                    {
                        DataGridViewButtonColumn buttonColumn2 = new DataGridViewButtonColumn();
                        buttonColumn2.Text = "Process";
                        buttonColumn2.UseColumnTextForButtonValue = true;
                        dgvRemittance.Columns.Add(buttonColumn2);
                        //columnLoaded = 2;
                    }
                    else if (SessionInfo.rights.Contains("REMITANCE_SECOND_APPROVE"))
                    {
                        DataGridViewButtonColumn buttonColumn2 = new DataGridViewButtonColumn();
                        buttonColumn2.Text = "Process";
                        buttonColumn2.UseColumnTextForButtonValue = true;
                        dgvRemittance.Columns.Add(buttonColumn2);
                        //columnLoaded = 2;
                    }


                    dgvRemittance.DataSource = null;
                    dgvRemittance.DataSource = remittances.Select(o => new RemittanceGrid(o)
                    {
                        id = o.id,
                        district = o.benificaryAddress.district.title,
                        exchangeHouse = o.exchangeHouse.companyName,
                        outlet = o.outletName,
                        benificaryName = o.benificaryName,
                        benificarynid = o.benificarynid,
                        senderName = o.senderName,
                        pinCode = o.pinCode,
                        referenceNumber = o.referanceNumber,
                        remittanceStatus = o.remittanceStatus,
                        entryUser = o.entryUser,
                        entryUserDateTime = o.entryUserDateTime,
                        firstApprover = o.firstApprover,
                        firstApprovalDateTime = o.firstApprovalDateTime,
                        secondApprover = o.secondApprover,
                        secondApprovalDateTime = o.secondApprovalDateTime,
                        comments = o.comments,
                        filter =
                        (
                            o.id +
                            o.benificaryAddress.district.title +
                            o.exchangeHouse.companyName +
                            o.outletName +
                            o.benificaryName +
                            o.benificarynid +
                            o.senderName +
                            o.pinCode +
                            o.referanceNumber +
                            o.remittanceStatus +
                            o.entryUser +
                            o.entryUserDateTime +
                            o.firstApprover +
                            o.firstApprovalDateTime +
                            o.secondApprover +
                            o.secondApprovalDateTime +
                            o.comments
                        ).ToLower()
                    }).ToList();
                    if (dgvRemittance.Columns.Count > 0)
                    {
                        dgvRemittance.Columns[dgvRemittance.Columns.Count - 1].Visible = false;
                    }
                }
                else
                {
                    ProgressUIManager.CloseProgress();
                    btnSearch.Enabled = true;
                    MsgBox.showInformation("No applications available");
                }
            }
            catch (Exception ex)
            {
                ProgressUIManager.CloseProgress();
                btnSearch.Enabled = true;
                MsgBox.ShowError(ex.Message);
            }

            lblItemsFound.Text = "   Item(s) Found: " + dgvRemittance.Rows.Count.ToString();
            this.Enabled = true;
            this.UseWaitCursor = false;
        }

        public class RemittanceGrid
        {
            public long id { get; set; }
            public string district { get; set; }
            public string outlet { get; set; }
            public string exchangeHouse { get; set; }
            public String referenceNumber { get; set; }
            public RemittanceStatus remittanceStatus { get; set; }
            //public ExchangeHouse exchangeHouse { get; set; }
            public String pinCode { get; set; }
            //public Decimal expectedAmount { get; set; }
            public String senderName { get; set; }
            //public Country senderCountry { get; set; }
            //public String senderPurpose { get; set; }                                            
            //public string remittanceStatus { get; set; }
            public String benificaryName { get; set; }
            //public String benificaryFatherName { get; set; }
            //public String benificaryMotherName { get; set; }
            //public Address benificaryAddress { get; set; }
            //public String benificaryMobileNumber { get; set; }
            //public String status { get; set; }
            public String comments { get; set; }
            public String benificarynid { get; set; }
            public string entryUser { get; set; }
            public string entryUserDateTime { get; set; }
            public string firstApprover { get; set; }
            public string firstApprovalDateTime { get; set; }
            public string secondApprover { get; set; }
            public string secondApprovalDateTime { get; set; }
            public string filter { get; set; }

            private Remittance _obj;

            public RemittanceGrid(Remittance obj)
            {
                _obj = obj;
            }

            public Remittance GetModel()
            {
                return _obj;
            }
        }

        private Remittance FillRemittance()
        {
            ExchangeHouse exHouse = new ExchangeHouse
            {
                id = (long)cmbNameofExchangeHouse.SelectedValue
            };
            Remittance remittance = new Remittance
            {
                exchangeHouse = exHouse,
                benificaryName = txtRecipientName.Text,
                pinCode = txtPINCode.Text,
                referanceNumber = txtReferanceNumber.Text
            };

            if (SessionInfo.rights.Contains("REMITANCE_ENTRY"))
            {
                remittance.entryUser = SessionInfo.username;
            }

            RemittanceStatus remStatus = new RemittanceStatus();
            Enum.TryParse<RemittanceStatus>(cmbRemittanceStatus.Text, out remStatus);
            remittance.remittanceStatus = remStatus;

            if (SessionInfo.userBasicInformation.userCategory == UserCategory.SubAgentUser)
            {
                remittance.outletId = SessionInfo.userBasicInformation.outlet.id;
            }
            else if (SessionInfo.userBasicInformation.userCategory == UserCategory.BranchUser)
            {
                remittance.branchId = SessionInfo.userBasicInformation.userBranch ?? 0;
            }

            return remittance;
        }

        private void FillExchangeHouseList(ref ComboBox cbxExHouse)
        {
            List<ExchangeHouse> exHouses = new List<ExchangeHouse>(LocalStorageService.LocalCache.GetExchangeHouses());
            exHouses.Insert(0, new ExchangeHouse
            {
                id = 0,
                companyName = "(All)"
            });
            FillComboBox(cbxExHouse, new BindingSource { DataSource = exHouses }, "companyName", "id");
        }

        public static void FillComboBox(ComboBox cmb, BindingSource bs, string displayMember, string valueMember)
        {
            cmb.DataSource = bs;
            cmb.DisplayMember = displayMember;
            cmb.ValueMember = valueMember;
        }

        public static void FillDataGridView(DataGridView dgv, BindingSource bs, string dataMember)
        {
            dgv.DataSource = bs;
            dgv.DataMember = dataMember;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ResetSearch();
        }

        private void ResetSearch()
        {
            txtRecipientName.Text = null;
            txtRecipientNationalID.Text = null;
            txtSenderName.Text = null;
            txtPINCode.Text = null;
            txtReferanceNumber.Text = null;
            cmbRemittanceStatus.SelectedIndex = 0;
            cmbNameofExchangeHouse.SelectedIndex = 0;
            cmbNameofExchangeHouse.Focus();
        }

        private void frmRemittanceAdmin_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            FillExchangeHouseList(ref cmbNameofExchangeHouse);

            if (SessionInfo.rights.Contains("REMITANCE_ENTRY"))
            {
                cmbRemittanceStatus.Items.Clear();
                cmbRemittanceStatus.Items.Add("Submitted");
                cmbRemittanceStatus.Items.Add("ApprovedFirst");
                cmbRemittanceStatus.Items.Add("Approved");
                cmbRemittanceStatus.Items.Add("Corrected"); ///editable
                cmbRemittanceStatus.Items.Add("Rejected");
                cmbRemittanceStatus.Items.Add("Disbursed");
                cmbRemittanceStatus.SelectedIndex = 0;
            }
            if (SessionInfo.rights.Contains("REMITANCE_FIRST_APPROVE"))
            {
                if (!cmbRemittanceStatus.Items.Contains("Submitted"))
                {
                    cmbRemittanceStatus.Items.Add("Submitted");
                    cmbRemittanceStatus.SelectedIndex = 0;
                }
            }
            if (SessionInfo.rights.Contains("REMITANCE_SECOND_APPROVE"))
            {
                if (!cmbRemittanceStatus.Items.Contains("ApprovedFirst"))
                {
                    cmbRemittanceStatus.Items.Add("ApprovedFirst");
                    cmbRemittanceStatus.SelectedIndex = 0;
                }
            }
        }

        private void dgvRemittance_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (dgvRemittance.Rows.Count <= 0) return;
            if (e.RowIndex < 0 || !(senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn)) return;

            try
            {
                if (dgvRemittance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "Process")
                {
                    switch (remittances[e.RowIndex].remittanceStatus)
                    {
                        case RemittanceStatus.Submitted:
                            new frmRemittanceApprove
                            {
                                _remittance = remittances[e.RowIndex]
                            }.ShowDialog();
                            Search();
                            return;
                        case RemittanceStatus.ApprovedFirst:
                            new frmRemittanceApprove
                            {
                                _remittance = remittances[e.RowIndex]
                            }.ShowDialog();
                            Search();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }

            try
            {
                if (dgvRemittance.Rows.Count <= 0) return;
                if (e.RowIndex < 0 || !(senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn)) return;

                if (dgvRemittance.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "Open")
                {
                    frmRemittanceAgentRequest frmRemittanceAgentRequest = new frmRemittanceAgentRequest
                    {
                        _remittance = remittances[e.RowIndex],
                        _IsFromAdmin = true
                    };
                    frmRemittanceAgentRequest.changeControlsReadOnly((remittances[e.RowIndex].remittanceStatus != RemittanceStatus.Corrected));
                    frmRemittanceAgentRequest.ShowDialog();
                    Search();
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        private void frmRemittanceAdmin_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void autoRefreshTimer_Tick(object sender, EventArgs e)
        {
            //Search();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                new ExportUtility().GeneratePDFFromDataGridView(
                        dgvRemittance,
                        "AIBL",
                        "AIBL Address",
                        "Outlet Name",
                        "Remittance"
                    );
            }
            catch (IndexOutOfRangeException)
            {
                MsgBox.showWarning("Data not available.");
            }
            catch (Exception ex)
            {
                MsgBox.showWarning(ex.Message);
            }
        }

        /*
        private void btnFilter_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text))
            {
                int i = 0;
                for (; i < dgvRemittance.Rows.Count; i++)
                {
                    dgvRemittance.Rows[i].Visible = true;
                }
                lblItemsFound.Text = "   Item(s) Found: " + i.ToString();
            }
            else
            {
                dgvRemittance.ClearSelection();
                ((CurrencyManager)BindingContext[dgvRemittance.DataSource]).SuspendBinding();
                dgvRemittance.CurrentCell = null;

                int found = 0;
                for (int i = 0; i < dgvRemittance.Rows.Count; i++)
                {
                    if (
                        dgvRemittance.Rows[i].Cells[dgvRemittance.Columns.Count - 1].Value.ToString()
                            .IndexOf(textBox1.Text) < 0)
                    {
                        dgvRemittance.Rows[i].Visible = false;
                    }
                    else
                    {
                        dgvRemittance.Rows[i].Visible = true;
                        found++;
                    }
                }
                lblItemsFound.Text = "   Item(s) Found: " + found.ToString();
            }
        }
        */
    }
}