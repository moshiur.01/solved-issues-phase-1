﻿using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Module.Common.Exporter;
using MISL.Ababil.Agent.Module.Common.Exportor;

namespace MISL.Ababil.Agent.UI.forms
{
    partial class frmRemittanceAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRemittanceAdmin));
            this.dgvRemittance = new System.Windows.Forms.DataGridView();
            this.lblSenderName = new System.Windows.Forms.Label();
            this.lblPINCode = new System.Windows.Forms.Label();
            this.lblRecipientNationalID = new System.Windows.Forms.Label();
            this.lblRecipientName = new System.Windows.Forms.Label();
            this.lblNameofExchangeHouse = new System.Windows.Forms.Label();
            this.txtRecipientName = new System.Windows.Forms.TextBox();
            this.txtRecipientNationalID = new System.Windows.Forms.TextBox();
            this.txtSenderName = new System.Windows.Forms.TextBox();
            this.txtPINCode = new System.Windows.Forms.TextBox();
            this.cmbNameofExchangeHouse = new System.Windows.Forms.ComboBox();
            this.lblReferanceNumber = new System.Windows.Forms.Label();
            this.txtReferanceNumber = new System.Windows.Forms.TextBox();
            this.lblRemittanceStatus = new System.Windows.Forms.Label();
            this.cmbRemittanceStatus = new System.Windows.Forms.ComboBox();
            this.lblItemsFound = new System.Windows.Forms.Label();
            this.autoRefreshTimer = new System.Windows.Forms.Timer(this.components);
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.lblMndField5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClsoe = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.customDataGridViewHeader = new MISL.Ababil.Agent.Module.Common.Exporter.CustomDataGridViewHeader();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRemittance)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvRemittance
            // 
            this.dgvRemittance.AllowUserToAddRows = false;
            this.dgvRemittance.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvRemittance.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvRemittance.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvRemittance.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(113)))), ((int)(((byte)(198)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvRemittance.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvRemittance.ColumnHeadersHeight = 28;
            this.dgvRemittance.EnableHeadersVisualStyles = false;
            this.dgvRemittance.GridColor = System.Drawing.Color.LightGray;
            this.dgvRemittance.Location = new System.Drawing.Point(1, 153);
            this.dgvRemittance.MultiSelect = false;
            this.dgvRemittance.Name = "dgvRemittance";
            this.dgvRemittance.RowHeadersVisible = false;
            this.dgvRemittance.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRemittance.Size = new System.Drawing.Size(935, 334);
            this.dgvRemittance.TabIndex = 16;
            this.dgvRemittance.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRemittance_CellContentClick);
            // 
            // lblSenderName
            // 
            this.lblSenderName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblSenderName.AutoSize = true;
            this.lblSenderName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblSenderName.ForeColor = System.Drawing.Color.White;
            this.lblSenderName.Location = new System.Drawing.Point(406, 488);
            this.lblSenderName.Name = "lblSenderName";
            this.lblSenderName.Size = new System.Drawing.Size(80, 17);
            this.lblSenderName.TabIndex = 6;
            this.lblSenderName.Text = "Sender Name :";
            this.lblSenderName.UseCompatibleTextRendering = true;
            this.lblSenderName.Visible = false;
            // 
            // lblPINCode
            // 
            this.lblPINCode.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblPINCode.AutoSize = true;
            this.lblPINCode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblPINCode.ForeColor = System.Drawing.Color.White;
            this.lblPINCode.Location = new System.Drawing.Point(484, 37);
            this.lblPINCode.Name = "lblPINCode";
            this.lblPINCode.Size = new System.Drawing.Size(59, 17);
            this.lblPINCode.TabIndex = 8;
            this.lblPINCode.Text = "PIN Code :";
            this.lblPINCode.UseCompatibleTextRendering = true;
            // 
            // lblRecipientNationalID
            // 
            this.lblRecipientNationalID.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblRecipientNationalID.AutoSize = true;
            this.lblRecipientNationalID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblRecipientNationalID.ForeColor = System.Drawing.Color.White;
            this.lblRecipientNationalID.Location = new System.Drawing.Point(34, 97);
            this.lblRecipientNationalID.Name = "lblRecipientNationalID";
            this.lblRecipientNationalID.Size = new System.Drawing.Size(117, 17);
            this.lblRecipientNationalID.TabIndex = 4;
            this.lblRecipientNationalID.Text = "Recipient National ID :";
            this.lblRecipientNationalID.UseCompatibleTextRendering = true;
            // 
            // lblRecipientName
            // 
            this.lblRecipientName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblRecipientName.AutoSize = true;
            this.lblRecipientName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblRecipientName.ForeColor = System.Drawing.Color.White;
            this.lblRecipientName.Location = new System.Drawing.Point(60, 68);
            this.lblRecipientName.Name = "lblRecipientName";
            this.lblRecipientName.Size = new System.Drawing.Size(91, 17);
            this.lblRecipientName.TabIndex = 2;
            this.lblRecipientName.Text = "Recipient Name :";
            this.lblRecipientName.UseCompatibleTextRendering = true;
            // 
            // lblNameofExchangeHouse
            // 
            this.lblNameofExchangeHouse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblNameofExchangeHouse.AutoSize = true;
            this.lblNameofExchangeHouse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblNameofExchangeHouse.ForeColor = System.Drawing.Color.White;
            this.lblNameofExchangeHouse.Location = new System.Drawing.Point(55, 38);
            this.lblNameofExchangeHouse.Name = "lblNameofExchangeHouse";
            this.lblNameofExchangeHouse.Size = new System.Drawing.Size(96, 17);
            this.lblNameofExchangeHouse.TabIndex = 0;
            this.lblNameofExchangeHouse.Text = "Exchange House :";
            this.lblNameofExchangeHouse.UseCompatibleTextRendering = true;
            // 
            // txtRecipientName
            // 
            this.txtRecipientName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtRecipientName.Location = new System.Drawing.Point(158, 65);
            this.txtRecipientName.Name = "txtRecipientName";
            this.txtRecipientName.Size = new System.Drawing.Size(247, 20);
            this.txtRecipientName.TabIndex = 3;
            // 
            // txtRecipientNationalID
            // 
            this.txtRecipientNationalID.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtRecipientNationalID.Location = new System.Drawing.Point(157, 94);
            this.txtRecipientNationalID.Name = "txtRecipientNationalID";
            this.txtRecipientNationalID.Size = new System.Drawing.Size(247, 20);
            this.txtRecipientNationalID.TabIndex = 5;
            // 
            // txtSenderName
            // 
            this.txtSenderName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtSenderName.Location = new System.Drawing.Point(503, 488);
            this.txtSenderName.Name = "txtSenderName";
            this.txtSenderName.Size = new System.Drawing.Size(301, 20);
            this.txtSenderName.TabIndex = 7;
            this.txtSenderName.Visible = false;
            // 
            // txtPINCode
            // 
            this.txtPINCode.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtPINCode.Location = new System.Drawing.Point(547, 34);
            this.txtPINCode.Name = "txtPINCode";
            this.txtPINCode.Size = new System.Drawing.Size(247, 20);
            this.txtPINCode.TabIndex = 9;
            // 
            // cmbNameofExchangeHouse
            // 
            this.cmbNameofExchangeHouse.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbNameofExchangeHouse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNameofExchangeHouse.FormattingEnabled = true;
            this.cmbNameofExchangeHouse.Location = new System.Drawing.Point(157, 34);
            this.cmbNameofExchangeHouse.Name = "cmbNameofExchangeHouse";
            this.cmbNameofExchangeHouse.Size = new System.Drawing.Size(247, 21);
            this.cmbNameofExchangeHouse.TabIndex = 1;
            // 
            // lblReferanceNumber
            // 
            this.lblReferanceNumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblReferanceNumber.AutoSize = true;
            this.lblReferanceNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblReferanceNumber.ForeColor = System.Drawing.Color.White;
            this.lblReferanceNumber.Location = new System.Drawing.Point(437, 67);
            this.lblReferanceNumber.Name = "lblReferanceNumber";
            this.lblReferanceNumber.Size = new System.Drawing.Size(106, 17);
            this.lblReferanceNumber.TabIndex = 10;
            this.lblReferanceNumber.Text = "Reference Number :";
            this.lblReferanceNumber.UseCompatibleTextRendering = true;
            // 
            // txtReferanceNumber
            // 
            this.txtReferanceNumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtReferanceNumber.Location = new System.Drawing.Point(547, 64);
            this.txtReferanceNumber.Name = "txtReferanceNumber";
            this.txtReferanceNumber.Size = new System.Drawing.Size(247, 20);
            this.txtReferanceNumber.TabIndex = 11;
            // 
            // lblRemittanceStatus
            // 
            this.lblRemittanceStatus.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblRemittanceStatus.AutoSize = true;
            this.lblRemittanceStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblRemittanceStatus.ForeColor = System.Drawing.Color.White;
            this.lblRemittanceStatus.Location = new System.Drawing.Point(440, 97);
            this.lblRemittanceStatus.Name = "lblRemittanceStatus";
            this.lblRemittanceStatus.Size = new System.Drawing.Size(103, 17);
            this.lblRemittanceStatus.TabIndex = 12;
            this.lblRemittanceStatus.Text = "Remittance Status :";
            this.lblRemittanceStatus.UseCompatibleTextRendering = true;
            // 
            // cmbRemittanceStatus
            // 
            this.cmbRemittanceStatus.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbRemittanceStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRemittanceStatus.FormattingEnabled = true;
            this.cmbRemittanceStatus.Location = new System.Drawing.Point(547, 93);
            this.cmbRemittanceStatus.Name = "cmbRemittanceStatus";
            this.cmbRemittanceStatus.Size = new System.Drawing.Size(247, 21);
            this.cmbRemittanceStatus.TabIndex = 13;
            // 
            // lblItemsFound
            // 
            this.lblItemsFound.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblItemsFound.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblItemsFound.ForeColor = System.Drawing.Color.White;
            this.lblItemsFound.Location = new System.Drawing.Point(0, 490);
            this.lblItemsFound.Name = "lblItemsFound";
            this.lblItemsFound.Size = new System.Drawing.Size(937, 24);
            this.lblItemsFound.TabIndex = 19;
            this.lblItemsFound.Text = "   Item(s) Found: 0";
            this.lblItemsFound.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // autoRefreshTimer
            // 
            this.autoRefreshTimer.Interval = 120000;
            this.autoRefreshTimer.Tick += new System.EventHandler(this.autoRefreshTimer_Tick);
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(937, 126);
            this.customTitlebar1.TabIndex = 20;
            // 
            // lblMndField5
            // 
            this.lblMndField5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblMndField5.AutoSize = true;
            this.lblMndField5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblMndField5.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMndField5.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblMndField5.Location = new System.Drawing.Point(404, 29);
            this.lblMndField5.Name = "lblMndField5";
            this.lblMndField5.Size = new System.Drawing.Size(28, 37);
            this.lblMndField5.TabIndex = 18;
            this.lblMndField5.Text = "*";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.label1.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Location = new System.Drawing.Point(794, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 37);
            this.label1.TabIndex = 18;
            this.label1.Text = "*";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 514);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(937, 1);
            this.panel1.TabIndex = 165;
            // 
            // btnClsoe
            // 
            this.btnClsoe.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnClsoe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnClsoe.FlatAppearance.BorderSize = 0;
            this.btnClsoe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClsoe.Image = global::MISL.Ababil.Agent.UI.Properties.Resources.Delete_16__1_;
            this.btnClsoe.Location = new System.Drawing.Point(829, 90);
            this.btnClsoe.Name = "btnClsoe";
            this.btnClsoe.Size = new System.Drawing.Size(96, 26);
            this.btnClsoe.TabIndex = 70;
            this.btnClsoe.Text = "Close";
            this.btnClsoe.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClsoe.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClsoe.UseVisualStyleBackColor = false;
            this.btnClsoe.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnReset
            // 
            this.btnReset.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnReset.FlatAppearance.BorderSize = 0;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.Image = global::MISL.Ababil.Agent.UI.Properties.Resources.Broom_16;
            this.btnReset.Location = new System.Drawing.Point(829, 61);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(96, 26);
            this.btnReset.TabIndex = 69;
            this.btnReset.Text = "Reset";
            this.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Image = global::MISL.Ababil.Agent.UI.Properties.Resources.Search_16__1_;
            this.btnSearch.Location = new System.Drawing.Point(829, 32);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(96, 26);
            this.btnSearch.TabIndex = 68;
            this.btnSearch.Text = "Search";
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // customDataGridViewHeader
            // 
            this.customDataGridViewHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customDataGridViewHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(123)))), ((int)(((byte)(208)))));
            this.customDataGridViewHeader.ExportReportSubtitleOne = "AIBL";
            this.customDataGridViewHeader.ExportReportSubtitleThree = "Subtitle Three";
            this.customDataGridViewHeader.ExportReportSubtitleTwo = "Report";
            this.customDataGridViewHeader.ExportReportTitle = "Remittance";
            this.customDataGridViewHeader.Filter = this.dgvRemittance;
            this.customDataGridViewHeader.HeaderText = "Remittance";
            this.customDataGridViewHeader.Location = new System.Drawing.Point(1, 127);
            this.customDataGridViewHeader.Name = "customDataGridViewHeader";
            this.customDataGridViewHeader.Size = new System.Drawing.Size(935, 28);
            this.customDataGridViewHeader.TabIndex = 169;
            // 
            // frmRemittanceAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 515);
            this.Controls.Add(this.customDataGridViewHeader);
            this.Controls.Add(this.dgvRemittance);
            this.Controls.Add(this.btnClsoe);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.lblItemsFound);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblMndField5);
            this.Controls.Add(this.cmbRemittanceStatus);
            this.Controls.Add(this.cmbNameofExchangeHouse);
            this.Controls.Add(this.txtReferanceNumber);
            this.Controls.Add(this.txtPINCode);
            this.Controls.Add(this.txtSenderName);
            this.Controls.Add(this.txtRecipientNationalID);
            this.Controls.Add(this.txtRecipientName);
            this.Controls.Add(this.lblReferanceNumber);
            this.Controls.Add(this.lblSenderName);
            this.Controls.Add(this.lblPINCode);
            this.Controls.Add(this.lblRecipientNationalID);
            this.Controls.Add(this.lblRemittanceStatus);
            this.Controls.Add(this.lblRecipientName);
            this.Controls.Add(this.lblNameofExchangeHouse);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmRemittanceAdmin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Remittance Admin";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmRemittanceAdmin_FormClosing);
            this.Load += new System.EventHandler(this.frmRemittanceAdmin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRemittance)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvRemittance;
        private System.Windows.Forms.Label lblSenderName;
        private System.Windows.Forms.Label lblPINCode;
        private System.Windows.Forms.Label lblRecipientNationalID;
        private System.Windows.Forms.Label lblRecipientName;
        private System.Windows.Forms.Label lblNameofExchangeHouse;
        private System.Windows.Forms.TextBox txtRecipientName;
        private System.Windows.Forms.TextBox txtRecipientNationalID;
        private System.Windows.Forms.TextBox txtSenderName;
        private System.Windows.Forms.TextBox txtPINCode;
        private System.Windows.Forms.ComboBox cmbNameofExchangeHouse;
        private System.Windows.Forms.Label lblReferanceNumber;
        private System.Windows.Forms.TextBox txtReferanceNumber;
        private System.Windows.Forms.Label lblRemittanceStatus;
        private System.Windows.Forms.ComboBox cmbRemittanceStatus;
        private System.Windows.Forms.Label lblItemsFound;
        private System.Windows.Forms.Timer autoRefreshTimer;
        private CustomTitlebar customTitlebar1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMndField5;
        private System.Windows.Forms.Button btnClsoe;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Panel panel1;
        private CustomDataGridViewHeader customDataGridViewHeader;
    }
}