﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MetroFramework.Forms;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.cis;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.fingerprint;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.LocalStorageService;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.UI.forms.ProgressUI;
using MetroFramework.Controls;
using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.UI.forms
{
    public partial class frmUserCreationWithPersonalInfo : CustomForm
    {
        Packet _packet;
        UserType _userType;
        UserCategory _userCategory;
        BankUserType _bankUserType;
        BankUser _bankUser;
        AgentUser _agentUser;
        SubAgentUser _outletUser;
        IndividualInformation _individualInformation;
        AgentUserDto _agentUserDto;
        
        FingerInfo _finger;
        public List<FingerInfo> _fingerList;

        private List<Country> _countries;
        private List<Division> _divisions;
        private List<District> _districts;

        private bool _isImageUpdated = false;

        UserService userService = new UserService();

        //private List<District> _cdistricts;
        //private List<Thana> _cthanas;
        //private List<PostalCode> _cpostalCodes;
        //private List<District> _pdistricts;
        //private List<Thana> _pthanas;
        //private List<PostalCode> _ppostalCodes;
        //private List<Thana> _thanas;
        //private Dictionary<string, Address> _addresses;

        public frmUserCreationWithPersonalInfo(Packet packet, BankUserType bankUserType, UserCategory userCategory, object receivedObject)
        {
            InitializeComponent();

            _packet = packet;

            if (_packet.actionType == FormActionType.New)
            {
                btnResetCredential.Visible = false;
            }

            _userCategory = userCategory;

            switch (_userCategory)
            {
                case UserCategory.SubAgentUser:
                    _outletUser = receivedObject as SubAgentUser;
                    lblUserBranchHeader.Visible = cmbUserBranch.Visible = false;
                    break;
                case UserCategory.AgentUser:
                    _agentUser = receivedObject as AgentUser;
                    lblUserBranchHeader.Visible = cmbUserBranch.Visible = false;
                    break;
                case UserCategory.BranchUser:
                    _bankUser = receivedObject as BankUser;
                    lblUserBranchHeader.Visible = cmbUserBranch.Visible = true;
                    btnFingerPrint.Enabled = false;
                    _bankUserType = bankUserType;
                    break;
            }

            SetupDataLoad();
            SetupFormControls();

            cmbContactCountryCode.Enabled = false;
            cmbPermanentCountryCode.Enabled = false;
            pnlChangeCredential.Visible = false;
        }

        public frmUserCreationWithPersonalInfo(Packet packet, AgentUserDto agentUserDto)
        {
            InitializeComponent();

            _packet = packet;
            _agentUserDto = agentUserDto;
            _userCategory = agentUserDto.userCategory;
            if (_userCategory == UserCategory.SubAgentUser) _outletUser = agentUserDto.subAgentUser;
            else if (_userCategory == UserCategory.AgentUser) _agentUser = agentUserDto.agentUser;
            else if (_userCategory == UserCategory.BranchUser)
            {
                btnFingerPrint.Enabled = false;
                _bankUser = agentUserDto.bankUser;
                _bankUserType = agentUserDto.bankUser.bankUserType;
            }

            SetupDataLoad();
            FillComponentsWithObjectValues();
            SetupFormControls();

            if (_packet.actionType == FormActionType.View) SetFormForViewMode();
            else if (_packet.actionType == FormActionType.Edit) SetFormForEditMode();

            cmbContactCountryCode.Enabled = false;
            cmbPermanentCountryCode.Enabled = false;
        }

        private void SetupFormControls()
        {
            txtMobileNumber.MaxLength = 11;
            if (_packet.actionType != FormActionType.New)
            {
                txtUserName.Enabled = false;
                txtUserName.ReadOnly = true;
            }
        }
        private void SetFormForEditMode()
        {
            btnFingerPrint.Enabled = false;
            txtUserName.ReadOnly = true;

            pnlChangeCredential.Visible = true;
            chkResetFingerPrint.Checked = chkResetPassword.Checked = btnResetCredential.Enabled = false;

            if (_agentUserDto.userCategory == UserCategory.BranchUser)
            {
                if (_agentUserDto.bankUser.userStatus == UserStatus.Inactive)
                {
                    pnlChangeCredential.Visible = false;
                }
                else
                {
                    chkResetFingerPrint.Visible = false;
                }
            }
            else if (_agentUserDto.userCategory == UserCategory.AgentUser)
            {
                if (_agentUserDto.agentUser.userStatus == UserStatus.Inactive)
                {
                    pnlChangeCredential.Visible = false;
                }
                else if (_agentUserDto.agentUser.fingerStatus != FingerStatus.Active)
                {
                    chkResetFingerPrint.Visible = false;
                }
            }
            else if (_agentUserDto.userCategory == UserCategory.SubAgentUser)
            {
                if (_packet.actionType == FormActionType.View || _packet.actionType == FormActionType.Edit)
                {
                    cmbUserBranch.Visible = false;
                }
                if (_agentUserDto.subAgentUser.userStatus == UserStatus.Inactive)
                {
                    pnlChangeCredential.Visible = false;
                }
                else if (_agentUserDto.subAgentUser.fingerStatus != FingerStatus.Active)
                {
                    chkResetFingerPrint.Visible = false;
                }
            }
        }
        private void SetFormForViewMode()
        {
            for (int i = 0; i < Controls.Count; i++)
            {
                if (Controls[i].HasChildren)
                {
                    for (int j = 0; j < Controls[i].Controls.Count; j++)
                    {
                        Controls[i].Controls[j].Enabled = false; ;
                    }
                }
                else Controls[i].Enabled = false;
            }

            btnClose.Enabled = true;
            pnlChangeCredential.Visible = false;

            //btnBrowse.Enabled = btnCapture.Enabled = btnFingerPrint.Enabled = btnSave.Enabled = false;
            //txtUserName.ReadOnly = true;

            // Now this form will never be opened in VIEW mode. So we skipped this coding.
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                UtilityServices.uploadPhoto(ref openFileDialog1, ref pbImageUser);
                lblImgExtension.Text = Path.GetExtension(openFileDialog1.FileName).ToLower().Trim('.');

                _isImageUpdated = true;

                //if (_packet.actionType == FormActionType.Edit) _isImageUpdated = true;
            }
            catch (Exception)
            {
                throw new ApplicationException("Image loading error....");
            }
        }
        private void btnCapture_Click(object sender, EventArgs e)
        {
            try
            {
                frmWebCam webCam = new frmWebCam();
                DialogResult dr = webCam.ShowDialog();

                if (dr == DialogResult.OK)
                {
                    ImageConverter converter = new ImageConverter();
                    Bitmap imgbitmap = (Bitmap)converter.ConvertFrom(webCam.getPhoto());
                    Image img = (Image)converter.ConvertFrom(webCam.getPhoto());
                    img = UtilityServices.getResizedImage(imgbitmap, CommonRules.imageSizeLimit, 100, "");
                    #region Image file length check
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    if (ms.Length > CommonRules.imageSizeLimit)
                    {
                        Message.showWarning("Image file should be in " + CommonRules.imageSizeLimit / 1024 + " KB");
                        return;
                    }
                    #endregion
                    if (img != null)
                    {
                        pbImageUser.Image = img;
                        lblImgExtension.Text = "jpg";
                        _isImageUpdated = true;
                        //if (_packet.actionType == FormActionType.Edit) _isImageUpdated = true;
                    }
                    else Message.showError("Photo not taken.");
                }
            }
            catch (Exception ex)
            {
                Message.showError("Photo not taken.");
            }
        }

        private void SetupDataLoad()
        {
            cmbUserType.DataSource = Enum.GetValues(typeof(UserType));
            cmbUserStatus.DataSource = Enum.GetValues(typeof(UserStatus));

            #region Populate Address
            _countries = LocalCache.GetCountries();

            BindingSource bsCountry = new BindingSource();
            bsCountry.DataSource = _countries;

            _divisions = LocalCache.GetDivisions();

            BindingSource bsDivisionContact = new BindingSource();
            bsDivisionContact.DataSource = _divisions;

            BindingSource bsDivisionPermanent = new BindingSource();
            bsDivisionPermanent.DataSource = _divisions;

            _districts = LocalCache.GetDistricts();


            UtilityServices.fillComboBox(cmbContactDivision, bsDivisionContact, "name", "id");
            UtilityServices.fillComboBox(cmbPermanentDivision, bsDivisionPermanent, "name", "id");
            cmbContactDivision.SelectedIndex = -1;
            cmbPermanentDivision.SelectedIndex = -1;

            UtilityServices.fillComboBox(cmbContactCountryCode, bsCountry, "name", "id");
            UtilityServices.fillComboBox(cmbPermanentCountryCode, bsCountry, "name", "id");
            cmbContactCountryCode.SelectedValue = CommonRules.countryId;
            cmbPermanentCountryCode.SelectedValue = CommonRules.countryId;

            UtilityServices.fillBranches(ref cmbUserBranch);
            cmbUserBranch.SelectedIndex = -1;
            #endregion
        }

        private void FillObjectWithComponentValues()
        {
            try
            {
                if (_agentUserDto == null) _agentUserDto = new AgentUserDto();
                FillIndividualObject();

                _agentUserDto.userCategory = _userCategory;
                switch (_userCategory)
                {
                    case UserCategory.SubAgentUser:
                        FillOutletUserInformation();
                        break;
                    case UserCategory.AgentUser:
                        FillAgentUserInformation();
                        break;
                    case UserCategory.BranchUser:
                        FillBranchUserInformation();
                        break;
                }

                if (_isImageUpdated)
                {
                    _agentUserDto.individualInformation.image = UtilityServices.imageToByteArray(pbImageUser.Image);
                    _agentUserDto.individualInformation.imageMineType = lblImgExtension.Text.Trim();
                }                
                _agentUserDto.individualInformation.fingerInfos = _fingerList;                
            }
            catch (Exception ex)
            {
                throw new Exception("Individual setup failed.\n" + ex.Message);
            }
        }
        private void FillBranchUserInformation()
        {
            if (_agentUserDto.bankUser == null)
            {
                _agentUserDto.bankUser = new BankUser();
            }

            _bankUser.username = txtUserName.Text.Trim();
            _bankUser.userStatus = (UserStatus)cmbUserStatus.SelectedValue;
            _bankUser.userType = (UserType)cmbUserType.SelectedValue;
            _bankUser.bankUserType = _bankUserType;
            _bankUser.userBranch = (long)cmbUserBranch.SelectedValue;

            _agentUserDto.bankUser = _bankUser;
            _agentUserDto.bankUser.statusChangeRemark = txtNarration.Text;
        }
        private void FillAgentUserInformation()
        {
            if (_agentUserDto.agentUser == null)
            {
                _agentUserDto.agentUser = new AgentUser();
            }

            _agentUser.username = txtUserName.Text.Trim();
            _agentUser.userStatus = (UserStatus)cmbUserStatus.SelectedValue;
            _agentUser.userType = (UserType)cmbUserType.SelectedValue;

            _agentUserDto.agentUser = _agentUser;
            _agentUserDto.agentUser.statusChangeRemark = txtNarration.Text;
        }
        private void FillOutletUserInformation()
        {
            if (_agentUserDto.subAgentUser == null)
            {
                _agentUserDto.subAgentUser = new SubAgentUser();
            }

            _outletUser.username = txtUserName.Text.Trim();
            _outletUser.userStatus = (UserStatus)cmbUserStatus.SelectedValue;
            _outletUser.userType = (UserType)cmbUserType.SelectedValue;

            _agentUserDto.subAgentUser = _outletUser;
            _agentUserDto.subAgentUser.statusChangeRemark = txtNarration.Text;
        }

        private void FillComponentsWithObjectValues()
        {
            try
            {
                #region Set User Info
                if (_userCategory == UserCategory.SubAgentUser)
                {
                    txtUserName.Text = _agentUserDto.subAgentUser.username;
                    cmbUserStatus.SelectedItem = _agentUserDto.subAgentUser.userStatus;
                    cmbUserType.SelectedItem = _agentUserDto.subAgentUser.userType;

                    if (_agentUserDto.subAgentUser.userStatus == UserStatus.Inactive)
                    {
                        lblNarration.Visible = txtNarration.Visible = true;
                        txtNarration.Text = _agentUserDto.subAgentUser.statusChangeRemark;
                    }
                    else
                    {
                        lblNarration.Visible = txtNarration.Visible = false;
                        txtNarration.Text = _agentUserDto.subAgentUser.statusChangeRemark;
                    }
                }
                else if (_userCategory == UserCategory.AgentUser)
                {
                    txtUserName.Text = _agentUserDto.agentUser.username;
                    cmbUserStatus.SelectedItem = _agentUserDto.agentUser.userStatus;
                    cmbUserType.SelectedItem = _agentUserDto.agentUser.userType;
                    txtNarration.Text = _agentUserDto.bankUser?.statusChangeRemark;
                }
                else if (_userCategory == UserCategory.BranchUser)
                {
                    _bankUserType = _agentUserDto.bankUser.bankUserType;
                    txtUserName.Text = _agentUserDto.bankUser.username;
                    cmbUserStatus.SelectedItem = _agentUserDto.bankUser.userStatus;
                    cmbUserType.SelectedItem = _agentUserDto.bankUser.userType;
                    txtNarration.Text = _agentUserDto.bankUser.statusChangeRemark;
                    cmbUserBranch.SelectedValue = _agentUserDto.bankUser.userBranch;
                }
                #endregion

                #region Set individual info
                _individualInformation = _agentUserDto.individualInformation;

                txtFirstName.Text = _individualInformation.firstName;
                txtLastName.Text = _individualInformation.lastName;
                txtFathersFirstName.Text = _individualInformation.fatherFirstName;
                txtFathersLastName.Text = _individualInformation.fatherLastName;
                txtMothersFirstName.Text = _individualInformation.motherFirstName;
                txtMothersLastName.Text = _individualInformation.motherLastName;
                txtMobileNumber.Text = _individualInformation.mobileNo;

                //string birthDateSt = (Convert.ToDateTime(_individualInformation.dateOfBirthSt)).ToString("dd-MM-yyyy");
                dtpBirthDate.PresetServerDate = false;
                if (_individualInformation.dateOfBirth != null) dtpBirthDate.Value = UtilityServices.getDateFromLong(_individualInformation.dateOfBirth);

                //_individualInformation.spouseName = txtSpouseName.Text;
                //_individualInformation.fax = txtFax.Text;
                //_individualInformation.email = txtEmail.Text;
                //_individualInformation.documentInfoId = _docId;
                //_individualInformation.gender = (Gender)cmbGender.SelectedItem;
                //_individualInformation.maritalStatus = (MaritalStatus)cmbMaritalStatus.SelectedItem;

                #region Contact Address
                if (_individualInformation.presentAddress != null)
                {
                    if (_individualInformation.presentAddress.division.id > 0)
                    {
                        List<Division> divisionsContact = LocalCache.GetDivisions();
                        BindingSource bsDivisionsContact = new BindingSource();
                        bsDivisionsContact.DataSource = divisionsContact;
                        UtilityServices.fillComboBox(cmbContactDivision, bsDivisionsContact, "name", "id");
                        cmbContactDivision.SelectedIndex = -1;
                        cmbContactDivision.SelectedValue = _individualInformation.presentAddress.division.id;
                    }
                    else return;

                    List<District> DistrictTmpContact = LocalCache.GetDistrictsByDivisionID(_individualInformation.presentAddress.division.id);
                    BindingSource bsDistrictContact = new BindingSource();
                    bsDistrictContact.DataSource = DistrictTmpContact;
                    UtilityServices.fillComboBox(cmbContactDistrict, bsDistrictContact, "title", "id");
                    if (_individualInformation.presentAddress.district.id > 0)
                    {
                        long tmpIdContact = _individualInformation.presentAddress.district.id;
                        List<Thana> ThanaTmpContact = LocalCache.GetThanasByDistrictID(tmpIdContact);
                        BindingSource bsThanaContact = new BindingSource();
                        bsThanaContact.DataSource = ThanaTmpContact;
                        UtilityServices.fillComboBox(cmbContactThana, bsThanaContact, "title", "id");

                        List<PostalCode> PostalCodeTmpContact = LocalCache.GetPostalCodesByDistrictID(tmpIdContact);
                        BindingSource bsPostalCodeTmpContact = new BindingSource();
                        bsPostalCodeTmpContact.DataSource = PostalCodeTmpContact;
                        UtilityServices.fillComboBox(cmbContactPostCode, bsPostalCodeTmpContact, "postalCode", "id");
                    }

                    if (_individualInformation.presentAddress != null)
                    {
                        txtContactAddress1.Text = _individualInformation.presentAddress.addressLineOne;
                        txtContactAddress2.Text = _individualInformation.presentAddress.addressLineTwo;
                    }

                    if (_individualInformation.presentAddress.district != null)
                    {
                        cmbContactDistrict.SelectedValue = _individualInformation.presentAddress.district.id;
                    }
                    if (_individualInformation.presentAddress.thana != null)
                    {
                        cmbContactThana.SelectedValue = _individualInformation.presentAddress.thana.id;
                    }
                    if (_individualInformation.presentAddress.postalCode != null)
                    {
                        cmbContactPostCode.SelectedValue = _individualInformation.presentAddress.postalCode.id;
                    }
                    if (_individualInformation.presentAddress.country != null)
                    {
                        cmbContactCountryCode.SelectedItem = _individualInformation.presentAddress.country;
                    }

                    //_individualInformation.presentAddress.country = _countries[cmbContactCountryCode.SelectedIndex];
                }
                else return;
                #endregion

                #region Permament Address
                if (_individualInformation.permanentAddress == null)
                {
                    _individualInformation.permanentAddress = new Address();
                    cmbPermanentDivision.SelectedIndex = -1;
                }
                else
                {
                    List<Division> divisionsPermanent = LocalCache.GetDivisions();
                    BindingSource bsDivisionsPermanent = new BindingSource();
                    bsDivisionsPermanent.DataSource = divisionsPermanent;
                    UtilityServices.fillComboBox(cmbPermanentDivision, bsDivisionsPermanent, "name", "id");
                    cmbPermanentDivision.SelectedIndex = -1;
                    if (_individualInformation.permanentAddress.division != null)
                    {
                        cmbPermanentDivision.SelectedValue = _individualInformation.permanentAddress.division.id;
                    }
                    else return;

                    List<District> DistrictTmpPermanent = LocalCache.GetDistrictsByDivisionID(_individualInformation.permanentAddress.division.id);
                    BindingSource bsDistrictPermanent = new BindingSource();
                    bsDistrictPermanent.DataSource = DistrictTmpPermanent;
                    UtilityServices.fillComboBox(cmbPermanentDistrict, bsDistrictPermanent, "title", "id");

                    long tmpIdPermanent = _individualInformation.permanentAddress.district.id;
                    List<Thana> ThanaTmpPermanent = LocalCache.GetThanasByDistrictID(tmpIdPermanent);
                    BindingSource bsThanaPermanent = new BindingSource();
                    bsThanaPermanent.DataSource = ThanaTmpPermanent;
                    UtilityServices.fillComboBox(cmbPermanentThana, bsThanaPermanent, "title", "id");

                    List<PostalCode> PostalCodeTmpPermanent = LocalCache.GetPostalCodesByDistrictID(tmpIdPermanent);
                    BindingSource bsPostalCodeTmpPermanent = new BindingSource();
                    bsPostalCodeTmpPermanent.DataSource = PostalCodeTmpPermanent;
                    UtilityServices.fillComboBox(cmbPermanentPostalCode, bsPostalCodeTmpPermanent, "postalCode", "id");

                    txtPermanentAddress1.Text = _individualInformation.permanentAddress.addressLineOne;
                    txtPermanentAddress2.Text = _individualInformation.permanentAddress.addressLineTwo;
                    if (_individualInformation.permanentAddress.district != null)
                    {
                        cmbPermanentDistrict.SelectedValue = _individualInformation.permanentAddress.district.id;
                    }
                    if (_individualInformation.permanentAddress.thana != null)
                    {
                        cmbPermanentThana.SelectedValue = _individualInformation.permanentAddress.thana.id;
                    }
                    if (_individualInformation.permanentAddress.postalCode != null)
                    {
                        cmbPermanentPostalCode.SelectedValue = _individualInformation.permanentAddress.postalCode.id;
                    }
                    if (_individualInformation.permanentAddress.country != null)
                    {
                        cmbPermanentCountryCode.SelectedItem = _individualInformation.permanentAddress.country;
                    }
                }
                #endregion
                #endregion

                if (_individualInformation.image != null)
                {
                    if (_individualInformation.image.Length != 0)
                    {
                        pbImageUser.Image = UtilityServices.byteArrayToImage(_individualInformation.image);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Individual data load failed.\n" + ex.Message);
            }
        }

        private void cmbContactDivision_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!cb.Focused)
            {
                return;
            }
            
            List<District> DistrictTmpContact = LocalCache.GetDistrictsByDivisionID(((Division)cmbContactDivision.SelectedItem).id);
            BindingSource bsDistrictContact = new BindingSource();
            bsDistrictContact.DataSource = DistrictTmpContact;
            UtilityServices.fillComboBox(cmbContactDistrict, bsDistrictContact, "title", "id");

            cmbContactDistrict.SelectedIndex = -1;
            cmbContactThana.SelectedIndex = -1;
            cmbContactPostCode.SelectedIndex = -1;
        }
        private void cmbContactDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!cb.Focused) return;

            long tmpIdContact = ((District)cmbContactDistrict.SelectedItem).id;
            List<Thana> ThanaTmpContact = LocalCache.GetThanasByDistrictID(tmpIdContact);
            BindingSource bsThanaContact = new BindingSource();
            bsThanaContact.DataSource = ThanaTmpContact;
            UtilityServices.fillComboBox(cmbContactThana, bsThanaContact, "title", "id");

            List<PostalCode> PostalCodeTmpContact = LocalCache.GetPostalCodesByDistrictID(tmpIdContact);
            BindingSource bsPostalCodeTmpContact = new BindingSource();
            bsPostalCodeTmpContact.DataSource = PostalCodeTmpContact;
            UtilityServices.fillComboBox(cmbContactPostCode, bsPostalCodeTmpContact, "postalCode", "id");

            cmbContactThana.SelectedIndex = -1;
            cmbContactPostCode.SelectedIndex = -1;
        }

        private void cmbPermanentDivision_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!cb.Focused)
            {
                return;
            }

            List<District> DistrictTmpPermanent = LocalCache.GetDistrictsByDivisionID(((Division)cmbPermanentDivision.SelectedItem).id);
            BindingSource bsDistrictPermanent = new BindingSource();
            bsDistrictPermanent.DataSource = DistrictTmpPermanent;
            UtilityServices.fillComboBox(cmbPermanentDistrict, bsDistrictPermanent, "title", "id");

            cmbPermanentDistrict.SelectedIndex = -1;
            cmbPermanentThana.SelectedIndex = -1;
            cmbPermanentPostalCode.SelectedIndex = -1;
        }
        private void cmbPermanentDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!cb.Focused) return;

            long tmpIdPermanent = ((District)cmbPermanentDistrict.SelectedItem).id;
            List<Thana> ThanaTmpPermanent = LocalCache.GetThanasByDistrictID(tmpIdPermanent);
            BindingSource bsThanaPermanent = new BindingSource();
            bsThanaPermanent.DataSource = ThanaTmpPermanent;
            UtilityServices.fillComboBox(cmbPermanentThana, bsThanaPermanent, "title", "id");

            List<PostalCode> PostalCodeTmpPermanent = LocalCache.GetPostalCodesByDistrictID(tmpIdPermanent);
            BindingSource bsPostalCodeTmpPermanent = new BindingSource();
            bsPostalCodeTmpPermanent.DataSource = PostalCodeTmpPermanent;
            UtilityServices.fillComboBox(cmbPermanentPostalCode, bsPostalCodeTmpPermanent, "postalCode", "id");

            cmbPermanentThana.SelectedIndex = -1;
            cmbPermanentPostalCode.SelectedIndex = -1;
        }

        private void chkUseAsPermanent_CheckedChanged(object sender, EventArgs e)
        {
            if (chkUseAsPermanent.Checked)
            {
                UtilityServices.copyAddress(txtContactAddress1, txtContactAddress2, cmbContactPostCode, cmbContactThana, cmbContactDistrict,
                                            cmbContactDivision, cmbContactCountryCode,
                                            ref txtPermanentAddress1, ref txtPermanentAddress2, ref cmbPermanentPostalCode, ref cmbPermanentThana,
                                            ref cmbPermanentDistrict, ref cmbPermanentDivision, ref cmbPermanentCountryCode
                                           );
                txtPermanentAddress1.Focus();
            }
        }

        private void FillIndividualObject()
        {
            try
            {
                IndividualInformation individualInformationTmp = new IndividualInformation();
                if (_individualInformation != null) individualInformationTmp = _individualInformation;
                else
                {
                    _individualInformation = new IndividualInformation();
                    _individualInformation.imageId = 0;
                    _individualInformation.personFingerInfoId = 0;
                }

                _individualInformation.firstName = txtFirstName.Text;
                _individualInformation.lastName = txtLastName.Text;
                _individualInformation.fatherFirstName = txtFathersFirstName.Text;
                _individualInformation.fatherLastName = txtFathersLastName.Text;
                _individualInformation.motherFirstName = txtMothersFirstName.Text;
                _individualInformation.motherLastName = txtMothersLastName.Text;
                _individualInformation.mobileNo = txtMobileNumber.Text;

                //string dateString = "";
                //string[] dateArray = dtpBirthDate.Date.Split('/').ToArray();
                //dateString = dateArray[1] + "-" + dateArray[0] + "-" + dateArray[2];

                _individualInformation.dateOfBirth = UtilityServices.GetLongDate(dtpBirthDate.Value);

                //_individualInformation.occupation = new Occupation();
                //_individualInformation.spouseName = txtSpouseName.Text;
                //_individualInformation.fax = txtFax.Text;
                //_individualInformation.email = txtEmail.Text;
                //_individualInformation.documentInfoId = _docId;
                //_individualInformation.gender = (Gender)cmbGender.SelectedItem;
                //_individualInformation.maritalStatus = (MaritalStatus)cmbMaritalStatus.SelectedItem;

                #region Contact Address
                if (_individualInformation.presentAddress == null) _individualInformation.presentAddress = new Address();
                if (individualInformationTmp.presentAddress != null) _individualInformation.presentAddress.id = individualInformationTmp.presentAddress.id;

                _individualInformation.presentAddress.addressLineOne = txtContactAddress1.Text;
                _individualInformation.presentAddress.addressLineTwo = txtContactAddress2.Text;

                _individualInformation.presentAddress.division = (Division)cmbContactDivision.SelectedItem;
                _individualInformation.presentAddress.district = (District)cmbContactDistrict.SelectedItem;
                _individualInformation.presentAddress.thana = (Thana)cmbContactThana.SelectedItem;
                _individualInformation.presentAddress.postalCode = (PostalCode)cmbContactPostCode.SelectedItem;
                _individualInformation.presentAddress.country = (Country)cmbContactCountryCode.SelectedItem;
                #endregion

                #region Permament Address
                if (_individualInformation.permanentAddress == null) _individualInformation.permanentAddress = new Address();
                if (individualInformationTmp.permanentAddress != null) _individualInformation.permanentAddress.id = individualInformationTmp.permanentAddress.id;

                _individualInformation.permanentAddress.addressLineOne = txtPermanentAddress1.Text;
                _individualInformation.permanentAddress.addressLineTwo = txtPermanentAddress2.Text;

                _individualInformation.permanentAddress.division = (Division)cmbPermanentDivision.SelectedItem;
                _individualInformation.permanentAddress.district = (District)cmbPermanentDistrict.SelectedItem;
                _individualInformation.permanentAddress.thana = (Thana)cmbPermanentThana.SelectedItem;
                _individualInformation.permanentAddress.postalCode = (PostalCode)cmbPermanentPostalCode.SelectedItem;
                _individualInformation.permanentAddress.country = (Country)cmbPermanentCountryCode.SelectedItem;
                #endregion

                _individualInformation.birthPalce = _individualInformation.permanentAddress.district;
                _individualInformation.birthCountry = _individualInformation.permanentAddress.country;

                _agentUserDto.individualInformation = _individualInformation;
            }
            catch (Exception ex)
            {
                throw new Exception("Individual setup failed.\n" + ex.Message);
            }
        }

        private ValidationCheck ValidationCheck(IndividualInformation individual)
        {
            ValidationCheck validCheck = new ValidationCheck();
            validCheck.validInput = true;
            validCheck.message = "Following input(s) require attention !\n\n";
            DateTime birthDate = new DateTime();

            //try
            //{ birthDate = DateTime.ParseExact(individual.dateOfBirthSt, "MM-dd-yyyy", CultureInfo.InvariantCulture); }
            //catch
            //{
            //    validCheck.message += "Enter the date in correct format.\n";
            //    validCheck.validInput = false;
            //}

            try
            { birthDate = UtilityServices.getDateFromLong(individual.dateOfBirth); }
            catch
            {
                validCheck.message += "Enter the date in correct format.\n";
                validCheck.validInput = false;
            }

            if (individual.firstName == "")
            {
                validCheck.message += "First name should not be empty.\n";
                validCheck.validInput = false;
            }
            if (individual.lastName == "")
            {
                validCheck.message += "Last name should not be empty.\n";
                validCheck.validInput = false;
            }

            if (individual.fatherFirstName == "")
            {
                validCheck.message += "Father's first name should not be empty.\n";
                validCheck.validInput = false;
            }
            if (individual.fatherLastName == "")
            {
                validCheck.message += "Father's last name should not be empty.\n";
                validCheck.validInput = false;
            }

            if (individual.motherFirstName == "")
            {
                validCheck.message += "Mother's first name should not be empty.\n";
                validCheck.validInput = false;
            }
            if (individual.motherLastName == "")
            {
                validCheck.message += "Mother's last name should not be empty.\n";
                validCheck.validInput = false;
            }

            if (birthDate > SessionInfo.currentDate)
            {
                validCheck.message += "Future birth date is not allowed.\n";
                validCheck.validInput = false;
            }

            if (individual.birthPalce == null)
            {
                validCheck.message += "Birth place not found.\n";
                validCheck.validInput = false;
            }
            else if (individual.birthPalce.id == 0)
            {
                Message.showWarning("Birth place not found.");
                validCheck.validInput = false;
            }

            if (individual.birthCountry == null)
            {
                validCheck.message += "Birth country not found.\n";
                validCheck.validInput = false;
            }
            else if (individual.birthCountry.id == 0)
            {
                validCheck.message += "Birth country not found.\n";
                validCheck.validInput = false;
            }

            if (individual.mobileNo == "")
            {
                validCheck.message += "Mobile number not found.\n";
                validCheck.validInput = false;
            }

            if (_individualInformation.presentAddress == null)
            {
                validCheck.message += "Present address not found.\n";
                validCheck.validInput = false;
            }
            else
            {
                if (txtContactAddress1.Text.Trim() == "" && txtContactAddress2.Text.Trim() == "")
                {
                    validCheck.message += "Present address not found.\n";
                    validCheck.validInput = false;
                }
            }

            if (_individualInformation.permanentAddress == null)
            {
                validCheck.message += "Permanent Address address not found.\n";
                validCheck.validInput = false;
            }
            else
            {
                if (txtPermanentAddress1.Text.Trim() == "" && txtPermanentAddress2.Text.Trim() == "")
                {
                    validCheck.message += "Permanent Address address not found.\n";
                    validCheck.validInput = false;
                }
            }

            return validCheck;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool validForm = CustomControlValidation.IsAllValid(true, true, txtUserName, txtFirstName, txtLastName,
                    txtFathersFirstName, txtFathersLastName,
                    txtMothersFirstName, txtMothersLastName, txtMobileNumber, txtContactAddress1, txtPermanentAddress1,
                    cmbContactCountryCode, cmbContactDistrict, cmbContactDivision, cmbContactPostCode, cmbContactThana,
                    cmbPermanentCountryCode, cmbPermanentDistrict, cmbPermanentDivision, cmbPermanentPostalCode,
                    cmbPermanentThana, txtNarration);
                if (!validForm) return;

                FillObjectWithComponentValues();

                #region Validation

                bool isError = false;
                string errMessage = "";

                if (_agentUserDto.individualInformation.image == null)
                {
                    errMessage += "User photo is required !\n";
                    isError = true;
                }
                else if (_agentUserDto.individualInformation.image.Length == 0)
                {
                    errMessage += "User photo is required !\n";
                    isError = true;
                }

                if (isError)
                {
                    Message.showError(errMessage);
                    return;
                }

                #endregion

                string reply = "";
                switch (_packet.actionType)
                {
                    case FormActionType.New:
                        if (_userCategory != UserCategory.BranchUser)
                        {
                            if (_agentUserDto.individualInformation.fingerInfos != null && _agentUserDto.individualInformation.fingerInfos.Count > 0)
                            {
                                reply = Message.showConfirmation("Are you sure to save this user ?");
                            }
                            else
                            {
                                _agentUserDto.individualInformation.fingerInfos = null;
                                reply = Message.showConfirmation("Are you sure to save user without fingerprint ?");
                            }
                        }
                        else
                        {
                            reply = Message.showConfirmation("Are you sure to save this user ?");
                        }
                        break;
                    case FormActionType.Edit:
                        reply = Message.showConfirmation("Are you sure to update this user ?");
                        break;
                }

                if (reply == "yes")
                {
                    ValidationCheck validCheck = ValidationCheck(_agentUserDto.individualInformation);
                    if (validCheck.validInput == false)
                    {
                        Message.showWarning(validCheck.message);
                        return;
                    }
                    else
                    {
                        ServiceResult serviceResult = ServiceResult.CreateServiceResult();
                        ProgressUIManager.ShowProgress(this);

                        if (_packet.actionType == FormActionType.New)
                        {
                            serviceResult = userService.SaveUser(_agentUserDto);
                        }
                        else if (_packet.actionType == FormActionType.Edit)
                        {
                            if (!_isImageUpdated)
                            {
                                _agentUserDto.individualInformation.image = null;
                                _agentUserDto.individualInformation.imageMineType = "";
                            }
                            serviceResult = userService.UpdateUser(_agentUserDto);
                        }

                        ProgressUIManager.CloseProgress();
                        if (serviceResult.Success)
                        {
                            Message.showInformation(serviceResult.Message);
                            this.Close();
                        }
                        else
                        {
                            Message.showError("User is not saved. \n\n" + serviceResult.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ProgressUIManager.CloseProgress();
                Message.showError("User is not saved. \n\n" + ex.Message);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnFingerPrint_Click(object sender, EventArgs e)
        {
            frmFingerprintCapture objFrmFinger = new frmFingerprintCapture(txtUserName.Text.Trim());
            Color defaultButtonColor = Color.FromArgb(0, 122, 170);

            DialogResult dr = objFrmFinger.ShowDialog();
            if (dr == DialogResult.OK)
            {
                _fingerList = objFrmFinger.FingerInfos;

                if (_fingerList != null)
                {
                    if (_fingerList.Count > 0)
                    {
                        btnFingerPrint.BackColor = Color.LightGreen;

                        //string fingerName;
                        //_personFingerTemplate.fingerInfos = new List<FingerInfo>();
                        //foreach (FingerInfo finger in _fingerList)
                        //{
                        //    _finger = new FingerInfo();
                        //    _finger.id = 0;
                        //    _finger.fingerData = finger.fingerData;

                        //    //fingerName = finger.hand + finger.finger;
                        //   // FingerIndex fingIndex = (FingerIndex)Enum.Parse(typeof(FingerIndex), fingerName);
                        //    _finger.fingerIndex = finger.fingerIndex;

                        //    _fingerList.Add(_finger);
                        //}

                    }
                    else btnFingerPrint.BackColor = defaultButtonColor;
                }
                else btnFingerPrint.BackColor = defaultButtonColor;
            }
        }

        private void txtMobileNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) && (Keys)e.KeyChar != Keys.Back && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            base.OnKeyPress(e);
        }

        private void btnResetCredential_Click(object sender, EventArgs e)
        {
            if (_packet.actionType == FormActionType.Edit)
            {
                try
                {
                    ResetPasswordDto resetPassDto = new ResetPasswordDto();
                    if (chkResetFingerPrint.Checked) resetPassDto.fingerReset = true;
                    if (chkResetPassword.Checked) resetPassDto.passwordReset = true;

                    string reply = Message.showConfirmation("Are you sure to reset credential(s) for this user ?");
                    if (reply == "yes")
                    {
                        ServiceResult serviceResult = ServiceResult.CreateServiceResult();
                        resetPassDto.userCategory = _userCategory;
                        if (_userCategory == UserCategory.BranchUser) resetPassDto.userId = _agentUserDto.bankUser.id;
                        else if (_userCategory == UserCategory.AgentUser) resetPassDto.userId = _agentUserDto.agentUser.id;
                        else if (_userCategory == UserCategory.SubAgentUser) resetPassDto.userId = _agentUserDto.subAgentUser.id;

                        serviceResult = userService.ResetUserCredential(resetPassDto);
                        ProgressUIManager.CloseProgress();
                        if (serviceResult.Success) Message.showInformation(serviceResult.Message);
                        else Message.showError("Credential reset error !\n" + serviceResult.Message);
                    }
                }
                catch (Exception exp)
                {
                    ProgressUIManager.CloseProgress();
                    Message.showInformation("Password could not be set. \nPlease contact with the bank.");
                }
            }
        }

        public void control_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }

        private void frmUserCreationWithPersonalInfo_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void chkResetCredential_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkResetFingerPrint.Checked && !chkResetPassword.Checked) btnResetCredential.Enabled = false;
            else btnResetCredential.Enabled = true;
        }

        private void cmbUserStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((UserStatus)cmbUserStatus.SelectedItem == UserStatus.Inactive)
            {
                lblNarration.Visible = txtNarration.Visible = true;
                txtNarration.Text = "";
                txtNarration.InputScopeAllowEmpty = false;
                txtNarration.ShowMandatoryMark = true;
            }
            else
            {
                lblNarration.Visible = txtNarration.Visible = false;
                txtNarration.InputScopeAllowEmpty = true;
                txtNarration.ShowMandatoryMark = false;
            }
        }
    }

    internal class ValidationCheck
    {
        public bool validInput { get; set; }
        public string message { get; set; }
    }
}
