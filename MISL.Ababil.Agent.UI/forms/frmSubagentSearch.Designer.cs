﻿using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.UI.forms
{
    partial class frmSubagentSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dgvSubAgent = new System.Windows.Forms.DataGridView();
            this.gridColOutletCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridColOutletName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridColMobileNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridColCreationDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridColOutletStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridColSubagentId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridColViewButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.dataGridViewButtonColumn1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.cmbAgentName = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbOutletStatus = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.txtMobileNo = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtOutletCode = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtOutletName = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.mandatoryMark1 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSubAgent)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(87, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Agent Name :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(83, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Outlet Name  :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(467, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Mobile No. :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(463, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 15);
            this.label4.TabIndex = 2;
            this.label4.Text = "Outlet Code :";
            // 
            // dgvSubAgent
            // 
            this.dgvSubAgent.AllowUserToAddRows = false;
            this.dgvSubAgent.AllowUserToDeleteRows = false;
            this.dgvSubAgent.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvSubAgent.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Turquoise;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSubAgent.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSubAgent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSubAgent.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gridColOutletCode,
            this.gridColOutletName,
            this.gridColMobileNo,
            this.gridColCreationDate,
            this.gridColOutletStatus,
            this.gridColSubagentId,
            this.gridColViewButton});
            this.dgvSubAgent.GridColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgvSubAgent.Location = new System.Drawing.Point(22, 143);
            this.dgvSubAgent.Name = "dgvSubAgent";
            this.dgvSubAgent.RowHeadersVisible = false;
            this.dgvSubAgent.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSubAgent.Size = new System.Drawing.Size(804, 396);
            this.dgvSubAgent.TabIndex = 16;
            this.dgvSubAgent.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSubAgent_CellContentClick);
            this.dgvSubAgent.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSubAgent_CellContentDoubleClick);
            // 
            // gridColOutletCode
            // 
            this.gridColOutletCode.HeaderText = "Code";
            this.gridColOutletCode.Name = "gridColOutletCode";
            this.gridColOutletCode.ReadOnly = true;
            this.gridColOutletCode.Width = 117;
            // 
            // gridColOutletName
            // 
            this.gridColOutletName.HeaderText = "Outlet Name";
            this.gridColOutletName.Name = "gridColOutletName";
            this.gridColOutletName.ReadOnly = true;
            this.gridColOutletName.Width = 175;
            // 
            // gridColMobileNo
            // 
            this.gridColMobileNo.HeaderText = "Contact No.";
            this.gridColMobileNo.Name = "gridColMobileNo";
            this.gridColMobileNo.ReadOnly = true;
            this.gridColMobileNo.Width = 150;
            // 
            // gridColCreationDate
            // 
            this.gridColCreationDate.HeaderText = "Created On";
            this.gridColCreationDate.Name = "gridColCreationDate";
            this.gridColCreationDate.ReadOnly = true;
            this.gridColCreationDate.Width = 130;
            // 
            // gridColOutletStatus
            // 
            this.gridColOutletStatus.HeaderText = "Status";
            this.gridColOutletStatus.Name = "gridColOutletStatus";
            this.gridColOutletStatus.ReadOnly = true;
            this.gridColOutletStatus.Width = 125;
            // 
            // gridColSubagentId
            // 
            this.gridColSubagentId.HeaderText = "Subagent Id";
            this.gridColSubagentId.MinimumWidth = 2;
            this.gridColSubagentId.Name = "gridColSubagentId";
            this.gridColSubagentId.ReadOnly = true;
            this.gridColSubagentId.Visible = false;
            this.gridColSubagentId.Width = 2;
            // 
            // gridColViewButton
            // 
            this.gridColViewButton.HeaderText = "View";
            this.gridColViewButton.Name = "gridColViewButton";
            this.gridColViewButton.Text = "View";
            this.gridColViewButton.UseColumnTextForButtonValue = true;
            this.gridColViewButton.Width = 98;
            // 
            // btnReset
            // 
            this.btnReset.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnReset.FlatAppearance.BorderSize = 0;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btnReset.ForeColor = System.Drawing.Color.White;
            this.btnReset.Location = new System.Drawing.Point(673, 108);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(113, 24);
            this.btnReset.TabIndex = 15;
            this.btnReset.Text = "&Reset";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btnSearch.ForeColor = System.Drawing.Color.White;
            this.btnSearch.Location = new System.Drawing.Point(546, 108);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(113, 24);
            this.btnSearch.TabIndex = 14;
            this.btnSearch.Text = "&Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(713, 545);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(113, 27);
            this.btnClose.TabIndex = 17;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_1);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(121, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 15);
            this.label5.TabIndex = 12;
            this.label5.Text = "Status :";
            // 
            // dataGridViewButtonColumn1
            // 
            this.dataGridViewButtonColumn1.HeaderText = "View";
            this.dataGridViewButtonColumn1.Name = "dataGridViewButtonColumn1";
            this.dataGridViewButtonColumn1.Text = "View";
            this.dataGridViewButtonColumn1.UseColumnTextForButtonValue = true;
            this.dataGridViewButtonColumn1.Width = 98;
            // 
            // cmbAgentName
            // 
            this.cmbAgentName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAgentName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbAgentName.FormattingEnabled = true;
            this.cmbAgentName.InputScopeAllowEmpty = true;
            this.cmbAgentName.IsValid = null;
            this.cmbAgentName.ItemHeight = 13;
            this.cmbAgentName.Location = new System.Drawing.Point(175, 46);
            this.cmbAgentName.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.cmbAgentName.Name = "cmbAgentName";
            this.cmbAgentName.PromptText = "(Select)";
            this.cmbAgentName.ReadOnly = false;
            this.cmbAgentName.ShowMandatoryMark = false;
            this.cmbAgentName.Size = new System.Drawing.Size(240, 21);
            this.cmbAgentName.TabIndex = 1;
            this.cmbAgentName.ValidationErrorMessage = "Validation Error!";
            // 
            // cmbOutletStatus
            // 
            this.cmbOutletStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOutletStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbOutletStatus.FormattingEnabled = true;
            this.cmbOutletStatus.InputScopeAllowEmpty = true;
            this.cmbOutletStatus.IsValid = null;
            this.cmbOutletStatus.ItemHeight = 13;
            this.cmbOutletStatus.Location = new System.Drawing.Point(175, 107);
            this.cmbOutletStatus.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.cmbOutletStatus.Name = "cmbOutletStatus";
            this.cmbOutletStatus.PromptText = "(Select)";
            this.cmbOutletStatus.ReadOnly = false;
            this.cmbOutletStatus.ShowMandatoryMark = false;
            this.cmbOutletStatus.Size = new System.Drawing.Size(240, 21);
            this.cmbOutletStatus.TabIndex = 13;
            this.cmbOutletStatus.ValidationErrorMessage = "Validation Error!";
            // 
            // txtMobileNo
            // 
            this.txtMobileNo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtMobileNo.BackColor = System.Drawing.SystemColors.Window;
            this.txtMobileNo.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.MobileNumber;
            this.txtMobileNo.InputScopeAllowEmpty = true;
            this.txtMobileNo.InputScopeCustomString = null;
            this.txtMobileNo.IsValid = null;
            this.txtMobileNo.Location = new System.Drawing.Point(546, 78);
            this.txtMobileNo.MaxLength = 11;
            this.txtMobileNo.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtMobileNo.Name = "txtMobileNo";
            this.txtMobileNo.PromptText = "(Type here...)";
            this.txtMobileNo.ReadOnly = false;
            this.txtMobileNo.ShowMandatoryMark = false;
            this.txtMobileNo.Size = new System.Drawing.Size(240, 21);
            this.txtMobileNo.TabIndex = 7;
            this.txtMobileNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtMobileNo.UpperCaseOnly = false;
            this.txtMobileNo.ValidationErrorMessage = "Validation Error!";
            // 
            // txtOutletCode
            // 
            this.txtOutletCode.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtOutletCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtOutletCode.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtOutletCode.InputScopeAllowEmpty = false;
            this.txtOutletCode.InputScopeCustomString = null;
            this.txtOutletCode.IsValid = null;
            this.txtOutletCode.Location = new System.Drawing.Point(546, 48);
            this.txtOutletCode.MaxLength = 32767;
            this.txtOutletCode.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtOutletCode.Name = "txtOutletCode";
            this.txtOutletCode.PromptText = "(Type here...)";
            this.txtOutletCode.ReadOnly = false;
            this.txtOutletCode.ShowMandatoryMark = false;
            this.txtOutletCode.Size = new System.Drawing.Size(240, 21);
            this.txtOutletCode.TabIndex = 3;
            this.txtOutletCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtOutletCode.UpperCaseOnly = false;
            this.txtOutletCode.ValidationErrorMessage = "Validation Error!";
            // 
            // txtOutletName
            // 
            this.txtOutletName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtOutletName.BackColor = System.Drawing.SystemColors.Window;
            this.txtOutletName.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtOutletName.InputScopeAllowEmpty = true;
            this.txtOutletName.InputScopeCustomString = null;
            this.txtOutletName.IsValid = null;
            this.txtOutletName.Location = new System.Drawing.Point(175, 76);
            this.txtOutletName.MaxLength = 32767;
            this.txtOutletName.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtOutletName.Name = "txtOutletName";
            this.txtOutletName.PromptText = "(Type here...)";
            this.txtOutletName.ReadOnly = false;
            this.txtOutletName.ShowMandatoryMark = false;
            this.txtOutletName.Size = new System.Drawing.Size(240, 21);
            this.txtOutletName.TabIndex = 5;
            this.txtOutletName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtOutletName.UpperCaseOnly = false;
            this.txtOutletName.ValidationErrorMessage = "Validation Error!";
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(849, 26);
            this.customTitlebar1.TabIndex = 18;
            // 
            // mandatoryMark1
            // 
            this.mandatoryMark1.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark1.Location = new System.Drawing.Point(421, 47);
            this.mandatoryMark1.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.Name = "mandatoryMark1";
            this.mandatoryMark1.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.TabIndex = 29;
            // 
            // frmSubagentSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 586);
            this.Controls.Add(this.cmbOutletStatus);
            this.Controls.Add(this.cmbAgentName);
            this.Controls.Add(this.mandatoryMark1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.txtMobileNo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtOutletCode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtOutletName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.dgvSubAgent);
            this.DisplayHeader = false;
            this.Name = "frmSubagentSearch";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.Resizable = false;
            this.Text = "Outlet Searching";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSubagentSearch_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSubAgent)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvSubAgent;
        private CustomTitlebar customTitlebar1;
        private CustomTextBox txtOutletName;
        private CustomTextBox txtOutletCode;
        private CustomTextBox txtMobileNo;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColOutletCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColOutletName;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColMobileNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColCreationDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColOutletStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn gridColSubagentId;
        private System.Windows.Forms.DataGridViewButtonColumn gridColViewButton;
        private CustomComboBoxDropDownList cmbOutletStatus;
        private CustomComboBoxDropDownList cmbAgentName;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn1;
        private CustomControls.MandatoryMark mandatoryMark1;
    }
}