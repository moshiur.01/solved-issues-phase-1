﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.consumer;
using MISL.Ababil.Agent.Infrastructure.Models.models.transaction;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.Infrastructure.Validation;
using MISL.Ababil.Agent.Report.Reports;
using MISL.Ababil.Agent.Report.DataSets;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using CrystalDecisions.CrystalReports.Engine;
using MetroFramework.Forms;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.UI.forms.ProgressUI;

namespace MISL.Ababil.Agent.UI.forms
{
    public partial class frmMiniStatementForTermAccount : CustomForm
    {
        public GUI _gui = new GUI();
        TransactionService _txnService = new TransactionService();
        ConsumerServices _consumerService = new ConsumerServices();
        ConsumerInformationDto _consumerInformationDto = new ConsumerInformationDto();
        List<TransactionDetail> _transactionDetails = new List<TransactionDetail>();
        string _captureFor;
        int _captureIndexNo;
        int _noOfCapturefinger = 0;
        string _capturefingerData;
        string _subagentFingerData;

        public frmMiniStatementForTermAccount()
        {
            InitializeComponent();
            DataGridViewButtonColumn buttonColumn = new DataGridViewButtonColumn();
            buttonColumn.Text = "Capture";
            buttonColumn.UseColumnTextForButtonValue = true;
            lblConsumerTitle.Text = string.Empty;
            ConfigureValidation();
            PrintReport();
            ConfigUIEnhancement();
        }

        public void ConfigUIEnhancement()
        {
            _gui = new GUI(this);
            _gui.Config(ref txtConsumerAccount, ValidCheck.VALIDATIONTYPES.TEXTBOX_EMPTY, null);
        }

        private void PrintReport()
        {
            if (miniStatementGrid.Rows.Count == 0) btnPrint.Enabled = false;
            if (miniStatementGrid.Rows.Count > 0) btnPrint.Enabled = true;
        }

        private void ConfigureValidation()
        {
            ValidationManager.ConfigureValidation(this, txtConsumerAccount, "Account", (long)ValidationType.NonWhitespaceNonEmptyText, true);
        }

        private bool validationCheck()
        {
            return ValidationManager.ValidateForm(this);
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(txtConsumerAccount.Text))
            {
                MsgBox.showWarning("Please insert an account no.");
            }
            else
            {
                if (validationCheck())
                {
                    MinistatementRequest ministatementRequest = FillMinistatementRequestData();
                    try
                    {
                        ProgressUIManager.ShowProgress(this);
                        _transactionDetails = _txnService.MiniStatemnetForTermAccount(ministatementRequest, txtConsumerAccount.Text);
                        ProgressUIManager.CloseProgress();

                        miniStatementGrid.DataSource = null;
                        miniStatementGrid.Columns.Clear();
                        miniStatementGrid.Columns.Add(new DataGridViewTextBoxColumn() { HeaderText = "Serial", DefaultCellStyle = new DataGridViewCellStyle() { Alignment = DataGridViewContentAlignment.TopRight } });

                        if (_transactionDetails != null)
                        {
                            miniStatementGrid.DataSource = _transactionDetails.Select(o => new accountStatementGrid(o) { txnDate = o.txnDate, txnNarration = o.txnNarration, trAmount = o.trAmount, txnNumber = o.txnNumber, txnPostBalance = o.txnPostBalance, instrumentNumber = o.instrumentNumber }).ToList();
                        }
                        for (int i = 0; i < miniStatementGrid.Rows.Count; i++)
                        {
                            miniStatementGrid.Rows[i].Cells[0].Value = (i + 1).ToString();
                        }
                        PrintReport();
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex.Message);
                    }
                }

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            clearAllInputData();
            PrintReport();
        }

        private void clearAllInputData()
        {
            txtConsumerAccount.Text = string.Empty;
            lblCustomerTitle.Text = string.Empty;
            lblConsumerTitle.Text = string.Empty;
            miniStatementGrid.DataSource = null;
            miniStatementGrid.Rows.Clear();
            _noOfCapturefinger = 0;
            _miniStatement.Clear();
            _transactionDetails.Clear();
            miniStatementGrid.DataSource = null;
            txtConsumerAccount.Focus();
        }

        private void txtConsumerAccount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) && (Keys)e.KeyChar != Keys.Back && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            base.OnKeyPress(e);
        }

        private MinistatementRequest FillMinistatementRequestData()
        {
            MinistatementRequest request = new MinistatementRequest();
            request.customerAccount = txtConsumerAccount.Text;
            request.transactionDate = UtilityServices.GetLongDate(SessionInfo.currentDate);
            return request;
        }

        private void loadConsumerInformation()
        {
            if (txtConsumerAccount.Text != "")
            {
                try
                {

                    //try
                    //{
                    //    _consumerInformationDto = _consumerService.getConsumerInformationDtoByAcc(txtConsumerAccount.Text);
                    //}
                    //catch (Exception ex)
                    //{
                    //    MessageBox.Show(ex.Message);
                    //}
                    //
                    //lblConsumerTitle.Text = _consumerInformationDto.consumerTitle;

                    TermAccInforamationDto termAccInforamationDto =
                    (new TermAccountInqueryService()).GetTermAccountInformationByAccountNo(txtConsumerAccount.Text);
                    lblConsumerTitle.Text = termAccInforamationDto.accountTitle;
                }
                catch (Exception ex)
                {
                    lblConsumerTitle.Text = "";
                    MsgBox.ShowError(ex.Message);
                }

            }
        }

        private void GetStatement()
        {
            MinistatementRequest ministatementRequest = FillMinistatementRequestData();

            try
            {
                _transactionDetails = _txnService.Ministatemnet(ministatementRequest, txtConsumerAccount.Text);
                if (_transactionDetails != null)
                {

                    miniStatementGrid.DataSource = _transactionDetails.Select(o => new accountStatementGrid(o) { txnDate = o.txnDate, txnNarration = o.txnNarration, trAmount = o.trAmount, txnNumber = o.txnNumber, txnPostBalance = o.txnPostBalance, instrumentNumber = o.instrumentNumber }).ToList();
                }
                lblConsumerTitle.Text = "";
                txtConsumerAccount.Text = "";
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        public class operatorfingerPrintGrid
        {
            public string identity { get; set; }
            public string identityName { get; set; }

            private AccountOperator _obj;

            public operatorfingerPrintGrid(AccountOperator obj)
            {
                _obj = obj;
            }

            public AccountOperator GetModel()
            {
                return _obj;
            }
        }

        public class accountStatementGrid
        {
            public string txnDate { get; set; }
            public string txnNarration { get; set; }
            public string trAmount { get; set; }
            public decimal? txnNumber { get; set; }
            public decimal? txnPostBalance { get; set; }
            public string instrumentNumber { get; set; }

            private TransactionDetail _obj;

            public accountStatementGrid(TransactionDetail obj)
            {
                _obj = obj;
            }

            public TransactionDetail GetModel()
            {
                return _obj;
            }
        }

        private void txtConsumerAccount_MouseLeave(object sender, EventArgs e)
        {

        }

        private void frmMiniStatementForTermAccount_FormClosing(object sender, FormClosingEventArgs e)
        {
            ValidationManager.ReleaseValidationData(this);
            this.Owner = null;
        }

        List<MinistatementReport> _miniStatement = new List<MinistatementReport>();
        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                crMiniStatementForTermAccount repObj = new crMiniStatementForTermAccount();
                frmReportViewer viewer = new frmReportViewer();
                ReportHeaders rptHeaders = new ReportHeaders();
                rptHeaders = UtilityServices.getReportHeaders("Term Account Mini Statement");

                TextObject txtBankName = repObj.ReportDefinition.ReportObjects["txtBankName"] as TextObject;
                TextObject txtBranchName = repObj.ReportDefinition.ReportObjects["txtBranchName"] as TextObject;
                TextObject txtBranchAddress = repObj.ReportDefinition.ReportObjects["txtBranchAddress"] as TextObject;
                TextObject txtReportHeading = repObj.ReportDefinition.ReportObjects["txtReportHeading"] as TextObject;
                TextObject txtPrintUser = repObj.ReportDefinition.ReportObjects["txtPrintUser"] as TextObject;
                TextObject txtPrintDate = repObj.ReportDefinition.ReportObjects["txtPrintDate"] as TextObject;

                TextObject txtCustomerTitle = repObj.ReportDefinition.ReportObjects["txtName"] as TextObject;
                TextObject txtAccountNumber = repObj.ReportDefinition.ReportObjects["txtAccountNumber"] as TextObject;
                //TextObject txtSerial = repObj.ReportDefinition.ReportObjects["txtSerial"] as TextObject;
                //if (_consumerInformationDto.consumerTitle != null)
                //{
                txtCustomerTitle.Text = lblConsumerTitle.Text;
                //}
                //if (_consumerInformationDto.consumerTitle != null)
                //{
                txtAccountNumber.Text = txtConsumerAccount.Text;
                //}

                if (rptHeaders != null)
                {
                    if (rptHeaders.branchDto != null)
                    {
                        txtBankName.Text = rptHeaders.branchDto.bankName;
                        txtBranchName.Text = rptHeaders.branchDto.branchName;
                        txtBranchAddress.Text = rptHeaders.branchDto.branchAddress;
                    }
                    txtReportHeading.Text = rptHeaders.reportHeading;
                    txtPrintUser.Text = rptHeaders.printUser;
                    txtPrintDate.Text = rptHeaders.printDate.ToString("dd-MM-yyyy").Replace("-", "/");
                }
                _miniStatement.Clear();
                for (int i = 0; i < _transactionDetails.Count; i++)
                {
                    TransactionDetail tnxDetails = _transactionDetails[i];
                    string drcr = tnxDetails.trAmount.Substring(tnxDetails.trAmount.Length - 3).Trim();
                    string num = tnxDetails.trAmount.Substring(0, tnxDetails.trAmount.Length - 3);
                    Decimal amount = decimal.Parse(num);

                    MinistatementReport MiniStatement = new MinistatementReport
                    {
                        TxnSerial = i + 1,
                        TxnDate = (DateTime.Parse(tnxDetails.txnDate)).ToString("dd-MM-yyyy").Replace("-", "/"),
                        TxnNarration = FirstLetterToUpper(tnxDetails.txnNarration),
                        TrAmount = amount.ToString("N", new CultureInfo("BN-BD")) + " " + drcr,
                        TxnNumber = tnxDetails.txnNumber ?? 0,
                        TxnPostBalance = tnxDetails.txnPostBalance ?? 0,
                        InstrumentNumber = tnxDetails.instrumentNumber
                    };
                    //MiniStatement.TxnNarration = tnxDetails.txnNarration;

                    _miniStatement.Add(MiniStatement);
                }
                repObj.SetDataSource(_miniStatement);
                viewer.crvReportViewer.ReportSource = repObj;
                viewer.ShowDialog(this.Parent);
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        public string FirstLetterToUpper(string str)
        {
            if (str == null)
                return null;

            if (str.Length > 1)
                return char.ToUpper(str[0]) + str.Substring(1).ToLower();

            return str.ToUpper();
        }

        private void txtConsumerAccount_Leave(object sender, EventArgs e)
        {
            ProgressUIManager.ShowProgress(this);
            miniStatementGrid.DataSource = null;
            miniStatementGrid.Rows.Clear();
            loadConsumerInformation();
            ProgressUIManager.CloseProgress();
        }
        internal class MinistatementReport
        {
            public long TxnSerial { get; set; }
            public string TxnDate { get; set; }
            public string TxnNarration { get; set; }
            public string TrAmount { get; set; }
            public decimal TxnNumber { get; set; }
            public decimal TxnPostBalance { get; set; }
            public string InstrumentNumber { get; set; }
        }
    }
}