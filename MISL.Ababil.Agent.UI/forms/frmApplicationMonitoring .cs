﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.consumer;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Validation;
using MISL.Ababil.Agent.UI.forms.ProgressUI;
using MetroFramework.Forms;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.UI.forms
{
    public partial class frmApplicationMonitoring : CustomForm
    {
        ConsumerServices _objConsumerServices = new ConsumerServices();
        List<WorkFlowLogResultDto> _workFlowLogResultDtos = new List<WorkFlowLogResultDto>();
        int _columnLoaded = 0;
        private string _customerTitleQuickFix;

        public frmApplicationMonitoring()
        {
            InitializeComponent();
            //ConfigureValidation();
            setSubagent();
            fillSetupData();
            this.MinimizeBox = false;
        }

        private void fillSetupData()
        {
            Array workFlowType = Enum.GetValues(typeof (WorkFlowType));
            for (int i = 0; i <=3 ; i++)
            {
                cmbWorkFlowType.Items.Add(workFlowType.GetValue(i));
            }


            cmbWorkFlowType.Items.Add(workFlowType.GetValue(5));
            cmbWorkFlowType.Items.Add(workFlowType.GetValue(6));

        }



        private void setSubagent()
        {
            try
            {
                AgentServices _objAgentServices = new AgentServices();
                AgentInformation agentInformation = new AgentInformation();
                //if (SessionInfo.userBasicInformation.userType == AgentUserType.Outlet)
                if (SessionInfo.userBasicInformation.userCategory == UserCategory.SubAgentUser)
                {
                    agentInformation = _objAgentServices.getAgentInfoById(SessionInfo.userBasicInformation.agent.id.ToString());
                    if (agentInformation != null)
                    {
                        BindingSource bs = new BindingSource();
                        bs.DataSource = agentInformation.subAgents;
                        {
                            try
                            {
                                SubAgentInformation saiSelect = new SubAgentInformation();
                                saiSelect.name = "(Select)";
                                agentInformation.subAgents.Insert(0, saiSelect);
                            }
                            catch //suppressed
                            {

                            }
                        }
                        UtilityServices.fillComboBox(cmbOutlet, bs, "name", "id");
                        if (cmbOutlet.Items.Count > 0)
                        {
                            cmbOutlet.SelectedValue = SessionInfo.userBasicInformation.outlet.id;
                            cmbOutlet.Enabled = false;
                            return;
                        }
                    }
                }
                else
                {
                    SubAgentServices subAgentServices = new SubAgentServices();
                    BindingSource bs = new BindingSource();
                    bs.DataSource = subAgentServices.GetAllSubAgents();
                    {
                        try
                        {
                            SubAgentInformation saiSelect = new SubAgentInformation();
                            saiSelect.name = "(Select)";
                            agentInformation.subAgents.Insert(0, saiSelect);
                        }
                        catch //suppressed
                        {

                        }
                    }
                    UtilityServices.fillComboBox(cmbOutlet, bs, "name", "id");
                    if (cmbOutlet.Items.Count > 0)
                    {
                        cmbOutlet.SelectedIndex = -1;
                    }
                }
            }
            catch { }
        }

        //private void ConfigureValidation()
        //{
        //    ValidationManager.ConfigureValidation(this, texReferenceNo, "Reference No", (long)ValidationType.Numeric, false);
        //    ValidationManager.ConfigureValidation(this, txtMobileNo, "Mobile No", (long)ValidationType.BangladeshiCellphoneNumber, false);
        //}

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void frmPendingApplication_Load(object sender, EventArgs e)
        {
            txtMobileNo.MaxLength = CommonRules.mobileNoLength;
        }

        class ConsumerApplicationComparer : IComparer<ConsumerApplication>
        {
            private readonly string _memberName = string.Empty; // the member name to be sorted
            private readonly SortOrder _sortOrder = SortOrder.None;

            public ConsumerApplicationComparer(string memberName, SortOrder sortingOrder)
            {
                _memberName = memberName;
                _sortOrder = sortingOrder;
            }

            public int Compare(ConsumerApplication consumerApplication1, ConsumerApplication consumerApplication2)
            {
                if (_sortOrder != SortOrder.Ascending)
                {
                    var tmp = consumerApplication1;
                    consumerApplication1 = consumerApplication2;
                    consumerApplication2 = tmp;
                }

                switch (_memberName)
                {
                    case "creationDate":
                        return consumerApplication1.creationDate.Value.CompareTo(consumerApplication2.creationDate.Value);
                    default:
                        return consumerApplication1.creationDate.Value.CompareTo(consumerApplication2.creationDate.Value);
                }
            }
        }

        private void loadAllApplications(WorkflowLogSearchDto workflowLogSearchDto)
        {
            try
            {
                _workFlowLogResultDtos = ConsumerServices.getWorkFlowLog(workflowLogSearchDto);
                if (_workFlowLogResultDtos != null)
                {
                    dataGridView1.DataSource = null;
                    dataGridView1.Columns.Clear();
                    dataGridView1.DataSource = _workFlowLogResultDtos.Select(o => new WorkFlowLogResulGrid(o)
                    {
                        RefNo= o.refNo,
                        UserName = o.userName,
                        Date = (UtilityServices.getDateFromLong(o.entryDate ?? 0)).ToString("dd-MM-yyyy"),
                        Time = (UtilityServices.getDateFromLong(o.entryTime  ?? 0)).ToString("hh:mm:ss tt"),
                        ApplicantName = o.applicantName,
                        Status = o.status,
                        filter =
                            (
                                o.refNo +
                                o.userName +
                                UtilityServices.getDateFromLong(o.entryDate ?? 0).ToString("dd-MM-yyyy") +
                                UtilityServices.getDateFromLong(o.entryTime ?? 0).ToString("hh:mm:ss tt zz")+
                                o.applicantName +
                                o.status 
                            ).ToLower()

                    }).ToList();
                    dataGridView1.Columns[0].Width = 35;
                    dataGridView1.Columns[1].Width = 150;
                    dataGridView1.Columns[2].Width = 70;
                    dataGridView1.Columns[3].Width = 70;
                    dataGridView1.Columns[4].Width = 50;
                    dataGridView1.Columns[5].Width = 50;

                    
                    //DataGridViewButtonColumn buttonColumn = new DataGridViewButtonColumn();
                    //if (SessionInfo.userBasicInformation.userType == AgentUserType.Outlet)
                    //{
                    //    buttonColumn.Text = "Edit";
                    //}
                    //else
                    //{
                    //    buttonColumn.Text = "View";
                    //}
                    //buttonColumn.UseColumnTextForButtonValue = true;
                    ////buttonColumn.DefaultCellStyle.
                    //dataGridView1.Columns.Add(buttonColumn);

                    //DataGridViewImageColumn buttonColumn = new DataGridViewImageColumn();
                    //if (SessionInfo.userBasicInformation.userType == AgentUserType.Outlet)
                    //{
                    //    //buttonColumn.Text = "Edit";
                    //}
                    //else
                    //{
                    //    buttonColumn.Text = "View";
                    //}
                    //buttonColumn.UseColumnTextForButtonValue = true;

                    dataGridView1.Columns["filter"].Visible = false;

                    //buttonColumn.Image = imageList1.Images[0];
                    //dataGridView1.Columns.Insert(dataGridView1.Columns.Count - 1, buttonColumn);
                    _columnLoaded = 1;
                    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                    {
                        WorkFlowLogResultDto workFlowLogResultDto = new WorkFlowLogResultDto();
                        workFlowLogResultDto = _workFlowLogResultDtos[i];
                        //if (workFlowLogResultDto.appStatus == ApplicationStatus.draft_at_branch)
                        //    dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.OrangeRed;
                    }


                }
                else
                    MsgBox.showConfirmation("No applications available");
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        //private void loadAllApplications(ConsumerApplicationDto consumerApplicationDto)
        //{
        //    try
        //    {
        //        consumerApplications = ConsumerServices.getConsumerApplications(consumerApplicationDto);

        //        //{
        //        //    DataGridViewColumn column = dataGridView1.Columns[1];

        //        //    string columnName = column.Name;

        //        //    SortOrder sortOrder = column.HeaderCell.SortGlyphDirection == SortOrder.Ascending
        //        //                              ? SortOrder.Descending
        //        //                              : SortOrder.Ascending;

        //        //    consumerApplications.Sort(new ConsumerApplicationComparer(columnName, sortOrder));

        //        //    dataGridView1.Refresh();

        //        //    column.HeaderCell.SortGlyphDirection = sortOrder;

        //        //}
        //        if (consumerApplications != null)
        //        {
        //            dataGridView1.DataSource = null;
        //            dataGridView1.Columns.Clear();
        //            //consumerApplications.Sort();
        //            dataGridView1.DataSource = consumerApplications.Select(o => new ConsumerApplicationGrid(o) { CreationDate = (UtilityServices.getDateFromLong(o.creationDate)).ToString("dd-MM-yyyy"), ConsumerName = o.consumerName, NationalId = o.nationalId, MobileNumber = o.mobileNo, ReferenceNumber = o.referenceNumber, ApplicationStatus = o.applicationStatus }).ToList();
        //            //if (columnLoaded == 0)
        //            //{
        //            DataGridViewButtonColumn buttonColumn = new DataGridViewButtonColumn();
        //            buttonColumn.Text = "Edit";
        //            buttonColumn.UseColumnTextForButtonValue = true;
        //            dataGridView1.Columns.Add(buttonColumn);
        //            columnLoaded = 1;
        //            //}
        //            //else
        //            //{
        //            //    dataGridView1.Columns[0].DisplayIndex = 6;
        //            //}
        //            for (int i = 0; i < dataGridView1.Rows.Count; i++)
        //            {
        //                ConsumerApplication consumerApplicationToEdit = new ConsumerApplication();
        //                consumerApplicationToEdit = consumerApplications[i];
        //                //----if (consumerApplicationToEdit.applicationStatus == ApplicationStatus.draft_at_branch && consumerApplicationToEdit.remarks != null)
        //                if (consumerApplicationToEdit.applicationStatus == ApplicationStatus.draft_at_branch)
        //                    dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.OrangeRed;
        //            }
        //        }
        //        else
        //            MessageBox.Show("No applications available");
        //    }
        //    catch (Exception ex)
        //    {
        //        Message.ShowError(ex.Message);
        //    }
        //}

        public class WorkFlowLogResulGrid
        {
            private WorkFlowLogResultDto _obj;

            //public long WorkflowId { get; set; }

            //public UserCategory UserCategory { get; set; }
            public string RefNo { get; set; }
            public string ApplicantName { get; set; }
            
            public string Status { get; set; }
            public string UserName { get; set; }
            public string Date { get; set; }
            public string Time { get; set; }
            public string Comments { get; set; }
            public string filter { get; set; }

            public WorkFlowLogResulGrid(WorkFlowLogResultDto obj)
            {
                _obj = obj;
            }

            public WorkFlowLogResultDto GetModel()
            {
                return _obj;
            }
        }
        private bool validationCheck()
        {
            return ValidationManager.ValidateForm(this);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            
            try
            {
                if (cmbWorkFlowType.SelectedIndex == -1)
                {
                    MsgBox.showWarning("Please select a service type");
                }
                else
                {
                    btnSearch.Enabled = false;
                    Cursor.Current = Cursors.WaitCursor;
                    search();
                    Cursor.Current = Cursors.Default;
                    btnSearch.Enabled = true;
                }
                
            }
            catch (Exception)
            {
                Cursor.Current = Cursors.Default;
            }
           
            
        }

        private void search()
        {
            //if (validationCheck())
            //{
            WorkflowLogSearchDto workflowLogSearchDto = new WorkflowLogSearchDto(); //SessionInfo.rights.Any(p => p == Rights.CREATE_BANK_USER.ToString()


            //workflowLogSearchDto.mobileNo = txtMobileNo.Text;
            // consumerApplicationDto.nationalId = txtNationalId.Text;
            workflowLogSearchDto.referenceNumber = texReferenceNo.Text;
            workflowLogSearchDto.fromDate = UtilityServices.GetLongDate(dtpFromDate.Value);
            workflowLogSearchDto.toDate = UtilityServices.GetLongDate(dtpToDate.Value);
            workflowLogSearchDto.workFlowType=(WorkFlowType) cmbWorkFlowType.SelectedItem;
            //workflowLogSearchDto.branchId = SessionInfo.userBasicInformation.userBranch;


           

            ProgressUIManager.ShowProgress(this);
            loadAllApplications(workflowLogSearchDto);
            ProgressUIManager.CloseProgress();

            lblItemsFound.Text = "   Item(s) Found: " + dataGridView1.Rows.Count.ToString();
            //}                         

            try
            {
                customDataGridViewHeader.ExportReportSubtitleThree = cmbWorkFlowType.SelectedItem.ToString();
                DataGridHeaderUtility.ReportHederDataExportToPdf(customDataGridViewHeader);
            }
            catch (Exception ex)
            {
                //suppressed
            }
        }


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //int selRow = e.RowIndex;
            //var senderGrid = (DataGridView)sender;
            //if (e.RowIndex >= 0)
            //{
            //    senderGrid.Rows[e.RowIndex].Selected = true;
            //}

            //if (senderGrid.Columns[e.ColumnIndex] is DataGridViewImageColumn &&
            //    e.RowIndex >= 0)
            //{
            //    ProgressUIManager.ShowProgress(this);

            //    wor consumerAppResultDtoEdit = new ConsumerAppResultDto();
            //    consumerAppResultDtoEdit = _consumerAppResultDto[e.RowIndex];

            //    if (consumerAppResultDtoEdit.appStatus == ApplicationStatus.draft_at_branch)
            //    {
            //        frmBrRemarks objFrmBrRemarks = new frmBrRemarks(consumerAppResultDtoEdit);
            //        objFrmBrRemarks.Show();

            //    }
            //    else
            //    {
            //        //ConsumerApplication consumerApplicationTmp = _objConsumerServices.getConsumerApplicationById(consumerAppResultDtoEdit.refNo);
            //        ConsumerApplicationDto consumerApplicationDto = new ConsumerApplicationDto();
            //        consumerApplicationDto.referenceNumber = consumerAppResultDtoEdit.refNo;

            //        ConsumerApplication consumerApplication = _objConsumerServices.getConsumerApplicationById(consumerAppResultDtoEdit.refNo);
            //        _customerTitleQuickFix = consumerApplication.customer.title;
            //        consumerApplication.customer = null;
            //        //List<ConsumerApplication> consumerApplicationList = _objConsumerServices.getListOfConsumerApplicationsA(consumerApplicationDto);

            //        try
            //        {
            //            byte[] consumerPhoto = _objConsumerServices.getConumerPhotoByAppId(consumerAppResultDtoEdit.appId);
            //            consumerApplication.photo = consumerPhoto;
            //        }
            //        catch (Exception ex)
            //        {
            //            MsgBox.ShowError(ex.Message);
            //        }

            //        Packet packet = new Packet();
            //        packet.DeveloperMode = false;

            //        if (SessionInfo.roles.Contains("Sub Agent"))
            //        {
            //            if (
            //                consumerApplication.applicationStatus != ApplicationStatus.approved
            //                &&
            //                consumerApplication.applicationStatus != ApplicationStatus.canceled
            //                )
            //            {
            //                packet.actionType = FormActionType.Edit;
            //            }
            //            else
            //            {
            //                packet.actionType = FormActionType.View;
            //            }
            //        }
            //        else
            //        {
            //            packet.actionType = FormActionType.View;
            //        }

            //        packet.intentType = IntentType.Request;
            //        packet.otherObj = _customerTitleQuickFix;


            //        frmConsumerCreation objFrmConsumerCreation = new frmConsumerCreation(packet, consumerApplication);
            //        ProgressUIManager.CloseProgress();
            //        if (objFrmConsumerCreation.ShowDialog(this) != DialogResult.No)
            //        {
            //            btnSearch.Enabled = false;
            //            btnSearch.Enabled = true;
            //        }
            //        search();
            //        try
            //        {
            //            dataGridView1.Rows[selRow].Selected = true;
            //        }
            //        catch { }
            //    }
            //}
        }

        private void txtNationalId_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!char.IsNumber(e.KeyChar) && (Keys)e.KeyChar != Keys.Back && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            base.OnKeyPress(e);
        }

        private void txtMobileNo_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!char.IsNumber(e.KeyChar) && (Keys)e.KeyChar != Keys.Back && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            base.OnKeyPress(e);
        }

        private void frmPendingApplication_FormClosing(object sender, FormClosingEventArgs e)
        {
            //ValidationManager.ReleaseValidationData(this);
            this.Owner = null;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            search();
        }

        private void dtpDOB_ValueChanged(object sender, EventArgs e)
        {
            //mtbDOB.Text = dtpDOB.Value.ToString("dd-MM-yyyy");
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {
            //mtbFromDate.Text = dtpFromDate.Value.ToString("dd-MM-yyyy");
        }

        private void dtpToDate_ValueChanged(object sender, EventArgs e)
        {
            //mtbToDate.Text = dtpToDate.Value.ToString("dd-MM-yyyy");
        }

        private void mtbDOB_KeyUp(object sender, KeyEventArgs e)
        {
            //suppressed to avoid mtb to dtp conversion
            //try
            //{
            //    string[] str = mtbDOB.Text.Split('-');
            //    DateTime d = new DateTime(int.Parse(str[2].Trim()), int.Parse(str[1].Trim()), int.Parse(str[0].Trim()));
            //    dtpDOB.Value = d;
            //}
            //catch (Exception ex) { }
        }

        //private void mtbFromDate_KeyUp(object sender, KeyEventArgs e)
        //{
        //    //suppressed to avoid mtb to dtp conversion
        //    try
        //    {
        //        string[] str = mtbFromDate.Text.Split('-');
        //        DateTime d = new DateTime(int.Parse(str[2].Trim()), int.Parse(str[1].Trim()), int.Parse(str[0].Trim()));
        //        dtpFromDate.Value = d;
        //    }
        //    catch (Exception ex) { }
        //}

        //private void mtbToDate_KeyUp(object sender, KeyEventArgs e)
        //{
        //    //suppressed to avoid mtb to dtp conversion
        //    try
        //    {
        //        string[] str = mtbToDate.Text.Split('-');
        //        DateTime d = new DateTime(int.Parse(str[2].Trim()), int.Parse(str[1].Trim()), int.Parse(str[0].Trim()));
        //        dtpToDate.Value = d;
        //    }
        //    catch (Exception ex) { }
        //}

        private void btnToDateClear_Click(object sender, EventArgs e)
        {
            ///mtbToDate.Clear();
        }

        private void btnFromDateClear_Click(object sender, EventArgs e)
        {
            //mtbFromDate.Clear();
        }

        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void ClearAll()
        {
            //consumerApplications = new List<ConsumerApplication>();
            _workFlowLogResultDtos = new List<WorkFlowLogResultDto>();
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
            //mtbFromDate.Clear();
            //mtbToDate.Clear();
            cmbOutlet.SelectedIndex = -1;
            txtAccNO.Text = "";
            texReferenceNo.Clear();
            // txtNationalId.Clear();
            //consumerNameTxt.Clear();
            txtMobileNo.Clear();
            cmbWorkFlowType.SelectedIndex = -1;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ClearAll();
        }

        private void txtNationalId_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            //if (dataGridView1.Rows.Count <= 20)
            //{
            //    if (dataGridView1.Columns[e.ColumnIndex] is DataGridViewImageColumn &&
            //        e.RowIndex >= 0)
            //    {
            //        (dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex]).Value = imageList1.Images[1];

            //        //try
            //        //{
            //        //    for (int i = 0; i < dataGridView1.Rows.Count; i++)
            //        //    {
            //        //        (dataGridView1.Rows[e.RowIndex].Cells[i]).Style.BackColor = Color.RoyalBlue;
            //        //    }
            //        //}
            //        //catch (Exception)
            //        //{


            //        //}
            //    }
            //}
        }

        private void dataGridView1_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            //if (dataGridView1.Rows.Count <= 20)
            //{
            //    if (dataGridView1.Columns[e.ColumnIndex] is DataGridViewImageColumn &&
            //        e.RowIndex >= 0)
            //    {
            //        (dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex]).Value = imageList1.Images[0];

            //        //try
            //        //{
            //        //    for (int i = 0; i < dataGridView1.Rows.Count; i++)
            //        //    {
            //        //        (dataGridView1.Rows[e.RowIndex].Cells[i]).Style.BackColor = Color.White;
            //        //    }

            //        //}
            //        //catch (Exception)
            //        //{


            //        //}
            //    }
            //}
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Focused && e.RowIndex >= 0)
            {
                (dataGridView1.Rows[e.RowIndex].Cells[dataGridView1.ColumnCount - 1]).Value = imageList1.Images[1];
            }
        }

        private void dataGridView1_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Focused)
            {
                (dataGridView1.Rows[e.RowIndex].Cells[dataGridView1.ColumnCount - 1]).Value = imageList1.Images[0];
            }
        }

        //private void dataGridView1_CellEnter(object sender, DataGridViewCellEventArgs e)
        //{
        //    dataGridView1_CellClick(sender, e);
        //}
        //bool IsTheSameCellValue(int column, int row)
        //{
        //    DataGridViewCell cell1 = dataGridView1[column, row];
        //    DataGridViewCell cell2 = dataGridView1[column, row - 1];
        //    if (cell1.Value == null || cell2.Value == null)
        //    {
        //        return false;
        //    }
        //    return cell1.Value.ToString() == cell2.Value.ToString();
        //}

        private void dataGridView1_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            //e.AdvancedBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
            //e.AdvancedBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.Single;

            //if (e.ColumnIndex > 1)
            //{
            //    e.AdvancedBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.Single;
            //    e.AdvancedBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
            //    return;
            //}


            //if (e.RowIndex < 1 || e.ColumnIndex < 0)
            //{

            //    return;
            //}
            //else
            //{
            //    e.AdvancedBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
            //}



            //if (IsTheSameCellValue(e.ColumnIndex, e.RowIndex))
            //{
            //    e.AdvancedBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
            //    e.AdvancedBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
            //}
            //else
            //{
            //    e.AdvancedBorderStyle.Top = dataGridView1.AdvancedCellBorderStyle.Top;
            //    e.AdvancedBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
            //}
        }

        private void texReferenceNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) && (Keys)e.KeyChar != Keys.Back && e.KeyChar != '.')
            {
                e.Handled = true;
            }
            base.OnKeyPress(e);
        }

        //private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        //{
        //    if (e.RowIndex == 0)
        //        return;
        //    if (IsTheSameCellValue(e.ColumnIndex, e.RowIndex))
        //    {
        //        e.Value = "";
        //        e.FormattingApplied = true;
        //    }
        //}
    }
}