﻿using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.cis;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.consumer;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.fingerprint;
using MISL.Ababil.Agent.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using MISL.Ababil.Agent.Infrastructure.Validation;
using MISL.Ababil.Agent.Report;
using System.IO;
using MISL.Ababil.Agent.UI.forms.ProgressUI;
using MISL.Ababil.Agent.Communication;
using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.UI.forms
{
    public partial class frmConsumerApplication : CustomForm
    {
        private Packet _receivePacket;
        private GUI _gui = new GUI();
        private CustomerApplyDto _customerApplyDto;
        private AgentProduct _agentProduct = new AgentProduct();
        private ConsumerServices _consumerServices = new ConsumerServices();
        private AgentServices _agentServices = new AgentServices();
        private List<custIndividualData> _individuals = new List<custIndividualData>();
        private List<FingerInfo> _fingerList = new List<FingerInfo>();
        private List<AgentProduct> _agentProductList = new List<AgentProduct>();
        private List<CisType> _cisTypes;
        private int _columnLoaded = 0;

        public frmConsumerApplication(Packet packet)
        {
            InitializeComponent();
            _receivePacket = packet;
            InitialSetup();
            _customerApplyDto = new CustomerApplyDto { ownerInformations = new List<OwnerInformation>() };
            AddCaptureButtons();
            AddDeleteButtons();
            AddOperatorCheckBox();
            AddMandatoryOperatorCheckBox();
            ConfigUIEnhancement();
        }

        private void InitialSetup()
        {
            SetupDataLoad();
            SetupComponents();
        }

        private void SetupDataLoad()
        {
            UtilityServices.fillCustomerTypes(ref cmbCustomerType);

            UtilityCom objUtil = new UtilityCom();
            try
            {
                _cisTypes = objUtil.getcmbCustomerTypes();

                BindingSource bsc = new BindingSource { DataSource = _cisTypes };
                UtilityServices.fillComboBox(cmbCustomerType, bsc, "description", "id");
                cmbCustomerType.SelectedIndex = -1;
                cmbCustomerType.Text = "Select";
                //UtilityServices.fillOwnerTypes(ref cmbOwnerType);
                cmbAccType.DataSource = Enum.GetValues(typeof(ProductType));
                cmbAccType.Enabled = false;
                txtMobile.MaxLength = 11;
                _agentProductList = _agentServices.getAgentProductByProductType(cmbAccType.Text);
                BindingSource bs = new BindingSource { DataSource = _agentProductList };
                UtilityServices.fillComboBox(cmbProduct, bs, "productTitle", "id");
                cmbProduct.SelectedIndex = -1;
                txtOpeningDeposit.Text = "";

            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        public void SetupComponents()
        {
            tabCtrlCustomer.TabPages.Remove(tabPageExistingCustomer);  // WALI :: 15-May-2016
        }

        public void ConfigUIEnhancement()
        {
            _gui = new GUI(this);
            //_gui.Config(ref txtConsumeName, ValidCheck.VALIDATIONTYPES.TEXTBOX_EMPTY, null);      // 'Wali :: 25-Apr-2016
            //gui.Config(ref txtNationalId, ValidCheck.VALIDATIONTYPES.TEXTBOX_NUMBER, null);
            //_gui.Config(ref txtMobile, ValidCheck.VALIDATIONTYPES.TEXTBOX_NUMBER_MOBILE, null);   // 'Wali :: 25-Apr-2016
            _gui.Config(ref cmbAccType, ValidCheck.VALIDATIONTYPES.COMBOBOX_EMPTY, null);
            _gui.Config(ref cmbProduct, ValidCheck.VALIDATIONTYPES.COMBOBOX_EMPTY, null);
            _gui.Config(ref txtOpeningDeposit);

            _gui.Config(ref cmbCustomerType, ValidCheck.VALIDATIONTYPES.COMBOBOX_EMPTY, null);
            _gui.Config(ref cmbOwnerType, ValidCheck.VALIDATIONTYPES.COMBOBOX_EMPTY, null);
            //_gui.Config(ref txtIndiFirstName);
            //_gui.Config(ref txtIndiLastName);
            _gui.Config(ref txtAccOperated, ValidCheck.VALIDATIONTYPES.TEXTBOX_NUMBER, null);
            _gui.Config(ref txtCustomerId);
            //_gui.Config(ref txtNationalId, ValidCheck.VALIDATIONTYPES.TEXTBOX_NationalID, "");    // 'Wali :: 25-Apr-2016
        }

        private void AddCaptureButtons()
        {
            DataGridViewButtonColumn buttonCapture = new DataGridViewButtonColumn
            {
                Text = "Capture",
                UseColumnTextForButtonValue = true
            };
            gvIndividualsList.Columns.Add(buttonCapture);
        }

        private void AddDeleteButtons()
        {
            DataGridViewButtonColumn btnDelete = new DataGridViewButtonColumn
            {
                Text = "Delete",
                UseColumnTextForButtonValue = true
            };
            gvIndividualsList.Columns.Add(btnDelete);
        }

        private void AddOperatorCheckBox()
        {
            DataGridViewCheckBoxColumn chkbOperator = new DataGridViewCheckBoxColumn
            {
                HeaderText = "Operator",
                IndeterminateValue = true,
                TrueValue = true,
                FalseValue = false
            };
            gvIndividualsList.Columns.Add(chkbOperator);

        }

        private void AddMandatoryOperatorCheckBox()
        {
            DataGridViewCheckBoxColumn chkbxMandatoryOperator = new DataGridViewCheckBoxColumn
            {
                HeaderText = "Mandatory Operator",
                IndeterminateValue = false,
                TrueValue = true,
                FalseValue = false
            };
            gvIndividualsList.Columns.Add(chkbxMandatoryOperator);
        }

        private void btnPicUpload_Click(object sender, EventArgs e)
        {
            try
            {
                UtilityServices.uploadPhoto(ref openFileDialog1, ref pbConsumerPic);
            }
            catch (Exception)
            {
                throw new ApplicationException("Image loading error....");
            }
        }

        private bool CheckValidation(CustomerApplyDto customerApplyDto)
        {
            if (customerApplyDto.customerName.Trim() == "")
            {
                MsgBox.showWarning("Consumer name can not be left blank");
                return false;
            }
            //if (customerApplyDto.nationalId.Trim() == "")
            //{
            //    MsgBox.showWarning("National Id can not be left blank");
            //    return false;
            //}
            if (customerApplyDto.mobileNo.Trim() == "")
            {
                MsgBox.showWarning("Mobile number can not be left blank");
                return false;
            }
            if (customerApplyDto.photo == null)
            {
                MsgBox.showWarning("Consumer Photo can not be left blank.");
                return false;
            }
            if (customerApplyDto.productType == 0)
            {
                MsgBox.showWarning("Product can not be left blank.");
                return false;
            }
            if (customerApplyDto.openingDeposit < 10 || String.IsNullOrEmpty(customerApplyDto.openingDeposit.ToString()))
            {
                MsgBox.showWarning("Opening deposit should not be less than Tk 10.");
                return false;
            }

            //
            //keep for future modification
            //
            //if (customerApplyDto.openingDeposit < _agentProduct.openingDeposit)
            //{
            //    Message.showWarning("Opening deposit should be atleast " + _agentProduct.openingDeposit + ".");
            //    return false;
            //}

            if (gvIndividualsList.Rows.Count <= 0)
            {
                MsgBox.showWarning("Owner information not found!.");
                return false;
            }

            //fingerprint capture checking
            {
                int noOfOperator = (txtAccOperated.Text != "") ? Convert.ToInt32(txtAccOperated.Text) : 0;

                return ValidateFingerprint(customerApplyDto, noOfOperator);




                //for (int i = 0; i < customerApplyDto.ownerInformations.Count; i++)
                //{
                //    if (customerApplyDto.ownerInformations[i].individualInformation.fingerInfos == null || customerApplyDto.ownerInformations[i].individualInformation.fingerInfos.Count <= 0)
                //    {
                //        //MsgBox.showWarning("Fingerprint not captured for individual: " + customerApplyDto.ownerInformations[i].individualInformation.firstName + "!");
                //        //return false;
                //        noOfBioData--;
                //    }
                //}

                //if (noOfBioData < noOfOperator)
                //{
                //    MsgBox.showWarning("Fingerprint not captured for required operators !");
                //    return false;
                //}
            }

            if (customerApplyDto.noOfOperator <= 0)
            {
                MsgBox.showWarning("Number of operators not valid.");
                return false;
            }

            if (customerApplyDto.noOfOperator > gvIndividualsList.Rows.Count)
            {
                MsgBox.showWarning("Number of operators not valid.");
                return false;
            }

            return true;
        }

        private bool ValidateFingerprint(CustomerApplyDto customerApplyDto, int? noOfOperator)
        {
            int noOfBioData = 0;
            int noOfBioDataChecked = 0;
            //int noOfBioData = customerApplyDto.ownerInformations.Count;
            for (int i = 0; i < customerApplyDto.ownerInformations.Count; i++)
            {
                if (customerApplyDto.ownerInformations[i].mandatoryOperator == true
                    &&
                    customerApplyDto.ownerInformations[i].accountOperator == false)
                {
                    MsgBox.showWarning("Account Operator required!");
                    return false;
                }
                if (customerApplyDto.ownerInformations[i].accountOperator == true
                    &&
                    customerApplyDto.ownerInformations[i].individualInformation.fingerInfos != null
                    &&
                    customerApplyDto.ownerInformations[i].individualInformation.fingerInfos?.Count > 0)
                {
                    noOfBioDataChecked++;
                }
                else
                {

                }

                if (customerApplyDto.ownerInformations[i].individualInformation.fingerInfos != null
                    &&
                    customerApplyDto.ownerInformations[i].individualInformation.fingerInfos?.Count > 0)
                {
                    noOfBioData++;
                }
                else
                {

                }


                //if (customerApplyDto.ownerInformations[i].individualInformation.fingerInfos == null || customerApplyDto.ownerInformations[i].individualInformation.fingerInfos.Count <= 0)
                //{
                //    
                //}
            }
            //if (noOfBioData < noOfOperator)
            if (noOfBioDataChecked >= noOfOperator && noOfBioData == customerApplyDto.ownerInformations.Count)
            {
                return true;
            }
            else
            {
                MsgBox.showWarning("Account Operator Fingerprint required!");
            }
            return false;
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            // Wali :: 25-Apr-2016
            bool validForm = CustomControlValidation.IsAllValid(true, false, txtConsumeName, cmbDocType, txtMobile, txtDocID);

            if (!validForm) return;
            //

            btnApply.Enabled = false;
            string result = MsgBox.showConfirmation("Are you sure to apply?");
            _gui.RefreshOwnerForm();
            if (result == "yes")
            {
                if (_gui.IsAllControlValidated())
                {
                    if (pbConsumerPic.Image == null)
                    {
                        MsgBox.showWarning("Consumer Photo can not be left blank.");
                        btnApply.Enabled = true;
                        _gui.RefreshOwnerForm();
                        return;
                    }
                    FillObjectWithComponentValue();

                    if (CheckValidation(_customerApplyDto))
                    {
                        try
                        {
                            ProgressUIManager.ShowProgress(this);
                            string retrnMsg = _consumerServices.applyConsumer(_customerApplyDto);
                            ProgressUIManager.CloseProgress();

                            MsgBox.showInformation("Consumer application draft saved successfully with reference number " + retrnMsg);
                            ClearAllControls();
                            _gui.RefreshOwnerForm();
                            frmShowReport objfrmShowReport = new frmShowReport();
                            objfrmShowReport.PreAccountReport(retrnMsg);
                            _gui.RefreshOwnerForm();
                        }
                        catch (Exception ex)
                        {
                            ProgressUIManager.CloseProgress();
                            btnApply.Enabled = true;
                            MsgBox.ShowError(ex.Message);
                            _gui.RefreshOwnerForm();
                        }
                    }
                    else
                    {
                        MsgBox.ShowError("Validation error!");
                        _gui.RefreshOwnerForm();
                        _gui.IsAllControlValidated();
                    }
                }
                else
                {
                    MsgBox.ShowError("Validation error!");
                    _gui.RefreshOwnerForm();
                    _gui.IsAllControlValidated();
                }
            }
            else
            {
                btnApply.Enabled = true;
                _gui.RefreshOwnerForm();

                return;
            }
            btnApply.Enabled = true;
            _gui.RefreshOwnerForm();
            _gui.IsAllControlValidated();
        }

        private void FillObjectWithComponentValue()
        {
            try
            {
                _customerApplyDto.customerTitle = txtCustomerTitle.Text.Trim();
                _customerApplyDto.customerName = txtConsumeName.Text.Trim();
                //_customerApplyDto.nationalId = txtNationalId.Text.Trim();
                _customerApplyDto.mobileNo = txtMobile.Text.Trim();
                _customerApplyDto.photo = UtilityServices.imageToByteArray(pbConsumerPic.Image);
                if (cmbCustomerType.SelectedIndex >= 0)
                {
                    _customerApplyDto.cisType = _cisTypes[cmbCustomerType.SelectedIndex];
                }
                // consumerApp.customer = _consumerApp.customer;
                _customerApplyDto.productType = _agentProduct.id;
                _customerApplyDto.noOfOperator = (txtAccOperated.Text.Trim() != "") ? Convert.ToInt32(txtAccOperated.Text.Trim()) : 0;
                _customerApplyDto.openingDeposit = (txtOpeningDeposit.Text.Trim() != "") ? Convert.ToDecimal(txtOpeningDeposit.Text.Trim()) : 0;

                _customerApplyDto.documentType = cmbDocType.Text;
                _customerApplyDto.documentNo = txtDocID.Text;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        private void ClearAllControls()
        {
            txtCustomerTitle.Text = string.Empty;
            txtConsumeName.Text = string.Empty;
            //txtNationalId.Text = string.Empty;
            cmbDocType.SelectedIndex = -1;
            cmbDocType.Text = string.Empty;
            txtDocID.Text = string.Empty;
            txtMobile.Text = string.Empty;
            cmbProduct.SelectedIndex = -1;
            txtOpeningDeposit.Text = "";
            lblCustomerId.Text = "";
            pbConsumerPic.Image = null;
            txtIndiFirstName.Text = string.Empty;
            txtIndiLastName.Text = string.Empty;
            gvIndividualsList.DataSource = null;
            txtAccOperated.Text = "0";
            chkSendToBranch.Checked = false;
            _customerApplyDto = new CustomerApplyDto();
            _customerApplyDto.ownerInformations = new List<OwnerInformation>();
            _individuals = new List<custIndividualData>();
            _fingerList = new List<FingerInfo>();
            cmbAccType.Text = "Select";
            cmbProduct.Text = "Select";
            cmbCustomerType.Text = "Select";
            cmbOwnerType.Text = "Select";
            tabControl1.SelectedTab = tabControl1.TabPages[0];
            tabCtrlCustomer.SelectedTab = tabCtrlCustomer.TabPages[0];
            _gui.RefreshOwnerForm();
            _gui.FocusControl(txtConsumeName);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            string result = MsgBox.showConfirmation("Are you sure to clear all data?");
            _gui.RefreshOwnerForm();
            if (result == "yes")
            {
                ClearAllControls();
                _gui.RefreshOwnerForm();
            }
            _gui.RefreshOwnerForm();
        }

        private void frmConsumerApplication_Load(object sender, EventArgs e)
        {
            try
            {
                txtConsumeName.Focus();
                tabControl1.SelectedTab = tabControl1.TabPages[0];
                txtConsumeName.Focus();
                SendKeys.Send("{Tab}");
            }
            catch { }
        }

        private void btnFinger_Click(object sender, EventArgs e)
        {
            frmFingerprintCapture objFrmFinger = new frmFingerprintCapture(txtConsumeName.Text.Trim());
            DialogResult dr = objFrmFinger.ShowDialog();
            if (dr == DialogResult.OK)
            {
                _fingerList = objFrmFinger.FingerInfos;
            }
        }

        private void txtNationalId_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (!char.IsNumber(e.KeyChar) && (Keys)e.KeyChar != Keys.Back && e.KeyChar != '.')
            //{
            //    e.Handled = true;
            //}
            //
            //base.OnKeyPress(e);
        }

        private void txtMobile_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) && (Keys)e.KeyChar != Keys.Back && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            base.OnKeyPress(e);
        }

        private void btnSearchCustomer_Click(object sender, EventArgs e)
        {
            if (txtCustomerId.Text.Trim() != "")
                showDataFromCustomerId();
            else
                MsgBox.showInformation("Please provide valid Customer Id");
        }
        private void showDataFromCustomerId()
        {
            try
            {
                #region OLD..
                //int customerId = Convert.ToInt32(txtCustomerId.Text.Trim());
                //CustomerDto objCustomer = (new CustomerServices()).GetCustomerInfo(customerId);
                //if (objCustomer != null)
                //{
                //    List<OwnerInformation> objOwnerInfo = new List<OwnerInformation>();
                //    foreach (OwnerInformation owner in objOwnerInfo)
                //    {
                //        custIndividualData custIndi = new custIndividualData();
                //        custIndi.firstName = owner.individualInformation.firstName;
                //        custIndi.lastName = owner.individualInformation.lastName;
                //        //custIndi.fingerDatas=
                //        if (individuals == null)
                //            individuals = new List<custIndividualData>();
                //        individuals.Add(custIndi);
                //    }
                //    gvIndividualsList.DataSource = individuals;
                //}
                #endregion

                lblCustomerName.Text = "";
                gvIndividualsList.DataSource = null;
                gvIndividualsList.Refresh();

                int customerId = Convert.ToInt32(txtCustomerId.Text.Trim());
                CustomerInformation objCustomer = _consumerServices.getCustomerByAppId(customerId);
                if (objCustomer != null)
                {
                    lblCustomerName.Text = objCustomer.title;
                    _customerApplyDto.ownerInformations = new List<OwnerInformation>();
                    foreach (OwnerInformation owner in objCustomer.owners)
                    {
                        _customerApplyDto.ownerInformations.Add(owner);
                    }
                    gvIndividualsList.DataSource = _customerApplyDto.ownerInformations.Select(o => new CustomerIndividualGrid(o)
                    {
                        FirstName = o.individualInformation.firstName,
                        LastName = o.individualInformation.lastName

                    }).ToList();
                    txtAccOperated.Text = (((txtAccOperated.Text != "") ? Convert.ToInt32(txtAccOperated.Text) : 0) + 1).ToString();
                    //ColorFingerDataRows(_customerApplyDto.ownerInformations);
                    gvIndividualsList.Columns[0].ReadOnly = true;
                    gvIndividualsList.Columns[1].ReadOnly = true;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        private bool validIndividual()
        {
            if (cmbCustomerType.Text == "Select" || cmbCustomerType.Text == "")
            {
                MsgBox.ShowError("Customer type not found");
                return false;
            }
            if (cmbOwnerType.Text == "" || cmbOwnerType.Text == "Select")
            {
                MsgBox.ShowError("Owner type not found");
                return false;
            }
            //if (txtIndiFirstName.Text == "")
            //{
            //    MsgBox.ShowError("Individual first name not found");
            //    return false;
            //}
            if (txtIndiLastName.Text == "")
            {
                MsgBox.ShowError("Individual last name not found");
                return false;
            }
            return true;
        }

        private void btnAddIndividual_Click(object sender, EventArgs e)
        {
            if (validIndividual())
            {
                OwnerInformation ownerInformation = new OwnerInformation();
                IndividualInformation individualInformation = new IndividualInformation
                {
                    firstName = txtIndiFirstName.Text.Trim(),
                    lastName = txtIndiLastName.Text.Trim()
                };

                if (cmbCustomerType.Text != "Select")
                {
                    try
                    {
                        ownerInformation.ownerType = UtilityServices.getOwnerTypes(Convert.ToInt32(cmbCustomerType.SelectedValue)).Single(o => o.id == Convert.ToInt32(cmbOwnerType.SelectedValue));
                        //if (objOwner.ownerType.cisType.description == "Personal")
                        if (cbxUseAsCustomerName.Checked)
                        {
                            //txtCustomerTitle.Text = objIndividualInfo.firstName;
                            txtConsumeName.Text = (individualInformation.firstName + " " + individualInformation.lastName).Trim();
                            cbxUseAsCustomerName.Checked = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex.Message);
                    }
                }
                ownerInformation.individualInformation = individualInformation;
                _customerApplyDto.ownerInformations.Add(ownerInformation);
                _customerApplyDto.ownerInformations[_customerApplyDto.ownerInformations.Count - 1].accountOperator = true;

                gvIndividualsList.DataSource =
                    _customerApplyDto.ownerInformations.Select(o => new CustomerIndividualGrid(o)
                    {
                        FirstName = o.individualInformation.firstName,
                        LastName = o.individualInformation.lastName
                    }).ToList();

                for (int i = 0; i < _customerApplyDto.ownerInformations.Count; i++)
                {
                    gvIndividualsList.Rows[i].Cells[2].Value = _customerApplyDto.ownerInformations[i].accountOperator;
                    gvIndividualsList.Rows[i].Cells[3].Value = _customerApplyDto.ownerInformations[i].mandatoryOperator;
                }

                txtAccOperated.Text = (((txtAccOperated.Text != "") ? Convert.ToInt32(txtAccOperated.Text) : 0) + 1).ToString(); // WALI :: 17-Feb-2016
                ColorFingerDataRows(_customerApplyDto.ownerInformations);
                txtIndiFirstName.Text = "";
                txtIndiLastName.Text = "";
            }
            txtIndiFirstName.Focus();
        }

        private void ColorFingerDataRows(List<OwnerInformation> owners)
        {
            for (int i = 0; i < owners.Count; i++)
            {
                if (owners[i].individualInformation.fingerInfos != null && owners[i].individualInformation.fingerInfos.Count != 0)
                {
                    gvIndividualsList.Rows[i].DefaultCellStyle.BackColor = Color.Green;
                }
            }
        }

        private void gvIndividualsList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView senderGrid = (DataGridView)sender;
            senderGrid.CommitEdit(DataGridViewDataErrorContexts.Commit);

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                if (e.ColumnIndex == 0)
                {
                    //frmFingerprintCapture objFrmFinger = new frmFingerprintCapture(txtConsumeName.Text.Trim());
                    frmFingerprintCapture objFrmFinger = new frmFingerprintCapture("");
                    DialogResult dr = objFrmFinger.ShowDialog();
                    if (dr == DialogResult.OK)
                    {
                        _fingerList = objFrmFinger.FingerInfos;
                        _customerApplyDto.ownerInformations[e.RowIndex].individualInformation.fingerInfos = _fingerList;
                        //_customerApplyDto.ownerInformations[e.RowIndex].capture = true;
                        ColorFingerDataRows(_customerApplyDto.ownerInformations);
                    }
                }
                if (e.ColumnIndex == 1)
                {
                    int rowIndex = e.RowIndex;
                    _customerApplyDto.ownerInformations.RemoveAt(rowIndex);
                    gvIndividualsList.DataSource = null;
                    gvIndividualsList.DataSource = _customerApplyDto.ownerInformations.Select(o => new CustomerIndividualGrid(o) { FirstName = o.individualInformation.firstName, LastName = o.individualInformation.lastName }).ToList();

                    ColorFingerDataRows(_customerApplyDto.ownerInformations);
                    int individualCount = gvIndividualsList.Rows.Count;
                    int operatorCount = (txtAccOperated.Text != "") ? Convert.ToInt32(txtAccOperated.Text.Trim()) : 0;
                    if (operatorCount <= individualCount)
                    {
                        //txtAccOperated.Text = "1";
                    }
                    else
                        txtAccOperated.Text = (((txtAccOperated.Text != "") ? Convert.ToInt32(txtAccOperated.Text) : 0) - 1).ToString();
                }
            }
            //if (senderGrid.Columns[e.ColumnIndex] is DataGridViewCheckBoxColumn && e.RowIndex >= 0)
            //{

            //    if (e.ColumnIndex == 3) //for mandatory operator
            //    {
            //        _customerApplyDto.ownerInformations[e.RowIndex].mandatoryOperator = (bool?)senderGrid.Rows[e.RowIndex].Cells[3].Value == true;

            //    }
            //    if (e.ColumnIndex == 2) //for acc operator
            //    {
            //        _customerApplyDto.ownerInformations[e.RowIndex].accountOperator = (bool?)senderGrid.Rows[e.RowIndex].Cells[2].Value == true;
            //    }
            //}

        }

        private void cmbProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!cb.Focused)
            {
                return;
            }
            _agentProduct = _agentProductList.Single(o => o.id == Convert.ToInt32(cmbProduct.SelectedValue));
            txtOpeningDeposit.Text = _agentProduct.openingDeposit.ToString(); //String.Format("{0:#,##0}", _agentProduct.openingDeposit);
        }

        private void txtAccOperated_Leave(object sender, EventArgs e)
        {
            int individualCount = gvIndividualsList.Rows.Count;
            int operatorCount = (txtAccOperated.Text != "") ? Convert.ToInt32(txtAccOperated.Text.Trim()) : 0;
            if (operatorCount < 0)
            {
                MsgBox.ShowError("Number of operator cannot be negative.");
                txtAccOperated.Focus();
            }
            else
            {
                if (operatorCount > individualCount)
                {
                    MsgBox.ShowError("Number of operators is higher than number of individuals.");
                    txtAccOperated.Focus();
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbCustomerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!cb.Focused)
            {
                return;
            }
            UtilityServices.fillOwnerTypesByCisType(ref cmbOwnerType, Convert.ToInt32(cmbCustomerType.SelectedValue));
            cmbOwnerType.SelectedIndex = -1;
            cmbOwnerType.Text = "Select";
        }

        private void btnWebcam_Click(object sender, EventArgs e)
        {
            try
            {
                frmWebCam webCam = new frmWebCam();
                DialogResult dr = webCam.ShowDialog();

                if (dr == DialogResult.OK)
                {
                    ImageConverter converter = new ImageConverter();
                    Bitmap imgbitmap = (Bitmap)converter.ConvertFrom(webCam.getPhoto());
                    Image img = (Image)converter.ConvertFrom(webCam.getPhoto());
                    img = UtilityServices.getResizedImage(imgbitmap, CommonRules.imageSizeLimit, 100, "");
                    #region Image file length check
                    MemoryStream ms = new MemoryStream();
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    if (ms.Length > CommonRules.imageSizeLimit)
                    {
                        MsgBox.ShowError("Image file should be in " + CommonRules.imageSizeLimit / 1024 + " KB");
                        return;
                    }
                    #endregion
                    if (img != null)
                        pbConsumerPic.Image = img;
                    else
                        MsgBox.ShowError("Photo not taken.");
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError("Photo not taken.");
            }
        }

        private void txtOpeningDeposit_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtOpeningDeposit.Text))
            {
                txtOpeningDeposit.Text = "0";
            }
            //decimal openingDepositAmt = (txtOpeningDeposit.Text.Trim() != "") ? Convert.ToDecimal(txtOpeningDeposit.Text.Trim()) : 0;
            //decimal minimumAmt = _agentProduct.openingDeposit;
            //if (openingDepositAmt < minimumAmt)
            //{
            //    Message.showInformation("Minimum opening deposit for this product is " + minimumAmt + " TK.");
            //    txtOpeningDeposit.Focus();
            //}
        }

        private void frmConsumerApplication_FormClosing(object sender, FormClosingEventArgs e)
        {
            ValidationManager.ReleaseValidationData(this);
            this.Owner = null;
        }

        private void frmConsumerApplication_Deactivate(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.None;
        }

        private void txtOpeningDeposit_Enter(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtOpeningDeposit.Text))
            {
                txtOpeningDeposit.Text = "0";
            }
        }

        private void frmConsumerApplication_Shown(object sender, EventArgs e)
        {
            Application.DoEvents();
            txtCustomerTitle.Focus();
        }

        private void txtIndiFirstName_KeyPress(object sender, KeyPressEventArgs e)
        {
        }

        private void txtIndiFirstName_Load(object sender, EventArgs e)
        {

        }

        private void gvIndividualsList_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView senderGrid = (DataGridView)sender;
            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewCheckBoxColumn && e.RowIndex >= 0)
            {

                if (e.ColumnIndex == 3) //for mandatory operator
                {
                    _customerApplyDto.ownerInformations[e.RowIndex].mandatoryOperator = (bool?)senderGrid.Rows[e.RowIndex].Cells[3].Value == true;
                }
                if (e.ColumnIndex == 2) //for acc operator
                {
                    _customerApplyDto.ownerInformations[e.RowIndex].accountOperator = (bool?)senderGrid.Rows[e.RowIndex].Cells[2].Value == true;
                }
            }
        }
    }

    public class custIndividualData
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public List<BiometricTemplate> fingerDatas { get; set; }
    }

    public class CustomerIndividualGrid
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }

        private OwnerInformation _obj;

        public CustomerIndividualGrid(OwnerInformation obj)
        {
            _obj = obj;
        }

        public OwnerInformation GetModel()
        {
            return _obj;
        }
    }

    public class AgentProductsGrid
    {
        public string Product_Prefix { get; set; }
        public string Product_Title { get; set; }

        private AgentProduct _obj;

        public AgentProductsGrid(AgentProduct obj)
        {
            _obj = obj;
        }

        public AgentProduct GetModel()
        {
            return _obj;
        }
    }
}