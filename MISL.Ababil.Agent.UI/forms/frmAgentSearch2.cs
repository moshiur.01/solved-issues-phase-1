﻿using MetroFramework.Forms;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.UI.forms.ProgressUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.UI.forms
{
    public partial class frmAgentSearch2 : CustomForm
    {
        public frmAgentSearch2()
        {
            InitializeComponent();
            SetupComponent();
        }

        List<AgentInformation> agentInformationList = new List<AgentInformation>();
        private int _currentRowIndex;

        private void SetupComponent()
        {
            List<AgentTransactionStatus> dsAgentTransactionStatus = Enum.GetValues(typeof(AgentTransactionStatus)).Cast<AgentTransactionStatus>().ToList();
            cmbTransactionStatus.DataSource = dsAgentTransactionStatus;
            cmbTransactionStatus.SelectedIndex = -1;

            List<ApprovalStatus> dsApprovalStatus = Enum.GetValues(typeof(ApprovalStatus)).Cast<ApprovalStatus>().ToList();
            cmbApprovalStatus.DataSource = dsApprovalStatus;
            cmbApprovalStatus.SelectedIndex = -1;

            //dtpCreationDateFrom.Value = dtpCreationDateTo.Value = SessionInfo.currentDate;
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void Search()
        {
            ProgressUIManager.ShowProgress(this);

            try
            {
                AgentSearchDto agentSearchDto = FillAgentSearchDto();
                AgentServices agentServices = new AgentServices();

                agentInformationList = agentServices.AgentSearch(agentSearchDto);
                dgResults.DataSource = agentInformationList.Select(o => new AgentInformationGrid(o)
                {
                    Id = o.id,
                    Agent_Code = o.agentCode,
                    Business_Name = o.businessName,
                    Mobile_Number = o.mobileNumber,
                    Agent_Address = o.agentAddress.addressLineOne,
                    Creation_Date = UtilityServices.getDateFromLong(o.creationDate).ToString("dd-MM-yyyy").Replace("-", "/"),
                    Approval_Status = o.approvalStatus.ToString(),
                    Transaction_Status = o.transactionStatus.ToString()
                }).ToList();
            }
            catch
            {
                ProgressUIManager.CloseProgress();
            }
            ProgressUIManager.CloseProgress();
        }

        private AgentSearchDto FillAgentSearchDto()
        {
            AgentSearchDto agentSearchDto = new AgentSearchDto();
            agentSearchDto.agentCode = txtAgentCode.Text;
            agentSearchDto.businessName = txtBusinessName.Text;
            agentSearchDto.mobileNumber = txtMobileBumber.Text;
            //agentSearchDto.creationDateFrom = UtilityServices.GetLongDate(dtpCreationDateFrom.Value);
            //agentSearchDto.creationDateTo = UtilityServices.GetLongDate(dtpCreationDateTo.Value);

            //if (cmbTransactionStatus.SelectedIndex > -1)
            //{
            //    agentSearchDto.transactionStatus = (AgentTransactionStatus)cmbTransactionStatus.SelectedValue;
            //}
            //else
            //{
            //    agentSearchDto.transactionStatus = null;
            //}



            //if (cmbApprovalStatus.SelectedIndex > -1)
            //{
            //    agentSearchDto.approvalStatus = (ApprovalStatus)cmbTransactionStatus.SelectedValue;
            //}
            //else
            //{
            //    agentSearchDto.approvalStatus = null;
            //}

            return agentSearchDto;
        }

        private void Reset()
        {
            cmbTransactionStatus.SelectedIndex = -1;
            cmbApprovalStatus.SelectedIndex = -1;

            txtAgentCode.Text = txtBusinessName.Text = txtMobileBumber.Text = "";
            dgResults.DataSource = null;// .Rows.Clear();
            dgResults.Refresh();

            //dtpCreationDateFrom.Value = dtpCreationDateTo.Value = SessionInfo.currentDate;
        }

        public class AgentInformationGrid
        {
            public long Id { get; set; }
            public string Agent_Code { get; set; }
            public string Business_Name { get; set; }
            public string Mobile_Number { get; set; }

            public string Agent_Address { get; set; }
            public string Creation_Date { get; set; }
            public string Transaction_Status { get; set; }

            public string Approval_Status { get; set; }

            private AgentInformation _obj;

            public AgentInformationGrid(AgentInformation obj)
            {
                _obj = obj;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            if (dgResults.RowCount > 0)
            {
                AgentInformation agentInformation = null;
                if (_currentRowIndex > -1)
                {
                    agentInformation = agentInformationList[_currentRowIndex];
                }
                if (agentInformation != null)
                {
                    AgentServices services = new AgentServices();
                    try
                    {
                        agentInformation = services.getAgentInfoById(agentInformation.id.ToString());
                    }
                    catch (Exception ex)
                    {
                        Message.showError(ex.Message);
                    }
                    frmAgentCreation agentCreation = new frmAgentCreation(agentInformation, ActionType.update);
                    agentCreation.IsCalledByOtherForm = true;
                    agentCreation.ShowDialog();
                }
            }
        }

        private void dgResults_SelectionChanged(object sender, EventArgs e)
        {
            if (dgResults.CurrentRow != null) _currentRowIndex = dgResults.CurrentRow.Index;
        }
    }
}