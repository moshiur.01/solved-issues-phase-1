﻿using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;

namespace MISL.Ababil.Agent.UI.forms
{
    partial class frmMainApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMainApp));
            this.picUser = new System.Windows.Forms.PictureBox();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblLastLoginTime = new System.Windows.Forms.Label();
            this.picExit = new System.Windows.Forms.PictureBox();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblTitle2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picExit)).BeginInit();
            this.SuspendLayout();
            // 
            // picUser
            // 
            this.picUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picUser.Image = ((System.Drawing.Image)(resources.GetObject("picUser.Image")));
            this.picUser.Location = new System.Drawing.Point(82, 289);
            this.picUser.Name = "picUser";
            this.picUser.Size = new System.Drawing.Size(110, 116);
            this.picUser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picUser.TabIndex = 6;
            this.picUser.TabStop = false;
            this.picUser.Visible = false;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F);
            this.lblUserName.ForeColor = System.Drawing.Color.Black;
            this.lblUserName.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblUserName.Location = new System.Drawing.Point(198, 318);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(202, 37);
            this.lblUserName.TabIndex = 7;
            this.lblUserName.Text = "lblUserName";
            this.lblUserName.Visible = false;
            // 
            // lblLastLoginTime
            // 
            this.lblLastLoginTime.AutoSize = true;
            this.lblLastLoginTime.BackColor = System.Drawing.Color.Transparent;
            this.lblLastLoginTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblLastLoginTime.Location = new System.Drawing.Point(201, 356);
            this.lblLastLoginTime.Name = "lblLastLoginTime";
            this.lblLastLoginTime.Size = new System.Drawing.Size(102, 15);
            this.lblLastLoginTime.TabIndex = 8;
            this.lblLastLoginTime.Text = "lblLastLoginTime";
            this.lblLastLoginTime.Visible = false;
            // 
            // picExit
            // 
            this.picExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.picExit.BackColor = System.Drawing.Color.Transparent;
            this.picExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picExit.Image = global::MISL.Ababil.Agent.UI.Properties.Resources.path3347_2;
            this.picExit.Location = new System.Drawing.Point(1187, 619);
            this.picExit.Name = "picExit";
            this.picExit.Size = new System.Drawing.Size(64, 50);
            this.picExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picExit.TabIndex = 9;
            this.picExit.TabStop = false;
            this.picExit.Click += new System.EventHandler(this.picExit_Click);
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = this.lblTitle;
            this.customTitlebar1.ShowTitle = false;
            this.customTitlebar1.Size = new System.Drawing.Size(1276, 26);
            this.customTitlebar1.TabIndex = 21;
            this.customTitlebar1.TabStop = false;
            // 
            // lblTitle
            // 
            this.lblTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblTitle.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(11, 3);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(173, 23);
            this.lblTitle.TabIndex = 25;
            this.lblTitle.Text = "Ababil Agent Banking";
            // 
            // lblTitle2
            // 
            this.lblTitle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblTitle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle2.ForeColor = System.Drawing.Color.White;
            this.lblTitle2.Location = new System.Drawing.Point(11, 3);
            this.lblTitle2.Name = "lblTitle2";
            this.lblTitle2.Size = new System.Drawing.Size(173, 23);
            this.lblTitle2.TabIndex = 25;
            this.lblTitle2.Text = "Ababil Agent Banking";
            // 
            // frmMainApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MISL.Ababil.Agent.UI.Properties.Resources.wp_0001;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1276, 692);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.picExit);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.lblLastLoginTime);
            this.Controls.Add(this.picUser);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.lblTitle2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMainApp";
            this.Text = "Ababil Agent Banking";
            this.TransparencyKey = System.Drawing.Color.Blue;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMainApp_FormClosing);
            this.Load += new System.EventHandler(this.frmMainApp_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picExit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        //@@@//private System.Windows.Forms.MenuStrip menuStrip1;
        //~//private System.Windows.Forms.ToolStripMenuItem administratorToolStripMenuItem;
        //~//private System.Windows.Forms.ToolStripMenuItem createAgentToolStripMenuItem;
        //~//private System.Windows.Forms.ToolStripMenuItem agentToolStripMenuItem;
        //~//private System.Windows.Forms.ToolStripMenuItem createIndividualToolStripMenuItem;
        //~//private System.Windows.Forms.ToolStripMenuItem createCustomerToolStripMenuItem;
        //~//private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        //~//private System.Windows.Forms.ToolStripMenuItem createUserToolStripMenuItem1;
        //~//private System.Windows.Forms.ToolStripMenuItem createSubAgentToolStripMenuItem1;
        //~//private System.Windows.Forms.ToolStripMenuItem createConsumerToolStripMenuItem;
        //~//private System.Windows.Forms.ToolStripMenuItem cashDepositToolStripMenuItem;
        //~//private System.Windows.Forms.ToolStripMenuItem cashWithdrawToolStripMenuItem1;
        //~//private System.Windows.Forms.ToolStripMenuItem fundTransferToolStripMenuItem1;
        //~//private System.Windows.Forms.ToolStripMenuItem balanceInquiryToolStripMenuItem1;
        //~//private System.Windows.Forms.ToolStripMenuItem miniStatementToolStripMenuItem1;
        //~//private System.Windows.Forms.ToolStripMenuItem viewAgentToolStripMenuItem;
        //~//private System.Windows.Forms.ToolStripMenuItem consumerApplicationToolStripMenuItem;
        //~//private System.Windows.Forms.ToolStripMenuItem consumerCreationToolStripMenuItem;
        //~//private System.Windows.Forms.ToolStripMenuItem consumerApplicationSearchToolStripMenuItem;
        //~//private System.Windows.Forms.ToolStripMenuItem transactionProfileToolStripMenuItem;
        //~//private System.Windows.Forms.ToolStripMenuItem kYCToolStripMenuItem;
        //~//private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem;
        //~//private System.Windows.Forms.ToolStripMenuItem reportFormToolStripMenuItem;
        //~//private System.Windows.Forms.ToolStripMenuItem consumerApplicationSearchToolStripMenuItem1;
        //~//private System.Windows.Forms.ToolStripMenuItem remittanceAgentRequestToolStripMenuItem;
        //~//private System.Windows.Forms.ToolStripMenuItem transactionRecordSearchToolStripMenuItem;
        //~//private System.Windows.Forms.ToolStripMenuItem remittanceAdminToolStripMenuItem;
        //~//private System.Windows.Forms.ToolStripMenuItem minimizeToolStripMenuItem;      
        //~//private System.Windows.Forms.ToolStripMenuItem searchAgentsToolStripMenuItem;
        //~//private System.Windows.Forms.ToolStripMenuItem agentToolStripMenuItem1;
        //~//private System.Windows.Forms.ToolStripMenuItem branchUserCreationToolStripMenuItem;
        //~//private System.Windows.Forms.ToolStripMenuItem userNameToolStripMenuItem;
        //~//private System.Windows.Forms.ToolStripMenuItem lastLoginToolStripMenuItem;
        //~//private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        //~//private System.Windows.Forms.ToolStripMenuItem setFingerprintToolStripMenuItem;

        private System.Windows.Forms.PictureBox picUser;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblLastLoginTime;
        private System.Windows.Forms.PictureBox picExit;
        private Agent.CustomControls.CustomTitlebar customTitlebar1;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Button button1;
        //~//private Dashboard.ucDashboard _dashboard;
        private System.Windows.Forms.Label lblTitle2;
    }
}