﻿using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Module.Common.Exporter;

namespace MISL.Ababil.Agent.UI.forms
{
    partial class frmAgentIncome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint3 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(60D, 1D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint4 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(40D, 2D);
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblAgent = new System.Windows.Forms.Label();
            this.lblOutlet = new System.Windows.Forms.Label();
            this.lblIncomeFrom = new System.Windows.Forms.Label();
            this.lblIncomeDateForm = new System.Windows.Forms.Label();
            this.IncomeDateTo = new System.Windows.Forms.Label();
            this.lblPostingStatus = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.lblPostingDateFrom = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pieChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.dgvTotal = new System.Windows.Forms.DataGridView();
            this.dgvResult = new System.Windows.Forms.DataGridView();
            this.lblTotalValue = new System.Windows.Forms.Label();
            this.llblBalance = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.lblResultCount = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblFilter = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSummaryReport = new System.Windows.Forms.Button();
            this.txtUser = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.cmbAgent = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbIncomeFrom = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbPostingStatus = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbOutlet = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.dtpPostingDateTo = new MISL.Ababil.Agent.UI.forms.CustomControls.CustomDateTimePicker();
            this.dtpIncomeDateTo = new MISL.Ababil.Agent.UI.forms.CustomControls.CustomDateTimePicker();
            this.dtpIncomeDateFrom = new MISL.Ababil.Agent.UI.forms.CustomControls.CustomDateTimePicker();
            this.dtpPostingDateFrom = new MISL.Ababil.Agent.UI.forms.CustomControls.CustomDateTimePicker();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.btnDataVisualization = new System.Windows.Forms.Button();
            this.customDataGridViewHeader = new MISL.Ababil.Agent.Module.Common.Exporter.CustomDataGridViewHeader();
            ((System.ComponentModel.ISupportInitialize)(this.pieChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResult)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblAgent
            // 
            this.lblAgent.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblAgent.AutoSize = true;
            this.lblAgent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblAgent.ForeColor = System.Drawing.Color.White;
            this.lblAgent.Location = new System.Drawing.Point(47, 31);
            this.lblAgent.Name = "lblAgent";
            this.lblAgent.Size = new System.Drawing.Size(35, 13);
            this.lblAgent.TabIndex = 1;
            this.lblAgent.Text = "Agent";
            // 
            // lblOutlet
            // 
            this.lblOutlet.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblOutlet.AutoSize = true;
            this.lblOutlet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblOutlet.ForeColor = System.Drawing.Color.White;
            this.lblOutlet.Location = new System.Drawing.Point(46, 67);
            this.lblOutlet.Name = "lblOutlet";
            this.lblOutlet.Size = new System.Drawing.Size(35, 13);
            this.lblOutlet.TabIndex = 3;
            this.lblOutlet.Text = "Outlet";
            // 
            // lblIncomeFrom
            // 
            this.lblIncomeFrom.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblIncomeFrom.AutoSize = true;
            this.lblIncomeFrom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblIncomeFrom.ForeColor = System.Drawing.Color.White;
            this.lblIncomeFrom.Location = new System.Drawing.Point(359, 82);
            this.lblIncomeFrom.Name = "lblIncomeFrom";
            this.lblIncomeFrom.Size = new System.Drawing.Size(68, 13);
            this.lblIncomeFrom.TabIndex = 1;
            this.lblIncomeFrom.Text = "Income From";
            this.lblIncomeFrom.Visible = false;
            // 
            // lblIncomeDateForm
            // 
            this.lblIncomeDateForm.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblIncomeDateForm.AutoSize = true;
            this.lblIncomeDateForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblIncomeDateForm.ForeColor = System.Drawing.Color.White;
            this.lblIncomeDateForm.Location = new System.Drawing.Point(417, 33);
            this.lblIncomeDateForm.Name = "lblIncomeDateForm";
            this.lblIncomeDateForm.Size = new System.Drawing.Size(94, 13);
            this.lblIncomeDateForm.TabIndex = 5;
            this.lblIncomeDateForm.Text = "Income Date From";
            // 
            // IncomeDateTo
            // 
            this.IncomeDateTo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.IncomeDateTo.AutoSize = true;
            this.IncomeDateTo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.IncomeDateTo.ForeColor = System.Drawing.Color.White;
            this.IncomeDateTo.Location = new System.Drawing.Point(766, 34);
            this.IncomeDateTo.Name = "IncomeDateTo";
            this.IncomeDateTo.Size = new System.Drawing.Size(46, 13);
            this.IncomeDateTo.TabIndex = 7;
            this.IncomeDateTo.Text = "Date To";
            // 
            // lblPostingStatus
            // 
            this.lblPostingStatus.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblPostingStatus.AutoSize = true;
            this.lblPostingStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblPostingStatus.ForeColor = System.Drawing.Color.White;
            this.lblPostingStatus.Location = new System.Drawing.Point(436, 68);
            this.lblPostingStatus.Name = "lblPostingStatus";
            this.lblPostingStatus.Size = new System.Drawing.Size(75, 13);
            this.lblPostingStatus.TabIndex = 9;
            this.lblPostingStatus.Text = "Posting Status";
            // 
            // lblUser
            // 
            this.lblUser.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblUser.AutoSize = true;
            this.lblUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblUser.ForeColor = System.Drawing.Color.White;
            this.lblUser.Location = new System.Drawing.Point(398, 168);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(29, 13);
            this.lblUser.TabIndex = 1;
            this.lblUser.Text = "User";
            this.lblUser.Visible = false;
            // 
            // lblPostingDateFrom
            // 
            this.lblPostingDateFrom.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblPostingDateFrom.AutoSize = true;
            this.lblPostingDateFrom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblPostingDateFrom.ForeColor = System.Drawing.Color.White;
            this.lblPostingDateFrom.Location = new System.Drawing.Point(333, 107);
            this.lblPostingDateFrom.Name = "lblPostingDateFrom";
            this.lblPostingDateFrom.Size = new System.Drawing.Size(94, 13);
            this.lblPostingDateFrom.TabIndex = 1;
            this.lblPostingDateFrom.Text = "Posting Date From";
            this.lblPostingDateFrom.Visible = false;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(343, 135);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Posting Date To";
            this.label9.Visible = false;
            // 
            // pieChart
            // 
            this.pieChart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            chartArea2.Name = "ChartArea1";
            this.pieChart.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.pieChart.Legends.Add(legend2);
            this.pieChart.Location = new System.Drawing.Point(7, 437);
            this.pieChart.Name = "pieChart";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series2.CustomProperties = "PieLineColor=Gray, PieLabelStyle=Outside";
            series2.Label = "#VALY{#.##}";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            series2.Points.Add(dataPoint3);
            series2.Points.Add(dataPoint4);
            this.pieChart.Series.Add(series2);
            this.pieChart.Size = new System.Drawing.Size(338, 156);
            this.pieChart.TabIndex = 19;
            this.pieChart.Text = "chart1";
            // 
            // dgvTotal
            // 
            this.dgvTotal.AllowUserToAddRows = false;
            this.dgvTotal.AllowUserToResizeRows = false;
            this.dgvTotal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvTotal.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTotal.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.dgvTotal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvTotal.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(190)))), ((int)(((byte)(226)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.HotTrack;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTotal.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvTotal.ColumnHeadersHeight = 28;
            this.dgvTotal.EnableHeadersVisualStyles = false;
            this.dgvTotal.GridColor = System.Drawing.Color.Silver;
            this.dgvTotal.Location = new System.Drawing.Point(7, 105);
            this.dgvTotal.MultiSelect = false;
            this.dgvTotal.Name = "dgvTotal";
            this.dgvTotal.ReadOnly = true;
            this.dgvTotal.RowHeadersVisible = false;
            this.dgvTotal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTotal.Size = new System.Drawing.Size(338, 259);
            this.dgvTotal.TabIndex = 14;
            this.dgvTotal.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTotal_CellClick_1);
            this.dgvTotal.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTotal_CellContentClick);
            // 
            // dgvResult
            // 
            this.dgvResult.AllowUserToResizeRows = false;
            this.dgvResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvResult.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvResult.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.dgvResult.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvResult.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(190)))), ((int)(((byte)(226)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.HotTrack;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvResult.ColumnHeadersHeight = 28;
            this.dgvResult.EnableHeadersVisualStyles = false;
            this.dgvResult.GridColor = System.Drawing.Color.Silver;
            this.dgvResult.Location = new System.Drawing.Point(351, 133);
            this.dgvResult.MultiSelect = false;
            this.dgvResult.Name = "dgvResult";
            this.dgvResult.ReadOnly = true;
            this.dgvResult.RowHeadersVisible = false;
            this.dgvResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvResult.Size = new System.Drawing.Size(751, 459);
            this.dgvResult.TabIndex = 15;
            // 
            // lblTotalValue
            // 
            this.lblTotalValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTotalValue.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalValue.Location = new System.Drawing.Point(109, 406);
            this.lblTotalValue.Name = "lblTotalValue";
            this.lblTotalValue.Size = new System.Drawing.Size(209, 28);
            this.lblTotalValue.TabIndex = 17;
            this.lblTotalValue.Text = "0.00";
            this.lblTotalValue.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // llblBalance
            // 
            this.llblBalance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.llblBalance.AutoSize = true;
            this.llblBalance.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.llblBalance.Location = new System.Drawing.Point(4, 412);
            this.llblBalance.Name = "llblBalance";
            this.llblBalance.Size = new System.Drawing.Size(106, 21);
            this.llblBalance.TabIndex = 16;
            this.llblBalance.Text = "Balance Total :";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnClose.BackColor = System.Drawing.SystemColors.Control;
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Image = global::MISL.Ababil.Agent.UI.Properties.Resources.Delete_16__1_;
            this.btnClose.Location = new System.Drawing.Point(955, 60);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(88, 26);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "Close";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnReset
            // 
            this.btnReset.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnReset.BackColor = System.Drawing.SystemColors.Control;
            this.btnReset.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.Image = global::MISL.Ababil.Agent.UI.Properties.Resources.Broom_16;
            this.btnReset.Location = new System.Drawing.Point(863, 60);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(88, 26);
            this.btnReset.TabIndex = 12;
            this.btnReset.Text = "Reset";
            this.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSearch.BackColor = System.Drawing.SystemColors.Control;
            this.btnSearch.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Image = global::MISL.Ababil.Agent.UI.Properties.Resources.Search_16__1_;
            this.btnSearch.Location = new System.Drawing.Point(769, 60);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(89, 26);
            this.btnSearch.TabIndex = 11;
            this.btnSearch.Text = "Search";
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lblResultCount
            // 
            this.lblResultCount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblResultCount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblResultCount.ForeColor = System.Drawing.Color.White;
            this.lblResultCount.Location = new System.Drawing.Point(920, 0);
            this.lblResultCount.Name = "lblResultCount";
            this.lblResultCount.Size = new System.Drawing.Size(181, 24);
            this.lblResultCount.TabIndex = 1;
            this.lblResultCount.Text = "   Result : 0 Item(s)";
            this.lblResultCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 620);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1109, 1);
            this.panel1.TabIndex = 164;
            // 
            // lblFilter
            // 
            this.lblFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFilter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblFilter.ForeColor = System.Drawing.Color.White;
            this.lblFilter.Location = new System.Drawing.Point(353, 0);
            this.lblFilter.Name = "lblFilter";
            this.lblFilter.Size = new System.Drawing.Size(539, 24);
            this.lblFilter.TabIndex = 0;
            this.lblFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.panel2.Controls.Add(this.lblResultCount);
            this.panel2.Controls.Add(this.lblFilter);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 596);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1109, 24);
            this.panel2.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label1.Location = new System.Drawing.Point(312, 412);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 21);
            this.label1.TabIndex = 18;
            this.label1.Text = "TK";
            // 
            // btnSummaryReport
            // 
            this.btnSummaryReport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSummaryReport.BackColor = System.Drawing.SystemColors.Control;
            this.btnSummaryReport.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnSummaryReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSummaryReport.Image = global::MISL.Ababil.Agent.UI.Properties.Resources.Document_16;
            this.btnSummaryReport.Location = new System.Drawing.Point(241, 370);
            this.btnSummaryReport.Name = "btnSummaryReport";
            this.btnSummaryReport.Size = new System.Drawing.Size(104, 26);
            this.btnSummaryReport.TabIndex = 165;
            this.btnSummaryReport.Text = "View Report";
            this.btnSummaryReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSummaryReport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSummaryReport.UseVisualStyleBackColor = false;
            this.btnSummaryReport.Click += new System.EventHandler(this.btnSummaryReport_Click);
            // 
            // txtUser
            // 
            this.txtUser.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.txtUser.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtUser.InputScopeAllowEmpty = false;
            this.txtUser.InputScopeCustomString = null;
            this.txtUser.IsValid = null;
            this.txtUser.Location = new System.Drawing.Point(804, 354);
            this.txtUser.MaxLength = 32767;
            this.txtUser.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtUser.Name = "txtUser";
            this.txtUser.PromptText = "(Type here...)";
            this.txtUser.ReadOnly = true;
            this.txtUser.ShowMandatoryMark = false;
            this.txtUser.Size = new System.Drawing.Size(214, 21);
            this.txtUser.TabIndex = 46;
            this.txtUser.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtUser.UpperCaseOnly = false;
            this.txtUser.ValidationErrorMessage = "Validation Error!";
            this.txtUser.Visible = false;
            // 
            // cmbAgent
            // 
            this.cmbAgent.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbAgent.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cmbAgent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAgent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbAgent.FormattingEnabled = true;
            this.cmbAgent.InputScopeAllowEmpty = false;
            this.cmbAgent.IsValid = null;
            this.cmbAgent.Location = new System.Drawing.Point(93, 28);
            this.cmbAgent.Name = "cmbAgent";
            this.cmbAgent.PromptText = "(Select)";
            this.cmbAgent.ReadOnly = false;
            this.cmbAgent.ShowMandatoryMark = false;
            this.cmbAgent.Size = new System.Drawing.Size(273, 21);
            this.cmbAgent.TabIndex = 2;
            this.cmbAgent.ValidationErrorMessage = "Validation Error!";
            this.cmbAgent.SelectedIndexChanged += new System.EventHandler(this.cmbAgent_SelectedIndexChanged);
            // 
            // cmbIncomeFrom
            // 
            this.cmbIncomeFrom.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbIncomeFrom.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cmbIncomeFrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbIncomeFrom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbIncomeFrom.FormattingEnabled = true;
            this.cmbIncomeFrom.InputScopeAllowEmpty = false;
            this.cmbIncomeFrom.IsValid = null;
            this.cmbIncomeFrom.Location = new System.Drawing.Point(-217, 101);
            this.cmbIncomeFrom.Name = "cmbIncomeFrom";
            this.cmbIncomeFrom.PromptText = "(Select)";
            this.cmbIncomeFrom.ReadOnly = false;
            this.cmbIncomeFrom.ShowMandatoryMark = false;
            this.cmbIncomeFrom.Size = new System.Drawing.Size(220, 21);
            this.cmbIncomeFrom.TabIndex = 14;
            this.cmbIncomeFrom.ValidationErrorMessage = "Validation Error!";
            this.cmbIncomeFrom.Visible = false;
            // 
            // cmbPostingStatus
            // 
            this.cmbPostingStatus.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbPostingStatus.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cmbPostingStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPostingStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbPostingStatus.FormattingEnabled = true;
            this.cmbPostingStatus.InputScopeAllowEmpty = false;
            this.cmbPostingStatus.IsValid = null;
            this.cmbPostingStatus.Location = new System.Drawing.Point(522, 63);
            this.cmbPostingStatus.Name = "cmbPostingStatus";
            this.cmbPostingStatus.PromptText = "(Select)";
            this.cmbPostingStatus.ReadOnly = false;
            this.cmbPostingStatus.ShowMandatoryMark = false;
            this.cmbPostingStatus.Size = new System.Drawing.Size(214, 21);
            this.cmbPostingStatus.TabIndex = 10;
            this.cmbPostingStatus.ValidationErrorMessage = "Validation Error!";
            // 
            // cmbOutlet
            // 
            this.cmbOutlet.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbOutlet.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cmbOutlet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOutlet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbOutlet.FormattingEnabled = true;
            this.cmbOutlet.InputScopeAllowEmpty = false;
            this.cmbOutlet.IsValid = null;
            this.cmbOutlet.Location = new System.Drawing.Point(93, 63);
            this.cmbOutlet.Name = "cmbOutlet";
            this.cmbOutlet.PromptText = "(Select)";
            this.cmbOutlet.ReadOnly = false;
            this.cmbOutlet.ShowMandatoryMark = false;
            this.cmbOutlet.Size = new System.Drawing.Size(273, 21);
            this.cmbOutlet.TabIndex = 4;
            this.cmbOutlet.ValidationErrorMessage = "Validation Error!";
            // 
            // dtpPostingDateTo
            // 
            this.dtpPostingDateTo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtpPostingDateTo.Date = "04/02/2016";
            this.dtpPostingDateTo.Location = new System.Drawing.Point(439, 129);
            this.dtpPostingDateTo.MaximumSize = new System.Drawing.Size(400, 25);
            this.dtpPostingDateTo.MinimumSize = new System.Drawing.Size(60, 25);
            this.dtpPostingDateTo.Name = "dtpPostingDateTo";
            this.dtpPostingDateTo.PresetServerDate = true;
            this.dtpPostingDateTo.Size = new System.Drawing.Size(219, 25);
            this.dtpPostingDateTo.TabIndex = 100;
            this.dtpPostingDateTo.Value = new System.DateTime(2016, 2, 4, 15, 43, 35, 296);
            this.dtpPostingDateTo.Visible = false;
            // 
            // dtpIncomeDateTo
            // 
            this.dtpIncomeDateTo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtpIncomeDateTo.Date = "04/02/2016";
            this.dtpIncomeDateTo.Location = new System.Drawing.Point(824, 27);
            this.dtpIncomeDateTo.MaximumSize = new System.Drawing.Size(400, 25);
            this.dtpIncomeDateTo.MinimumSize = new System.Drawing.Size(60, 25);
            this.dtpIncomeDateTo.Name = "dtpIncomeDateTo";
            this.dtpIncomeDateTo.PresetServerDate = true;
            this.dtpIncomeDateTo.Size = new System.Drawing.Size(219, 25);
            this.dtpIncomeDateTo.TabIndex = 8;
            this.dtpIncomeDateTo.Value = new System.DateTime(2016, 2, 4, 15, 43, 34, 714);
            // 
            // dtpIncomeDateFrom
            // 
            this.dtpIncomeDateFrom.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtpIncomeDateFrom.Date = "04/02/2016";
            this.dtpIncomeDateFrom.Location = new System.Drawing.Point(522, 27);
            this.dtpIncomeDateFrom.MaximumSize = new System.Drawing.Size(400, 25);
            this.dtpIncomeDateFrom.MinimumSize = new System.Drawing.Size(60, 25);
            this.dtpIncomeDateFrom.Name = "dtpIncomeDateFrom";
            this.dtpIncomeDateFrom.PresetServerDate = true;
            this.dtpIncomeDateFrom.Size = new System.Drawing.Size(214, 25);
            this.dtpIncomeDateFrom.TabIndex = 6;
            this.dtpIncomeDateFrom.Value = new System.DateTime(2016, 2, 4, 15, 43, 33, 500);
            // 
            // dtpPostingDateFrom
            // 
            this.dtpPostingDateFrom.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtpPostingDateFrom.Date = "04/02/2016";
            this.dtpPostingDateFrom.Location = new System.Drawing.Point(439, 101);
            this.dtpPostingDateFrom.MaximumSize = new System.Drawing.Size(400, 25);
            this.dtpPostingDateFrom.MinimumSize = new System.Drawing.Size(60, 25);
            this.dtpPostingDateFrom.Name = "dtpPostingDateFrom";
            this.dtpPostingDateFrom.PresetServerDate = true;
            this.dtpPostingDateFrom.Size = new System.Drawing.Size(219, 25);
            this.dtpPostingDateFrom.TabIndex = 97;
            this.dtpPostingDateFrom.Value = new System.DateTime(2016, 2, 4, 15, 43, 32, 624);
            this.dtpPostingDateFrom.Visible = false;
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(1109, 100);
            this.customTitlebar1.TabIndex = 0;
            // 
            // btnDataVisualization
            // 
            this.btnDataVisualization.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDataVisualization.BackColor = System.Drawing.SystemColors.Control;
            this.btnDataVisualization.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnDataVisualization.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDataVisualization.Image = global::MISL.Ababil.Agent.UI.Properties.Resources.Document_16;
            this.btnDataVisualization.Location = new System.Drawing.Point(131, 370);
            this.btnDataVisualization.Name = "btnDataVisualization";
            this.btnDataVisualization.Size = new System.Drawing.Size(104, 26);
            this.btnDataVisualization.TabIndex = 166;
            this.btnDataVisualization.Text = "Visualize Data";
            this.btnDataVisualization.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDataVisualization.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDataVisualization.UseVisualStyleBackColor = false;
            this.btnDataVisualization.Visible = false;
            this.btnDataVisualization.Click += new System.EventHandler(this.btnDataVisualization_Click);
            // 
            // customDataGridViewHeader
            // 
            this.customDataGridViewHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(123)))), ((int)(((byte)(208)))));
            this.customDataGridViewHeader.ExportReportSubtitleOne = "Subtitle One";
            this.customDataGridViewHeader.ExportReportSubtitleThree = "Subtitle Three";
            this.customDataGridViewHeader.ExportReportSubtitleTwo = "Subtitle Two";
            this.customDataGridViewHeader.ExportReportTitle = "Title";
            this.customDataGridViewHeader.Filter = this.dgvResult;
            this.customDataGridViewHeader.HeaderText = "Agent Income Information";
            this.customDataGridViewHeader.Location = new System.Drawing.Point(352, 101);
            this.customDataGridViewHeader.Name = "customDataGridViewHeader";
            this.customDataGridViewHeader.Size = new System.Drawing.Size(749, 28);
            this.customDataGridViewHeader.TabIndex = 167;
            // 
            // frmAgentIncome
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1109, 621);
            this.Controls.Add(this.cmbIncomeFrom);
            this.Controls.Add(this.cmbOutlet);
            this.Controls.Add(this.cmbPostingStatus);
            this.Controls.Add(this.cmbAgent);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.customDataGridViewHeader);
            this.Controls.Add(this.btnDataVisualization);
            this.Controls.Add(this.btnSummaryReport);
            this.Controls.Add(this.dgvTotal);
            this.Controls.Add(this.dtpIncomeDateTo);
            this.Controls.Add(this.dtpIncomeDateFrom);
            this.Controls.Add(this.pieChart);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.lblAgent);
            this.Controls.Add(this.lblOutlet);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.llblBalance);
            this.Controls.Add(this.lblTotalValue);
            this.Controls.Add(this.lblIncomeDateForm);
            this.Controls.Add(this.IncomeDateTo);
            this.Controls.Add(this.lblPostingStatus);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dgvResult);
            this.Controls.Add(this.dtpPostingDateTo);
            this.Controls.Add(this.dtpPostingDateFrom);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.lblIncomeFrom);
            this.Controls.Add(this.lblPostingDateFrom);
            this.Controls.Add(this.label9);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmAgentIncome";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agent Income";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmAgentIncome_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pieChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResult)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblAgent;
        private System.Windows.Forms.Label lblPostingStatus;
        private System.Windows.Forms.Label IncomeDateTo;
        private System.Windows.Forms.Label lblIncomeDateForm;
        private System.Windows.Forms.Label lblIncomeFrom;
        private System.Windows.Forms.Label lblOutlet;
        private Agent.CustomControls.CustomComboBoxDropDownList cmbIncomeFrom;
        private Agent.CustomControls.CustomComboBoxDropDownList cmbPostingStatus;
        private Agent.CustomControls.CustomComboBoxDropDownList cmbAgent;
        private Agent.CustomControls.CustomComboBoxDropDownList cmbOutlet;
        private Agent.CustomControls.CustomTextBox txtUser;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblPostingDateFrom;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView dgvResult;
        private System.Windows.Forms.DataVisualization.Charting.Chart pieChart;
        private System.Windows.Forms.Label llblBalance;
        private System.Windows.Forms.Label lblTotalValue;
        private System.Windows.Forms.DataGridView dgvTotal;
        private CustomTitlebar customTitlebar1;
        private CustomControls.CustomDateTimePicker dtpPostingDateTo;
        private CustomControls.CustomDateTimePicker dtpIncomeDateTo;
        private CustomControls.CustomDateTimePicker dtpIncomeDateFrom;
        private CustomControls.CustomDateTimePicker dtpPostingDateFrom;
        private System.Windows.Forms.Label lblResultCount;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblFilter;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSummaryReport;
        private System.Windows.Forms.Button btnDataVisualization;
        private CustomDataGridViewHeader customDataGridViewHeader;
    }
}