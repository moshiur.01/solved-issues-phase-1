﻿using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.UI.forms
{
    partial class frmOutletUserCashLimitInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClose = new System.Windows.Forms.Button();
            this.btnShow = new System.Windows.Forms.Button();
            this.lblSubAgent = new System.Windows.Forms.Label();
            this.lblAgent = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblTotalLimitHeader = new System.Windows.Forms.Label();
            this.lblTotalCashinHandHeader = new System.Windows.Forms.Label();
            this.lblTotalLimitValue = new System.Windows.Forms.Label();
            this.lblTotalCashinHandValue = new System.Windows.Forms.Label();
            this.lblRemainingLimitValue = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbAgentName = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.cmbSubAgnetName = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.mandatoryMark1 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.mandatoryMark2 = new MISL.Ababil.Agent.UI.forms.CustomControls.MandatoryMark();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(366, 268);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(90, 29);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnShow
            // 
            this.btnShow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnShow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShow.ForeColor = System.Drawing.Color.White;
            this.btnShow.Location = new System.Drawing.Point(250, 268);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(110, 29);
            this.btnShow.TabIndex = 4;
            this.btnShow.Text = "Show Details";
            this.btnShow.UseVisualStyleBackColor = false;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // lblSubAgent
            // 
            this.lblSubAgent.AutoSize = true;
            this.lblSubAgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lblSubAgent.Location = new System.Drawing.Point(102, 131);
            this.lblSubAgent.Margin = new System.Windows.Forms.Padding(3, 13, 3, 3);
            this.lblSubAgent.Name = "lblSubAgent";
            this.lblSubAgent.Size = new System.Drawing.Size(54, 17);
            this.lblSubAgent.TabIndex = 2;
            this.lblSubAgent.Text = "Outlet :";
            // 
            // lblAgent
            // 
            this.lblAgent.AutoSize = true;
            this.lblAgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lblAgent.Location = new System.Drawing.Point(102, 99);
            this.lblAgent.Margin = new System.Windows.Forms.Padding(3, 13, 3, 3);
            this.lblAgent.Name = "lblAgent";
            this.lblAgent.Size = new System.Drawing.Size(53, 17);
            this.lblAgent.TabIndex = 0;
            this.lblAgent.Text = "Agent :";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(24, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(458, 25);
            this.label4.TabIndex = 7;
            this.label4.Text = "Outlet User Cash limit Info";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTotalLimitHeader
            // 
            this.lblTotalLimitHeader.AutoSize = true;
            this.lblTotalLimitHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lblTotalLimitHeader.Location = new System.Drawing.Point(136, 174);
            this.lblTotalLimitHeader.Margin = new System.Windows.Forms.Padding(3, 13, 3, 3);
            this.lblTotalLimitHeader.Name = "lblTotalLimitHeader";
            this.lblTotalLimitHeader.Size = new System.Drawing.Size(81, 17);
            this.lblTotalLimitHeader.TabIndex = 8;
            this.lblTotalLimitHeader.Text = "Total Limit :";
            // 
            // lblTotalCashinHandHeader
            // 
            this.lblTotalCashinHandHeader.AutoSize = true;
            this.lblTotalCashinHandHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lblTotalCashinHandHeader.Location = new System.Drawing.Point(80, 200);
            this.lblTotalCashinHandHeader.Margin = new System.Windows.Forms.Padding(3, 13, 3, 3);
            this.lblTotalCashinHandHeader.Name = "lblTotalCashinHandHeader";
            this.lblTotalCashinHandHeader.Size = new System.Drawing.Size(137, 17);
            this.lblTotalCashinHandHeader.TabIndex = 9;
            this.lblTotalCashinHandHeader.Text = "Total Cash in Hand :";
            // 
            // lblTotalLimitValue
            // 
            this.lblTotalLimitValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold);
            this.lblTotalLimitValue.Location = new System.Drawing.Point(225, 176);
            this.lblTotalLimitValue.Margin = new System.Windows.Forms.Padding(3, 13, 3, 3);
            this.lblTotalLimitValue.Name = "lblTotalLimitValue";
            this.lblTotalLimitValue.Size = new System.Drawing.Size(170, 17);
            this.lblTotalLimitValue.TabIndex = 10;
            this.lblTotalLimitValue.Text = "00000.00  BDT";
            this.lblTotalLimitValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTotalCashinHandValue
            // 
            this.lblTotalCashinHandValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold);
            this.lblTotalCashinHandValue.Location = new System.Drawing.Point(225, 202);
            this.lblTotalCashinHandValue.Margin = new System.Windows.Forms.Padding(3, 13, 3, 3);
            this.lblTotalCashinHandValue.Name = "lblTotalCashinHandValue";
            this.lblTotalCashinHandValue.Size = new System.Drawing.Size(170, 17);
            this.lblTotalCashinHandValue.TabIndex = 11;
            this.lblTotalCashinHandValue.Text = "00000.00  BDT";
            this.lblTotalCashinHandValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblRemainingLimitValue
            // 
            this.lblRemainingLimitValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold);
            this.lblRemainingLimitValue.Location = new System.Drawing.Point(225, 229);
            this.lblRemainingLimitValue.Margin = new System.Windows.Forms.Padding(3, 13, 3, 3);
            this.lblRemainingLimitValue.Name = "lblRemainingLimitValue";
            this.lblRemainingLimitValue.Size = new System.Drawing.Size(170, 17);
            this.lblRemainingLimitValue.TabIndex = 13;
            this.lblRemainingLimitValue.Text = "00000.00  BDT";
            this.lblRemainingLimitValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.label2.Location = new System.Drawing.Point(101, 227);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 13, 3, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 17);
            this.label2.TabIndex = 12;
            this.label2.Text = "Remaining Limit :";
            // 
            // cmbAgentName
            // 
            this.cmbAgentName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAgentName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbAgentName.FormattingEnabled = true;
            this.cmbAgentName.InputScopeAllowEmpty = false;
            this.cmbAgentName.IsValid = null;
            this.cmbAgentName.Location = new System.Drawing.Point(161, 97);
            this.cmbAgentName.Name = "cmbAgentName";
            this.cmbAgentName.PromptText = "(Select)";
            this.cmbAgentName.ReadOnly = false;
            this.cmbAgentName.ShowMandatoryMark = false;
            this.cmbAgentName.Size = new System.Drawing.Size(233, 21);
            this.cmbAgentName.TabIndex = 14;
            this.cmbAgentName.ValidationErrorMessage = "Validation Error!";
            this.cmbAgentName.SelectedIndexChanged += new System.EventHandler(this.cmbAgentName_SelectedIndexChanged);
            // 
            // cmbSubAgnetName
            // 
            this.cmbSubAgnetName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSubAgnetName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbSubAgnetName.FormattingEnabled = true;
            this.cmbSubAgnetName.InputScopeAllowEmpty = false;
            this.cmbSubAgnetName.IsValid = null;
            this.cmbSubAgnetName.Location = new System.Drawing.Point(161, 129);
            this.cmbSubAgnetName.Name = "cmbSubAgnetName";
            this.cmbSubAgnetName.PromptText = "(Select)";
            this.cmbSubAgnetName.ReadOnly = false;
            this.cmbSubAgnetName.ShowMandatoryMark = false;
            this.cmbSubAgnetName.Size = new System.Drawing.Size(233, 21);
            this.cmbSubAgnetName.TabIndex = 19;
            this.cmbSubAgnetName.ValidationErrorMessage = "Validation Error!";
            this.cmbSubAgnetName.SelectedIndexChanged += new System.EventHandler(this.cmbSubAgnetName_SelectedIndexChanged);
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(488, 26);
            this.customTitlebar1.TabIndex = 24;
            // 
            // mandatoryMark1
            // 
            this.mandatoryMark1.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark1.Location = new System.Drawing.Point(399, 98);
            this.mandatoryMark1.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.Name = "mandatoryMark1";
            this.mandatoryMark1.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark1.TabIndex = 25;
            // 
            // mandatoryMark2
            // 
            this.mandatoryMark2.BackColor = System.Drawing.Color.Transparent;
            this.mandatoryMark2.Location = new System.Drawing.Point(399, 131);
            this.mandatoryMark2.MaximumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.MinimumSize = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.Name = "mandatoryMark2";
            this.mandatoryMark2.Size = new System.Drawing.Size(18, 22);
            this.mandatoryMark2.TabIndex = 27;
            // 
            // frmOutletUserCashLimitInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(488, 315);
            this.Controls.Add(this.cmbAgentName);
            this.Controls.Add(this.cmbSubAgnetName);
            this.Controls.Add(this.mandatoryMark2);
            this.Controls.Add(this.mandatoryMark1);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.lblRemainingLimitValue);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblTotalCashinHandValue);
            this.Controls.Add(this.lblTotalLimitValue);
            this.Controls.Add(this.lblTotalCashinHandHeader);
            this.Controls.Add(this.lblTotalLimitHeader);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblAgent);
            this.Controls.Add(this.lblSubAgent);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnShow);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmOutletUserCashLimitInfo";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Outlet User Cash Information";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmOutletUserCashLimitInfo_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.Label lblSubAgent;
        private System.Windows.Forms.Label lblAgent;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblTotalLimitHeader;
        private System.Windows.Forms.Label lblTotalCashinHandHeader;
        private System.Windows.Forms.Label lblTotalLimitValue;
        private System.Windows.Forms.Label lblTotalCashinHandValue;
        private System.Windows.Forms.Label lblRemainingLimitValue;
        private System.Windows.Forms.Label label2;
        private Agent.CustomControls.CustomComboBoxDropDownList cmbAgentName;
        private Agent.CustomControls.CustomComboBoxDropDownList cmbSubAgnetName;
        private CustomTitlebar customTitlebar1;
        private CustomControls.MandatoryMark mandatoryMark2;
        private CustomControls.MandatoryMark mandatoryMark1;
    }
}