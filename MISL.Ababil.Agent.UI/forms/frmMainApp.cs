﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.CustomControls;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Interfaces;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.UI.MenuRepository;

namespace MISL.Ababil.Agent.UI.forms
{
    public partial class frmMainApp : CustomForm, ILogoutable
    {
        public bool _frmClosing = false;
        private const int GWL_EXSTYLE = -20;
        private const uint SWP_FRAMECHANGED = 0x0020;
        private const uint SWP_NOACTIVATE = 0x0010;
        private const uint SWP_NOMOVE = 0x0002;
        private const uint SWP_NOOWNERZORDER = 0x0200;
        private const uint SWP_NOSIZE = 0x0001;
        private const uint SWP_NOZORDER = 0x0004;
        private const int WS_EX_CLIENTEDGE = 0x200;
        private frmLogin _frmLogin = null;

        public frmMainApp(frmLogin login)
        {
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            InitializeComponent();
            _frmLogin = login;

            lblUserName.Text = SessionInfo.username;
            lblLastLoginTime.Text = "Last Login Time: " + SessionInfo.loginTime.ToString();

            // Always load from server. Ignore local cache.
            Image userImage = GetLoginUserImage(SessionInfo.username);
            RerenderBackgroundWithUserInfo(userImage);
            DashboardLoader();

            //~//pbx.Left = 0;
            //~//pbx.Top = 0;
            //~//pbx.Size = this.Size;
            SetBevel(this, false);
            LoadTheme();
        }

        public void SetPageLayerVisibility(bool invisibility)
        {
            if (invisibility)
            {
                //~//pbx.Visible = false;
                picExit.Visible = true;
            }
            else
            {
                picExit.Visible = false;
                //~//pbx.Size = this.Size;
                Bitmap bitmap = new Bitmap(Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height);
                this.DrawToBitmap(bitmap, new Rectangle(0, 0, Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height));
                Image image = bitmap;
                Graphics g = Graphics.FromImage(image);
                SolidBrush blueBrush = new SolidBrush(Color.FromArgb(100, Color.Black));
                //~//Rectangle rect = new Rectangle(0, 0, pbx.Width, pbx.Height);
                //~//g.FillRectangle(blueBrush, rect);
                //~//pbx.Image = image;
                //~//pbx.Visible = true;
                //~//pbx.BringToFront();
            }
        }

        public void ShowDashboard()
        {
            //~//pbx.Visible = false;
            //~//_dashboard.Show();
            //~//_dashboard.RefreshDashboardData();
        }

        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        private static bool SetBevel(Form form, bool show)
        {
            foreach (Control c in form.Controls)
            {
                MdiClient client = c as MdiClient;
                if (client != null)
                {
                    int windowLong = GetWindowLong(c.Handle, GWL_EXSTYLE);
                    if (show)
                    {
                        windowLong |= WS_EX_CLIENTEDGE;
                    }
                    else
                    {
                        windowLong &= ~WS_EX_CLIENTEDGE;
                    }
                    SetWindowLong(c.Handle, GWL_EXSTYLE, windowLong);
                    SetWindowPos(client.Handle, IntPtr.Zero, 0, 0, 0, 0,
                        SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER |
                        SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
                    return true;
                }
            }
            return false;
        }

        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        [DllImport("user32.dll", ExactSpelling = true)]
        private static extern int SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

        private void AddMenuSupportingEvents(MenuStrip menuStrip)
        {
            for (int i = 0; i < menuStrip.Items.Count; i++)
            {
                ((ToolStripMenuItem)menuStrip.Items[i]).DropDownOpened += customerToolStripMenuItem_DropDownOpened;
                ((ToolStripMenuItem)menuStrip.Items[i]).DropDownClosed += customerToolStripMenuItem_DropDownClosed;
            }
        }

        private void customerToolStripMenuItem_DropDownClosed(object sender, EventArgs e)
        {
            ToolStripMenuItem toolStripMenuItem = (ToolStripMenuItem)sender;
            toolStripMenuItem.ForeColor = Color.White;
        }

        private void customerToolStripMenuItem_DropDownOpened(object sender, EventArgs e)
        {
            ToolStripMenuItem toolStripMenuItem = (ToolStripMenuItem)sender;
            toolStripMenuItem.ForeColor = Color.Black;
        }

        private void DashboardLoader()
        {
            //~//if (SessionInfo.userBasicInformation.outlet == null) return;
            //~//if (Properties.Settings.Default.dashboardAutoLoad)
            //~//{
            //~//    ShowDashboard();
            //~//}
            //~//else
            //~//{
            //~//    _dashboard.Visible = false;
            //~//}
        }

        private void frmMainApp_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_frmClosing == false)
            {
                _frmClosing = true;
                if (Logout() == false)
                {
                    e.Cancel = true;
                    _frmClosing = false;
                }
                else
                {
                    _frmClosing = true;
                }
            }
        }

        private void frmMainApp_Load(object sender, EventArgs e)
        {
            this.MaximizedBounds = Screen.PrimaryScreen.WorkingArea;
            this.WindowState = FormWindowState.Maximized;

            #region Special Access

            ////SpecialUserAccess for developers only
            ////if (SessionInfo.username == "branch_sh")
            ////{
            ////    if (UtilityServices.isRightExist(Rights.ADMINISTRATIVE))
            ////    {
            ////        administrativeToolStripMenuItem.Visible = true;
            ////        specialActionMenuItemToolStripMenuItem.Visible = true;
            ////    }
            ////    this.MainMenuStrip = menuBranch;
            ////    menuBranch.Visible = true;
            ////    return;
            ////}

            #endregion

            //UserCategory userCategory = SessionInfo.userBasicInformation.userCategory ?? UserCategory.AgentUser;
            UserCategory? userCategory = SessionInfo.userBasicInformation.userCategory;
            BankUserType bankUserType = SessionInfo.userBasicInformation.bankUserType ?? BankUserType.RemittanceUser;
            UserType userType = SessionInfo.userBasicInformation.userType ?? UserType.ReportViewer;

            if (userCategory == UserCategory.BranchUser)
            {
                if (bankUserType == BankUserType.BranchUser)
                {
                    MenuStrip menu = new BranchMenu() { MainForm = this }.menuBranch;
                    menu.Items["administrativeToolStripMenuItem"].Visible = userType == UserType.Admin;
                    this.Controls.Add(menu);
                    this.MainMenuStrip = menu;
                    menu.Renderer = new ToolStripProfessionalRenderer(new MenuColorTable());
                    AddMenuSupportingEvents(menu);
                    menu.BringToFront();
                    //~//_dashboard.BringToFront();
                }
                if (bankUserType == BankUserType.FieldUser)
                {
                    MenuStrip menu = new FieldOfficerMenu() { MainForm = this }.menuFieldOfficer;
                    this.Controls.Add(menu);
                    this.MainMenuStrip = menu;
                    menu.Renderer = new ToolStripProfessionalRenderer(new MenuColorTable());
                    AddMenuSupportingEvents(menu);
                    menu.BringToFront();
                    //~//_dashboard.BringToFront();
                }
                if (bankUserType == BankUserType.RemittanceUser)
                {
                    MenuStrip menu = new RemittanceMenu() { MainForm = this }.menuRemittance;
                    this.Controls.Add(menu);
                    this.MainMenuStrip = menu;
                    menu.Renderer = new ToolStripProfessionalRenderer(new MenuColorTable());
                    AddMenuSupportingEvents(menu);
                    menu.BringToFront();
                    //~//_dashboard.BringToFront();
                }
            }
            if (userCategory == UserCategory.AgentUser)
            {
                MenuStrip menu = new AgentMenu() { MainForm = this }.menuAgent;
                if (userType == UserType.Admin)
                {
                    menu.Items[0].Visible = true;
                }
                else
                {
                    menu.Items[0].Visible = false;
                }
                this.Controls.Add(menu);
                this.MainMenuStrip = menu;
                menu.Renderer = new ToolStripProfessionalRenderer(new MenuColorTable());
                AddMenuSupportingEvents(menu);
                menu.BringToFront();
                //~//_dashboard.BringToFront();
            }
            if (userCategory == UserCategory.SubAgentUser)
            {
                MenuStrip menu = new SubAgentMenu() { MainForm = this }.menuSubAgent;
                this.Controls.Add(menu);
                this.MainMenuStrip = menu;
                menu.Renderer = new ToolStripProfessionalRenderer(new MenuColorTable());
                AddMenuSupportingEvents(menu);
                menu.BringToFront();
                //~//_dashboard.BringToFront();
            }
        }

        private Image GetLoginUserImage(string userName)
        {
            LoginService loginService = new LoginService();
            try
            {
                ServiceResult serviceResult = loginService.GetUserImageByUserName(userName);
                if (serviceResult.Success)
                {
                    Dictionary<string, byte[]> returnedDictionary = serviceResult.ReturnedObject as Dictionary<string, byte[]>;
                    if (returnedDictionary.ContainsKey("image"))
                    {
                        byte[] imageBytes = returnedDictionary["image"];
                        if (imageBytes.Length > 0)
                        {
                            return UtilityServices.byteArrayToImage(imageBytes);
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void LoadTheme()
        {
            //~//Theming.Theme theme = new Theming.Themes.Default();
            //~//customTitlebar1.BackColor = theme.TitleBarBackColor;
            //~//customTitlebar1.ForeColor = theme.TitleBarForeColor;
            //~//lblTitle.BackColor = lblTitle2.BackColor = theme.TitleBarBackColor;
            //~//lblTitle.ForeColor = lblTitle2.ForeColor = theme.TitleBarForeColor;
            //@@//@@//menuSubAgent.BackColor = theme.MenuBackColor;
            //@@//@@//menuSubAgent.ForeColor = theme.MenuForeColor;
        }

        private bool Logout()
        {
            SetPageLayerVisibility(false);
            if (MsgBox.showConfirmation("Do you want to Logout?") == "yes")
            {
                if (this.MdiChildren.Length > 0)
                {
                    MsgBox.showWarning("Please close all child windows before logging-out.");
                    SetPageLayerVisibility(true);
                    return false;
                }
                else
                {
                    SetPageLayerVisibility(true);
                    _frmLogin.Show();
                    //~//_frmLogin.EnableTimer();
                    _frmClosing = true;
                    this.Close();
                    return true;
                }
            }
            SetPageLayerVisibility(true);
            return false;
        }

        private void picExit_Click(object sender, EventArgs e)
        {
            Logout();
        }

        private void RerenderBackgroundWithUserInfo(Image image)
        {
            try
            {
                Bitmap b = new Bitmap(this.BackgroundImage);
                Graphics g = Graphics.FromImage(b);
                {
                    g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBilinear;
                    g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                }

                g.FillRectangle(Brushes.Black, picUser.Left, picUser.Top, picUser.Width, picUser.Height);
                if (image != null)
                {
                    Image retVal = ScaleImage(image, picUser.Width, picUser.Height);
                    g.DrawImage(retVal, picUser.Left + (picUser.Width - retVal.Width) / 2,
                        picUser.Top + (picUser.Height - retVal.Height) / 2, retVal.Width, retVal.Height);
                }

                g.DrawString(lblUserName.Text, lblUserName.Font, Brushes.Black,
                    new PointF(lblUserName.Left, lblUserName.Top));
                g.DrawString(lblLastLoginTime.Text, lblLastLoginTime.Font, Brushes.Black,
                    new PointF(lblLastLoginTime.Left, lblLastLoginTime.Top));

                MemoryStream ms = new MemoryStream();
                b.Save(ms, ImageFormat.Png);
                this.BackgroundImage = (Image)Bitmap.FromStream(ms);
                ms.Close();
            }
            catch (Exception ex)
            {
                //suppressed 
            }
        }

        private Bitmap ScaleImage(Image image, int maxWidth, int maxHeight)
        {
            double ratioX = (double)maxWidth / image.Width;
            double ratioY = (double)maxHeight / image.Height;
            double ratio = Math.Min(ratioX, ratioY);

            int newWidth = (int)(image.Width * ratio);
            int newHeight = (int)(image.Height * ratio);

            Bitmap newImage = new Bitmap(newWidth, newHeight);
            Graphics.FromImage(newImage).DrawImage(image, 0, 0, newWidth, newHeight);
            return new Bitmap(newImage);
        }
        private class MenuColorTable : ProfessionalColorTable
        {
            public MenuColorTable()
            {
                base.UseSystemColors = false;
            }
            public override System.Drawing.Color MenuBorder
            {
                get { return Color.FromArgb(43, 87, 104); }
            }
            public override Color MenuItemSelectedGradientBegin
            {
                get { return Color.FromArgb(43, 87, 180); }
            }
            public override Color MenuItemSelectedGradientEnd
            {
                get { return Color.FromArgb(43, 87, 180); }
            }
        }

        public bool LogoutForced()
        {
            _frmLogin.Show();
            _frmLogin.EnableTimer();
            _frmClosing = true;
            this.Close();
            return true;
        }

        public void LogoutExt()
        {
            LogoutForcedExt();
        }

        private void LogoutForcedExt()
        {
            _frmClosing = true;
            Application.ExitThread();
            Application.Restart();
        }
    }
}