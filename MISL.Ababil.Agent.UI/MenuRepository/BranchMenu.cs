﻿using System;
using System.Linq;
using System.Windows.Forms;
using MISL.Ababil.Agent.Configuration.UI.forms;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.LocalStorageService;
using MISL.Ababil.Agent.Module.BillPayment.UI;
using MISL.Ababil.Agent.Module.BillPayment.UI.AdminUI;
using MISL.Ababil.Agent.Module.BillPayment.UI.ConfigUI;
using MISL.Ababil.Agent.Module.ChequeRequisition.Report;
using MISL.Ababil.Agent.Module.ChequeRequisition.UI;
using MISL.Ababil.Agent.Module.Security.UI.FingerprintUI;
using MISL.Ababil.Agent.Module.Security.UI.PasswordResetUI;
//~//using MISL.Ababil.Agent.Module.Security.UI.WorkstationManagementUI;
using MISL.Ababil.Agent.Report;
using MISL.Ababil.Agent.UI.forms;
using MISL.Ababil.Agent.UI.forms.AccountClosingUI;
//~//using MISL.Ababil.Agent.UI.forms.Deposit;
using MISL.Ababil.Agent.UI.forms.ProgressUI;
using MISL.Ababil.Agent.UI.forms.RemittanceUI;

//~//using MISL.Ababil.Agent.UI.NotificationUI;

namespace MISL.Ababil.Agent.UI.MenuRepository
{
    public partial class BranchMenu : Form
    {
        public frmMainApp MainForm { get; set; }
        

        public BranchMenu()
        {
            InitializeComponent();
            if (SessionInfo.userBasicInformation.userCategory == UserCategory.BranchUser &&
                SessionInfo.userBasicInformation.bankUserType == BankUserType.BranchUser &&
                SessionInfo.userBasicInformation.userType == UserType.Operational)
            {
                userLimitToolStripMenuItem.Visible = true;
            }
            else
            {
                userLimitToolStripMenuItem.Visible = false;
            }
        }
 

        private void toolStripMenuItem29_Click(object sender, EventArgs e)
        {
            if (SessionInfo.rights.Any(p => p == Rights.CREATE_BANK_USER.ToString()))
            {
                new frmBranchUserRegistration().ShowDialog(MainForm);
            }
        }

        private void toolStripMenuItem30_Click(object sender, EventArgs e)
        {
            if (SessionInfo.rights.Any(p => p == Rights.APPROVE_CONSUMER.ToString()))
            {
                MainForm.SetPageLayerVisibility(false);
                new frmAgentCreation().ShowDialog(MainForm);
                MainForm.SetPageLayerVisibility(true);
            }
        }

        private void toolStripMenuItem31_Click(object sender, EventArgs e)
        {
            new frmAgent().Show(MainForm);
        }

        private void toolStripMenuItem32_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmAgentSearch2().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void pendingCustomerApplicationsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmPendingApplication().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void toolStripMenuItem33_Click(object sender, EventArgs e)
        {
            if (SessionInfo.rights.Any(p => p == Rights.APPROVE_CONSUMER.ToString()))
            {
                MainForm.SetPageLayerVisibility(false);
                bool rightExist = SessionInfo.rights.Any(p => p == Rights.APPROVE_CONSUMER.ToString());
                if (rightExist)
                {
                    frmSubAgent objfrmSubAgent = new frmSubAgent();
                    objfrmSubAgent.ShowDialog(MainForm);
                }

                MainForm.SetPageLayerVisibility(true);
            }
        }

        private void toolStripMenuItem34_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmSubagentSearch().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void userManagementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmUserList().ShowDialog(MainForm);
        }

        private void passwordResetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmPasswordResetAdmin(new Packet { actionType = FormActionType.New }).ShowDialog(MainForm);
        }

        private void cacheUpdateAdministrationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new forms.CacheUI.frmCacheUpdateAdministration().ShowDialog(MainForm);
        }

        private void specialActionMenuItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmSpecialAction().ShowDialog(MainForm);
        }

        private void accountEditorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //~//new frmAccountInfoEditor().ShowDialog(MainForm);
        }

        private void sSPRequestListToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmTermRequestSearch().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void transactionProfileToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new frmTransactionProfile().ShowDialog(MainForm);
        }

        private void viewCustomerInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmCustomerSearch().ShowDialog(MainForm);
        }

        private void chequeRequisitionSearchToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new frmChequeRequisitionSearch().ShowDialog(MainForm);
        }

        private void fingerprintAdminToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmFingerprintAdmin().ShowDialog(MainForm);
        }

        private void searchAccountsByMobileNoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new frmAccountSearchByMobile().ShowDialog(MainForm);
        }

        private void customerSearch2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //~//new frmCustomerSearch2().ShowDialog(MainForm);
        }

        private void individualSearch2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //~//new frmIndividualSearch2().ShowDialog(MainForm);
        }

        //----------------------------------------------------------------------------------------------------------

        private void toolStripMenuItem18_Click(object sender, EventArgs e)
        {
            new frmCashRegister().Show(MainForm);
        }

        private void transactionReportToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new frmTransactionReport().Show(MainForm);
        }

        private void toolStripMenuItem19_Click(object sender, EventArgs e)
        {
            new frmAccountWiseBalance().Show(MainForm);
        }

        private void toolStripMenuItem20_Click(object sender, EventArgs e)
        {
            new frmAccountOpeningRport().Show(MainForm);
        }

        private void itdBalanceOutsaandingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmItdBalancOutstanding().Show(MainForm);
        }

        private void rejectedConsumerApplicationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmRejectedConsumerAppReport().Show(MainForm);
        }

        private void toolStripMenuItem21_Click(object sender, EventArgs e)
        {
            new frmRemittanceRecord().Show(MainForm);
        }

        private void toolStripMenuItem22_Click(object sender, EventArgs e)
        {
            new frmAccountMonitoringRport().Show(MainForm);
        }

        private void toolStripMenuItem23_Click(object sender, EventArgs e)
        {
            new frmTrMonitoringRport().Show(MainForm);
        }

        private void agentChargeCommissionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //~//new frmChargeCommission().Show(MainForm);
        }

        private void toolStripMenuItem24_Click(object sender, EventArgs e)
        {
            new frmAgentCommissionRport().Show(MainForm);
        }

        private void toolStripMenuItem25_Click(object sender, EventArgs e)
        {
            new frmAgentIncomeStatement().Show(MainForm);
        }

        private void agentIncomeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new frmAgentIncome().Show(MainForm);
        }

        private void cashInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmOutletCashInfoAll(new Packet
            {
                actionType = FormActionType.New,
                intentType = IntentType.SelfDriven
            }).Show();
        }

        private void outletInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmOutletInfoReport().Show(MainForm);
        }

        private void outletUserInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmOutletUserInfoReport().Show(MainForm);
        }

        private void billPaymentInformationToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new frmBillPaymentReport().Show(MainForm);
        }

        private void chequeRequsitionReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmChequeRequisitionInformation().Show(MainForm);
        }

        private void chequeDeliveryInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmChequeDeliveryReport().Show(MainForm);
        }

        private void chequeBookInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmChequeBookInformation().Show(MainForm);
        }

        private void agentInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmAgentInformationReport().loadAgentInformation();
        }

        private void agentCashInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmAgentCashInformation().Show(MainForm);
        }

        private void toolStripMenuItem35_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmPasswordChange(false) { btnFingerPrint = { Visible = false } }.ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void exchangeHouseSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmExchangeHouseSetup().ShowDialog(MainForm);
        }

        private void billPaymentConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmBillPaymentConfiguration().ShowDialog(MainForm);
        }

        private void toolStripMenuItem36_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            if (MsgBox.showConfirmation("Do you want to reset local cache?\n\nPlease save all your work before continuing. You will be logged-out in this process.") == "yes")
            {
                //~//MainForm._frmClosing = true;
                ProgressUIManager.ShowProgress(MainForm);
                new LocalStorage().ResetLocalCache();
                ProgressUIManager.CloseProgress();
                Application.ExitThread();
                Application.Restart();
            }
            MainForm.SetPageLayerVisibility(true);
        }

        private void pendingApplicationSearchToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            //~//new forms.Deposit.frmPendingApplication2().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void workstationManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            //~//new frmWorkstationManager().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void workstationAllocationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            //~//new frmWorkstationAllocation().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void depositAccountApplicationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            //~//new DepositAccountApplciationForm().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void dailyReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            //~//new frmDailyReport().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void notificationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //new NotificationUI.NotificationForm().ShowDialog(MainForm);
            //~//NotificationForm notificationForm = new NotificationForm();
            //~//notificationForm.Show();
            //~//notificationForm.UpdateLocation();
        }

        private void customerToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void accountInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            //~//new frmAccountInfoByAccountNumber().ShowDialog();
            MainForm.SetPageLayerVisibility(true);
        }

        private void outletUserLimitSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmOutletUserCashLimitInfo(new Packet() { }).ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void productMappingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmProductMapping().ShowDialog();
            MainForm.SetPageLayerVisibility(true);
        }

        private void transactionMonitoringReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmTrMonitoringRport().Show(MainForm);
        }

        private void applicationMonitoringToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmApplicationMonitoring().Show(MainForm);
        }

        private void agentUserInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmAgentuserInfoReport().Show(MainForm);
        }

        private void branchUserInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmBranchUserInfoReport().Show(MainForm);
        }

        private void userTransferHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmUserTransferHistory().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void remittanceSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmRemittanceSetup().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void agentUserInformattionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            new frmAgentuserInfoReport().ShowDialog(MainForm);
            Cursor.Current = Cursors.Default;
        }

        private void branchUserInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            new frmBranchUserInfoReport().ShowDialog(MainForm);
            Cursor.Current = Cursors.Default;
        }

        private void outletAccToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmOutletWiseAccountInfoReport().ShowDialog(MainForm);
        }

        private void rermCloseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Packet packet = null;
            new frmTermAccountClosing(packet).ShowDialog(MainForm);
        }

        private void useridWiseRptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmUserIdWiseAccApprovedInfo().ShowDialog(MainForm);
        }

        private void nidVerificationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmDepositApplicantInfo().ShowDialog(MainForm);
        }

        private void accountClosingRequestsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmAccountClosingRequestSearch(new Packet() { actionType = FormActionType.New }).ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void mobileNumberUpdateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmCustomerMobileNoUpdate().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void daillyPositionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmDailyPositionReport().ShowDialog(MainForm);
        }

        private void nidVerificationReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmDepositApplicantInfo().ShowDialog(MainForm);
        }

        private void applicationMonitoringToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new frmApplicationMonitoring().Show(MainForm);
        }

        private void userIdWiseAccounrApproveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmUserIdWiseAccApprovedInfo().ShowDialog(MainForm);
        }

        private void dailyPositionReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmDailyPositionReport().ShowDialog(MainForm);

        }

        private void daaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmDailyReport().ShowDialog(MainForm);
        }

        private void onlineReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmOnlineReport().ShowDialog(MainForm);
        }

        private void setupLimitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmOutletUserCashLimitInfo(new Packet() { }).ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void billMonitoringReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            new frmBillMonitoringReport().ShowDialog(MainForm);
            Cursor.Current = Cursors.Default;
        }

        private void dailyElectricityBillCollectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            new frmDailyElecticityBillCollectionReceipt().ShowDialog(MainForm);
            Cursor.Current = Cursors.Default;
        }

        private void shomityConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmShomitySetup().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void zoneConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmZoneSetup().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void outletMappingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmOutletMapping().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void billPaymentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmBillPaymentAdmin().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void billPaymentsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmBillPaymentAdmin().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }
    }
}