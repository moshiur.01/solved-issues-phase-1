﻿using System.Windows.Forms;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.LocalStorageService;
using MISL.Ababil.Agent.Module.ChequeRequisition.UI;
using MISL.Ababil.Agent.Module.Security.UI.FingerprintChangeUI;
//~//using MISL.Ababil.Agent.Module.Security.UI.FingerprintChangeUI;
using MISL.Ababil.Agent.Module.Security.UI.FingerprintUI;
using MISL.Ababil.Agent.Report;
using MISL.Ababil.Agent.UI.forms;
using MISL.Ababil.Agent.UI.forms.AccountClosingUI;
//~//using MISL.Ababil.Agent.UI.forms.Deposit;
using MISL.Ababil.Agent.UI.forms.Development;
using MISL.Ababil.Agent.UI.forms.ProgressUI;
using MISL.Ababil.Agent.Module.ChequeRequisition.Report;
using MISL.Ababil.Agent.Module.BillPayment.UI;
//~//using MISL.Ababil.Agent.UIWPF;

namespace MISL.Ababil.Agent.UI.MenuRepository
{
    public partial class SubAgentMenu : Form
    {
        public frmMainApp MainForm { get; set; }

        public SubAgentMenu()
        {
            InitializeComponent();
        }

        private void toolStripMenuItem44_Click(object sender, System.EventArgs e)
        {
            //~//MainForm.ShowDashboard();
        }

        private void customerApplicationToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmConsumerApplication(new Packet())
            {
                ShowInTaskbar = false
            }.ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void customerApplicationSearchToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmPendingApplication().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void sSPAccountRequestToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmTermAccountOpening(new Packet
            {
                actionType = FormActionType.New,
                intentType = IntentType.SelfDriven
            }, null).ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void sSPRequestListToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmTermRequestSearch().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void chequeRequisitionToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmChequeRequisitionRequest(new Packet
            {
                actionType = FormActionType.New
            }, new Module.ChequeRequisition.Models.ChequeRequisitionSearchResultDto()).ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void chequeRequisitionSearchToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmChequeRequisitionSearch().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void fingerprintManagementToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmFingerprintChangeRequest(new Packet { actionType = FormActionType.New }, null, null, null).ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void toolStripMenuItem28_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmTransactionProfile { _viewMode = true }.ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void searchAccountsByMobileNoToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmAccountSearchByMobile().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void cashDepositToolStripMenuItem1_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmCashDeposit().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void cashWithdrawToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmCashWithdraw().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void fundTransferToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmFundTransfer().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void balanceInquiryToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmBalanceInquiry().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void miniStatementToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmMiniStatement().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void itdInstallmentPaymentToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            //~//new frmItdInstallmentPayment(new Packet { actionType = FormActionType.New }, null).ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void billPaymentToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            #region previous

            {

                //MainForm.SetPageLayerVisibility(false);
                //new frmBillPayment(new Packet
                //{
                //    actionType = FormActionType.New,
                //    intentType = IntentType.SelfDriven
                //}, null).ShowDialog(MainForm);
                //MainForm.SetPageLayerVisibility(true);
            }
            #endregion

            MainForm.SetPageLayerVisibility(false);
            DialogResult result = new frmBillPayment2(new Packet
            {
                actionType = FormActionType.New,
                intentType = IntentType.SelfDriven
            }).ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);

            if (result == DialogResult.OK)
            {
                billPaymentToolStripMenuItem_Click(sender, e);
            }
        }

        private void kYCViewerToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            new frmKYCViewer().Show(MainForm);
        }

        private void sMECollectionToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            //~//new frmSMECollectionSheet(new Packet
            //~//{
            //~//    intentType = IntentType.SelfDriven,
            //~//    actionType = FormActionType.New
            //~//}).ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void toolStripMenuItem3_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmRemittanceAgentRequest
            {
                _IsFromAdmin = false
            }.ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void toolStripMenuItem4_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmRemittanceAdmin().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void toolStripMenuItem5_Click(object sender, System.EventArgs e)
        {
            if (SessionInfo.userBasicInformation.agent.id != 1)
            {
                MainForm.SetPageLayerVisibility(false);
                new frmCashEntry(new Packet()).ShowDialog(MainForm);
                MainForm.SetPageLayerVisibility(true);
            }
            else MsgBox.showInformation("You have no right to perform this operation.");
        }

        private void cashInformationToolStripMenuItem1_Click(object sender, System.EventArgs e)
        {
            //MainForm.SetPageLayerVisibility(false);
            //new frmOutletCashInfo(new Packet(), new OutletCashSumReqDto
            //{
            //    informationDate = SessionInfo.currentDate,
            //    outletId = SessionInfo.userBasicInformation.outlet.id                
            //}, "outletuser").ShowDialog(MainForm);
            //MainForm.SetPageLayerVisibility(true);


            MainForm.SetPageLayerVisibility(false);
            new frmOutletCashInfo(new Packet(), new OutletCashSumReqDto
            {
                informationDate = SessionInfo.currentDate,
                outletId = SessionInfo.userBasicInformation.userId ?? 0
            }, "outletuser").ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void toolStripMenuItem27_Click(object sender, System.EventArgs e)
        {
            new frmCashRegister().ShowDialog(MainForm);
        }

        private void toolStripMenuItem7_Click(object sender, System.EventArgs e)
        {
            new frmTransactionRecord().ShowDialog(MainForm);
        }

        private void toolStripMenuItem8_Click(object sender, System.EventArgs e)
        {
            new frmTransactionRecodeSearch().Show(MainForm);
        }

        private void dailyTransactionReportToolStripMenuItem1_Click(object sender, System.EventArgs e)
        {
            new frmTransactionReport().Show(MainForm);
        }

        private void toolStripMenuItem11_Click(object sender, System.EventArgs e)
        {
            new frmAccountWiseBalance().Show(MainForm);
        }

        private void toolStripMenuItem10_Click(object sender, System.EventArgs e)
        {
            new frmAccountOpeningRport().Show(MainForm);
        }

        private void toolStripMenuItem9_Click(object sender, System.EventArgs e)
        {
            new frmAllConsumerApplicationSearch().Show(MainForm);
        }

        private void termAccountInqueryToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            new frmTermAccountInquery().Show(MainForm);
        }

        private void iTDOutstandingBalanceToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            new frmItdBalancOutstanding().Show(MainForm);
        }

        private void consumerApplicationInoToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            new frmRejectedConsumerAppReport().Show(MainForm);
        }

        private void toolStripMenuItem12_Click(object sender, System.EventArgs e)
        {
            new frmRemittanceRecord().Show(MainForm);
        }

        private void toolStripMenuItem15_Click(object sender, System.EventArgs e)
        {
            new frmAgentCommissionRport().Show(MainForm);
        }

        private void toolStripMenuItem16_Click(object sender, System.EventArgs e)
        {
            new frmAgentIncomeStatement().Show(MainForm);
        }

        private void agentIncomeToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            new frmAgentIncome().Show(MainForm);
        }

        private void agentChargeCommissionToolStripMenuItem1_Click(object sender, System.EventArgs e)
        {
            new frmChargeCommission().Show(MainForm);
        }

        private void billPaymentInformationToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            new frmBillPaymentReport().Show(MainForm);
        }

        private void changePasswordToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmPasswordChange(false) { btnFingerPrint = { Visible = false } }.ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void resetCacheToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            if (MsgBox.showConfirmation("Do you want to reset local cache?\n\nPlease save all your work before continuing. You will be logged-out in this process.") == "yes")
            {
                //~//MainForm._frmClosing = true;

                ProgressUIManager.ShowProgress(MainForm);
                new LocalStorage().ResetLocalCache();
                ProgressUIManager.CloseProgress();

                Application.ExitThread();
                Application.Restart();
            }
            MainForm.SetPageLayerVisibility(true);
        }

        private void preferenceToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            //~//new forms.PreferenceUI.frmPreference().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void dataAssignToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            new frmDataAsign().ShowDialog(MainForm);
        }

        private void frmCustomerTestToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            new frmCustomer(new Packet(), null).ShowDialog(MainForm);
        }

        private void customerApply2ToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            //~//new frmCustomerApply2().ShowDialog(MainForm);
        }

        private void customerApplyToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            //~//new frmCustomerApply2().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void depositAccountApplyToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            //~//new forms.Deposit.DepositAccountApplyForm(new Packet() { actionType = FormActionType.New }).ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void pendingApplicationSearchToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            //~//new forms.Deposit.frmPendingApplication2().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void depositApplicationWPFUIToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            //~//new DepositApplicationsWindow().ShowDialog();
            MainForm.SetPageLayerVisibility(true);
        }

        private void cashDeposit2ToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            //~//new frmCashDeposit2().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void cashWithdraw2ToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            //~//new frmCashWithdraw2().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void accountInformationToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            //~//new frmAccountInfoByAccountNumber().ShowDialog();
            MainForm.SetPageLayerVisibility(true);
        }

        private void miniStatementForTermAccountToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmMiniStatementForTermAccount().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void termAccountClosingRequestToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            //~//new frmTermAccountClosing().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void termAccountClosingRequestToolStripMenuItem1_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            //~//new frmTermAccountClosing().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void termAccountClosingRequestSearchToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            //~//new frmTermAccountClosingAdmin().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void tESTToolStripMenuItem1_Click(object sender, System.EventArgs e)
        {

        }

        private void miniStatementForTermAccountToolStripMenuItem1_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmMiniStatementForTermAccount().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void fingerprintChangeRequestByIndividualIDToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            //~//new frmFingerprintChangeByIndividualID().ShowDialog(MainForm);            
            MainForm.SetPageLayerVisibility(true);
        }

        private void accountClosingToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmAccountClosingRequest(new Packet() { actionType = FormActionType.New }).ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void accountClosingRequestsToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmAccountClosingRequestSearch(new Packet() { actionType = FormActionType.New }).ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void depositAccountInfoToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmDepositAccountOperator().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void fingerprintUpdateRequestByIndividualIDToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmFingerprintChangeByIndividualID(new Packet() { actionType = FormActionType.New }).ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void chequeDeliverInformationToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            new frmChequeDeliveryReport().ShowDialog(MainForm);
        }

        private void billMonitoringReportToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
          
        }

        private void dailyElectricityBillCollectionToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
          
        }

        private void billMonitoringReportToolStripMenuItem_Click_1(object sender, System.EventArgs e)
        {
            new frmBillMonitoringReport().ShowDialog(MainForm);
        }

        private void depositSlipToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            new frmDepositSlip().ShowDialog(MainForm);
        }

        private void dailyElectricityBillCollectionReportToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            new frmDailyElecticityBillCollectionReceipt().ShowDialog(MainForm);
            Cursor.Current = Cursors.Default;
        }
    }
}