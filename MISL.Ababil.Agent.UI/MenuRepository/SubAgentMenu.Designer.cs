﻿namespace MISL.Ababil.Agent.UI.MenuRepository
{
    partial class SubAgentMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuSubAgent = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem44 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerApplicationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerApplicationSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.sSPAccountRequestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sSPRequestListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.termAccountClosingRequestToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.termAccountClosingRequestSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem19 = new System.Windows.Forms.ToolStripSeparator();
            this.accountClosingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountClosingRequestsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.chequeRequisitionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chequeRequisitionSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.fingerprintManagementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fingerprintUpdateRequestByIndividualIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem28 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem20 = new System.Windows.Forms.ToolStripSeparator();
            this.depositAccountInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchAccountsByMobileNoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.servicesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cashDepositToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cashWithdrawToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fundTransferToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.balanceInquiryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miniStatementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem17 = new System.Windows.Forms.ToolStripSeparator();
            this.itdInstallmentPaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miniStatementForTermAccountToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.billPaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripSeparator();
            this.sMECollectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripSeparator();
            this.kYCViewerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.remittanceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.outletToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.cashInformationToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.transactionalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem27 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.dailyTransactionReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.accountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.termAccountInqueryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iTDOutstandingBalanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consumerApplicationInoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.remittanceToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.monitoringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
            this.agentIncomeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agentChargeCommissionToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.billPaymentToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.billPaymentInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chequeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chequeDeliverInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.changePasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetCacheToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem45 = new System.Windows.Forms.ToolStripSeparator();
            this.preferenceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tESTToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dataAssignToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.frmCustomerTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerApply2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem46 = new System.Windows.Forms.ToolStripSeparator();
            this.customerApplyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.depositAccountApplyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pendingApplicationSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem47 = new System.Windows.Forms.ToolStripSeparator();
            this.depositApplicationWPFUIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.cashDeposit2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cashWithdraw2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.miniStatementForTermAccountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripSeparator();
            this.termAccountClosingRequestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem18 = new System.Windows.Forms.ToolStripSeparator();
            this.fingerprintChangeRequestByIndividualIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tESTMSGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tESTCustDTPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportPreLoadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.billMonitoringReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.depositSlipToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dailyElectricityBillCollectionReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSubAgent.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuSubAgent
            // 
            this.menuSubAgent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.menuSubAgent.Font = new System.Drawing.Font("Tahoma", 9F);
            this.menuSubAgent.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem44,
            this.customerToolStripMenuItem,
            this.servicesToolStripMenuItem,
            this.remittanceToolStripMenuItem1,
            this.outletToolStripMenuItem,
            this.reportToolStripMenuItem1,
            this.settingsToolStripMenuItem1,
            this.tESTToolStripMenuItem1,
            this.tESTMSGToolStripMenuItem,
            this.tESTCustDTPToolStripMenuItem,
            this.reportPreLoadToolStripMenuItem});
            this.menuSubAgent.Location = new System.Drawing.Point(0, 0);
            this.menuSubAgent.Name = "menuSubAgent";
            this.menuSubAgent.Size = new System.Drawing.Size(1181, 24);
            this.menuSubAgent.TabIndex = 14;
            this.menuSubAgent.Text = "menuStrip2";
            // 
            // toolStripMenuItem44
            // 
            this.toolStripMenuItem44.Font = new System.Drawing.Font("Tahoma", 9F);
            this.toolStripMenuItem44.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem44.Name = "toolStripMenuItem44";
            this.toolStripMenuItem44.Size = new System.Drawing.Size(76, 20);
            this.toolStripMenuItem44.Text = "Dashboard";
            this.toolStripMenuItem44.Visible = false;
            this.toolStripMenuItem44.Click += new System.EventHandler(this.toolStripMenuItem44_Click);
            // 
            // customerToolStripMenuItem
            // 
            this.customerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerApplicationToolStripMenuItem,
            this.customerApplicationSearchToolStripMenuItem,
            this.toolStripSeparator4,
            this.sSPAccountRequestToolStripMenuItem,
            this.sSPRequestListToolStripMenuItem,
            this.termAccountClosingRequestToolStripMenuItem1,
            this.termAccountClosingRequestSearchToolStripMenuItem,
            this.toolStripMenuItem19,
            this.accountClosingToolStripMenuItem,
            this.accountClosingRequestsToolStripMenuItem,
            this.toolStripSeparator2,
            this.chequeRequisitionToolStripMenuItem,
            this.chequeRequisitionSearchToolStripMenuItem,
            this.toolStripSeparator1,
            this.fingerprintManagementToolStripMenuItem,
            this.fingerprintUpdateRequestByIndividualIDToolStripMenuItem,
            this.toolStripSeparator3,
            this.toolStripMenuItem28,
            this.toolStripMenuItem20,
            this.depositAccountInfoToolStripMenuItem,
            this.searchAccountsByMobileNoToolStripMenuItem});
            this.customerToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.customerToolStripMenuItem.Name = "customerToolStripMenuItem";
            this.customerToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.customerToolStripMenuItem.Text = "Customer";
            // 
            // customerApplicationToolStripMenuItem
            // 
            this.customerApplicationToolStripMenuItem.Name = "customerApplicationToolStripMenuItem";
            this.customerApplicationToolStripMenuItem.Size = new System.Drawing.Size(339, 22);
            this.customerApplicationToolStripMenuItem.Text = "Customer Application";
            this.customerApplicationToolStripMenuItem.Click += new System.EventHandler(this.customerApplicationToolStripMenuItem_Click);
            // 
            // customerApplicationSearchToolStripMenuItem
            // 
            this.customerApplicationSearchToolStripMenuItem.Name = "customerApplicationSearchToolStripMenuItem";
            this.customerApplicationSearchToolStripMenuItem.Size = new System.Drawing.Size(339, 22);
            this.customerApplicationSearchToolStripMenuItem.Text = "Draft Applications List";
            this.customerApplicationSearchToolStripMenuItem.Click += new System.EventHandler(this.customerApplicationSearchToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(336, 6);
            // 
            // sSPAccountRequestToolStripMenuItem
            // 
            this.sSPAccountRequestToolStripMenuItem.Name = "sSPAccountRequestToolStripMenuItem";
            this.sSPAccountRequestToolStripMenuItem.Size = new System.Drawing.Size(339, 22);
            this.sSPAccountRequestToolStripMenuItem.Text = "Term Account Request";
            this.sSPAccountRequestToolStripMenuItem.Click += new System.EventHandler(this.sSPAccountRequestToolStripMenuItem_Click);
            // 
            // sSPRequestListToolStripMenuItem
            // 
            this.sSPRequestListToolStripMenuItem.Name = "sSPRequestListToolStripMenuItem";
            this.sSPRequestListToolStripMenuItem.Size = new System.Drawing.Size(339, 22);
            this.sSPRequestListToolStripMenuItem.Text = "Submitted Term Request";
            this.sSPRequestListToolStripMenuItem.Click += new System.EventHandler(this.sSPRequestListToolStripMenuItem_Click);
            // 
            // termAccountClosingRequestToolStripMenuItem1
            // 
            this.termAccountClosingRequestToolStripMenuItem1.Name = "termAccountClosingRequestToolStripMenuItem1";
            this.termAccountClosingRequestToolStripMenuItem1.Size = new System.Drawing.Size(339, 22);
            this.termAccountClosingRequestToolStripMenuItem1.Text = "Term Account Closing Request - HIDDEN";
            this.termAccountClosingRequestToolStripMenuItem1.Visible = false;
            this.termAccountClosingRequestToolStripMenuItem1.Click += new System.EventHandler(this.termAccountClosingRequestToolStripMenuItem1_Click);
            // 
            // termAccountClosingRequestSearchToolStripMenuItem
            // 
            this.termAccountClosingRequestSearchToolStripMenuItem.Name = "termAccountClosingRequestSearchToolStripMenuItem";
            this.termAccountClosingRequestSearchToolStripMenuItem.Size = new System.Drawing.Size(339, 22);
            this.termAccountClosingRequestSearchToolStripMenuItem.Text = "Term Account Closing Request Search - HIDDEN";
            this.termAccountClosingRequestSearchToolStripMenuItem.Visible = false;
            this.termAccountClosingRequestSearchToolStripMenuItem.Click += new System.EventHandler(this.termAccountClosingRequestSearchToolStripMenuItem_Click);
            // 
            // toolStripMenuItem19
            // 
            this.toolStripMenuItem19.Name = "toolStripMenuItem19";
            this.toolStripMenuItem19.Size = new System.Drawing.Size(336, 6);
            // 
            // accountClosingToolStripMenuItem
            // 
            this.accountClosingToolStripMenuItem.Name = "accountClosingToolStripMenuItem";
            this.accountClosingToolStripMenuItem.Size = new System.Drawing.Size(339, 22);
            this.accountClosingToolStripMenuItem.Text = "Close Account";
            this.accountClosingToolStripMenuItem.Click += new System.EventHandler(this.accountClosingToolStripMenuItem_Click);
            // 
            // accountClosingRequestsToolStripMenuItem
            // 
            this.accountClosingRequestsToolStripMenuItem.Name = "accountClosingRequestsToolStripMenuItem";
            this.accountClosingRequestsToolStripMenuItem.Size = new System.Drawing.Size(339, 22);
            this.accountClosingRequestsToolStripMenuItem.Text = "Account Closing Requests";
            this.accountClosingRequestsToolStripMenuItem.Click += new System.EventHandler(this.accountClosingRequestsToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(336, 6);
            // 
            // chequeRequisitionToolStripMenuItem
            // 
            this.chequeRequisitionToolStripMenuItem.Name = "chequeRequisitionToolStripMenuItem";
            this.chequeRequisitionToolStripMenuItem.Size = new System.Drawing.Size(339, 22);
            this.chequeRequisitionToolStripMenuItem.Text = "Cheque Requisition";
            this.chequeRequisitionToolStripMenuItem.Click += new System.EventHandler(this.chequeRequisitionToolStripMenuItem_Click);
            // 
            // chequeRequisitionSearchToolStripMenuItem
            // 
            this.chequeRequisitionSearchToolStripMenuItem.Name = "chequeRequisitionSearchToolStripMenuItem";
            this.chequeRequisitionSearchToolStripMenuItem.Size = new System.Drawing.Size(339, 22);
            this.chequeRequisitionSearchToolStripMenuItem.Text = "Cheque Requisition Search";
            this.chequeRequisitionSearchToolStripMenuItem.Click += new System.EventHandler(this.chequeRequisitionSearchToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(336, 6);
            this.toolStripSeparator1.Visible = false;
            // 
            // fingerprintManagementToolStripMenuItem
            // 
            this.fingerprintManagementToolStripMenuItem.Name = "fingerprintManagementToolStripMenuItem";
            this.fingerprintManagementToolStripMenuItem.Size = new System.Drawing.Size(339, 22);
            this.fingerprintManagementToolStripMenuItem.Text = "Fingerprint Update Request - HIDDEN";
            this.fingerprintManagementToolStripMenuItem.Visible = false;
            this.fingerprintManagementToolStripMenuItem.Click += new System.EventHandler(this.fingerprintManagementToolStripMenuItem_Click);
            // 
            // fingerprintUpdateRequestByIndividualIDToolStripMenuItem
            // 
            this.fingerprintUpdateRequestByIndividualIDToolStripMenuItem.Name = "fingerprintUpdateRequestByIndividualIDToolStripMenuItem";
            this.fingerprintUpdateRequestByIndividualIDToolStripMenuItem.Size = new System.Drawing.Size(339, 22);
            this.fingerprintUpdateRequestByIndividualIDToolStripMenuItem.Text = "Fingerprint Update Request By Individual ID";
            this.fingerprintUpdateRequestByIndividualIDToolStripMenuItem.Click += new System.EventHandler(this.fingerprintUpdateRequestByIndividualIDToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(336, 6);
            // 
            // toolStripMenuItem28
            // 
            this.toolStripMenuItem28.Name = "toolStripMenuItem28";
            this.toolStripMenuItem28.Size = new System.Drawing.Size(339, 22);
            this.toolStripMenuItem28.Text = "Transaction Profile";
            this.toolStripMenuItem28.Click += new System.EventHandler(this.toolStripMenuItem28_Click);
            // 
            // toolStripMenuItem20
            // 
            this.toolStripMenuItem20.Name = "toolStripMenuItem20";
            this.toolStripMenuItem20.Size = new System.Drawing.Size(336, 6);
            // 
            // depositAccountInfoToolStripMenuItem
            // 
            this.depositAccountInfoToolStripMenuItem.Name = "depositAccountInfoToolStripMenuItem";
            this.depositAccountInfoToolStripMenuItem.Size = new System.Drawing.Size(339, 22);
            this.depositAccountInfoToolStripMenuItem.Text = "Deposit Account Info";
            this.depositAccountInfoToolStripMenuItem.Click += new System.EventHandler(this.depositAccountInfoToolStripMenuItem_Click);
            // 
            // searchAccountsByMobileNoToolStripMenuItem
            // 
            this.searchAccountsByMobileNoToolStripMenuItem.Name = "searchAccountsByMobileNoToolStripMenuItem";
            this.searchAccountsByMobileNoToolStripMenuItem.Size = new System.Drawing.Size(339, 22);
            this.searchAccountsByMobileNoToolStripMenuItem.Text = "Search Accounts By Mobile No.";
            this.searchAccountsByMobileNoToolStripMenuItem.Click += new System.EventHandler(this.searchAccountsByMobileNoToolStripMenuItem_Click);
            // 
            // servicesToolStripMenuItem
            // 
            this.servicesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cashDepositToolStripMenuItem1,
            this.cashWithdrawToolStripMenuItem,
            this.fundTransferToolStripMenuItem,
            this.balanceInquiryToolStripMenuItem,
            this.miniStatementToolStripMenuItem,
            this.toolStripMenuItem17,
            this.itdInstallmentPaymentToolStripMenuItem,
            this.miniStatementForTermAccountToolStripMenuItem1,
            this.toolStripSeparator7,
            this.billPaymentToolStripMenuItem,
            this.toolStripMenuItem6,
            this.sMECollectionToolStripMenuItem,
            this.toolStripMenuItem14,
            this.kYCViewerToolStripMenuItem});
            this.servicesToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.servicesToolStripMenuItem.Name = "servicesToolStripMenuItem";
            this.servicesToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.servicesToolStripMenuItem.Text = "Service";
            // 
            // cashDepositToolStripMenuItem1
            // 
            this.cashDepositToolStripMenuItem1.Name = "cashDepositToolStripMenuItem1";
            this.cashDepositToolStripMenuItem1.ShowShortcutKeys = false;
            this.cashDepositToolStripMenuItem1.Size = new System.Drawing.Size(261, 22);
            this.cashDepositToolStripMenuItem1.Text = "Cash Deposit";
            this.cashDepositToolStripMenuItem1.Click += new System.EventHandler(this.cashDepositToolStripMenuItem1_Click);
            // 
            // cashWithdrawToolStripMenuItem
            // 
            this.cashWithdrawToolStripMenuItem.Name = "cashWithdrawToolStripMenuItem";
            this.cashWithdrawToolStripMenuItem.ShowShortcutKeys = false;
            this.cashWithdrawToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.cashWithdrawToolStripMenuItem.Text = "Cash Withdraw";
            this.cashWithdrawToolStripMenuItem.Click += new System.EventHandler(this.cashWithdrawToolStripMenuItem_Click);
            // 
            // fundTransferToolStripMenuItem
            // 
            this.fundTransferToolStripMenuItem.Name = "fundTransferToolStripMenuItem";
            this.fundTransferToolStripMenuItem.ShowShortcutKeys = false;
            this.fundTransferToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.fundTransferToolStripMenuItem.Text = "Fund Transfer";
            this.fundTransferToolStripMenuItem.Click += new System.EventHandler(this.fundTransferToolStripMenuItem_Click);
            // 
            // balanceInquiryToolStripMenuItem
            // 
            this.balanceInquiryToolStripMenuItem.Name = "balanceInquiryToolStripMenuItem";
            this.balanceInquiryToolStripMenuItem.ShowShortcutKeys = false;
            this.balanceInquiryToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.balanceInquiryToolStripMenuItem.Text = "Balance Inquiry";
            this.balanceInquiryToolStripMenuItem.Click += new System.EventHandler(this.balanceInquiryToolStripMenuItem_Click);
            // 
            // miniStatementToolStripMenuItem
            // 
            this.miniStatementToolStripMenuItem.Name = "miniStatementToolStripMenuItem";
            this.miniStatementToolStripMenuItem.ShowShortcutKeys = false;
            this.miniStatementToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.miniStatementToolStripMenuItem.Text = "Mini Statement";
            this.miniStatementToolStripMenuItem.Click += new System.EventHandler(this.miniStatementToolStripMenuItem_Click);
            // 
            // toolStripMenuItem17
            // 
            this.toolStripMenuItem17.Name = "toolStripMenuItem17";
            this.toolStripMenuItem17.Size = new System.Drawing.Size(258, 6);
            this.toolStripMenuItem17.Visible = false;
            // 
            // itdInstallmentPaymentToolStripMenuItem
            // 
            this.itdInstallmentPaymentToolStripMenuItem.Name = "itdInstallmentPaymentToolStripMenuItem";
            this.itdInstallmentPaymentToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.itdInstallmentPaymentToolStripMenuItem.Text = "Itd Installment Payment - Hidden";
            this.itdInstallmentPaymentToolStripMenuItem.Visible = false;
            this.itdInstallmentPaymentToolStripMenuItem.Click += new System.EventHandler(this.itdInstallmentPaymentToolStripMenuItem_Click);
            // 
            // miniStatementForTermAccountToolStripMenuItem1
            // 
            this.miniStatementForTermAccountToolStripMenuItem1.Name = "miniStatementForTermAccountToolStripMenuItem1";
            this.miniStatementForTermAccountToolStripMenuItem1.Size = new System.Drawing.Size(261, 22);
            this.miniStatementForTermAccountToolStripMenuItem1.Text = "Mini Statement For Term Account";
            this.miniStatementForTermAccountToolStripMenuItem1.Click += new System.EventHandler(this.miniStatementForTermAccountToolStripMenuItem1_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(258, 6);
            // 
            // billPaymentToolStripMenuItem
            // 
            this.billPaymentToolStripMenuItem.Name = "billPaymentToolStripMenuItem";
            this.billPaymentToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.billPaymentToolStripMenuItem.Text = "Bill Payment";
            this.billPaymentToolStripMenuItem.Click += new System.EventHandler(this.billPaymentToolStripMenuItem_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(258, 6);
            this.toolStripMenuItem6.Visible = false;
            // 
            // sMECollectionToolStripMenuItem
            // 
            this.sMECollectionToolStripMenuItem.Name = "sMECollectionToolStripMenuItem";
            this.sMECollectionToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.sMECollectionToolStripMenuItem.Text = "SME Collection - HIDDEN";
            this.sMECollectionToolStripMenuItem.Visible = false;
            this.sMECollectionToolStripMenuItem.Click += new System.EventHandler(this.sMECollectionToolStripMenuItem_Click);
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(258, 6);
            this.toolStripMenuItem14.Visible = false;
            // 
            // kYCViewerToolStripMenuItem
            // 
            this.kYCViewerToolStripMenuItem.Name = "kYCViewerToolStripMenuItem";
            this.kYCViewerToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.kYCViewerToolStripMenuItem.Text = "KYC Viewer - HIDDEN";
            this.kYCViewerToolStripMenuItem.Visible = false;
            this.kYCViewerToolStripMenuItem.Click += new System.EventHandler(this.kYCViewerToolStripMenuItem_Click);
            // 
            // remittanceToolStripMenuItem1
            // 
            this.remittanceToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.toolStripMenuItem4});
            this.remittanceToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.remittanceToolStripMenuItem1.Name = "remittanceToolStripMenuItem1";
            this.remittanceToolStripMenuItem1.Size = new System.Drawing.Size(81, 20);
            this.remittanceToolStripMenuItem1.Text = "Remittance";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.ShowShortcutKeys = false;
            this.toolStripMenuItem3.Size = new System.Drawing.Size(178, 22);
            this.toolStripMenuItem3.Text = "Remittance Request";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(178, 22);
            this.toolStripMenuItem4.Text = "Remittance Search";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // outletToolStripMenuItem
            // 
            this.outletToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem5,
            this.cashInformationToolStripMenuItem1});
            this.outletToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.outletToolStripMenuItem.Name = "outletToolStripMenuItem";
            this.outletToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.outletToolStripMenuItem.Text = "Outlet";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(166, 22);
            this.toolStripMenuItem5.Text = "Cash Entry";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.toolStripMenuItem5_Click);
            // 
            // cashInformationToolStripMenuItem1
            // 
            this.cashInformationToolStripMenuItem1.Name = "cashInformationToolStripMenuItem1";
            this.cashInformationToolStripMenuItem1.Size = new System.Drawing.Size(166, 22);
            this.cashInformationToolStripMenuItem1.Text = "Cash Information";
            this.cashInformationToolStripMenuItem1.Click += new System.EventHandler(this.cashInformationToolStripMenuItem1_Click);
            // 
            // reportToolStripMenuItem1
            // 
            this.reportToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.transactionalToolStripMenuItem,
            this.accountToolStripMenuItem,
            this.remittanceToolStripMenuItem3,
            this.monitoringToolStripMenuItem,
            this.billPaymentToolStripMenuItem1,
            this.chequeToolStripMenuItem});
            this.reportToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.reportToolStripMenuItem1.Name = "reportToolStripMenuItem1";
            this.reportToolStripMenuItem1.Size = new System.Drawing.Size(56, 20);
            this.reportToolStripMenuItem1.Text = "Report";
            // 
            // transactionalToolStripMenuItem
            // 
            this.transactionalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem27,
            this.toolStripMenuItem7,
            this.toolStripMenuItem8,
            this.dailyTransactionReportToolStripMenuItem1});
            this.transactionalToolStripMenuItem.Name = "transactionalToolStripMenuItem";
            this.transactionalToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.transactionalToolStripMenuItem.Text = "Transactional";
            // 
            // toolStripMenuItem27
            // 
            this.toolStripMenuItem27.Name = "toolStripMenuItem27";
            this.toolStripMenuItem27.Size = new System.Drawing.Size(241, 22);
            this.toolStripMenuItem27.Text = "Agent Cash Register";
            this.toolStripMenuItem27.Click += new System.EventHandler(this.toolStripMenuItem27_Click);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(241, 22);
            this.toolStripMenuItem7.Text = "Day Wise Transaction - Hidden";
            this.toolStripMenuItem7.Visible = false;
            this.toolStripMenuItem7.Click += new System.EventHandler(this.toolStripMenuItem7_Click);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(241, 22);
            this.toolStripMenuItem8.Text = "Transaction Record Search";
            this.toolStripMenuItem8.Click += new System.EventHandler(this.toolStripMenuItem8_Click);
            // 
            // dailyTransactionReportToolStripMenuItem1
            // 
            this.dailyTransactionReportToolStripMenuItem1.Name = "dailyTransactionReportToolStripMenuItem1";
            this.dailyTransactionReportToolStripMenuItem1.Size = new System.Drawing.Size(241, 22);
            this.dailyTransactionReportToolStripMenuItem1.Text = "Daily Transaction Report";
            this.dailyTransactionReportToolStripMenuItem1.Click += new System.EventHandler(this.dailyTransactionReportToolStripMenuItem1_Click);
            // 
            // accountToolStripMenuItem
            // 
            this.accountToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem11,
            this.toolStripMenuItem10,
            this.toolStripMenuItem9,
            this.termAccountInqueryToolStripMenuItem,
            this.iTDOutstandingBalanceToolStripMenuItem,
            this.consumerApplicationInoToolStripMenuItem});
            this.accountToolStripMenuItem.Name = "accountToolStripMenuItem";
            this.accountToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.accountToolStripMenuItem.Text = "Account";
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(270, 22);
            this.toolStripMenuItem11.Text = "Account Balance as on Date";
            this.toolStripMenuItem11.Click += new System.EventHandler(this.toolStripMenuItem11_Click);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(270, 22);
            this.toolStripMenuItem10.Text = "Account Opening Register";
            this.toolStripMenuItem10.Click += new System.EventHandler(this.toolStripMenuItem10_Click);
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(270, 22);
            this.toolStripMenuItem9.Text = "Customer Application Search";
            this.toolStripMenuItem9.Visible = false;
            this.toolStripMenuItem9.Click += new System.EventHandler(this.toolStripMenuItem9_Click);
            // 
            // termAccountInqueryToolStripMenuItem
            // 
            this.termAccountInqueryToolStripMenuItem.Name = "termAccountInqueryToolStripMenuItem";
            this.termAccountInqueryToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.termAccountInqueryToolStripMenuItem.Text = "Term Account Inquery";
            this.termAccountInqueryToolStripMenuItem.Click += new System.EventHandler(this.termAccountInqueryToolStripMenuItem_Click);
            // 
            // iTDOutstandingBalanceToolStripMenuItem
            // 
            this.iTDOutstandingBalanceToolStripMenuItem.Name = "iTDOutstandingBalanceToolStripMenuItem";
            this.iTDOutstandingBalanceToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.iTDOutstandingBalanceToolStripMenuItem.Text = "ITD Outstanding Balance";
            this.iTDOutstandingBalanceToolStripMenuItem.Click += new System.EventHandler(this.iTDOutstandingBalanceToolStripMenuItem_Click);
            // 
            // consumerApplicationInoToolStripMenuItem
            // 
            this.consumerApplicationInoToolStripMenuItem.Name = "consumerApplicationInoToolStripMenuItem";
            this.consumerApplicationInoToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.consumerApplicationInoToolStripMenuItem.Text = "Rejected Consumer Application Info";
            this.consumerApplicationInoToolStripMenuItem.Visible = false;
            this.consumerApplicationInoToolStripMenuItem.Click += new System.EventHandler(this.consumerApplicationInoToolStripMenuItem_Click);
            // 
            // remittanceToolStripMenuItem3
            // 
            this.remittanceToolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem12});
            this.remittanceToolStripMenuItem3.Name = "remittanceToolStripMenuItem3";
            this.remittanceToolStripMenuItem3.Size = new System.Drawing.Size(180, 22);
            this.remittanceToolStripMenuItem3.Text = "Remittance";
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(203, 22);
            this.toolStripMenuItem12.Text = "Remittance Information";
            this.toolStripMenuItem12.Click += new System.EventHandler(this.toolStripMenuItem12_Click);
            // 
            // monitoringToolStripMenuItem
            // 
            this.monitoringToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem15,
            this.toolStripMenuItem16,
            this.agentIncomeToolStripMenuItem,
            this.agentChargeCommissionToolStripMenuItem1});
            this.monitoringToolStripMenuItem.Name = "monitoringToolStripMenuItem";
            this.monitoringToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.monitoringToolStripMenuItem.Text = "Monitoring";
            // 
            // toolStripMenuItem15
            // 
            this.toolStripMenuItem15.Name = "toolStripMenuItem15";
            this.toolStripMenuItem15.Size = new System.Drawing.Size(216, 22);
            this.toolStripMenuItem15.Text = "Outlet Commission";
            this.toolStripMenuItem15.Visible = false;
            this.toolStripMenuItem15.Click += new System.EventHandler(this.toolStripMenuItem15_Click);
            // 
            // toolStripMenuItem16
            // 
            this.toolStripMenuItem16.Name = "toolStripMenuItem16";
            this.toolStripMenuItem16.Size = new System.Drawing.Size(216, 22);
            this.toolStripMenuItem16.Text = "Outlet Income";
            this.toolStripMenuItem16.Click += new System.EventHandler(this.toolStripMenuItem16_Click);
            // 
            // agentIncomeToolStripMenuItem
            // 
            this.agentIncomeToolStripMenuItem.Name = "agentIncomeToolStripMenuItem";
            this.agentIncomeToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.agentIncomeToolStripMenuItem.Text = "Agent Income";
            this.agentIncomeToolStripMenuItem.Click += new System.EventHandler(this.agentIncomeToolStripMenuItem_Click);
            // 
            // agentChargeCommissionToolStripMenuItem1
            // 
            this.agentChargeCommissionToolStripMenuItem1.Name = "agentChargeCommissionToolStripMenuItem1";
            this.agentChargeCommissionToolStripMenuItem1.Size = new System.Drawing.Size(216, 22);
            this.agentChargeCommissionToolStripMenuItem1.Text = "Agent Charge Commission";
            this.agentChargeCommissionToolStripMenuItem1.Visible = false;
            this.agentChargeCommissionToolStripMenuItem1.Click += new System.EventHandler(this.agentChargeCommissionToolStripMenuItem1_Click);
            // 
            // billPaymentToolStripMenuItem1
            // 
            this.billPaymentToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.billPaymentInformationToolStripMenuItem,
            this.billMonitoringReportToolStripMenuItem,
            this.depositSlipToolStripMenuItem,
            this.dailyElectricityBillCollectionReportToolStripMenuItem});
            this.billPaymentToolStripMenuItem1.Name = "billPaymentToolStripMenuItem1";
            this.billPaymentToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.billPaymentToolStripMenuItem1.Text = "Bill Payment";
            // 
            // billPaymentInformationToolStripMenuItem
            // 
            this.billPaymentInformationToolStripMenuItem.Name = "billPaymentInformationToolStripMenuItem";
            this.billPaymentInformationToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.billPaymentInformationToolStripMenuItem.Text = "Bill Payment Information - [HIDDEN]";
            this.billPaymentInformationToolStripMenuItem.Visible = false;
            this.billPaymentInformationToolStripMenuItem.Click += new System.EventHandler(this.billPaymentInformationToolStripMenuItem_Click);
            // 
            // chequeToolStripMenuItem
            // 
            this.chequeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.chequeDeliverInformationToolStripMenuItem});
            this.chequeToolStripMenuItem.Name = "chequeToolStripMenuItem";
            this.chequeToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.chequeToolStripMenuItem.Text = "Cheque";
            // 
            // chequeDeliverInformationToolStripMenuItem
            // 
            this.chequeDeliverInformationToolStripMenuItem.Name = "chequeDeliverInformationToolStripMenuItem";
            this.chequeDeliverInformationToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.chequeDeliverInformationToolStripMenuItem.Text = "Cheque Deliver Information";
            this.chequeDeliverInformationToolStripMenuItem.Click += new System.EventHandler(this.chequeDeliverInformationToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem1
            // 
            this.settingsToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changePasswordToolStripMenuItem,
            this.resetCacheToolStripMenuItem,
            this.toolStripMenuItem45,
            this.preferenceToolStripMenuItem});
            this.settingsToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.settingsToolStripMenuItem1.Name = "settingsToolStripMenuItem1";
            this.settingsToolStripMenuItem1.Size = new System.Drawing.Size(64, 20);
            this.settingsToolStripMenuItem1.Text = "Settings";
            // 
            // changePasswordToolStripMenuItem
            // 
            this.changePasswordToolStripMenuItem.Name = "changePasswordToolStripMenuItem";
            this.changePasswordToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.changePasswordToolStripMenuItem.Text = "Change Password";
            this.changePasswordToolStripMenuItem.Click += new System.EventHandler(this.changePasswordToolStripMenuItem_Click);
            // 
            // resetCacheToolStripMenuItem
            // 
            this.resetCacheToolStripMenuItem.Name = "resetCacheToolStripMenuItem";
            this.resetCacheToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.resetCacheToolStripMenuItem.Text = "Reset Cache";
            this.resetCacheToolStripMenuItem.Click += new System.EventHandler(this.resetCacheToolStripMenuItem_Click);
            // 
            // toolStripMenuItem45
            // 
            this.toolStripMenuItem45.Name = "toolStripMenuItem45";
            this.toolStripMenuItem45.Size = new System.Drawing.Size(167, 6);
            this.toolStripMenuItem45.Visible = false;
            // 
            // preferenceToolStripMenuItem
            // 
            this.preferenceToolStripMenuItem.Name = "preferenceToolStripMenuItem";
            this.preferenceToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.preferenceToolStripMenuItem.Text = "Preference";
            this.preferenceToolStripMenuItem.Visible = false;
            this.preferenceToolStripMenuItem.Click += new System.EventHandler(this.preferenceToolStripMenuItem_Click);
            // 
            // tESTToolStripMenuItem1
            // 
            this.tESTToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataAssignToolStripMenuItem,
            this.frmCustomerTestToolStripMenuItem,
            this.customerApply2ToolStripMenuItem,
            this.toolStripMenuItem46,
            this.customerApplyToolStripMenuItem,
            this.depositAccountApplyToolStripMenuItem,
            this.pendingApplicationSearchToolStripMenuItem,
            this.toolStripMenuItem47,
            this.depositApplicationWPFUIToolStripMenuItem,
            this.toolStripMenuItem1,
            this.cashDeposit2ToolStripMenuItem,
            this.cashWithdraw2ToolStripMenuItem,
            this.toolStripMenuItem2,
            this.miniStatementForTermAccountToolStripMenuItem,
            this.toolStripMenuItem13,
            this.termAccountClosingRequestToolStripMenuItem,
            this.toolStripMenuItem18,
            this.fingerprintChangeRequestByIndividualIDToolStripMenuItem});
            this.tESTToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.tESTToolStripMenuItem1.Name = "tESTToolStripMenuItem1";
            this.tESTToolStripMenuItem1.Size = new System.Drawing.Size(92, 20);
            this.tESTToolStripMenuItem1.Text = "Development";
            this.tESTToolStripMenuItem1.Visible = false;
            this.tESTToolStripMenuItem1.Click += new System.EventHandler(this.tESTToolStripMenuItem1_Click);
            // 
            // dataAssignToolStripMenuItem
            // 
            this.dataAssignToolStripMenuItem.Name = "dataAssignToolStripMenuItem";
            this.dataAssignToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.dataAssignToolStripMenuItem.Text = "Data Assign";
            this.dataAssignToolStripMenuItem.Visible = false;
            this.dataAssignToolStripMenuItem.Click += new System.EventHandler(this.dataAssignToolStripMenuItem_Click);
            // 
            // frmCustomerTestToolStripMenuItem
            // 
            this.frmCustomerTestToolStripMenuItem.Name = "frmCustomerTestToolStripMenuItem";
            this.frmCustomerTestToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.frmCustomerTestToolStripMenuItem.Text = "FrmCustomer Test";
            this.frmCustomerTestToolStripMenuItem.Visible = false;
            this.frmCustomerTestToolStripMenuItem.Click += new System.EventHandler(this.frmCustomerTestToolStripMenuItem_Click);
            // 
            // customerApply2ToolStripMenuItem
            // 
            this.customerApply2ToolStripMenuItem.Name = "customerApply2ToolStripMenuItem";
            this.customerApply2ToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.customerApply2ToolStripMenuItem.Text = "Customer Apply 2";
            this.customerApply2ToolStripMenuItem.Visible = false;
            this.customerApply2ToolStripMenuItem.Click += new System.EventHandler(this.customerApply2ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem46
            // 
            this.toolStripMenuItem46.Name = "toolStripMenuItem46";
            this.toolStripMenuItem46.Size = new System.Drawing.Size(310, 6);
            this.toolStripMenuItem46.Visible = false;
            // 
            // customerApplyToolStripMenuItem
            // 
            this.customerApplyToolStripMenuItem.Name = "customerApplyToolStripMenuItem";
            this.customerApplyToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.customerApplyToolStripMenuItem.Text = "Customer Apply";
            this.customerApplyToolStripMenuItem.Click += new System.EventHandler(this.customerApplyToolStripMenuItem_Click);
            // 
            // depositAccountApplyToolStripMenuItem
            // 
            this.depositAccountApplyToolStripMenuItem.Name = "depositAccountApplyToolStripMenuItem";
            this.depositAccountApplyToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.depositAccountApplyToolStripMenuItem.Text = "Deposit Account Apply";
            this.depositAccountApplyToolStripMenuItem.Click += new System.EventHandler(this.depositAccountApplyToolStripMenuItem_Click);
            // 
            // pendingApplicationSearchToolStripMenuItem
            // 
            this.pendingApplicationSearchToolStripMenuItem.Name = "pendingApplicationSearchToolStripMenuItem";
            this.pendingApplicationSearchToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.pendingApplicationSearchToolStripMenuItem.Text = "Pending Application Search";
            this.pendingApplicationSearchToolStripMenuItem.Click += new System.EventHandler(this.pendingApplicationSearchToolStripMenuItem_Click);
            // 
            // toolStripMenuItem47
            // 
            this.toolStripMenuItem47.Name = "toolStripMenuItem47";
            this.toolStripMenuItem47.Size = new System.Drawing.Size(310, 6);
            // 
            // depositApplicationWPFUIToolStripMenuItem
            // 
            this.depositApplicationWPFUIToolStripMenuItem.Name = "depositApplicationWPFUIToolStripMenuItem";
            this.depositApplicationWPFUIToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.depositApplicationWPFUIToolStripMenuItem.Text = "Deposit Application - WPF UI";
            this.depositApplicationWPFUIToolStripMenuItem.Click += new System.EventHandler(this.depositApplicationWPFUIToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(310, 6);
            // 
            // cashDeposit2ToolStripMenuItem
            // 
            this.cashDeposit2ToolStripMenuItem.Name = "cashDeposit2ToolStripMenuItem";
            this.cashDeposit2ToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.cashDeposit2ToolStripMenuItem.Text = "Cash Deposit 2";
            this.cashDeposit2ToolStripMenuItem.Click += new System.EventHandler(this.cashDeposit2ToolStripMenuItem_Click);
            // 
            // cashWithdraw2ToolStripMenuItem
            // 
            this.cashWithdraw2ToolStripMenuItem.Name = "cashWithdraw2ToolStripMenuItem";
            this.cashWithdraw2ToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.cashWithdraw2ToolStripMenuItem.Text = "Cash Withdraw 2";
            this.cashWithdraw2ToolStripMenuItem.Click += new System.EventHandler(this.cashWithdraw2ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(310, 6);
            // 
            // miniStatementForTermAccountToolStripMenuItem
            // 
            this.miniStatementForTermAccountToolStripMenuItem.Name = "miniStatementForTermAccountToolStripMenuItem";
            this.miniStatementForTermAccountToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.miniStatementForTermAccountToolStripMenuItem.Text = "Mini Statement For Term Account";
            this.miniStatementForTermAccountToolStripMenuItem.Click += new System.EventHandler(this.miniStatementForTermAccountToolStripMenuItem_Click);
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(310, 6);
            // 
            // termAccountClosingRequestToolStripMenuItem
            // 
            this.termAccountClosingRequestToolStripMenuItem.Name = "termAccountClosingRequestToolStripMenuItem";
            this.termAccountClosingRequestToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.termAccountClosingRequestToolStripMenuItem.Text = "Term Account Closing Request";
            this.termAccountClosingRequestToolStripMenuItem.Click += new System.EventHandler(this.termAccountClosingRequestToolStripMenuItem_Click);
            // 
            // toolStripMenuItem18
            // 
            this.toolStripMenuItem18.Name = "toolStripMenuItem18";
            this.toolStripMenuItem18.Size = new System.Drawing.Size(310, 6);
            // 
            // fingerprintChangeRequestByIndividualIDToolStripMenuItem
            // 
            this.fingerprintChangeRequestByIndividualIDToolStripMenuItem.Name = "fingerprintChangeRequestByIndividualIDToolStripMenuItem";
            this.fingerprintChangeRequestByIndividualIDToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.fingerprintChangeRequestByIndividualIDToolStripMenuItem.Text = "Fingerprint Change Request By Individual ID";
            this.fingerprintChangeRequestByIndividualIDToolStripMenuItem.Click += new System.EventHandler(this.fingerprintChangeRequestByIndividualIDToolStripMenuItem_Click);
            // 
            // tESTMSGToolStripMenuItem
            // 
            this.tESTMSGToolStripMenuItem.ForeColor = System.Drawing.Color.Navy;
            this.tESTMSGToolStripMenuItem.Name = "tESTMSGToolStripMenuItem";
            this.tESTMSGToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.tESTMSGToolStripMenuItem.Text = "--TEST-MSG--";
            this.tESTMSGToolStripMenuItem.Visible = false;
            // 
            // tESTCustDTPToolStripMenuItem
            // 
            this.tESTCustDTPToolStripMenuItem.ForeColor = System.Drawing.Color.Navy;
            this.tESTCustDTPToolStripMenuItem.Name = "tESTCustDTPToolStripMenuItem";
            this.tESTCustDTPToolStripMenuItem.Size = new System.Drawing.Size(120, 20);
            this.tESTCustDTPToolStripMenuItem.Text = "--TEST-CustDTP---";
            this.tESTCustDTPToolStripMenuItem.Visible = false;
            // 
            // reportPreLoadToolStripMenuItem
            // 
            this.reportPreLoadToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadReportToolStripMenuItem});
            this.reportPreLoadToolStripMenuItem.ForeColor = System.Drawing.Color.Navy;
            this.reportPreLoadToolStripMenuItem.Name = "reportPreLoadToolStripMenuItem";
            this.reportPreLoadToolStripMenuItem.Size = new System.Drawing.Size(104, 20);
            this.reportPreLoadToolStripMenuItem.Text = "Report Pre load";
            this.reportPreLoadToolStripMenuItem.Visible = false;
            // 
            // loadReportToolStripMenuItem
            // 
            this.loadReportToolStripMenuItem.Name = "loadReportToolStripMenuItem";
            this.loadReportToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.loadReportToolStripMenuItem.Text = "Load Report";
            // 
            // billMonitoringReportToolStripMenuItem
            // 
            this.billMonitoringReportToolStripMenuItem.Name = "billMonitoringReportToolStripMenuItem";
            this.billMonitoringReportToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.billMonitoringReportToolStripMenuItem.Text = "Bill Monitoring Report";
            this.billMonitoringReportToolStripMenuItem.Click += new System.EventHandler(this.billMonitoringReportToolStripMenuItem_Click_1);
            // 
            // depositSlipToolStripMenuItem
            // 
            this.depositSlipToolStripMenuItem.Name = "depositSlipToolStripMenuItem";
            this.depositSlipToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.depositSlipToolStripMenuItem.Text = "Deposit Slip";
            this.depositSlipToolStripMenuItem.Visible = false;
            this.depositSlipToolStripMenuItem.Click += new System.EventHandler(this.depositSlipToolStripMenuItem_Click);
            // 
            // dailyElectricityBillCollectionReportToolStripMenuItem
            // 
            this.dailyElectricityBillCollectionReportToolStripMenuItem.Name = "dailyElectricityBillCollectionReportToolStripMenuItem";
            this.dailyElectricityBillCollectionReportToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.dailyElectricityBillCollectionReportToolStripMenuItem.Text = "Daily Electricity Bill Collection Report";
            this.dailyElectricityBillCollectionReportToolStripMenuItem.Click += new System.EventHandler(this.dailyElectricityBillCollectionReportToolStripMenuItem_Click);
            // 
            // SubAgentMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1181, 24);
            this.ControlBox = false;
            this.Controls.Add(this.menuSubAgent);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SubAgentMenu";
            this.Text = "SubAgentMenu";
            this.menuSubAgent.ResumeLayout(false);
            this.menuSubAgent.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.MenuStrip menuSubAgent;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem44;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerApplicationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerApplicationSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem sSPAccountRequestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sSPRequestListToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem chequeRequisitionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chequeRequisitionSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem fingerprintManagementToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem28;
        private System.Windows.Forms.ToolStripMenuItem searchAccountsByMobileNoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem servicesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cashDepositToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cashWithdrawToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fundTransferToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem balanceInquiryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miniStatementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itdInstallmentPaymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem billPaymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kYCViewerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sMECollectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem remittanceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem outletToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem cashInformationToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem transactionalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem27;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem dailyTransactionReportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem accountToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem termAccountInqueryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iTDOutstandingBalanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consumerApplicationInoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem remittanceToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem monitoringToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem15;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem16;
        private System.Windows.Forms.ToolStripMenuItem agentIncomeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agentChargeCommissionToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem billPaymentToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem billPaymentInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem changePasswordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetCacheToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem45;
        private System.Windows.Forms.ToolStripMenuItem preferenceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tESTToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem dataAssignToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem frmCustomerTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerApply2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem46;
        private System.Windows.Forms.ToolStripMenuItem customerApplyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem depositAccountApplyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pendingApplicationSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem47;
        private System.Windows.Forms.ToolStripMenuItem depositApplicationWPFUIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tESTMSGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tESTCustDTPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportPreLoadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cashDeposit2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cashWithdraw2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem miniStatementForTermAccountToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem termAccountClosingRequestToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem termAccountClosingRequestSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem termAccountClosingRequestToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem17;
        private System.Windows.Forms.ToolStripMenuItem miniStatementForTermAccountToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem6;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem18;
        private System.Windows.Forms.ToolStripMenuItem fingerprintChangeRequestByIndividualIDToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem19;
        private System.Windows.Forms.ToolStripMenuItem accountClosingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountClosingRequestsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem20;
        private System.Windows.Forms.ToolStripMenuItem depositAccountInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fingerprintUpdateRequestByIndividualIDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chequeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chequeDeliverInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem billMonitoringReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem depositSlipToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dailyElectricityBillCollectionReportToolStripMenuItem;
    }
}