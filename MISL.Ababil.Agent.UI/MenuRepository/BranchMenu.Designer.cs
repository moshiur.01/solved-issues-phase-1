﻿namespace MISL.Ababil.Agent.UI.MenuRepository
{
    partial class BranchMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuBranch = new System.Windows.Forms.MenuStrip();
            this.administrativeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.branchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem29 = new System.Windows.Forms.ToolStripMenuItem();
            this.agentToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem30 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem31 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem32 = new System.Windows.Forms.ToolStripMenuItem();
            this.subAgentToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem33 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem34 = new System.Windows.Forms.ToolStripMenuItem();
            this.outletUserLimitSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.userManagementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userTransferHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripSeparator();
            this.mobileNumberUpdateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.productMappingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.remittanceSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.billPaymentConfigurationToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.shomityConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zoneConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outletMappingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.passwordResetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.cacheToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cacheUpdateAdministrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.specialActionMenuItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountEditorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.branchUserCreationToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.agentToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.agentCreationToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.viewAgentProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchAgentsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.subAgentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subAgentCreationToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.searchSubagentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.depositAccountApplicationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pendingCustomerApplicationsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.sSPRequestListToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.chequeRequisitionSearchToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.fingerprintAdminToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.accountClosingRequestsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripSeparator();
            this.transactionProfileToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripSeparator();
            this.searchAccountsByMobileNoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.viewCustomerInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.customerSearch2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.individualSearch2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transactionalToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem18 = new System.Windows.Forms.ToolStripMenuItem();
            this.transactionReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.transactionMonitoringReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.applicationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nidVerificationReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.applicationMonitoringToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.accountToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.accountInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem19 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem20 = new System.Windows.Forms.ToolStripMenuItem();
            this.itdBalanceOutsaandingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rejectedConsumerApplicationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userIdWiseAccounrApproveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.remittanceToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem21 = new System.Windows.Forms.ToolStripMenuItem();
            this.monitoringToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem22 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem23 = new System.Windows.Forms.ToolStripMenuItem();
            this.agentChargeCommissionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem24 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem25 = new System.Windows.Forms.ToolStripMenuItem();
            this.agentIncomeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dailyReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outletToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cashInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outletInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outletUserInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.billPaymentToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.billPaymentInformationToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.billMonitoringReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dailyElectricityBillCollectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chequeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chequeRequsitionReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chequeDeliveryInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chequeBookInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agnetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agentInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agentCashInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agentUserInformattionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.branchUserInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dailyReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dailyPositionReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.daaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.onlineReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem35 = new System.Windows.Forms.ToolStripMenuItem();
            this.exchangeHouseSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.billPaymentConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem36 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem26 = new System.Windows.Forms.ToolStripMenuItem();
            this.tESTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mTDRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.developmentToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pendingApplicationSearchToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.workstationManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.workstationAllocationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agentUserInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.branchUserInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outletAccToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rermCloseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.useridWiseRptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nidVerificationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.daillyPositionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notificationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userLimitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setupLimitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.servicesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.billPaymentsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBranch.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuBranch
            // 
            this.menuBranch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.menuBranch.Font = new System.Drawing.Font("Tahoma", 9F);
            this.menuBranch.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.administrativeToolStripMenuItem,
            this.usersToolStripMenuItem,
            this.agentToolStripMenuItem2,
            this.subAgentToolStripMenuItem,
            this.customerToolStripMenuItem1,
            this.servicesToolStripMenuItem,
            this.reportsToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.toolStripMenuItem26,
            this.tESTToolStripMenuItem,
            this.reportTestToolStripMenuItem,
            this.developmentToolStripMenuItem1,
            this.notificationToolStripMenuItem,
            this.userLimitToolStripMenuItem});
            this.menuBranch.Location = new System.Drawing.Point(0, 0);
            this.menuBranch.Name = "menuBranch";
            this.menuBranch.Size = new System.Drawing.Size(1164, 24);
            this.menuBranch.TabIndex = 12;
            this.menuBranch.Text = "menuBranch";
            // 
            // administrativeToolStripMenuItem
            // 
            this.administrativeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.branchToolStripMenuItem,
            this.agentToolStripMenuItem4,
            this.subAgentToolStripMenuItem1,
            this.toolStripSeparator6,
            this.userManagementToolStripMenuItem,
            this.userTransferHistoryToolStripMenuItem,
            this.toolStripMenuItem7,
            this.mobileNumberUpdateToolStripMenuItem,
            this.toolStripMenuItem2,
            this.productMappingToolStripMenuItem,
            this.remittanceSetupToolStripMenuItem,
            this.billPaymentConfigurationToolStripMenuItem1,
            this.passwordResetToolStripMenuItem,
            this.toolStripSeparator5,
            this.cacheToolStripMenuItem,
            this.specialActionMenuItemToolStripMenuItem,
            this.accountEditorToolStripMenuItem});
            this.administrativeToolStripMenuItem.Font = new System.Drawing.Font("Tahoma", 9F);
            this.administrativeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.administrativeToolStripMenuItem.Name = "administrativeToolStripMenuItem";
            this.administrativeToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.administrativeToolStripMenuItem.Tag = "administrativeToolStripMenuItem";
            this.administrativeToolStripMenuItem.Text = "Administrative";
            this.administrativeToolStripMenuItem.Visible = false;
            // 
            // branchToolStripMenuItem
            // 
            this.branchToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem29});
            this.branchToolStripMenuItem.Name = "branchToolStripMenuItem";
            this.branchToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.branchToolStripMenuItem.Text = "Branch";
            this.branchToolStripMenuItem.Visible = false;
            // 
            // toolStripMenuItem29
            // 
            this.toolStripMenuItem29.Name = "toolStripMenuItem29";
            this.toolStripMenuItem29.Size = new System.Drawing.Size(188, 22);
            this.toolStripMenuItem29.Text = "Branch User Creation";
            this.toolStripMenuItem29.Click += new System.EventHandler(this.toolStripMenuItem29_Click);
            // 
            // agentToolStripMenuItem4
            // 
            this.agentToolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem30,
            this.toolStripMenuItem31,
            this.toolStripMenuItem32});
            this.agentToolStripMenuItem4.Name = "agentToolStripMenuItem4";
            this.agentToolStripMenuItem4.Size = new System.Drawing.Size(252, 22);
            this.agentToolStripMenuItem4.Text = "Agent";
            // 
            // toolStripMenuItem30
            // 
            this.toolStripMenuItem30.Name = "toolStripMenuItem30";
            this.toolStripMenuItem30.Size = new System.Drawing.Size(176, 22);
            this.toolStripMenuItem30.Text = "Agent Creation";
            this.toolStripMenuItem30.Click += new System.EventHandler(this.toolStripMenuItem30_Click);
            // 
            // toolStripMenuItem31
            // 
            this.toolStripMenuItem31.Name = "toolStripMenuItem31";
            this.toolStripMenuItem31.Size = new System.Drawing.Size(176, 22);
            this.toolStripMenuItem31.Text = "View Agent Profile";
            this.toolStripMenuItem31.Visible = false;
            this.toolStripMenuItem31.Click += new System.EventHandler(this.toolStripMenuItem31_Click);
            // 
            // toolStripMenuItem32
            // 
            this.toolStripMenuItem32.Name = "toolStripMenuItem32";
            this.toolStripMenuItem32.Size = new System.Drawing.Size(176, 22);
            this.toolStripMenuItem32.Text = "Search Agents";
            this.toolStripMenuItem32.Click += new System.EventHandler(this.toolStripMenuItem32_Click);
            // 
            // subAgentToolStripMenuItem1
            // 
            this.subAgentToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem33,
            this.toolStripMenuItem34,
            this.outletUserLimitSetupToolStripMenuItem});
            this.subAgentToolStripMenuItem1.Name = "subAgentToolStripMenuItem1";
            this.subAgentToolStripMenuItem1.Size = new System.Drawing.Size(252, 22);
            this.subAgentToolStripMenuItem1.Text = "Outlet";
            // 
            // toolStripMenuItem33
            // 
            this.toolStripMenuItem33.Name = "toolStripMenuItem33";
            this.toolStripMenuItem33.Size = new System.Drawing.Size(203, 22);
            this.toolStripMenuItem33.Text = "Outlet Creation";
            this.toolStripMenuItem33.Click += new System.EventHandler(this.toolStripMenuItem33_Click);
            // 
            // toolStripMenuItem34
            // 
            this.toolStripMenuItem34.Name = "toolStripMenuItem34";
            this.toolStripMenuItem34.Size = new System.Drawing.Size(203, 22);
            this.toolStripMenuItem34.Text = "Outlet Search";
            this.toolStripMenuItem34.Click += new System.EventHandler(this.toolStripMenuItem34_Click);
            // 
            // outletUserLimitSetupToolStripMenuItem
            // 
            this.outletUserLimitSetupToolStripMenuItem.Name = "outletUserLimitSetupToolStripMenuItem";
            this.outletUserLimitSetupToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.outletUserLimitSetupToolStripMenuItem.Text = "Outlet User Limit Setup";
            this.outletUserLimitSetupToolStripMenuItem.Click += new System.EventHandler(this.outletUserLimitSetupToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(249, 6);
            this.toolStripSeparator6.Visible = false;
            // 
            // userManagementToolStripMenuItem
            // 
            this.userManagementToolStripMenuItem.Name = "userManagementToolStripMenuItem";
            this.userManagementToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.userManagementToolStripMenuItem.Text = "User Management";
            this.userManagementToolStripMenuItem.Click += new System.EventHandler(this.userManagementToolStripMenuItem_Click);
            // 
            // userTransferHistoryToolStripMenuItem
            // 
            this.userTransferHistoryToolStripMenuItem.Name = "userTransferHistoryToolStripMenuItem";
            this.userTransferHistoryToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.userTransferHistoryToolStripMenuItem.Text = "User Transfer History";
            this.userTransferHistoryToolStripMenuItem.Visible = false;
            this.userTransferHistoryToolStripMenuItem.Click += new System.EventHandler(this.userTransferHistoryToolStripMenuItem_Click);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(249, 6);
            // 
            // mobileNumberUpdateToolStripMenuItem
            // 
            this.mobileNumberUpdateToolStripMenuItem.Name = "mobileNumberUpdateToolStripMenuItem";
            this.mobileNumberUpdateToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.mobileNumberUpdateToolStripMenuItem.Text = "Mobile Number Update";
            this.mobileNumberUpdateToolStripMenuItem.Click += new System.EventHandler(this.mobileNumberUpdateToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(249, 6);
            // 
            // productMappingToolStripMenuItem
            // 
            this.productMappingToolStripMenuItem.Name = "productMappingToolStripMenuItem";
            this.productMappingToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.productMappingToolStripMenuItem.Text = "Product Mapping";
            this.productMappingToolStripMenuItem.Click += new System.EventHandler(this.productMappingToolStripMenuItem_Click);
            // 
            // remittanceSetupToolStripMenuItem
            // 
            this.remittanceSetupToolStripMenuItem.Name = "remittanceSetupToolStripMenuItem";
            this.remittanceSetupToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.remittanceSetupToolStripMenuItem.Text = "Remittance Setup";
            this.remittanceSetupToolStripMenuItem.Click += new System.EventHandler(this.remittanceSetupToolStripMenuItem_Click);
            // 
            // billPaymentConfigurationToolStripMenuItem1
            // 
            this.billPaymentConfigurationToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.shomityConfigurationToolStripMenuItem,
            this.zoneConfigurationToolStripMenuItem,
            this.outletMappingToolStripMenuItem});
            this.billPaymentConfigurationToolStripMenuItem1.Name = "billPaymentConfigurationToolStripMenuItem1";
            this.billPaymentConfigurationToolStripMenuItem1.Size = new System.Drawing.Size(252, 22);
            this.billPaymentConfigurationToolStripMenuItem1.Text = "Bill Payment Configuration";
            // 
            // shomityConfigurationToolStripMenuItem
            // 
            this.shomityConfigurationToolStripMenuItem.Name = "shomityConfigurationToolStripMenuItem";
            this.shomityConfigurationToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.shomityConfigurationToolStripMenuItem.Text = "Shomity Configuration";
            this.shomityConfigurationToolStripMenuItem.Click += new System.EventHandler(this.shomityConfigurationToolStripMenuItem_Click);
            // 
            // zoneConfigurationToolStripMenuItem
            // 
            this.zoneConfigurationToolStripMenuItem.Name = "zoneConfigurationToolStripMenuItem";
            this.zoneConfigurationToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.zoneConfigurationToolStripMenuItem.Text = "Zone Configuration";
            this.zoneConfigurationToolStripMenuItem.Click += new System.EventHandler(this.zoneConfigurationToolStripMenuItem_Click);
            // 
            // outletMappingToolStripMenuItem
            // 
            this.outletMappingToolStripMenuItem.Name = "outletMappingToolStripMenuItem";
            this.outletMappingToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.outletMappingToolStripMenuItem.Text = "Outlet Mapping";
            this.outletMappingToolStripMenuItem.Click += new System.EventHandler(this.outletMappingToolStripMenuItem_Click);
            // 
            // passwordResetToolStripMenuItem
            // 
            this.passwordResetToolStripMenuItem.Name = "passwordResetToolStripMenuItem";
            this.passwordResetToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.passwordResetToolStripMenuItem.Text = "Password Reset - Hidden";
            this.passwordResetToolStripMenuItem.Visible = false;
            this.passwordResetToolStripMenuItem.Click += new System.EventHandler(this.passwordResetToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(249, 6);
            // 
            // cacheToolStripMenuItem
            // 
            this.cacheToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cacheUpdateAdministrationToolStripMenuItem});
            this.cacheToolStripMenuItem.Name = "cacheToolStripMenuItem";
            this.cacheToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.cacheToolStripMenuItem.Text = "Cache";
            // 
            // cacheUpdateAdministrationToolStripMenuItem
            // 
            this.cacheUpdateAdministrationToolStripMenuItem.Name = "cacheUpdateAdministrationToolStripMenuItem";
            this.cacheUpdateAdministrationToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.cacheUpdateAdministrationToolStripMenuItem.Text = "Cache Update Administration";
            this.cacheUpdateAdministrationToolStripMenuItem.Click += new System.EventHandler(this.cacheUpdateAdministrationToolStripMenuItem_Click);
            // 
            // specialActionMenuItemToolStripMenuItem
            // 
            this.specialActionMenuItemToolStripMenuItem.Name = "specialActionMenuItemToolStripMenuItem";
            this.specialActionMenuItemToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.specialActionMenuItemToolStripMenuItem.Text = "SpecialActionMenuItem - Hidden";
            this.specialActionMenuItemToolStripMenuItem.Visible = false;
            this.specialActionMenuItemToolStripMenuItem.Click += new System.EventHandler(this.specialActionMenuItemToolStripMenuItem_Click);
            // 
            // accountEditorToolStripMenuItem
            // 
            this.accountEditorToolStripMenuItem.Name = "accountEditorToolStripMenuItem";
            this.accountEditorToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.accountEditorToolStripMenuItem.Text = "Account Information Editor";
            this.accountEditorToolStripMenuItem.Visible = false;
            this.accountEditorToolStripMenuItem.Click += new System.EventHandler(this.accountEditorToolStripMenuItem_Click);
            // 
            // usersToolStripMenuItem
            // 
            this.usersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.branchUserCreationToolStripMenuItem1});
            this.usersToolStripMenuItem.Font = new System.Drawing.Font("Tahoma", 9F);
            this.usersToolStripMenuItem.ForeColor = System.Drawing.Color.Navy;
            this.usersToolStripMenuItem.Name = "usersToolStripMenuItem";
            this.usersToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.usersToolStripMenuItem.Text = "Branch";
            this.usersToolStripMenuItem.Visible = false;
            // 
            // branchUserCreationToolStripMenuItem1
            // 
            this.branchUserCreationToolStripMenuItem1.Name = "branchUserCreationToolStripMenuItem1";
            this.branchUserCreationToolStripMenuItem1.Size = new System.Drawing.Size(188, 22);
            this.branchUserCreationToolStripMenuItem1.Text = "Branch User Creation";
            // 
            // agentToolStripMenuItem2
            // 
            this.agentToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.agentCreationToolStripMenuItem1,
            this.viewAgentProfileToolStripMenuItem,
            this.searchAgentsToolStripMenuItem1});
            this.agentToolStripMenuItem2.Font = new System.Drawing.Font("Tahoma", 9F);
            this.agentToolStripMenuItem2.ForeColor = System.Drawing.Color.Navy;
            this.agentToolStripMenuItem2.Name = "agentToolStripMenuItem2";
            this.agentToolStripMenuItem2.Size = new System.Drawing.Size(53, 20);
            this.agentToolStripMenuItem2.Text = "Agent";
            this.agentToolStripMenuItem2.Visible = false;
            // 
            // agentCreationToolStripMenuItem1
            // 
            this.agentCreationToolStripMenuItem1.Name = "agentCreationToolStripMenuItem1";
            this.agentCreationToolStripMenuItem1.Size = new System.Drawing.Size(176, 22);
            this.agentCreationToolStripMenuItem1.Text = "Agent Creation";
            // 
            // viewAgentProfileToolStripMenuItem
            // 
            this.viewAgentProfileToolStripMenuItem.Name = "viewAgentProfileToolStripMenuItem";
            this.viewAgentProfileToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.viewAgentProfileToolStripMenuItem.Text = "View Agent Profile";
            // 
            // searchAgentsToolStripMenuItem1
            // 
            this.searchAgentsToolStripMenuItem1.Name = "searchAgentsToolStripMenuItem1";
            this.searchAgentsToolStripMenuItem1.Size = new System.Drawing.Size(176, 22);
            this.searchAgentsToolStripMenuItem1.Text = "Search Agents";
            // 
            // subAgentToolStripMenuItem
            // 
            this.subAgentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subAgentCreationToolStripMenuItem1,
            this.searchSubagentToolStripMenuItem});
            this.subAgentToolStripMenuItem.Font = new System.Drawing.Font("Tahoma", 9F);
            this.subAgentToolStripMenuItem.ForeColor = System.Drawing.Color.Navy;
            this.subAgentToolStripMenuItem.Name = "subAgentToolStripMenuItem";
            this.subAgentToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.subAgentToolStripMenuItem.Text = "Sub Agent";
            this.subAgentToolStripMenuItem.Visible = false;
            // 
            // subAgentCreationToolStripMenuItem1
            // 
            this.subAgentCreationToolStripMenuItem1.Name = "subAgentCreationToolStripMenuItem1";
            this.subAgentCreationToolStripMenuItem1.Size = new System.Drawing.Size(182, 22);
            this.subAgentCreationToolStripMenuItem1.Text = "Sub Agent Creation";
            // 
            // searchSubagentToolStripMenuItem
            // 
            this.searchSubagentToolStripMenuItem.Name = "searchSubagentToolStripMenuItem";
            this.searchSubagentToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.searchSubagentToolStripMenuItem.Text = "Search Subagent";
            // 
            // customerToolStripMenuItem1
            // 
            this.customerToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.depositAccountApplicationsToolStripMenuItem,
            this.pendingCustomerApplicationsToolStripMenuItem1,
            this.sSPRequestListToolStripMenuItem1,
            this.toolStripMenuItem3,
            this.chequeRequisitionSearchToolStripMenuItem1,
            this.fingerprintAdminToolStripMenuItem,
            this.toolStripMenuItem5,
            this.accountClosingRequestsToolStripMenuItem,
            this.toolStripMenuItem13,
            this.transactionProfileToolStripMenuItem1,
            this.toolStripMenuItem6,
            this.searchAccountsByMobileNoToolStripMenuItem1,
            this.viewCustomerInfoToolStripMenuItem,
            this.toolStripMenuItem4,
            this.customerSearch2ToolStripMenuItem,
            this.individualSearch2ToolStripMenuItem});
            this.customerToolStripMenuItem1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.customerToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.customerToolStripMenuItem1.Name = "customerToolStripMenuItem1";
            this.customerToolStripMenuItem1.Size = new System.Drawing.Size(71, 20);
            this.customerToolStripMenuItem1.Text = "Customer";
            this.customerToolStripMenuItem1.Click += new System.EventHandler(this.customerToolStripMenuItem1_Click);
            // 
            // depositAccountApplicationsToolStripMenuItem
            // 
            this.depositAccountApplicationsToolStripMenuItem.Name = "depositAccountApplicationsToolStripMenuItem";
            this.depositAccountApplicationsToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.depositAccountApplicationsToolStripMenuItem.Text = "Deposit Account Applications - Hidden";
            this.depositAccountApplicationsToolStripMenuItem.Visible = false;
            this.depositAccountApplicationsToolStripMenuItem.Click += new System.EventHandler(this.depositAccountApplicationsToolStripMenuItem_Click);
            // 
            // pendingCustomerApplicationsToolStripMenuItem1
            // 
            this.pendingCustomerApplicationsToolStripMenuItem1.Name = "pendingCustomerApplicationsToolStripMenuItem1";
            this.pendingCustomerApplicationsToolStripMenuItem1.Size = new System.Drawing.Size(283, 22);
            this.pendingCustomerApplicationsToolStripMenuItem1.Text = "Pending Customer Applications";
            this.pendingCustomerApplicationsToolStripMenuItem1.Click += new System.EventHandler(this.pendingCustomerApplicationsToolStripMenuItem1_Click);
            // 
            // sSPRequestListToolStripMenuItem1
            // 
            this.sSPRequestListToolStripMenuItem1.Name = "sSPRequestListToolStripMenuItem1";
            this.sSPRequestListToolStripMenuItem1.Size = new System.Drawing.Size(283, 22);
            this.sSPRequestListToolStripMenuItem1.Text = "Term Request Search";
            this.sSPRequestListToolStripMenuItem1.Click += new System.EventHandler(this.sSPRequestListToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(280, 6);
            // 
            // chequeRequisitionSearchToolStripMenuItem1
            // 
            this.chequeRequisitionSearchToolStripMenuItem1.Name = "chequeRequisitionSearchToolStripMenuItem1";
            this.chequeRequisitionSearchToolStripMenuItem1.Size = new System.Drawing.Size(283, 22);
            this.chequeRequisitionSearchToolStripMenuItem1.Text = "Cheque Requisition Search";
            this.chequeRequisitionSearchToolStripMenuItem1.Visible = false;
            this.chequeRequisitionSearchToolStripMenuItem1.Click += new System.EventHandler(this.chequeRequisitionSearchToolStripMenuItem1_Click);
            // 
            // fingerprintAdminToolStripMenuItem
            // 
            this.fingerprintAdminToolStripMenuItem.Name = "fingerprintAdminToolStripMenuItem";
            this.fingerprintAdminToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.fingerprintAdminToolStripMenuItem.Text = "Fingerprint Request Search";
            this.fingerprintAdminToolStripMenuItem.Click += new System.EventHandler(this.fingerprintAdminToolStripMenuItem_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(280, 6);
            // 
            // accountClosingRequestsToolStripMenuItem
            // 
            this.accountClosingRequestsToolStripMenuItem.Name = "accountClosingRequestsToolStripMenuItem";
            this.accountClosingRequestsToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.accountClosingRequestsToolStripMenuItem.Text = "Account Closing Requests";
            this.accountClosingRequestsToolStripMenuItem.Click += new System.EventHandler(this.accountClosingRequestsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(280, 6);
            // 
            // transactionProfileToolStripMenuItem1
            // 
            this.transactionProfileToolStripMenuItem1.Name = "transactionProfileToolStripMenuItem1";
            this.transactionProfileToolStripMenuItem1.Size = new System.Drawing.Size(283, 22);
            this.transactionProfileToolStripMenuItem1.Text = "Transaction Profile";
            this.transactionProfileToolStripMenuItem1.Click += new System.EventHandler(this.transactionProfileToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(280, 6);
            // 
            // searchAccountsByMobileNoToolStripMenuItem1
            // 
            this.searchAccountsByMobileNoToolStripMenuItem1.Name = "searchAccountsByMobileNoToolStripMenuItem1";
            this.searchAccountsByMobileNoToolStripMenuItem1.Size = new System.Drawing.Size(283, 22);
            this.searchAccountsByMobileNoToolStripMenuItem1.Text = "Search Accounts By Mobile No.";
            this.searchAccountsByMobileNoToolStripMenuItem1.Click += new System.EventHandler(this.searchAccountsByMobileNoToolStripMenuItem1_Click);
            // 
            // viewCustomerInfoToolStripMenuItem
            // 
            this.viewCustomerInfoToolStripMenuItem.Name = "viewCustomerInfoToolStripMenuItem";
            this.viewCustomerInfoToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.viewCustomerInfoToolStripMenuItem.Text = "View Customer Info";
            this.viewCustomerInfoToolStripMenuItem.Visible = false;
            this.viewCustomerInfoToolStripMenuItem.Click += new System.EventHandler(this.viewCustomerInfoToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(280, 6);
            this.toolStripMenuItem4.Visible = false;
            // 
            // customerSearch2ToolStripMenuItem
            // 
            this.customerSearch2ToolStripMenuItem.Name = "customerSearch2ToolStripMenuItem";
            this.customerSearch2ToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.customerSearch2ToolStripMenuItem.Text = "Customer Search 2 - Hidden";
            this.customerSearch2ToolStripMenuItem.Visible = false;
            this.customerSearch2ToolStripMenuItem.Click += new System.EventHandler(this.customerSearch2ToolStripMenuItem_Click);
            // 
            // individualSearch2ToolStripMenuItem
            // 
            this.individualSearch2ToolStripMenuItem.Name = "individualSearch2ToolStripMenuItem";
            this.individualSearch2ToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.individualSearch2ToolStripMenuItem.Text = "Individual Search 2 - Hidden";
            this.individualSearch2ToolStripMenuItem.Visible = false;
            this.individualSearch2ToolStripMenuItem.Click += new System.EventHandler(this.individualSearch2ToolStripMenuItem_Click);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.transactionalToolStripMenuItem1,
            this.applicationToolStripMenuItem,
            this.accountToolStripMenuItem1,
            this.remittanceToolStripMenuItem2,
            this.monitoringToolStripMenuItem1,
            this.outletToolStripMenuItem1,
            this.billPaymentToolStripMenuItem2,
            this.chequeToolStripMenuItem,
            this.agnetToolStripMenuItem,
            this.branchUserInformationToolStripMenuItem,
            this.dailyReportToolStripMenuItem1,
            this.onlineReportToolStripMenuItem});
            this.reportsToolStripMenuItem.Font = new System.Drawing.Font("Tahoma", 9F);
            this.reportsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.reportsToolStripMenuItem.Text = "Reports";
            // 
            // transactionalToolStripMenuItem1
            // 
            this.transactionalToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem18,
            this.transactionReportToolStripMenuItem1,
            this.transactionMonitoringReportToolStripMenuItem});
            this.transactionalToolStripMenuItem1.Name = "transactionalToolStripMenuItem1";
            this.transactionalToolStripMenuItem1.Size = new System.Drawing.Size(206, 22);
            this.transactionalToolStripMenuItem1.Text = "Transactional";
            // 
            // toolStripMenuItem18
            // 
            this.toolStripMenuItem18.Name = "toolStripMenuItem18";
            this.toolStripMenuItem18.Size = new System.Drawing.Size(239, 22);
            this.toolStripMenuItem18.Text = "Agent Cash Register";
            this.toolStripMenuItem18.Click += new System.EventHandler(this.toolStripMenuItem18_Click);
            // 
            // transactionReportToolStripMenuItem1
            // 
            this.transactionReportToolStripMenuItem1.Name = "transactionReportToolStripMenuItem1";
            this.transactionReportToolStripMenuItem1.Size = new System.Drawing.Size(239, 22);
            this.transactionReportToolStripMenuItem1.Text = "Transaction Report";
            this.transactionReportToolStripMenuItem1.Click += new System.EventHandler(this.transactionReportToolStripMenuItem1_Click);
            // 
            // transactionMonitoringReportToolStripMenuItem
            // 
            this.transactionMonitoringReportToolStripMenuItem.Name = "transactionMonitoringReportToolStripMenuItem";
            this.transactionMonitoringReportToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.transactionMonitoringReportToolStripMenuItem.Text = "Transaction Monitoring Report";
            this.transactionMonitoringReportToolStripMenuItem.Click += new System.EventHandler(this.transactionMonitoringReportToolStripMenuItem_Click);
            // 
            // applicationToolStripMenuItem
            // 
            this.applicationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nidVerificationReportToolStripMenuItem,
            this.applicationMonitoringToolStripMenuItem1});
            this.applicationToolStripMenuItem.Name = "applicationToolStripMenuItem";
            this.applicationToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.applicationToolStripMenuItem.Text = "Application";
            // 
            // nidVerificationReportToolStripMenuItem
            // 
            this.nidVerificationReportToolStripMenuItem.Name = "nidVerificationReportToolStripMenuItem";
            this.nidVerificationReportToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.nidVerificationReportToolStripMenuItem.Text = "Nid Verification Report";
            this.nidVerificationReportToolStripMenuItem.Click += new System.EventHandler(this.nidVerificationReportToolStripMenuItem_Click);
            // 
            // applicationMonitoringToolStripMenuItem1
            // 
            this.applicationMonitoringToolStripMenuItem1.Name = "applicationMonitoringToolStripMenuItem1";
            this.applicationMonitoringToolStripMenuItem1.Size = new System.Drawing.Size(196, 22);
            this.applicationMonitoringToolStripMenuItem1.Text = "Application Monitoring";
            this.applicationMonitoringToolStripMenuItem1.Click += new System.EventHandler(this.applicationMonitoringToolStripMenuItem1_Click);
            // 
            // accountToolStripMenuItem1
            // 
            this.accountToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accountInformationToolStripMenuItem,
            this.toolStripMenuItem19,
            this.toolStripMenuItem20,
            this.itdBalanceOutsaandingToolStripMenuItem,
            this.rejectedConsumerApplicationToolStripMenuItem,
            this.userIdWiseAccounrApproveToolStripMenuItem});
            this.accountToolStripMenuItem1.Name = "accountToolStripMenuItem1";
            this.accountToolStripMenuItem1.Size = new System.Drawing.Size(206, 22);
            this.accountToolStripMenuItem1.Text = "Account";
            // 
            // accountInformationToolStripMenuItem
            // 
            this.accountInformationToolStripMenuItem.Name = "accountInformationToolStripMenuItem";
            this.accountInformationToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.accountInformationToolStripMenuItem.Text = "Account Information";
            this.accountInformationToolStripMenuItem.Visible = false;
            this.accountInformationToolStripMenuItem.Click += new System.EventHandler(this.accountInformationToolStripMenuItem_Click);
            // 
            // toolStripMenuItem19
            // 
            this.toolStripMenuItem19.Name = "toolStripMenuItem19";
            this.toolStripMenuItem19.Size = new System.Drawing.Size(270, 22);
            this.toolStripMenuItem19.Text = "Account Balance as on Date";
            this.toolStripMenuItem19.Click += new System.EventHandler(this.toolStripMenuItem19_Click);
            // 
            // toolStripMenuItem20
            // 
            this.toolStripMenuItem20.Name = "toolStripMenuItem20";
            this.toolStripMenuItem20.Size = new System.Drawing.Size(270, 22);
            this.toolStripMenuItem20.Text = "Account Opening Register";
            this.toolStripMenuItem20.Click += new System.EventHandler(this.toolStripMenuItem20_Click);
            // 
            // itdBalanceOutsaandingToolStripMenuItem
            // 
            this.itdBalanceOutsaandingToolStripMenuItem.Name = "itdBalanceOutsaandingToolStripMenuItem";
            this.itdBalanceOutsaandingToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.itdBalanceOutsaandingToolStripMenuItem.Text = "ITD Outstanding Balance";
            this.itdBalanceOutsaandingToolStripMenuItem.Click += new System.EventHandler(this.itdBalanceOutsaandingToolStripMenuItem_Click);
            // 
            // rejectedConsumerApplicationToolStripMenuItem
            // 
            this.rejectedConsumerApplicationToolStripMenuItem.Name = "rejectedConsumerApplicationToolStripMenuItem";
            this.rejectedConsumerApplicationToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.rejectedConsumerApplicationToolStripMenuItem.Text = "Rejected Consumer Application Info";
            this.rejectedConsumerApplicationToolStripMenuItem.Click += new System.EventHandler(this.rejectedConsumerApplicationToolStripMenuItem_Click);
            // 
            // userIdWiseAccounrApproveToolStripMenuItem
            // 
            this.userIdWiseAccounrApproveToolStripMenuItem.Name = "userIdWiseAccounrApproveToolStripMenuItem";
            this.userIdWiseAccounrApproveToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.userIdWiseAccounrApproveToolStripMenuItem.Text = "User Id Wise Accounr Approve";
            this.userIdWiseAccounrApproveToolStripMenuItem.Click += new System.EventHandler(this.userIdWiseAccounrApproveToolStripMenuItem_Click);
            // 
            // remittanceToolStripMenuItem2
            // 
            this.remittanceToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem21});
            this.remittanceToolStripMenuItem2.Name = "remittanceToolStripMenuItem2";
            this.remittanceToolStripMenuItem2.Size = new System.Drawing.Size(206, 22);
            this.remittanceToolStripMenuItem2.Text = "Remittance";
            // 
            // toolStripMenuItem21
            // 
            this.toolStripMenuItem21.Name = "toolStripMenuItem21";
            this.toolStripMenuItem21.Size = new System.Drawing.Size(203, 22);
            this.toolStripMenuItem21.Text = "Remittance Information";
            this.toolStripMenuItem21.Click += new System.EventHandler(this.toolStripMenuItem21_Click);
            // 
            // monitoringToolStripMenuItem1
            // 
            this.monitoringToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem22,
            this.toolStripMenuItem23,
            this.agentChargeCommissionToolStripMenuItem,
            this.toolStripMenuItem24,
            this.toolStripMenuItem25,
            this.agentIncomeToolStripMenuItem1,
            this.dailyReportToolStripMenuItem});
            this.monitoringToolStripMenuItem1.Name = "monitoringToolStripMenuItem1";
            this.monitoringToolStripMenuItem1.Size = new System.Drawing.Size(206, 22);
            this.monitoringToolStripMenuItem1.Text = "Monitoring";
            // 
            // toolStripMenuItem22
            // 
            this.toolStripMenuItem22.Name = "toolStripMenuItem22";
            this.toolStripMenuItem22.Size = new System.Drawing.Size(216, 22);
            this.toolStripMenuItem22.Text = "Account Monitoring";
            this.toolStripMenuItem22.Click += new System.EventHandler(this.toolStripMenuItem22_Click);
            // 
            // toolStripMenuItem23
            // 
            this.toolStripMenuItem23.Name = "toolStripMenuItem23";
            this.toolStripMenuItem23.Size = new System.Drawing.Size(216, 22);
            this.toolStripMenuItem23.Text = "Transaction Monitoring";
            this.toolStripMenuItem23.Visible = false;
            this.toolStripMenuItem23.Click += new System.EventHandler(this.toolStripMenuItem23_Click);
            // 
            // agentChargeCommissionToolStripMenuItem
            // 
            this.agentChargeCommissionToolStripMenuItem.Name = "agentChargeCommissionToolStripMenuItem";
            this.agentChargeCommissionToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.agentChargeCommissionToolStripMenuItem.Text = "Agent Charge Commission";
            this.agentChargeCommissionToolStripMenuItem.Visible = false;
            this.agentChargeCommissionToolStripMenuItem.Click += new System.EventHandler(this.agentChargeCommissionToolStripMenuItem_Click);
            // 
            // toolStripMenuItem24
            // 
            this.toolStripMenuItem24.Name = "toolStripMenuItem24";
            this.toolStripMenuItem24.Size = new System.Drawing.Size(216, 22);
            this.toolStripMenuItem24.Text = "Agent Commission";
            this.toolStripMenuItem24.Visible = false;
            this.toolStripMenuItem24.Click += new System.EventHandler(this.toolStripMenuItem24_Click);
            // 
            // toolStripMenuItem25
            // 
            this.toolStripMenuItem25.Name = "toolStripMenuItem25";
            this.toolStripMenuItem25.Size = new System.Drawing.Size(216, 22);
            this.toolStripMenuItem25.Text = "Agent Income Statement";
            this.toolStripMenuItem25.Visible = false;
            this.toolStripMenuItem25.Click += new System.EventHandler(this.toolStripMenuItem25_Click);
            // 
            // agentIncomeToolStripMenuItem1
            // 
            this.agentIncomeToolStripMenuItem1.Name = "agentIncomeToolStripMenuItem1";
            this.agentIncomeToolStripMenuItem1.Size = new System.Drawing.Size(216, 22);
            this.agentIncomeToolStripMenuItem1.Text = "Agent Income";
            this.agentIncomeToolStripMenuItem1.Click += new System.EventHandler(this.agentIncomeToolStripMenuItem1_Click);
            // 
            // dailyReportToolStripMenuItem
            // 
            this.dailyReportToolStripMenuItem.Name = "dailyReportToolStripMenuItem";
            this.dailyReportToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.dailyReportToolStripMenuItem.Text = "Daily Report";
            this.dailyReportToolStripMenuItem.Visible = false;
            this.dailyReportToolStripMenuItem.Click += new System.EventHandler(this.dailyReportToolStripMenuItem_Click);
            // 
            // outletToolStripMenuItem1
            // 
            this.outletToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cashInformationToolStripMenuItem,
            this.outletInformationToolStripMenuItem,
            this.outletUserInformationToolStripMenuItem});
            this.outletToolStripMenuItem1.Name = "outletToolStripMenuItem1";
            this.outletToolStripMenuItem1.Size = new System.Drawing.Size(206, 22);
            this.outletToolStripMenuItem1.Text = "Outlet";
            // 
            // cashInformationToolStripMenuItem
            // 
            this.cashInformationToolStripMenuItem.Name = "cashInformationToolStripMenuItem";
            this.cashInformationToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.cashInformationToolStripMenuItem.Text = "Cash Information";
            this.cashInformationToolStripMenuItem.Click += new System.EventHandler(this.cashInformationToolStripMenuItem_Click);
            // 
            // outletInformationToolStripMenuItem
            // 
            this.outletInformationToolStripMenuItem.Name = "outletInformationToolStripMenuItem";
            this.outletInformationToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.outletInformationToolStripMenuItem.Text = "Outlet Information";
            this.outletInformationToolStripMenuItem.Click += new System.EventHandler(this.outletInformationToolStripMenuItem_Click);
            // 
            // outletUserInformationToolStripMenuItem
            // 
            this.outletUserInformationToolStripMenuItem.Name = "outletUserInformationToolStripMenuItem";
            this.outletUserInformationToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.outletUserInformationToolStripMenuItem.Text = "Outlet User Information";
            this.outletUserInformationToolStripMenuItem.Click += new System.EventHandler(this.outletUserInformationToolStripMenuItem_Click);
            // 
            // billPaymentToolStripMenuItem2
            // 
            this.billPaymentToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.billPaymentInformationToolStripMenuItem1,
            this.billMonitoringReportToolStripMenuItem,
            this.dailyElectricityBillCollectionToolStripMenuItem});
            this.billPaymentToolStripMenuItem2.Name = "billPaymentToolStripMenuItem2";
            this.billPaymentToolStripMenuItem2.Size = new System.Drawing.Size(206, 22);
            this.billPaymentToolStripMenuItem2.Text = "Bill Payment";
            // 
            // billPaymentInformationToolStripMenuItem1
            // 
            this.billPaymentInformationToolStripMenuItem1.Name = "billPaymentInformationToolStripMenuItem1";
            this.billPaymentInformationToolStripMenuItem1.Size = new System.Drawing.Size(271, 22);
            this.billPaymentInformationToolStripMenuItem1.Text = "Bill Payment Information - [HIDDEN]";
            this.billPaymentInformationToolStripMenuItem1.Visible = false;
            this.billPaymentInformationToolStripMenuItem1.Click += new System.EventHandler(this.billPaymentInformationToolStripMenuItem1_Click);
            // 
            // billMonitoringReportToolStripMenuItem
            // 
            this.billMonitoringReportToolStripMenuItem.Name = "billMonitoringReportToolStripMenuItem";
            this.billMonitoringReportToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.billMonitoringReportToolStripMenuItem.Text = "Bill Monitoring Report";
            this.billMonitoringReportToolStripMenuItem.Click += new System.EventHandler(this.billMonitoringReportToolStripMenuItem_Click);
            // 
            // dailyElectricityBillCollectionToolStripMenuItem
            // 
            this.dailyElectricityBillCollectionToolStripMenuItem.Name = "dailyElectricityBillCollectionToolStripMenuItem";
            this.dailyElectricityBillCollectionToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.dailyElectricityBillCollectionToolStripMenuItem.Text = "Daily Electricity Bill Collection";
            this.dailyElectricityBillCollectionToolStripMenuItem.Click += new System.EventHandler(this.dailyElectricityBillCollectionToolStripMenuItem_Click);
            // 
            // chequeToolStripMenuItem
            // 
            this.chequeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.chequeRequsitionReportToolStripMenuItem,
            this.chequeDeliveryInformationToolStripMenuItem,
            this.chequeBookInformationToolStripMenuItem});
            this.chequeToolStripMenuItem.Name = "chequeToolStripMenuItem";
            this.chequeToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.chequeToolStripMenuItem.Text = "Cheque ";
            // 
            // chequeRequsitionReportToolStripMenuItem
            // 
            this.chequeRequsitionReportToolStripMenuItem.Name = "chequeRequsitionReportToolStripMenuItem";
            this.chequeRequsitionReportToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.chequeRequsitionReportToolStripMenuItem.Text = "Cheque Requsition Report";
            this.chequeRequsitionReportToolStripMenuItem.Click += new System.EventHandler(this.chequeRequsitionReportToolStripMenuItem_Click);
            // 
            // chequeDeliveryInformationToolStripMenuItem
            // 
            this.chequeDeliveryInformationToolStripMenuItem.Name = "chequeDeliveryInformationToolStripMenuItem";
            this.chequeDeliveryInformationToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.chequeDeliveryInformationToolStripMenuItem.Text = "Cheque Delivery Information";
            this.chequeDeliveryInformationToolStripMenuItem.Click += new System.EventHandler(this.chequeDeliveryInformationToolStripMenuItem_Click);
            // 
            // chequeBookInformationToolStripMenuItem
            // 
            this.chequeBookInformationToolStripMenuItem.Name = "chequeBookInformationToolStripMenuItem";
            this.chequeBookInformationToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.chequeBookInformationToolStripMenuItem.Text = "Cheque Book Information";
            this.chequeBookInformationToolStripMenuItem.Click += new System.EventHandler(this.chequeBookInformationToolStripMenuItem_Click);
            // 
            // agnetToolStripMenuItem
            // 
            this.agnetToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.agentInformationToolStripMenuItem,
            this.agentCashInformationToolStripMenuItem,
            this.agentUserInformattionToolStripMenuItem});
            this.agnetToolStripMenuItem.Name = "agnetToolStripMenuItem";
            this.agnetToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.agnetToolStripMenuItem.Text = "Agent";
            // 
            // agentInformationToolStripMenuItem
            // 
            this.agentInformationToolStripMenuItem.Name = "agentInformationToolStripMenuItem";
            this.agentInformationToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.agentInformationToolStripMenuItem.Text = "Agent Information";
            this.agentInformationToolStripMenuItem.Click += new System.EventHandler(this.agentInformationToolStripMenuItem_Click);
            // 
            // agentCashInformationToolStripMenuItem
            // 
            this.agentCashInformationToolStripMenuItem.Name = "agentCashInformationToolStripMenuItem";
            this.agentCashInformationToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.agentCashInformationToolStripMenuItem.Text = "Agent Balance Information";
            this.agentCashInformationToolStripMenuItem.Click += new System.EventHandler(this.agentCashInformationToolStripMenuItem_Click);
            // 
            // agentUserInformattionToolStripMenuItem
            // 
            this.agentUserInformattionToolStripMenuItem.Name = "agentUserInformattionToolStripMenuItem";
            this.agentUserInformattionToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.agentUserInformattionToolStripMenuItem.Text = "Agent User Informattion";
            this.agentUserInformattionToolStripMenuItem.Click += new System.EventHandler(this.agentUserInformattionToolStripMenuItem_Click);
            // 
            // branchUserInformationToolStripMenuItem
            // 
            this.branchUserInformationToolStripMenuItem.Name = "branchUserInformationToolStripMenuItem";
            this.branchUserInformationToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.branchUserInformationToolStripMenuItem.Text = "Branch User Information";
            this.branchUserInformationToolStripMenuItem.Click += new System.EventHandler(this.branchUserInformationToolStripMenuItem_Click);
            // 
            // dailyReportToolStripMenuItem1
            // 
            this.dailyReportToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dailyPositionReportToolStripMenuItem,
            this.daaToolStripMenuItem});
            this.dailyReportToolStripMenuItem1.Name = "dailyReportToolStripMenuItem1";
            this.dailyReportToolStripMenuItem1.Size = new System.Drawing.Size(206, 22);
            this.dailyReportToolStripMenuItem1.Text = "Daily Report";
            // 
            // dailyPositionReportToolStripMenuItem
            // 
            this.dailyPositionReportToolStripMenuItem.Name = "dailyPositionReportToolStripMenuItem";
            this.dailyPositionReportToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.dailyPositionReportToolStripMenuItem.Text = "Daily Position Report";
            this.dailyPositionReportToolStripMenuItem.Click += new System.EventHandler(this.dailyPositionReportToolStripMenuItem_Click);
            // 
            // daaToolStripMenuItem
            // 
            this.daaToolStripMenuItem.Name = "daaToolStripMenuItem";
            this.daaToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.daaToolStripMenuItem.Text = "Daily Report";
            this.daaToolStripMenuItem.Click += new System.EventHandler(this.daaToolStripMenuItem_Click);
            // 
            // onlineReportToolStripMenuItem
            // 
            this.onlineReportToolStripMenuItem.Name = "onlineReportToolStripMenuItem";
            this.onlineReportToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.onlineReportToolStripMenuItem.Text = "Online Report";
            this.onlineReportToolStripMenuItem.Visible = false;
            this.onlineReportToolStripMenuItem.Click += new System.EventHandler(this.onlineReportToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem35,
            this.exchangeHouseSetupToolStripMenuItem,
            this.billPaymentConfigurationToolStripMenuItem,
            this.toolStripMenuItem36});
            this.settingsToolStripMenuItem.Font = new System.Drawing.Font("Tahoma", 9F);
            this.settingsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // toolStripMenuItem35
            // 
            this.toolStripMenuItem35.Name = "toolStripMenuItem35";
            this.toolStripMenuItem35.Size = new System.Drawing.Size(202, 22);
            this.toolStripMenuItem35.Text = "Change Password";
            this.toolStripMenuItem35.Click += new System.EventHandler(this.toolStripMenuItem35_Click);
            // 
            // exchangeHouseSetupToolStripMenuItem
            // 
            this.exchangeHouseSetupToolStripMenuItem.Name = "exchangeHouseSetupToolStripMenuItem";
            this.exchangeHouseSetupToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.exchangeHouseSetupToolStripMenuItem.Text = "Exchange House Setup";
            this.exchangeHouseSetupToolStripMenuItem.Visible = false;
            this.exchangeHouseSetupToolStripMenuItem.Click += new System.EventHandler(this.exchangeHouseSetupToolStripMenuItem_Click);
            // 
            // billPaymentConfigurationToolStripMenuItem
            // 
            this.billPaymentConfigurationToolStripMenuItem.Name = "billPaymentConfigurationToolStripMenuItem";
            this.billPaymentConfigurationToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.billPaymentConfigurationToolStripMenuItem.Text = "Bill Payment Setup";
            this.billPaymentConfigurationToolStripMenuItem.Visible = false;
            this.billPaymentConfigurationToolStripMenuItem.Click += new System.EventHandler(this.billPaymentConfigurationToolStripMenuItem_Click);
            // 
            // toolStripMenuItem36
            // 
            this.toolStripMenuItem36.Name = "toolStripMenuItem36";
            this.toolStripMenuItem36.Size = new System.Drawing.Size(202, 22);
            this.toolStripMenuItem36.Text = "Reset Cache";
            this.toolStripMenuItem36.Click += new System.EventHandler(this.toolStripMenuItem36_Click);
            // 
            // toolStripMenuItem26
            // 
            this.toolStripMenuItem26.Font = new System.Drawing.Font("Tahoma", 9F);
            this.toolStripMenuItem26.ForeColor = System.Drawing.Color.Navy;
            this.toolStripMenuItem26.Name = "toolStripMenuItem26";
            this.toolStripMenuItem26.Size = new System.Drawing.Size(115, 20);
            this.toolStripMenuItem26.Text = "Change Password";
            this.toolStripMenuItem26.Visible = false;
            // 
            // tESTToolStripMenuItem
            // 
            this.tESTToolStripMenuItem.Font = new System.Drawing.Font("Tahoma", 9F);
            this.tESTToolStripMenuItem.ForeColor = System.Drawing.Color.Navy;
            this.tESTToolStripMenuItem.Name = "tESTToolStripMenuItem";
            this.tESTToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.tESTToolStripMenuItem.Text = "TEST";
            this.tESTToolStripMenuItem.Visible = false;
            // 
            // reportTestToolStripMenuItem
            // 
            this.reportTestToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mTDRToolStripMenuItem});
            this.reportTestToolStripMenuItem.Font = new System.Drawing.Font("Tahoma", 9F);
            this.reportTestToolStripMenuItem.ForeColor = System.Drawing.Color.Navy;
            this.reportTestToolStripMenuItem.Name = "reportTestToolStripMenuItem";
            this.reportTestToolStripMenuItem.Size = new System.Drawing.Size(85, 20);
            this.reportTestToolStripMenuItem.Text = "Report Test";
            this.reportTestToolStripMenuItem.Visible = false;
            // 
            // mTDRToolStripMenuItem
            // 
            this.mTDRToolStripMenuItem.Name = "mTDRToolStripMenuItem";
            this.mTDRToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.mTDRToolStripMenuItem.Text = "MTDR";
            // 
            // developmentToolStripMenuItem1
            // 
            this.developmentToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pendingApplicationSearchToolStripMenuItem1,
            this.toolStripMenuItem1,
            this.workstationManagerToolStripMenuItem,
            this.workstationAllocationToolStripMenuItem,
            this.agentUserInfoToolStripMenuItem,
            this.branchUserInfoToolStripMenuItem,
            this.outletAccToolStripMenuItem,
            this.rermCloseToolStripMenuItem,
            this.useridWiseRptToolStripMenuItem,
            this.nidVerificationToolStripMenuItem,
            this.daillyPositionToolStripMenuItem});
            this.developmentToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.developmentToolStripMenuItem1.Name = "developmentToolStripMenuItem1";
            this.developmentToolStripMenuItem1.Size = new System.Drawing.Size(92, 20);
            this.developmentToolStripMenuItem1.Text = "Development";
            this.developmentToolStripMenuItem1.Visible = false;
            // 
            // pendingApplicationSearchToolStripMenuItem1
            // 
            this.pendingApplicationSearchToolStripMenuItem1.Name = "pendingApplicationSearchToolStripMenuItem1";
            this.pendingApplicationSearchToolStripMenuItem1.Size = new System.Drawing.Size(222, 22);
            this.pendingApplicationSearchToolStripMenuItem1.Text = "Pending Application Search";
            this.pendingApplicationSearchToolStripMenuItem1.Click += new System.EventHandler(this.pendingApplicationSearchToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(219, 6);
            // 
            // workstationManagerToolStripMenuItem
            // 
            this.workstationManagerToolStripMenuItem.Name = "workstationManagerToolStripMenuItem";
            this.workstationManagerToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.workstationManagerToolStripMenuItem.Text = "Workstation Manager";
            this.workstationManagerToolStripMenuItem.Click += new System.EventHandler(this.workstationManagerToolStripMenuItem_Click);
            // 
            // workstationAllocationToolStripMenuItem
            // 
            this.workstationAllocationToolStripMenuItem.Name = "workstationAllocationToolStripMenuItem";
            this.workstationAllocationToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.workstationAllocationToolStripMenuItem.Text = "Workstation Allocation";
            this.workstationAllocationToolStripMenuItem.Click += new System.EventHandler(this.workstationAllocationToolStripMenuItem_Click);
            // 
            // agentUserInfoToolStripMenuItem
            // 
            this.agentUserInfoToolStripMenuItem.Name = "agentUserInfoToolStripMenuItem";
            this.agentUserInfoToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.agentUserInfoToolStripMenuItem.Text = "agent user info";
            this.agentUserInfoToolStripMenuItem.Click += new System.EventHandler(this.agentUserInfoToolStripMenuItem_Click);
            // 
            // branchUserInfoToolStripMenuItem
            // 
            this.branchUserInfoToolStripMenuItem.Name = "branchUserInfoToolStripMenuItem";
            this.branchUserInfoToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.branchUserInfoToolStripMenuItem.Text = "branch user info";
            this.branchUserInfoToolStripMenuItem.Click += new System.EventHandler(this.branchUserInfoToolStripMenuItem_Click);
            // 
            // outletAccToolStripMenuItem
            // 
            this.outletAccToolStripMenuItem.Name = "outletAccToolStripMenuItem";
            this.outletAccToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.outletAccToolStripMenuItem.Text = "outlet acc";
            this.outletAccToolStripMenuItem.Click += new System.EventHandler(this.outletAccToolStripMenuItem_Click);
            // 
            // rermCloseToolStripMenuItem
            // 
            this.rermCloseToolStripMenuItem.Name = "rermCloseToolStripMenuItem";
            this.rermCloseToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.rermCloseToolStripMenuItem.Text = "rerm close";
            this.rermCloseToolStripMenuItem.Click += new System.EventHandler(this.rermCloseToolStripMenuItem_Click);
            // 
            // useridWiseRptToolStripMenuItem
            // 
            this.useridWiseRptToolStripMenuItem.Name = "useridWiseRptToolStripMenuItem";
            this.useridWiseRptToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.useridWiseRptToolStripMenuItem.Text = "userid wise rpt";
            this.useridWiseRptToolStripMenuItem.Click += new System.EventHandler(this.useridWiseRptToolStripMenuItem_Click);
            // 
            // nidVerificationToolStripMenuItem
            // 
            this.nidVerificationToolStripMenuItem.Name = "nidVerificationToolStripMenuItem";
            this.nidVerificationToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.nidVerificationToolStripMenuItem.Text = "nid verification";
            this.nidVerificationToolStripMenuItem.Click += new System.EventHandler(this.nidVerificationToolStripMenuItem_Click);
            // 
            // daillyPositionToolStripMenuItem
            // 
            this.daillyPositionToolStripMenuItem.Name = "daillyPositionToolStripMenuItem";
            this.daillyPositionToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.daillyPositionToolStripMenuItem.Text = "dailly position";
            this.daillyPositionToolStripMenuItem.Click += new System.EventHandler(this.daillyPositionToolStripMenuItem_Click);
            // 
            // notificationToolStripMenuItem
            // 
            this.notificationToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.notificationToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.notificationToolStripMenuItem.Image = global::MISL.Ababil.Agent.UI.Properties.Resources.Comments_16;
            this.notificationToolStripMenuItem.Name = "notificationToolStripMenuItem";
            this.notificationToolStripMenuItem.Size = new System.Drawing.Size(28, 20);
            this.notificationToolStripMenuItem.Visible = false;
            this.notificationToolStripMenuItem.Click += new System.EventHandler(this.notificationToolStripMenuItem_Click);
            // 
            // userLimitToolStripMenuItem
            // 
            this.userLimitToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setupLimitToolStripMenuItem});
            this.userLimitToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.userLimitToolStripMenuItem.Name = "userLimitToolStripMenuItem";
            this.userLimitToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.userLimitToolStripMenuItem.Text = "User Limit";
            // 
            // setupLimitToolStripMenuItem
            // 
            this.setupLimitToolStripMenuItem.Name = "setupLimitToolStripMenuItem";
            this.setupLimitToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.setupLimitToolStripMenuItem.Text = "Setup Limit";
            this.setupLimitToolStripMenuItem.Click += new System.EventHandler(this.setupLimitToolStripMenuItem_Click);
            // 
            // servicesToolStripMenuItem
            // 
            this.servicesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.billPaymentsToolStripMenuItem1});
            this.servicesToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.servicesToolStripMenuItem.Name = "servicesToolStripMenuItem";
            this.servicesToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.servicesToolStripMenuItem.Text = "Services";
            // 
            // billPaymentsToolStripMenuItem1
            // 
            this.billPaymentsToolStripMenuItem1.Name = "billPaymentsToolStripMenuItem1";
            this.billPaymentsToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.billPaymentsToolStripMenuItem1.Text = "Bill Payments";
            this.billPaymentsToolStripMenuItem1.Click += new System.EventHandler(this.billPaymentsToolStripMenuItem1_Click);
            // 
            // BranchMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1164, 24);
            this.ControlBox = false;
            this.Controls.Add(this.menuBranch);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BranchMenu";
            this.Text = "BranchMenu";
            this.menuBranch.ResumeLayout(false);
            this.menuBranch.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStripMenuItem administrativeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem branchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem29;
        private System.Windows.Forms.ToolStripMenuItem agentToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem30;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem31;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem32;
        private System.Windows.Forms.ToolStripMenuItem subAgentToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem33;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem34;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem userManagementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem passwordResetToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem cacheToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cacheUpdateAdministrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem specialActionMenuItemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountEditorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem branchUserCreationToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem agentToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem agentCreationToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem viewAgentProfileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchAgentsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem subAgentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subAgentCreationToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem searchSubagentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pendingCustomerApplicationsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sSPRequestListToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem transactionProfileToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem viewCustomerInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chequeRequisitionSearchToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem fingerprintAdminToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchAccountsByMobileNoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem customerSearch2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem individualSearch2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transactionalToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem18;
        private System.Windows.Forms.ToolStripMenuItem transactionReportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem accountToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem19;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem20;
        private System.Windows.Forms.ToolStripMenuItem itdBalanceOutsaandingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rejectedConsumerApplicationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem remittanceToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem21;
        private System.Windows.Forms.ToolStripMenuItem monitoringToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem22;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem23;
        private System.Windows.Forms.ToolStripMenuItem agentChargeCommissionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem24;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem25;
        private System.Windows.Forms.ToolStripMenuItem agentIncomeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem outletToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cashInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outletInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outletUserInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem billPaymentToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem billPaymentInformationToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem chequeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chequeRequsitionReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chequeDeliveryInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chequeBookInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agnetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agentInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agentCashInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem35;
        private System.Windows.Forms.ToolStripMenuItem exchangeHouseSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem billPaymentConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem36;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem26;
        private System.Windows.Forms.ToolStripMenuItem tESTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mTDRToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem developmentToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pendingApplicationSearchToolStripMenuItem1;
        public System.Windows.Forms.MenuStrip menuBranch;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem workstationManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem workstationAllocationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem depositAccountApplicationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dailyReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notificationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outletUserLimitSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productMappingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transactionMonitoringReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agentUserInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem branchUserInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userTransferHistoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem remittanceSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agentUserInformattionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem branchUserInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outletAccToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rermCloseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem useridWiseRptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nidVerificationToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem accountClosingRequestsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem daillyPositionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mobileNumberUpdateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applicationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nidVerificationReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applicationMonitoringToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem userIdWiseAccounrApproveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dailyReportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem dailyPositionReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem daaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem onlineReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userLimitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setupLimitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem billMonitoringReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dailyElectricityBillCollectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem billPaymentConfigurationToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem shomityConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zoneConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outletMappingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem servicesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem billPaymentsToolStripMenuItem1;
    }
}