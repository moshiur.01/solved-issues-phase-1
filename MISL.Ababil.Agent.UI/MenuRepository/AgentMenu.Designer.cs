﻿namespace MISL.Ababil.Agent.UI.MenuRepository
{
    partial class AgentMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuAgent = new System.Windows.Forms.MenuStrip();
            this.administrativeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.outletLimitSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outletUserLimitSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agentToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.accountBalanceAsOnDateToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.accountOpeningReportToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.agentCashRegisterToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.agentCommissionReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.agentIncomeStatementToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.cashInformationToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.remittanceReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transactionMonitoringReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dailyTransactionReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agentIncomeToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.agentChargeCommissionToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.billPaymentInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outletInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outletUserInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.developmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.termCloseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem37 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem38 = new System.Windows.Forms.ToolStripMenuItem();
            this.accountMonitoringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAgent.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuAgent
            // 
            this.menuAgent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.menuAgent.Font = new System.Drawing.Font("Tahoma", 9F);
            this.menuAgent.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.administrativeToolStripMenuItem1,
            this.agentToolStripMenuItem3,
            this.reportsToolStripMenuItem1,
            this.developmentToolStripMenuItem,
            this.toolStripMenuItem1});
            this.menuAgent.Location = new System.Drawing.Point(0, 0);
            this.menuAgent.Name = "menuAgent";
            this.menuAgent.Size = new System.Drawing.Size(911, 24);
            this.menuAgent.TabIndex = 13;
            this.menuAgent.Text = "menuStrip2";
            // 
            // administrativeToolStripMenuItem1
            // 
            this.administrativeToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.outletLimitSetupToolStripMenuItem,
            this.outletUserLimitSetupToolStripMenuItem});
            this.administrativeToolStripMenuItem1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.administrativeToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.administrativeToolStripMenuItem1.Name = "administrativeToolStripMenuItem1";
            this.administrativeToolStripMenuItem1.Size = new System.Drawing.Size(95, 20);
            this.administrativeToolStripMenuItem1.Tag = "AdminMenuTag";
            this.administrativeToolStripMenuItem1.Text = "Administrative";
            this.administrativeToolStripMenuItem1.Visible = false;
            // 
            // outletLimitSetupToolStripMenuItem
            // 
            this.outletLimitSetupToolStripMenuItem.Name = "outletLimitSetupToolStripMenuItem";
            this.outletLimitSetupToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.outletLimitSetupToolStripMenuItem.Text = "Outlet Limit Setup";
            this.outletLimitSetupToolStripMenuItem.Visible = false;
            this.outletLimitSetupToolStripMenuItem.Click += new System.EventHandler(this.outletLimitSetupToolStripMenuItem_Click);
            // 
            // outletUserLimitSetupToolStripMenuItem
            // 
            this.outletUserLimitSetupToolStripMenuItem.Name = "outletUserLimitSetupToolStripMenuItem";
            this.outletUserLimitSetupToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.outletUserLimitSetupToolStripMenuItem.Text = "Outlet User Limit Setup";
            this.outletUserLimitSetupToolStripMenuItem.Click += new System.EventHandler(this.outletUserLimitSetupToolStripMenuItem_Click);
            // 
            // agentToolStripMenuItem3
            // 
            this.agentToolStripMenuItem3.Font = new System.Drawing.Font("Tahoma", 9F);
            this.agentToolStripMenuItem3.ForeColor = System.Drawing.Color.White;
            this.agentToolStripMenuItem3.Name = "agentToolStripMenuItem3";
            this.agentToolStripMenuItem3.Size = new System.Drawing.Size(83, 20);
            this.agentToolStripMenuItem3.Text = "Applications";
            this.agentToolStripMenuItem3.Visible = false;
            // 
            // reportsToolStripMenuItem1
            // 
            this.reportsToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accountBalanceAsOnDateToolStripMenuItem1,
            this.accountOpeningReportToolStripMenuItem2,
            this.agentCashRegisterToolStripMenuItem1,
            this.agentCommissionReportToolStripMenuItem1,
            this.agentIncomeStatementToolStripMenuItem2,
            this.cashInformationToolStripMenuItem2,
            this.remittanceReportToolStripMenuItem,
            this.transactionMonitoringReportToolStripMenuItem,
            this.dailyTransactionReportToolStripMenuItem,
            this.agentIncomeToolStripMenuItem2,
            this.agentChargeCommissionToolStripMenuItem2,
            this.billPaymentInformationToolStripMenuItem,
            this.outletInformationToolStripMenuItem,
            this.outletUserInformationToolStripMenuItem,
            this.accountMonitoringToolStripMenuItem});
            this.reportsToolStripMenuItem1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.reportsToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.reportsToolStripMenuItem1.Name = "reportsToolStripMenuItem1";
            this.reportsToolStripMenuItem1.Size = new System.Drawing.Size(61, 20);
            this.reportsToolStripMenuItem1.Text = "Reports";
            // 
            // accountBalanceAsOnDateToolStripMenuItem1
            // 
            this.accountBalanceAsOnDateToolStripMenuItem1.Name = "accountBalanceAsOnDateToolStripMenuItem1";
            this.accountBalanceAsOnDateToolStripMenuItem1.Size = new System.Drawing.Size(239, 22);
            this.accountBalanceAsOnDateToolStripMenuItem1.Text = "Account Balance as on Date";
            this.accountBalanceAsOnDateToolStripMenuItem1.Click += new System.EventHandler(this.accountBalanceAsOnDateToolStripMenuItem1_Click);
            // 
            // accountOpeningReportToolStripMenuItem2
            // 
            this.accountOpeningReportToolStripMenuItem2.Name = "accountOpeningReportToolStripMenuItem2";
            this.accountOpeningReportToolStripMenuItem2.Size = new System.Drawing.Size(239, 22);
            this.accountOpeningReportToolStripMenuItem2.Text = "Account Opening Register";
            this.accountOpeningReportToolStripMenuItem2.Click += new System.EventHandler(this.accountOpeningReportToolStripMenuItem2_Click);
            // 
            // agentCashRegisterToolStripMenuItem1
            // 
            this.agentCashRegisterToolStripMenuItem1.Name = "agentCashRegisterToolStripMenuItem1";
            this.agentCashRegisterToolStripMenuItem1.Size = new System.Drawing.Size(239, 22);
            this.agentCashRegisterToolStripMenuItem1.Text = "Agent Cash Register";
            this.agentCashRegisterToolStripMenuItem1.Click += new System.EventHandler(this.agentCashRegisterToolStripMenuItem1_Click);
            // 
            // agentCommissionReportToolStripMenuItem1
            // 
            this.agentCommissionReportToolStripMenuItem1.Name = "agentCommissionReportToolStripMenuItem1";
            this.agentCommissionReportToolStripMenuItem1.Size = new System.Drawing.Size(239, 22);
            this.agentCommissionReportToolStripMenuItem1.Text = "Agent Commission Report";
            this.agentCommissionReportToolStripMenuItem1.Click += new System.EventHandler(this.agentCommissionReportToolStripMenuItem1_Click);
            // 
            // agentIncomeStatementToolStripMenuItem2
            // 
            this.agentIncomeStatementToolStripMenuItem2.Name = "agentIncomeStatementToolStripMenuItem2";
            this.agentIncomeStatementToolStripMenuItem2.Size = new System.Drawing.Size(239, 22);
            this.agentIncomeStatementToolStripMenuItem2.Text = "Agent Income Statement";
            this.agentIncomeStatementToolStripMenuItem2.Click += new System.EventHandler(this.agentIncomeStatementToolStripMenuItem2_Click);
            // 
            // cashInformationToolStripMenuItem2
            // 
            this.cashInformationToolStripMenuItem2.Name = "cashInformationToolStripMenuItem2";
            this.cashInformationToolStripMenuItem2.Size = new System.Drawing.Size(239, 22);
            this.cashInformationToolStripMenuItem2.Text = "Cash Information";
            this.cashInformationToolStripMenuItem2.Click += new System.EventHandler(this.cashInformationToolStripMenuItem2_Click);
            // 
            // remittanceReportToolStripMenuItem
            // 
            this.remittanceReportToolStripMenuItem.Name = "remittanceReportToolStripMenuItem";
            this.remittanceReportToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.remittanceReportToolStripMenuItem.Text = "Remittance Report";
            this.remittanceReportToolStripMenuItem.Click += new System.EventHandler(this.remittanceReportToolStripMenuItem_Click);
            // 
            // transactionMonitoringReportToolStripMenuItem
            // 
            this.transactionMonitoringReportToolStripMenuItem.Name = "transactionMonitoringReportToolStripMenuItem";
            this.transactionMonitoringReportToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.transactionMonitoringReportToolStripMenuItem.Text = "Transaction Monitoring Report";
            this.transactionMonitoringReportToolStripMenuItem.Click += new System.EventHandler(this.transactionMonitoringReportToolStripMenuItem_Click);
            // 
            // dailyTransactionReportToolStripMenuItem
            // 
            this.dailyTransactionReportToolStripMenuItem.Name = "dailyTransactionReportToolStripMenuItem";
            this.dailyTransactionReportToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.dailyTransactionReportToolStripMenuItem.Text = "Daily Transaction Report";
            this.dailyTransactionReportToolStripMenuItem.Click += new System.EventHandler(this.dailyTransactionReportToolStripMenuItem_Click);
            // 
            // agentIncomeToolStripMenuItem2
            // 
            this.agentIncomeToolStripMenuItem2.Name = "agentIncomeToolStripMenuItem2";
            this.agentIncomeToolStripMenuItem2.Size = new System.Drawing.Size(239, 22);
            this.agentIncomeToolStripMenuItem2.Text = "Agent Income";
            this.agentIncomeToolStripMenuItem2.Click += new System.EventHandler(this.agentIncomeToolStripMenuItem2_Click);
            // 
            // agentChargeCommissionToolStripMenuItem2
            // 
            this.agentChargeCommissionToolStripMenuItem2.Name = "agentChargeCommissionToolStripMenuItem2";
            this.agentChargeCommissionToolStripMenuItem2.Size = new System.Drawing.Size(239, 22);
            this.agentChargeCommissionToolStripMenuItem2.Text = "Agent Charge Commission";
            this.agentChargeCommissionToolStripMenuItem2.Click += new System.EventHandler(this.agentChargeCommissionToolStripMenuItem2_Click);
            // 
            // billPaymentInformationToolStripMenuItem
            // 
            this.billPaymentInformationToolStripMenuItem.Name = "billPaymentInformationToolStripMenuItem";
            this.billPaymentInformationToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.billPaymentInformationToolStripMenuItem.Text = "Bill Payment Information";
            this.billPaymentInformationToolStripMenuItem.Click += new System.EventHandler(this.billPaymentInformationToolStripMenuItem_Click);
            // 
            // outletInformationToolStripMenuItem
            // 
            this.outletInformationToolStripMenuItem.Name = "outletInformationToolStripMenuItem";
            this.outletInformationToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.outletInformationToolStripMenuItem.Text = "Outlet Information";
            this.outletInformationToolStripMenuItem.Click += new System.EventHandler(this.outletInformationToolStripMenuItem_Click);
            // 
            // outletUserInformationToolStripMenuItem
            // 
            this.outletUserInformationToolStripMenuItem.Name = "outletUserInformationToolStripMenuItem";
            this.outletUserInformationToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.outletUserInformationToolStripMenuItem.Text = "Outlet User Information";
            this.outletUserInformationToolStripMenuItem.Click += new System.EventHandler(this.outletUserInformationToolStripMenuItem_Click);
            // 
            // developmentToolStripMenuItem
            // 
            this.developmentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.termCloseToolStripMenuItem});
            this.developmentToolStripMenuItem.Font = new System.Drawing.Font("Tahoma", 9F);
            this.developmentToolStripMenuItem.ForeColor = System.Drawing.Color.Navy;
            this.developmentToolStripMenuItem.Name = "developmentToolStripMenuItem";
            this.developmentToolStripMenuItem.Size = new System.Drawing.Size(92, 20);
            this.developmentToolStripMenuItem.Text = "Development";
            this.developmentToolStripMenuItem.Visible = false;
            // 
            // termCloseToolStripMenuItem
            // 
            this.termCloseToolStripMenuItem.Name = "termCloseToolStripMenuItem";
            this.termCloseToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.termCloseToolStripMenuItem.Text = "term close";
            this.termCloseToolStripMenuItem.Click += new System.EventHandler(this.termCloseToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem37,
            this.toolStripMenuItem38});
            this.toolStripMenuItem1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.toolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(64, 20);
            this.toolStripMenuItem1.Text = "Settings";
            // 
            // toolStripMenuItem37
            // 
            this.toolStripMenuItem37.Name = "toolStripMenuItem37";
            this.toolStripMenuItem37.Size = new System.Drawing.Size(170, 22);
            this.toolStripMenuItem37.Text = "Change Password";
            this.toolStripMenuItem37.Click += new System.EventHandler(this.toolStripMenuItem37_Click);
            // 
            // toolStripMenuItem38
            // 
            this.toolStripMenuItem38.Name = "toolStripMenuItem38";
            this.toolStripMenuItem38.Size = new System.Drawing.Size(170, 22);
            this.toolStripMenuItem38.Text = "Reset Cache";
            this.toolStripMenuItem38.Click += new System.EventHandler(this.toolStripMenuItem38_Click);
            // 
            // accountMonitoringToolStripMenuItem
            // 
            this.accountMonitoringToolStripMenuItem.Name = "accountMonitoringToolStripMenuItem";
            this.accountMonitoringToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.accountMonitoringToolStripMenuItem.Text = "Account Monitoring";
            this.accountMonitoringToolStripMenuItem.Click += new System.EventHandler(this.accountMonitoringToolStripMenuItem_Click);
            // 
            // AgentMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(911, 24);
            this.ControlBox = false;
            this.Controls.Add(this.menuAgent);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AgentMenu";
            this.Text = "AgentMenu";
            this.menuAgent.ResumeLayout(false);
            this.menuAgent.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStripMenuItem administrativeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem outletLimitSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agentToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem accountBalanceAsOnDateToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem accountOpeningReportToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem agentCashRegisterToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem agentCommissionReportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem agentIncomeStatementToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem cashInformationToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem remittanceReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transactionMonitoringReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dailyTransactionReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agentIncomeToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem agentChargeCommissionToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem developmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem37;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem38;
        public System.Windows.Forms.MenuStrip menuAgent;
        private System.Windows.Forms.ToolStripMenuItem outletUserLimitSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem billPaymentInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outletInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outletUserInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem termCloseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountMonitoringToolStripMenuItem;
    }
}