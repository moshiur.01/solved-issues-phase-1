﻿namespace MISL.Ababil.Agent.UI.MenuRepository
{
    partial class FieldOfficerMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuFieldOfficer = new System.Windows.Forms.MenuStrip();
            this.applicationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pendingCustomerApplicationsToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.chequeRequisitionSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem39 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem40 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem41 = new System.Windows.Forms.ToolStripMenuItem();
            this.serviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.balanceInquiryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nidVerificationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountOpeningRegisterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.developmentToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.pendingApplicationSearchToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.chequeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chequeRequisitionReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chequeDeliveryInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chequeBookInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFieldOfficer.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuFieldOfficer
            // 
            this.menuFieldOfficer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.menuFieldOfficer.Font = new System.Drawing.Font("Tahoma", 9F);
            this.menuFieldOfficer.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.applicationsToolStripMenuItem,
            this.toolStripMenuItem39,
            this.serviceToolStripMenuItem,
            this.reportToolStripMenuItem,
            this.developmentToolStripMenuItem2});
            this.menuFieldOfficer.Location = new System.Drawing.Point(0, 0);
            this.menuFieldOfficer.Name = "menuFieldOfficer";
            this.menuFieldOfficer.Size = new System.Drawing.Size(904, 24);
            this.menuFieldOfficer.TabIndex = 16;
            this.menuFieldOfficer.Text = "menuStrip2";
            // 
            // applicationsToolStripMenuItem
            // 
            this.applicationsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pendingCustomerApplicationsToolStripMenuItem2,
            this.chequeRequisitionSearchToolStripMenuItem});
            this.applicationsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.applicationsToolStripMenuItem.Name = "applicationsToolStripMenuItem";
            this.applicationsToolStripMenuItem.Size = new System.Drawing.Size(83, 20);
            this.applicationsToolStripMenuItem.Text = "Applications";
            // 
            // pendingCustomerApplicationsToolStripMenuItem2
            // 
            this.pendingCustomerApplicationsToolStripMenuItem2.Name = "pendingCustomerApplicationsToolStripMenuItem2";
            this.pendingCustomerApplicationsToolStripMenuItem2.Size = new System.Drawing.Size(242, 22);
            this.pendingCustomerApplicationsToolStripMenuItem2.Text = "Pending Customer Applications";
            this.pendingCustomerApplicationsToolStripMenuItem2.Click += new System.EventHandler(this.pendingCustomerApplicationsToolStripMenuItem2_Click);
            // 
            // chequeRequisitionSearchToolStripMenuItem
            // 
            this.chequeRequisitionSearchToolStripMenuItem.Name = "chequeRequisitionSearchToolStripMenuItem";
            this.chequeRequisitionSearchToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.chequeRequisitionSearchToolStripMenuItem.Text = "Cheque Requisition Search";
            this.chequeRequisitionSearchToolStripMenuItem.Click += new System.EventHandler(this.chequeRequisitionSearchToolStripMenuItem_Click);
            // 
            // toolStripMenuItem39
            // 
            this.toolStripMenuItem39.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem40,
            this.toolStripMenuItem41});
            this.toolStripMenuItem39.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem39.Name = "toolStripMenuItem39";
            this.toolStripMenuItem39.Size = new System.Drawing.Size(64, 20);
            this.toolStripMenuItem39.Text = "Settings";
            // 
            // toolStripMenuItem40
            // 
            this.toolStripMenuItem40.Name = "toolStripMenuItem40";
            this.toolStripMenuItem40.Size = new System.Drawing.Size(170, 22);
            this.toolStripMenuItem40.Text = "Change Password";
            this.toolStripMenuItem40.Click += new System.EventHandler(this.toolStripMenuItem40_Click);
            // 
            // toolStripMenuItem41
            // 
            this.toolStripMenuItem41.Name = "toolStripMenuItem41";
            this.toolStripMenuItem41.Size = new System.Drawing.Size(170, 22);
            this.toolStripMenuItem41.Text = "Reset Cache";
            this.toolStripMenuItem41.Click += new System.EventHandler(this.toolStripMenuItem41_Click);
            // 
            // serviceToolStripMenuItem
            // 
            this.serviceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.balanceInquiryToolStripMenuItem});
            this.serviceToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.serviceToolStripMenuItem.Name = "serviceToolStripMenuItem";
            this.serviceToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.serviceToolStripMenuItem.Text = "Service";
            this.serviceToolStripMenuItem.Visible = false;
            // 
            // balanceInquiryToolStripMenuItem
            // 
            this.balanceInquiryToolStripMenuItem.Name = "balanceInquiryToolStripMenuItem";
            this.balanceInquiryToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.balanceInquiryToolStripMenuItem.Text = "Balance Inquiry";
            this.balanceInquiryToolStripMenuItem.Click += new System.EventHandler(this.balanceInquiryToolStripMenuItem_Click);
            // 
            // reportToolStripMenuItem
            // 
            this.reportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nidVerificationToolStripMenuItem,
            this.accountOpeningRegisterToolStripMenuItem,
            this.chequeToolStripMenuItem});
            this.reportToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.reportToolStripMenuItem.Name = "reportToolStripMenuItem";
            this.reportToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.reportToolStripMenuItem.Text = "Report";
            // 
            // nidVerificationToolStripMenuItem
            // 
            this.nidVerificationToolStripMenuItem.Name = "nidVerificationToolStripMenuItem";
            this.nidVerificationToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.nidVerificationToolStripMenuItem.Text = "Nid Verification ";
            this.nidVerificationToolStripMenuItem.Click += new System.EventHandler(this.nidVerificationToolStripMenuItem_Click);
            // 
            // accountOpeningRegisterToolStripMenuItem
            // 
            this.accountOpeningRegisterToolStripMenuItem.Name = "accountOpeningRegisterToolStripMenuItem";
            this.accountOpeningRegisterToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.accountOpeningRegisterToolStripMenuItem.Text = "Account Opening Register";
            this.accountOpeningRegisterToolStripMenuItem.Click += new System.EventHandler(this.accountOpeningRegisterToolStripMenuItem_Click);
            // 
            // developmentToolStripMenuItem2
            // 
            this.developmentToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pendingApplicationSearchToolStripMenuItem2});
            this.developmentToolStripMenuItem2.ForeColor = System.Drawing.Color.White;
            this.developmentToolStripMenuItem2.Name = "developmentToolStripMenuItem2";
            this.developmentToolStripMenuItem2.Size = new System.Drawing.Size(92, 20);
            this.developmentToolStripMenuItem2.Text = "Development";
            this.developmentToolStripMenuItem2.Visible = false;
            // 
            // pendingApplicationSearchToolStripMenuItem2
            // 
            this.pendingApplicationSearchToolStripMenuItem2.Name = "pendingApplicationSearchToolStripMenuItem2";
            this.pendingApplicationSearchToolStripMenuItem2.Size = new System.Drawing.Size(222, 22);
            this.pendingApplicationSearchToolStripMenuItem2.Text = "Pending Application Search";
            this.pendingApplicationSearchToolStripMenuItem2.Click += new System.EventHandler(this.pendingApplicationSearchToolStripMenuItem2_Click);
            // 
            // chequeToolStripMenuItem
            // 
            this.chequeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.chequeRequisitionReportToolStripMenuItem,
            this.chequeDeliveryInformationToolStripMenuItem,
            this.chequeBookInformationToolStripMenuItem});
            this.chequeToolStripMenuItem.Name = "chequeToolStripMenuItem";
            this.chequeToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.chequeToolStripMenuItem.Text = "Cheque";
            // 
            // chequeRequisitionReportToolStripMenuItem
            // 
            this.chequeRequisitionReportToolStripMenuItem.Name = "chequeRequisitionReportToolStripMenuItem";
            this.chequeRequisitionReportToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.chequeRequisitionReportToolStripMenuItem.Text = "Cheque Requisition Report";
            this.chequeRequisitionReportToolStripMenuItem.Click += new System.EventHandler(this.chequeRequisitionReportToolStripMenuItem_Click);
            // 
            // chequeDeliveryInformationToolStripMenuItem
            // 
            this.chequeDeliveryInformationToolStripMenuItem.Name = "chequeDeliveryInformationToolStripMenuItem";
            this.chequeDeliveryInformationToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.chequeDeliveryInformationToolStripMenuItem.Text = "Cheque Delivery Information";
            this.chequeDeliveryInformationToolStripMenuItem.Click += new System.EventHandler(this.chequeDeliveryInformationToolStripMenuItem_Click);
            // 
            // chequeBookInformationToolStripMenuItem
            // 
            this.chequeBookInformationToolStripMenuItem.Name = "chequeBookInformationToolStripMenuItem";
            this.chequeBookInformationToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.chequeBookInformationToolStripMenuItem.Text = "Cheque Book Information";
            this.chequeBookInformationToolStripMenuItem.Click += new System.EventHandler(this.chequeBookInformationToolStripMenuItem_Click);
            // 
            // FieldOfficerMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(904, 24);
            this.Controls.Add(this.menuFieldOfficer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FieldOfficerMenu";
            this.Text = "FieldOfficerMenu";
            this.menuFieldOfficer.ResumeLayout(false);
            this.menuFieldOfficer.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStripMenuItem applicationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pendingCustomerApplicationsToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem39;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem40;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem41;
        private System.Windows.Forms.ToolStripMenuItem developmentToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem pendingApplicationSearchToolStripMenuItem2;
        public System.Windows.Forms.MenuStrip menuFieldOfficer;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serviceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem balanceInquiryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nidVerificationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountOpeningRegisterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chequeRequisitionSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chequeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chequeRequisitionReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chequeDeliveryInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chequeBookInformationToolStripMenuItem;
    }
}