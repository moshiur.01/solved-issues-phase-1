﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.LocalStorageService;
using MISL.Ababil.Agent.Report;
using MISL.Ababil.Agent.UI.forms;
using MISL.Ababil.Agent.UI.forms.AccountClosingUI;
using MISL.Ababil.Agent.UI.forms.ProgressUI;

namespace MISL.Ababil.Agent.UI.MenuRepository
{
    public partial class AgentMenu : Form
    {
        public frmMainApp MainForm { get; set; }

        public AgentMenu()
        {
            InitializeComponent();
        }

        private void outletLimitSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmOutletList().ShowDialog(MainForm);
        }

        private void accountBalanceAsOnDateToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new frmAccountWiseBalance().Show(MainForm);
        }

        private void accountOpeningReportToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            new frmAccountOpeningRport().Show(MainForm);
        }

        private void agentCashRegisterToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new frmCashRegister().Show(this);
        }

        private void agentCommissionReportToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new frmAgentCommissionRport().Show(MainForm);
        }

        private void agentIncomeStatementToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            new frmAgentIncomeStatement().Show(MainForm);
        }

        private void cashInformationToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            new frmAgentDayEndSummary().Show(MainForm);
        }

        private void remittanceReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmRemittanceRecord().Show(MainForm);
        }

        private void transactionMonitoringReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmTrMonitoringRport().Show(MainForm);
        }

        private void dailyTransactionReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmTransactionReport().Show(MainForm);
        }

        private void agentIncomeToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            new frmAgentIncome().Show(MainForm);
        }

        private void agentChargeCommissionToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            //~//new frmChargeCommission().Show(MainForm);
        }

        private void toolStripMenuItem37_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmPasswordChange(false) { btnFingerPrint = { Visible = false } }.ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void toolStripMenuItem38_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            if (MsgBox.showConfirmation("Do you want to reset local cache?\n\nPlease save all your work before continuing. You will be logged-out in this process.") == "yes")
            {
                //~//MainForm._frmClosing = true;
                ProgressUIManager.ShowProgress(MainForm);
                new LocalStorage().ResetLocalCache();
                ProgressUIManager.CloseProgress();
                Application.ExitThread();
                Application.Restart();
            }
            MainForm.SetPageLayerVisibility(true);
        }

        private void outletUserLimitSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SessionInfo.userBasicInformation.agent.id == 1)
            {
                Packet packet = new Packet();
                frmOutletUserCashLimitInfo frm = new frmOutletUserCashLimitInfo(packet);
                frm.ShowDialog(this);
            }
            else MsgBox.showInformation("You have no right to perform this operation.");
        }

        private void billPaymentInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmBillPaymentReport frm = new frmBillPaymentReport();
            frm.ShowDialog(this);
        }

        private void outletInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmOutletInfoReport frm = new frmOutletInfoReport();
            frm.ShowDialog(this);
        }

        private void outletUserInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmOutletUserInfoReport frm = new frmOutletUserInfoReport();
            frm.ShowDialog(this);
        }

        private void termCloseToolStripMenuItem_Click(object sender, EventArgs e)
        {
           // new frmTermAccountClosing().ShowDialog(this);
        }

        private void accountMonitoringToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmAccountMonitoringRport().Show(MainForm);
        }
    }
}