﻿namespace MISL.Ababil.Agent.UI.MenuRepository
{
    partial class RemittanceMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuRemittance = new System.Windows.Forms.MenuStrip();
            this.remittanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.remittanceRecordSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem17 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem42 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem43 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuRemittance.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuRemittance
            // 
            this.menuRemittance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.menuRemittance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.menuRemittance.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.remittanceToolStripMenuItem,
            this.remittanceRecordSearchToolStripMenuItem,
            this.toolStripMenuItem17});
            this.menuRemittance.Location = new System.Drawing.Point(0, 0);
            this.menuRemittance.Name = "menuRemittance";
            this.menuRemittance.Size = new System.Drawing.Size(942, 24);
            this.menuRemittance.TabIndex = 18;
            this.menuRemittance.Text = "menuStrip2";
            // 
            // remittanceToolStripMenuItem
            // 
            this.remittanceToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.remittanceToolStripMenuItem.Name = "remittanceToolStripMenuItem";
            this.remittanceToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.remittanceToolStripMenuItem.Text = "Remittance";
            this.remittanceToolStripMenuItem.Click += new System.EventHandler(this.remittanceToolStripMenuItem_Click);
            // 
            // remittanceRecordSearchToolStripMenuItem
            // 
            this.remittanceRecordSearchToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.remittanceRecordSearchToolStripMenuItem.Name = "remittanceRecordSearchToolStripMenuItem";
            this.remittanceRecordSearchToolStripMenuItem.Size = new System.Drawing.Size(122, 20);
            this.remittanceRecordSearchToolStripMenuItem.Text = "Remittance Report";
            this.remittanceRecordSearchToolStripMenuItem.Click += new System.EventHandler(this.remittanceRecordSearchToolStripMenuItem_Click);
            // 
            // toolStripMenuItem17
            // 
            this.toolStripMenuItem17.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem42,
            this.toolStripMenuItem43});
            this.toolStripMenuItem17.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem17.Name = "toolStripMenuItem17";
            this.toolStripMenuItem17.Size = new System.Drawing.Size(64, 20);
            this.toolStripMenuItem17.Text = "Settings";
            // 
            // toolStripMenuItem42
            // 
            this.toolStripMenuItem42.Name = "toolStripMenuItem42";
            this.toolStripMenuItem42.Size = new System.Drawing.Size(170, 22);
            this.toolStripMenuItem42.Text = "Change Password";
            this.toolStripMenuItem42.Click += new System.EventHandler(this.toolStripMenuItem42_Click);
            // 
            // toolStripMenuItem43
            // 
            this.toolStripMenuItem43.Name = "toolStripMenuItem43";
            this.toolStripMenuItem43.Size = new System.Drawing.Size(170, 22);
            this.toolStripMenuItem43.Text = "Reset Cache";
            this.toolStripMenuItem43.Click += new System.EventHandler(this.toolStripMenuItem43_Click);
            // 
            // RemittanceMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(942, 24);
            this.ControlBox = false;
            this.Controls.Add(this.menuRemittance);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "RemittanceMenu";
            this.Text = "RemittanceMenu";
            this.menuRemittance.ResumeLayout(false);
            this.menuRemittance.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStripMenuItem remittanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem remittanceRecordSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem17;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem42;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem43;
        public System.Windows.Forms.MenuStrip menuRemittance;
    }
}