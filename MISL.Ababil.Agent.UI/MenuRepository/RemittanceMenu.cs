﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.LocalStorageService;
using MISL.Ababil.Agent.Report;
using MISL.Ababil.Agent.UI.forms;
using MISL.Ababil.Agent.UI.forms.ProgressUI;

namespace MISL.Ababil.Agent.UI.MenuRepository
{
    public partial class RemittanceMenu : Form
    {
        public frmMainApp MainForm { get; set; }

        public RemittanceMenu()
        {
            InitializeComponent();
        }

        private void remittanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmRemittanceAdmin().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void remittanceRecordSearchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmRemittanceRecord().Show(MainForm);
        }

        private void toolStripMenuItem42_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmPasswordChange(false) { btnFingerPrint = { Visible = false } }.ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void toolStripMenuItem43_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            if (MsgBox.showConfirmation("Do you want to reset local cache?\n\nPlease save all your work before continuing. You will be logged-out in this process.") == "yes")
            {
                //~//MainForm._frmClosing = true;
                ProgressUIManager.ShowProgress(MainForm);
                new LocalStorage().ResetLocalCache();
                ProgressUIManager.CloseProgress();
                Application.ExitThread();
                Application.Restart();
            }
            MainForm.SetPageLayerVisibility(true);
        }
    }
}