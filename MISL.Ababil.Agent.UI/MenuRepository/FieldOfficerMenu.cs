﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.LocalStorageService;
using MISL.Ababil.Agent.Report;
using MISL.Ababil.Agent.UI.forms;
using MISL.Ababil.Agent.UI.forms.ProgressUI;
using MISL.Ababil.Agent.Module.ChequeRequisition.UI;
using MISL.Ababil.Agent.Module.ChequeRequisition.Report;

namespace MISL.Ababil.Agent.UI.MenuRepository
{
    public partial class FieldOfficerMenu : Form
    {
        public frmMainApp MainForm { get; set; }

        public FieldOfficerMenu()
        {
            InitializeComponent();
        }

        private void pendingCustomerApplicationsToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmPendingApplication().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void toolStripMenuItem40_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmPasswordChange(false) { btnFingerPrint = { Visible = false } }.ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void toolStripMenuItem41_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            if (MsgBox.showConfirmation("Do you want to reset local cache?\n\nPlease save all your work before continuing. You will be logged-out in this process.") == "yes")
            {
                //~//MainForm._frmClosing = true;
                ProgressUIManager.ShowProgress(MainForm);
                new LocalStorage().ResetLocalCache();
                ProgressUIManager.CloseProgress();
                Application.ExitThread();
                Application.Restart();
            }
            MainForm.SetPageLayerVisibility(true);
        }

        private void pendingApplicationSearchToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            //~//new forms.Deposit.frmPendingApplication2().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void agentBalanceInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmAgentCashInformation().Show(MainForm);
        }

        private void balanceInquiryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm.SetPageLayerVisibility(false);
            new frmBalanceInquiry().ShowDialog(MainForm);
            MainForm.SetPageLayerVisibility(true);
        }

        private void nidVerificationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            new frmDepositApplicantInfo().ShowDialog(MainForm);
            Cursor.Current = Cursors.Default;
        }

        private void accountOpeningRegisterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmAccountOpeningRport().Show(MainForm);
        }

        private void chequeRequisitionSearchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmChequeRequisitionSearch().ShowDialog(MainForm);
        }

        private void chequeRequisitionReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmChequeRequisitionInformation().ShowDialog(MainForm);
        }

        private void chequeDeliveryInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmChequeDeliveryReport().ShowDialog(MainForm);
        }

        private void chequeBookInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmChequeBookInformation().ShowDialog(MainForm);
        }
    }
}