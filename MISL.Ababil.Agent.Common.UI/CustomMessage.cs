﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.Common.UI.Properties;
using MISL.Ababil.Agent.Infrastructure.Interfaces;
using MISL.Ababil.Agent.Infrastructure.Models.common;

namespace MISL.Ababil.Agent.Common.UI
{
    public static class CustomMessage
    {

        public static string showConfirmation(string msg)
        {
            MessageUI frm = new MessageUI();
            frm.picIcon.Image = Resources.help;
            frm.Style = MetroFramework.MetroColorStyle.Blue;
            frm.lblTitle.Text = "Confirmation";
            frm.lblMsg.Text = msg;
            frm.btnYes.DialogResult = DialogResult.Yes;
            frm.btnYes.Focus();
            frm.btnNo.DialogResult = DialogResult.No;
            DialogResult result = frm.ShowDialog();

            //DialogResult result = MessageBox.Show(msg,
            //   "Confirmation",
            //   MessageBoxButtons.YesNo,
            //   MessageBoxIcon.Question,
            //   MessageBoxDefaultButton.Button2);

            if (result == DialogResult.Yes) return "yes";
            else return "no";
        }

        public static void showError(string msg)
        {
            string authFailedMsg0 = "Authentication failed.";
            string authFailedMsg1 = "Authentication failed";

            MessageUI frm = new MessageUI();
            frm.picIcon.Image = Resources.err;
            frm.Style = MetroFramework.MetroColorStyle.Red;
            frm.lblTitle.Text = "Error";
            frm.lblMsg.Text = msg;
            frm.btnYes.Visible = false;
            frm.btnNo.DialogResult = DialogResult.OK;
            frm.btnNo.Text = "OK";
            frm.btnNo.Focus();
            DialogResult result = frm.ShowDialog();

            if ((msg.ToLower() == authFailedMsg0.ToLower()) || (msg.ToLower() == authFailedMsg1.ToLower()))
            {
                ((ILogoutable)SessionUIStorage.MainForm).LogoutExt();
            }
            //MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void showInformation(string msg)
        {
            MessageUI frm = new MessageUI();
            frm.picIcon.Image = Resources.info;
            frm.Style = MetroFramework.MetroColorStyle.Blue;
            frm.lblTitle.Text = "Information";
            frm.lblMsg.Text = msg;
            frm.btnYes.Visible = false;
            frm.btnNo.DialogResult = DialogResult.OK;
            frm.btnNo.Text = "OK";
            frm.btnNo.Focus();
            DialogResult result = frm.ShowDialog();

            //MessageBox.Show(msg, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void showWarning(string msg)
        {
            MessageUI frm = new MessageUI();
            frm.picIcon.Image = Resources.warn;
            frm.Style = MetroFramework.MetroColorStyle.Yellow;
            frm.lblTitle.Text = "Warning";
            frm.lblMsg.Text = msg;
            frm.btnYes.Visible = false;
            frm.btnNo.DialogResult = DialogResult.OK;
            frm.btnNo.Text = "OK";
            frm.btnNo.Focus();
            DialogResult result = frm.ShowDialog();

            //MessageBox.Show(msg, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

    }
}
