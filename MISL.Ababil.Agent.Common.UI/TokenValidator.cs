﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MISL.Ababil.Agent.Common.UI
{
    public partial class TokenValidator : MetroForm
    {
        private string _token;
        public bool isValidToken = false;

        public TokenValidator(string token)
        {
            InitializeComponent();
            txtTokenInput.Focus();

            _token = token;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtTokenInput.Text = "";
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (_token == txtTokenInput.Text.Trim())
            {
                lblErrLabel.Text = "";
                lblErrLabel.Visible = false;

                this.DialogResult = DialogResult.OK;
                isValidToken = true;
                this.Close();
            }
            else
            {
                isValidToken = false;
                lblErrLabel.Visible = true;
            }
        }

    }
}
