﻿using System;
using System.Collections.Generic;
using System.Net;
using MISL.Ababil.Agent.Communication;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Module.ChequeRequisition.Models.Report;
using Newtonsoft.Json;

namespace MISL.Ababil.Agent.Module.ChequeRequisition.Communication
{
   public class ReportChequeCom
    {

       public List<ChequeBookInfoResultReportDto> GetChequeBookInfoReportList(ChequeRequisitionReportSearchDto chequeBookInfoSearchDto)
       {
           List<ChequeBookInfoResultReportDto> Data = new List<ChequeBookInfoResultReportDto>();
           var jsonString = JsonConvert.SerializeObject(chequeBookInfoSearchDto);
           WebClient client = new WebClient();
           try
           {
               string path = SessionInfo.rootServiceUrl + "resources/chequerequisition/chequebook";
               client = UtilityCom.setClientHeaders(client);
               string responseString = client.UploadString(path, "POST", jsonString);
               string serviceResponse = UtilityCom.getServerResponse(client);
               if (!serviceResponse.Equals("OK")) return null;
               else
               {
                   Data = JsonConvert.DeserializeObject<List<ChequeBookInfoResultReportDto>>(responseString);
                   return Data;
               }
           }
           catch (WebException webEx)
           { throw new Exception(UtilityCom.parseErrorData(webEx)); }
       }
    }
}
