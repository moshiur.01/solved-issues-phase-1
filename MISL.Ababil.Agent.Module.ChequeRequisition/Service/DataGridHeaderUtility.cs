﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using MISL.Ababil.Agent.Module.Common.Exporter;
using MISL.Ababil.Agent.Module.Common.Exportor;
using MISL.Ababil.Agent.Module.Common.UI.MessageUI;
using MISL.Ababil.Agent.Services;

namespace MISL.Ababil.Agent.UI
{
    public static class DataGridHeaderUtility
    {
        public static void ReportHederDataExportToPdf(CustomDataGridViewHeader viewHeader)
        {
            try
            {
                ReportHeaders rptHeaders = new ReportHeaders();
                rptHeaders = UtilityServices.getReportHeaders("");
                if (rptHeaders != null)
                {
                    if (rptHeaders.branchDto != null)
                    {
                        viewHeader.ExportReportTitle = rptHeaders.branchDto.bankName;
                    }
                }
                if (SessionInfo.userBasicInformation.userCategory != null)
                {
                    if (SessionInfo.userBasicInformation.userCategory == UserCategory.BranchUser)
                    {
                        viewHeader.ExportReportSubtitleOne = rptHeaders.branchDto.branchName;
                        viewHeader.ExportReportSubtitleTwo = rptHeaders.branchDto.branchAddress;
                    }
                    if (SessionInfo.userBasicInformation.userCategory == UserCategory.SubAgentUser)
                    {
                        viewHeader.ExportReportSubtitleOne = "Outlet Name :" + SessionInfo.userBasicInformation.outlet.name;
                        viewHeader.ExportReportSubtitleTwo =
                            SessionInfo.userBasicInformation.outlet.businessAddress.addressLineOne + ", " +
                            //SessionInfo.userBasicInformation.outlet.businessAddress.addressLineTwo + ", " +
                            SessionInfo.userBasicInformation.outlet.businessAddress.district.title + ", " +
                            SessionInfo.userBasicInformation.outlet.businessAddress.thana.title;
                    }
                }
                //viewHeader.ExportReportSubtitleThree = reportTitle;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }
    }
}
