﻿using System;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Module.ChequeRequisition.Communication;
using MISL.Ababil.Agent.Module.ChequeRequisition.Models.Report;

namespace MISL.Ababil.Agent.Module.ChequeRequisition.Service
{
   public class ReportChequeService
    {
       public ServiceResult GetChequeBookInfoReportList(ChequeRequisitionReportSearchDto chequeBookInfoSearchDto)
       {
           ReportChequeCom objRepCom = new ReportChequeCom();
           var serviceResult = ServiceResult.CreateServiceResult();
           serviceResult.ReturnedObject = "";

           try
           {
               serviceResult.ReturnedObject = objRepCom.GetChequeBookInfoReportList(chequeBookInfoSearchDto);
               if (string.IsNullOrEmpty(serviceResult.ReturnedObject.ToString()))
               {
                   serviceResult.ReturnedObject = "";
                   serviceResult.Message = "ITD Outsanding report could not be generated successfully, please check connectivity and inform Bank Administration";
               }
               else
               {
                   serviceResult.Success = true;
                   serviceResult.Message = serviceResult.ReturnedObject.ToString();
               }
           }
           catch (Exception exception)
           {
               serviceResult.Message = exception.Message;
           }

           return serviceResult;
       }
    }
}
