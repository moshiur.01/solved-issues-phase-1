﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using MISL.Ababil.Agent.Module.ChequeRequisition.Models.Report;
using MISL.Ababil.Agent.Module.Common.UI.MessageUI;
using MISL.Ababil.Agent.Report.Reports;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Module.ChequeRequisition.DataSets;
using MISL.Ababil.Agent.Module.ChequeRequisition.Service;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;

namespace MISL.Ababil.Agent.Module.ChequeRequisition.Report
{
    public partial class frmChequeBookInformation : CustomForm
    {
        List<AgentInformation> objAgentInfoList = new List<AgentInformation>();
        AgentServices objAgentServices = new AgentServices();
        List<SubAgentInformation> subAgentList = new List<SubAgentInformation>();
        private ChequeRepuisitionDS chequeBookInfoDS = new ChequeRepuisitionDS();
        public frmChequeBookInformation()
        {
            InitializeComponent();
            controlActivity();
            try
            {
                SetAgentList();

            }
            catch (Exception)
            { }
        }
        private void setUIForFieldOfficer()
        {
            cmbAgentName.SelectedIndex = -1;
            cmbAgentName.Enabled = false;
        }
        private void btnViewReport_Click(object sender, EventArgs e)
        {
            try
            {
                if (ChequeValidetion())
                {
                    crChequeBookInfoReport report = new crChequeBookInfoReport();
                    frmReportViewer viewer = new frmReportViewer();

                    ReportHeaders rptHeaders = new ReportHeaders();
                    rptHeaders = UtilityServices.getReportHeaders("Cheque Book Information");

                    TextObject txtBankName = report.ReportDefinition.ReportObjects["txtBankName"] as TextObject;
                    TextObject txtBranchName = report.ReportDefinition.ReportObjects["txtBranchName"] as TextObject;
                    TextObject txtBranchAddress = report.ReportDefinition.ReportObjects["txtBranchAddress"] as TextObject;
                    TextObject txtReportHeading = report.ReportDefinition.ReportObjects["txtReportHeading"] as TextObject;
                    TextObject txtPrintUser = report.ReportDefinition.ReportObjects["txtPrintUser"] as TextObject;
                    TextObject txtPrintDate = report.ReportDefinition.ReportObjects["txtPrintDate"] as TextObject;
                    TextObject txtFromDate = report.ReportDefinition.ReportObjects["txtFromDate"] as TextObject;
                    TextObject txtToDate = report.ReportDefinition.ReportObjects["txtToDate"] as TextObject;

                    if (rptHeaders != null)
                    {
                        if (rptHeaders.branchDto != null)
                        {
                            txtBankName.Text = rptHeaders.branchDto.bankName;
                            txtBranchName.Text = rptHeaders.branchDto.branchName;
                            txtBranchAddress.Text = rptHeaders.branchDto.branchAddress;
                        }
                        txtReportHeading.Text = rptHeaders.reportHeading;
                        txtPrintUser.Text = rptHeaders.printUser;
                        txtPrintDate.Text = rptHeaders.printDate.ToString("dd-MM-yyyy").Replace("-", "/");
                    }

                    txtFromDate.Text = dtpFromDate.Date;
                    txtToDate.Text = dtpToDate.Date;

                    LoadChequeBookInformation();

                    report.SetDataSource(chequeBookInfoDS);

                    viewer.crvReportViewer.ReportSource = report;
                    viewer.Show(this.Parent);
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError("Report loding problem\n" + ex.Message);
            }

        }
        private void LoadChequeBookInformation()
        {
            chequeBookInfoDS.Clear();
            // ChequeBookInfoSearchDto chequeBookInfoSearchDto = FillChequeBookInfoSearchDto();
            ChequeRequisitionReportSearchDto chequeBookInfoSearchDto = FillChequeBookInfoSearchDto();
            ServiceResult result;
            chequeBookInfoDS = new ChequeRepuisitionDS();

            try
            {
                ReportChequeService reportChequeService = new ReportChequeService();
                result = reportChequeService.GetChequeBookInfoReportList(chequeBookInfoSearchDto);

                if (!result.Success)
                {
                    MsgBox.ShowError(result.Message);
                    return;
                }

                List<ChequeBookInfoResultReportDto> chequeBookInfo = result.ReturnedObject as List<ChequeBookInfoResultReportDto>;

                if (chequeBookInfo != null)
                {
                    foreach (ChequeBookInfoResultReportDto cheBookInfo in chequeBookInfo)
                    {

                        ChequeRepuisitionDS.ChequeBookInfoDTRow newRow =
                            chequeBookInfoDS.ChequeBookInfoDT.NewChequeBookInfoDTRow();

                        newRow.agentId = cheBookInfo.agentId;
                        newRow.agentName = cheBookInfo.agentName;
                        newRow.outletId = cheBookInfo.outletId;
                        newRow.outletName = cheBookInfo.outletName;
                        //newRow.bank = cheBookInfo.bank;
                        newRow.bank = "SIBL";
                        newRow.accountNo = cheBookInfo.accountNo;
                        newRow.accountTitle = cheBookInfo.accountTitle;
                        newRow.noOfLeaves = cheBookInfo.noOfLeaves ?? 0;
                        newRow.routingNo = cheBookInfo.routingNo ?? 0;
                        newRow.trNumber = cheBookInfo.trNumber ?? 0;
                        chequeBookInfoDS.ChequeBookInfoDT.AddChequeBookInfoDTRow(newRow);
                    }
                }
                chequeBookInfoDS.AcceptChanges();
            }
            catch (Exception)
            {
                //ignored
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private bool ChequeValidetion()
        {
            DateTime fromDate, toDate;
            try
            {
                fromDate = DateTime.ParseExact(dtpFromDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
            }
            catch
            {
                MsgBox.showWarning("Please enter the Date in correct format!!");
                return false;
            }
            try
            {
                toDate = DateTime.ParseExact(dtpToDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
            }
            catch
            {
                MsgBox.showWarning("Please enter the Date in correct format!!");
                return false;
            }

            if (SessionInfo.currentDate < toDate)
            {
                MsgBox.showWarning("Future date is not allowed!");
                return false;
            }
            if (fromDate > toDate)
            {
                MsgBox.showWarning("From date can not be greater than to date!!");
                return false;
            }

            if (!(SessionInfo.userBasicInformation.userCategory == UserCategory.BranchUser && SessionInfo.userBasicInformation.bankUserType == BankUserType.FieldUser) && cmbAgentName.SelectedIndex < 1)
            {
                MsgBox.showWarning("Please select an Agent!!");
                return false;
            }
            return true;
        }
        private ChequeRequisitionReportSearchDto FillChequeBookInfoSearchDto()
        {


            ChequeRequisitionReportSearchDto cheRequeSearchDto = new ChequeRequisitionReportSearchDto();

            if (this.cmbAgentName.SelectedIndex > 0)
            {
                if (cmbAgentName.SelectedIndex != 0 && cmbAgentName.SelectedIndex != 1)
                {
                    cheRequeSearchDto.agentId = (long)cmbAgentName.SelectedValue;
                }
                if (cmbAgentName.SelectedIndex == 0 || cmbAgentName.SelectedIndex == 1)
                {
                    cheRequeSearchDto.agentId = 0; // all Agent
                }
            }
            if (cmbSubAgnetName.SelectedIndex > 1)
            {
                cheRequeSearchDto.subAgentId = (long)cmbSubAgnetName.SelectedValue;
            }
            else
            {
                cheRequeSearchDto.subAgentId = 0; // all SubAgent
            }
            #region
            //ChequeBookInfoSearchDto chequeBookAccSearchDto = new ChequeBookInfoSearchDto();
            //if (this.cmbAgentName.SelectedIndex > -1)
            //{
            //    if (cmbAgentName.SelectedIndex != 0)
            //    {
            //        chequeBookAccSearchDto.agentId = (long)cmbAgentName.SelectedValue;
            //    }
            //    if (cmbAgentName.SelectedIndex == 0)
            //    {
            //        chequeBookAccSearchDto.agentId = 0; // all Agent
            //    }
            //}
            //else chequeBookAccSearchDto.agentId = 0; // all Agent

            //if (this.cmbSubAgnetName.SelectedIndex > -1)
            //{
            //    if (cmbSubAgnetName.SelectedIndex != 0 && cmbSubAgnetName.SelectedIndex != 1)
            //    {
            //        chequeBookAccSearchDto.outletId = (long)cmbSubAgnetName.SelectedValue;
            //    }
            //    if (cmbSubAgnetName.SelectedIndex == 0 || cmbSubAgnetName.SelectedIndex == 1)
            //    {
            //        chequeBookAccSearchDto.outletId = 0; // all SubAgent
            //    }
            //}
            //else
            //{
            //    chequeBookAccSearchDto.outletId = 0;
            //}

            //chequeBookAccSearchDto.fromDate = UtilityServices.GetLongDate
            //(
            //    DateTime.ParseExact(dtpFromDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture)
            //);

            //chequeBookAccSearchDto.toDate = UtilityServices.GetLongDate
            //(
            //    DateTime.ParseExact(dtpToDate.Date.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture)
            //);


            //return chequeBookAccSearchDto;
            #endregion
            cheRequeSearchDto.fromDate = DateTime.ParseExact(dtpFromDate.Date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            cheRequeSearchDto.toDate = DateTime.ParseExact(dtpToDate.Date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            return cheRequeSearchDto;
        }
        private void frmChequeBookInformation_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }
        private void SetAgentList()
        {
            objAgentInfoList = objAgentServices.getAgentInfoBranchWise();
            BindingSource bs = new BindingSource();
            bs.DataSource = objAgentInfoList;
            AgentInformation agSelect = new AgentInformation();
            agSelect.businessName = "(Select)";
            AgentInformation agAll = new AgentInformation();
            agAll.businessName = "(All)";
            objAgentInfoList.Insert(0, agSelect);
            objAgentInfoList.Insert(1, agAll);
            UtilityServices.fillComboBox(cmbAgentName, bs, "businessName", "id");
            //UtilityServices.fillComboBox(cmbAgentName, bs, "businessName", "id");

            if (SessionInfo.userBasicInformation.userCategory == UserCategory.BranchUser && SessionInfo.userBasicInformation.bankUserType == BankUserType.FieldUser)
            {
                List<SubAgentInformation> subAgentInformation = new SubAgentServices().GetSubAgentsFieldOfficer();

                BindingSource bsSubAgents = new BindingSource();
                bsSubAgents.DataSource = subAgentInformation;

                try
                {
                    SubAgentInformation saiSelect = new SubAgentInformation();
                    saiSelect.name = "(Select)";

                    SubAgentInformation saiAll = new SubAgentInformation();
                    saiAll.name = "(All)";

                    subAgentInformation.Insert(0, saiSelect);
                    subAgentInformation.Insert(1, saiAll);
                }
                catch (Exception exp)
                { }
                UtilityServices.fillComboBox(cmbSubAgnetName, bsSubAgents, "name", "id");
                if (cmbSubAgnetName.Items.Count > 0)
                { cmbSubAgnetName.SelectedIndex = 0; }

            }
        }
        private void SetSubagent()
        {

            if (subAgentList != null)
            {
                BindingSource bs = new BindingSource();
                bs.DataSource = subAgentList;

                try
                {
                    SubAgentInformation saiSelect = new SubAgentInformation();
                    saiSelect.name = "(Select)";

                    SubAgentInformation saiAll = new SubAgentInformation();
                    saiAll.name = "(All)";

                    subAgentList.Insert(0, saiSelect);
                    subAgentList.Insert(1, saiAll);
                }
                catch //suppressed
                { }

                UtilityServices.fillComboBox(cmbSubAgnetName, bs, "name", "id");
                if (cmbSubAgnetName.Items.Count > 0)
                {
                    cmbSubAgnetName.SelectedIndex = 0;
                }
            }
        }
        private void controlActivity()
        {
            try
            {
                if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_CENTRALLY.ToString())) //branch user
                {
                    cmbAgentName.Enabled = true;
                    cmbSubAgnetName.Enabled = true;
                }
                else if (SessionInfo.rights.Contains(Rights.REPORT_VIEW_AGENTWISE.ToString())) //agent user
                {
                    cmbAgentName.SelectedValue = UtilityServices.getCurrentAgent().id;
                    cmbAgentName.Enabled = false;
                    cmbSubAgnetName.Enabled = true;
                    subAgentList = objAgentServices.GetSubagentsByAgentId((long)cmbAgentName.SelectedValue);
                    SetSubagent();
                }
                else if (SessionInfo.userBasicInformation.userCategory == UserCategory.BranchUser && SessionInfo.userBasicInformation.bankUserType == BankUserType.FieldUser)
                {
                    setUIForFieldOfficer();

                }
                else
                {
                    SubAgentInformation currentSubagentInfo = UtilityServices.getCurrentSubAgent();
                    cmbAgentName.SelectedValue = currentSubagentInfo.agent.id;
                    subAgentList = objAgentServices.GetSubagentsByAgentId((long)cmbAgentName.SelectedValue);
                    SetSubagent();
                    cmbAgentName.Enabled = false;
                    cmbSubAgnetName.SelectedValue = currentSubagentInfo.id;
                    cmbSubAgnetName.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }
        private void cmbAgentName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmbAgentName.SelectedIndex > -1)
                {
                    if (cmbAgentName.SelectedIndex > 0)
                    {
                        subAgentList = objAgentServices.GetSubagentsByAgentId((long)cmbAgentName.SelectedValue);
                    }
                    else subAgentList = null;
                    SetSubagent();
                }
                if (cmbAgentName.SelectedIndex == 0)
                {
                    cmbSubAgnetName.DataSource = null;
                    cmbSubAgnetName.Items.Clear();
                    return;
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }

        }

    }
}
