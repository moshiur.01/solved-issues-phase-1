﻿using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.Module.ChequeRequisition.Report
{
    partial class frmChequeBookInformation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClose = new System.Windows.Forms.Button();
            this.btnViewReport = new System.Windows.Forms.Button();
            this.lblTodate = new System.Windows.Forms.Label();
            this.lblFromDate = new System.Windows.Forms.Label();
            this.lblSubAgent = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblAgent = new System.Windows.Forms.Label();
            this.cmbAgentName = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.dtpToDate = new MISL.Ababil.Agent.UI.forms.CustomControls.CustomDateTimePicker();
            this.dtpFromDate = new MISL.Ababil.Agent.UI.forms.CustomControls.CustomDateTimePicker();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.cmbSubAgnetName = new MISL.Ababil.Agent.CustomControls.CustomComboBoxDropDownList();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(620, 193);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(98, 33);
            this.btnClose.TabIndex = 24;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnViewReport
            // 
            this.btnViewReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(170)))));
            this.btnViewReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewReport.ForeColor = System.Drawing.Color.White;
            this.btnViewReport.Location = new System.Drawing.Point(473, 193);
            this.btnViewReport.Name = "btnViewReport";
            this.btnViewReport.Size = new System.Drawing.Size(127, 33);
            this.btnViewReport.TabIndex = 23;
            this.btnViewReport.Text = "View Report";
            this.btnViewReport.UseVisualStyleBackColor = false;
            this.btnViewReport.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // lblTodate
            // 
            this.lblTodate.AutoSize = true;
            this.lblTodate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lblTodate.Location = new System.Drawing.Point(437, 151);
            this.lblTodate.Name = "lblTodate";
            this.lblTodate.Size = new System.Drawing.Size(33, 17);
            this.lblTodate.TabIndex = 19;
            this.lblTodate.Text = "To :";
            // 
            // lblFromDate
            // 
            this.lblFromDate.AutoSize = true;
            this.lblFromDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lblFromDate.Location = new System.Drawing.Point(52, 154);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(48, 17);
            this.lblFromDate.TabIndex = 17;
            this.lblFromDate.Text = "From :";
            // 
            // lblSubAgent
            // 
            this.lblSubAgent.AutoSize = true;
            this.lblSubAgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lblSubAgent.Location = new System.Drawing.Point(410, 113);
            this.lblSubAgent.Name = "lblSubAgent";
            this.lblSubAgent.Size = new System.Drawing.Size(58, 17);
            this.lblSubAgent.TabIndex = 15;
            this.lblSubAgent.Text = "Outlet  :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(292, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(214, 20);
            this.label1.TabIndex = 25;
            this.label1.Text = "Cheque Book Information";
            // 
            // lblAgent
            // 
            this.lblAgent.AutoSize = true;
            this.lblAgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.lblAgent.Location = new System.Drawing.Point(46, 113);
            this.lblAgent.Name = "lblAgent";
            this.lblAgent.Size = new System.Drawing.Size(53, 17);
            this.lblAgent.TabIndex = 13;
            this.lblAgent.Text = "Agent :";
            // 
            // cmbAgentName
            // 
            this.cmbAgentName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAgentName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbAgentName.FormattingEnabled = true;
            this.cmbAgentName.InputScopeAllowEmpty = false;
            this.cmbAgentName.IsValid = null;
            this.cmbAgentName.Location = new System.Drawing.Point(104, 113);
            this.cmbAgentName.Name = "cmbAgentName";
            this.cmbAgentName.PromptText = "(Select)";
            this.cmbAgentName.ReadOnly = false;
            this.cmbAgentName.ShowMandatoryMark = false;
            this.cmbAgentName.Size = new System.Drawing.Size(264, 21);
            this.cmbAgentName.TabIndex = 40;
            this.cmbAgentName.ValidationErrorMessage = "Validation Error!";
            this.cmbAgentName.SelectedIndexChanged += new System.EventHandler(this.cmbAgentName_SelectedIndexChanged);
            // 
            // dtpToDate
            // 
            this.dtpToDate.Date = "10/02/2016";
            this.dtpToDate.Location = new System.Drawing.Point(473, 147);
            this.dtpToDate.MaximumSize = new System.Drawing.Size(400, 25);
            this.dtpToDate.MinimumSize = new System.Drawing.Size(60, 25);
            this.dtpToDate.Name = "dtpToDate";
            this.dtpToDate.PresetServerDate = true;
            this.dtpToDate.Size = new System.Drawing.Size(243, 25);
            this.dtpToDate.TabIndex = 32;
            this.dtpToDate.Value = new System.DateTime(2016, 2, 10, 11, 59, 21, 327);
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.Date = "10/02/2016";
            this.dtpFromDate.Location = new System.Drawing.Point(104, 151);
            this.dtpFromDate.MaximumSize = new System.Drawing.Size(400, 25);
            this.dtpFromDate.MinimumSize = new System.Drawing.Size(60, 25);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.PresetServerDate = true;
            this.dtpFromDate.Size = new System.Drawing.Size(264, 25);
            this.dtpFromDate.TabIndex = 31;
            this.dtpFromDate.Value = new System.DateTime(2016, 2, 10, 11, 58, 23, 924);
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(791, 26);
            this.customTitlebar1.TabIndex = 0;
            // 
            // cmbSubAgnetName
            // 
            this.cmbSubAgnetName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSubAgnetName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbSubAgnetName.FormattingEnabled = true;
            this.cmbSubAgnetName.InputScopeAllowEmpty = false;
            this.cmbSubAgnetName.IsValid = null;
            this.cmbSubAgnetName.Location = new System.Drawing.Point(473, 113);
            this.cmbSubAgnetName.Name = "cmbSubAgnetName";
            this.cmbSubAgnetName.PromptText = "(Select)";
            this.cmbSubAgnetName.ReadOnly = false;
            this.cmbSubAgnetName.ShowMandatoryMark = false;
            this.cmbSubAgnetName.Size = new System.Drawing.Size(243, 21);
            this.cmbSubAgnetName.TabIndex = 43;
            this.cmbSubAgnetName.ValidationErrorMessage = "Validation Error!";
            // 
            // frmChequeBookInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(791, 266);
            this.Controls.Add(this.cmbSubAgnetName);
            this.Controls.Add(this.cmbAgentName);
            this.Controls.Add(this.dtpToDate);
            this.Controls.Add(this.dtpFromDate);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnViewReport);
            this.Controls.Add(this.lblTodate);
            this.Controls.Add(this.lblFromDate);
            this.Controls.Add(this.lblSubAgent);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblAgent);
            this.Controls.Add(this.customTitlebar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "frmChequeBookInformation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cheque Book Information";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmChequeBookInformation_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomTitlebar customTitlebar1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnViewReport;
        private System.Windows.Forms.Label lblTodate;
        private System.Windows.Forms.Label lblFromDate;
        private System.Windows.Forms.Label lblSubAgent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblAgent;
        private Agent.UI.forms.CustomControls.CustomDateTimePicker dtpToDate;
        private Agent.UI.forms.CustomControls.CustomDateTimePicker dtpFromDate;
        private CustomComboBoxDropDownList cmbAgentName;
        private CustomComboBoxDropDownList cmbSubAgnetName;


    }
}