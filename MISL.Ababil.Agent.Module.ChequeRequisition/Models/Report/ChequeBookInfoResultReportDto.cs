﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Module.ChequeRequisition.Models.Report
{
   public class ChequeBookInfoResultReportDto
    {
       public long agentId { get; set; }
       public long outletId { get; set; }
       public string agentName { get; set; }
        public string outletName;
        public string bank { get; set; }
        public string accountNo { get; set; }
        public string accountTitle { get; set; }
        public long? noOfLeaves { get; set; }
        public long? routingNo { get; set; }
        public long? trNumber { get; set; }
    }
}
