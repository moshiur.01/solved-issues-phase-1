﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Module.ChequeRequisition.Models.Report
{
   public class ChequeBookInfoResultDto
    {
        public long agentId { set; get; }
        public long outletId { set; get; }
        public string agentName { set; get; }
        public string outletName { set; get; }
        public string accountNo { set; get; }
        public string accountTitle { set; get; }
        public long? noOfLeaves { set; get; }
        public string routingNo { set; get; }
        public decimal trNumber { set; get; }

    }
}
