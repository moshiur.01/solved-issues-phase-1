﻿using System;
using System.Windows.Forms;

namespace MISL.Ababil.Agent.Module.Common.Exporter
{
    public partial class CustomDataGridViewHeader : UserControl
    {
        private DataGridView _filter = null;
        private string _exportReportTitle = "Title";
        private string _exportReportSubtitleOne = "Subtitle One";
        private string _exportReportSubtitleTwo = "Subtitle Two";
        private string _exportReportSubtitleThree = "Subtitle Three";

        public CustomDataGridViewHeader()
        {
            InitializeComponent();
        }

        public string HeaderText
        {
            get { return lblHeaderText.Text; }
            set { lblHeaderText.Text = value; }
        }

        public DataGridView Filter
        {
            get { return _filter; }
            set { _filter = value; }
        }

        public string ExportReportTitle
        {
            get { return _exportReportTitle; }
            set { _exportReportTitle = value; }
        }

        public string ExportReportSubtitleOne
        {
            get { return _exportReportSubtitleOne; }
            set { _exportReportSubtitleOne = value; }
        }

        public string ExportReportSubtitleTwo
        {
            get { return _exportReportSubtitleTwo; }
            set { _exportReportSubtitleTwo = value; }
        }

        public string ExportReportSubtitleThree
        {
            get { return _exportReportSubtitleThree; }
            set { _exportReportSubtitleThree = value; }
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFilter.Text))
            {
                int i = 0;
                for (; i < _filter.Rows.Count; i++)
                {
                    _filter.Rows[i].Visible = true;
                }
                //lblItemsFound.Text = "   Item(s) Found: " + i.ToString();
            }
            else
            {
                _filter.ClearSelection();
                if(_filter.DataSource!=null)
                { ((CurrencyManager)BindingContext[_filter.DataSource]).SuspendBinding();}
                _filter.CurrentCell = null;

                int found = 0;
                for (int i = 0; i < _filter.Rows.Count; i++)
                {
                    if (
                        _filter.Rows[i].Cells[_filter.Columns.Count - 1].Value?.ToString().ToLower()
                            .IndexOf(txtFilter.Text.ToLower()) < 0)
                    {
                        _filter.Rows[i].Visible = false;
                    }
                    else
                    {
                        _filter.Rows[i].Visible = true;
                        found++;
                    }
                }
                //lblItemsFound.Text = "   Item(s) Found: " + found.ToString();
            }
        }

        private void btnExportToPDF_Click(object sender, EventArgs e)
        {
            try
            {
                new ExportUtility().GeneratePDFFromDataGridView(
                        _filter,
                        _exportReportTitle,
                        _exportReportSubtitleOne,
                        _exportReportSubtitleTwo,
                        _exportReportSubtitleThree
                    );
            }
            catch (IndexOutOfRangeException)
            {
                //MessageBox.Show("Data not available.");
            }
            catch (Exception ex)
            {
                //MsgBox.showWarning(ex.Message);
            }
        }

        private void btnFilterClear_Click(object sender, EventArgs e)
        {
            txtFilter.Text = "";
            txtFilter.Focus();
            for (int i = 0; i < _filter.Rows.Count; i++)
            {
                _filter.Rows[i].Visible = true;
            }
        }
    }
}