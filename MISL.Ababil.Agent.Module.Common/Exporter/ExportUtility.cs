﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using MISL.Ababil.Agent.Module.Common.Exportor;
using Orientation = MigraDoc.DocumentObjectModel.Orientation;

namespace MISL.Ababil.Agent.Module.Common.Exporter
{
    public class ExportUtility
    {
        public void GeneratePDFFromDataGridView(DataGridView dataGridView, string title, string subtitleOne, string subtitleTwo, string subtitleThree, bool generateSerial = true)
        {
            if (dataGridView.Columns.Count == 0 || dataGridView.Rows.Count == 0)
            {
                throw new IndexOutOfRangeException();
            }

            Dictionary<int, string> cols = new Dictionary<int, string>();
            for (int i = 0; i < dataGridView.Columns.Count; i++)
            {
                if (
                        dataGridView.Columns[i] is DataGridViewButtonColumn
                    ||
                        dataGridView.Columns[i] is DataGridViewLinkColumn
                    ||
                        dataGridView.Columns[i] is DataGridViewImageColumn
                    ||
                        dataGridView.Columns[i] is DataGridViewCheckBoxColumn
                    )
                {
                    continue;
                }
                if (dataGridView.Columns[i].Visible == true)
                {
                    cols.Add(i, dataGridView.Columns[i].HeaderText);
                }
            }

            if (cols.ElementAt(cols.Count - 1).Value.ToLower() == "filter")
            {
                cols.Remove(cols.ElementAt(cols.Count - 1).Key);
            }

            try
            {
                frmExportUtilityDlg frm = new frmExportUtilityDlg();
                for (int i = 0; i < cols.Count; i++)
                {
                    frm.clbColumns.Items.Add(cols.ElementAt(i).Value, true);
                }
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
                //ProgressUIManager.ShowProgress(frm);

                Document document = new Document
                {
                    Info =
                    {
                        Title = title,
                        Author = "MISL Ababil Agent Banking",
                        Keywords = "",
                        Tag = title,
                        Comment = "",
                        Subject = title
                    }
                };

                Unit width, height;
                document.DefaultPageSetup.PageFormat = PageFormat.A4;
                document.DefaultPageSetup.HeaderDistance = new Unit(0.25, UnitType.Inch);
                document.DefaultPageSetup.FooterDistance = new Unit(0.10, UnitType.Inch);
                document.DefaultPageSetup.LeftMargin = "0.25in";
                document.DefaultPageSetup.RightMargin = "0.25in";
                document.DefaultPageSetup.TopMargin = "1.20in";
                document.DefaultPageSetup.BottomMargin = "0.30in";

                if (frm.rBtnPortrait.Checked == true)
                {
                    document.DefaultPageSetup.Orientation = Orientation.Portrait;
                }
                else
                {
                    document.DefaultPageSetup.Orientation = Orientation.Landscape;
                }

                Section section = document.AddSection();

                Paragraph paragraph = section.Headers.Primary.AddParagraph();
                paragraph.AddText(title);
                paragraph.Format.Font.Size = 16;
                paragraph.Format.Font.Bold = true;
                paragraph.Format.Alignment = ParagraphAlignment.Center;

                paragraph = section.Headers.Primary.AddParagraph();
                paragraph.AddText(subtitleOne);
                paragraph.Format.Font.Size = 10;
                paragraph.Format.Alignment = ParagraphAlignment.Center;

                paragraph = section.Headers.Primary.AddParagraph();
                paragraph.AddText(subtitleTwo);
                paragraph.Format.Font.Size = 8;
                paragraph.Format.Alignment = ParagraphAlignment.Center;

                paragraph = section.Headers.Primary.AddParagraph();
                paragraph.AddText(subtitleThree);
                paragraph.Format.Font.Size = 12;
                paragraph.Format.Font.Bold = true;
                paragraph.Format.Alignment = ParagraphAlignment.Center;


                paragraph = section.Footers.Primary.AddParagraph();
                paragraph.AddNumPagesField();
                paragraph.Format.Font.Size = 8;
                paragraph.Format.Alignment = ParagraphAlignment.Right;

                Table table = section.AddTable();

                table.Borders.Width = new Unit(0.05, UnitType.Millimeter);
                table.Borders.Color = Color.FromRgbColor(0, Colors.Gray);
                table.Style = "Table";
                table.Rows.LeftIndent = 0;

                Column column = null;

                //serial
                int serialCol = 0;
                if (generateSerial == true)
                {
                    serialCol = 1;
                    column = table.AddColumn();
                    column.Format.Alignment = ParagraphAlignment.Center;
                }

                column = null;

                int j = 0;
                for (int i = 0; i < cols.Count; i++)
                {
                    if (frm.clbColumns.GetItemCheckState(i) == CheckState.Checked)
                    {
                        column = table.AddColumn();
                        column.Format.Alignment = ParagraphAlignment.Center;
                        column.Width = new Unit(dataGridView.Columns[cols.Keys.ElementAt(i)].Width, UnitType.Point);
                    }
                }

                //table header
                Row row = table.AddRow();
                row.HeadingFormat = true;
                row.Height = new Unit(0.55, UnitType.Centimeter);
                row.VerticalAlignment = VerticalAlignment.Center;

                Cell cell = null;

                for (int i = 0; i < cols.Count; i++)
                {
                    if (generateSerial == true && i == 0)
                    {
                        cell = null;
                        cell = row.Cells[j++];
                        cell.Format.Font.Color = Colors.Black;
                        cell.Format.Alignment = ParagraphAlignment.Left;
                        cell.Format.Font.ApplyFont(new Font(dataGridView.ColumnHeadersDefaultCellStyle.Font.Name, dataGridView.ColumnHeadersDefaultCellStyle.Font.Size));
                        cell.Format.Font.Bold = true;
                        cell.AddParagraph("No.");
                        //continue;
                    }

                    //if (generateSerial == false && i == 0)
                    //{
                    //    cell = null;
                    //    cell = row.Cells[j];
                    //    cell.Format.Font.Color = Colors.Black;
                    //    cell.Format.Alignment = ParagraphAlignment.Left;
                    //    cell.Format.Font.ApplyFont(new Font(dataGridView.ColumnHeadersDefaultCellStyle.Font.Name, dataGridView.ColumnHeadersDefaultCellStyle.Font.Size));
                    //    cell.Format.Font.Bold = true;
                    //    cell.AddParagraph(dataGridView.Columns[cols.Keys.ElementAt(i)].HeaderText);
                    //    continue;
                    //}

                    if (frm.clbColumns.GetItemCheckState(i) == CheckState.Checked)
                    {
                        cell = null;
                        cell = row.Cells[j++];
                        cell.Format.Font.Color = Colors.Black;
                        cell.Format.Alignment = ParagraphAlignment.Left;
                        cell.Format.Font.ApplyFont(new Font(dataGridView.ColumnHeadersDefaultCellStyle.Font.Name, dataGridView.ColumnHeadersDefaultCellStyle.Font.Size));
                        cell.Format.Font.Bold = true;
                        cell.AddParagraph(dataGridView.Columns[cols.Keys.ElementAt(i)].HeaderText);
                    }
                }

                int count = 1;
                for (int i = 0; i < dataGridView.Rows.Count; i++)
                {
                    if (dataGridView.Rows[i].Visible == false)
                    {
                        continue;
                    }
                    j = 0;
                    row = table.AddRow();
                    for (int k = 0; k < cols.Count+1; k++)
                    {
                        if (generateSerial == true && k == 0)
                        {
                            cell = row.Cells[j++];
                            cell.Format.Font.Color = Colors.Black;
                            cell.Format.Alignment = ParagraphAlignment.Left;
                            cell.Format.Font.ApplyFont(new Font(dataGridView.DefaultCellStyle.Font.Name, dataGridView.DefaultCellStyle.Font.Size));
                            cell.AddParagraph(count++.ToString());
                            continue;
                        }
                        if (frm.clbColumns.GetItemCheckState(k - serialCol) == CheckState.Checked)                        
                        {
                            cell = row.Cells[j++];
                            cell.Format.Font.Color = Colors.Black;
                            cell.Format.Alignment = ParagraphAlignment.Left;
                            cell.Format.Font.ApplyFont(new Font(dataGridView.DefaultCellStyle.Font.Name, dataGridView.DefaultCellStyle.Font.Size));
                            if (dataGridView.Rows[i].Cells[cols.Keys.ElementAt(k - serialCol)].Value != null)
                            {
                                Paragraph p = cell.AddParagraph(dataGridView.Rows[i].Cells[cols.Keys.ElementAt(k - serialCol)].Value.ToString());
                                p.Format.PageBreakBefore = true;
                            }
                            else
                            {
                                cell.AddParagraph("");
                            }
                        }
                    }
                }

                PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer
                {
                    Document = document
                };

                pdfRenderer.RenderDocument();
                string tempFileLocation = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Ababil Cache", "Temp");
                Directory.CreateDirectory(tempFileLocation);
                tempFileLocation += "\\ExportDocument.pdf";
                pdfRenderer.PdfDocument.Save(tempFileLocation);
                //ProgressUIManager.CloseProgress();
                Process.Start(tempFileLocation);
            }
            catch (Exception ex)
            {
                //ProgressUIManager.CloseProgress();
                throw ex;
            }
        }

        public void GeneratePDFFromDataGridView()
        {
            Document document = new Document
            {
                Info =
                {
                    Author = "MISL Ababil Agent Banking",
                    Keywords = "MigraDoc, Examples, C#"
                }
            };

            Unit width, height;
            PageSetup.GetPageSize(PageFormat.A4, out width, out height);

            Section section = document.AddSection();
            section.PageSetup.PageHeight = height;
            section.PageSetup.PageWidth = width;
            section.PageSetup.LeftMargin = "0.25in";
            section.PageSetup.RightMargin = "0.25in";
            section.PageSetup.TopMargin = "0.25in";
            section.PageSetup.BottomMargin = "0.25in";

            TextFrame addressFrame = section.AddTextFrame();
            addressFrame.Width = width;
            addressFrame.Height = "1in";
            addressFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            addressFrame.Top = "0.25in";
            addressFrame.RelativeVertical = RelativeVertical.Page;
            addressFrame.Left = ShapePosition.Center;

            Paragraph paragraph = addressFrame.AddParagraph();
            paragraph.AddText("Title");
            paragraph.Format.Font.Size = 16;
            paragraph.Format.Alignment = ParagraphAlignment.Center;

            paragraph = addressFrame.AddParagraph();
            paragraph.AddText("SubTitle");
            paragraph.Format.Font.Size = 10;
            paragraph.Format.Alignment = ParagraphAlignment.Center;

            paragraph = addressFrame.AddParagraph();
            paragraph.AddText("EndTitle");
            paragraph.Format.Font.Size = 8;
            paragraph.Format.Alignment = ParagraphAlignment.Center;

            Table table = new Table
            {
                Borders = { Width = 0.25 },
                Style = "Table",
                Rows = { LeftIndent = 0 },
            };

            Column column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;
            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;

            Font font = new Font("Arial", 12);

            Row row = table.AddRow();
            Cell cell = row.Cells[0];
            {
                cell = row.Cells[0];

                cell.Format.Font.Color = Colors.Black;
                cell.Format.Alignment = ParagraphAlignment.Left;
                cell.Format.Font.ApplyFont(font);
                cell.AddParagraph("756-1");
            }
            {
                cell = row.Cells[1];
                cell.Format.Font.Color = Colors.Black;
                cell.Format.Alignment = ParagraphAlignment.Left;
                cell.Format.Font.ApplyFont(font);
                cell.AddParagraph("756-2");
            }

            TextFrame frame2 = section.AddTextFrame();
            frame2.Width = width;
            frame2.Height = height - new Unit(2.5, UnitType.Inch);
            frame2.RelativeHorizontal = RelativeHorizontal.Margin;
            frame2.Top = "1.05in";
            frame2.RelativeVertical = RelativeVertical.Page;
            frame2.Left = ShapePosition.Left;
            frame2.Add(table);

            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer
            {
                Document = document
            };

            pdfRenderer.RenderDocument();
            pdfRenderer.PdfDocument.Save("TestDocument.pdf");
            Process.Start("TestDocument.pdf");
        }
    }
}