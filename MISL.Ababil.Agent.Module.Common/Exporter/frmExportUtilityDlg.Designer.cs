﻿using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.Module.Common.Exportor
{
    partial class frmExportUtilityDlg : CustomForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClsoe = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.clbColumns = new System.Windows.Forms.CheckedListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.rBtnPortrait = new System.Windows.Forms.RadioButton();
            this.rBtnLandscape = new System.Windows.Forms.RadioButton();
            this.cbxAllColumns = new System.Windows.Forms.CheckBox();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.SuspendLayout();
            // 
            // btnClsoe
            // 
            this.btnClsoe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClsoe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnClsoe.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClsoe.FlatAppearance.BorderSize = 0;
            this.btnClsoe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClsoe.Image = global::MISL.Ababil.Agent.Module.Common.Properties.Resources.close;
            this.btnClsoe.Location = new System.Drawing.Point(268, 493);
            this.btnClsoe.Name = "btnClsoe";
            this.btnClsoe.Size = new System.Drawing.Size(96, 26);
            this.btnClsoe.TabIndex = 6;
            this.btnClsoe.Text = "Cancel";
            this.btnClsoe.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClsoe.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClsoe.UseVisualStyleBackColor = false;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnSearch.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Image = global::MISL.Ababil.Agent.Module.Common.Properties.Resources.Ok_16;
            this.btnSearch.Location = new System.Drawing.Point(166, 493);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(96, 26);
            this.btnSearch.TabIndex = 5;
            this.btnSearch.Text = "OK";
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Check columns to export.";
            // 
            // clbColumns
            // 
            this.clbColumns.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.clbColumns.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clbColumns.FormattingEnabled = true;
            this.clbColumns.HorizontalScrollbar = true;
            this.clbColumns.IntegralHeight = false;
            this.clbColumns.Location = new System.Drawing.Point(12, 80);
            this.clbColumns.Name = "clbColumns";
            this.clbColumns.ScrollAlwaysVisible = true;
            this.clbColumns.Size = new System.Drawing.Size(352, 349);
            this.clbColumns.TabIndex = 1;
            this.clbColumns.UseCompatibleTextRendering = true;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 439);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Page Orientation";
            // 
            // rBtnPortrait
            // 
            this.rBtnPortrait.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rBtnPortrait.AutoSize = true;
            this.rBtnPortrait.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rBtnPortrait.Location = new System.Drawing.Point(12, 462);
            this.rBtnPortrait.Name = "rBtnPortrait";
            this.rBtnPortrait.Size = new System.Drawing.Size(63, 17);
            this.rBtnPortrait.TabIndex = 3;
            this.rBtnPortrait.Text = "Portrait";
            this.rBtnPortrait.UseVisualStyleBackColor = true;
            // 
            // rBtnLandscape
            // 
            this.rBtnLandscape.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rBtnLandscape.AutoSize = true;
            this.rBtnLandscape.Checked = true;
            this.rBtnLandscape.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rBtnLandscape.Location = new System.Drawing.Point(82, 462);
            this.rBtnLandscape.Name = "rBtnLandscape";
            this.rBtnLandscape.Size = new System.Drawing.Size(79, 17);
            this.rBtnLandscape.TabIndex = 4;
            this.rBtnLandscape.TabStop = true;
            this.rBtnLandscape.Text = "Landscape";
            this.rBtnLandscape.UseVisualStyleBackColor = true;
            // 
            // cbxAllColumns
            // 
            this.cbxAllColumns.AutoSize = true;
            this.cbxAllColumns.Checked = true;
            this.cbxAllColumns.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxAllColumns.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.cbxAllColumns.Location = new System.Drawing.Point(15, 57);
            this.cbxAllColumns.Name = "cbxAllColumns";
            this.cbxAllColumns.Size = new System.Drawing.Size(93, 17);
            this.cbxAllColumns.TabIndex = 8;
            this.cbxAllColumns.Text = "(All Columns)";
            this.cbxAllColumns.UseVisualStyleBackColor = true;
            this.cbxAllColumns.CheckedChanged += new System.EventHandler(this.cbxAllColumns_CheckedChanged);
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(376, 26);
            this.customTitlebar1.TabIndex = 9;
            this.customTitlebar1.TabStop = false;
            // 
            // frmExportUtilityDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(376, 531);
            this.Controls.Add(this.customTitlebar1);
            this.Controls.Add(this.cbxAllColumns);
            this.Controls.Add(this.rBtnLandscape);
            this.Controls.Add(this.rBtnPortrait);
            this.Controls.Add(this.clbColumns);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClsoe);
            this.Controls.Add(this.btnSearch);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmExportUtilityDlg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Export";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmExportUtilityDlg_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        //private CustomControls.CustomTitlebar customTitlebar1;
        private System.Windows.Forms.Button btnClsoe;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.CheckedListBox clbColumns;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.RadioButton rBtnPortrait;
        public System.Windows.Forms.RadioButton rBtnLandscape;
        private System.Windows.Forms.CheckBox cbxAllColumns;
        private CustomTitlebar customTitlebar1;
    }
}