﻿using System;
using System.Windows.Forms;
using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.Module.Common.Exportor
{
    public partial class frmExportUtilityDlg : CustomForm
    {
        public frmExportUtilityDlg()    
        {
            InitializeComponent();
        }

        private void frmExportUtilityDlg_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

        }

        private void cbxAllColumns_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < clbColumns.Items.Count; i++)
            {
                if (cbxAllColumns.Checked)
                {
                    clbColumns.SetItemCheckState(i, CheckState.Checked);
                }
                else
                {
                    clbColumns.SetItemCheckState(i, CheckState.Unchecked);
                }
            }
        }
    }
}