﻿using System;
using System.Drawing;
using System.Windows.Forms;
using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.Module.Common.UI.ProgressUI
{
    public partial class frmProgress : CustomForm
    {
        public frmProgress()
        {
            this.TransparencyKey = Color.Empty;
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            InitializeComponent();
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);            
            //label1.BackColor = System.Drawing.Color.Transparent;
        }

        private void frmProgress_Load(object sender, EventArgs e)
        {
            Application.DoEvents();
        }        
    }
}
