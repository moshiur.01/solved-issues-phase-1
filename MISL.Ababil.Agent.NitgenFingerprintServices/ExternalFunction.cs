﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace MISL.Ababil.Agent.NitgenFingerprintServices
{
    public class ExternalFunction
    {
        [DllImport("NBioNFIQ.dll")]
        public static extern uint NBioAPI_GetNFIQInfoFromRaw(byte[] pRawImage, uint nWidth,
    uint nHeight, ref int pNFIQ);

        [DllImport("NTGImageMan.dll")]
        public static extern uint NTGImg_CreateImage(ref uint pImageHandle, byte[] pSourcedata, uint sourceSize, int nImageType, ref int pImageWidth, ref int pImageHeight);

        [DllImport("NTGImageMan.dll")]
        public unsafe static extern uint NTGImg_WsqBuf(uint hImage, byte** pDestData, ref uint pDestSize, float ratio);


        public static IntPtr GetIntPtr_BySize(int nSize)
        {
            IntPtr retptr = Marshal.AllocHGlobal(Marshal.SizeOf(nSize));

            return retptr;

        }

        public unsafe static byte[] GetByteArrayFromBytePointer(byte* pBytes, int length)
        {
            byte[] convertarray = new byte[length];
            if (pBytes != null)
            {
                UnmanagedMemoryStream readstream = new UnmanagedMemoryStream(pBytes, length, length, FileAccess.Read);
                readstream.Read(convertarray, 0, length);
                readstream.Close();

            }

            return convertarray;

        }


    }
}
