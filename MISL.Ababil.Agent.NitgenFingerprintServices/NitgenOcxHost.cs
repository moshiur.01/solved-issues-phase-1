﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NITGEN.SDK.NBioBSP;

namespace MISL.Ababil.Agent.NitgenFingerprintServices
{
    public partial class NitgenOcxHost : Form
    {
        public const string FingerprintNotfoundErrorMessage =
            "Couldn't find the fingerprint device!\n\nPlease connect the fingerprint device and try again.";

        public enum OperationType
        {
            Register,
            Capture
        }

        private OperationType _Type;

        public string CaptureDataData;
        public List<string> EnrollData;
        public bool StillCapturing = true;

        NBioAPI m_NBioAPI;
        NBioAPI.Export m_Export;

        private bool m_bBspInit;

        private void BSP_Init()
        {

            m_bBspInit = false;

            m_NBioAPI = new NBioAPI();
            m_Export = new NBioAPI.Export(m_NBioAPI);

            if (m_NBioAPI.OpenDevice(NBioAPI.Type.DEVICE_ID.AUTO) == NBioAPI.Error.NONE)
            {
                m_bBspInit = true;
            }

        }

        private void BSP_Dispose()
        {
            m_NBioAPI.CloseDevice(NBioAPI.Type.DEVICE_ID.AUTO);
        }


        public NitgenOcxHost(OperationType type)
        {
            InitializeComponent();
            m_NBioAPI = new NBioAPI();
            _Type = type;
        }

        private void NitgenOcxHost_Load(object sender, EventArgs e)
        {
            switch (_Type)
            {
                case OperationType.Capture:
                    CaptureFingerprint();
                    break;
                case OperationType.Register:
                    Register();
                    break;
            }
            //CaptureFingerprint();
            this.Close();
        }

        //To authenticate
        private void CaptureFingerprint()
        {
            try
            {
                uint ret = NBioAPI.Error.NONE;
                NBioAPI.Type.HFIR CaptureFIR;

                StillCapturing = true;

                BSP_Init();
                if (!m_bBspInit)
                {
                    MessageBox.Show(FingerprintNotfoundErrorMessage, "Error", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    StillCapturing = false;
                    return;
                }

                ret = m_NBioAPI.Capture(out CaptureFIR, NBioAPI.Type.TIMEOUT.DEFAULT, null);
                if (ret == NBioAPI.Error.NONE)
                {
                    NBioAPI.Export.EXPORT_DATA exportdata;

                    //exportdata.FingerData[0].Template[0].Data

                    ret = m_Export.NBioBSPToFDx(CaptureFIR, out exportdata, NBioAPI.Type.MINCONV_DATA_TYPE.MINCONV_TYPE_FDU);

                    //NBioAPI.Type.MINCONV_DATA_TYPE.

                    if (ret != NBioAPI.Error.NONE)
                    {
                        MessageBox.Show("Export Error!!!");
                        CaptureDataData = "";
                        StillCapturing = false;
                        return;
                    }
                    CaptureDataData = Convert.ToBase64String(exportdata.FingerData[0].Template[0].Data);

                }
                Close();
                StillCapturing = false;
            }
            catch { StillCapturing = false; }
        }


        private void Register()
        {

            EnrollData = new List<string>();
            uint ret = NBioAPI.Error.NONE;
            NBioAPI.Type.HFIR NewFIR;

            StillCapturing = true;
            NBioAPI.Type.HFIR haudit_fir = new NBioAPI.Type.HFIR();

            BSP_Init();
            if (!m_bBspInit)
            {
                MessageBox.Show(FingerprintNotfoundErrorMessage, "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                StillCapturing = false;
                return;
            }

            ret = m_NBioAPI.Enroll(null, out NewFIR, null, NBioAPI.Type.TIMEOUT.DEFAULT, haudit_fir, null);
            if (ret == NBioAPI.Error.NONE)
            {
                NBioAPI.Export.EXPORT_DATA exportdata;
                NBioAPI.Export.EXPORT_AUDIT_DATA auditdata;
                ret = m_Export.NBioBSPToFDx(NewFIR, out exportdata, NBioAPI.Type.MINCONV_DATA_TYPE.MINCONV_TYPE_FDU);
                ret |= m_Export.NBioBSPToImage(haudit_fir, out auditdata);

                CaptureDataData = "";
                if (ret != NBioAPI.Error.NONE)
                {
                    MessageBox.Show("Export Error!!!");
                    StillCapturing = false;
                    return;
                }

                List<EnrollTemplate> templates = new List<EnrollTemplate>();

                //byte[] templateBytes = exportdata.FingerData[0].Template[0].Data;
                //template
                for (int inum = 0; inum < exportdata.FingerData.Length; inum++)
                {
                    EnrollTemplate template = new EnrollTemplate();
                    template.Template = exportdata.FingerData[inum].Template[0].Data;
                    template.FingerId = exportdata.FingerData[inum].FingerID;
                    templates.Add(template);
                    //    template_repo.Append(1, exportdata.FingerData[inum].FingerID, 1, exportdata.FingerData[inum].Template[0].Data);

                    //    /*
                    //         for (int nSample = 2; nSample <= exportdata.SamplesPerFinger; nSample++ )
                    //             template_repo.Append(1, exportdata.FingerData[inum].FingerID, nSample, exportdata.FingerData[inum].Template[nSample].Data);
                    //     * */


                    //    m_nRegTemplate++;
                }
                //image
                //if (image_repo != null)
                for (int inum = 0; inum < auditdata.AuditData.Length; inum++)
                {
                    int GetQty = 0;
                    ExternalFunction.NBioAPI_GetNFIQInfoFromRaw(auditdata.AuditData[0].Image[0].Data, auditdata.ImageWidth, auditdata.ImageHeight, ref GetQty);

                    uint ImageHandle = 0;
                    int tempwidth = int.Parse(auditdata.ImageWidth.ToString());
                    int tempheight = int.Parse(auditdata.ImageHeight.ToString());

                    ExternalFunction.NTGImg_CreateImage(ref ImageHandle, auditdata.AuditData[0].Image[0].Data,
                        auditdata.ImageWidth * auditdata.ImageHeight, 1, ref tempwidth, ref tempheight);


                    uint WsqSize = uint.Parse((tempwidth * tempheight).ToString());


                    byte[] imageBytes = null;
                    unsafe
                    {
                        byte* pDestData = null;

                        uint sret = ExternalFunction.NTGImg_WsqBuf(ImageHandle, &pDestData, ref WsqSize, 0.75f);

                        imageBytes = ExternalFunction.GetByteArrayFromBytePointer(pDestData, int.Parse(WsqSize.ToString()));

                        //image_repo.Append(Userindex, auditdata.AuditData[0].FingerID, 1, byte.Parse(GetQty.ToString())
                        //, int.Parse(WsqSize.ToString()), imageBytes);
                    }

                    //TODO: Read image quality from configuration

                    //Format
                    //<TempleteData>#<FingerId>#<Quality>#<ImageBytes>
                    //m_nRegImage++;
                    EnrollTemplate template;
                    bool haveToAdd = false;
                    if (templates.Count < inum + 1)
                    {
                        template = new EnrollTemplate();
                    }
                    else
                    {
                        template = templates[inum];
                    }
                    template.Quality = GetQty;
                    template.ImageBytes = imageBytes;
                    if (haveToAdd) templates.Add(template);
                }
                templates.ForEach(x => EnrollData.Add(x.GetEnrollString()));
                //EnrollData.Add(String.Format("{0}#{1}#{2}#{3}", Convert.ToBase64String(templateBytes), exportdata.FingerData[0].FingerID.ToString(), GetQty.ToString(), Convert.ToBase64String(imageBytes)));

            }
            Close();
            StillCapturing = false;
        }  

        private void NitgenOcxHost_FormClosed(object sender, FormClosedEventArgs e)
        {
            BSP_Dispose();
        }
    }
}
