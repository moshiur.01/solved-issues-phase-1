﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.NitgenFingerprintServices
{
    internal class EnrollTemplate
    {
        public byte[] Template;
        public int Quality;
        public byte[] ImageBytes;
        public int FingerId;

        public string GetEnrollString()
        {
            return string.Format("{0}#{1}#{2}#{3}", Convert.ToBase64String(Template), FingerId, Quality, Convert.ToBase64String(ImageBytes));
        }
    }
}
