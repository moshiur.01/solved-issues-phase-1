﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MISL.Ababil.Agent.FingerprintServiceFactory
{
    public class FingerprintServiceFactory
    {
        public GenericFingerprintServices.FingerprintDevice getFingerprintDevice()
        {
            //return new M2SysFingerprintServices.M2SysFingerprintServices();//For M2Sys
            return new NitgenFingerprintServices.NitgenFingerprintServices();
        }

        public static bool CanReadMultipleFinger() { return true; } //Fro Nitgen
        //public static bool CanReadMultipleFinger() { return false; } //Fro M2Sys
    }
}