using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ActivityMonitor")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Millennium Information Solution Limited")]
[assembly: AssemblyProduct("ActivityMonitor")]
[assembly: AssemblyCopyright("Copyright © 2015 Millennium Information Solution Limited")]
[assembly: AssemblyTrademark("Millennium Information Solution Limited")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("47c47bb6-5c09-4da2-a2db-a6e7c845a646")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.3.0425")]
[assembly: AssemblyFileVersion("1.0.3.0425")]

[assembly: AssemblyInformationalVersion("1.0.0-alpha-02")]