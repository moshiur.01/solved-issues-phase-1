﻿using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Services.Communication;

namespace MISL.Ababil.Agent.Communication
{
    public class DepositAccountCom
    {
   

      

        public string ChangeApplicationStatus(ApplicationStatusChangeDto applicationStatusChangeDto, ApplicationStatus applicationStatus)
        {
            string path = "resources/account/deposit/";

            switch (applicationStatus)
            {
                case ApplicationStatus.canceled:
                    path += "reject";
                    break;
                case ApplicationStatus.approved:
                    path += "approve";
                    break;
                case ApplicationStatus.correction:
                    path += "correction";
                    break;
                default:
                    break;
            }
            return new WebClientCommunicator<ApplicationStatusChangeDto, string>().GetPostResult(applicationStatusChangeDto, path);
        }

        public AccountOperatorDto GetAccountOperatorsByAccountNo(string accountNumber)
        {
            return new WebClientCommunicator<string, AccountOperatorDto>().GetResult(
                "resources/accountinfo/deposit/operators/", accountNumber);
        }
    }
}