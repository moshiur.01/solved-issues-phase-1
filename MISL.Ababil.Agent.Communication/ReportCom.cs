﻿using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;

namespace MISL.Ababil.Agent.Communication
{
    public class ReportCom
    {

        public static List<CustomerApplicationInfoDto> GetRejectedConsumerList(BasicSearchDto searcDto)
        {
            List<CustomerApplicationInfoDto> Data = new List<CustomerApplicationInfoDto>();

            string jsonObj = JsonConvert.SerializeObject(searcDto);
            WebClient client = new WebClient();
            try
            {
                ///report/customer/application/rejection
                string path = SessionInfo.rootServiceUrl + "resources/report/customer/application/rejection";
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.UploadString(path, "POST", jsonObj);
                string serviceResponse = UtilityCom.getServerResponse(client);
                if (!serviceResponse.Equals("OK"))
                {
                    return null;
                }
                else
                {
                    //using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(responseString)))
                    //{
                    //    var ser = new DataContractJsonSerializer(Data.GetType());
                    //    Data = ser.ReadObject(ms) as List<CustomerApplicationInfoDto>;
                    //}
                    Data = JsonConvert.DeserializeObject<List<CustomerApplicationInfoDto>>(responseString);
                    return Data;
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }


        public List<ItdAccReportResultDto> getItdOutstandingReportList(ItdAccReportSearchDto itdAccountSearchDto)
        {
            List<ItdAccReportResultDto> Data = new List<ItdAccReportResultDto>();
            var jsonString = JsonConvert.SerializeObject(itdAccountSearchDto);
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/termaccount/itd/info/installment";
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.UploadString(path, "POST", jsonString);
                string serviceResponse = UtilityCom.getServerResponse(client);
                if (!serviceResponse.Equals("OK")) return null;
                else
                {
                    Data = JsonConvert.DeserializeObject<List<ItdAccReportResultDto>>(responseString);
                    return Data;
                }
            }
            catch (WebException webEx)
            { throw new Exception(UtilityCom.parseErrorData(webEx)); }
        }
    }
}
