﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using Newtonsoft.Json;

namespace MISL.Ababil.Agent.Communication
{
    public class AgentIncomeCom
    {
        public List<AgentIncomeSearchResultDto> GetAgentIncomeSearchResultList(AgentIncomeSearchDto agentIncomeSearchDto)
        {
            List<AgentIncomeSearchResultDto> Data = new List<AgentIncomeSearchResultDto>();

            string jsonObj = JsonConvert.SerializeObject(agentIncomeSearchDto);
            WebClient client = new WebClient();
            try
            {
                //ToDo: Set URL
                string path = SessionInfo.rootServiceUrl + "resources/report/agent/income";
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.UploadString(path, "POST", jsonObj);
                string serviceResponse = UtilityCom.getServerResponse(client);
                if (!serviceResponse.Equals("OK"))
                {
                    return null;
                }
                else
                {
                    //using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(responseString)))
                    //{
                    //    Data = new DataContractJsonSerializer(Data.GetType()).ReadObject(ms) as List<AgentIncomeSearchResultDto>;
                    //}

                    Data = JsonConvert.DeserializeObject<List<AgentIncomeSearchResultDto>>(responseString);
                    return Data;
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }
    }
}