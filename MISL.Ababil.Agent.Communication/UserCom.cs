﻿using System;
using System.Collections.Generic;
using System.Net;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using Newtonsoft.Json;
using MISL.Ababil.Agent.Infrastructure.Models.reports;

namespace MISL.Ababil.Agent.Communication
{
    public class UserCom
    {
        public string CreateBranchUser(string branchUserJson)
        {
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/security/user/create";
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.UploadString(path, "POST", branchUserJson);
                string responseStatusCode;
                string responseStatusDescription;
                JsonCom.GetStatusCode(client, out responseStatusDescription, out responseStatusCode);
                if (responseStatusCode == HttpStatusCode.NotFound.ToString())
                    return null;
                return responseString;
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }

        }
        public List<SubAgentUserLimitDto> GetOutletUserInformation(string outletid)
        {

            List<SubAgentUserLimitDto> Data = new List<SubAgentUserLimitDto>();
            WebClient client = new WebClient();
            try
            {
                String path = SessionInfo.rootServiceUrl + "resources/userinfo/outletusers/" + outletid;
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.DownloadString(path);
                string responseStatusCode;
                string responseStatusDescription;
                JsonCom.GetStatusCode(client, out responseStatusDescription, out responseStatusCode);
                if (responseStatusCode == HttpStatusCode.NotFound.ToString())
                    return null;
                else
                {
                    Data = JsonConvert.DeserializeObject<List<SubAgentUserLimitDto>>(responseString);
                    return Data;
                }

            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }
        public List<BankUserReportDto> GetBanktUserInformation(BankUserSearchDto bankUserSearchDto)
        {

            List<BankUserReportDto> Data = new List<BankUserReportDto>();
            var jsonString = JsonConvert.SerializeObject(bankUserSearchDto);
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/userinfo/bankuser/report";
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.UploadString(path, "POST", jsonString);
                string serviceResponse = UtilityCom.getServerResponse(client);
                if (!serviceResponse.Equals("OK")) return null;
                else
                {
                    Data = JsonConvert.DeserializeObject<List<BankUserReportDto>>(responseString);
                    return Data;
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }



        public List<UserIdWiseAccApproveResultDto> GetUserIdWiseAccApproveInfo(UserIdWiseAccApproveSearchDto userIdWiseAccApproveSearchDto)
        {

            List<UserIdWiseAccApproveResultDto> Data = new List<UserIdWiseAccApproveResultDto>();
            var jsonString = JsonConvert.SerializeObject(userIdWiseAccApproveSearchDto);
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/consumerapp/userIdWise";
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.UploadString(path, "POST", jsonString);
                string serviceResponse = UtilityCom.getServerResponse(client);
                if (!serviceResponse.Equals("OK")) return null;
                else
                {
                    Data = JsonConvert.DeserializeObject<List<UserIdWiseAccApproveResultDto>>(responseString);
                    return Data;
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }
        public List<AgentUserReportDto> GetAgentUserInformation(UserSearchDtoForAgent userSearchDtoForAgent)
        {

            List<AgentUserReportDto> Data = new List<AgentUserReportDto>();
            var jsonString = JsonConvert.SerializeObject(userSearchDtoForAgent);
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/userinfo/agentuser/report";
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.UploadString(path, "POST", jsonString);
                string serviceResponse = UtilityCom.getServerResponse(client);
                if (!serviceResponse.Equals("OK")) return null;
                else
                {
                    Data = JsonConvert.DeserializeObject<List<AgentUserReportDto>>(responseString);
                    return Data;
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }
        public AgentUserSearchResultDto GetUserDetailsByUserName(string username)
        {
            AgentUserSearchResultDto Data = new AgentUserSearchResultDto();
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/userinfo/agentuserdetail/" + username;
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.DownloadString(path);
                string responseStatusCode;
                string responseStatusDescription;
                JsonCom.GetStatusCode(client, out responseStatusDescription, out responseStatusCode);
                if (responseStatusCode != HttpStatusCode.OK.ToString())
                { return null; }
                else
                {
                    Data = JsonConvert.DeserializeObject<AgentUserSearchResultDto>(responseString);
                    return Data;
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }

        //public List<OutletUserInfoReportResultDto> GetOutletUserInformation(string outletid)
        //{

        //    List<OutletUserInfoReportResultDto> Data = new List<OutletUserInfoReportResultDto>();
        //    WebClient client = new WebClient();
        //    try
        //    {
        //        String path = SessionInfo.rootServiceUrl + "resources/userinfo/outletusers/" + outletid;
        //        client = UtilityCom.setClientHeaders(client);
        //        string responseString = client.DownloadString(path);
        //        string responseStatusCode;
        //        string responseStatusDescription;
        //        JsonCom.GetStatusCode(client, out responseStatusDescription, out responseStatusCode);
        //        if (responseStatusCode == HttpStatusCode.NotFound.ToString())
        //            return null;
        //        else
        //        {
        //            Data = JsonConvert.DeserializeObject<List<OutletUserInfoReportResultDto>>(responseString);
        //            return Data;
        //        }

        //    }
        //    catch (WebException webEx)
        //    {
        //        throw new Exception(UtilityCom.parseErrorData(webEx));
        //    }
        //}
        public string SaveUser(AgentUserDto userDto)
        {
            var jsonString = JsonConvert.SerializeObject(userDto);
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/userinfo/agentuser";
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.UploadString(path, "POST", jsonString);
                string responseStatusCode;
                string responseStatusDescription;
                JsonCom.GetStatusCode(client, out responseStatusDescription, out responseStatusCode);
                if (responseStatusCode != HttpStatusCode.OK.ToString()) return null;
                else
                {
                    return "User saved successfully.";
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }
        public List<AgentUserSearchResultDto> GetUserList(AgentUserSearchDto searchDto)
        {
            List<AgentUserSearchResultDto> Data = new List<AgentUserSearchResultDto>();
            string jsonObj = JsonConvert.SerializeObject(searchDto);
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/userinfo/agentusersearch";
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.UploadString(path, "POST", jsonObj);
                string responseStatusCode;
                string responseStatusDescription;
                JsonCom.GetStatusCode(client, out responseStatusDescription, out responseStatusCode);
                if (responseStatusCode != HttpStatusCode.OK.ToString())
                { return null; }
                else
                {
                    Data = JsonConvert.DeserializeObject<List<AgentUserSearchResultDto>>(responseString);
                    return Data;
                }

                #region Dami Data
                //AgentUserSearchResultDto result = new AgentUserSearchResultDto();
                //result.userName = "User 1";
                //result.fullName = "Full Name 1";
                //result.userStatus = UserStatus.Inactive;
                //result.userType = UserType.Operational;
                //result.mobileNumber = "sadfsahdfsad";
                //result.userId = 1;
                //result.individualId = 11;
                //Data.Add(result);

                //result = new AgentUserSearchResultDto();
                //result.userName = "User 2";
                //result.fullName = "Full Name 2";
                //result.userStatus = UserStatus.Active;
                //result.userType = UserType.ReportViewer;
                //result.mobileNumber = "0294380";
                //result.userId = 2;
                //result.individualId = 22;
                //Data.Add(result);

                //return Data;
                #endregion
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }
        public AgentUserDto GetUserDetailsByUserId(AgentUserDetilSearchDto searchDto)
        {
            AgentUserDto Data = new AgentUserDto();
            string jsonObj = JsonConvert.SerializeObject(searchDto);
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/userinfo/agentuserdetail";
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.UploadString(path, "POST", jsonObj);
                string responseStatusCode;
                string responseStatusDescription;
                JsonCom.GetStatusCode(client, out responseStatusDescription, out responseStatusCode);
                if (responseStatusCode != HttpStatusCode.OK.ToString())
                { return null; }
                else
                {
                    Data = JsonConvert.DeserializeObject<AgentUserDto>(responseString);
                    return Data;
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }
        public string UpdateUser(AgentUserDto userDto)
        {
            var jsonString = JsonConvert.SerializeObject(userDto);
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/userinfo/agentuserupdate";
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.UploadString(path, "POST", jsonString);
                string responseStatusCode;
                string responseStatusDescription;
                JsonCom.GetStatusCode(client, out responseStatusDescription, out responseStatusCode);
                if (responseStatusCode != HttpStatusCode.OK.ToString()) return null;
                else
                {
                    return "User info updated successfully.";
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }
        public string ResetUserCredential(ResetPasswordDto resetPassDto)
        {
            var jsonString = JsonConvert.SerializeObject(resetPassDto);
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/userinfo/resetpassword";
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.UploadString(path, "POST", jsonString);
                string responseStatusCode;
                string responseStatusDescription;
                JsonCom.GetStatusCode(client, out responseStatusDescription, out responseStatusCode);

                if (responseStatusCode != HttpStatusCode.OK.ToString()) return null;
                else return "Reset credential successful.";
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }
    }
}