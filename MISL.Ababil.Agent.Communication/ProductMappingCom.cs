﻿using MISL.Ababil.Agent.Infrastructure.Models.common;
//using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.configuration;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.ssp;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.termaccount;

namespace MISL.Ababil.Agent.Communication
{
    public class ProductMappingCom
    {
        public static List<CbsProduct> GetProductsList(ProductType productType)
        {
            List<CbsProduct> Data = new List<CbsProduct>();
            var jsonString = JsonConvert.SerializeObject(productType);

            string jsonObj = JsonConvert.SerializeObject(null);
            WebClient client = new WebClient();
            try
            {
                //string path = SessionInfo.rootServiceUrl + "resources/agentproduct/agentproductsbytype" + productType.ToString();
                string path = SessionInfo.rootServiceUrl + "resources/agentproduct/agentproductsbytype";
                client = UtilityCom.setClientHeaders(client);
                //string responseString = client.DownloadString(path);
                string responseString = client.UploadString(path, "POST", jsonString);
                string serviceResponse = UtilityCom.getServerResponse(client);
                if (!serviceResponse.Equals("OK"))
                {
                    return null;
                }
                else
                {
                    Data = JsonConvert.DeserializeObject<List<CbsProduct>>(responseString);
                    
                    return Data;
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }


        public List<ProductConfigDto> getDepositMappingInfo()
        {
            List<ProductConfigDto> Data = new List<ProductConfigDto>();
            var jsonString = JsonConvert.SerializeObject(AccountType.Deposit);

            string jsonObj = JsonConvert.SerializeObject(null);
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/agentproduct/productconfigs";
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.UploadString(path, "POST", jsonString);
                string serviceResponse = UtilityCom.getServerResponse(client);
                if (!serviceResponse.Equals("OK")) return null;
                else
                {
                    Data = JsonConvert.DeserializeObject<List<ProductConfigDto>>(responseString);
                    return Data;
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }
        public string deleteDepositMap(ProductConfigDto depositMap)
        {
            var jsonString = JsonConvert.SerializeObject(depositMap.agentProduct);
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/agentproduct/agentproduct";
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.UploadString(path, "POST", jsonString);
                string responseStatusCode;
                string responseStatusDescription;
                JsonCom.GetStatusCode(client, out responseStatusDescription, out responseStatusCode);
                if (responseStatusCode == HttpStatusCode.NotFound.ToString())
                    return null;
                else
                {
                    return "Product deleted successfully";
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }
        public string saveDepositMap(List<ProductConfigDto> depositMapList)
        {
            var jsonString = JsonConvert.SerializeObject(depositMapList);
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/agentproduct/productconfig";
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.UploadString(path, "POST", jsonString);
                string responseStatusCode;
                string responseStatusDescription;
                JsonCom.GetStatusCode(client, out responseStatusDescription, out responseStatusCode);
                
                if (responseStatusCode == HttpStatusCode.NotFound.ToString()) return null;
                else return "Product saved successfully";
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }



        public List<ProductConfigDto> getMTDRMappingInfo()
        {
            List<ProductConfigDto> Data = new List<ProductConfigDto>();
            var jsonString = JsonConvert.SerializeObject(AccountType.MTDR);

            string jsonObj = JsonConvert.SerializeObject(null);
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/agentproduct/productconfigs";
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.UploadString(path, "POST", jsonString);
                string serviceResponse = UtilityCom.getServerResponse(client);
                
                if (!serviceResponse.Equals("OK")) return null;
                else
                {
                    Data = JsonConvert.DeserializeObject<List<ProductConfigDto>>(responseString);
                    return Data;
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }
        public string deletMTDRMap(ProductConfigDto mtdrMap)
        {
            var jsonString = JsonConvert.SerializeObject(mtdrMap.termProductType);
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/agentproduct/mtdrproduct";
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.UploadString(path, "POST", jsonString);
                string responseStatusCode;
                string responseStatusDescription;
                JsonCom.GetStatusCode(client, out responseStatusDescription, out responseStatusCode);
                
                if (responseStatusCode == HttpStatusCode.NotFound.ToString()) return null;
                else return "Product deleted successfully";
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }
        public string saveMTDRMap(List<ProductConfigDto> mtdrMapList)
        {
            var jsonString = JsonConvert.SerializeObject(mtdrMapList);
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/agentproduct/productconfig";
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.UploadString(path, "POST", jsonString);
                string responseStatusCode;
                string responseStatusDescription;
                JsonCom.GetStatusCode(client, out responseStatusDescription, out responseStatusCode);
                if (responseStatusCode == HttpStatusCode.NotFound.ToString()) return null;
                else return "Product saved successfully";
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }



        public static List<SspInstallment> GetSSPInstallmentListByProduct(CbsProduct product)
        {
            List<SspInstallment> Data = new List<SspInstallment>();
            var jsonString = JsonConvert.SerializeObject(product.productId);

            string jsonObj = JsonConvert.SerializeObject(null);
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/agentproduct/cbssspinstallmentsbyid/" + product.productId;
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.DownloadString(path);
                string responseStatusCode;
                string responseStatusDescription;
                JsonCom.GetStatusCode(client, out responseStatusDescription, out responseStatusCode);

                Data = JsonConvert.DeserializeObject<List<SspInstallment>>(responseString);
                return Data;

                //if (!serviceResponse.Equals("OK"))
                //{
                //    return null;
                //}
                //else
                //{
                //    Data = JsonConvert.DeserializeObject<List<CbsProduct>>(responseString);
                //    return Data;
                //}
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }

        public List<ProductConfigDto> getSSPMappingInfo()
        {
            List<ProductConfigDto> Data = new List<ProductConfigDto>();
            var jsonString = JsonConvert.SerializeObject(AccountType.SSP);

            string jsonObj = JsonConvert.SerializeObject(null);
            WebClient client = new WebClient();
            try
            {
                //string path = SessionInfo.rootServiceUrl + "resources/agentproduct/agentproductsbytype"; // + AccountType.SSP.ToString();
                string path = SessionInfo.rootServiceUrl + "resources/agentproduct/productconfigs";
                client = UtilityCom.setClientHeaders(client);
                //string responseString = client.DownloadString(path);
                string responseString = client.UploadString(path, "POST", jsonString);
                string serviceResponse = UtilityCom.getServerResponse(client);
                if (!serviceResponse.Equals("OK"))
                {
                    return null;
                }
                else
                {
                    Data = JsonConvert.DeserializeObject<List<ProductConfigDto>>(responseString);
                    return Data;
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }

        public List<TermProductType> GetSSPProducts()
        {
            List<TermProductType> Data = new List<TermProductType>();
            var jsonString = JsonConvert.SerializeObject(AccountType.SSP);

            string jsonObj = JsonConvert.SerializeObject(null);
            WebClient client = new WebClient();
            try
            {
                //string path = SessionInfo.rootServiceUrl + "resources/agentproduct/agentproductsbytype"; // + AccountType.SSP.ToString();
                string path = SessionInfo.rootServiceUrl + "resources/agentproduct/productconfigs/term";
                client = UtilityCom.setClientHeaders(client);
                //string responseString = client.DownloadString(path);
                string responseString = client.UploadString(path, "POST", jsonString);
                string serviceResponse = UtilityCom.getServerResponse(client);
                if (!serviceResponse.Equals("OK"))
                {
                    return null;
                }
                else
                {
                    Data = JsonConvert.DeserializeObject<List<TermProductType>>(responseString);
                    return Data;
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }

        public List<TermProductType> GetMTDRProducts()
        {
            List<TermProductType> Data = new List<TermProductType>();
            var jsonString = JsonConvert.SerializeObject(AccountType.MTDR);

            string jsonObj = JsonConvert.SerializeObject(null);
            WebClient client = new WebClient();
            try
            {
                //string path = SessionInfo.rootServiceUrl + "resources/agentproduct/agentproductsbytype"; // + AccountType.SSP.ToString();
                string path = SessionInfo.rootServiceUrl + "resources/agentproduct/productconfigs/term";
                client = UtilityCom.setClientHeaders(client);
                //string responseString = client.DownloadString(path);
                string responseString = client.UploadString(path, "POST", jsonString);
                string serviceResponse = UtilityCom.getServerResponse(client);
                if (!serviceResponse.Equals("OK"))
                {
                    return null;
                }
                else
                {
                    Data = JsonConvert.DeserializeObject<List<TermProductType>>(responseString);
                    return Data;
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }

        public string deleteSSPMap(ProductConfigDto sspMap)
        {
            var jsonString = JsonConvert.SerializeObject(sspMap.termProductType);
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/agentproduct/sspproduct";
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.UploadString(path, "POST", jsonString);
                string responseStatusCode;
                string responseStatusDescription;
                JsonCom.GetStatusCode(client, out responseStatusDescription, out responseStatusCode);
                if (responseStatusCode == HttpStatusCode.NotFound.ToString())
                    return null;
                else
                {
                    return "Product deleted successfully";
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }
        public string saveSSPMap(List<ProductConfigDto> SSPMapList)
        {
            var jsonString = JsonConvert.SerializeObject(SSPMapList);
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/agentproduct/productconfig";
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.UploadString(path, "POST", jsonString);
                string responseStatusCode;
                string responseStatusDescription;
                JsonCom.GetStatusCode(client, out responseStatusDescription, out responseStatusCode);
                if (responseStatusCode == HttpStatusCode.NotFound.ToString())
                    return null;
                else
                {
                    return "Product saved successfully";
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }


    }
}
