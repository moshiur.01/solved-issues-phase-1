﻿using System.Collections.Generic;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Services.Communication;

namespace MISL.Ababil.Agent.Communication
{
    public class AccountClosingCom
    {
        public void CloseAccount(object dto)
        {
            new WebClientCommunicator<object, object>().GetPostResult(dto, "");
        }

        public string SubmitAccountClosingRequest(AccountClosingRequestDto accountClosingRequest)
        {
            return new WebClientCommunicator<AccountClosingRequestDto, string>()
                .GetPostResult(accountClosingRequest, "resources/accountinfo/close/request");
        }

        public void CloseDepositAccount(DepositAccountClosing depositAccountClosing)
        {
            new WebClientCommunicator<DepositAccountClosing, object>().GetPostResult(depositAccountClosing, "");
        }

        public void RejectAccountClosingRequest(AccountClosingRequestDto accountClosingRequest)
        {
            new WebClientCommunicator<AccountClosingRequestDto, object>().GetPostResult(accountClosingRequest, "resources/accountinfo/close/rejection");
        }

        public void AcceptAccountClosingRequest(AccountClosingRequestDto accountClosingRequest)
        {
            new WebClientCommunicator<AccountClosingRequestDto, object>().GetPostResult(accountClosingRequest, "resources/accountinfo/close/accept");
        }

        public void CorrectAccountClosingRequest(AccountClosingRequestDto accountClosingRequest)
        {
            new WebClientCommunicator<AccountClosingRequestDto, object>().GetPostResult(accountClosingRequest, "resources/accountinfo/close/correction");
        }

        public List<AccountClosingSearchResultDto> GetAccountClosingRequests(AccountClosingSearchDto accountClosingSearchDto)
        {
            return new WebClientCommunicator<AccountClosingSearchDto, List<AccountClosingSearchResultDto>>().GetPostResult(accountClosingSearchDto, "resources/accountinfo/close/request/search");
        }

        public AccountClosingRequestDto GetAccountClosingRequestDtoByRefNumber(string refNo)
        {
            return new WebClientCommunicator<object, AccountClosingRequestDto>().GetResult("resources/accountinfo/close/request/search/", refNo);
        }

        public void CloseTermAccount(TermAccountClosingDto termAccountClosingDto)
        {
            new WebClientCommunicator<TermAccountClosingDto, object>().GetPostResult(termAccountClosingDto, "");
        }
    }
}