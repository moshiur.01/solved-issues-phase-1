﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Services.Communication;

namespace MISL.Ababil.Agent.Communication
{
    public class SubagentTransferCom
    {
        public void TransferSubagentUser(string username, string outletId)
        {
            new WebClientCommunicator<object, object>().GetResult("resources/userinfo/transfer/outletuser/" + username +
                                                                  "/" + outletId);
        }
    }
}