﻿using System;
using System.Net;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using Newtonsoft.Json;

namespace MISL.Ababil.Agent.Communication
{
    public class CustomerMobileNoUpdateCom
    {
        public string requestCustomerMobileMobileNoUpdate(MobileNoChangeRequestDto mobileNoChangeRequestDto)
        {
            var jsonString = JsonConvert.SerializeObject(mobileNoChangeRequestDto);
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/accountinfo/update/mobile";
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.UploadString(path, "POST", jsonString);
                string responseStatusCode;
                string responseStatusDescription;
                JsonCom.GetStatusCode(client, out responseStatusDescription, out responseStatusCode);
                if (responseStatusCode == HttpStatusCode.NotFound.ToString())
                    return null;
                else
                {
                    if (responseString == "")
                    {
                        return null;
                    }
                    else
                    {
                        //using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(responseString)))
                        //{
                        //    var ser = new DataContractJsonSerializer(typeof(string));
                        //    responseString = ser.ReadObject(ms) as string;
                        //}
                        return responseString;
                    }
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }
    }
}