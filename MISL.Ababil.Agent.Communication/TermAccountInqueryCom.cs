﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using Newtonsoft.Json;

namespace MISL.Ababil.Agent.Communication
{
    public class TermAccountInqueryCom
    {        
        public TermAccInforamationDto GetTermAccountInformationByAccountNo(string accountNumber)
        {
            TermAccInforamationDto termAccInforamationDto = new TermAccInforamationDto();

            WebClient client = new WebClient();

            try
            {
                // to change this line
                string path = SessionInfo.rootServiceUrl + "resources/termaccount/inquiry/" + accountNumber;
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.DownloadString(path);
                string responseStatusCode;
                string responseStatusDescription;
                JsonCom.GetStatusCode(client, out responseStatusDescription, out responseStatusCode);
                if (responseStatusCode == HttpStatusCode.NotFound.ToString())
                {
                    return null;
                }
                else
                {
                    return termAccInforamationDto = JsonConvert.DeserializeObject<TermAccInforamationDto>(responseString);
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }


            //TermAccInforamationDto termAccInforamationDto = new TermAccInforamationDto();
            //WebClient client = new WebClient();
            //try
            //{
            //    string path = SessionInfo.rootServiceUrl + "resources/termaccount/inquiry/" + accountNumber;
            //    client = UtilityCom.setClientHeaders(client);
            //    string responseString = client.DownloadString(path);
            //    string responseStatusCode;
            //    string responseStatusDescription;
            //    JsonCom.GetStatusCode(client, out responseStatusDescription, out responseStatusCode);
            //    if (responseStatusCode == HttpStatusCode.NotFound.ToString())
            //    {
            //        return null;
            //    }
            //    else
            //    {
            //        using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(responseString)))
            //        {
            //            var ser = new DataContractJsonSerializer(termAccInforamationDto.GetType());
            //            return ser.ReadObject(ms) as TermAccInforamationDto;
            //        }

            //    }
            //}
            //catch (WebException webEx)
            //{
            //    throw new Exception(UtilityCom.parseErrorData(webEx));
            //}
        }
    }
}