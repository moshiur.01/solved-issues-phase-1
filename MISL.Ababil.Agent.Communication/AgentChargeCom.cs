﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using Newtonsoft.Json;

namespace MISL.Ababil.Agent.Communication
{
    public class AgentChargeCom
    {
        public List<AgentChargeSearchResultDto> GetAgentChargeSearchResultList(AgentChargeSearchDto agentChargeSearchDto)
        {
            List<AgentChargeSearchResultDto> Data = new List<AgentChargeSearchResultDto>();

            string jsonObj = JsonConvert.SerializeObject(agentChargeSearchDto);
            WebClient client = new WebClient();
            try
            {
                //ToDo: Set URL
                string path = SessionInfo.rootServiceUrl + "resources/report/agent/charge"; //@@@@@@@@@@@//
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.UploadString(path, "POST", jsonObj);
                string serviceResponse = UtilityCom.getServerResponse(client);
                if (!serviceResponse.Equals("OK"))
                {
                    return null;
                }
                else
                {
                    Data = JsonConvert.DeserializeObject<List<AgentChargeSearchResultDto>>(responseString);
                    return Data;
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }
    }
}
