﻿using System;
using System.Collections.Specialized;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using System.Net;
using Newtonsoft.Json;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.reports;
using MISL.Ababil.Agent.Services.Communication;

namespace MISL.Ababil.Agent.Communication
{
    public class AccountInformationCom
    {
        public AccountInformation GetAccountInformation(string accountNumber)
        {
            //string path = SessionInfo.rootServiceUrl + "resources/accountinfo/details/" + accountNumber;
            //try
            //{
            //    NameValueCollection reqparm = new NameValueCollection();
            //    //reqparm.Add("username", username);
            //    //reqparm.Add("password", password);
            //    //reqparm.Add("terminal", terminal);

            //    string responseString = JsonCom.getJson(reqparm, path);
            //    return responseString;
            //}
            //catch (Exception ex)
            //{
            //    //throw new Exception(ex.Message);
            //    return ex.Message;
            //}
            AccountInformation data = new AccountInformation();
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/accountinfo/details/" + accountNumber;
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.DownloadString(path);
                string responseStatusCode;
                string responseStatusDescription;
                JsonCom.GetStatusCode(client, out responseStatusDescription, out responseStatusCode);
                if (responseStatusCode == HttpStatusCode.NotFound.ToString())
                    return null;
                else
                {
                    data = JsonConvert.DeserializeObject<AccountInformation>(responseString);
                    return data;
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }
        public AccountInformationDto GetAccountDto(string accountNumber)
        {
            AccountInformationDto accInfoDto = new AccountInformationDto();
            //string path = SessionInfo.rootServiceUrl + "resources/accountinfo/details/" + accountNumber;
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/accountinfo/details/" + accountNumber;
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.DownloadString(path);
                string responseStatusCode;
                string responseStatusDescription;
                JsonCom.GetStatusCode(client, out responseStatusDescription, out responseStatusCode);
                if (responseStatusCode == HttpStatusCode.NotFound.ToString())
                {
                    return null;
                }
                else
                {
                    return accInfoDto = JsonConvert.DeserializeObject<AccountInformationDto>(responseString);
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }

        public static List<CashRegisterDto> GetCashRegister(DailyTrnRecordSearchDto searcDto)
        {
            List<CashRegisterDto> Data = new List<CashRegisterDto>();

            string jsonObj = JsonConvert.SerializeObject(searcDto);
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/report/cashregister";
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.UploadString(path, "POST", jsonObj);
                string serviceResponse = UtilityCom.getServerResponse(client);
                if (!serviceResponse.Equals("OK"))
                {
                    return null;
                }
                else
                {
                    using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(responseString)))
                    {
                        var ser = new DataContractJsonSerializer(Data.GetType());
                        Data = ser.ReadObject(ms) as List<CashRegisterDto>;
                    }
                    return Data;
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }

        public List<AccountResultSearchDto> GetAccountListByMobileNumber(string mobileNumber)
        {
            List<AccountResultSearchDto> accList = new List<AccountResultSearchDto>();
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/accountinfo/search/" + mobileNumber;
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.DownloadString(path);
                string responseStatusCode;
                string responseStatusDescription;
                JsonCom.GetStatusCode(client, out responseStatusDescription, out responseStatusCode);
                if (responseStatusCode != HttpStatusCode.OK.ToString())
                {
                    return null;
                }
                else
                {
                    return accList = JsonConvert.DeserializeObject<List<AccountResultSearchDto>>(responseString);
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }

        public AccountOperatorDto GetAccountOperatorDtoByAccountNumber(string accountNo)
        {

            AccountOperatorDto accountOperatorDto = new AccountOperatorDto();
            WebClient client = new WebClient();
            try
            {
                string path = SessionInfo.rootServiceUrl + "resources/accountinfo/deposit/operators/" + accountNo;
                client = UtilityCom.setClientHeaders(client);
                string responseString = client.DownloadString(path);
                string responseStatusCode;
                string responseStatusDescription;
                JsonCom.GetStatusCode(client, out responseStatusDescription, out responseStatusCode);
                if (responseStatusCode == HttpStatusCode.NotFound.ToString())
                {
                    return null;
                }
                else
                {
                    using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(responseString)))
                    {
                        DataContractJsonSerializer ser = new DataContractJsonSerializer(accountOperatorDto.GetType());
                        accountOperatorDto = ser.ReadObject(ms) as AccountOperatorDto;
                    }

                    return accountOperatorDto;
                }
            }
            catch (WebException webEx)
            {
                throw new Exception(UtilityCom.parseErrorData(webEx));
            }
        }

        public AccountOperatorDto GetDepositAccountOperatorDto(string accountNumber)
        {
            //ToDo: ______________WORK______________
            return new WebClientCommunicator<string, AccountOperatorDto>().GetResult(
                "resources/accountinfo/deposit/account/operators/", accountNumber);
        }

        public AccountOperatorDto GetAccountOperatorDtoByAccountNumber(string accountNumber, AccountType accountType)
        {
            //ToDo: ______________WORK______________
            switch (accountType)
            {
                case AccountType.Deposit:
                    return new WebClientCommunicator<string, AccountOperatorDto>().GetResult(
                        "resources/accountinfo/deposit/operators/", accountNumber);
                    break;
                case AccountType.SSP:
                    return new WebClientCommunicator<string, AccountOperatorDto>().GetResult(
                        "resources/accountinfo/ssp/operators/", accountNumber);
                    break;
                case AccountType.MTDR:
                    return new WebClientCommunicator<string, AccountOperatorDto>().GetResult(
                        "resources/accountinfo/mtdr/operators/", accountNumber);
                    break;
            }
            return null;
        }

        public AccountOperatorDto GetDepositAccountOperators(string accountNumber)
        {
            return new WebClientCommunicator<string, AccountOperatorDto>().GetResult(
             "resources/accountinfo/deposit/account/operator/", accountNumber);
        }
    }
}