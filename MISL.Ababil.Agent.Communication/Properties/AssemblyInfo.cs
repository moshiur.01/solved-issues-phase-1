using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("MISL.Ababil.Agent.Communication")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Millennium Information Solution Limited")]
[assembly: AssemblyProduct("MISL.Ababil.Agent.Communication")]
[assembly: AssemblyCopyright("Copyright © 2015 Millennium Information Solution Limited")]
[assembly: AssemblyTrademark("Millennium Information Solution Limited")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("bd3dbe5e-3997-469a-b192-d206606b91b3")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.3.0425")]
[assembly: AssemblyFileVersion("1.0.3.0425")]

[assembly: AssemblyInformationalVersion("1.0.0-alpha-02")]