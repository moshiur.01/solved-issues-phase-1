﻿using System;
using System.Windows.Forms;
using MISL.Ababil.Agent.UI.forms;
using System.Diagnostics;
using System.Threading;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.UI.forms.ProgressUI;

namespace ApplicationLauncher
{
    static class Program
    {
        private const int MaxTryCount = 10;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            frmLogin frm = new frmLogin();

            if (PriorProcess() != null)
            {
                Form frmTemp = new Form
                {
                    FormBorderStyle = FormBorderStyle.None,
                    Text = "",
                    WindowState = FormWindowState.Minimized,
                    Size = new System.Drawing.Size(0, 0),
                    ShowInTaskbar = false
                };
                frmTemp.Show();
                ProgressUIManager.ShowProgress(frmTemp);

                Process process;
                for (int tryCount = 0; tryCount <= MaxTryCount; tryCount++)
                {
                    process = PriorProcess();
                    if (process == null) { break; }
                    Thread.Sleep(1000);
                    if (process != null && tryCount == MaxTryCount)
                    {
                        MISL.Ababil.Agent.UI.Message.showError("Another instance of the application is already running.\n\nPlease close existing instance and run the application again.");
                        return;
                    }
                }

                ProgressUIManager.CloseProgress();
                frmTemp.Close();
            }

            if (new UpdateService().IsUpdateaAvailable())
            {
                Application.DoEvents();                
                Process.Start(new ProcessStartInfo()
                {
                    FileName = Application.StartupPath + "\\setup.exe",
                });
                Application.DoEvents();
                Application.Exit();
            }
            else
            {
                Application.Run(frm);
            }

            // // // // bool exitRequired;
            // // // // frmUpdater frmUpdater = new frmUpdater();
            // // // // 
            // // // // bool updating = frmUpdater.InitiateUpdate(out exitRequired);
            // // // // 
            // // // // if (exitRequired)
            // // // // {
            // // // //     Application.Exit();
            // // // //     return;
            // // // // }
            // // // // 
            // // // // if (updating)
            // // // // {
            // // // //     while (!frmUpdater.OkToExit) Application.DoEvents();
            // // // //     Application.Exit();
            // // // //     return;
            // // // // }

        }

        public static Process PriorProcess()
        {
            Process curr = Process.GetCurrentProcess();

            if (curr.ProcessName.IndexOf(".vshost") > 0)
            {
                return null;
            }
            Process[] procs = Process.GetProcessesByName(curr.ProcessName);
            for (int i = 0; i < procs.Length; i++)
            {
                Process p = procs[i];
                if ((p.Id != curr.Id) && (p.MainModule.ModuleName == curr.MainModule.ModuleName))
                {
                    return p;
                }
            }
            return null;
        }
    }
}