﻿using MISL.Ababil.Agent.Infrastructure.Models.domain.models.consumer;
using System.Collections.Generic;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;

namespace MISL.Ababil.Agent.Infrastructure.Models.models.transaction
{
    public class CashOutRequest : TransactionRequest
    {

        public string customerAccount { get; set; }
        public string agentFingerData { get; set; }
        public AccountOperatorDto accountOperatorDto { get; set; }
    }
}