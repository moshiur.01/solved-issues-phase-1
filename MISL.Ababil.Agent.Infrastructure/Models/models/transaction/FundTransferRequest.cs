﻿using MISL.Ababil.Agent.Infrastructure.Models.domain.models.consumer;
using System.Collections.Generic;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;

namespace MISL.Ababil.Agent.Infrastructure.Models.models.transaction
{

    public class FundTransferRequest : TransactionRequest
    {
        public string fromAccount { get; set; }
        public string toAccount { get; set; }
        public string agentFingerData { get; set; }
        public AccountOperatorDto accountOperatorDto { get; set; }
    }
}