﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.reports
{
   public class CumulativeReportResultDto
    {
        public string outletName { get; set; }
        public long cumulativeDraftAcc { get; set; }
        public long cumulativeSubmitAcc { get; set; }
        public long cumulativeCorrectionAcc { get; set; }
        public long todayCorrectionAcc { get; set; }
        public long cumulativeApproveAcc { get; set; }
        public long todayApproveAcc { get; set; }
        public long cumulativeVerifyAcc { get; set; }
        public string branchName { get; set; }
    }
}
