﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.reports
{
    public class ApplicationMonitoringSumDto
    {
        public long? agentId { get; set; }
        public string agentName { get; set; }
        public long? outletId { get; set; }
        public string outletName { get; set; }
        public string outletContactNumber { get; set; }
        public long? draftCount { get; set; }
        public long? correctionCount { get; set; }
        public string outletCode { get; set; }

    }
}
