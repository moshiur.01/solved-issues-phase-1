﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.reports
{
    public class BillCompanyDepositSlip
    {
        public long? outletId { get; set; }
        public long? reportdate { get; set; }
        public String outletName { get; set; }
        public long? billZoneId { get; set; }
        public String zoneAccountNo { get; set; }
        public String zoneAccountName { get; set; }
        public String zoneVatAccountNo { get; set; }
        public String zoneVatAccountName { get; set; }
        public String zoneMobileNo { get; set; }
        public long noOfBillCollect { get; set; }
        public decimal? totalBillAmount { get; set; }
        public decimal? totalVatAmount { get; set; }

    }
}
