﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;

namespace MISL.Ababil.Agent.Infrastructure.Models.reports
{
    public class ApplicationMonitoringSearchDto
    {
        public long? agentId { get; set; }
        public long? outletId { get; set; }
        public long? fromDate { get; set; }
        public long? toDate { get; set; }
        public ApplicationStatus? status { get; set; }
    }
}
