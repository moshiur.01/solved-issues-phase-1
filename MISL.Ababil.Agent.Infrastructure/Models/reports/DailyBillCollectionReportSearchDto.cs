﻿namespace MISL.Ababil.Agent.Infrastructure.Models.reports
{
    public class DailyBillCollectionReportSearchDto
    {
        public long? agentId { get; set; }//
        public long? subagentId { get; set; }//
        public long? zoneId { get; set; }//
        public long? subZoneId { get; set; }//
        public long? billCompanyId { get; set; }
        public long? fromDate { get; set; }
        public long? toDate { get; set; }
        public long? voucherNumber { get; set; }//
    }
}