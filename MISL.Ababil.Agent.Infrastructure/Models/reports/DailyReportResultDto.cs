﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.reports
{
    public class DailyReportResultDto
    {
        public long agentId { get; set; }
        public long outletId { get; set; }
        public string agentName { get; set; }
        public string outletName { get; set; }
        public long accOpenedAsOn { get; set; }
        public long submittedToBranchAsOn { get; set; }
        public long ApprovedOn { get; set; }
        public long correctionFromHeadOfficeOn { get; set; }
        public long correctionFromBranchOn { get; set; }
        public long pendingForApprovalOn { get; set; }
        public long draftAsOn { get; set; }
    }
}
