﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.reports
{
    public class CustomerApplicationInfoDto
    {
        public long agentId { get; set; }
        public string agentName { get; set; }
        public long outletId { get; set; }
        public string outlateName { get; set; }
        public string outletCode { get; set; }
        public string consumerName { get; set; }
        public string nationalId { get; set; }
        public string mobileNo { get; set; }
        public string referenceNo { get; set; }
        public decimal openingAmount { get; set; }
        public long? applyDate { get; set; }
        public string applyUser { get; set; }
        public string rejectedUser { get; set; }
        public string documentType { get; set; }
        public string documentNo { get; set; }
        public long? rejectedDate { get; set; }
        public string comments { get; set; }


    }
}
