﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.reports
{
   public class OnlineReportResultDto
    {
        public long? agentId { get; set; }
        public long? outletId { get; set; }
        public string agentName { get; set; }
        public string outletName { get; set; }
        public string trDate { get; set; }
        public string accNo { get; set; }
        public string accName { get; set; }
        public string branchName { get; set; }
        public decimal debitAmount { get; set; }
        public decimal creditAmount { get; set; }
        public string particulars { get; set; }
    }
}
