﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;

namespace MISL.Ababil.Agent.Infrastructure.Models.reports
{
    public class BillCustomerSlipDto
    {
        public string billCompanyName { get; set; }
        public string outletName { get; set; }
        public Address outletAddress { get; set; }
        public string zoneName { get; set; }
        public string subZoneName { get; set; }
        public string billMonth { get; set; }
        public string custBillAcc { get; set; }
        public string custName { get; set; }
        public string custMobileNo { get; set; }
        public string txnTime { get; set; }
        public string debitAcc { get; set; }
        public decimal? billAmount { get; set; }
        public decimal? serviceChargeAmount { get; set; }
        public decimal? stampChargeAmount { get; set; }
        public decimal? lateFeeAmount { get; set; }
        public decimal? vatAmount { get; set; }

        public string billNo { get; set; }
    }
}
