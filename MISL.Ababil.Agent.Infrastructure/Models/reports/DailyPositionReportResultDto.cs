﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.reports
{
    public class DailyPositionReportResultDto
    {
        public string agentId { get; set; }
        public string agentName { get; set; }
        public string outletid { get; set; }
        public string outletName { get; set; }
        public string agentBalance { get; set; }
        public string openingDate { get; set; }
        public string previousDayAccOpen { get; set; }
        public string previousDayAmount { get; set; }
        public string todayDayAccOpen { get; set; }
        public string todayDayAmount { get; set; }
        public string noOfDraft { get; set; }





    }
}
