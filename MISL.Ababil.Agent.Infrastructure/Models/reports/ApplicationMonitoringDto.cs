﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.reports
{
    public class ApplicationMonitoringDto
    {        
        public string refNo { get; set; }
        public string accountName { get; set; }
        public string accountNo { get; set; }
        public string mobileNo { get; set; }
        public string userStatus { get; set; }
        public string applyUser { get; set; }
        public string applyDate { get; set; }
        public string submitUser { get; set; }
        public string submitDate { get; set; }
        public string verifyOfficer { get; set; }
        public string verifyDate { get; set; }
        public string approveOfficer { get; set; }
        public string approveDate { get; set; }
    }
}
