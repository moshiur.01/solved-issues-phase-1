﻿using MISL.Ababil.Agent.Infrastructure.Models.domain.models.termaccount;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.reports
{
    public class ItdAccReportSearchDto
    {
        public long? agentId { get; set; }
        public long? outletId { get; set; }
        public long? productId { get; set; }

    }
}