﻿namespace MISL.Ababil.Agent.Infrastructure.Models.reports
{
    public class DailyBillCollectionReportResultDto
    {
        public long? agentId { get; set; }
        public string agentName { get; set; }
        public long? outletId { get; set; }
        public string outletName { get; set; }
        public long? zoneId { get; set; }
        public string zoneName { get; set; }
        public long? subZoneId { get; set; }
        public string subZoneName { get; set; }
        public string outletUser { get; set; }
        public string transactionDateTime { get; set; }
        public string customerBillAccNo { get; set; }
        public string customerName { get; set; }
        public string customerMobileNo { get; set; }

        public string collectionYearMonth { get; set; }
        public decimal? billAmount { get; set; }
        public decimal? serviceChargeAmount { get; set; }
        public decimal? vatAmount { get; set; }
        public decimal? stampChargeAmount { get; set; }

        public decimal? lateFee { get; set; }

        public long? voucherNumber { get; set; }

        public string billNo { get; set; }
    }
}