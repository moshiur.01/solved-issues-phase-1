﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.reports
{
    public class UserIdWiseAccApproveSearchDto
    {
        public long? branchId { get; set; }
        public long? fromDate { get; set; }
        public long? toDate { get; set; }
    }
}
