﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.reports
{
    public class DepositApplicantSearchDto
    {
        public long? agentId { get; set; }
        public long? outletId { get; set; }
        public long? fromDate { get; set; }
        public long? toDate { get; set; }
    }
}
