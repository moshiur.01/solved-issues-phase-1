﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.reports
{
    public class DepositApplicantResultDto
    {
        public long agentId { get; set; }

        public long outletId { get; set; }
        public  string agentName { get; set; }
        public string outletName { get; set; }
        public string applicantName { get; set; }
        public string fatherName { get; set; }
        public string motherName { get; set; }
        public string spouseName { get; set; }
        public string refNo { get; set; }
        public long dateOfBirth { get; set; }
        public string nidNo { get; set; }
        

    }
}
