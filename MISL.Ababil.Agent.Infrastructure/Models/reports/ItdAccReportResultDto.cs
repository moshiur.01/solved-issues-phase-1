﻿using MISL.Ababil.Agent.Infrastructure.Models.domain.models.termaccount;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.reports
{
    public class ItdAccReportResultDto
    {
        public long agentId { set; get; }
        public long outletId { set; get; }
        public string agentName { set; get; }
        public string outletName { set; get; }
        public string accountNo { set; get; }
        public string accountTitle { set; get; }
        public string dueDate { set; get; }
        public long? noOfDueInstallment { set; get; }
        public string mobileNo { set; get; }
        public decimal balance { set; get; }
        public long productId { set; get; }
        public string productName { set; get; }

        public string openDate { set; get; }

        public decimal installmentAmount { set; get; }

        //termaccount/itd/info/installment


    }
}
