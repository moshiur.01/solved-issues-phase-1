﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.reports
{
    public class UserIdWiseAccApproveResultDto

    {
        public string branchName { get; set; }
        public string userId { get; set; }
        public long noOfMSD { get; set; }
        public long noOfCD { get; set; }
        public long noOfSSP { get; set; }
        public long noOfMTDR { get; set; }
        
    }
}
