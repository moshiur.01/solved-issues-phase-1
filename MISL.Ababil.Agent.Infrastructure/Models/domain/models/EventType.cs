﻿namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models
{
    public enum EventType
    {
        ServiceEvent, PeriodicEvent
    }
}
