﻿namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models
{
    public enum Month
    {
        Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec
    }
}