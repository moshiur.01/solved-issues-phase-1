﻿namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models
{
    public enum AuthenticationWay
    {
        Biometric, Token
    }
}