﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms.VisualStyles;

namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.Remittance
{
    public class ExchangeHouse
    {

        //public long id { get; set; }
        //public string companyName { get; set; }
        //public string shortName { get; set; }
        //public Address address { get; set; }
        //public string mobile { get; set; }
        //public string telephone { get; set; }
        //public string fax { get; set; }
        //public string email { get; set; }
        //public string website { get; set; }

        public string companyName { get; set; }
        public long? id { get; set; }
        public string shortName { get; set; }
        public Address address { get; set; }
        public string mobile { get; set; }
        public string telephone { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
        public string website { get; set; }
        public long? remitanceAccountSetup { get; set; }
        public AccountNature? accountNature { get; set; }
        public string accountNumber { get; set; }
        public Status? status { get; set; }
        public byte[] logo { get; set; }
    }
}