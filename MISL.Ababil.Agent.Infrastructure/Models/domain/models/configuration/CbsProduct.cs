﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.fingerprint;

namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.configuration
{
    public class CbsProduct
    {
        public string productPrefix { get; set; }
        public string productTitle { get; set; }
        public long productId { get; set; }
    }

}
