﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.fingerprint;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.termaccount;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.ssp;

namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.configuration
{
    public class ProductConfigDto
    {
        public AgentProduct agentProduct { get; set; }
        public TermProductType termProductType { get; set; }
        public List<SspInstallment> installments { get; set; }
        public AccountType accountType { get; set; }
    }

}
