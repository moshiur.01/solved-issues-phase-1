﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models
{
    public class Branch
    {
        public long id { get; set; }
        public string branchCode { get; set; }
        public string name { get; set; }
        private string routingNumber { get; set; }
    }
}
