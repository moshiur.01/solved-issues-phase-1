﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.fingerprint
{
    public class FingerInfo
    {
        private static  long serialVersionUID = 1L;
        public long? id { get; set; }
        public long personId { get; set; }
        public long fingerIndex { get; set; }
        public string fingerData { get; set; }
    }
}
