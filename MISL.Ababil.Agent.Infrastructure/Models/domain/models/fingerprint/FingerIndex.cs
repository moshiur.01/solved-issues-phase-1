﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.fingerprint
{
    public enum FingerIndex
    {
        LeftPinky = 1,
        LeftRing = 2,
        LeftMiddle = 3,
        LeftIndex = 4,
        LeftThumb = 5,

        RightThumb = 6,
        RightIndex = 7,
        RightMiddle = 8,
        RightRing = 9,
        RightPinky = 10,
    }
}
