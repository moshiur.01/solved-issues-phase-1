﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.fingerprint
{
    public class PersonFingerInfo
    {
        public long? id { get; set; }
        public string personName { get; set; }
        public DateTime? entryDate { get; set; }
        public List<FingerInfo> fingerInfos { get; set; }
    }
}
