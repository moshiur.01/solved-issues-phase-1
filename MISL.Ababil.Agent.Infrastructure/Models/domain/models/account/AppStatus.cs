﻿namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.account
{
    public enum AppStatus
    {
        Apply, Accept, Approve, Correction, Rejection
    }
}