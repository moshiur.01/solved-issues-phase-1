﻿using System.Collections.Generic;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.consumer;

namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.account
{
    public class AccountClosingRequest
    {
        public AccountType accountType { get; set; }
        public string accountNumber { get; set; }

        //Account Info
        public string name { get; set; }
        public AccountStatus status { get; set; }
        public decimal balance { get; set; }

        public AuthenticationWay authenticationWay { get; set; }
        public List<AccountOperator> accountOperators { get; set; }
    }
}