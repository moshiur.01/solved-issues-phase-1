﻿using System.Collections.Generic;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.consumer;
using MISL.Ababil.Agent.Infrastructure.Models.dto;

namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.account
{
    public class AccountClosingRequestDto
    {
        public AccountClosingRequestInfo accountClosingRequestInfo { get; set; }
        //public List<AccountOperator> accountOperators { get; set; }        
        public AccountOperatorDto accountOperatorDto { get; set; }

        //public string token { get; set; }
        public string userFingerData { get; set; }
        public CommentDto comment { get; set; }
    }
}