﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.account
{
    public class FinanceProduct
    {
        public long id { get; set; }
        public long cbsId { get; set; }
        public string productName { get; set; }
    }
}
