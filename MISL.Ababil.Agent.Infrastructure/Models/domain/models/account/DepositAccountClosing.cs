﻿namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.account
{
    public class DepositAccountClosing
    {
        //Customer Info
        public string accountNumber { get; set; }
        public string customerName { get; set; }
        public long? openingDate { get; set; }

        public decimal lienAmount { get; set; }
        public decimal blockAmount { get; set; }


        //Transaction Info
        public decimal balance { get; set; }
        public decimal provisionedProfit { get; set; }
        public decimal uncalculatedProfit { get; set; }
        public decimal taxOnProfit { get; set; }
        public decimal closingCharge { get; set; }
        public decimal vat { get; set; }
        public decimal exciseDuty { get; set; }
    }
}