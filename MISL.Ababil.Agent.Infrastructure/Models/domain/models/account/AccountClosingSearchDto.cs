﻿namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.account
{
    public class AccountClosingSearchDto
    {
        public long? agentId { get; set; }
        public long? outletId { get; set; }
        public long? fromDate { get; set; }
        public long? toDate { get; set; }
        public string refNo { get; set; }
        public string mobileNumber { get; set; }
        public AppStatus? appStatus { get; set; }
    }
}