﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.account
{
    public class CustomerInformationDto
    {
        public long custId { get; set; }
        public string customerName { get; set; }
        public string contactNo { get; set; }

    }
}
