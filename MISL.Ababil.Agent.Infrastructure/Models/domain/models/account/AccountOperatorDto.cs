﻿using System.Collections.Generic;

namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.account
{
    public class AccountOperatorDto
    {
        public string accountNo { get; set; }
        public AccountType? accountType { get; set; }
        public long? noOfRequireOperator { get; set; }
        public List<AccountOperatorDetailsDto> operators { get; set; }
    }
}