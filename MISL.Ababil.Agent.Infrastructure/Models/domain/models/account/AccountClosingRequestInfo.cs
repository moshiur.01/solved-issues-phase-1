﻿namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.account
{
    public class AccountClosingRequestInfo
    {
        public long id { get; set; }
        public long requestOutletId { get; set; }
        public AccountType accountType { get; set; }
        public string accountNumber { get; set; }
        public string applicantName { get; set; }
        public AuthenticationWay authenticationWay { get; set; }
        public string refNo { get; set; }
        public AppStatus appStatus { get; set; }
        public string lastActivityUser { get; set; }
        public long? lastActivityDate { get; set; }
        public long? lastActivityTime { get; set; }
        public long? commentId { get; set; }
        public long? docId { get; set; }        
    }
}