﻿namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.account
{
    public class AccountOperatorDetailsDto
    {
        public long? individualId { get; set; }
        public string individualName { get; set; }
        public string fingerData { get; set; }
        public string token { get; set; }
        public bool mandatory { get; set; }
        public string mobileNo { get; set; }
        //public byte[] photo { get; set; }
    }
}