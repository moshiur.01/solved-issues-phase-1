﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.account
{
    public class OwnerInfoDto
    {
        public long individualId { get; set; }
        public String individualName { get; set; }
        public String individualMobile { get; set; }
        public bool fingerCapture { get; set; }
        public String fingerData { get; set; }
        public String token { get; set; }
        private bool accountOperator { get; set; }
    }
}
