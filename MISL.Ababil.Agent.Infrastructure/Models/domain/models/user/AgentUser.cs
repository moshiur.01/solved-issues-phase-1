﻿using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.fingerprint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.common;

namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.user
{
    public class AgentUser
    {
        private static long serialVersionUID = 1L;
        public long id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public AgentInformation agentInformation { get; set; }
        public List<FingerInfo> fingerDatas { get; set; }

        public UserStatus? userStatus {get; set;}
        public UserType? userType {get; set;}
        public long? individualId {get; set; }
        public FingerStatus? fingerStatus { get; set; }
        public string statusChangeRemark { get; set; }

        #region Not Used
        //private long id;

        //private String username;


        //private AgentInformation agentInformation;

        //public AgentUser(long id, String username)
        //{
        //    this.id = id;
        //    this.username = username;
        //}

        //public AgentUser()
        //{


        //}



        //public String getUsername()
        //{
        //    return username;
        //}

        //public void setUsername(String username)
        //{
        //    this.username = username;
        //}

        //public long getId()
        //{
        //    return id;
        //}

        //public void setId(long id)
        //{
        //    this.id = id;
        //}

        //public AgentInformation getAgentInformation()
        //{
        //    return agentInformation;
        //}

        //public void setAgentInformation(AgentInformation agentInformation)
        //{
        //    this.agentInformation = agentInformation;
        //}
        #endregion
    }
}
