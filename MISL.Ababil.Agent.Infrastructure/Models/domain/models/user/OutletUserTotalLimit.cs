﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.user
{
    public class OutletUserTotalLimit
    {
        public decimal? totalCashLimit { get; set; }
        public decimal? totalCashUsage { get; set; }
    }
}
