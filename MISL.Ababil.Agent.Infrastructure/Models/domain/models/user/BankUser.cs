﻿using MISL.Ababil.Agent.Infrastructure.Models.common;

namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.user
{
    public class BankUser
    {
        public long id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public UserStatus userStatus { get; set; }
        public BankUserType bankUserType { get; set; }
        public UserType userType { get; set; }
        public long? individualId { get; set; }
        public string statusChangeRemark { get; set; }
        public long? userBranch { get; set; }
    }
}