﻿using MISL.Ababil.Agent.Infrastructure.Models.domain.models.documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.fingerprint;

namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.cis
{
    public class IndividualInformation
    {
        private static long serialVersionUID = 1L;
        public long id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string fatherFirstName { get; set; }
        public string fatherLastName { get; set; }
        public string motherFirstName { get; set; }
        public string motherLastName { get; set; }
        //public string dateOfBirthSt { get; set; }
        public Occupation occupation { get; set; } //=
        public Gender? gender { get; set; }
        public MaritalStatus? maritalStatus { get; set; }
        public string spouseName { get; set; }
        public Address presentAddress { get; set; }
        public Address permanentAddress { get; set; }
        public string email { get; set; }
        public string mobileNo { get; set; }
        public string fax { get; set; }
        public District birthPalce { get; set; } //=
        public Country birthCountry { get; set; } //=
        public long? documentInfoId { get; set; }
        public long? cbsIndividualId { get; set; }

        public long? imageId { get; set; }
        public long? personFingerInfoId { get; set; }
        public string imageMineType { get; set; }

        public long? dateOfBirth { get; set; }

        public string fingerRefId { get; set; }
        public List<FingerInfo> fingerInfos { get; set; }
        public byte[] image { get; set; }
        public FingerStatus? fingerStatus { get; set; }
    }
}
