﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.cis;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.fingerprint;

namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.cis
{
    public class OwnerInformation
    {
        private static long serialVersionUID = 1L;
        public long id { get; set; }
        public CisOwnerType ownerType { get; set; }
        public IndividualInformation individualInformation { get; set; }

        public long customerId { get; set; }
        public  long individualId { get; set; }
        public bool mandatoryOperator { get; set; }
        public bool accountOperator { get; set; }
        //private long id;
        //private CisOwnerType ownerType;
        //private IndividualInformation individualInformation;
        //private AgentInformation agent;
        //public CisOwnerType getOwnerType()
        //{
        //    return ownerType;
        //}
        //public void setOwnerType(CisOwnerType ownerType)
        //{
        //    this.ownerType = ownerType;
        //}
        //public IndividualInformation getIndividualInformation()
        //{
        //    return individualInformation;
        //}
        //public void setIndividualInformation(IndividualInformation individualInformation)
        //{
        //    this.individualInformation = individualInformation;
        //}
    }
}
