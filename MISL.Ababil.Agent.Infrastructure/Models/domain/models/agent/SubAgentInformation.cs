﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using Newtonsoft.Json;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.consumer;
using MISL.Ababil.Agent.Infrastructure.Models.dto;

namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent
{
    public class SubAgentInformation
    {
        private static long serialVersionUID = 1L;

        public long id { get; set; }
        public string name { get; set; }
        public string subAgentCode { get; set; }
        public Address businessAddress { get; set; }
        public string mobleNumber { get; set; }
        public string phoneNumber { get; set; }

        public AccountInformation agentAccount { get; set; }

        public List<SubAgentUser> users { get; set; }
        public AgentInformation agent { get; set; }
        public string email { get; set; }

        //[JsonConverter(typeof(ByteArrayConverter))]
        //public string img { get; set; }
        //public byte[] img { get; set; }
        public long? monitoringBranch { get; set; }
        //public string monitoringFieldOfficer { get; set; }      // Commented :: WALI :: 18-Jan-2016

        public decimal? dailyCreditLimit { get; set; }
        public decimal? dailyDebitLimit { get; set; }
        public decimal? usedDailyCreditLimit { get; set; }
        public decimal? usedDailyDebitLimit { get; set; }

        public bool? creditLimitApplicable { get; set; }
        public bool? debitLimitApplicable { get; set; }
        //public string status { get; set; }   // WALI :: 19-Jan-2016

        public bool? cashLimitApplicable { get; set; }
        public decimal? cashBalance { get; set; }
        public decimal? cashLimit { get; set; }

        public List<BankUser> monitoringFieldOfficers { get; set; }
        public long? creationDate { get; set; }

        public OutLetStatus? outletStatus { get; set; }
    }
}