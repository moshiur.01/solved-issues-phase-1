﻿using System;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;

namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent
{
    public class AgentTransactionRecord
    {
        public long id { get; set; }
        public decimal? txnAmount { get; set; }
        public decimal chargeAmount { get; set; }
        public string creditAccount { get; set; }
        public string debitAccount { get; set; }
        public AccountType? creditAccountType { get; set; }
        public AccountType? debitAccountType { get; set; }
        public string txnUser { get; set; }
        public long txnOutlet { get; set; }
        public DateTime? txnDate { get; set; }
        public DateTime? txnTime { get; set; }
        public string txnRefno { get; set; }
        public string userNarration { get; set; }
        public string narration { get; set; }
        public EventType? eventType { get; set; }
        public long eventId { get; set; }
    }
}
