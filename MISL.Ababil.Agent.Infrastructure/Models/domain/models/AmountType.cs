﻿namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models
{
    public enum AmountType
    {
        Percent, Fixed
    }
}