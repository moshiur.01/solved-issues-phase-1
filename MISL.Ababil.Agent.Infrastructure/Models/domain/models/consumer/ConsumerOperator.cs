﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.consumer
{
   public class AccountOperator
    {
        private static long serialVersionUID = 1L;
        public string identity { get; set; }

        public String identityName { get; set; }

        public String fingerData { get; set; }
    }
}