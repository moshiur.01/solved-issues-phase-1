﻿using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.ssp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models.termaccount
{
    public class TermAccountInformation
    {
        public long? id { get; set; }
        public string depositAccNum { get; set; }
        public string depositAcctitle { get; set; }
        public AccountType? accountType { get; set; }
        public long? termProductType { get; set; }
        public string accountNumber { get; set; }
        public string accTitle { get; set; }
        public decimal amount { get; set; }
        public string subAgentUser { get; set; }
        public TermAccountStatus? accountStatus { get; set; }
        public string referanceNumber { get; set; }
        public long? kycProfileNo { get; set; }
        public string requestDate { get; set; }
        public string approveDate { get; set; }
        public string approveUser { get; set; }
        public string activationDate { get; set; }
        public string expiryDate { get; set; }
        public string voucherNumber { get; set; }
        public long? commentId { get; set; }
    }
}