﻿namespace MISL.Ababil.Agent.Infrastructure.Models.domain.models
{
    public enum Status
    {
        Active, Inactive
    }
}