﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class UserDto
    {
        public string userName { get; set; }
        public string userFullName { get; set; }
        public string fathersName { get; set; }
        public string mobileNo { get; set; }
    }
}