﻿namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class TransactionalEventDto
    {
        public long eventId { get; set; }
        public string eventName { get; set; }
    }
}
