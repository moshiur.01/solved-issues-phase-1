﻿using System;
using MISL.Ababil.Agent.Infrastructure.Models.common;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class AgentIncomeSearchResultDto
    {
        public long? incomeDate { get; set; }
        public string incomeFrom { get; set; }
        public decimal? amount { get; set; }
        public long? outletId { get; set; }
        public string outletName { get; set; }
        public string userName { get; set; }
        public PostedStatus postedStatus { get; set; }
        public long? postingDate { get; set; }
    }
}