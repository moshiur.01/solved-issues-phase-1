﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class AuthConfigDto
    {
        public bool reqPassChange { get; set; }
        public bool reqFingerChange { get; set; }
        public string authType { get; set; }
    }
}
