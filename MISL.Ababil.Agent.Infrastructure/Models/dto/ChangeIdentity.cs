﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class ChangeIdentity
    {
        public passwordChangeInfo passwordChangeInfo { get; set; }
        public FingerChangeInfo fingerChangeInfo { get; set; }
    }
}
