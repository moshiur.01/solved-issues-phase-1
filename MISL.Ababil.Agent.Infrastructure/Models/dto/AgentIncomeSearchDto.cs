﻿using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class AgentIncomeSearchDto
    {
        public long? agentId { get; set; }
        public long? outletId { get; set; }
        public long? incomeDateFrom { get; set; }
        public long? incomeDateTo { get; set; }
        public PostedStatus? postedStatus { get; set; }

        //public long? postDateFrom { get; set; }
        //public long? postDateTo { get; set; }
        //public string userName { get; set; }
        //public AgentServicesType incomeFrom { get; set; }
        //public long? commissionTypeId { get; set; }
    }
}