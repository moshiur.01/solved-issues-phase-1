﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
   public class WorkFlowLogResultDto
    {
        public long id { get; set; }
        public long workflowId { get; set; }
      
        public UserCategory userCategory { get; set; }
        public string refNo { get; set; }
        public string applicantName { get; set; }
        public string userName { get; set; }
        public string status { get; set; }
        public long? entryDate  { get; set; }
        public long? entryTime { get; set; }
        public string comments { get; set; }
    }
}
