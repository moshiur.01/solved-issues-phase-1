﻿using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class SubAgentUserLimitDto
    {
        public long id { get; set; }
        public UserStatus? userStatus { get; set; }
        public string userName { get; set; }
        public string fullName { get; set; }
        public string mobileNumber { get; set; }
        public Address address { get; set; }

        public bool? creditLimitApplicable { get; set; }
        public decimal? dailyCreditLimit { get; set; }
        public decimal? dailyDebitLimit { get; set; }
        public bool? debitLimitApplicable { get; set; }
        public decimal? usedDailyCreditLimit { get; set; }
        public decimal? usedDailyDebitLimit { get; set; }

        public bool? cashLimitApplicable { get; set; }
        public decimal? cashBalance { get; set; }
        public decimal? cashLimit { get; set; }

        public long? individualId { get; set; }

        public long? creationDate { get; set; }
    }
}
