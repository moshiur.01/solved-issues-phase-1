﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class BankUserReportDto
    {
        public string userId { get; set; }
        public string userName { get; set; }
        public string contactNo { get; set; }
        public UserStatus status { get; set; }
        public UserType usertype { get; set; }
        public BankUserType bankUserType { get; set; }
        public string branchName { get; set; }
        public long branchId { get; set; }
    }
}
