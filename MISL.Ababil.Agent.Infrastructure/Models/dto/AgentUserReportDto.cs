﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class AgentUserReportDto
    {
        public string userId { get; set; }
        public string userName { get; set; }
        public string contactNo { get; set; }
        public UserStatus status { get; set; }
        public UserType usertype { get; set; }
        public string agentName { get; set; }
        public long agentId { get; set; }
    }
}
