﻿using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class AgentSearchDto
    {
        public string agentCode { get; set; }
        public string businessName { get; set; }
        public long? creationDateFrom { get; set; }
        public long? creationDateTo { get; set; }
        //public long? approvalDateFrom { get; set; }
        //public long? approvalDateTo { get; set; }
        //public AgentTransactionStatus? transactionStatus { get; set; }
        //public ApprovalStatus? approvalStatus { get; set; }
        public string mobileNumber { get; set; }
    }
}