﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
   public class UserSearchDtoForAgent
    {
        public long? agentId { get; set; }
        public UserType? userType { get; set; }
        public UserStatus? userStatus { get; set; }
    }
}
