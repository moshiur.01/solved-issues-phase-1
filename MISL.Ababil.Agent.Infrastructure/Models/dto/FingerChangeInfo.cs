﻿using MISL.Ababil.Agent.Infrastructure.Models.domain.models.fingerprint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class FingerChangeInfo
    {
        public string userName { get; set; }
        public PersonFingerInfo personFingerInfo { get; set; }
    }
}
