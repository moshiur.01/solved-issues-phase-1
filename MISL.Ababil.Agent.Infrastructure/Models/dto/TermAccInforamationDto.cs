﻿using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class TermAccInforamationDto
    {
        public string accountNo { get; set; }
        public string accountTitle { get; set; }
        public string productTitle { get; set; }
        public string accountstatus { get; set; }
        public string openingdate { get; set; }
        public string expirydate { get; set; }
        public string lastOperationdate { get; set; }
        public string closingdate { get; set; }
        public long? custId { get; set; }
        public string customerName { get; set; }
        public decimal? currentbalance { get; set; }
        public decimal? installmentAmount { get; set; }
        public long? totalNoOfInstallment { get; set; }
        public decimal? noOfGivenInstallment { get; set; }
        public decimal? noOfDueInstallment { get; set; }
        //public decimal? noOfAdvInstallment { get; set; }
        public decimal? totalAdvInstallment { get; set; }
        public decimal? quardAmount { get; set; }
        public decimal? lienAmount { get; set; }
        public string renewDate { get; set; }
        public decimal? renewRate { get; set; }
        public AccountType? accountType { get; set; }
    }
}
