﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class AgentChargeSearchDto
    {
        public long? agentId { get; set; }
        public long? outletId { get; set; }
        public long? chargeDateFrom { get; set; }
        public long? chargeDateTo { get; set; }
    }
}