﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class AgentUserSearchResultDto
    {
        public long userId { get; set; }
        public long individualId { get; set; }
        public string userName { get; set; }
        public string fullName { get; set; }
        public UserType userType { get; set; }
        public UserStatus userStatus { get; set; }
        public string mobileNumber { get; set; }
        public string userWorkstation { get; set; }
        public long userWorkstationId { get; set; }
    }
}
