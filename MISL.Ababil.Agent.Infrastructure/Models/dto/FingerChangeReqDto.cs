﻿using System.Collections.Generic;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.fingerprint;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class FingerChangeReqDto
    {
        public long individualId { get; set; }
        public string individualName { get; set; }
        public string mobileNo { get; set; }
        public string fatherName { get; set; }
        public string motherName { get; set; }
        public byte[] image { get; set; }
        public List<FingerInfo> fingerInfos { get; set; }
        public string reason { get; set; }
        public string outletUserTemplate { get; set; }
        public string token { get; set; }
        public string refNo { get; set; }
    }
}