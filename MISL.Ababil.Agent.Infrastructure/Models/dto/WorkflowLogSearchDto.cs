﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class WorkflowLogSearchDto
    {
        public WorkFlowType workFlowType { get; set; }
        public long fromDate { get; set; }
        public long toDate { get; set; }
        public String referenceNumber { get; set; }
    }
}
