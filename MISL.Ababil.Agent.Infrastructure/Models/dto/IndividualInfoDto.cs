﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class IndividualInfoDto
    {
        public long individualId { get; set; }
        public string name { get; set; }
        public string mobileNo { get; set; }
    }
}
