﻿using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class SecurityCheckDto
    {
        public string accountNo { get; set; }
        public AgentServicesType agentServices { get; set; }
        public decimal txnAmount { get; set; }
    }
}
