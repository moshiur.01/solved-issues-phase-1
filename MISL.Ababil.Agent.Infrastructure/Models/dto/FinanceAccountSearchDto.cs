﻿using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class FinanceAccountSearchDto
    {
        
        public long? agentId { get; set; }
        public long? outletId { get; set; }
        public FinanceProduct financeProduct { get; set; }
        public long fromDate { get; set; }
        public long toDate { get; set; }
    }
}
