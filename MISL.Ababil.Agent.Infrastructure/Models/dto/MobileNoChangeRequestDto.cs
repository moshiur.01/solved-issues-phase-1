﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class MobileNoChangeRequestDto
    {
        public string accountNo { get; set; }
        public string customerMobileNo { get; set; }
        public List<IndividualInfoDto> individualInfoDtos { get; set; }
        public string remark { get; set; }
    }
}
