﻿using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class AccountResultSearchDto
    {
        public string accountNumber { get; set; }
        public string accountTitle { get; set; }
        public AccountType accountType { get; set; }
    }
}
