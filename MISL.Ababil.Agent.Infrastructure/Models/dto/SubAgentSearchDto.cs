﻿using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class SubAgentSearchDto
    {
        public long? agentId { get; set; }
        public string outletMobileNumber { get; set; }
        public string outletName { get; set; }
        public string outletCode { get; set; }
        public long? fromDate { get; set; }
        public long? toDate { get; set; }
        public OutLetStatus? outLetStatus { get; set; }
    }
}
