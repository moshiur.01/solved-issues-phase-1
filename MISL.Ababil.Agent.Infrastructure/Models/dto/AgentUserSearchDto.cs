﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class AgentUserSearchDto
    {
        public UserCategory userCategory;
        public BankUserType bankUserType;
        public long? agentId;
        public long? subAgentId;
    }
}
