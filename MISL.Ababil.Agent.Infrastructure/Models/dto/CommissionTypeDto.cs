﻿namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class CommissionTypeDto
    {
        public long? id { get; set; }
        public string commissionName { get; set; }
    }
}
