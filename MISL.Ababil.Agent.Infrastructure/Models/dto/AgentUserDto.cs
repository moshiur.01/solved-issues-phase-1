﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.cis;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.fingerprint;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class AgentUserDto
    {
        public AgentUser agentUser { get; set;}
        public SubAgentUser subAgentUser { get; set; }
        public BankUser bankUser { get; set; }
        public UserCategory userCategory { get; set; }

        public IndividualInformation individualInformation { get; set; }
       
    }
}
