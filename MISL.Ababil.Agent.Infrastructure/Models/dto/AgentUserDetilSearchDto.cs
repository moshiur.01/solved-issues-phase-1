﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class AgentUserDetilSearchDto
    {
        public UserCategory userCategory { get; set; }
        public long? userId { get; set; }
        public long? individualId { get; set; }
    }
}
