﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class TermAccountClosingDto
    {
        public string accountNo { get; set; }
        public string name { get; set; }
        public AccountType accounttype { get; set; }
        public decimal quardAmmount { get; set; }
        public decimal lienAmmount { get; set; }
        public long? accOpenDate { get; set; }
        public long? lastRenewaldate { get; set; }
        public long? accMaturityDate { get; set; }
        public long? lastExciseDutyDate { get; set; }
        public string curAddressLine1 { get; set; }
        public string curAddressLine2 { get; set; }
        public long? period { get; set; }
        public long? accClosingDate { get; set; }
        public decimal closingvoucherAccBalanceCr { get; set; }
        public decimal closingvoucherProfitDr { get; set; }
        public decimal closingvoucherProfitCr { get; set; }
        public decimal closingvoucherTaxAdjustedDr { get; set; }
        public decimal closingvoucherTaxAdjustedCr { get; set; }
        
        public decimal closingvoucherExciseDutyDr { get; set; }
        
        public decimal closingvoucherClosingChgDr { get; set; }
        
        public decimal closingvoucherVatDr { get; set; }
        public decimal closingvoucherNetPayableDr { get; set; }

        public decimal transactionInfoPrincipalDr { get; set; }
        public decimal transactionInfoPrincipalCr { get; set; }
        public decimal TransactionInfoProfitDr { get; set; }

        public decimal TransactionInfoProfitCr { get; set; }
        public decimal TransactionInfoTaxDr { get; set; }
        public decimal TransactionInfoTaxCr { get; set; }
        public decimal TransactionInfoExciseDutyCr { get; set; }
        public decimal TransactionInfoExciseDutyDr { get; set; }
        public decimal TransactionInfoOtherCr { get; set; }
        public decimal TransactionInfoOtherDr { get; set; }
        public decimal TransactionInfoTotalBalance { get; set; }

        public decimal profitToClientActualProfit { get; set; }
        public decimal profitToClientActualProvisionalProfit { get; set; }

        public SetteledAccountType SetteledAccountType { get; set; }


    }
}
