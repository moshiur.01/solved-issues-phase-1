﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class OutletAddressDto
    {
        public string agentName { get; set; }
        public string outletName { get; set; }
        public string outletCode { get; set; }
        public string address { get; set; }
        public string mobileNo { get; set; }
    }
}