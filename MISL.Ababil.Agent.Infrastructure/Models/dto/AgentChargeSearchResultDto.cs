﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class AgentChargeSearchResultDto
    {
        public long? chargeDate { get; set; }
        public string chargeFrom { get; set; }
        public decimal? amount { get; set; }
        public long? outletId { get; set; }
        public string outletName { get; set; }
        public string userName { get; set; }
        public long? agentId { get; set; }
        public string agentName { get; set; }
    }
}
