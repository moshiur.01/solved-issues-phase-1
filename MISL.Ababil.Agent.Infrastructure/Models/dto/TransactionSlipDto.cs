﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class TransactionSlipDto
    {
        public OutletAddressDto outletAddressDto { get; set; }
        public string creditAccount { get; set; }
        public string creditAccName { get; set; }
        public string debitAccount { get; set; }
        public string debitAccountName { get; set; }
        public decimal? txnAmount { get; set; }
        public decimal? chargeAmount { get; set; }
        public string txnUser { get; set; }
        public string txnDate { get; set; }
        public string refNo { get; set; }
        public string amountInWords { get; set; }
    }
}