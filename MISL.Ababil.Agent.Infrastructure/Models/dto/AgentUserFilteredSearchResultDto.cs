﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class AgentUserFilteredSearchResultDto
    {
        public List<AgentUserSearchResultDto> agentUserSearchResultDtos { get; set; }
        public UserCategory userCategory { get; set; }
        public BankUserType bankUserType;
        public long agentId;
        public long outletId;
    }
}