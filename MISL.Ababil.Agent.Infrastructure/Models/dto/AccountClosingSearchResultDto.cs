﻿using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class AccountClosingSearchResultDto
    {
        public long agentId { get; set; }
        public string agentName { get; set; }
        public long outletId { get; set; }
        public string outletName { get; set; }
        public AccountType accountType { get; set; }
        public string accountNo { get; set; }
        public string accountTitle { get; set; }
        public AppStatus appStatus { get; set; }
        public string requestDate { get; set; }
        public string refNo { get; set; }
        public long? docId { get; set; }
        public long? commentId { get; set; }
    }
}