﻿using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Infrastructure.Models.dto
{
    public class SubAgentInfoDto
    {
        public long id { get; set; }
        public string name { get; set; }
        public Address businessAddress { get; set; }
        public string outletContactNo { get; set; }
    }
}