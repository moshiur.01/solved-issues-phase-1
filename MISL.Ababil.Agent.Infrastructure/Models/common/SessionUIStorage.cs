﻿using System.Windows.Forms;

namespace MISL.Ababil.Agent.Infrastructure.Models.common
{
    public static class SessionUIStorage
    {
        public static Form StorgedLoginForm { get; set; }
        public static Form MainForm { get; set; }
    }
}