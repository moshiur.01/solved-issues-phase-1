﻿namespace MISL.Ababil.Agent.Infrastructure.Models.common
{
    public enum PostedStatus
    {
        Posted, Unposted, Cancel
    }
}