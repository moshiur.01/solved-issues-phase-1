﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Module.Security.Models
{
    public class FingerprintChangeReqSearchDto
    {
        public long agentId { get; set; }
        public long outlateId { get; set; }
        public long? individualId { get; set; }
        public string referanceNumber { get; set; }
        public string mobileNumber { get; set; }
        public long? branchId { get; set; }
    }
}
