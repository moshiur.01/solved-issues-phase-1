﻿using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Validation;
using MISL.Ababil.Agent.Module.Common.UI;
using MISL.Ababil.Agent.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using MISL.Ababil.Agent.Module.Common.UI.MessageUI;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.GenericFingerprintServices;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.UI.forms.ProgressUI;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Module.Security.Service;
using MISL.Ababil.Agent.Module.Security.Models;

using MISL.Ababil.Agent.Infrastructure.Models.domain.models.fingerprint;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.cis;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.consumer;
using MISL.Ababil.Agent.Module.Common.UI.IndividualInformationUI;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;

namespace MISL.Ababil.Agent.Module.Security.UI.FingerprintUI
{
    public partial class frmFingerprintChangeRequest : CustomForm, FingerprintEventObserver
    {
        public frmFingerprintChangeRequest(Packet receivePacket, string accoutNo, string referenceNo, string reqId)
        {
            _receivePacket = receivePacket;
            _reqId = reqId;

            InitializeComponent();

            lblRequiredFingerPrint.Text = "";
            lblConsumerTitle.Text = "";
            lblMobileNo.Text = "";

            ConfigUIEnhancement();
            SetupDataLoad();

            //if
            //    (
            //        SessionInfo.userBasicInformation.userType == AgentUserType.Branch
            //    ||
            //        SessionInfo.userBasicInformation.userType == AgentUserType.Admin
            //    )
            if
                (
                    SessionInfo.userBasicInformation.userCategory == UserCategory.BranchUser
                &&
                    SessionInfo.userBasicInformation.bankUserType == BankUserType.BranchUser
                )
            {
                _accoutNo = accoutNo;
                _referenceNumber = referenceNo;
                FillComponentWithObjectValue();
            }

            SetUIByUserRights();
        }


        string _accoutNo = "";
        string _referenceNumber = "";

        GUI _gui = new GUI();
        ConsumerServices _consumerService = new ConsumerServices();
        BioDataChangeReqDto _bioDataChangeReqDto = new BioDataChangeReqDto();
        FingerprintManagementService _fingerprintManagementService = new FingerprintManagementService();

        private string _captureFor;
        private int _captureIndexNo;
        private int _noOfCapturefinger = 0;
        private string _subagentFingerData;
        private Packet _receivePacket;
        private List<BiometricTemplate> _fingerList;
        private string _reqId;

        public List<CommentDto> _comments;
        public CommentDto _commentDraft;
        private ConsumerInformationDto _consumerInformationDto;
        private AccountOperatorDto _accountOperatorDto;

        private void SetUIByUserRights()
        {
            //if (
            //        SessionInfo.userBasicInformation.userType == AgentUserType.Branch
            //    ||
            //        SessionInfo.userBasicInformation.userType == AgentUserType.Admin
            //    )
            if
                (
                    SessionInfo.userBasicInformation.userCategory == UserCategory.BranchUser
                &&
                    SessionInfo.userBasicInformation.bankUserType == BankUserType.BranchUser
                )
            {
                btnApprove.Visible = true;
                btnApprove.Enabled = true;
                btnReject.Visible = true;
                btnReject.Enabled = true;

                txtConsumerAccount.ReadOnly = true;
                btnClear.Enabled = false;
                btnClear.Visible = false;
            }
            //if (SessionInfo.userBasicInformation.userType == AgentUserType.Outlet)
            if (SessionInfo.userBasicInformation.userCategory == UserCategory.SubAgentUser)
            {
                btnApply.Visible = true;
                btnApply.Enabled = true;

                txtConsumerAccount.ReadOnly = false;
            }
        }

        private void FillComponentWithObjectValue()
        {
            LoadConsumerInfoByUserType(_accoutNo, _referenceNumber);
        }

        public ConsumerServices ConsumerService
        {
            get
            {
                return _consumerService;
            }

            set
            {
                _consumerService = value;
            }
        }

        public void ConfigUIEnhancement()
        {
            _gui = new GUI(this);

            _gui.Config(ref txtConsumerAccount, GUIValidation.VALIDATIONTYPES.TEXTBOX_EMPTY, null);
        }

        private void SetupDataLoad()
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtConsumerAccount_Leave(object sender, EventArgs e)
        {
            //if (SessionInfo.userBasicInformation.userType == AgentUserType.Outlet)
            if (SessionInfo.userBasicInformation.userCategory == UserCategory.SubAgentUser)
            {
                //LoadConsumerInfoByUserType(txtConsumerAccount.Text, null);
                ///////////////////////////////////////////////////////////////////
                /// /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                LoadConsumerInformation(txtConsumerAccount.Text);
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ////////////////////////////////////////////////////////////////////// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////
                /// ///////////////////////////////////////////////////////////////////

            }
        }

        private void LoadConsumerInformation(string accountNumber)
        {
            if (ValidationManager.ValidateNonEmptyTextWithoutSpace(accountNumber))
            {
                try
                {
                    _consumerInformationDto = _consumerService.getConsumerInformationDtoByAcc(txtConsumerAccount.Text);
                    lblConsumerTitle.Text = _consumerInformationDto.consumerTitle;
                    lblMobileNo.Text = _consumerInformationDto.mobileNumber;
                    //lblBalanceValue.Text = (_consumerInformationDto.balance ?? 0).ToString("N", new CultureInfo("BN-BD"));

                    //txtrCharge.Text = "";
                    //txtrTotal.Text = "";
                    lblRequiredFingerPrint.Text = "At least " + _consumerInformationDto.numberOfOperator + " operator's finger print required.";
                    if (_consumerInformationDto.photo != null)
                    {
                        byte[] bytes = Convert.FromBase64String(_consumerInformationDto.photo);
                        Image image;
                        image = UtilityServices.byteArrayToImage(bytes);
                        pic_conusmer.Image = image;
                    }


                    _accountOperatorDto = new AccountInformationService().GetAccountOperatorDtoByAccountNumber(txtConsumerAccount.Text);
                    if (_accountOperatorDto?.operators != null && _accountOperatorDto.operators.Count > 0)
                    {
                        fingerPrintGrid.DataSource = _accountOperatorDto.operators.Select(o => new OperatorfingerPrintGrid(o) { identity = o.individualId.ToString(), identityName = o.individualName }).ToList();

                    }
                    //if ( _accountOperatorDto. != null)
                    //{
                    //    fingerPrintGrid.DataSource =  _accountOperatorDto..Select(o => new OperatorfingerPrintGrid(o) { identity = o.identity, identityName = o.identityName }).ToList();
                    //}
                }
                catch (Exception ex)
                {
                    MsgBox.ShowError(ex.Message);

                    lblConsumerTitle.Text = "";
                    lblMobileNo.Text = "";
                    pic_conusmer.Image = null;
                    fingerPrintGrid.DataSource = null;
                    lblRequiredFingerPrint.Text = null;
                }
            }
        }

        private void LoadConsumerInfoByUserType(string accountNumber, string referenceNumber)
        {
            //if (
            //        SessionInfo.userBasicInformation.userType == AgentUserType.Branch
            //    ||
            //        SessionInfo.userBasicInformation.userType == AgentUserType.Admin
            //    )
            if
                (
                    SessionInfo.userBasicInformation.userCategory == UserCategory.BranchUser
                &&
                    SessionInfo.userBasicInformation.bankUserType == BankUserType.BranchUser
                )
            {
                loadConsumerInformation(true, accountNumber, referenceNumber);
            }
            //if (
            //        SessionInfo.userBasicInformation.userType == AgentUserType.Outlet
            //    )
            if (
                    SessionInfo.userBasicInformation.userCategory == UserCategory.SubAgentUser
                )
            {
                loadConsumerInformation(false, accountNumber, null);
            }
        }

        private void bio_OnCapture_AgreeAndSubmit(object sender, EventArgs e)
        {
            //@@@//this.Enabled = false;
            //@@@//AxBIOPLUGINACTXLib.AxBioPlugInActX x = (AxBIOPLUGINACTXLib.AxBioPlugInActX)sender;
            //@@@//
            //@@@//if (x.result == "0")
            //@@@//{
            //@@@//    BioDataChangeReqDto _bioDataChangeReqDto_Send;
            //@@@//
            //@@@//    _subagentFingerData = bio.GetSafeLeftFingerData();
            //@@@//    if (!string.IsNullOrEmpty(_subagentFingerData))
            //@@@//    {
            //@@@//        bool fingerprintAvailableForAllflag = true;
            //@@@//        _bioDataChangeReqDto.outletUserTemplate = _subagentFingerData;
            //@@@//        if (_bioDataChangeReqDto.bioDataChReqOwnerDtos.Count > 0)
            //@@@//        {
            //@@@//            string retVal = "";
            //@@@//
            //@@@//            #region OLD
            //@@@//     //for (int i = 0; i < _bioDataChangeReqDto.bioDataChReqOwnerDtos.Count; i++)
            //@@@//     //{
            //@@@//     //    if (
            //@@@//     //            _bioDataChangeReqDto.bioDataChReqOwnerDtos[i].fingerDatas == null
            //@@@//     //        ||
            //@@@//     //            _bioDataChangeReqDto.bioDataChReqOwnerDtos[i].fingerDatas.Count <= 0
            //@@@//     //        )
            //@@@//     //    {
            //@@@//     //        fingerprintAvailableForAllflag = false;
            //@@@//     //        break;
            //@@@//     //    }
            //@@@//     //    else
            //@@@//     //    {
            //@@@//     //        retVal = fingerPrintGrid.Rows[i].Cells[4].Value.ToString();
            //@@@//     //        if (!string.IsNullOrEmpty(retVal))
            //@@@//     //        {
            //@@@//     //            _bioDataChangeReqDto.bioDataChReqOwnerDtos[i].reason = retVal;
            //@@@//     //        }
            //@@@//
            //@@@//     //    }
            //@@@//     //}
            //@@@//            #endregion
            //@@@//
            //@@@//            // NEW
            //@@@//            for (int i = (_bioDataChangeReqDto.bioDataChReqOwnerDtos.Count - 1); i >= 0; i--)
            //@@@//            {
            //@@@//                if (
            //@@@//                        _bioDataChangeReqDto.bioDataChReqOwnerDtos[i].fingerDatas == null
            //@@@//                    ||
            //@@@//                        _bioDataChangeReqDto.bioDataChReqOwnerDtos[i].fingerDatas.Count <= 0
            //@@@//                    )
            //@@@//                {
            //@@@//                    // fingerprintAvailableForAllflag = false;
            //@@@//                    _bioDataChangeReqDto.bioDataChReqOwnerDtos.RemoveAt(i);
            //@@@//                    //break;
            //@@@//                }
            //@@@//                else
            //@@@//                {
            //@@@//                    retVal = fingerPrintGrid.Rows[i].Cells[4].Value.ToString();
            //@@@//                    if (!string.IsNullOrEmpty(retVal))
            //@@@//                    {
            //@@@//                        _bioDataChangeReqDto.bioDataChReqOwnerDtos[i].reason = retVal;
            //@@@//                    }
            //@@@//                }
            //@@@//            }
            //@@@//            //// --------------
            //@@@//
            //@@@//            if (_bioDataChangeReqDto.bioDataChReqOwnerDtos.Count == 0)
            //@@@//                fingerprintAvailableForAllflag = false;
            //@@@//
            //@@@//            if (
            //@@@//                    (txtConsumerAccount.Text != "")
            //@@@//                &&
            //@@@//                    (fingerprintAvailableForAllflag == true)
            //@@@//                )
            //@@@//            {
            //@@@//                try
            //@@@//                {
            //@@@//                    ProgressUIManager.ShowProgress(this);
            //@@@//                    string ServiceResponse = _fingerprintManagementService.UpdateBioDataChangeReqDtoList(_bioDataChangeReqDto);
            //@@@//                    ProgressUIManager.CloseProgress();
            //@@@//                    MsgBox.showInformation(ServiceResponse);
            //@@@//                    ClearUI();
            //@@@//                }
            //@@@//                catch (Exception ex)
            //@@@//                {
            //@@@//                    ProgressUIManager.CloseProgress();
            //@@@//                    btnApply.Enabled = true;
            //@@@//                    MsgBox.ShowError(ex.Message);
            //@@@//                }
            //@@@//            }
            //@@@//        }
            //@@@//        if (fingerprintAvailableForAllflag == false)
            //@@@//        {
            //@@@//            MsgBox.ShowError("Required number of Fingerprint is not set!");
            //@@@//        }
            //@@@//    }
            //@@@//}
            //@@@//this.Enabled = true;
        }

        private void txtConsumerAccount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) && (Keys)e.KeyChar != Keys.Back && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            base.OnKeyPress(e);
        }

        private void fingerPrintGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)

        {
            int individualId = int.Parse(fingerPrintGrid.Rows[e.RowIndex].Cells["Individual_ID"].Value.ToString());


            //if (
            //        SessionInfo.userBasicInformation.userType == AgentUserType.Branch
            //    ||
            //        SessionInfo.userBasicInformation.userType == AgentUserType.Admin
            //    )
            if
                (
                    SessionInfo.userBasicInformation.userCategory == UserCategory.BranchUser
                &&
                    SessionInfo.userBasicInformation.bankUserType == BankUserType.BranchUser
                )
            {
                #region For bank user

                if (fingerPrintGrid.Columns[e.ColumnIndex].Name == "info")
                {
                    IndividualServices individualServices = new IndividualServices();




                    IndividualInformation individualInformation = individualServices.GetIndividualInfo(individualId);

                    Packet packet = new Packet();
                    packet.actionType = FormActionType.View;
                    frmIndividualInformation frm = new frmIndividualInformation(packet, individualInformation);
                    frm.ShowDialog();
                    return;
                }

                if (fingerPrintGrid.Columns[e.ColumnIndex].Name == "resend")
                {
                    try
                    {
                        BioChangeDto bioChangeDto = new BioChangeDto();
                        //bioChangeDto.identity = fingerPrintGrid.Rows[e.RowIndex].Cells[0].Value.ToString();
                        bioChangeDto.identity = fingerPrintGrid.Rows[e.RowIndex].Cells["Identity"].Value.ToString();

                        ///check
                        //individualId = long.Parse
                        //(
                        //    fingerPrintGrid.Rows[e.RowIndex].Cells[1].Value.ToString()
                        //);
                        bioChangeDto.individualId = individualId;

                        ProgressUIManager.ShowProgress(this);
                        _fingerprintManagementService.ResendToken(bioChangeDto);
                        ProgressUIManager.CloseProgress();

                        MsgBox.showInformation("Token is resent successfully.");
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex.Message);
                    }
                }
                #endregion
            }

            if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            {
                //@@@//frmFingerprintCapture objFrmFinger = new frmFingerprintCapture(lblConsumerTitle.Text);
                //@@@//DialogResult dr = objFrmFinger.ShowDialog();
                //@@@//if (dr == DialogResult.OK)
                //@@@//{
                //@@@//    _captureIndexNo = e.RowIndex;
                //@@@//
                //@@@//    _fingerList = objFrmFinger.bioMetricTemplates;
                //@@@//    _bioDataChangeReqDto.bioDataChReqOwnerDtos[_captureIndexNo].fingerDatas = _fingerList;
                //@@@//    colorFingerDataRows();
                //@@@//}

                frmFingerprintCapture objFrmFinger = new frmFingerprintCapture(lblConsumerTitle.Text);
                DialogResult dr = objFrmFinger.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    _captureIndexNo = e.RowIndex;

                    _fingerList = objFrmFinger.bioMetricTemplates;
                    _bioDataChangeReqDto.bioDataChReqOwnerDtos[_captureIndexNo].fingerDatas = _fingerList;
                    colorFingerDataRows();
                }
            }
        }

        private void colorFingerDataRows()
        {
            for (int i = 0; i < fingerPrintGrid.Rows.Count; i++)
            {
                if (_bioDataChangeReqDto.bioDataChReqOwnerDtos[_captureIndexNo].fingerDatas.Count > 0)
                {
                    //fingerPrintGrid.Rows[i].DefaultCellStyle.BackColor = Color.Green;
                    fingerPrintGrid.Rows[_captureIndexNo].DefaultCellStyle.BackColor = Color.LightGreen;             // WALI :: 15-Feb-2016
                    fingerPrintGrid.Rows[_captureIndexNo].Cells[fingerPrintGrid.Columns.Count - 1].Style.BackColor = Color.White;
                }
            }
        }

        private void loadConsumerInformation(bool showAdminOnlyColumns, string accountNumber, string referenceNumber)
        {
            if (ValidationManager.ValidateNonEmptyTextWithoutSpace(accountNumber))
            {
                try
                {
                    FingerprintManagementService fingerprintManagementService = new FingerprintManagementService();
                    _bioDataChangeReqDto = fingerprintManagementService.GetBioDataChangeReqDtoList(accountNumber, referenceNumber);
                    if (showAdminOnlyColumns == true)
                    {
                        txtConsumerAccount.Text = accountNumber;
                    }
                    lblConsumerTitle.Text = _bioDataChangeReqDto.accountTitle;
                    lblMobileNo.Text = _bioDataChangeReqDto.mobileNumber;

                    if (_bioDataChangeReqDto.image != null)
                    {
                        Image image;
                        image = UtilityServices.byteArrayToImage(_bioDataChangeReqDto.image);
                        pic_conusmer.Image = image;
                    }

                    //Clearing the Grid
                    {
                        fingerPrintGrid.DataSource = null;
                        fingerPrintGrid.Columns.Clear();
                    }

                    //New Capture - Button
                    {
                        if (showAdminOnlyColumns == false)
                        {
                            DataGridViewButtonColumn buttonColumn = new DataGridViewButtonColumn();
                            buttonColumn.Text = "New Capture";
                            buttonColumn.UseColumnTextForButtonValue = true;
                            fingerPrintGrid.Columns.Add(buttonColumn);
                        }
                    }

                    if (_bioDataChangeReqDto.bioDataChReqOwnerDtos != null)
                    {
                        fingerPrintGrid.DataSource = _bioDataChangeReqDto.bioDataChReqOwnerDtos.Select(o => new BioDataChReqOwnerDtoGrid(o) { Identity = o.identity, Individual_ID = o.individualId.ToString(), Identity_Name = o.individualName, Reason = o.reason }).ToList();
                    }

                    for (int i = 0; i < fingerPrintGrid.Columns.Count; i++)
                    {
                        fingerPrintGrid.Columns[i].ReadOnly = true;
                    }

                    //Individual Info - Button
                    {
                        if (showAdminOnlyColumns == true)
                        {
                            DataGridViewButtonColumn buttonColumn = new DataGridViewButtonColumn();
                            buttonColumn.Text = "Info...";
                            buttonColumn.Name = "info";
                            buttonColumn.UseColumnTextForButtonValue = true;
                            fingerPrintGrid.Columns.Add(buttonColumn);
                        }
                    }

                    //Token - Cell TextBox
                    {
                        if (showAdminOnlyColumns == true)
                        {
                            DataGridViewTextBoxColumn textBoxColumn = new DataGridViewTextBoxColumn();
                            textBoxColumn.HeaderText = "Token";
                            textBoxColumn.Name = "Token";

                            textBoxColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                            fingerPrintGrid.Columns.Add(textBoxColumn);
                        }
                    }

                    //Token Resend Button - Button
                    {
                        if (showAdminOnlyColumns == true)
                        {
                            DataGridViewButtonColumn buttonColumn = new DataGridViewButtonColumn();
                            buttonColumn.Text = "Resend";
                            buttonColumn.Name = "resend";
                            buttonColumn.UseColumnTextForButtonValue = true;
                            fingerPrintGrid.Columns.Add(buttonColumn);
                        }
                    }

                    if (showAdminOnlyColumns == true)
                    {
                        fingerPrintGrid.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        fingerPrintGrid.Columns[0].Visible = false;
                        lblRequiredFingerPrint.Visible = false;
                    }
                    else
                    {
                        fingerPrintGrid.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        fingerPrintGrid.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        fingerPrintGrid.Columns[1].Visible = false;
                        fingerPrintGrid.Columns[4].ReadOnly = false;
                    }
                }
                catch (Exception ex)
                {
                    MsgBox.ShowError(ex.Message);

                    lblConsumerTitle.Text = "";
                    lblMobileNo.Text = "";
                    //lblBalanceValue.Text = "";
                    pic_conusmer.Image = null;
                    fingerPrintGrid.DataSource = null;
                    lblRequiredFingerPrint.Text = null;
                }
            }
        }

        public class BioDataChReqOwnerDtoGrid
        {
            public string Identity { get; set; }

            public string Individual_ID { get; set; }
            public string Identity_Name { get; set; }

            public string Reason { get; set; }

            private BioDataChReqOwnerDto _obj;

            public BioDataChReqOwnerDtoGrid(BioDataChReqOwnerDto obj)
            {
                _obj = obj;
            }

            public BioDataChReqOwnerDto GetModel()
            {
                return _obj;
            }
        }

        private void ClearUI()
        {
            _commentDraft = null;
            _comments = null;

            txtConsumerAccount.Enabled = true;
            txtConsumerAccount.Text = "";
            pic_conusmer.Image = null;
            fingerPrintGrid.DataSource = null;
            lblConsumerTitle.Text = "";
            lblMobileNo.Text = "";
            lblRequiredFingerPrint.Text = "";
            _noOfCapturefinger = 0;

            _gui.SetControlState
            (
                GUI.CONTROLSTATES.CS_READWRITE,
                new Control[]
                {
                    txtConsumerAccount,
                }
            );
        }

        private void btClear_Click(object sender, EventArgs e)
        {
            if (MsgBox.showConfirmation("Are you sure to clear?") == "yes")
            {
                ClearUI();
            }
        }

        private void frmChequeRequisition_Load(object sender, EventArgs e)
        {

        }

        private void frmChequeRequisition_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            if (CheckForValidationFromOutlet(false))
            {
                //@@@//bio.CaptureFingerData();
                FingerprintServiceFactory.FingerprintServiceFactory factory = new FingerprintServiceFactory.FingerprintServiceFactory();
                FingerprintDevice device = factory.getFingerprintDevice();
                device.registerEventObserver(this);
                device.capture();
            }
        }

        private void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                if (CheckForValidationFromBranch(false))
                {
                    if (FillDto(false))
                    {
                        ProgressUIManager.ShowProgress(this);
                        string retVal = _fingerprintManagementService.SubmitTokens(_bioDataChangeReqDto);
                        ProgressUIManager.CloseProgress();
                        MsgBox.showInformation("Fingerprint updated successfully.");
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                ProgressUIManager.CloseProgress();
                MsgBox.ShowError(ex.Message);
            }

        }

        private bool CheckForValidationFromBranch(bool suppressed)
        {
            bool flag = true;
            for (int i = 0; i < fingerPrintGrid.Rows.Count; i++)
            {
                object value = fingerPrintGrid.Rows[i].Cells[fingerPrintGrid.Columns.Count - 1].Value;
                if (
                        (value == null)
                    ||
                        (string.IsNullOrEmpty(value.ToString()))
                    )
                {
                    flag = false;
                }
            }
            if (flag == false)
            {
                if (!suppressed)
                {
                    MsgBox.ShowError("Token not found!");
                }
                return false;
            }
            return true;
        }

        private bool CheckForValidationFromOutlet(bool suppressed)
        {
            if (string.IsNullOrEmpty(txtConsumerAccount.Text))
            {
                if (!suppressed)
                {
                    MsgBox.ShowError("Account number not found!");
                }
                return false;
            }

            // Wali :: 25-Apr-2016
            if (fingerPrintGrid.Rows.Count == 0)
            {
                if (!suppressed)
                {
                    MsgBox.ShowError("Credential not found!");
                }
                return false;
            }
            //

            bool flag = true;
            for (int i = 0; i < fingerPrintGrid.Rows.Count; i++)
            {
                //if (fingerPrintGrid.Rows[i].Cells[4].Value == null || string.IsNullOrEmpty(fingerPrintGrid.Rows[i].Cells[4].Value.ToString()))
                //{
                //    flag = false;
                //}


                if (
                        _bioDataChangeReqDto.bioDataChReqOwnerDtos[i].fingerDatas != null
                    &&
                        _bioDataChangeReqDto.bioDataChReqOwnerDtos[i].fingerDatas.Count > 0
                    )
                {
                    string reason = (fingerPrintGrid.Rows[i].Cells[4].Value == null) ? "" : fingerPrintGrid.Rows[i].Cells[4].Value.ToString().Trim();
                    if (reason == "")
                    {
                        flag = false;
                    }
                }

                // Wali :: 25-Apr-2016
                else
                {
                    if (!suppressed)
                    {
                        MsgBox.ShowError("Please capture new fingerprint.");
                    }
                    return false;
                }
                //
            }
            if (flag == false)
            {
                if (!suppressed)
                {
                    MsgBox.ShowError("Reason not found!");
                }
                return false;
            }
            return true;
        }

        private bool FillDto(bool suppressed)
        {
            for (int i = 0; i < fingerPrintGrid.Rows.Count; i++)
            {
                if (fingerPrintGrid.Rows[i].Cells[fingerPrintGrid.Columns.Count - 2].Value != null)
                {
                    //_bioDataChangeReqDto.bioDataChReqOwnerDtos[i].token = fingerPrintGrid.Rows[i].Cells[fingerPrintGrid.Columns.Count - 2].Value.ToString();
                    _bioDataChangeReqDto.bioDataChReqOwnerDtos[i].token = fingerPrintGrid.Rows[i].Cells["Token"].Value.ToString();
                }
                else
                {
                    if (!suppressed)
                    {
                        MsgBox.ShowError("Token not found!");
                    }
                    return false;
                }
            }
            _bioDataChangeReqDto.referanceNumber = _referenceNumber;
            return true;
        }

        private void btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                if (MsgBox.showConfirmation("Do you want to reject this request?") == "yes")
                {
                    ProgressUIManager.ShowProgress(this);
                    string retVal = _fingerprintManagementService.Reject(_reqId);
                    ProgressUIManager.CloseProgress();
                    MsgBox.showInformation("Fingerprint update request is rejected.");
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
                ProgressUIManager.CloseProgress();
            }
        }

        public void FingerPrintEventOccured(FingerprintEvents eventSpec, object EventData)
        {
            if (EventData == null)
            {
                return;
            }
            this.Enabled = false;
            _subagentFingerData = EventData.ToString();
            if (!string.IsNullOrEmpty(_subagentFingerData))
            {
                bool fingerprintAvailableForAllflag = true;
                _bioDataChangeReqDto.outletUserTemplate = _subagentFingerData;
                if (_bioDataChangeReqDto.bioDataChReqOwnerDtos.Count > 0)
                {
                    for (int i = (_bioDataChangeReqDto.bioDataChReqOwnerDtos.Count - 1); i >= 0; i--)
                    {
                        if (
                                _bioDataChangeReqDto.bioDataChReqOwnerDtos[i].fingerDatas == null
                            ||
                                _bioDataChangeReqDto.bioDataChReqOwnerDtos[i].fingerDatas.Count <= 0
                            )
                        {
                            _bioDataChangeReqDto.bioDataChReqOwnerDtos.RemoveAt(i);
                        }
                        else
                        {
                            string retVal = fingerPrintGrid.Rows[i].Cells[4].Value.ToString();
                            if (!string.IsNullOrEmpty(retVal))
                            {
                                _bioDataChangeReqDto.bioDataChReqOwnerDtos[i].reason = retVal;
                            }
                        }
                    }

                    if (_bioDataChangeReqDto.bioDataChReqOwnerDtos.Count == 0)
                    {
                        fingerprintAvailableForAllflag = false;
                    }

                    if (
                            (txtConsumerAccount.Text != "")
                        &&
                            (fingerprintAvailableForAllflag == true)
                        )
                    {
                        try
                        {
                            ProgressUIManager.ShowProgress(this);
                            string ServiceResponse = _fingerprintManagementService.UpdateBioDataChangeReqDtoList(_bioDataChangeReqDto);
                            ProgressUIManager.CloseProgress();
                            MsgBox.showInformation(ServiceResponse);
                            ClearUI();
                        }
                        catch (Exception ex)
                        {
                            ProgressUIManager.CloseProgress();
                            btnApply.Enabled = true;
                            MsgBox.ShowError(ex.Message);
                        }
                    }
                }
                if (fingerprintAvailableForAllflag == false)
                {
                    MsgBox.ShowError("Required number of Fingerprint is not set!");
                }
            }
            this.Enabled = true;
        }

        internal class OperatorfingerPrintGrid
        {
            public string identity { get; set; }
            public string identityName { get; set; }

            private AccountOperatorDetailsDto _obj;

            public OperatorfingerPrintGrid(AccountOperatorDetailsDto obj)
            {
                _obj = obj;
            }

            public AccountOperatorDetailsDto GetModel()
            {
                return _obj;
            }
        }
    }
}