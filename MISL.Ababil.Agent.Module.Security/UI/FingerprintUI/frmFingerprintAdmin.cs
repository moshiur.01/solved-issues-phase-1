﻿using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Module.Common.Model;
using MISL.Ababil.Agent.Module.Common.UI.MessageUI;
using MISL.Ababil.Agent.Module.Security.Models;
using MISL.Ababil.Agent.Module.Security.Service;
using MISL.Ababil.Agent.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.Module.Common.UI.ProgressUI;
using MISL.Ababil.Agent.Module.Security.UI.FingerprintChangeUI;
using MISL.Ababil.Agent.UI.forms.ProgressUI;

namespace MISL.Ababil.Agent.Module.Security.UI.FingerprintUI
{
    public partial class frmFingerprintAdmin : CustomForm
    {
        private bool _filling;

        public frmFingerprintAdmin()
        {
            InitializeComponent();
            SetupComponent();
        }

        private void SetupComponent()
        {
            try
            {
                //List of agents
                {
                    List<AgentInformation> agentInfoList = new AgentServices().getAgentInfoBranchWise();
                    agentInfoList.Insert(0, new AgentInformation { businessName = "(Select)" });
                    agentInfoList.Insert(1, new AgentInformation { businessName = "(All)" });
                    UtilityServices.fillComboBox(cmbAgentName, new BindingSource { DataSource = agentInfoList }, "businessName", "id");
                    cmbAgentName.Text = "Select";
                    cmbAgentName.SelectedIndex = 0;
                }

                //List of outlets
                {
                    List<SubagentList> subAgentList = new SubAgentServices().GetAllSubAgents().Select(o => new SubagentList()
                    {
                        id = o.id,
                        name = o.name,
                        subAgentCode = o.subAgentCode,
                        subAgentNameWithCode = o.name + "  [" + o.subAgentCode + "]"
                    }).ToList();
                    subAgentList.Insert(0, new SubagentList { subAgentNameWithCode = "(Select)" });
                    subAgentList.Insert(1, new SubagentList { subAgentNameWithCode = "(All)" });
                    UtilityServices.fillComboBox(cmbOutlet, new BindingSource { DataSource = subAgentList }, "subAgentNameWithCode", "id");
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
                this.Close();
            }
        }

        private void frmChequeRequisitionSearch_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void Search()
        {
            try
            {
                dgvResult.DataSource = null;
                dgvResult.Rows.Clear();
                dgvResult.Columns.Clear();
                //TODO: Fill when service is ready for parameters
                FingerprintChangeReqSearchDto fingerprintChangeReqSearchDto = new FingerprintChangeReqSearchDto();
                {
                    fingerprintChangeReqSearchDto.agentId = (long)cmbAgentName.SelectedValue;
                    fingerprintChangeReqSearchDto.outlateId = (long)cmbOutlet.SelectedValue;
                    fingerprintChangeReqSearchDto.individualId = long.Parse(txtIndividualID.Text == "" ? "0" : txtIndividualID.Text);
                    //TODO: NOTE: TextBox disabled
                    fingerprintChangeReqSearchDto.referanceNumber = txtReferenceNumber.Text;
                    //TODO: NOTE: TextBox disabled
                    fingerprintChangeReqSearchDto.mobileNumber = txtMobileNo.Text;
                    fingerprintChangeReqSearchDto.branchId = SessionInfo.userBasicInformation.userBranch;
                }

                ProgressUIManager.ShowProgress(this);
                List<BioDataChangeReqSearchResultDto> bioDataChangeReqSearchResultDtoList = new FingerprintManagementService().FingerprintDataChangeReqSearchDtoList(fingerprintChangeReqSearchDto);
                ProgressUIManager.CloseProgress();

                dgvResult.DataSource =
                    bioDataChangeReqSearchResultDtoList?.Select(o => new BioDataChangeReqSearchResultDtoGrid(o)
                    {
                        requestId = o.requestId,
                        referenceNumber = o.refrenceNumber,
                        agentName = o.agentName,
                        subAgentName = o.subAgentName,
                        individualId = o.individualId,
                        individualName = o.individualName,
                        mobileNumber = o.mobileNumber
                    }).ToList();

                //Open Button
                {
                    DataGridViewButtonColumn dgvButtonColumn = new DataGridViewButtonColumn
                    {
                        HeaderText = "",
                        Text = "Open",
                        UseColumnTextForButtonValue = true
                    };
                    dgvResult.Columns.Add(dgvButtonColumn);
                }
                //lblItemsFound.Text = "Item(s) Found: " + dgvRemittance.Rows.Count.ToString();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
                ProgressUIManager.CloseProgress();
            }
            this.Enabled = true;
            this.UseWaitCursor = false;
        }

        private void dgvResult_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == dgvResult.Columns.Count - 1)
                {
                    string individualId = dgvResult.Rows[e.RowIndex].Cells[4].Value.ToString();
                    string refNum = dgvResult.Rows[e.RowIndex].Cells[1].Value.ToString();
                    string reqId = dgvResult.Rows[e.RowIndex].Cells[0].Value.ToString();

                    new frmFingerprintChangeByIndividualID(new Packet()
                    {
                        actionType = FormActionType.View,
                        obj = refNum,
                        otherObj = individualId

                    }).ShowDialog(this);

                    Search();
                }
            }
            catch (Exception ex)
            {

            }

            //try
            //{
            //    if (e.ColumnIndex == dgvResult.Columns.Count - 1)
            //    {
            //        Packet packet = new Packet { actionType = FormActionType.View };
            //        string accNum = dgvResult.Rows[e.RowIndex].Cells[4].Value.ToString();
            //        string refNum = dgvResult.Rows[e.RowIndex].Cells[1].Value.ToString();
            //        string reqId = dgvResult.Rows[e.RowIndex].Cells[0].Value.ToString();
            //        new frmFingerprintChangeRequest(packet, accNum, refNum, reqId).ShowDialog(this);
            //        Search();
            //    }
            //}
            //catch { }
        }

        private void cbxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            //if (
            //        SessionInfo.userBasicInformation.userType == AgentUserType.Branch
            //    ||
            //        SessionInfo.userBasicInformation.userType == AgentUserType.Admin
            //    )
            {

            }

            //if (SessionInfo.userBasicInformation.userType == AgentUserType.Outlet)
            //{

            //}
        }


        private void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                txtIndividualID.Text = "";
                txtMobileNo.Text = "";
                txtReferenceNumber.Text = "";
                cmbAgentName.SelectedIndex = 0;
                cmbOutlet.SelectedIndex = 0;
                dgvResult.DataSource = null;
                dgvResult.Rows.Clear();
            }
            catch
            {
                //suppressed
            }
        }

        private class BioDataChangeReqSearchResultDtoGrid
        {
            public long requestId { get; set; }
            public string referenceNumber { get; set; }
            public string agentName { get; set; }
            public string subAgentName { get; set; }
            public long? individualId { get; set; }
            public string individualName { get; set; }
            public string mobileNumber { get; set; }

            private BioDataChangeReqSearchResultDto _obj;

            public BioDataChangeReqSearchResultDtoGrid(BioDataChangeReqSearchResultDto obj)
            {
                _obj = obj;
            }

            public BioDataChangeReqSearchResultDto GetModel()
            {
                return _obj;
            }
        }

        private void cmbAgentName_SelectedIndexChanged(object sender, EventArgs e)
        {
            //List of outlets
            {
                if (cmbAgentName.SelectedIndex > 1)
                {
                    List<SubagentList> subAgentList = new AgentServices().GetSubagentsByAgentId((long)cmbAgentName.SelectedValue).Select(o => new SubagentList()
                    {
                        id = o.id,
                        name = o.name,
                        subAgentCode = o.subAgentCode,
                        subAgentNameWithCode = o.name + "  [" + o.subAgentCode + "]"
                    }).ToList();
                    subAgentList.Insert(0, new SubagentList { subAgentNameWithCode = "(Select)" });
                    subAgentList.Insert(1, new SubagentList { subAgentNameWithCode = "(All)" });
                    UtilityServices.fillComboBox(cmbOutlet, new BindingSource { DataSource = subAgentList }, "subAgentNameWithCode", "id");
                }
                else
                {
                    List<SubagentList> subAgentList = new SubAgentServices().GetAllSubAgents().Select(o => new SubagentList()
                    {
                        id = o.id,
                        name = o.name,
                        subAgentCode = o.subAgentCode,
                        subAgentNameWithCode = o.name + "  [" + o.subAgentCode + "]"
                    }).ToList();
                    subAgentList.Insert(0, new SubagentList { subAgentNameWithCode = "(Select)" });
                    subAgentList.Insert(1, new SubagentList { subAgentNameWithCode = "(All)" });
                    UtilityServices.fillComboBox(cmbOutlet, new BindingSource { DataSource = subAgentList }, "subAgentNameWithCode", "id");
                }
            }
        }
    }
}