﻿using MISL.Ababil.Agent.CustomControls;

namespace MISL.Ababil.Agent.Module.Security.UI.FingerprintUI
{
    partial class frmFingerprintAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblOutlet = new System.Windows.Forms.Label();
            this.cmbOutlet = new System.Windows.Forms.ComboBox();
            this.lblIndividualID = new System.Windows.Forms.Label();
            this.txtIndividualID = new System.Windows.Forms.TextBox();
            this.lblReferenceNumber = new System.Windows.Forms.Label();
            this.txtReferenceNumber = new System.Windows.Forms.TextBox();
            this.dgvResult = new System.Windows.Forms.DataGridView();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMobileNo = new System.Windows.Forms.TextBox();
            this.lblAgentName = new System.Windows.Forms.Label();
            this.cmbAgentName = new System.Windows.Forms.ComboBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResult)).BeginInit();
            this.SuspendLayout();
            // 
            // lblOutlet
            // 
            this.lblOutlet.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblOutlet.AutoSize = true;
            this.lblOutlet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblOutlet.ForeColor = System.Drawing.Color.White;
            this.lblOutlet.Location = new System.Drawing.Point(14, 75);
            this.lblOutlet.Name = "lblOutlet";
            this.lblOutlet.Size = new System.Drawing.Size(72, 13);
            this.lblOutlet.TabIndex = 1;
            this.lblOutlet.Text = "Outlet Name :";
            // 
            // cmbOutlet
            // 
            this.cmbOutlet.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbOutlet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOutlet.FormattingEnabled = true;
            this.cmbOutlet.Location = new System.Drawing.Point(92, 72);
            this.cmbOutlet.Name = "cmbOutlet";
            this.cmbOutlet.Size = new System.Drawing.Size(212, 21);
            this.cmbOutlet.TabIndex = 2;
            // 
            // lblIndividualID
            // 
            this.lblIndividualID.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblIndividualID.AutoSize = true;
            this.lblIndividualID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblIndividualID.ForeColor = System.Drawing.Color.White;
            this.lblIndividualID.Location = new System.Drawing.Point(347, 44);
            this.lblIndividualID.Name = "lblIndividualID";
            this.lblIndividualID.Size = new System.Drawing.Size(72, 13);
            this.lblIndividualID.TabIndex = 3;
            this.lblIndividualID.Text = "Individual ID :";
            // 
            // txtIndividualID
            // 
            this.txtIndividualID.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtIndividualID.Location = new System.Drawing.Point(425, 41);
            this.txtIndividualID.Name = "txtIndividualID";
            this.txtIndividualID.Size = new System.Drawing.Size(176, 20);
            this.txtIndividualID.TabIndex = 4;
            // 
            // lblReferenceNumber
            // 
            this.lblReferenceNumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblReferenceNumber.AutoSize = true;
            this.lblReferenceNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblReferenceNumber.ForeColor = System.Drawing.Color.White;
            this.lblReferenceNumber.Location = new System.Drawing.Point(316, 76);
            this.lblReferenceNumber.Name = "lblReferenceNumber";
            this.lblReferenceNumber.Size = new System.Drawing.Size(103, 13);
            this.lblReferenceNumber.TabIndex = 3;
            this.lblReferenceNumber.Text = "Reference Number :";
            // 
            // txtReferenceNumber
            // 
            this.txtReferenceNumber.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtReferenceNumber.Enabled = false;
            this.txtReferenceNumber.Location = new System.Drawing.Point(425, 73);
            this.txtReferenceNumber.Name = "txtReferenceNumber";
            this.txtReferenceNumber.Size = new System.Drawing.Size(176, 20);
            this.txtReferenceNumber.TabIndex = 4;
            // 
            // dgvResult
            // 
            this.dgvResult.AllowUserToAddRows = false;
            this.dgvResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvResult.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvResult.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvResult.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvResult.Location = new System.Drawing.Point(12, 118);
            this.dgvResult.Name = "dgvResult";
            this.dgvResult.RowHeadersVisible = false;
            this.dgvResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvResult.Size = new System.Drawing.Size(963, 378);
            this.dgvResult.TabIndex = 7;
            this.dgvResult.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvResult_CellContentClick);
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(987, 106);
            this.customTitlebar1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(617, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Mobile No. :";
            // 
            // txtMobileNo
            // 
            this.txtMobileNo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtMobileNo.Enabled = false;
            this.txtMobileNo.Location = new System.Drawing.Point(687, 73);
            this.txtMobileNo.Name = "txtMobileNo";
            this.txtMobileNo.Size = new System.Drawing.Size(176, 20);
            this.txtMobileNo.TabIndex = 4;
            // 
            // lblAgentName
            // 
            this.lblAgentName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblAgentName.AutoSize = true;
            this.lblAgentName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.lblAgentName.ForeColor = System.Drawing.Color.White;
            this.lblAgentName.Location = new System.Drawing.Point(14, 45);
            this.lblAgentName.Name = "lblAgentName";
            this.lblAgentName.Size = new System.Drawing.Size(72, 13);
            this.lblAgentName.TabIndex = 1;
            this.lblAgentName.Text = "Agent Name :";
            // 
            // cmbAgentName
            // 
            this.cmbAgentName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbAgentName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAgentName.FormattingEnabled = true;
            this.cmbAgentName.Location = new System.Drawing.Point(92, 41);
            this.cmbAgentName.Name = "cmbAgentName";
            this.cmbAgentName.Size = new System.Drawing.Size(212, 21);
            this.cmbAgentName.TabIndex = 2;
            this.cmbAgentName.SelectedIndexChanged += new System.EventHandler(this.cmbAgentName_SelectedIndexChanged);
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btnSearch.ForeColor = System.Drawing.Color.Black;
            this.btnSearch.Image = global::MISL.Ababil.Agent.Module.Security.Properties.Resources.Search_16__1_;
            this.btnSearch.Location = new System.Drawing.Point(873, 39);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(101, 25);
            this.btnSearch.TabIndex = 8;
            this.btnSearch.Text = "&Search";
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnReset
            // 
            this.btnReset.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnReset.FlatAppearance.BorderSize = 0;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btnReset.ForeColor = System.Drawing.Color.Black;
            this.btnReset.Image = global::MISL.Ababil.Agent.Module.Security.Properties.Resources.Broom_16;
            this.btnReset.Location = new System.Drawing.Point(873, 69);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(101, 25);
            this.btnReset.TabIndex = 8;
            this.btnReset.Text = "&Reset";
            this.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // frmFingerprintAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(987, 508);
            this.Controls.Add(this.cmbAgentName);
            this.Controls.Add(this.lblAgentName);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.dgvResult);
            this.Controls.Add(this.txtReferenceNumber);
            this.Controls.Add(this.txtMobileNo);
            this.Controls.Add(this.txtIndividualID);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblReferenceNumber);
            this.Controls.Add(this.lblIndividualID);
            this.Controls.Add(this.cmbOutlet);
            this.Controls.Add(this.lblOutlet);
            this.Controls.Add(this.customTitlebar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmFingerprintAdmin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fingerprint Update Request";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmChequeRequisitionSearch_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgvResult)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomTitlebar customTitlebar1;
        private System.Windows.Forms.TextBox txtIndividualID;
        private System.Windows.Forms.Label lblIndividualID;
        private System.Windows.Forms.ComboBox cmbOutlet;
        private System.Windows.Forms.Label lblOutlet;
        private System.Windows.Forms.Label lblReferenceNumber;
        private System.Windows.Forms.TextBox txtReferenceNumber;
        private System.Windows.Forms.DataGridView dgvResult;
        private System.Windows.Forms.TextBox txtMobileNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbAgentName;
        private System.Windows.Forms.Label lblAgentName;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnSearch;
    }
}