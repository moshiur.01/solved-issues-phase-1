﻿namespace MISL.Ababil.Agent.Module.Security.UI.FingerprintChangeUI
{
    partial class frmFingerprintChangeByIndividualID
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtToken = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.txtIndividualId = new MISL.Ababil.Agent.CustomControls.CustomTextBox();
            this.btnResendToken = new System.Windows.Forms.Button();
            this.txtReason = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblToken = new System.Windows.Forms.Label();
            this.btnIndDetails = new System.Windows.Forms.Button();
            this.btnFingerprint = new System.Windows.Forms.Button();
            this.pbxIndividualPicture = new System.Windows.Forms.PictureBox();
            this.lblNameValue = new System.Windows.Forms.Label();
            this.lblAccountName = new System.Windows.Forms.Label();
            this.lblMotherNameValue = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblMobileNumberValue = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblFatherNameValue = new System.Windows.Forms.Label();
            this.lblAccountType = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnReject = new System.Windows.Forms.Button();
            this.btnApprove = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.customTitlebar1 = new MISL.Ababil.Agent.CustomControls.CustomTitlebar();
            ((System.ComponentModel.ISupportInitialize)(this.pbxIndividualPicture)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtToken
            // 
            this.txtToken.BackColor = System.Drawing.Color.White;
            this.txtToken.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Default;
            this.txtToken.InputScopeAllowEmpty = false;
            this.txtToken.InputScopeCustomString = null;
            this.txtToken.IsValid = null;
            this.txtToken.Location = new System.Drawing.Point(312, 339);
            this.txtToken.MaxLength = 32767;
            this.txtToken.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtToken.Name = "txtToken";
            this.txtToken.PromptText = "(Type here...)";
            this.txtToken.ReadOnly = false;
            this.txtToken.ShowMandatoryMark = false;
            this.txtToken.Size = new System.Drawing.Size(134, 21);
            this.txtToken.TabIndex = 59;
            this.txtToken.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtToken.ValidationErrorMessage = "Validation Error!";
            this.txtToken.Visible = false;
            // 
            // txtIndividualId
            // 
            this.txtIndividualId.BackColor = System.Drawing.Color.White;
            this.txtIndividualId.Font = new System.Drawing.Font("Segoe UI Semilight", 18F);
            this.txtIndividualId.InputScope = MISL.Ababil.Agent.CustomControls.CustomTextBox.InputScopeType.Numeric;
            this.txtIndividualId.InputScopeAllowEmpty = false;
            this.txtIndividualId.InputScopeCustomString = null;
            this.txtIndividualId.IsValid = null;
            this.txtIndividualId.Location = new System.Drawing.Point(183, 28);
            this.txtIndividualId.MaxLength = 10;
            this.txtIndividualId.MinimumSize = new System.Drawing.Size(0, 21);
            this.txtIndividualId.Name = "txtIndividualId";
            this.txtIndividualId.PromptText = "(Type here...)";
            this.txtIndividualId.ReadOnly = false;
            this.txtIndividualId.ShowMandatoryMark = false;
            this.txtIndividualId.Size = new System.Drawing.Size(293, 40);
            this.txtIndividualId.TabIndex = 46;
            this.txtIndividualId.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtIndividualId.ValidationErrorMessage = "Validation Error!";
            // 
            // btnResendToken
            // 
            this.btnResendToken.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnResendToken.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnResendToken.ForeColor = System.Drawing.Color.White;
            this.btnResendToken.Location = new System.Drawing.Point(451, 337);
            this.btnResendToken.Name = "btnResendToken";
            this.btnResendToken.Size = new System.Drawing.Size(103, 26);
            this.btnResendToken.TabIndex = 62;
            this.btnResendToken.Text = "Resend";
            this.btnResendToken.UseVisualStyleBackColor = false;
            this.btnResendToken.Click += new System.EventHandler(this.btnResendToken_Click);
            // 
            // txtReason
            // 
            this.txtReason.Location = new System.Drawing.Point(310, 272);
            this.txtReason.Multiline = true;
            this.txtReason.Name = "txtReason";
            this.txtReason.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtReason.Size = new System.Drawing.Size(245, 59);
            this.txtReason.TabIndex = 56;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label6.Location = new System.Drawing.Point(253, 274);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 15);
            this.label6.TabIndex = 50;
            this.label6.Text = "Reason :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label5.Location = new System.Drawing.Point(257, 212);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 15);
            this.label5.TabIndex = 50;
            this.label5.Text = "Details :";
            // 
            // lblToken
            // 
            this.lblToken.AutoSize = true;
            this.lblToken.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblToken.Location = new System.Drawing.Point(259, 342);
            this.lblToken.Name = "lblToken";
            this.lblToken.Size = new System.Drawing.Size(45, 15);
            this.lblToken.TabIndex = 50;
            this.lblToken.Text = "Token :";
            this.lblToken.Visible = false;
            // 
            // btnIndDetails
            // 
            this.btnIndDetails.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnIndDetails.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIndDetails.ForeColor = System.Drawing.Color.White;
            this.btnIndDetails.Location = new System.Drawing.Point(309, 206);
            this.btnIndDetails.Name = "btnIndDetails";
            this.btnIndDetails.Size = new System.Drawing.Size(138, 26);
            this.btnIndDetails.TabIndex = 36;
            this.btnIndDetails.Text = "View Individual Details";
            this.btnIndDetails.UseVisualStyleBackColor = false;
            this.btnIndDetails.Click += new System.EventHandler(this.btnIndDetails_Click);
            // 
            // btnFingerprint
            // 
            this.btnFingerprint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnFingerprint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFingerprint.ForeColor = System.Drawing.Color.White;
            this.btnFingerprint.Location = new System.Drawing.Point(309, 238);
            this.btnFingerprint.Name = "btnFingerprint";
            this.btnFingerprint.Size = new System.Drawing.Size(138, 26);
            this.btnFingerprint.TabIndex = 36;
            this.btnFingerprint.Text = "Fingerprint";
            this.btnFingerprint.UseVisualStyleBackColor = false;
            this.btnFingerprint.Click += new System.EventHandler(this.btnFingerprint_Click);
            // 
            // pbxIndividualPicture
            // 
            this.pbxIndividualPicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbxIndividualPicture.Location = new System.Drawing.Point(68, 98);
            this.pbxIndividualPicture.Name = "pbxIndividualPicture";
            this.pbxIndividualPicture.Size = new System.Drawing.Size(118, 134);
            this.pbxIndividualPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxIndividualPicture.TabIndex = 49;
            this.pbxIndividualPicture.TabStop = false;
            // 
            // lblNameValue
            // 
            this.lblNameValue.AutoSize = true;
            this.lblNameValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblNameValue.Location = new System.Drawing.Point(307, 98);
            this.lblNameValue.Name = "lblNameValue";
            this.lblNameValue.Size = new System.Drawing.Size(56, 15);
            this.lblNameValue.TabIndex = 38;
            this.lblNameValue.Text = "<Name>";
            // 
            // lblAccountName
            // 
            this.lblAccountName.AutoSize = true;
            this.lblAccountName.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblAccountName.Location = new System.Drawing.Point(259, 98);
            this.lblAccountName.Name = "lblAccountName";
            this.lblAccountName.Size = new System.Drawing.Size(45, 15);
            this.lblAccountName.TabIndex = 39;
            this.lblAccountName.Text = "Name :";
            // 
            // lblMotherNameValue
            // 
            this.lblMotherNameValue.AutoSize = true;
            this.lblMotherNameValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblMotherNameValue.Location = new System.Drawing.Point(307, 182);
            this.lblMotherNameValue.Name = "lblMotherNameValue";
            this.lblMotherNameValue.Size = new System.Drawing.Size(106, 15);
            this.lblMotherNameValue.TabIndex = 40;
            this.lblMotherNameValue.Text = "<Mother\'sName>";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label3.Location = new System.Drawing.Point(233, 242);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 15);
            this.label3.TabIndex = 43;
            this.label3.Text = "Fingerprint :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label2.Location = new System.Drawing.Point(209, 182);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 15);
            this.label2.TabIndex = 43;
            this.label2.Text = "Mother\'s Name :";
            // 
            // lblMobileNumberValue
            // 
            this.lblMobileNumberValue.AutoSize = true;
            this.lblMobileNumberValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblMobileNumberValue.Location = new System.Drawing.Point(307, 126);
            this.lblMobileNumberValue.Name = "lblMobileNumberValue";
            this.lblMobileNumberValue.Size = new System.Drawing.Size(107, 15);
            this.lblMobileNumberValue.TabIndex = 41;
            this.lblMobileNumberValue.Text = "<MobileNumber>";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.label4.Location = new System.Drawing.Point(207, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 15);
            this.label4.TabIndex = 44;
            this.label4.Text = "Mobile Number :";
            // 
            // lblFatherNameValue
            // 
            this.lblFatherNameValue.AutoSize = true;
            this.lblFatherNameValue.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblFatherNameValue.Location = new System.Drawing.Point(307, 154);
            this.lblFatherNameValue.Name = "lblFatherNameValue";
            this.lblFatherNameValue.Size = new System.Drawing.Size(100, 15);
            this.lblFatherNameValue.TabIndex = 42;
            this.lblFatherNameValue.Text = "<Father\'sName>";
            // 
            // lblAccountType
            // 
            this.lblAccountType.AutoSize = true;
            this.lblAccountType.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblAccountType.Location = new System.Drawing.Point(215, 154);
            this.lblAccountType.Name = "lblAccountType";
            this.lblAccountType.Size = new System.Drawing.Size(89, 15);
            this.lblAccountType.TabIndex = 45;
            this.lblAccountType.Text = "Father\'s Name :";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.btnReject);
            this.flowLayoutPanel1.Controls.Add(this.btnApprove);
            this.flowLayoutPanel1.Controls.Add(this.btnSubmit);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(140, 387);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(369, 39);
            this.flowLayoutPanel1.TabIndex = 37;
            // 
            // btnReject
            // 
            this.btnReject.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReject.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnReject.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReject.ForeColor = System.Drawing.Color.White;
            this.btnReject.Location = new System.Drawing.Point(252, 3);
            this.btnReject.Name = "btnReject";
            this.btnReject.Size = new System.Drawing.Size(114, 32);
            this.btnReject.TabIndex = 36;
            this.btnReject.Text = "Reject";
            this.btnReject.UseVisualStyleBackColor = false;
            this.btnReject.Click += new System.EventHandler(this.btnReject_Click);
            // 
            // btnApprove
            // 
            this.btnApprove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApprove.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnApprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApprove.ForeColor = System.Drawing.Color.White;
            this.btnApprove.Location = new System.Drawing.Point(132, 3);
            this.btnApprove.Name = "btnApprove";
            this.btnApprove.Size = new System.Drawing.Size(114, 32);
            this.btnApprove.TabIndex = 36;
            this.btnApprove.Text = "Approve";
            this.btnApprove.UseVisualStyleBackColor = false;
            this.btnApprove.Click += new System.EventHandler(this.btnApprove_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSubmit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubmit.ForeColor = System.Drawing.Color.White;
            this.btnSubmit.Location = new System.Drawing.Point(12, 3);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(114, 32);
            this.btnSubmit.TabIndex = 36;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = false;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(520, 390);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(114, 32);
            this.btnClose.TabIndex = 36;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnReset.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnReset.FlatAppearance.BorderSize = 0;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.Image = global::MISL.Ababil.Agent.Module.Security.Properties.Resources.Broom_24;
            this.btnReset.Location = new System.Drawing.Point(521, 29);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(33, 39);
            this.btnReset.TabIndex = 23;
            this.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.btnSearch.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Image = global::MISL.Ababil.Agent.Module.Security.Properties.Resources.Search_Filled_24;
            this.btnSearch.Location = new System.Drawing.Point(482, 29);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(33, 39);
            this.btnSearch.TabIndex = 22;
            this.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.label1.Font = new System.Drawing.Font("Segoe UI Semilight", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(31, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(155, 32);
            this.label1.TabIndex = 1;
            this.label1.Text = "Individual ID :";
            // 
            // customTitlebar1
            // 
            this.customTitlebar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.customTitlebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.customTitlebar1.Location = new System.Drawing.Point(0, 0);
            this.customTitlebar1.MinimumSize = new System.Drawing.Size(0, 26);
            this.customTitlebar1.Name = "customTitlebar1";
            this.customTitlebar1.OwnerForm = this;
            this.customTitlebar1.SecondaryTitleLabel = null;
            this.customTitlebar1.ShowTitle = true;
            this.customTitlebar1.Size = new System.Drawing.Size(646, 84);
            this.customTitlebar1.TabIndex = 0;
            this.customTitlebar1.TabStop = false;
            // 
            // frmFingerprintChangeByIndividualID
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(646, 434);
            this.ControlBox = false;
            this.Controls.Add(this.txtToken);
            this.Controls.Add(this.txtIndividualId);
            this.Controls.Add(this.btnResendToken);
            this.Controls.Add(this.txtReason);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblToken);
            this.Controls.Add(this.btnIndDetails);
            this.Controls.Add(this.btnFingerprint);
            this.Controls.Add(this.pbxIndividualPicture);
            this.Controls.Add(this.lblNameValue);
            this.Controls.Add(this.lblAccountName);
            this.Controls.Add(this.lblMotherNameValue);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblMobileNumberValue);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblFatherNameValue);
            this.Controls.Add(this.lblAccountType);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.customTitlebar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmFingerprintChangeByIndividualID";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fingerprint Change By Individual ID";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmFingerprintChangeByIndividualID_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pbxIndividualPicture)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomControls.CustomTitlebar customTitlebar1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnReject;
        private System.Windows.Forms.Button btnApprove;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label lblAccountType;
        private System.Windows.Forms.Label lblFatherNameValue;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblMobileNumberValue;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblMotherNameValue;
        private System.Windows.Forms.Label lblAccountName;
        private System.Windows.Forms.Label lblNameValue;
        private CustomControls.CustomTextBox txtIndividualId;
        private System.Windows.Forms.PictureBox pbxIndividualPicture;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnFingerprint;
        private System.Windows.Forms.Label lblToken;
        private System.Windows.Forms.TextBox txtReason;
        private CustomControls.CustomTextBox txtToken;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnResendToken;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnIndDetails;
    }
}