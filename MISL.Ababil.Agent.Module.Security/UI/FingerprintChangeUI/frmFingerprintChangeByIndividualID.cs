﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.CustomControls;
using MISL.Ababil.Agent.GenericFingerprintServices;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.cis;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.fingerprint;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using MISL.Ababil.Agent.Module.Common.UI;
//using MISL.Ababil.Agent.Module.Common.UI.FingerprintUI;
using MISL.Ababil.Agent.Module.Common.UI.IndividualInformationUI;
using MISL.Ababil.Agent.Module.Common.UI.MessageUI;
using MISL.Ababil.Agent.Module.Common.UI.ProgressUI;
using MISL.Ababil.Agent.Module.Security.Service;
using MISL.Ababil.Agent.Module.Security.UI.FingerprintUI;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.UI.forms.ProgressUI;

namespace MISL.Ababil.Agent.Module.Security.UI.FingerprintChangeUI
{
    public partial class frmFingerprintChangeByIndividualID : CustomForm, FingerprintEventObserver
    {

        private FingerChangeReqDto _fingerChangeReqDto = null;
        private List<BiometricTemplate> _biometricTemplates;
        List<FingerInfo> _fingerInfos = new List<FingerInfo>();
        private byte[] _subagentFingerPrint;
        private Packet _packet;
        private string _individualId;

        public frmFingerprintChangeByIndividualID(Packet packet)
        {

            _packet = packet;
            InitializeComponent();
            lblNameValue.Text = "";
            lblMobileNumberValue.Text = "";
            lblFatherNameValue.Text = "";
            lblMotherNameValue.Text = "";
            if (SessionInfo.userBasicInformation.userCategory == UserCategory.BranchUser)
            {
                btnApprove.Visible = btnReject.Visible = lblToken.Visible = txtToken.Visible = btnResendToken.Visible = true;
                btnSubmit.Visible = false;
                new GUI(this).SetControlState(GUI.CONTROLSTATES.CS_READONLY, new Control[]
                {
                    txtReason
                });
                new GUI(this).SetControlState(GUI.CONTROLSTATES.CS_DISABLE, new Control[]
                {
                    btnFingerprint,
                    txtIndividualId,
                    btnSearch,
                    btnReset
                });
            }
            else
            {
                btnApprove.Visible = btnReject.Visible = lblToken.Visible = txtToken.Visible = btnResendToken.Visible = false;
                btnSubmit.Visible = true;
            }
            if (_packet.actionType != FormActionType.New)
            {
                //GetIndividualInfoByIndividualId(_packet.otherObj.ToString());
                GetIndividualInformationByReferenceNumber(_packet.obj.ToString());
            }
        }

        //public frmFingerprintChangeByIndividualID(Packet packet, long? individualId)
        //{
        //    _packet = packet;
        //    _individualId = individualId.ToString();
        //    InitializeComponent();
        //    if (SessionInfo.userBasicInformation.userCategory == UserCategory.BranchUser)
        //    {
        //        btnApprove.Visible = btnReject.Visible = lblToken.Visible = txtToken.Visible = btnResendToken.Visible = true;
        //        btnSubmit.Visible = false;
        //        new GUI(this).SetControlState(GUI.CONTROLSTATES.CS_READONLY, new Control[]
        //        {
        //            txtReason,
        //        });
        //        new GUI(this).SetControlState(GUI.CONTROLSTATES.CS_DISABLE, new Control[]
        //        {
        //            btnFingerprint,
        //            txtIndividualId,
        //            btnSearch,
        //            btnReset
        //        });
        //    }
        //    else
        //    {
        //        new GUI(this).SetControlState(GUI.CONTROLSTATES.CS_READONLY, new Control[]
        //        {
        //            txtIndividualId
        //        });
        //        txtIndividualId.BackColor = customTitlebar1.BackColor;
        //        txtIndividualId.ForeColor = Color.White;
        //        btnApprove.Visible = btnReject.Visible = lblToken.Visible = txtToken.Visible = btnResendToken.Visible = false;
        //        btnSubmit.Visible = true;
        //    }
        //    if (_packet.actionType != FormActionType.New)
        //    {
        //        //GetIndividualInfoByIndividualId(_packet.otherObj.ToString());
        //        GetIndividualInformationByReferenceNumber(_packet.obj.ToString());
        //    }
        //}

        private void GetIndividualInformationByReferenceNumber(string referenceNumber)
        {
            _fingerChangeReqDto = new FingerprintManagementService().GetFingerChangeReqDtoByReferenceNumber(referenceNumber);
            lblNameValue.Text = _fingerChangeReqDto.individualName;
            lblMobileNumberValue.Text = _fingerChangeReqDto.mobileNo;
            lblFatherNameValue.Text = _fingerChangeReqDto.fatherName;
            lblMotherNameValue.Text = _fingerChangeReqDto.motherName;
            txtReason.Text = _fingerChangeReqDto.reason;
            if (string.IsNullOrEmpty(txtIndividualId.Text))
            {
                txtIndividualId.Text = _packet.otherObj.ToString();
            }
            pbxIndividualPicture.Image = UtilityServices.byteArrayToImage(_fingerChangeReqDto.image);
            if (pbxIndividualPicture.Image == null)
            {
                try
                {
                    pbxIndividualPicture.Image = UtilityServices.byteArrayToImage(new ConsumerServices().GetConsumerPhotoByIndividualId(_fingerChangeReqDto.individualId.ToString()));
                }
                catch (Exception ex)
                {
                    pbxIndividualPicture.Image = null;
                    //suppressed
                    //MsgBox.ShowError(ex.Message);
                }
            }
        }

        private void GetIndividualInfoByIndividualId(string individualId)
        {
            _fingerChangeReqDto = new FingerprintManagementService().GetFingerChangeReqDtoByIndividualID(individualId);
            if (_fingerChangeReqDto == null || _fingerChangeReqDto.individualId == 0)
            {
                MsgBox.showWarning("Individual ID is not valid!");
                return;
            }
            lblNameValue.Text = _fingerChangeReqDto.individualName;
            lblMobileNumberValue.Text = _fingerChangeReqDto.mobileNo;
            lblFatherNameValue.Text = _fingerChangeReqDto.fatherName;
            lblMotherNameValue.Text = _fingerChangeReqDto.motherName;
            txtReason.Text = _fingerChangeReqDto.reason;
            if (string.IsNullOrEmpty(txtIndividualId.Text))
            {
                txtIndividualId.Text = _packet.otherObj.ToString();
            }
            pbxIndividualPicture.Image = UtilityServices.byteArrayToImage(_fingerChangeReqDto.image);
            if (pbxIndividualPicture.Image == null)
            {
                try
                {
                    pbxIndividualPicture.Image = UtilityServices.byteArrayToImage(new ConsumerServices().GetConsumerPhotoByIndividualId(individualId));
                }
                catch (Exception ex)
                {
                    pbxIndividualPicture.Image = null;
                    //suppressed
                    //MsgBox.ShowError(ex.Message);
                }
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

            GetIndividualInfoByIndividualId(txtIndividualId.Text);

            //_fingerChangeReqDto = new IndividualServices().GetFingerChangeReqDtoByIndividualID(txtIndividualId.Text);
            //lblNameValue.Text = _fingerChangeReqDto.individualName;
            //lblMobileNumberValue.Text = _fingerChangeReqDto.mobileNo;
            //lblFatherNameValue.Text = _fingerChangeReqDto.fatherName;
            //lblMotherNameValue.Text = _fingerChangeReqDto.motherName;
            //pbxIndividualPicture.Image = UtilityServices.byteArrayToImage(_fingerChangeReqDto.image);
        }

        private void btnFingerprint_Click(object sender, EventArgs e)
        {
            if (_fingerChangeReqDto == null)
            {
                return;
            }
            frmFingerprintCapture fingerprintCaptureForm = new frmFingerprintCapture(_fingerChangeReqDto.individualName);
            DialogResult dr = fingerprintCaptureForm.ShowDialog();
            if (dr == DialogResult.OK)
            {
                //_biometricTemplates = fingerprintCaptureForm.bioMetricTemplates;
                _fingerInfos = fingerprintCaptureForm.FingerInformation;
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (_fingerChangeReqDto != null && (_fingerInfos != null && _fingerInfos.Count > 0))
                {
                    if (string.IsNullOrEmpty(txtReason.Text))
                    {
                        MsgBox.ShowError("\"Reason\" not found!");
                        return;
                    }
                    FingerprintServiceFactory.FingerprintServiceFactory factory = new FingerprintServiceFactory.FingerprintServiceFactory();
                    FingerprintDevice device = factory.getFingerprintDevice();
                    device.registerEventObserver(this);
                    device.capture();
                }
                else
                {
                    ProgressUIManager.CloseProgress();
                    MsgBox.ShowError("Finger data not found!");
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex.Message);
            }
        }

        private void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                if (_fingerChangeReqDto != null)
                {
                    _fingerChangeReqDto.image = null;
                    _fingerChangeReqDto.token = txtToken.Text;
                    ProgressUIManager.ShowProgress(this);
                    string responseString = new FingerprintManagementService().ApproveFingerprintChangeRequest(_fingerChangeReqDto);
                    ProgressUIManager.CloseProgress();
                    MsgBox.showInformation("Request is approved successfully.\n\nReference Number: " + responseString);
                    this.Close();
                }
                else
                {
                    ProgressUIManager.CloseProgress();
                    MsgBox.ShowError("Finger data not found!");
                }
            }
            catch (Exception ex)
            {
                ProgressUIManager.CloseProgress();
                MsgBox.ShowError(ex.Message);
            }
        }

        private void btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                if (_fingerChangeReqDto != null)
                {
                    _fingerChangeReqDto.image = null;
                    _fingerChangeReqDto.token = txtToken.Text;
                    _fingerChangeReqDto.reason = txtReason.Text;
                    ProgressUIManager.ShowProgress(this);
                    new FingerprintManagementService().RejectFingerprintChangeRequest(_packet.obj.ToString());
                    ProgressUIManager.CloseProgress();
                    MsgBox.showInformation("Request is successfully rejected.");
                    this.Close();
                }
                else
                {
                    ProgressUIManager.CloseProgress();
                    MsgBox.ShowError("Finger data not found!");
                }
            }
            catch (Exception ex)
            {
                ProgressUIManager.CloseProgress();
                MsgBox.ShowError(ex.Message);
            }
        }

        public void FingerPrintEventOccured(FingerprintEvents eventSpec, object EventData)
        {
            if (EventData == null)
            {
                return;
            }

            //byte[] data = ((List<byte[]>)EventData)[0];
            //if (data.Length > 0)

            if (!string.IsNullOrEmpty(EventData?.ToString()))
            {
                try
                {
                    if (string.IsNullOrEmpty(txtReason.Text))
                    {
                        MsgBox.ShowError("\"Reason\" not found!");
                        return;
                    }

                    if (_fingerChangeReqDto != null)
                    {
                        _fingerChangeReqDto.fingerInfos = _fingerInfos;
                        _fingerChangeReqDto.image = null;
                        _fingerChangeReqDto.outletUserTemplate = EventData.ToString();
                        _fingerChangeReqDto.reason = txtReason.Text;
                        ProgressUIManager.ShowProgress(this);
                        string response = new FingerprintManagementService().SubmitFingerprintChangeRequest(_fingerChangeReqDto);
                        ProgressUIManager.CloseProgress();
                        MsgBox.showInformation("Request submitted successfully.\n\nReference Number: " + response);
                        this.Close();
                    }
                    else
                    {
                        ProgressUIManager.CloseProgress();
                        MsgBox.ShowError("Finger data not found!");
                        return;
                    }
                }
                catch (Exception ex)
                {
                    ProgressUIManager.CloseProgress();
                    MsgBox.ShowError(ex.Message);
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmFingerprintChangeByIndividualID_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner = null;
        }

        private void btnResendToken_Click(object sender, EventArgs e)
        {
            try
            {
                if (_fingerChangeReqDto != null)
                {
                    ProgressUIManager.ShowProgress(this);
                    new FingerprintManagementService().ResendToken(_fingerChangeReqDto);
                    ProgressUIManager.CloseProgress();
                    MsgBox.showInformation("Token is resent successfully.");
                }
                else
                {
                    ProgressUIManager.CloseProgress();
                    MsgBox.ShowError("Finger data not found!");
                }
            }
            catch (Exception ex)
            {
                ProgressUIManager.CloseProgress();
                MsgBox.ShowError(ex.Message);
            }
        }

        private void btnIndDetails_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtIndividualId.Text))
            {
                MsgBox.showWarning("Please Insert an Individual Id");
                return;
            }
            if (_fingerChangeReqDto != null)
            {
                new frmIndividualInformation
                (
                    new Packet { actionType = FormActionType.View },
                    new IndividualServices().GetIndividualInfo((int)_fingerChangeReqDto.individualId)
                ).ShowDialog();
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            lblNameValue.Text = "";
            lblFatherNameValue.Text = "";
            lblMotherNameValue.Text = "";
            lblMobileNumberValue.Text = "";
            txtIndividualId.Text = "";
            txtReason.Text = "";
            txtToken.Text = "";
        }
    }
}