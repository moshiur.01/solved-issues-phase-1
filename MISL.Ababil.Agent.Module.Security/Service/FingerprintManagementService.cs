﻿using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.common;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Module.Security.Models;
using MISL.Ababil.Agent.Services.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Infrastructure.Models.dto;

namespace MISL.Ababil.Agent.Module.Security.Service
{
    public class FingerprintManagementService
    {
        public BioDataChangeReqDto GetBioDataChangeReqDtoList(string accountNumber, string referenceNumber)
        {
            WebClientCommunicator<object, BioDataChangeReqDto> webClientCommunicator = new WebClientCommunicator<object, BioDataChangeReqDto>();

            //if (SessionInfo.userBasicInformation.userType == AgentUserType.Outlet)
            if (SessionInfo.userBasicInformation.userCategory == UserCategory.SubAgentUser)
            {
                return webClientCommunicator.GetResult(null, "resources/biodatachange/reqdto/" + accountNumber);
            }
            else
            {
                return webClientCommunicator.GetResult(null, "resources/biodatachange/reqdto/" + accountNumber + "/" + referenceNumber);
            }
        }

        public string UpdateBioDataChangeReqDtoList(BioDataChangeReqDto _bioDataChangeReqDto)
        {
            WebClientCommunicator<BioDataChangeReqDto, string> webClientCommunicator = new WebClientCommunicator<BioDataChangeReqDto, string>();
            string retVal = webClientCommunicator.GetPostResult(_bioDataChangeReqDto, "resources/biodatachange/request");
            return "Request successfully sent. Reference No. : " + retVal;
        }

        public List<BioDataChangeReqSearchResultDto> FingerprintDataChangeReqSearchDtoList(FingerprintChangeReqSearchDto fingerprintChangeReqSearchDto)
        {
            WebClientCommunicator<FingerprintChangeReqSearchDto, List<BioDataChangeReqSearchResultDto>> webClientCommunicator = new WebClientCommunicator<FingerprintChangeReqSearchDto, List<BioDataChangeReqSearchResultDto>>();
            return webClientCommunicator.GetPostResult(fingerprintChangeReqSearchDto, "resources/biodatachange/search");

        }

        public string SubmitTokens(BioDataChangeReqDto _bioDataChangeReqSearchDto)
        {
            WebClientCommunicator<BioDataChangeReqDto, string> webClientCommunicator = new WebClientCommunicator<BioDataChangeReqDto, string>();
            return webClientCommunicator.GetPostResult(_bioDataChangeReqSearchDto, "resources/biodatachange/accept");
        }

        public void ResendToken(BioChangeDto bioChangeDto)
        {
            WebClientCommunicator<BioChangeDto, object> webClientCommunicator = new WebClientCommunicator<BioChangeDto, object>();
            webClientCommunicator.GetPostResult(bioChangeDto, "resources/biodatachange/rsendtoken/identity");
        }

        public void ResendToken(FingerChangeReqDto fingerChangeReqDto)
        {
            //ToDo: ______________WORK______________[DONE]
            WebClientCommunicator<FingerChangeReqDto, object> webClientCommunicator = new WebClientCommunicator<FingerChangeReqDto, object>();
            webClientCommunicator.GetPostResult(fingerChangeReqDto, "resources/biodatachange/rsendtoken");
        }

        public string Reject(string id)
        {
            WebClientCommunicator<object, string> webClientCommunicator = new WebClientCommunicator<object, string>();
            return webClientCommunicator.GetResult("resources/biodatachange/cancel/" + id);
        }

        public FingerChangeReqDto GetFingerChangeReqDtoByReferenceNumber(string referenceNumber)
        {
            //ToDo: ______________WORK______________
            WebClientCommunicator<object, FingerChangeReqDto> webClientCommunicator = new WebClientCommunicator<object, FingerChangeReqDto>();
            return webClientCommunicator.GetResult("resources/biodatachange/requestinfo/" + referenceNumber);
        }

        public FingerChangeReqDto GetFingerChangeReqDtoByIndividualID(string id)
        {
            //ToDo: ______________WORK______________
            return
                new WebClientCommunicator<object, FingerChangeReqDto>().GetResult(
                    "resources/individualinfo/individual/" + id);
        }

        public string ApproveFingerprintChangeRequest(FingerChangeReqDto fingerChangeReqDto)
        {
            //ToDo: ______________WORK______________
            return
                new WebClientCommunicator<FingerChangeReqDto, string>().GetPostResult(
                    fingerChangeReqDto,
                    "resources/biodatachange/approve");
        }

        public string RejectFingerprintChangeRequest(string referenceNumber)
        {
            //ToDo: ______________WORK______________
            WebClientCommunicator<object, string> webClientCommunicator = new WebClientCommunicator<object, string>();
            return webClientCommunicator.GetResult("resources/biodatachange/reject/" + referenceNumber);
        }

        public string SubmitFingerprintChangeRequest(FingerChangeReqDto fingerChangeReqDto)
        {
            //ToDo: ______________WORK______________
            WebClientCommunicator<FingerChangeReqDto, string> webClientCommunicator = new WebClientCommunicator<FingerChangeReqDto, string>();
            return webClientCommunicator.GetPostResult(fingerChangeReqDto, "resources/biodatachange/request");
        }
    }
}