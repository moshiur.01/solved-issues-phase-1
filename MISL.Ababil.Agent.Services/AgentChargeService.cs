﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Communication;
using MISL.Ababil.Agent.Infrastructure.Models.dto;

namespace MISL.Ababil.Agent.Services
{
    public class AgentChargeService
    {
        public List<AgentChargeSearchResultDto> GetAgentIncomeSearchResultList(AgentChargeSearchDto agentIncomeSearchDto)
        {
            return (new AgentChargeCom()).GetAgentChargeSearchResultList(agentIncomeSearchDto);
        }
    }
}