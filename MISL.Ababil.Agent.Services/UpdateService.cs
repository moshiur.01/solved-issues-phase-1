﻿using MISL.Ababil.Agent.Communication;
using MISL.Ababil.Agent.Infrastructure.Behavior;

namespace MISL.Ababil.Agent.Services
{
    public class UpdateService
    {
        public static UpdateType CheckUpdateType ()
        {
            UpdateType updateType = UpdateType.None;
            if (IsUpdateaAvailable())
            {
                updateType = UpdateType.Normal;
                if (UpdateCom.IsUpdateForced())
                {
                    updateType = UpdateType.Enforced;
                }
            }
            return updateType;
        }


        public static bool InitiateUpdate(ISoftwareUpdateObserver observer)
        {
            if (observer == null) return false;
            UpdateCom.ClearUpdateObservers();
            UpdateCom.RegisterUpdateObserver(observer);
            return UpdateCom.InitiateUpdate();
        }

        public static bool IsUpdateaAvailable()
        {
            return new UpdateCom().IsUpdateaAvailable();
        }

    }

    public enum UpdateType
    {
        Normal,
        Enforced,
        None
    }
}