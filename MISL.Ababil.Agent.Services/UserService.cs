﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using MISL.Ababil.Agent.Communication;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.user;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using Newtonsoft.Json;
using MISL.Ababil.Agent.Infrastructure.Models.reports;

namespace MISL.Ababil.Agent.Services
{
    public class UserService
    {
        UserCom userCom = new UserCom();
        public static ServiceResult CreateBranchUser(BranchUser user)
        {
            ServiceResult serviceResult = ServiceResult.CreateServiceResult();
            UserCom userCom = new UserCom();
            string json = JsonConvert.SerializeObject(user); //new JavaScriptSerializer().Serialize(user);
            string responseString;
            serviceResult.Success = false;
            try
            {
                responseString = userCom.CreateBranchUser(json);
                serviceResult.Message = responseString;
                serviceResult.Success = true;
            }
            catch (Exception ex)
            {
                serviceResult.Message = ex.Message;
                //throw;
            }
            return serviceResult;
        }

        public AgentUserSearchResultDto GetUserDetailsByUserName(string username)
        {
            try
            { return userCom.GetUserDetailsByUserName(username); }
            catch (Exception exp) { return null; }
        }

        public List<SubAgentUserLimitDto> GetUserBasicInformation(string outletid)
        {
            return userCom.GetOutletUserInformation(outletid);
        
        }
        public List<BankUserReportDto> GetBankUserInformation(BankUserSearchDto bankUserSearchDto)
        {
            return userCom.GetBanktUserInformation(bankUserSearchDto);

        }
        public List<UserIdWiseAccApproveResultDto> GetUserIdWiseAccApproveInfo(UserIdWiseAccApproveSearchDto userIdWiseAccApproveSearchDto)
        {
            return userCom.GetUserIdWiseAccApproveInfo(userIdWiseAccApproveSearchDto);

        }
        public List<AgentUserReportDto> getAgentUserInfoReport(UserSearchDtoForAgent userSearchDtoForAgent)
        {
            return userCom.GetAgentUserInformation(userSearchDtoForAgent);

        }


        //public List<OutletUserInfoReportResultDto> GetUserBasicInformation(string outletid)
        //{
        //    return userCom.GetOutletUserInformation(outletid);
        //}
        public ServiceResult SaveUser(AgentUserDto userDto)
        {
            string responseString;
            ServiceResult serviceResult = ServiceResult.CreateServiceResult();

            try
            {
                responseString = userCom.SaveUser(userDto);
                serviceResult.Message = responseString;
                serviceResult.Success = true;
            }
            catch (Exception ex)
            { serviceResult.Message = ex.Message; }

            return serviceResult;
        }
        public ServiceResult GetUserList(AgentUserSearchDto searchDto)
        {
            ServiceResult serviceResult = ServiceResult.CreateServiceResult();
            serviceResult.ReturnedObject = "";
            try
            {
                serviceResult.ReturnedObject = userCom.GetUserList(searchDto);
                if (string.IsNullOrEmpty(serviceResult.ReturnedObject.ToString()))
                {
                    serviceResult.ReturnedObject = "";
                    serviceResult.Message = "User list could not be fetched successfully, please check connectivity and inform Bank Administration";
                }
                else serviceResult.Success = true;
            }
            catch (Exception ex)
            { serviceResult.Message = ex.Message; }

            return serviceResult;
        }
        public ServiceResult GetUserDetailsByUserId(AgentUserDetilSearchDto searchDto)
        {
            ServiceResult serviceResult = ServiceResult.CreateServiceResult();
            serviceResult.ReturnedObject = "";
            try
            {
                serviceResult.ReturnedObject = userCom.GetUserDetailsByUserId(searchDto);
                if (string.IsNullOrEmpty(serviceResult.ReturnedObject.ToString()))
                {
                    serviceResult.ReturnedObject = "";
                    serviceResult.Message = "User detail could not be fetched successfully, please check connectivity and inform Bank Administration";
                }
                else serviceResult.Success = true;
            }
            catch (Exception ex)
            { serviceResult.Message = ex.Message; }

            return serviceResult;
        }
        public ServiceResult UpdateUser(AgentUserDto userDto)
        {
            string responseString;
            ServiceResult serviceResult = ServiceResult.CreateServiceResult();

            try
            {
                responseString = userCom.UpdateUser(userDto);
                serviceResult.Message = responseString;
                serviceResult.Success = true;
            }
            catch (Exception ex)
            { serviceResult.Message = ex.Message; }

            return serviceResult;
        }
        public ServiceResult ResetUserCredential(ResetPasswordDto resetPassDto)
        {
            string responseString;
            ServiceResult serviceResult = ServiceResult.CreateServiceResult();

            try
            {
                responseString = userCom.ResetUserCredential(resetPassDto);
                serviceResult.Message = responseString;
                serviceResult.Success = true;
            }
            catch (Exception ex)
            { serviceResult.Message = ex.Message; }

            return serviceResult;
        }
    }
}