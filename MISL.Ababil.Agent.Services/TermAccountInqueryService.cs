﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Communication;
using MISL.Ababil.Agent.Infrastructure.Models.dto;

namespace MISL.Ababil.Agent.Services
{
    public class TermAccountInqueryService
    {
        public TermAccInforamationDto GetTermAccountInformationByAccountNo(string accountNumber)
        {
            return (new TermAccountInqueryCom()).GetTermAccountInformationByAccountNo(accountNumber);
        }
    }
}