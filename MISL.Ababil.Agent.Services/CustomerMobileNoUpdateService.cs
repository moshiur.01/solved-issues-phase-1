﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Communication;
using MISL.Ababil.Agent.Infrastructure.Models.dto;

namespace MISL.Ababil.Agent.Services
{
    public class CustomerMobileNoUpdateService
    {
        public string cusMobNoUpdate(MobileNoChangeRequestDto mobileNoChangeRequestDto)
        {
            CustomerMobileNoUpdateCom customerMobileNoUpdate = new CustomerMobileNoUpdateCom();
            return customerMobileNoUpdate.requestCustomerMobileMobileNoUpdate(mobileNoChangeRequestDto);
        }
    }
}
