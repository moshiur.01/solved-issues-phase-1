﻿using System.Collections.Generic;
using MISL.Ababil.Agent.Communication;
using MISL.Ababil.Agent.Infrastructure.Models.dto;

namespace MISL.Ababil.Agent.Services
{
    public class AgentIncomeService
    {
        public List<AgentIncomeSearchResultDto> GetAgentIncomeSearchResultList(AgentIncomeSearchDto agentIncomeSearchDto)
        {
            return (new AgentIncomeCom()).GetAgentIncomeSearchResultList(agentIncomeSearchDto);
        }
    }
}