﻿using MISL.Ababil.Agent.Communication;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.agent;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.configuration;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.termaccount;
using MISL.Ababil.Agent.Services.Communication;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.ssp;

namespace MISL.Ababil.Agent.Services
{
    public class ProductMappingServices
    {
        ProductMappingCom objProductMappingCom = new ProductMappingCom();

        //public List<DepositMapping> getDepositMappingInfo()
        //{
        //    return objProductMappingCom.getDepositMappingInfo();
        //}
        public List<ProductConfigDto> getDepositMappingInfo()
        {
            return objProductMappingCom.getDepositMappingInfo();
        }
        public string deleteDepositMap(ProductConfigDto depositMap)
        {
            return objProductMappingCom.deleteDepositMap(depositMap);
        }
        public string saveDepositMap(List<ProductConfigDto> depositMapList)
        {
            return objProductMappingCom.saveDepositMap(depositMapList);
        }



        public List<ProductConfigDto> getSSPMappingInfo()
        {
            return objProductMappingCom.getSSPMappingInfo();
        }

        public List<TermProductType> GetSSPProducts()
        {
            return objProductMappingCom.GetSSPProducts();
        }

        public string deleteSSPMap(ProductConfigDto sspMap)
        {
            return objProductMappingCom.deleteSSPMap(sspMap);
        }
        public string saveSSPMapList(List<ProductConfigDto> sspMapList)
        {
            return objProductMappingCom.saveSSPMap(sspMapList);
        }



        public List<ProductConfigDto> getMTDRMappingInfo()
        {
            return objProductMappingCom.getMTDRMappingInfo();
        }
        public string deleteMTDRMap(ProductConfigDto mtdrMap)
        {
            return objProductMappingCom.deletMTDRMap(mtdrMap);
        }
        public string saveMTDRMap(List<ProductConfigDto> mtdrMapList)
        {
            return objProductMappingCom.saveMTDRMap(mtdrMapList);
        }



        public static ServiceResult GetProductsList(ProductType productType)
        {
            var serviceResult = ServiceResult.CreateServiceResult();
            serviceResult.ReturnedObject = "";

            try
            {
                serviceResult.ReturnedObject = ProductMappingCom.GetProductsList(productType);
                if (string.IsNullOrEmpty(serviceResult.ReturnedObject.ToString()))
                {
                    serviceResult.ReturnedObject = "";
                    serviceResult.Message = "Products could not be fetched successfully, please check connectivity and inform Bank Administration";
                }
                else
                {
                    serviceResult.Success = true;
                    serviceResult.Message = serviceResult.ReturnedObject.ToString();
                }
            }
            catch (Exception exception)
            {
                serviceResult.Message = exception.Message;
            }

            return serviceResult;
        }
        public static ServiceResult GetSSPInstallmentListByProduct(CbsProduct product)
        {
            var serviceResult = ServiceResult.CreateServiceResult();
            serviceResult.ReturnedObject = "";

            try
            {
                serviceResult.ReturnedObject = ProductMappingCom.GetSSPInstallmentListByProduct(product);
                if (string.IsNullOrEmpty(serviceResult.ReturnedObject.ToString()))
                {
                    serviceResult.ReturnedObject = "";
                    serviceResult.Message = "Installments could not be fetched successfully, please check connectivity and inform Bank Administration";
                }
                else
                {
                    serviceResult.Success = true;
                    serviceResult.Message = serviceResult.ReturnedObject.ToString();
                }
            }
            catch (Exception exception)
            {
                serviceResult.Message = exception.Message;
            }

            return serviceResult;
        }

        public void SaveSSPMapping(ProductConfigDto sspMap)
        {
            new WebClientCommunicator<ProductConfigDto, object>().GetPostResult(sspMap, "resources/agentproduct/productconfig");
        }

        public List<SspInstallment> GetAllInstallmentByProductID(long? id)
        {
            return new WebClientCommunicator<object, List<SspInstallment>>().GetResult($"resources/agentproduct/cbssspinstallsnotinagent/{id}");
        }

        public List<TermProductType> GetMTDRProducts()
        {
            return new ProductMappingCom().GetMTDRProducts();
        }

            #region Rejected
            //public static MISL.Ababil.Agent.Infrastructure.ServiceResult GetAgentCommissionInformaiton(AccountSearchDto requestDto)
            //{
            //    var serviceResult = MISL.Ababil.Agent.Infrastructure.ServiceResult.CreateServiceResult();
            //    serviceResult.ReturnedObject = "";

            //    try
            //    {
            //        serviceResult.ReturnedObject = AgentCom.GetAgentCommissionInformation(requestDto);
            //        if (string.IsNullOrEmpty(serviceResult.ReturnedObject.ToString()))
            //        {
            //            serviceResult.ReturnedObject = "";
            //            serviceResult.Message = "Agent Commission Info could not be fetched successfully, please check connectivity and inform Bank Administration";
            //        }
            //        else
            //        {
            //            serviceResult.Success = true;
            //            serviceResult.Message = serviceResult.ReturnedObject.ToString();
            //        }
            //    }
            //    catch (Exception exception)
            //    {
            //        serviceResult.Message = exception.Message;
            //    }

            //    return serviceResult;
            //}
            #endregion

        }
}