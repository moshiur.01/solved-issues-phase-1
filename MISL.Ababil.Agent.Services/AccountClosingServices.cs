﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Communication;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.dto;

namespace MISL.Ababil.Agent.Services
{
    public class AccountClosingServices
    {

        public string SubmitAccountClosingRequest(AccountClosingRequestDto accountClosingRequest)
        {
            return new AccountClosingCom().SubmitAccountClosingRequest(accountClosingRequest);
        }

        public void CloseDepositAccount(DepositAccountClosing depositAccountClosing)
        {
            new AccountClosingCom().CloseDepositAccount(depositAccountClosing);
        }

        public void RejectAccountClosingRequest(AccountClosingRequestDto accountClosingRequest)
        {
            new AccountClosingCom().RejectAccountClosingRequest(accountClosingRequest);
        }

        public void CorrectAccountClosingRequest(AccountClosingRequestDto accountClosingRequest)
        {
            new AccountClosingCom().CorrectAccountClosingRequest(accountClosingRequest);
        }

        public List<AccountClosingSearchResultDto> GetAccountClosingRequests(AccountClosingSearchDto accountClosingSearchDto)
        {
            return new AccountClosingCom().GetAccountClosingRequests(accountClosingSearchDto);
        }

        public AccountClosingRequestDto GetAccountClosingRequestDtoByRefNumber(string refNo)
        {
            return new AccountClosingCom().GetAccountClosingRequestDtoByRefNumber(refNo);
        }

        public void CloseTermAccount(TermAccountClosingDto termAccountClosingDto)
        {
            new AccountClosingCom().CloseTermAccount(termAccountClosingDto);
        }

        public void AcceptAccountClosingRequest(AccountClosingRequestDto accountClosingRequest)
        {
            new AccountClosingCom().AcceptAccountClosingRequest(accountClosingRequest);
        }
    }
}