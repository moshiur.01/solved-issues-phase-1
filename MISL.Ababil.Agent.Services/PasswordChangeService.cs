﻿using MISL.Ababil.Agent.Communication;
using MISL.Ababil.Agent.Infrastructure.Models.dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Services
{
    public class PasswordChangeService
    {
        public string savePasswordChange(passwordChangeInfo pcInfo)
        {
            PasswordChangeCom passwordChangeCom = new PasswordChangeCom();

            string responseString = passwordChangeCom.savePasswordChange(pcInfo);

            return responseString;
        }
        public string saveChangedPasswordWithFingerPrint(ChangeIdentity changeIdentity)
        {
            PasswordChangeCom passwordChangeCom = new PasswordChangeCom();

            string responseString = passwordChangeCom.saveChangedPasswordWithFingerPrint(changeIdentity);

            return responseString;
        }
        public string saveChangedFingerPrint(ChangeIdentity changeIdentity)
        {
            PasswordChangeCom passwordChangeCom = new PasswordChangeCom();
            string responseString = passwordChangeCom.saveChangedFingerPrint(changeIdentity);
            return responseString;
        }
    }
}
