﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.Services
{
    public class TestingUtilityServices
    {
        public List<string> Names = new List<string>() { "Rohim", "Korim", "Abul", "Ali" };
        private int _i = 0;

        public int i()
        {
            return _i >= Names.Count ? _i = 0 : _i++;
        }

        public bool IsDevMode()
        {
            return ConfigurationManager.AppSettings["mode"].ToString() == "test" ? true : false;
        }

        public string GetName()
        {
            return Names[i()].ToUpper();
        }

        public string GetEmailAddress()
        {
            return Names[i()].ToLower() + "@" + Names[i()].ToLower() + ".com";
        }

        public string GetAddressLine1()
        {
            return "Sample Address Line One " + ((DateTime.Now.Second * DateTime.Now.Minute * DateTime.Now.Hour) + DateTime.Now.Millisecond).ToString();
        }

        public string GetAddressLine2()
        {
            return "Sample Address Line Two " + ((DateTime.Now.Second * DateTime.Now.Minute * DateTime.Now.Hour) + DateTime.Now.Millisecond).ToString();
        }

        //public string GetMobileNumber()
        //{
        //    return "01919000756";
        //}


    }
}