﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MISL.Ababil.Agent.Communication;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models;
using MISL.Ababil.Agent.Infrastructure.Models.domain.models.account;
using MISL.Ababil.Agent.Infrastructure.Models.dto;

namespace MISL.Ababil.Agent.Services
{
    public class DepositAccountServices
    {
      

        

        public string ChangeApplicationStatus(ApplicationStatusChangeDto applicationStatusChangeDto, ApplicationStatus approved)
        {
            return new DepositAccountCom().ChangeApplicationStatus(applicationStatusChangeDto, approved);
        }

        public AccountOperatorDto GetAccountOperatorsByAccountNo(string accountNumber)
        {
            return new DepositAccountCom().GetAccountOperatorsByAccountNo(accountNumber);
        }
    }
}