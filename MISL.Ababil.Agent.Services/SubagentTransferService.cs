﻿using MISL.Ababil.Agent.Communication;

namespace MISL.Ababil.Agent.Services
{
    public class SubagentTransferService
    {
        public void TransferSubagentUser(string username, string outletId)
        {
            new SubagentTransferCom().TransferSubagentUser(username, outletId);
        }
    }
}