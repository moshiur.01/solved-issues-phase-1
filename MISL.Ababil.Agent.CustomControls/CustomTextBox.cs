﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Windows.Forms;
using MISL.Ababil.Agent.UI.forms.CustomControls;

namespace MISL.Ababil.Agent.CustomControls
{
    public partial class CustomTextBox : UserControl
    {
        public CustomTextBox()
        {
            InitializeComponent();
            base.AutoScaleMode = AutoScaleMode.None;
        }

        private Panel _panel = new Panel();
        //private Label _promptLabel = new Label();
        //private bool _showPromptText = false;
        private string _promptText = "(Type here...)";
        private bool? _isValid = null;
        private MandatoryMark _mandatoryMark = new MandatoryMark();
        private bool _showMandatoryMark = false;
        private string _validationErrorMessage;
        //private bool _inputScopeAllowEmpty = true;
        private InputScopeType inputScope;
        private bool _readOnly = false;
        private Font _font;

        protected override void OnVisibleChanged(EventArgs e)
        {
            try
            {
                //_panel.Visible = this.Visible;
                if (this.Visible == false)
                {
                    //_promptLabel.Visible = false;
                    _mandatoryMark.Visible = false;
                    _panel.Visible = false;
                }
                else
                {
                    _panel.Visible = true;
                    this.BringToFront();
                    if (ShowMandatoryMark == true)
                    {
                        _mandatoryMark.Visible = true;
                    }
                }
                _panel.SendToBack();
            }
            catch { }
            base.OnVisibleChanged(e);
        }

        public bool ShowMandatoryMark
        {
            get
            {

                return _mandatoryMark.Visible = _showMandatoryMark;
            }
            set
            {
                _mandatoryMark.Visible = _showMandatoryMark = value;
            }

        }

        public string ValidationErrorMessage
        {

            get
            {
                if (string.IsNullOrEmpty(_validationErrorMessage))
                    return "Validation Error!";
                else
                    return _validationErrorMessage;

            }
            set { _validationErrorMessage = value; }
        }

        public int MaxLength
        {
            get { return textBox1.MaxLength; }
            set { textBox1.MaxLength = value; }
        }

        public bool UpperCaseOnly
        {

            get
            {
                if (textBox1?.CharacterCasing == CharacterCasing.Upper)
                {
                    return true;
                }
                return false;
            }
            set
            {
                if (textBox1 != null && value == true)
                {
                    textBox1.CharacterCasing = CharacterCasing.Upper;
                }
                else
                {
                    textBox1.CharacterCasing = CharacterCasing.Normal;
                }

            }
        }

        public InputScopeType InputScope
        {
            get
            {
                return inputScope;
            }
            set
            {
                inputScope = value;

                switch (inputScope)
                {
                    case InputScopeType.Default:
                    case InputScopeType.Numeric:
                    case InputScopeType.Custom:
                    case InputScopeType.Alphanumeric:
                        textBox1.MaxLength = 32767;
                        break;
                    case InputScopeType.MobileNumber:
                        textBox1.MaxLength = 11;
                        break;
                    case InputScopeType.NationalId:
                        textBox1.MaxLength = 17;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public enum InputScopeType
        {
            Default,
            Alphanumeric,
            Numeric,
            MobileNumber,
            NationalId,
            Custom
        }

        public bool InputScopeAllowEmpty { get; set; }

        public string InputScopeCustomString { get; set; }

        public HorizontalAlignment TextAlign
        {
            get { return textBox1.TextAlign; }
            set { textBox1.TextAlign = value; }
        }

        public bool? IsValid { get; set; }

        //[Bindable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public override string Text
        {
            get
            {
                CheckToShowTypeHere(textBox1.Text);
                return textBox1.Text;
            }
            set
            {
                textBox1.Text = value;
                CheckToShowTypeHere(textBox1.Text);
            }
        }

        //[EditorBrowsable(EditorBrowsableState.Always)]
        //[Browsable(true)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        //public new Font Font
        //public Font Fonts

        //[Bindable(true),
        // Browsable(true),
        // Category("Appearance"),
        // Description("Get or Sets the Font of the Text being displayed."),
        // DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        // EditorBrowsable(EditorBrowsableState.Always)]
        //public override Font Font
        //{
        //    get
        //    {
        //        return base.Font;
        //    }
        //    set
        //    {
        //        base.Font = value;
        //        Invalidate();
        //        OnFontChanged(EventArgs.Empty);
        //    }
        //}


        public new Font Font
        {
            get
            {
                return base.Font;
            }
            set
            {
                base.Font = value;
                textBox1.Font = value;
            }
        }


        //public Font Fonts
        //{
        //    get
        //    {

        //        return textBox1.Font;

        //    }
        //    set
        //    {
        //        textBox1.Font = value;
        //    }
        //}

        //protected override void OnFontChanged(EventArgs e)
        //{
        //    try
        //    {
        //        textBox1.Font = Font;
        //    }
        //    catch (Exception)
        //    {

        //    }
        //    base.OnFontChanged(e);
        //}

        private void CheckToShowTypeHere(string value)
        {
            //if (ReadOnly == false)
            //{
            //    if (string.IsNullOrEmpty(value))
            //    {
            //        if (!textBox1.Focused && !this.Focused)
            //        {
            //            _promptLabel.Visible = true;
            //            _promptLabel.BringToFront();
            //        }
            //        else
            //        {
            //            _promptLabel.Visible = false;
            //        }
            //    }
            //    else
            //    {
            //        _promptLabel.Visible = false;
            //    }
            //}
            //else
            //{
            //    _promptLabel.Visible = false;
            //}
        }

        public string PromptText
        {
            get { return _promptText; }
            set { _promptText = value; }
        }

        public bool ReadOnly
        {
            get
            {

                return _readOnly;
            }
            set
            {
                if (ReadOnly)
                {
                    //_promptLabel.Visible = false;
                }
                _readOnly = value;
                textBox1.ReadOnly = _readOnly;
                if (_readOnly)
                {
                    textBox1.BackColor = this.BackColor = Color.FromArgb(240, 240, 240);
                }
                else
                {
                    textBox1.BackColor = this.BackColor = Color.White;
                }
            }
        }

        protected override void InitLayout()
        {
            try
            {
                textBox1.BringToFront();
                ResizeBaseToFitTextBox();
                if (_panel != null)
                {
                    UpdatePanelSizeAndLocation();

                    //_promptLabel.Text = "(Type here...)";

                    this.Parent.Controls.Add(_panel);
                    _panel.SendToBack();
                    textBox1.BringToFront();
                    //base.Parent.Controls.Add(_promptLabel);
                    base.Parent.Controls.Add(_mandatoryMark);

                    _mandatoryMark.TabStop = false;


                    //_promptLabel.Cursor = Cursors.IBeam;
                    //_promptLabel.AutoSize = true;
                    //_promptLabel.BackColor = textBox1.BackColor;
                    //_promptLabel.ForeColor = Color.Gray;

                    _panel.BackColor = Color.LightGray;

                    //textBox Events
                    {
                        textBox1.KeyDown += TextBox1_KeyDown;
                        textBox1.KeyPress += TextBox1_KeyPress;
                        textBox1.KeyUp += TextBox1_KeyUp;
                        textBox1.TextChanged += TextBox1_TextChanged;
                        textBox1.Leave += TextBox1_Leave;
                        textBox1.Enter += TextBox1_Enter;
                        textBox1.Click += TextBox1_Click;

                    }

                    //Enter To Focus Next Control
                    {
                        textBox1.KeyUp += TextBox1_KeyUpToFocusNextControl;
                    }

                    //promptLabel
                    {
                        //_promptLabel.Click += _promptLabel_Click;
                    }

                    textBox1.Text = Text;
                    CheckToShowTypeHere(textBox1.Text);


                    //CheckSelIndexExt();
                    _panel.SendToBack();
                }
            }
            catch (Exception ex)
            {
                //suppressed
            }
        }

        private void ResizeBaseToFitTextBox()
        {
            this.Height = textBox1.Height + (textBox1.Top * 2);
        }

        private void TextBox1_KeyUpToFocusNextControl(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void _promptLabel_Click(object sender, EventArgs e)
        {
            //_promptLabel.Visible = false;
            textBox1.Focus();
        }

        private void TextBox1_Click(object sender, EventArgs e)
        {
            base.OnClick(e);
        }

        private void TextBox1_Enter(object sender, EventArgs e)
        {
            base.OnEnter(e);
        }

        private void TextBox1_Leave(object sender, EventArgs e)
        {
            CheckToShowTypeHere(textBox1.Text);
            base.OnLeave(e);
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            IsValid = null;
            ClearAllErrorMessages(true);
            CheckToShowTypeHere(textBox1.Text);
            base.OnTextChanged(e);
        }

        private void TextBox1_KeyUp(object sender, KeyEventArgs e)
        {
            base.OnKeyUp(e);
        }

        private void TextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            base.OnKeyPress(e);
        }

        private void TextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            base.OnKeyDown(e);
        }

        private void UpdatePanelSizeAndLocation()
        {
            try
            {
                ResizeBaseToFitTextBox();
                if (_panel.Visible == true)
                {
                    _panel.Top = base.Location.Y - 2;
                    _panel.Left = base.Location.X - 2;
                    _panel.Height = this.Height + 4;
                    _panel.Width = base.Width + 4;

                    //_promptLabel.Font = textBox1.Font;
                    //_promptLabel.Top = this.Top + textBox1.Top - 1;
                    //_promptLabel.Left = this.Left + textBox1.Left;

                    _mandatoryMark.Top = base.Top;
                    _mandatoryMark.Left = base.Left + base.Width + 8;

                    try
                    {
                        //_panel.Visible = Visible;
                        _panel.SendToBack();
                        textBox1.BringToFront();
                        if (Visible == false)
                        {
                            //_promptLabel.Visible = false;
                            _mandatoryMark.Visible = false;
                        }
                    }
                    catch { }

                    CheckToShowTypeHere(textBox1.Text);
                }
            }
            catch (Exception ex)
            {
                //suppressed
            }
        }


        protected override void OnTextChanged(EventArgs e)
        {
            textBox1.Text = Text;
            base.OnTextChanged(e);
        }

        private void CheckSelIndex()
        {
            //if (this.Enabled == false)
            //{
            //    _promptLabel.Visible = false;
            //    return;
            //}
            //if (ReadOnly == true)
            //{
            //    _promptLabel.Visible = false;
            //    textBox1.ReadOnly = true;
            //    return;
            //}



            //if (string.IsNullOrEmpty(Text))
            //{
            //    if (!textBox1.Focused && !this.Focused)
            //    {
            //        _promptLabel.Visible = true;
            //        asd
            //    }
            //    else
            //    {
            //        _promptLabel.Visible = false;
            //    }
            //}
            ////}
            ////else
            ////{
            ////    _promptLabel.Visible = false;
            ////}
        }

        protected override void OnEnter(EventArgs e)
        {
            if (Enabled == false || ReadOnly == true)
            {
                SendKeys.Send("{TAB}");
                return;
            }
            CheckSelIndex();
            if (_panel != null)
            {
                _panel.BackColor = Color.RoyalBlue;
            }

            if (IsValid == false)
            {
                ShowValidationErrorToolTip();
            }
            if (IsValid == null)
            {
                ClearAllErrorMessages(false);
            }

            CheckToShowTypeHere(textBox1.Text);
            base.OnEnter(e);
        }

        protected override void OnLeave(EventArgs e)
        {
            ClearAllErrorMessages();
            CheckToShowTypeHere(textBox1.Text);
            if (_panel != null)
            {
                _panel.BackColor = Color.LightGray;
            }
            base.OnLeave(e);
        }

        public void ClearAllErrorMessages(bool clearToolTip = true)
        {
            if (IsValid == null || IsValid == true)
            {
                ErrorToolTip.SetError(this, "");
                ErrorToolTip.Dispose();
                ErrorToolTip = new ErrorProvider();
                ErrorToolTip.SetError(this, "");
            }
            if (clearToolTip)
            {
                toolTip.Hide(this);
            }
        }

        //private void CheckSelIndexExt()
        //{
        //    if (string.IsNullOrEmpty(textBox1.Text))
        //    {
        //        _promptLabel.Visible = true;
        //    }
        //    else
        //    {
        //        _promptLabel.Visible = false;
        //    }
        //    CheckSelIndex();
        //}

        protected override void OnLocationChanged(EventArgs e)
        {
            UpdatePanelSizeAndLocation();
            base.OnLocationChanged(e);
        }

        protected override void OnAutoSizeChanged(EventArgs e)
        {
            UpdatePanelSizeAndLocation();
            base.OnAutoSizeChanged(e);
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            UpdatePanelSizeAndLocation();
            base.OnSizeChanged(e);
        }

        protected override void OnLayout(LayoutEventArgs levent)
        {
            UpdatePanelSizeAndLocation();
            base.OnLayout(levent);
        }

        //protected override void OnControlRemoved(ControlEventArgs e)
        //{
        //    RemovePanel();
        //    base.OnControlRemoved(e);
        //}

        protected override void OnHandleDestroyed(EventArgs e)
        {
            RemovePanel();
            base.OnHandleDestroyed(e);
        }

        protected override void OnControlAdded(ControlEventArgs e)
        {
            CheckSelIndex();
            base.OnControlAdded(e);
        }

        protected override void OnCreateControl()
        {
            CheckSelIndex();
            base.OnCreateControl();
        }

        private void RemovePanel()
        {
            try
            {
                if (_panel != null)
                {
                    base.Parent.Controls.Remove(_panel);
                    //base.Parent.Controls.Remove(_promptLabel);
                    base.Parent.Controls.Remove(_mandatoryMark);
                }
            }
            catch
            {
                //suppressed
            }
        }

        public void ShowValidationError(string message = null)
        {
            ErrorToolTip.SetError(this, message ?? ValidationErrorMessage);
        }

        public void ShowValidationErrorToolTip(string message = null)
        {
            toolTip.ToolTipTitle = "Validation Error";
            toolTip.Show(message ?? ValidationErrorMessage, base.Parent, base.Left, base.Top + base.Height);
        }

        public void ShowValidationErrorToolTip(string title, string message)
        {
            toolTip.ToolTipTitle = title;
            toolTip.Show(message ?? ValidationErrorMessage, base.Parent, base.Left, base.Top + base.Height);
        }

        public void Clear()
        {
            ClearAllErrorMessages();
            textBox1.Text = "";
            IsValid = null;
        }

        private void textBox1_KeyPressForInputScope(object sender, KeyPressEventArgs e)
        {
            switch (InputScope)
            {
                case InputScopeType.Alphanumeric:
                    {
                        if ((!char.IsLetterOrDigit(e.KeyChar)) && ((Keys)e.KeyChar != Keys.Back) && ((Keys)e.KeyChar != Keys.Decimal))
                        {
                            e.Handled = true;
                        }
                    }
                    break;
                case InputScopeType.MobileNumber:
                    {
                        if ((!char.IsNumber(e.KeyChar)) && ((Keys)e.KeyChar != Keys.Back))
                        {
                            e.Handled = true;
                        }
                    }
                    break;
                case InputScopeType.Numeric:
                    if ((!char.IsNumber(e.KeyChar)) && ((Keys)e.KeyChar != Keys.Back) && ((Keys)e.KeyChar != Keys.Decimal) && ((Keys)e.KeyChar != Keys.OemPeriod) && (e.KeyChar == '.' && textBox1?.Text.IndexOf('.') >= 0))
                    {
                        e.Handled = true;
                    }
                    break;
                case InputScopeType.Custom:
                    if (InputScopeCustomString.IndexOf(e.KeyChar) < 0)
                    {
                        e.Handled = true;
                    }
                    break;
                case InputScopeType.Default:
                    {
                        //nothing
                    }
                    break;
                case InputScopeType.NationalId:
                    {
                        if ((!char.IsNumber(e.KeyChar)) && ((Keys)e.KeyChar != Keys.Back))
                        {
                            e.Handled = true;
                        }
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        ~CustomTextBox()
        {
            RemovePanel();
        }

        private void textBox1_Resize(object sender, EventArgs e)
        {
            ResizeBaseToFitTextBox();
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        public event EventHandler TextChangedExt
        {
            add { textBox1.TextChanged += value; }
            remove { textBox1.TextChanged -= value; }
        }
    }
}