﻿using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.ComponentModel.Com2Interop;

namespace MISL.Ababil.Agent.CustomControls
{
    public class CustomTabControl : TabControl
    {
        public CustomTabControl()
        {
            try
            {
                DrawMode = TabDrawMode.OwnerDrawFixed;
                ItemSize = new Size(0, 0);
                Appearance = TabAppearance.Buttons;
                SizeMode = TabSizeMode.Fixed;
            }
            catch
            {
                // ignored
            }
        }

        internal void DrawControl(Graphics g)
        {

            try
            {
                if (!Visible)
                    return;

                Rectangle tabControlArea = ClientRectangle;
                Rectangle tabArea = DisplayRectangle;



                Pen border = new Pen(Color.White);
                //TabArea.Inflate(nDelta, nDelta);
                tabArea.Inflate(2, 2);
                g.DrawRectangle(border, tabArea);
                border.Dispose();

                {
                    Brush br = new SolidBrush(Color.White);
                    g.FillRectangle(br, tabControlArea);
                    br.Dispose();
                }

                {
                    Brush br = new SolidBrush(Color.FromArgb(43, 87, 154));
                    Rectangle rectangle = tabControlArea;
                    rectangle.Height = 1;
                    rectangle.Y = 25;
                    g.FillRectangle(br, rectangle);
                    br.Dispose();
                }
                {
                    Brush br = new SolidBrush(Color.FromArgb(43, 87, 154));
                    Rectangle rectangle = tabControlArea;
                    rectangle.Height = 1;
                    rectangle.Y = 26;
                    g.FillRectangle(br, rectangle);
                    br.Dispose();
                }
                Region rsaved = g.Clip;

                for (int i = 0; i < TabCount; i++)
                {
                    DrawTab(g, TabPages[i], i);
                    TabPages[i].BackColor = Color.White;
                }

                g.Clip = rsaved;

                if (SelectedTab != null)
                {
                    Color color = Color.White;
                    border = new Pen(color);

                    tabArea.Offset(1, 1);
                    tabArea.Width -= 2;
                    tabArea.Height -= 2;

                    g.DrawRectangle(border, tabArea);
                    tabArea.Width -= 1;
                    tabArea.Height -= 1;
                    g.DrawRectangle(border, tabArea);

                    border.Dispose();
                }
            }
            catch
            {
                // ignored
            }
        }

        internal void DrawTab(Graphics g, TabPage tabPage, int nIndex)
        {
            Rectangle recBounds = GetTabRect(nIndex);
            RectangleF tabTextArea = GetTabRect(nIndex);

            bool bSelected = (SelectedIndex == nIndex);

            Point[] pt = new Point[7];
            if (Alignment == TabAlignment.Top)
            {
                pt[0] = new Point(recBounds.Left, recBounds.Bottom);
                pt[1] = new Point(recBounds.Left, recBounds.Top + 0);
                pt[2] = new Point(recBounds.Left + 0, recBounds.Top);
                pt[3] = new Point(recBounds.Right - 0, recBounds.Top);
                pt[4] = new Point(recBounds.Right, recBounds.Top + 0);
                pt[5] = new Point(recBounds.Right, recBounds.Bottom);
                pt[6] = new Point(recBounds.Left, recBounds.Bottom);
            }
            else
            {
                pt[0] = new Point(recBounds.Left, recBounds.Top);
                pt[1] = new Point(recBounds.Right, recBounds.Top);
                pt[2] = new Point(recBounds.Right, recBounds.Bottom - 0);
                pt[3] = new Point(recBounds.Right - 0, recBounds.Bottom);
                pt[4] = new Point(recBounds.Left + 0, recBounds.Bottom);
                pt[5] = new Point(recBounds.Left, recBounds.Bottom - 0);
                pt[6] = new Point(recBounds.Left, recBounds.Top);
            }

            Brush br;
            if (bSelected)
            {
                br = new SolidBrush(Color.FromArgb(43, 87, 154));
                g.FillPolygon(br, pt);
                br.Dispose();
                g.DrawPolygon(new Pen(Color.FromArgb(43, 87, 154)), pt);
            }
            else
            {
                br = new SolidBrush(Color.LightGray);
                g.FillPolygon(br, pt);
                br.Dispose();
                g.DrawPolygon(Pens.LightGray, pt);
            }

            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            stringFormat.LineAlignment = StringAlignment.Center;

            if (bSelected)
            {
                br = new SolidBrush(Color.White);
            }
            else
            {
                br = new SolidBrush(Color.Black);
            }

            RectangleF rectangleF = tabTextArea;
            rectangleF.Y += 2;
            g.DrawString(tabPage.Text, Font, br, rectangleF, stringFormat);
        }

        protected override void InitLayout()
        {
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.DoubleBuffer, true);
            SetStyle(ControlStyles.ResizeRedraw, true);
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            base.InitLayout();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            DrawControl(e.Graphics);
        }
    }
}
//[Designer(typeof(ExpandableObjectConverter))]
//[Designer("System.Windows.Forms.Design.ParentControlDesigner, System.Design", typeof(IDesigner))]