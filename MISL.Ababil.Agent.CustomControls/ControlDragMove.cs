﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MISL.Ababil.Agent.CustomControls
{
    public static class ControlDragMove
    {
        public enum Direction
        {
            Any,
            Horizontal,
            Vertical
        }

        public static void Init(Control control)
        {
            Init(control, Direction.Any);
        }

        public static void Init(Control control, Direction direction)
        {
            Init(control, control, direction);
        }

        public static void Init(Control control, Control container, Direction direction)
        {
            bool Dragging = false;
            Point DragStart = Point.Empty;
            control.MouseDown += delegate (object sender, MouseEventArgs e)
            {
                //if (control.Left > control.Parent.Width / 2)
                //{
                //    control.Left = control.Parent.Width / 2;
                //    Dragging = false;
                //    control.Capture = false;
                //    //return;
                //}
                Dragging = true;
                DragStart = new Point(e.X, e.Y);
                control.Capture = true;
            };
            control.MouseUp += delegate (object sender, MouseEventArgs e)
            {
                Dragging = false;
                control.Capture = false;
            };
            control.MouseMove += delegate (object sender, MouseEventArgs e)
            {
                int e_X = e.X;
                if (e_X > control.Parent.Width / 2)
                {
                    e_X = (int)(control.Parent.Width / 2.0);
                }
                if (Dragging)
                {
                    if (direction != Direction.Vertical)
                    {
                        if (control.Left <= control.Parent.Width / 2)
                        {
                            container.Left = Math.Max(0, e_X + container.Left - DragStart.X);
                        }
                        else
                        {
                            container.Left = Math.Max(0, container.Left - DragStart.X);
                        }
                    }
                    //if (direction != Direction.Horizontal)
                    //    container.Top = Math.Max(0, e.Y + container.Top - DragStart.Y);
                }
                //if (control.Left > control.Parent.Width / 2)
                //{
                //    control.Left = control.Parent.Width / 2;
                //}
            };

            //bool Dragging = false;
            //Point DragStart = Point.Empty;
            //control.MouseDown += delegate (object sender, MouseEventArgs e)
            //{
            //    Dragging = true;
            //    DragStart = new Point(e.X, e.Y);
            //    control.Capture = true;
            //};
            //control.MouseUp += delegate (object sender, MouseEventArgs e)
            //{
            //    Dragging = false;
            //    control.Capture = false;
            //};
            //control.MouseMove += delegate (object sender, MouseEventArgs e)
            //{
            //    if (Dragging)
            //    {
            //        if (direction != Direction.Vertical)
            //            container.Left = Math.Max(0, e.X + container.Left - DragStart.X);
            //        if (direction != Direction.Horizontal)
            //            container.Top = Math.Max(0, e.Y + container.Top - DragStart.Y);
            //    }
            //};
        }

        //public static void InitSwitch(Control control, Control container)
        //{
        //    Direction direction = Direction.Horizontal;
        //    bool Dragging = false;
        //    Point DragStart = Point.Empty;
        //    control.MouseDown += delegate (object sender, MouseEventArgs e)
        //    {
        //        if (control.Left > control.Parent.Width / 2)
        //        {
        //            control.Left = control.Parent.Width / 2;
        //            Dragging = false;
        //            control.Capture = false;
        //            //return;
        //        }
        //        Dragging = true;
        //        DragStart = new Point(e.X, e.Y);
        //        control.Capture = true;
        //    };
        //    control.MouseUp += delegate (object sender, MouseEventArgs e)
        //    {
        //        Dragging = false;
        //        control.Capture = false;
        //    };
        //    control.MouseMove += delegate (object sender, MouseEventArgs e)
        //    {
        //        if (Dragging)
        //        {
        //            if (direction != Direction.Vertical)
        //            {
        //                if (control.Left < control.Parent.Width / 2)
        //                {
        //                    container.Left = Math.Max(0, e.X + container.Left - DragStart.X);
        //                }
        //                else
        //                {
        //                    control.Left = control.Parent.Width / 2;
        //                }
        //            }
        //            //if (direction != Direction.Horizontal)
        //            //    container.Top = Math.Max(0, e.Y + container.Top - DragStart.Y);
        //        }
        //        if (control.Left > control.Parent.Width / 2)
        //        {
        //            control.Left = control.Parent.Width / 2;
        //        }
        //    };
        //}
    }
}