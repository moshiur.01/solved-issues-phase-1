﻿using System;
using System.Globalization;
using System.Windows.Forms;

namespace MISL.Ababil.Agent.CustomControls.CustomValidation
{
    public class CustomValidation
    {
        private const string DateSaperator = "-";

        public static bool IsNotEmpty(ref TextBox textBox)
        {
            return string.IsNullOrEmpty(textBox.Text.Trim());
        }

        public static bool IsNotEmpty(ref ComboBox comboBox)
        {
            return comboBox.SelectedIndex < 0;
        }

        public static bool IsNumber(ref TextBox textBox)
        {
            double tmp;
            return double.TryParse(textBox.Text, out tmp) == false;
        }

        public static bool IsNationalId(ref TextBox textBox)
        {
            return textBox.Text.Length == 11 || textBox.Text.Length == 13 || textBox.Text.Length == 17;
        }

        public static bool IsMobileNumber(ref TextBox textBox, string message)
        {
            int tmp;
            if (!int.TryParse(textBox.Text, out tmp) || textBox.Text.Length != 11)
            {
                return false;
            }
            return textBox.Text.Substring(0, 3) != "015"
                   &&
                   textBox.Text.Substring(0, 3) != "016"
                   &&
                   textBox.Text.Substring(0, 3) != "017"
                   &&
                   textBox.Text.Substring(0, 3) != "018"
                   &&
                   textBox.Text.Substring(0, 3) != "019"
                   &&
                   textBox.Text.Substring(0, 3) != "011"
                   &&
                   textBox.Text.Substring(0, 3) != "013"
                   &&
                   textBox.Text.Substring(0, 3) != "014";
        }

        public static bool IsDate(ref TextBox textBox)
        {
            CultureInfo enUS = new CultureInfo("en-US");
            DateTime tmp;
            if (textBox.Text.Length == 8)
            {
                textBox.Text = textBox.Text.Substring(0, 2) + DateSaperator + textBox.Text.Substring(2, 2) + DateSaperator + textBox.Text.Substring(4, 4); ;
            }
            if (DateTime.TryParseExact(textBox.Text.Replace("/", "-"), "d-M-yyyy", enUS, DateTimeStyles.None, out tmp) == false)
            {
                return true;
            }
            return false;
        }

        ////////public static bool IsDateExt(TextBox textBox, bool drawBorder)
        ////////{
        ////////    CultureInfo enUS = new CultureInfo("en-US");
        ////////    Graphics g = textBox.Parent.CreateGraphics();
        ////////    DateTime tmp;
        ////////    if (textBox.Text.Length == 8)
        ////////    {
        ////////        textBox.Text = textBox.Text.Substring(0, 2) + "/" + textBox.Text.Substring(2, 2) + "/" + textBox.Text.Substring(4, 4);
        ////////    }
        ////////    if (DateTime.TryParseExact(textBox.Text, "d/M/yyyy", enUS, DateTimeStyles.None, out tmp) == false)
        ////////    {
        ////////            return true;
        ////////    }
        ////////    return false;
        ////////}                     
    }
}