﻿namespace MISL.Ababil.Agent.CustomControls.CustomValidation
{
    public enum CustomValidationType
    {
        NONE = 0,
        TEXTBOX_EMPTY = 2,
        TEXTBOX_NUMBER = 4,
        TEXTBOX_NUMBER_MOBILE = 8,
        TEXTBOX_DATE = 16,
        TEXTBOX_DATE_NULLABLE = 32,
        TEXTBOX_NationalID = 64,
        COMBOBOX_EMPTY = 128,
    }    
}