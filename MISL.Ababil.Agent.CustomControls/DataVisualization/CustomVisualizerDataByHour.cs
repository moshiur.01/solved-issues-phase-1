﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace MISL.Ababil.Agent.CustomControls.DataVisualization
{
    public partial class CustomVisualizerDataByHour : UserControl
    {
        private Dictionary<string, decimal> _bindingDictionaryObject;
        private bool _enableEvents = false;

        public CustomVisualizerDataByHour()
        {
            InitializeComponent();
        }

        public bool EnableEvents
        {
            get { return _enableEvents; }
            set { _enableEvents = value; }
        }

        public Dictionary<string, decimal> BindingDictionaryObject
        {
            get
            {
                return _bindingDictionaryObject;
            }
            set
            {
                _bindingDictionaryObject = value;
                //RenderChart();
            }
        }

        public void Clear()
        {
            chart.Series["seriesDataByHour"].Points.Clear();
        }

        public void RenderChart()
        {
            if (_bindingDictionaryObject == null)
            {
                chart.Series[0].Points.Clear();
                return;
            }
            chart.Series["seriesDataByHour"].Points.Clear();
            for (int i = 0; i < _bindingDictionaryObject.Count; i++)
            {
                //chart.Series["seriesDataByHour"].Points.AddXY(_bindingDictionaryObject.Keys.ElementAt(i), _bindingDictionaryObject.Values.ElementAt(i));
                if ((long)_bindingDictionaryObject.Values.ElementAt(i) != 0)
                {
                    chart.Series["seriesDataByHour"].Points.AddXY(_bindingDictionaryObject.Keys.ElementAt(i), _bindingDictionaryObject.Values.ElementAt(i));
                    chart.Series["seriesDataByHour"].Points[i].Label =
                        _bindingDictionaryObject.Values.ElementAt(i).ToString();
                }
                else
                {
                    chart.Series["seriesDataByHour"].Points.AddXY(_bindingDictionaryObject.Keys.ElementAt(i), _bindingDictionaryObject.Values.ElementAt(i));
                    chart.Series["seriesDataByHour"].Points[i].Label = "";
                }
            }
            //Color tempColor = chart.Series["seriesDataByHour"].Points[_bindingDictionaryObject.Count - 1].Color;
            //chart.Series["seriesDataByHour"].Points[_bindingDictionaryObject.Count - 1].Color =
            //    Color.FromArgb(tempColor.R, tempColor.B + 30, tempColor.G);
        }

        private void chart_MouseDown(object sender, MouseEventArgs e)
        {
            if (_enableEvents == false)
            {
                return;
            }
            try
            {
                HitTestResult hitTestResult = ((Chart)sender).HitTest(e.X, e.Y);

                if (hitTestResult.ChartElementType == ChartElementType.DataPoint)
                {
                    ((Chart)sender).Cursor = Cursors.Hand;
                    //string str = hitTestResult.Series.Points[hitTestResult.PointIndex].LegendText;
                    string str = hitTestResult.Series.Points[hitTestResult.PointIndex].XValue.ToString()
                        + " "
                        + hitTestResult.Series.Points[hitTestResult.PointIndex].YValues[0].ToString();
                    MessageBox.Show(str);
                    hitTestResult.Series.Points[hitTestResult.PointIndex].YValues[0] =
                        hitTestResult.Series.Points[hitTestResult.PointIndex].YValues[0] * -1;
                }
            }
            catch
            {
                // ignored
            }
        }

        private void chart_MouseMove(object sender, MouseEventArgs e)
        {
            if (_enableEvents == false)
            {
                return;
            }
            try
            {
                HitTestResult hitTestResult = ((Chart)sender).HitTest(e.X, e.Y);

                if (hitTestResult.ChartElementType != ChartElementType.DataPoint)
                {
                    //System.Threading.Thread.Sleep(100);
                    ((Chart)sender).Cursor = Cursors.Default;
                    resetChartSerise((Chart)sender);
                }
                else
                {
                    resetChartSerise((Chart)sender);
                    ((Chart)sender).Cursor = Cursors.Hand;
                    //label1.Text = hitTestResult.Series.Points[hitTestResult.PointIndex].LegendText;
                    Color tmoColor = hitTestResult.Series.Points[hitTestResult.PointIndex].Color;
                    //hitTestResult.Series.Points[hitTestResult.PointIndex].Color = Color.Black;
                    hitTestResult.Series.Points[hitTestResult.PointIndex].BorderColor = Color.Black;
                    hitTestResult.Series.Points[hitTestResult.PointIndex].BorderWidth = 2;
                    hitTestResult.Series.Points[hitTestResult.PointIndex].LabelForeColor = Color.White;
                    hitTestResult.Series.Points[hitTestResult.PointIndex].CustomProperties = "Exploded=True";
                    //hitTestResult.Series.Points[hitTestResult.PointIndex].Color = Color.Black;
                }
            }
            catch
            {
                //((Chart)sender).Cursor = Cursors.Default;
                //resetChartSerise((Chart)sender);
            }
        }

        private void resetChartSerise(Chart chart)
        {
            for (int i = 0; i < chart.Series[0].Points.Count; i++)
            {
                chart.Series[0].Points[i].BorderWidth = 0;
                chart.Series[0].Points[i].CustomProperties = "Exploded=False";
            }
        }

        public void SetTitle(string title)
        {
            chart.Titles[0].Text = title; //string.Format("Today's Income: {0:F2} BDT", sum);
        }
    }
}