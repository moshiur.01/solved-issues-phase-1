﻿using MISL.Ababil.Agent.UI.forms.CustomControls;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Windows.Forms;

namespace MISL.Ababil.Agent.CustomControls
{
    public class CustomComboBoxDropDown : ComboBox
    {
        private readonly Panel _panel = new Panel();
        private Label _promptLabel = new Label();
        private bool _showPromptText = false;
        private ToolTip _toolTip = new ToolTip();
        private System.ComponentModel.IContainer components;
        private ErrorProvider _errorToolTip = new ErrorProvider();
        private string _promptText = "(Select)";
        private MandatoryMark _mandatoryMark = new MandatoryMark();
        private bool _showMandatoryMark = false;
        //private string _message = "";
        private string _validationErrorMessage;

        protected override void OnVisibleChanged(EventArgs e)
        {
            try
            {
                if (this.Visible == false)
                {
                    _mandatoryMark.Visible = false;
                    _panel.Visible = false;
                }
                else
                {
                    _panel.Visible = true;
                    _panel.SendToBack();
                    this.BringToFront();
                    if (ShowMandatoryMark == true)
                    {
                        _mandatoryMark.Visible = true;
                    }
                }
            }
            catch { }
            base.OnVisibleChanged(e);
        }

        public bool ShowMandatoryMark
        {
            get
            {

                return _mandatoryMark.Visible = _showMandatoryMark;
            }
            set
            {
                _mandatoryMark.Visible = _showMandatoryMark = value;
            }

        }

        public string ValidationErrorMessage
        {

            get
            {
                if (string.IsNullOrEmpty(_validationErrorMessage))
                    return "Validation Error!";
                else
                    return _validationErrorMessage;
            }
            set { _validationErrorMessage = value; }
        }

        public bool InputScopeAllowEmpty { get; set; }

        public bool? IsValid { get; set; }

        public string PromptText
        {
            get { return _promptText; }
            set { _promptText = value; }
        }

        public bool ReadOnly { get; set; }

        protected override void InitLayout()
        {
            try
            {
                base.FlatStyle = FlatStyle.Flat;
                base.DropDownStyle = ComboBoxStyle.DropDown;
                try
                {
                    base.Parent.Controls.Add(_mandatoryMark);
                    _mandatoryMark.TabStop = false;

                }
                catch { }



                if (_panel != null)
                {

                    _panel.Top = base.Location.Y - 2;
                    _panel.Left = base.Location.X - 2;
                    _panel.Height = base.Height + 4;
                    _panel.Width = base.Width + 4;

                    _promptLabel.Text = "(Select)";
                    UpdatePanelSizeAndLocation();

                    base.Parent.Controls.Add(_panel);
                    base.Parent.Controls.Add(_promptLabel);
                    _promptLabel.Visible = false;

                    _promptLabel.BackColor = base.BackColor;
                    _promptLabel.ForeColor = Color.Gray;

                    _panel.BackColor = Color.LightGray;

                    _promptLabel.Click += _promptLabel_Click;

                    //Focus Next Control
                    {
                        base.KeyUp += CustomComboBox_KeyUpToFocusNextControl;
                    }

                    //textBox1.Text = Text;                    
                    CheckSelIndexExt();

                    InitializeComponent();
                }
            }
            catch (Exception ex)
            {
                //suppressed
            }
        }

        private void CustomComboBox_KeyUpToFocusNextControl(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void _promptLabel_Click(object sender, EventArgs e)
        {
            _promptLabel.Visible = false;
            base.Focus();
        }

        private void UpdatePanelSizeAndLocation()
        {
            try
            {
                ResizeBaseToFitTextBox();
                if (_panel != null)
                {
                    _panel.Top = base.Location.Y - 2;
                    _panel.Left = base.Location.X - 2;
                    _panel.Height = base.Height + 4;
                    _panel.Width = base.Width + 4;

                    _promptLabel.AutoSize = true;
                    _promptLabel.Font = this.Font;
                    _promptLabel.Top = this.Top + (this.Height - _promptLabel.Height) / 2;
                    _promptLabel.Left = this.Left + 2;

                    try
                    {
                        _mandatoryMark.Top = base.Top;
                        _mandatoryMark.Left = base.Left + base.Width + 8;
                    }
                    catch { }

                    try
                    {
                        _promptLabel.Visible = _panel.Visible = base.Visible;
                    }
                    catch { }

                    try
                    {
                        if (_panel != null)
                        {
                            if (_panel.Parent != base.Parent)
                            {
                                Control panelParent = _panel.Parent;
                                panelParent.Controls.Remove(_panel);
                                base.Parent.Controls.Add(_panel);
                                ResizeBaseToFitTextBox();
                            }
                        }
                    }
                    catch
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                //suppressed
            }
        }

        private void ResizeBaseToFitTextBox()
        {
            //this.Height = this.Height + (this.Top * 2);
        }

        private void CheckSelIndex()
        {
            if (this.Enabled == false)
            {
                _promptLabel.Visible = false;
                return;
            }
            if (ReadOnly == true)
            {
                _promptLabel.Visible = false;
                base.Enabled = false;
                return;
            }

            if (_showPromptText == true)
            {
                //if (base.SelectedIndex < 0)
                //{
                if (string.IsNullOrEmpty(Text))
                {
                    _promptLabel.BringToFront();
                    _promptLabel.Visible = true;
                }
                //}
                //else
                //{
                //    _promptLabel.Visible = false;
                //    _showPromptText = false;
                //}
            }
            else
            {
                _promptLabel.Visible = false;
            }
        }

        protected override void OnEnter(EventArgs e)
        {
            if (Enabled == false || ReadOnly == true)
            {
                SendKeys.Send("{TAB}");
                return;
            }

            _showPromptText = false;
            CheckSelIndex();
            _panel.BackColor = Color.RoyalBlue;

            if (IsValid == false)
            {
                ShowValidationErrorToolTip();
            }
            if (IsValid == null)
            {
                ClearAllErrorMessages(false);
            }

            base.OnEnter(e);
        }

        protected override void OnLeave(EventArgs e)
        {
            ClearAllErrorMessages();

            CheckSelIndexExt();

            #region modification reference

            //if (base.SelectedIndex < 0 && base.SelectedItem == null && base.SelectedValue == null &&
            //   string.IsNullOrEmpty(base.SelectedText) && string.IsNullOrEmpty(base.Text))
            //{
            //    _showPromptText = true;
            //}
            //else
            //{
            //    _showPromptText = false;
            //}
            //CheckSelIndex();

            //_panel.BackColor = Color.LightGray;
            //base.OnLeave(e);

            #endregion

            _panel.BackColor = Color.LightGray;
            base.OnLeave(e);
        }

        public void ClearAllErrorMessages(bool clearToolTip = true)
        {
            if (IsValid == null || IsValid == true)
            {
                _errorToolTip.SetError(this, "");
                _errorToolTip.Dispose();
                _errorToolTip = new ErrorProvider();
                _errorToolTip.SetError(this, "");
            }
            if (clearToolTip)
            {
                _toolTip.Hide(this);
            }
        }

        private void CheckSelIndexExt()
        {
            #region modification reference

            //if (base.SelectedIndex < 0 && base.SelectedItem == null && base.SelectedValue == null &&
            //   string.IsNullOrEmpty(base.SelectedText) && string.IsNullOrEmpty(base.Text))
            //{
            //    _showPromptText = true;
            //}
            //else
            //{
            //    _showPromptText = false;
            //}
            //CheckSelIndex();

            //_panel.BackColor = Color.LightGray;
            //base.OnLeave(e);

            #endregion

            if (
                    base.SelectedIndex < 0 && base.SelectedItem == null && base.SelectedValue == null &&
                    string.IsNullOrEmpty(base.SelectedText) && string.IsNullOrEmpty(base.Text)
                )
            {
                _showPromptText = true;
            }
            else
            {
                _showPromptText = false;
            }
            CheckSelIndex();
        }

        protected override void OnLocationChanged(EventArgs e)
        {
            UpdatePanelSizeAndLocation();
            base.OnLocationChanged(e);
        }

        protected override void OnAutoSizeChanged(EventArgs e)
        {
            UpdatePanelSizeAndLocation();
            base.OnAutoSizeChanged(e);
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            UpdatePanelSizeAndLocation();
            base.OnSizeChanged(e);
        }

        protected override void OnLayout(LayoutEventArgs levent)
        {
            UpdatePanelSizeAndLocation();
            base.OnLayout(levent);
        }

        protected override void OnHandleDestroyed(EventArgs e)
        {
            RemoveControls();
            base.OnHandleDestroyed(e);
        }

        protected override void OnControlAdded(ControlEventArgs e)
        {
            _showPromptText = true;
            CheckSelIndex();
            base.OnControlAdded(e);
        }

        protected override void OnCreateControl()
        {
            _showPromptText = true;
            CheckSelIndex();
            base.OnCreateControl();
        }

        private void RemoveControls()
        {
            try
            {
                if (_panel != null)
                {
                    base.Parent.Controls.Remove(_panel);
                    base.Parent.Controls.Remove(_promptLabel);
                    base.Parent.Controls.Remove(_mandatoryMark);
                }
            }
            catch
            {
                //suppressed
            }
        }

        protected override void OnTextChanged(EventArgs e)
        {
            IsValid = null;
            ClearAllErrorMessages(true);
            base.OnTextChanged(e);
        }

        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            IsValid = null;
            ClearAllErrorMessages(true);
            //if (base.SelectedIndex < 0 || base.SelectedItem == null || base.SelectedValue == null)
            if (
                    base.SelectedIndex < 0 && base.SelectedItem == null && base.SelectedValue == null &&
                    string.IsNullOrEmpty(base.SelectedText) && string.IsNullOrEmpty(base.Text)
                )
            {
                _showPromptText = true;
            }
            else
            {
                _showPromptText = false;
            }
            CheckSelIndex();
            base.OnSelectedIndexChanged(e);
        }
        public void ShowValidationError(string message = null)
        {

            _errorToolTip.SetError(this, message ?? ValidationErrorMessage);
        }

        public void ShowValidationErrorToolTip(string message = null)
        {
            if (_toolTip == null)
                _toolTip = new ToolTip();
            _toolTip.Show(message ?? ValidationErrorMessage, base.Parent, base.Left, base.Top + base.Height);
        }

        public void Clear()
        {
            ClearAllErrorMessages();
            base.Text = "";
            base.SelectedIndex = -1;
            IsValid = null;
        }

        ~CustomComboBoxDropDown()
        {
            RemoveControls();
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._errorToolTip = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._errorToolTip)).BeginInit();
            this.SuspendLayout();
            // 
            // toolTip
            // 
            this._toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Error;
            this._toolTip.ToolTipTitle = "Validation Error";
            ((System.ComponentModel.ISupportInitialize)(this._errorToolTip)).EndInit();
            this.ResumeLayout(false);

        }
    }
}
