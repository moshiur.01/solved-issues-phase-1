﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MISL.Ababil.Agent.CustomControls
{
    public class CustomControlValidation
    {
        public static bool IsAllValid(bool showToolTipErrorMsg, bool showErrorProviderIcon, MetroFramework.Controls.MetroTabPage tabPage, params object[] controls)
        {
            bool flagIsAllControlValid = true;

            for (int i = 0; i < controls.Length; i++)
            {
                //CustomTextBox
                {
                    if (controls[i].GetType() == typeof(CustomTextBox))
                    {
                        CustomTextBox customTextBox = (CustomTextBox)controls[i];
                        customTextBox.IsValid = null;
                        customTextBox.ClearAllErrorMessages(true);

                        if (customTextBox.InputScopeAllowEmpty == false && string.IsNullOrEmpty(customTextBox.Text))
                        {
                            customTextBox.ShowValidationError();
                            customTextBox.IsValid = false;
                            if (flagIsAllControlValid)
                            {
                                customTextBox.ShowValidationErrorToolTip();
                                try
                                {
                                    ((MetroFramework.Controls.MetroTabControl)tabPage.Parent).SelectedTab = tabPage;
                                }
                                catch (Exception)
                                {
                                }
                                customTextBox.Focus();
                            }
                            flagIsAllControlValid = false;
                        }

                        switch (customTextBox.InputScope)
                        {
                            case CustomTextBox.InputScopeType.Default:
                                //not needed
                                continue;
                            case CustomTextBox.InputScopeType.Alphanumeric:
                                //not needed
                                continue;
                            case CustomTextBox.InputScopeType.Numeric:
                                //not needed
                                continue;
                            case CustomTextBox.InputScopeType.MobileNumber:
                                if (IsMobileNumber(customTextBox) == false)
                                {
                                    customTextBox.ShowValidationError();
                                    customTextBox.IsValid = false;
                                    if (flagIsAllControlValid)
                                    {
                                        customTextBox.ShowValidationErrorToolTip();
                                        try
                                        {
                                            ((MetroFramework.Controls.MetroTabControl)tabPage.Parent).SelectedTab = tabPage;
                                        }
                                        catch (Exception)
                                        {
                                        }
                                        customTextBox.Focus();
                                    }
                                    flagIsAllControlValid = false;
                                }
                                continue;
                            case CustomTextBox.InputScopeType.Custom:
                                //not needed
                                continue;
                            case CustomTextBox.InputScopeType.NationalId:
                                if (IsNationalId(customTextBox) == false)
                                {
                                    customTextBox.ShowValidationError();
                                    customTextBox.IsValid = false;
                                    if (flagIsAllControlValid)
                                    {
                                        customTextBox.ShowValidationErrorToolTip();
                                        try
                                        {
                                            ((MetroFramework.Controls.MetroTabControl)tabPage.Parent).SelectedTab = tabPage;
                                        }
                                        catch (Exception)
                                        {
                                        }
                                        customTextBox.Focus();
                                    }
                                    flagIsAllControlValid = false;
                                }
                                continue;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    }
                }

                //CustomComboBox
                {
                    if (controls[i].GetType() == typeof(CustomComboBoxDropDownList))
                    {
                        CustomComboBoxDropDownList customComboBox = (CustomComboBoxDropDownList)controls[i];
                        customComboBox.IsValid = null;
                        customComboBox.ClearAllErrorMessages(true);

                        if (
                                customComboBox.DropDownStyle == ComboBoxStyle.DropDown
                                &&
                                customComboBox.InputScopeAllowEmpty == false
                                &&
                                string.IsNullOrEmpty(customComboBox.Text)
                            )
                        {
                            customComboBox.ShowValidationError();
                            customComboBox.IsValid = false;
                            if (flagIsAllControlValid)
                            {
                                customComboBox.ShowValidationErrorToolTip();
                                try
                                {
                                    ((MetroFramework.Controls.MetroTabControl)tabPage.Parent).SelectedTab = tabPage;
                                }
                                catch (Exception)
                                {
                                }
                                customComboBox.Focus();
                            }
                            flagIsAllControlValid = false;
                        }

                        if (
                            customComboBox.DropDownStyle == ComboBoxStyle.DropDownList
                            &&
                            customComboBox.InputScopeAllowEmpty == false
                            &&
                            customComboBox.SelectedIndex < 0
                            )
                        {
                            customComboBox.ShowValidationError();
                            customComboBox.IsValid = false;
                            if (flagIsAllControlValid)
                            {
                                customComboBox.ShowValidationErrorToolTip();
                                try
                                {
                                    ((MetroFramework.Controls.MetroTabControl)tabPage.Parent).SelectedTab = tabPage;
                                }
                                catch (Exception)
                                {
                                }
                                customComboBox.Focus();
                            }
                            flagIsAllControlValid = false;
                        }
                    }
                }
            }
            return flagIsAllControlValid;
        }

        public static bool IsAllValid(bool showToolTipErrorMsg, bool showErrorProviderIcon, params object[] controls)
        {
            bool flagIsAllControlValid = true;

            for (int i = 0; i < controls.Length; i++)
            {
                //CustomTextBox
                {
                    if (controls[i].GetType() == typeof(CustomTextBox))
                    {
                        CustomTextBox customTextBox = (CustomTextBox)controls[i];
                        customTextBox.IsValid = null;
                        customTextBox.ClearAllErrorMessages(true);

                        if (customTextBox.InputScopeAllowEmpty == false && string.IsNullOrEmpty(customTextBox.Text))
                        {
                            customTextBox.ShowValidationError();
                            customTextBox.IsValid = false;
                            if (flagIsAllControlValid)
                            {
                                customTextBox.ShowValidationErrorToolTip();
                                customTextBox.Focus();
                            }
                            flagIsAllControlValid = false;
                        }

                        switch (customTextBox.InputScope)
                        {
                            case CustomTextBox.InputScopeType.Default:
                                //not needed
                                continue;
                            case CustomTextBox.InputScopeType.Alphanumeric:
                                //not needed
                                continue;
                            case CustomTextBox.InputScopeType.Numeric:
                                //not needed
                                continue;
                            case CustomTextBox.InputScopeType.MobileNumber:
                                if (IsMobileNumber(customTextBox) == false)
                                {
                                    customTextBox.ShowValidationError();
                                    customTextBox.IsValid = false;
                                    if (flagIsAllControlValid)
                                    {
                                        customTextBox.ShowValidationErrorToolTip();
                                        customTextBox.Focus();
                                    }
                                    flagIsAllControlValid = false;
                                }
                                continue;
                            case CustomTextBox.InputScopeType.Custom:
                                //not needed
                                continue;
                            case CustomTextBox.InputScopeType.NationalId:
                                if (IsNationalId(customTextBox) == false)
                                {
                                    customTextBox.ShowValidationError();
                                    customTextBox.IsValid = false;
                                    if (flagIsAllControlValid)
                                    {
                                        customTextBox.ShowValidationErrorToolTip();
                                        customTextBox.Focus();
                                    }
                                    flagIsAllControlValid = false;
                                }
                                continue;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    }
                }

                //CustomComboBox
                {
                    if (controls[i].GetType() == typeof(CustomComboBoxDropDownList))
                    {
                        CustomComboBoxDropDownList customComboBox = (CustomComboBoxDropDownList)controls[i];
                        customComboBox.IsValid = null;
                        customComboBox.ClearAllErrorMessages(true);

                        if (
                                customComboBox.DropDownStyle == ComboBoxStyle.DropDown
                                &&
                                customComboBox.InputScopeAllowEmpty == false
                                &&
                                string.IsNullOrEmpty(customComboBox.Text)
                            )
                        {
                            customComboBox.ShowValidationError();
                            customComboBox.IsValid = false;
                            if (flagIsAllControlValid)
                            {
                                customComboBox.ShowValidationErrorToolTip();
                                customComboBox.Focus();
                            }
                            flagIsAllControlValid = false;
                        }

                        if (
                            customComboBox.DropDownStyle == ComboBoxStyle.DropDownList
                            &&
                            customComboBox.InputScopeAllowEmpty == false
                            &&
                            customComboBox.SelectedIndex < 0
                            )
                        {
                            customComboBox.ShowValidationError();
                            customComboBox.IsValid = false;
                            if (flagIsAllControlValid)
                            {
                                customComboBox.ShowValidationErrorToolTip();
                                customComboBox.Focus();
                            }
                            flagIsAllControlValid = false;
                        }
                    }
                }
            }
            return flagIsAllControlValid;
        }

        public static bool IsAllValid(params object[] controls)
        {
            for (int i = 0; i < controls.Length; i++)
            {
                //CustomTextBox
                {
                    if (controls[i].GetType() == typeof(CustomTextBox))
                    {
                        CustomTextBox customTextBox = (CustomTextBox)controls[i];

                        if (customTextBox.InputScopeAllowEmpty == false && string.IsNullOrEmpty(customTextBox.Text))
                        {
                            return false;
                        }

                        switch (customTextBox.InputScope)
                        {
                            case CustomTextBox.InputScopeType.Default:
                                //not needed
                                continue;
                            case CustomTextBox.InputScopeType.Alphanumeric:
                                //not needed
                                continue;
                            case CustomTextBox.InputScopeType.Numeric:
                                //not needed
                                continue;
                            case CustomTextBox.InputScopeType.MobileNumber:
                                if (IsMobileNumber(customTextBox) == false) return false;
                                continue;
                            case CustomTextBox.InputScopeType.Custom:
                                //not needed
                                continue;
                            case CustomTextBox.InputScopeType.NationalId:
                                if (IsNationalId(customTextBox) == false)
                                    return false;
                                continue;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    }
                }

                //CustomComboBox
                {
                    if (controls[i].GetType() == typeof(CustomComboBoxDropDownList))
                    {
                        CustomComboBoxDropDownList customComboBox = (CustomComboBoxDropDownList)controls[i];

                        if (
                                customComboBox.DropDownStyle == ComboBoxStyle.DropDown
                                &&
                                customComboBox.InputScopeAllowEmpty == false
                                &&
                                string.IsNullOrEmpty(customComboBox.Text)
                            )
                        {
                            return false;
                        }

                        if (
                            customComboBox.DropDownStyle == ComboBoxStyle.DropDownList
                            &&
                            customComboBox.InputScopeAllowEmpty == false
                            &&
                            customComboBox.SelectedIndex < 0
                            )
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        private const string DateSaperator = "-";

        public static bool IsNotEmpty(CustomTextBox textBox)
        {
            return string.IsNullOrEmpty(textBox.Text.Trim());
        }

        public static bool IsNotEmpty(ref ComboBox comboBox)
        {
            return comboBox.SelectedIndex < 0;
        }

        public static bool IsNumber(CustomTextBox textBox)
        {
            double tmp;
            return double.TryParse(textBox.Text, out tmp);
        }

        public static bool IsNationalId(CustomTextBox customTextBox)
        {
            if (customTextBox.InputScopeAllowEmpty && customTextBox.Text.Length == 0) return true;
            if (IsNumber(customTextBox) == false) return false;
            return customTextBox.Text.Length == 11 || customTextBox.Text.Length == 13 || customTextBox.Text.Length == 17;
        }

        public static bool IsMobileNumber(CustomTextBox textBox)
        {
            int tmp;
            if (!int.TryParse(textBox.Text, out tmp) || textBox.Text.Length != 11)
            {
                return false;
            }
            return textBox.Text.Substring(0, 3) == "015"
                   ||
                   textBox.Text.Substring(0, 3) == "016"
                   ||
                   textBox.Text.Substring(0, 3) == "017"
                   ||
                   textBox.Text.Substring(0, 3) == "018"
                   ||
                   textBox.Text.Substring(0, 3) == "019"
                   ||
                   textBox.Text.Substring(0, 3) == "011"
                   ||
                   textBox.Text.Substring(0, 3) == "013"
                   ||
                   textBox.Text.Substring(0, 3) == "014";
        }

        public static bool IsDate(ref TextBox textBox)
        {
            CultureInfo enUS = new CultureInfo("en-US");
            DateTime tmp;
            if (textBox.Text.Length == 8)
            {
                textBox.Text = textBox.Text.Substring(0, 2) + DateSaperator + textBox.Text.Substring(2, 2) + DateSaperator + textBox.Text.Substring(4, 4); ;
            }
            if (DateTime.TryParseExact(textBox.Text.Replace("/", "-"), "d-M-yyyy", enUS, DateTimeStyles.None, out tmp) == false)
            {
                return true;
            }
            return false;
        }

        ////////public static bool IsDateExt(TextBox textBox, bool drawBorder)
        ////////{
        ////////    CultureInfo enUS = new CultureInfo("en-US");
        ////////    Graphics g = textBox.Parent.CreateGraphics();
        ////////    DateTime tmp;
        ////////    if (textBox.Text.Length == 8)
        ////////    {
        ////////        textBox.Text = textBox.Text.Substring(0, 2) + "/" + textBox.Text.Substring(2, 2) + "/" + textBox.Text.Substring(4, 4);
        ////////    }
        ////////    if (DateTime.TryParseExact(textBox.Text, "d/M/yyyy", enUS, DateTimeStyles.None, out tmp) == false)
        ////////    {
        ////////            return true;
        ////////    }
        ////////    return false;
        ////////}          
    }
}