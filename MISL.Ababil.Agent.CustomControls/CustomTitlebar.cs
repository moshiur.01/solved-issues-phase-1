﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace MISL.Ababil.Agent.CustomControls
{
    public partial class CustomTitlebar : UserControl
    {
        public Form OwnerForm { get; set; }

        public Label SecondaryTitleLabel
        {
            get { return _secondaryTitleLabel; }
            set
            {
                _secondaryTitleLabel = value;
                if (SecondaryTitleLabel != null)
                {
                    SecondaryTitleLabel.MouseMove += SecondaryTitleLabel_MouseMove;
                    SecondaryTitleLabel.MouseDown += CustomTitlebar_MouseDown;
                    SecondaryTitleLabel.MouseUp += CustomTitlebar_MouseUp;
                }
            }
        }

        public bool ShowTitle
        {
            get
            {
                return Title.Visible;
            }
            set
            {
                Title.Visible = value;
            }
        }

        private int tmpX = 0;
        private int tmpY = 0;
        private Label _secondaryTitleLabel;
        private bool _isMouseDown = false;

        public CustomTitlebar()
        {
            InitializeComponent();


            //this.Dock = DockStyle.Top;
            //if (OwnerForm != null)
            //{
            //    OwnerForm.Resize += OwnerForm_Resize;
            //}
        }

        private void SecondaryTitleLabel_MouseMove(object sender, MouseEventArgs e)
        {
            CustomTitlebar_MouseMove(sender, e);
        }

        private void CustomTitlebar_MouseDown(object sender, MouseEventArgs e)
        {
            _isMouseDown = true;
        }

        private void CustomTitlebar_MouseMove(object sender, MouseEventArgs e)
        {
            //if (e.Button == MouseButtons.Left)
            if (_isMouseDown == true)
            {
                try
                {
                    OwnerForm.Left = Cursor.Position.X - tmpX;
                    OwnerForm.Top = Cursor.Position.Y - tmpY;
                }
                catch
                {
                }
            }
            else
            {
                if (sender is Label)
                {
                    if (_secondaryTitleLabel != null)
                    {
                        tmpX = e.X + _secondaryTitleLabel.Left;
                        tmpY = e.Y + _secondaryTitleLabel.Top;
                    }
                }
                else
                {
                    tmpX = e.X;
                    tmpY = e.Y;
                }
            }
        }

        private void CustomTitlebar_Load(object sender, EventArgs e)
        {
            if (OwnerForm != null)
            {
                btnMax.Enabled = OwnerForm.MaximizeBox;
                btnMin.Enabled = OwnerForm.MinimizeBox;

                Title.Text = OwnerForm.Text;

                if (OwnerForm.WindowState == FormWindowState.Normal)
                {
                    btnMax.Text = "1";
                    return;
                }
                if (OwnerForm.WindowState == FormWindowState.Maximized)
                {
                    btnMax.Text = "2";
                }
            }
            this.TabStop = btnClose.TabStop = btnMax.TabStop = btnMin.TabStop = Title.TabStop = false;
        }

        private void btnMin_Click(object sender, EventArgs e)
        {
            OwnerForm.WindowState = FormWindowState.Minimized;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            OwnerForm.MaximumSize = new Size(Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height);

            //if (OwnerForm.WindowState == FormWindowState.Normal)
            if (btnMax.Text == "1")
            {
                OwnerForm.WindowState = FormWindowState.Maximized;
                btnMax.Text = "2";
                OwnerForm.Focus();
                return;
            }
            //if (OwnerForm.WindowState == FormWindowState.Maximized)
            if (btnMax.Text == "2")
            {
                OwnerForm.WindowState = FormWindowState.Normal;
                btnMax.Text = "1";
                OwnerForm.Focus();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            OwnerForm.Close();
        }

        private void btnMax_Enter(object sender, EventArgs e)
        {
            if (OwnerForm.WindowState == FormWindowState.Normal)
            {
                btnMax.Text = "1";
                return;
            }
            if (OwnerForm.WindowState == FormWindowState.Maximized)
            {
                btnMax.Text = "2";
            }
        }

        private void CustomTitlebar_MouseUp(object sender, MouseEventArgs e)
        {
            if (OwnerForm != null)
            {
                OwnerForm.Refresh();
                OwnerForm.Update();
            }
            _isMouseDown = false;
        }

        private void CustomTitlebar_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (btnMax.Enabled) btnMax_Click(sender, e);
        }
    }
}