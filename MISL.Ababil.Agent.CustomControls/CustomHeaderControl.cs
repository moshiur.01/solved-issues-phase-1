﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MISL.Ababil.Agent.CustomControls
{
    public partial class CustomHeaderControl : UserControl
    {
        public CustomHeaderControl()
        {
            InitializeComponent();
        }

        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public override string Text
        {
            get
            {
                return label1.Text;
            }
            set
            {
                label1.Font = this.Font;
                label1.Text = " " + value.Trim() + " ";
                label1.AutoSize = true;
                label1.AutoSize = false;
                label1.Height = this.Height;
                panel1.Top = (this.Height / 2) - 1;
                panel2.Top = panel1.Top;
                panel2.Left = label1.Left + label1.Width - 2;
                panel2.Width = this.Width;
            }
        }

        protected override void OnFontChanged(EventArgs e)
        {
            label1.Font = this.Font;
            label1.AutoSize = true;
            label1.AutoSize = false;
            label1.Height = this.Height;
            panel1.Top = (this.Height / 2) - 1;
            panel2.Top = panel1.Top;
            panel2.Left = label1.Left + label1.Width - 2;
            panel2.Width = this.Width;
            base.OnFontChanged(e);
        }

        private void CustomHeaderControl_Resize(object sender, EventArgs e)
        {
            label1.Height = this.Height;
            panel1.Top = (this.Height / 2) - 1;
            panel2.Top = panel1.Top;
            panel2.Left = label1.Left + label1.Width - 2;
            panel2.Width = this.Width;
        }

        private void CustomHeaderControl_SizeChanged(object sender, EventArgs e)
        {
            label1.AutoSize = true;
            label1.AutoSize = false;
            label1.Height = this.Height;
            panel1.Top = (this.Height / 2) - 1;
            panel2.Top = panel1.Top;
            panel2.Left = label1.Left + label1.Width - 2;
            panel2.Width = this.Width;
        }
    }
}