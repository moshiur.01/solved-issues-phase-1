﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MISL.Ababil.Agent.CustomControls
{
    public partial class CustomSwitchControl : UserControl
    {
        private bool _mouseDown = false;
        private int tmpX = 0;
        private int tmpY = 0;

        public CustomSwitchControl()
        {
            InitializeComponent();
            //ControlDragMove.Init(btnSwitch, ControlDragMove.Direction.Horizontal);
        }

        private void btnSwitch_Click(object sender, EventArgs e)
        {
            if (btnSwitch.Left == 0)
            {
                btnSwitch.Left = btnSwitch.Width;
                btnSwitch.BackColor = Color.RoyalBlue;
            }
            else
            {
                btnSwitch.Left = 0;
                btnSwitch.BackColor = Color.DimGray;
            }
            btnSwitch.Focus();
        }

        private void btnSwitch_MouseMove(object sender, MouseEventArgs e)
        {
            //if (e.Button == MouseButtons.Left)
            //{
            //    try
            //    {
            //        btnSwitch.Left = e.X;
            //    }
            //    catch
            //    {
            //    }
            //}
            //else
            //{
            //    tmpX = e.X;
            //    tmpY = e.Y;
            //}

        }

        private void btnSwitch_MouseDown(object sender, MouseEventArgs e)
        {
            if (_mouseDown == false)
            {

                _mouseDown = true;
            }
        }

        private void btnSwitch_MouseUp(object sender, MouseEventArgs e)
        {
            _mouseDown = false;

        }

        private void btnSwitch_LocationChanged(object sender, EventArgs e)
        {
            //if (btnSwitch.Left >  this.Width/2)
            //{
            //    btnSwitch.Left = this.Width/2;
            //}
        }
    }
}
