﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using System.Drawing;
using System.Drawing.Imaging;
using MISL.Ababil.Agent.Infrastructure;
using MISL.Ababil.Agent.Services;
using MISL.Ababil.Agent.Infrastructure.Models.common;

namespace MISL.Ababil.Agent.LocalStorageService
{
    public class LocalStorageIO
    {
        private const string LocalStorageName = "Ababil Cache";

        public bool Reset()
        {
            try
            {
                string folder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), LocalStorageName);
                Directory.Delete(folder, true);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool LoadJsonToLocalStorage(string key, out string json)
        {
            json = "";
            string folder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), LocalStorageName);
            if (!Directory.Exists(folder))
            {
                json = null;
                return false;
            }
            key = Path.Combine(folder, key);
            if (!File.Exists(key))
            {
                json = null;
                return false;
            }
            StreamReader streamReader = new StreamReader(key, Encoding.Unicode);
            {
                json = streamReader.ReadToEnd();
            }
            streamReader.Close();
            return true;
        }

        public void SaveJsonToLocalStorage(string key, ref string json)
        {
            string folder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), LocalStorageName);
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            key = Path.Combine(folder, key);
            StreamWriter sw = new StreamWriter(key, false, Encoding.Unicode);
            {
                sw.Write(json);
            }
            sw.Close();
        }                

        public void SaveObjectToLocalStorage(object objectToCache, string key)
        {
            string folder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), LocalStorageName);
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            string cacheFileName = Path.Combine(folder, key);
            StreamWriter sw = new StreamWriter(cacheFileName, false, Encoding.Unicode);
            {
                string json = JsonConvert.SerializeObject(objectToCache);
                sw.Write(json);
            }
            sw.Close();
        }

        internal Image LoadImageToLocalStorage(string key)
        {
            string imageFileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), LocalStorageName, "Images", key);
            if (!File.Exists(imageFileName))
            {
                Image userPicture = GetProfilePictureByUserName(key);
                return GetProfilePictureByUserName(key);
            }
            else
            {
                //throw new Exception("Image Test");
                Bitmap bmp = new Bitmap(imageFileName);
                return bmp;
                //return Bitmap.FromFile(imageFileName);
            }
        }

        private Image GetProfilePictureByUserName(string key)
        {
            Dictionary<string, byte[]> returnedDictionary;
            ServiceResult serviceResult = ServiceResult.CreateServiceResult();
            LoginService loginService = new LoginService();
            byte[] imageBytes;
            try
            {
                serviceResult = loginService.GetUserImageByUserName(key.Replace("UserImage_", ""));
                if (serviceResult.Success)
                {
                    returnedDictionary = serviceResult.ReturnedObject as Dictionary<string, byte[]>;

                    if (returnedDictionary.ContainsKey("image"))
                    {
                        imageBytes = returnedDictionary["image"];
                        if (imageBytes.Length > 0) return UtilityServices.byteArrayToImage(imageBytes);
                        else return null;
                    }
                    else return null;
                }
                else return null;
            }
            catch (Exception exp)
            { return null; }
        }

        internal void SaveImageToLocalStorage(Image image, string key)
        {
            try
            {
                string folder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    LocalStorageName);
                string folderImage = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    LocalStorageName, "Images");
                string imageFileName = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), LocalStorageName, "Images", key);
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                if (!Directory.Exists(folderImage))
                {
                    Directory.CreateDirectory(folderImage);
                }

                //ImageCodecInfo jpgCodec = ImageCodecInfo.GetImageEncoders().Where(codec => codec.FormatID.Equals(ImageFormat.Jpeg.Guid)).FirstOrDefault();
                //MemoryStream ms = new MemoryStream();

                ////image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

                //System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
                //System.Drawing.Imaging.Encoder myEncoder2 = System.Drawing.Imaging.Encoder.ColorDepth;
                //System.Drawing.Imaging.Encoder myEncoder3 = System.Drawing.Imaging.Encoder.Compression;

                //EncoderParameters myEncoderParameters = new EncoderParameters(3);

                //EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 25L);
                //EncoderParameter myEncoderParameter2 = new EncoderParameter(myEncoder2, 16L);
                //EncoderParameter myEncoderParameter3 = new EncoderParameter(myEncoder3, (long)EncoderValue.CompressionLZW);

                //myEncoderParameters.Param[0] = myEncoderParameter;
                //myEncoderParameters.Param[1] = myEncoderParameter2;
                //myEncoderParameters.Param[2] = myEncoderParameter3;

                ////image.Save(ms, jpgCodec, myEncoderParameters);
                //image.Save(ms,ImageFormat.Jpeg);
                //ms.Close();
                FileStream fs;
                Bitmap bmp;
                try
                {
                    if (!File.Exists(imageFileName))
                    {
                        fs = new FileStream(imageFileName, FileMode.CreateNew);
                        //image.Save(fs, jpgCodec, myEncoderParameters);
                        bmp = new Bitmap(image);

                        //image.Save(fs, ImageFormat.Jpeg);
                        bmp.Save(fs, ImageFormat.Jpeg);

                        fs.Close();
                    }
                    //else
                }
                catch (Exception ex)
                {                    
                    bmp = null;
                    fs = null;
                }
            }
            catch (Exception ex)
            {
                //suppressed
            }
        }
    }
}