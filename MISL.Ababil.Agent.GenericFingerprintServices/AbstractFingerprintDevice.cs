﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MISL.Ababil.Agent.GenericFingerprintServices
{
    abstract public class AbstractFingerprintDevice : FingerprintDevice
    {
        List<FingerprintEventObserver> _observers = new List<FingerprintEventObserver>();

        abstract public void capture();

        public void notifyObservers(FingerprintEvents eventSpec, object eventData)
        {
            for (int observerIndex = 0; observerIndex < _observers.Count; observerIndex++)
            //foreach (FingerprintEventObserver observer in _observers)
            {
                FingerprintEventObserver observer = _observers[observerIndex];
                if (observer != null)
                {
                    observer.FingerPrintEventOccured(eventSpec, eventData);
                    _observers[observerIndex] = null;
                }
            }

            _observers.RemoveAll(x => x == null);
        }

        public void registerEventObserver(FingerprintEventObserver observer)
        {
            if (observer != null)
            {
                _observers.Add(observer);
            }
        }

        public abstract void registerFinger();

        ~AbstractFingerprintDevice()
        {
            _observers.Clear();
        }
    }
}
