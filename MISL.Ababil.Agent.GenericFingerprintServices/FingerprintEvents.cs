﻿namespace MISL.Ababil.Agent.GenericFingerprintServices
{
    public enum FingerprintEvents { Captured, Registered };
}